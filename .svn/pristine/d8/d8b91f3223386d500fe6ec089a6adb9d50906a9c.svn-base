package com.ites.bankfinance.config;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.core.MethodParameter;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;


public class SecurityContextWrappedMultipartRequestArgumentResolver implements WebArgumentResolver {

    private final CommonsMultipartResolver commonsMultipartResolver;

    public SecurityContextWrappedMultipartRequestArgumentResolver() {
        this.commonsMultipartResolver = new CommonsMultipartResolver();
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, NativeWebRequest webRequest) throws Exception {
        if (MultipartHttpServletRequest.class.equals(methodParameter.getParameterType())) {
            Object object = webRequest.getNativeRequest();
            if (!(object instanceof SecurityContextHolderAwareRequestWrapper)) {
                return UNRESOLVED;
            }
            HttpServletRequest request = (HttpServletRequest) object;
            if (!ServletFileUpload.isMultipartContent(request)) {
                return UNRESOLVED;
            }
            SecurityContextHolderAwareRequestWrapper requestWrapper = (SecurityContextHolderAwareRequestWrapper) request;
            return commonsMultipartResolver.resolveMultipart(requestWrapper);
        }
        return UNRESOLVED;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.serviceImpl;

import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserRole;
import com.ites.bankfinance.service.UserService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
public class UserServiceImpl implements UserDetailsService,UserService{

    @Autowired
    private UserDao userDao;
        
    @Transactional(readOnly=true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        UmUser user = userDao.findByUserName(username);
        List<GrantedAuthority> authorities = buildUserAuthority(userDao.getUserRole(username));

        return buildUserForAuthentication(user, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UmUserRole> userRoles) {
        
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        for (UmUserRole userRole : userRoles) {
                setAuths.add(new SimpleGrantedAuthority(userRole.getUserRole()));
        }
        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
        return Result;
    }

    private UserDetails buildUserForAuthentication(UmUser user, List<GrantedAuthority> authorities) {
        return new User(user.getUserName(), user.getPassword(), user.getIsActive(), true, true, true, authorities);
    }
    
    
    @Transactional
    public int findByUserName() {
        return userDao.findByUserName();
    }

    
    
    @Transactional
    public int findUserTypeByUserId(int userId) {
        return userDao.findUserTypeByUserId(userId);
    }
    
    @Transactional
    public Employee findEmployeeById(int empId) {
        return userDao.findEmployeeById(empId);
    }
    
    @Transactional
    public String findApprovalsByPassword(String password){
        return userDao.findApprovalsByPassword(password);
    }

    @Transactional
    public boolean findLoggedUserApproval() {
        return userDao.findLoggedUserApproval();
    }

    
}

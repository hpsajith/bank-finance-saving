/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

public class PaymentDetails {

    private String paymentDate;
    private String installment;
    private String roundInstallment;
    private String total;
    private String balance;
    private String lossAmount;
    private Integer loanId;
    private String loanAmount;
    private String downPayment;
    private String investment;
    private String agreementNo;
    private String interest;
    private String totalInterest;
    private String othercharges;
    private Double o_charges;
    private String amountWithCharges;
    private String rate;
    private String capitalizeAmount;

    public PaymentDetails() {
    }

    public PaymentDetails(String paymentDate, String installment, String roundInstallment, String total, String balance, String lossAmount, Integer loanId, String loanAmount, String downPayment, String investment, String agreementNo, String interest, String totalInterest, String othercharges, Double o_charges, String amountWithCharges, String rate, String capitalizeAmount) {
        this.paymentDate = paymentDate;
        this.installment = installment;
        this.roundInstallment = roundInstallment;
        this.total = total;
        this.balance = balance;
        this.lossAmount = lossAmount;
        this.loanId = loanId;
        this.loanAmount = loanAmount;
        this.downPayment = downPayment;
        this.investment = investment;
        this.agreementNo = agreementNo;
        this.interest = interest;
        this.totalInterest = totalInterest;
        this.othercharges = othercharges;
        this.o_charges = o_charges;
        this.amountWithCharges = amountWithCharges;
        this.rate = rate;
        this.capitalizeAmount = capitalizeAmount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getRoundInstallment() {
        return roundInstallment;
    }

    public void setRoundInstallment(String roundInstallment) {
        this.roundInstallment = roundInstallment;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getLossAmount() {
        return lossAmount;
    }

    public void setLossAmount(String lossAmount) {
        this.lossAmount = lossAmount;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(String downPayment) {
        this.downPayment = downPayment;
    }

    public String getInvestment() {
        return investment;
    }

    public void setInvestment(String investment) {
        this.investment = investment;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getTotalInterest() {
        return totalInterest;
    }

    public void setTotalInterest(String totalInterest) {
        this.totalInterest = totalInterest;
    }

    public String getOthercharges() {
        return othercharges;
    }

    public void setOthercharges(String othercharges) {
        this.othercharges = othercharges;
    }

    public Double getO_charges() {
        return o_charges;
    }

    public void setO_charges(Double o_charges) {
        this.o_charges = o_charges;
    }

    public String getAmountWithCharges() {
        return amountWithCharges;
    }

    public void setAmountWithCharges(String amountWithCharges) {
        this.amountWithCharges = amountWithCharges;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCapitalizeAmount() {
        return capitalizeAmount;
    }

    public void setCapitalizeAmount(String capitalizeAmount) {
        this.capitalizeAmount = capitalizeAmount;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.Date;

/**
 *
 * @author ites
 */
public class DayPlanTargetModel {
    
    private Integer loanType;
    private Double dueAmount;
    private Double arriesAmount;
    private Double duaRate;
    private Double arriesRate;
    private Date monthStart;
    private Date monthEnd;

    public DayPlanTargetModel() {
    }

    public DayPlanTargetModel(Integer loanType, Double dueAmount, Double arriesAmount, Double duaRate, Double arriesRate, Date monthStart, Date monthEnd) {
        this.loanType = loanType;
        this.dueAmount = dueAmount;
        this.arriesAmount = arriesAmount;
        this.duaRate = duaRate;
        this.arriesRate = arriesRate;
        this.monthStart = monthStart;
        this.monthEnd = monthEnd;
    }

    public Integer getLoanType() {
        return loanType;
    }

    public void setLoanType(Integer loanType) {
        this.loanType = loanType;
    }

    public Double getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(Double dueAmount) {
        this.dueAmount = dueAmount;
    }

    public Double getArriesAmount() {
        return arriesAmount;
    }

    public void setArriesAmount(Double arriesAmount) {
        this.arriesAmount = arriesAmount;
    }

    public Double getDuaRate() {
        return duaRate;
    }

    public void setDuaRate(Double duaRate) {
        this.duaRate = duaRate;
    }

    public Double getArriesRate() {
        return arriesRate;
    }

    public void setArriesRate(Double arriesRate) {
        this.arriesRate = arriesRate;
    }

    public Date getMonthStart() {
        return monthStart;
    }

    public void setMonthStart(Date monthStart) {
        this.monthStart = monthStart;
    }

    public Date getMonthEnd() {
        return monthEnd;
    }

    public void setMonthEnd(Date monthEnd) {
        this.monthEnd = monthEnd;
    }    
}

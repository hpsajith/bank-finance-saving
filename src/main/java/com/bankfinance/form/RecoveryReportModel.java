/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import com.ites.bankfinance.model.LoanHeaderDetails;
import java.util.List;

/**
 *
 * @author Uththara
 */
public class RecoveryReportModel {
   private int custmerId; 
   private String name;
   private String nic;
   private String address;
   private int mobile;
   private List<viewLoan> loanList;
   private List<viewLoan> guarantorList;
   private String comment;
   private String title;

    public RecoveryReportModel() {
    }
   
   
   
    public RecoveryReportModel(int custmerId,String name, String nic, String address, int mobile, List<viewLoan> loanList, List<viewLoan> guarantorList, String comment, String title) {
        this.custmerId = custmerId;
        this.name = name;
        this.nic = nic;
        this.address = address;
        this.mobile = mobile;
        this.loanList = loanList;
        this.guarantorList = guarantorList;
        this.comment = comment;
        this.title = title;
        
    }

    public int getCustmerId() {
        return custmerId;
    }

    public void setCustmerId(int custmerId) {
        this.custmerId = custmerId;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMobile() {
        return mobile;
    }

    public void setMobile(int mobile) {
        this.mobile = mobile;
    }

    public List<viewLoan> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<viewLoan> loanList) {
        this.loanList = loanList;
    }

    public List<viewLoan> getGuarantorList() {
        return guarantorList;
    }

    public void setGuarantorList(List<viewLoan> guarantorList) {
        this.guarantorList = guarantorList;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
   
    
   
}

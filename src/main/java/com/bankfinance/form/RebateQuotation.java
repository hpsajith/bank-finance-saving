package com.bankfinance.form;

/**
 *
 * @author admin
 */
public class RebateQuotation {

    private String debtorName;
    private String epfNo;
    private String facilityNo;
    private String vehicleNo;
    private double loanBalance;
    private double dateBalance;
    private double futureCapital;
    private double futureInterest;
    private double rebateRate;
    private double discount;
    private double rebateAmount;
    private double rebateAmountWithTax;
    private double rentalOutstanding;
    private double interestCharge;
    private double rebateInterestCharge;
    private double odiWithTax;
    private double transferFee;
    private double transferFeeWithTax;
    private double insuranceCharges;
    private double seizingCharges;
    private double parkingCharges;
    private double valuationCharges;
    private double legalCharges;
    private double otherCharges;
    private double repaymentBalance;
    private double ppWithTaxt;
    private double ovpBalance;
    private double secDeposit;
    private double foregoneDeposit;
    private double totRecoverable;
    private String rebateValidDate;

    public RebateQuotation() {
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public String getEpfNo() {
        return epfNo;
    }

    public void setEpfNo(String epfNo) {
        this.epfNo = epfNo;
    }

    public String getFacilityNo() {
        return facilityNo;
    }

    public void setFacilityNo(String facilityNo) {
        this.facilityNo = facilityNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public double getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(double loanBalance) {
        this.loanBalance = loanBalance;
    }

    public double getFutureCapital() {
        return futureCapital;
    }

    public void setFutureCapital(double futureCapital) {
        this.futureCapital = futureCapital;
    }

    public double getFutureInterest() {
        return futureInterest;
    }

    public void setFutureInterest(double futureInterest) {
        this.futureInterest = futureInterest;
    }

    public double getRebateRate() {
        return rebateRate;
    }

    public void setRebateRate(double rebateRate) {
        this.rebateRate = rebateRate;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getRebateAmount() {
        return rebateAmount;
    }

    public void setRebateAmount(double rebateAmount) {
        this.rebateAmount = rebateAmount;
    }

    public double getRebateAmountWithTax() {
        return rebateAmountWithTax;
    }

    public void setRebateAmountWithTax(double rebateAmountWithTax) {
        this.rebateAmountWithTax = rebateAmountWithTax;
    }

    public double getRentalOutstanding() {
        return rentalOutstanding;
    }

    public void setRentalOutstanding(double rentalOutstanding) {
        this.rentalOutstanding = rentalOutstanding;
    }

    public double getInterestCharge() {
        return interestCharge;
    }

    public void setInterestCharge(double interestCharge) {
        this.interestCharge = interestCharge;
    }

    public double getRebateInterestCharge() {
        return rebateInterestCharge;
    }

    public void setRebateInterestCharge(double rebateInterestCharge) {
        this.rebateInterestCharge = rebateInterestCharge;
    }

    public double getOdiWithTax() {
        return odiWithTax;
    }

    public void setOdiWithTax(double odiWithTax) {
        this.odiWithTax = odiWithTax;
    }

    public double getTransferFee() {
        return transferFee;
    }

    public void setTransferFee(double transferFee) {
        this.transferFee = transferFee;
    }

    public double getTransferFeeWithTax() {
        return transferFeeWithTax;
    }

    public void setTransferFeeWithTax(double transferFeeWithTax) {
        this.transferFeeWithTax = transferFeeWithTax;
    }

    public double getInsuranceCharges() {
        return insuranceCharges;
    }

    public void setInsuranceCharges(double insuranceCharges) {
        this.insuranceCharges = insuranceCharges;
    }

    public double getSeizingCharges() {
        return seizingCharges;
    }

    public void setSeizingCharges(double seizingCharges) {
        this.seizingCharges = seizingCharges;
    }

    public double getParkingCharges() {
        return parkingCharges;
    }

    public void setParkingCharges(double parkingCharges) {
        this.parkingCharges = parkingCharges;
    }

    public double getValuationCharges() {
        return valuationCharges;
    }

    public void setValuationCharges(double valuationCharges) {
        this.valuationCharges = valuationCharges;
    }

    public double getLegalCharges() {
        return legalCharges;
    }

    public void setLegalCharges(double legalCharges) {
        this.legalCharges = legalCharges;
    }

    public double getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(double otherCharges) {
        this.otherCharges = otherCharges;
    }

    public double getRepaymentBalance() {
        return repaymentBalance;
    }

    public void setRepaymentBalance(double repaymentBalance) {
        this.repaymentBalance = repaymentBalance;
    }

    public double getPpWithTaxt() {
        return ppWithTaxt;
    }

    public void setPpWithTaxt(double ppWithTaxt) {
        this.ppWithTaxt = ppWithTaxt;
    }

    public double getOvpBalance() {
        return ovpBalance;
    }

    public void setOvpBalance(double ovpBalance) {
        this.ovpBalance = ovpBalance;
    }

    public double getSecDeposit() {
        return secDeposit;
    }

    public void setSecDeposit(double secDeposit) {
        this.secDeposit = secDeposit;
    }

    public double getForegoneDeposit() {
        return foregoneDeposit;
    }

    public void setForegoneDeposit(double foregoneDeposit) {
        this.foregoneDeposit = foregoneDeposit;
    }

    public double getTotRecoverable() {
        return totRecoverable;
    }

    public void setTotRecoverable(double totRecoverable) {
        this.totRecoverable = totRecoverable;
    }

    public double getDateBalance() {
        return dateBalance;
    }

    public void setDateBalance(double dateBalance) {
        this.dateBalance = dateBalance;
    }

    public String getRebateValidDate() {
        return rebateValidDate;
    }

    public void setRebateValidDate(String rebateValidDate) {
        this.rebateValidDate = rebateValidDate;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author IT
 */
@XmlRootElement(name = "ArriesLoans")
@XmlAccessorType (XmlAccessType.FIELD)
public class ArriesLoans {
    
    @XmlElement(name = "ArriesLoan")
    private List<ArriesLoan> arriesLoans = null;

    public ArriesLoans(){}
    
    /**
     * @return the arriesLoans
     */
    public List<ArriesLoan> getArriesLoans() {
        return arriesLoans;
    }

    /**
     * @param arriesLoans the arriesLoans to set
     */
    public void setArriesLoans(List<ArriesLoan> arriesLoans) {
        this.arriesLoans = arriesLoans;
    }
    
}

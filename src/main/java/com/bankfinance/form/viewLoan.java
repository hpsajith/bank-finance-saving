/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.LoanApprovedLevels;
import com.ites.bankfinance.model.LoanOtherCharges;
import java.util.Date;
import java.util.List;

public class viewLoan {

    private Integer loanId;
    private Integer debtorId;
    private String debtorName;
    private String debtorAddress;
    private Integer debtorTelephone;
    private String loanType;
    private Double loanAmount;
    private String loanPeriod;
    private Date loanDate;
    private Integer loanStatus;
    private Integer loanSpec;
    private Integer loanApprovalStatus;
    private Integer loanDocumentSubmission;
    private Integer loanIsIssue;
    private String agreementNo;
    private Integer userType;
    private Integer appLevelId;
    private List<DebtorHeaderDetails> loanGuarantor;
    private LoanOtherCharges[] loanOtherCharges;
    private Double loanInstallment;
    private List<LoanApprovedLevels> loanApprovals;
    private ApprovalDetailsModel userApproval;
    private DebtorHeaderDetails debtorHeaderDetails;
    private String nic;
    private String loanBookNo;
    private String remark;
    private String shiftDate;
    private Integer instlmntId;
    private boolean msgSameShiftDate;
    private boolean msgAfterShiftDate;
    private int msgDateShifted;
    private int branchId;
    private Date dueDate;
    private String vehicleNo;
    private String group;
    private int loanCount;
    private Integer userId;
    private Integer empId;

    public viewLoan() {
    }

    public viewLoan(int loanId, int loanperiod, Date dueDate, DebtorHeaderDetails debtorHeaderDetails, String agreementNo, double loanAmount, String loanBookNo, Integer instlmntId, Integer loanStatus, Integer loanSpec,Integer userId, Integer empId) {
        this.loanId = loanId;
        this.loanPeriod = loanperiod + "";
        this.dueDate = dueDate;
        this.debtorHeaderDetails = debtorHeaderDetails;
        this.agreementNo = agreementNo;
        this.loanAmount = loanAmount;
        this.loanBookNo = loanBookNo;
        this.instlmntId = instlmntId;
        this.loanStatus = loanStatus;
        this.loanSpec = loanSpec;
        this.userId = userId;
        this.empId = empId;
    }

    public viewLoan(Integer loanId, Integer debtorId, String debtorName, String debtorAddress, Integer debtorTelephone, String loanType, Double loanAmount, String loanPeriod, Date loanDate, Integer loanStatus, Integer loanSpec, Integer loanApprovalStatus, Integer loanDocumentSubmission, Integer loanIsIssue, String agreementNo, Integer userType, Integer appLevelId, List<DebtorHeaderDetails> loanGuarantor, LoanOtherCharges[] loanOtherCharges, Double loanInstallment, List<LoanApprovedLevels> loanApprovals, ApprovalDetailsModel userApproval, DebtorHeaderDetails debtorHeaderDetails, String nic, String loanBookNo, String remark, String shiftDate, Integer instlmntId, boolean msgSameShiftDate, boolean msgAfterShiftDate, int msgDateShifted, int branchId, Date dueDate, String vehicleNo, String group,Integer userId, Integer empId) {
        this.loanId = loanId;
        this.debtorId = debtorId;
        this.debtorName = debtorName;
        this.debtorAddress = debtorAddress;
        this.debtorTelephone = debtorTelephone;
        this.loanType = loanType;
        this.loanAmount = loanAmount;
        this.loanPeriod = loanPeriod;
        this.loanDate = loanDate;
        this.loanStatus = loanStatus;
        this.loanSpec = loanSpec;
        this.loanApprovalStatus = loanApprovalStatus;
        this.loanDocumentSubmission = loanDocumentSubmission;
        this.loanIsIssue = loanIsIssue;
        this.agreementNo = agreementNo;
        this.userType = userType;
        this.appLevelId = appLevelId;
        this.loanGuarantor = loanGuarantor;
        this.loanOtherCharges = loanOtherCharges;
        this.loanInstallment = loanInstallment;
        this.loanApprovals = loanApprovals;
        this.userApproval = userApproval;
        this.debtorHeaderDetails = debtorHeaderDetails;
        this.nic = nic;
        this.loanBookNo = loanBookNo;
        this.remark = remark;
        this.shiftDate = shiftDate;
        this.instlmntId = instlmntId;
        this.msgSameShiftDate = msgSameShiftDate;
        this.msgAfterShiftDate = msgAfterShiftDate;
        this.msgDateShifted = msgDateShifted;
        this.branchId = branchId;
        this.dueDate = dueDate;
        this.vehicleNo = vehicleNo;
        this.group = group;
        this.userId = userId;
        this.empId = empId;
    }

    

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(Integer debtorId) {
        this.debtorId = debtorId;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public String getDebtorAddress() {
        return debtorAddress;
    }

    public void setDebtorAddress(String debtorAddress) {
        this.debtorAddress = debtorAddress;
    }

    public Integer getDebtorTelephone() {
        return debtorTelephone;
    }

    public void setDebtorTelephone(Integer debtorTelephone) {
        this.debtorTelephone = debtorTelephone;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public Double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getLoanPeriod() {
        return loanPeriod;
    }

    public void setLoanPeriod(String loanPeriod) {
        this.loanPeriod = loanPeriod;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public Integer getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(Integer loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Integer getLoanApprovalStatus() {
        return loanApprovalStatus;
    }

    public void setLoanApprovalStatus(Integer loanApprovalStatus) {
        this.loanApprovalStatus = loanApprovalStatus;
    }

    public Integer getLoanDocumentSubmission() {
        return loanDocumentSubmission;
    }

    public void setLoanDocumentSubmission(Integer loanDocumentSubmission) {
        this.loanDocumentSubmission = loanDocumentSubmission;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getAppLevelId() {
        return appLevelId;
    }

    public void setAppLevelId(Integer appLevelId) {
        this.appLevelId = appLevelId;
    }

    public Integer getLoanIsIssue() {
        return loanIsIssue;
    }

    public void setLoanIsIssue(Integer loanIsIssue) {
        this.loanIsIssue = loanIsIssue;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public List<DebtorHeaderDetails> getLoanGuarantor() {
        return loanGuarantor;
    }

    public void setLoanGuarantor(List<DebtorHeaderDetails> loanGuarantor) {
        this.loanGuarantor = loanGuarantor;
    }

    public LoanOtherCharges[] getLoanOtherCharges() {
        return loanOtherCharges;
    }

    public void setLoanOtherCharges(LoanOtherCharges[] loanOtherCharges) {
        this.loanOtherCharges = loanOtherCharges;
    }

    public Double getLoanInstallment() {
        return loanInstallment;
    }

    public void setLoanInstallment(Double loanInstallment) {
        this.loanInstallment = loanInstallment;
    }

    public List<LoanApprovedLevels> getLoanApprovals() {
        return loanApprovals;
    }

    public void setLoanApprovals(List<LoanApprovedLevels> loanApprovals) {
        this.loanApprovals = loanApprovals;
    }

    public ApprovalDetailsModel getUserApproval() {
        return userApproval;
    }

    public void setUserApproval(ApprovalDetailsModel userApproval) {
        this.userApproval = userApproval;
    }

    public DebtorHeaderDetails getDebtorHeaderDetails() {
        return debtorHeaderDetails;
    }

    public void setDebtorHeaderDetails(DebtorHeaderDetails debtorHeaderDetails) {
        this.debtorHeaderDetails = debtorHeaderDetails;
    }

    public Integer getInstlmntId() {
        return instlmntId;
    }

    public void setInstlmntId(Integer instlmntId) {
        this.instlmntId = instlmntId;
    }

    public boolean isMsgSameShiftDate() {
        return msgSameShiftDate;
    }

    public void setMsgSameShiftDate(boolean msgSameShiftDate) {
        this.msgSameShiftDate = msgSameShiftDate;
    }

    public boolean isMsgAfterShiftDate() {
        return msgAfterShiftDate;
    }

    public void setMsgAfterShiftDate(boolean msgAfterShiftDate) {
        this.msgAfterShiftDate = msgAfterShiftDate;
    }

    public int getMsgDateShifted() {
        return msgDateShifted;
    }

    public void setMsgDateShifted(int msgDateShifted) {
        this.msgDateShifted = msgDateShifted;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getLoanBookNo() {
        return loanBookNo;
    }

    public void setLoanBookNo(String loanBookNo) {
        this.loanBookNo = loanBookNo;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getShiftDate() {
        return shiftDate;
    }

    public void setShiftDate(String shiftDate) {
        this.shiftDate = shiftDate;
    }

    public Integer getLoanSpec() {
        return loanSpec;
    }

    public void setLoanSpec(Integer loanSpec) {
        this.loanSpec = loanSpec;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    /**
     * @return the group
     */
    public String getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(String group) {
        this.group = group;
    }

    public int getLoanCount() {
        return loanCount;
    }

    public void setLoanCount(int loanCount) {
        this.loanCount = loanCount;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }
}

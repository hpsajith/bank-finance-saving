/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import com.ites.bankfinance.model.RecoveryLetterDetail;
import com.ites.bankfinance.model.RecoveryVisitsDetails;
import java.util.List;

/**
 *
 * @author ites
 */
public class RecoveryHistory {
    
    private String dueDate;
    private List<RecoveryLetterDetail> letters;
    private List<RecoveryVisitsDetails> visits;

    public RecoveryHistory() {
    }
    
    public RecoveryHistory(String dueDate, List<RecoveryLetterDetail> letters, List<RecoveryVisitsDetails> visits) {
        this.dueDate = dueDate;
        this.letters = letters;
        this.visits = visits;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public List<RecoveryLetterDetail> getLetters() {
        return letters;
    }

    public void setLetters(List<RecoveryLetterDetail> letters) {
        this.letters = letters;
    }

    public List<RecoveryVisitsDetails> getVisits() {
        return visits;
    }

    public void setVisits(List<RecoveryVisitsDetails> visits) {
        this.visits = visits;
    }
    
    
}

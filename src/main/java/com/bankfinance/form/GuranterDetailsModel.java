/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

/**
 *
 * @author Administrator
 */
public class GuranterDetailsModel {
    public Integer debtId;
    public String debtName;
    public String debtNic;
    public String debtAddress;
    public String debtTp;
    public Integer debtOrder;

    public GuranterDetailsModel() {
    }
    
    
    public GuranterDetailsModel(Integer debtId, String debtName, String debtNic, String debtAddress, String debtTp, Integer debtOrder) {
        this.debtId = debtId;
        this.debtName = debtName;
        this.debtNic = debtNic;
        this.debtAddress = debtAddress;
        this.debtTp = debtTp;
        this.debtOrder = debtOrder;
    }

    
    
    
    public Integer getDebtId() {
        return debtId;
    }

    public void setDebtId(Integer debtId) {
        this.debtId = debtId;
    }

    public String getDebtName() {
        return debtName;
    }

    public void setDebtName(String debtName) {
        this.debtName = debtName;
    }

    public String getDebtNic() {
        return debtNic;
    }

    public void setDebtNic(String debtNic) {
        this.debtNic = debtNic;
    }

    public String getDebtAddress() {
        return debtAddress;
    }

    public void setDebtAddress(String debtAddress) {
        this.debtAddress = debtAddress;
    }

    public String getDebtTp() {
        return debtTp;
    }

    public void setDebtTp(String debtTp) {
        this.debtTp = debtTp;
    }

    public Integer getDebtOrder() {
        return debtOrder;
    }

    public void setDebtOrder(Integer debtOrder) {
        this.debtOrder = debtOrder;
    }
    
    
    
}

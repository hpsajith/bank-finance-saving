/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.Date;

/**
 *
 * @author uththara
 */
public class OpenLoanForm {
    
    String bookNo;
    int branch;    
    
    int noOfInstallment;
    int periodType;
    Date lapsDate;
    int dueDate;
    Double rate;
    int loanType;
    
    String debtorName;
    String debtorNic;
    String debtorAdress;
    Integer debtorTele1;
    Integer debtorTele2;
    Integer debtorTele3;
    
    String gurantor1Name;
    String gurantor1Nic;
    String gurantor1Adress;
    Integer gurantor1Tele1;
    Integer gurantor1Tele2;
    Integer gurantor1Tele3;
    
    String gurantor2Name;
    String gurantor2Nic;
    String gurantor2Adress;
    Integer gurantor2Tele1;
    Integer gurantor2Tele2;
    Integer gurantor2Tele3;
            
    Double loanAmount;
    Double downPayment;
    Double investment;
    Double installment;
    Double monthInterest;
    Double OverPayment;
    Double arresWithoutOdi;
    Double totalArresrs;
    Double totalReceible;
    Double totalPyaments;
    Double stockbalance;
    Double suspendInterest;
    
    int paidInstallment;
    int remainInstallment;
    int areasInstallment;
    
    Double aresInsurance;
    Double aresOdi;
    Double aresRegisraion;
    Double aresSeizing;
    Double aresRecovery;
    Double aresLetter;
    Double aresValuation;
    Double aresBank;
    Double aresCheq;

    public OpenLoanForm() {
    }

    
    
    public OpenLoanForm(String bookNo, int branch, int noOfInstallment, int periodType, Date lapsDate, int dueDate, Double rate, int loanType, String debtorName, String debtorNic, String debtorAdress, Integer debtorTele1, Integer debtorTele2, Integer debtorTele3, String gurantor1Name, String gurantor1Nic, String gurantor1Adress, Integer gurantor1Tele1, Integer gurantor1Tele2, Integer gurantor1Tele3, String gurantor2Name, String gurantor2Nic, String gurantor2Adress, Integer gurantor2Tele1, Integer gurantor2Tele2, Integer gurantor2Tele3, Double loanAmount, Double downPayment, Double investment, Double installment, Double monthInterest, Double OverPayment, Double arresWithoutOdi, Double totalArresrs, Double totalReceible, Double totalPyaments, Double stockbalance, Double suspendInterest, int paidInstallment, int remainInstallment, int areasInstallment, Double aresInsurance, Double aresOdi, Double aresRegisraion, Double aresSeizing, Double aresRecovery, Double aresLetter, Double aresValuation, Double aresBank, Double aresCheq) {
        this.bookNo = bookNo;
        this.branch = branch;
        this.noOfInstallment = noOfInstallment;
        this.periodType = periodType;
        this.lapsDate = lapsDate;
        this.dueDate = dueDate;
        this.rate = rate;
        this.loanType = loanType;
        this.debtorName = debtorName;
        this.debtorNic = debtorNic;
        this.debtorAdress = debtorAdress;
        this.debtorTele1 = debtorTele1;
        this.debtorTele2 = debtorTele2;
        this.debtorTele3 = debtorTele3;
        this.gurantor1Name = gurantor1Name;
        this.gurantor1Nic = gurantor1Nic;
        this.gurantor1Adress = gurantor1Adress;
        this.gurantor1Tele1 = gurantor1Tele1;
        this.gurantor1Tele2 = gurantor1Tele2;
        this.gurantor1Tele3 = gurantor1Tele3;
        this.gurantor2Name = gurantor2Name;
        this.gurantor2Nic = gurantor2Nic;
        this.gurantor2Adress = gurantor2Adress;
        this.gurantor2Tele1 = gurantor2Tele1;
        this.gurantor2Tele2 = gurantor2Tele2;
        this.gurantor2Tele3 = gurantor2Tele3;
        this.loanAmount = loanAmount;
        this.downPayment = downPayment;
        this.investment = investment;
        this.installment = installment;
        this.monthInterest = monthInterest;
        this.OverPayment = OverPayment;
        this.arresWithoutOdi = arresWithoutOdi;
        this.totalArresrs = totalArresrs;
        this.totalReceible = totalReceible;
        this.totalPyaments = totalPyaments;
        this.stockbalance = stockbalance;
        this.suspendInterest = suspendInterest;
        this.paidInstallment = paidInstallment;
        this.remainInstallment = remainInstallment;
        this.areasInstallment = areasInstallment;
        this.aresInsurance = aresInsurance;
        this.aresOdi = aresOdi;
        this.aresRegisraion = aresRegisraion;
        this.aresSeizing = aresSeizing;
        this.aresRecovery = aresRecovery;
        this.aresLetter = aresLetter;
        this.aresValuation = aresValuation;
        this.aresBank = aresBank;
        this.aresCheq = aresCheq;
    }

    

    public String getBookNo() {
        return bookNo;
    }

    public void setBookNo(String bookNo) {
        this.bookNo = bookNo;
    }

    public int getBranch() {
        return branch;
    }

    public void setBranch(int branch) {
        this.branch = branch;
    }

    public int getNoOfInstallment() {
        return noOfInstallment;
    }

    public void setNoOfInstallment(int noOfInstallment) {
        this.noOfInstallment = noOfInstallment;
    }

    public int getPeriodType() {
        return periodType;
    }

    public void setPeriodType(int periodType) {
        this.periodType = periodType;
    }

    public Date getLapsDate() {
        return lapsDate;
    }

    public void setLapsDate(Date lapsDate) {
        this.lapsDate = lapsDate;
    }

    public int getDueDate() {
        return dueDate;
    }

    public void setDueDate(int dueDate) {
        this.dueDate = dueDate;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public String getDebtorNic() {
        return debtorNic;
    }

    public void setDebtorNic(String debtorNic) {
        this.debtorNic = debtorNic;
    }

    public String getDebtorAdress() {
        return debtorAdress;
    }

    public void setDebtorAdress(String debtorAdress) {
        this.debtorAdress = debtorAdress;
    }

    public Integer getDebtorTele1() {
        return debtorTele1;
    }

    public void setDebtorTele1(Integer debtorTele1) {
        this.debtorTele1 = debtorTele1;
    }

    public Integer getDebtorTele2() {
        return debtorTele2;
    }

    public void setDebtorTele2(Integer debtorTele2) {
        this.debtorTele2 = debtorTele2;
    }

    public Integer getDebtorTele3() {
        return debtorTele3;
    }

    public void setDebtorTele3(Integer debtorTele3) {
        this.debtorTele3 = debtorTele3;
    }

    public String getGurantor1Name() {
        return gurantor1Name;
    }

    public void setGurantor1Name(String gurantor1Name) {
        this.gurantor1Name = gurantor1Name;
    }

    public String getGurantor1Nic() {
        return gurantor1Nic;
    }

    public void setGurantor1Nic(String gurantor1Nic) {
        this.gurantor1Nic = gurantor1Nic;
    }

    public String getGurantor1Adress() {
        return gurantor1Adress;
    }

    public void setGurantor1Adress(String gurantor1Adress) {
        this.gurantor1Adress = gurantor1Adress;
    }

    public Integer getGurantor1Tele1() {
        return gurantor1Tele1;
    }

    public void setGurantor1Tele1(Integer gurantor1Tele1) {
        this.gurantor1Tele1 = gurantor1Tele1;
    }

    public Integer getGurantor1Tele2() {
        return gurantor1Tele2;
    }

    public void setGurantor1Tele2(Integer gurantor1Tele2) {
        this.gurantor1Tele2 = gurantor1Tele2;
    }

    public Integer getGurantor1Tele3() {
        return gurantor1Tele3;
    }

    public void setGurantor1Tele3(Integer gurantor1Tele3) {
        this.gurantor1Tele3 = gurantor1Tele3;
    }

    public String getGurantor2Name() {
        return gurantor2Name;
    }

    public void setGurantor2Name(String gurantor2Name) {
        this.gurantor2Name = gurantor2Name;
    }

    public String getGurantor2Nic() {
        return gurantor2Nic;
    }

    public void setGurantor2Nic(String gurantor2Nic) {
        this.gurantor2Nic = gurantor2Nic;
    }

    public String getGurantor2Adress() {
        return gurantor2Adress;
    }

    public void setGurantor2Adress(String gurantor2Adress) {
        this.gurantor2Adress = gurantor2Adress;
    }

    public Integer getGurantor2Tele1() {
        return gurantor2Tele1;
    }

    public void setGurantor2Tele1(Integer gurantor2Tele1) {
        this.gurantor2Tele1 = gurantor2Tele1;
    }

    public Integer getGurantor2Tele2() {
        return gurantor2Tele2;
    }

    public void setGurantor2Tele2(Integer gurantor2Tele2) {
        this.gurantor2Tele2 = gurantor2Tele2;
    }

    public Integer getGurantor2Tele3() {
        return gurantor2Tele3;
    }

    public void setGurantor2Tele3(Integer gurantor2Tele3) {
        this.gurantor2Tele3 = gurantor2Tele3;
    }

    public Double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Double getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(Double downPayment) {
        this.downPayment = downPayment;
    }

    public Double getInvestment() {
        return investment;
    }

    public void setInvestment(Double investment) {
        this.investment = investment;
    }

    public Double getInstallment() {
        return installment;
    }

    public void setInstallment(Double installment) {
        this.installment = installment;
    }

    public Double getMonthInterest() {
        return monthInterest;
    }

    public void setMonthInterest(Double monthInterest) {
        this.monthInterest = monthInterest;
    }

    public Double getOverPayment() {
        return OverPayment;
    }

    public void setOverPayment(Double OverPayment) {
        this.OverPayment = OverPayment;
    }

    public Double getArresWithoutOdi() {
        return arresWithoutOdi;
    }

    public void setArresWithoutOdi(Double arresWithoutOdi) {
        this.arresWithoutOdi = arresWithoutOdi;
    }

    public Double getTotalArresrs() {
        return totalArresrs;
    }

    public void setTotalArresrs(Double totalArresrs) {
        this.totalArresrs = totalArresrs;
    }

    public Double getTotalReceible() {
        return totalReceible;
    }

    public void setTotalReceible(Double totalReceible) {
        this.totalReceible = totalReceible;
    }

    public Double getTotalPyaments() {
        return totalPyaments;
    }

    public void setTotalPyaments(Double totalPyaments) {
        this.totalPyaments = totalPyaments;
    }

    public int getPaidInstallment() {
        return paidInstallment;
    }

    public void setPaidInstallment(int paidInstallment) {
        this.paidInstallment = paidInstallment;
    }

    public int getRemainInstallment() {
        return remainInstallment;
    }

    public void setRemainInstallment(int remainInstallment) {
        this.remainInstallment = remainInstallment;
    }

    public int getAreasInstallment() {
        return areasInstallment;
    }

    public void setAreasInstallment(int areasInstallment) {
        this.areasInstallment = areasInstallment;
    }

    public Double getAresInsurance() {
        return aresInsurance;
    }

    public void setAresInsurance(Double aresInsurance) {
        this.aresInsurance = aresInsurance;
    }

    public Double getAresOdi() {
        return aresOdi;
    }

    public void setAresOdi(Double aresOdi) {
        this.aresOdi = aresOdi;
    }

    public Double getAresRegisraion() {
        return aresRegisraion;
    }

    public void setAresRegisraion(Double aresRegisraion) {
        this.aresRegisraion = aresRegisraion;
    }

    public Double getAresSeizing() {
        return aresSeizing;
    }

    public void setAresSeizing(Double aresSeizing) {
        this.aresSeizing = aresSeizing;
    }

    public Double getAresRecovery() {
        return aresRecovery;
    }

    public void setAresRecovery(Double aresRecovery) {
        this.aresRecovery = aresRecovery;
    }

    public Double getAresLetter() {
        return aresLetter;
    }

    public void setAresLetter(Double aresLetter) {
        this.aresLetter = aresLetter;
    }

    public Double getAresValuation() {
        return aresValuation;
    }

    public void setAresValuation(Double aresValuation) {
        this.aresValuation = aresValuation;
    }

    public Double getAresBank() {
        return aresBank;
    }

    public void setAresBank(Double aresBank) {
        this.aresBank = aresBank;
    }

    public Double getAresCheq() {
        return aresCheq;
    }

    public void setAresCheq(Double aresCheq) {
        this.aresCheq = aresCheq;
    }

    public int getLoanType() {
        return loanType;
    }

    public void setLoanType(int loanType) {
        this.loanType = loanType;
    }

    public Double getStockbalance() {
        return stockbalance;
    }

    public void setStockbalance(Double stockbalance) {
        this.stockbalance = stockbalance;
    }

    public Double getSuspendInterest() {
        return suspendInterest;
    }

    public void setSuspendInterest(Double suspendInterest) {
        this.suspendInterest = suspendInterest;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.Date;

/**
 *
 * @author ites
 */
public class VoucherDetails {
    
    double amount;
    String transactionNo;
    int transId;
    String cheqNo;
    Date realizeDate;
    int bank;
    String type;
    
    public VoucherDetails() {
    }
    
    public VoucherDetails(double amount, String transactionNo, int transId, String cheqNo, Date realizeDate, int bank, String type) {
        this.amount = amount;
        this.transactionNo = transactionNo;
        this.transId = transId;
        this.cheqNo = cheqNo;
        this.realizeDate = realizeDate;
        this.bank = bank;
        this.type = type;
    }
    

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public String getCheqNo() {
        return cheqNo;
    }

    public void setCheqNo(String cheqNo) {
        this.cheqNo = cheqNo;
    }

    public Date getRealizeDate() {
        return realizeDate;
    }

    public void setRealizeDate(Date realizeDate) {
        this.realizeDate = realizeDate;
    }

    public int getBank() {
        return bank;
    }

    public void setBank(int bank) {
        this.bank = bank;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    
}

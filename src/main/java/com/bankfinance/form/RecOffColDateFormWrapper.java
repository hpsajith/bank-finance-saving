/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author IT
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecOffColDateFormWrapper {
    
    private List<RecOffColDateForm> rocdfs;

    /**
     * @return the rocdfs
     */
    public List<RecOffColDateForm> getRocdfs() {
        return rocdfs;
    }

    /**
     * @param rocdfs the rocdfs to set
     */
    public void setRocdfs(List<RecOffColDateForm> rocdfs) {
        this.rocdfs = rocdfs;
    }
    
}

package com.bankfinance.form;

import java.util.Date;

public class BatchCustomer {

    private int id;
    private String accountNo;
    private String customerIni;
    private String customerName;
    private String customerTitle;
    private String contactPerson;
    private String physicalAddress1;
    private String physicalAddress2;
    private String physicalAddress3;
    private String physicalAddress4;
    private String physicalAddress5;
    private String physicalPC;
    private String addressee;
    private String postalAddress1;
    private String postalAddress2;
    private String postalAddress3;
    private String postalAddress4;
    private String postalAddress5;
    private String postPC;
    private String deliveredTo;
    private String telephone1;
    private String telephone2;
    private String fax1;
    private String fax2;
    private String iClassId;
    private int accountTerms;
    private int ct;
    private String taxNumber;
    private String registration;
    private String iAreasID;
    private double creditLimit;
    private double interestRate;
    private double discount;
    private int onHold;
    private int bfOpenType;
    private String email;
    private String bankLink;
    private String branchCode;
    private String bankAccNum;
    private String bankAccType;
    private int autoDisc;
    private int discMtrxRow;
    private int checkTerms;
    private int useEmial;
    private Date exportDate;
    private String cAccDescription;
    private String cWebPage;
    private String cBankRefNr;
    private int iCurrencyId;
    private int bStatPrint;
    private int bStatEmail;
    private String cStatEmailPass;
    private int bForCurAcc;
    private String iSettlementTermsID;
    private String iEUCountryID;
    private String iDefTaxTypeID;
    private String repId;
    private String mainAccLink;
    private int cashDebtor;
    private String iIncidentTypeID;
    private String iBusTypeID;
    private String iBusClassID;
    private String iAgentID;
    private int bTaxPrompt;
    private String iARPriceListNameId;
    private int bCODAccount;

    public BatchCustomer() {
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the customerIni
     */
    public String getCustomerIni() {
        return customerIni;
    }

    /**
     * @param customerIni the customerIni to set
     */
    public void setCustomerIni(String customerIni) {
        this.customerIni = customerIni;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerTitle
     */
    public String getCustomerTitle() {
        return customerTitle;
    }

    /**
     * @param customerTitle the customerTitle to set
     */
    public void setCustomerTitle(String customerTitle) {
        this.customerTitle = customerTitle;
    }

    /**
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * @param contactPerson the contactPerson to set
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * @return the physicalAddress1
     */
    public String getPhysicalAddress1() {
        return physicalAddress1;
    }

    /**
     * @param physicalAddress1 the physicalAddress1 to set
     */
    public void setPhysicalAddress1(String physicalAddress1) {
        this.physicalAddress1 = physicalAddress1;
    }

    /**
     * @return the physicalAddress2
     */
    public String getPhysicalAddress2() {
        return physicalAddress2;
    }

    /**
     * @param physicalAddress2 the physicalAddress2 to set
     */
    public void setPhysicalAddress2(String physicalAddress2) {
        this.physicalAddress2 = physicalAddress2;
    }

    /**
     * @return the physicalAddress3
     */
    public String getPhysicalAddress3() {
        return physicalAddress3;
    }

    /**
     * @param physicalAddress3 the physicalAddress3 to set
     */
    public void setPhysicalAddress3(String physicalAddress3) {
        this.physicalAddress3 = physicalAddress3;
    }

    /**
     * @return the physicalAddress4
     */
    public String getPhysicalAddress4() {
        return physicalAddress4;
    }

    /**
     * @param physicalAddress4 the physicalAddress4 to set
     */
    public void setPhysicalAddress4(String physicalAddress4) {
        this.physicalAddress4 = physicalAddress4;
    }

    /**
     * @return the physicalAddress5
     */
    public String getPhysicalAddress5() {
        return physicalAddress5;
    }

    /**
     * @param physicalAddress5 the physicalAddress5 to set
     */
    public void setPhysicalAddress5(String physicalAddress5) {
        this.physicalAddress5 = physicalAddress5;
    }

    /**
     * @return the physicalPC
     */
    public String getPhysicalPC() {
        return physicalPC;
    }

    /**
     * @param physicalPC the physicalPC to set
     */
    public void setPhysicalPC(String physicalPC) {
        this.physicalPC = physicalPC;
    }

    /**
     * @return the addressee
     */
    public String getAddressee() {
        return addressee;
    }

    /**
     * @param addressee the addressee to set
     */
    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    /**
     * @return the postalAddress1
     */
    public String getPostalAddress1() {
        return postalAddress1;
    }

    /**
     * @param postalAddress1 the postalAddress1 to set
     */
    public void setPostalAddress1(String postalAddress1) {
        this.postalAddress1 = postalAddress1;
    }

    /**
     * @return the postalAddress2
     */
    public String getPostalAddress2() {
        return postalAddress2;
    }

    /**
     * @param postalAddress2 the postalAddress2 to set
     */
    public void setPostalAddress2(String postalAddress2) {
        this.postalAddress2 = postalAddress2;
    }

    /**
     * @return the postalAddress3
     */
    public String getPostalAddress3() {
        return postalAddress3;
    }

    /**
     * @param postalAddress3 the postalAddress3 to set
     */
    public void setPostalAddress3(String postalAddress3) {
        this.postalAddress3 = postalAddress3;
    }

    /**
     * @return the postalAddress4
     */
    public String getPostalAddress4() {
        return postalAddress4;
    }

    /**
     * @param postalAddress4 the postalAddress4 to set
     */
    public void setPostalAddress4(String postalAddress4) {
        this.postalAddress4 = postalAddress4;
    }

    /**
     * @return the postalAddress5
     */
    public String getPostalAddress5() {
        return postalAddress5;
    }

    /**
     * @param postalAddress5 the postalAddress5 to set
     */
    public void setPostalAddress5(String postalAddress5) {
        this.postalAddress5 = postalAddress5;
    }

    /**
     * @return the postPC
     */
    public String getPostPC() {
        return postPC;
    }

    /**
     * @param postPC the postPC to set
     */
    public void setPostPC(String postPC) {
        this.postPC = postPC;
    }

    /**
     * @return the deliveredTo
     */
    public String getDeliveredTo() {
        return deliveredTo;
    }

    /**
     * @param deliveredTo the deliveredTo to set
     */
    public void setDeliveredTo(String deliveredTo) {
        this.deliveredTo = deliveredTo;
    }

    /**
     * @return the telephone1
     */
    public String getTelephone1() {
        return telephone1;
    }

    /**
     * @param telephone1 the telephone1 to set
     */
    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    /**
     * @return the telephone2
     */
    public String getTelephone2() {
        return telephone2;
    }

    /**
     * @param telephone2 the telephone2 to set
     */
    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    /**
     * @return the fax1
     */
    public String getFax1() {
        return fax1;
    }

    /**
     * @param fax1 the fax1 to set
     */
    public void setFax1(String fax1) {
        this.fax1 = fax1;
    }

    /**
     * @return the fax2
     */
    public String getFax2() {
        return fax2;
    }

    /**
     * @param fax2 the fax2 to set
     */
    public void setFax2(String fax2) {
        this.fax2 = fax2;
    }

    /**
     * @return the iClassId
     */
    public String getiClassId() {
        return iClassId;
    }

    /**
     * @param iClassId the iClassId to set
     */
    public void setiClassId(String iClassId) {
        this.iClassId = iClassId;
    }

    /**
     * @return the accountTerms
     */
    public int getAccountTerms() {
        return accountTerms;
    }

    /**
     * @param accountTerms the accountTerms to set
     */
    public void setAccountTerms(int accountTerms) {
        this.accountTerms = accountTerms;
    }

    /**
     * @return the ct
     */
    public int getCt() {
        return ct;
    }

    /**
     * @param ct the ct to set
     */
    public void setCt(int ct) {
        this.ct = ct;
    }

    /**
     * @return the taxNumber
     */
    public String getTaxNumber() {
        return taxNumber;
    }

    /**
     * @param taxNumber the taxNumber to set
     */
    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    /**
     * @return the registration
     */
    public String getRegistration() {
        return registration;
    }

    /**
     * @param registration the registration to set
     */
    public void setRegistration(String registration) {
        this.registration = registration;
    }

    /**
     * @return the iAreasID
     */
    public String getiAreasID() {
        return iAreasID;
    }

    /**
     * @param iAreasID the iAreasID to set
     */
    public void setiAreasID(String iAreasID) {
        this.iAreasID = iAreasID;
    }

    /**
     * @return the creditLimit
     */
    public double getCreditLimit() {
        return creditLimit;
    }

    /**
     * @param creditLimit the creditLimit to set
     */
    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    /**
     * @return the interestRate
     */
    public double getInterestRate() {
        return interestRate;
    }

    /**
     * @param interestRate the interestRate to set
     */
    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    /**
     * @return the discount
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    /**
     * @return the onHold
     */
    public int getOnHold() {
        return onHold;
    }

    /**
     * @param onHold the onHold to set
     */
    public void setOnHold(int onHold) {
        this.onHold = onHold;
    }

    /**
     * @return the bfOpenType
     */
    public int getBfOpenType() {
        return bfOpenType;
    }

    /**
     * @param bfOpenType the bfOpenType to set
     */
    public void setBfOpenType(int bfOpenType) {
        this.bfOpenType = bfOpenType;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the bankLink
     */
    public String getBankLink() {
        return bankLink;
    }

    /**
     * @param bankLink the bankLink to set
     */
    public void setBankLink(String bankLink) {
        this.bankLink = bankLink;
    }

    /**
     * @return the branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * @param branchCode the branchCode to set
     */
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    /**
     * @return the bankAccNum
     */
    public String getBankAccNum() {
        return bankAccNum;
    }

    /**
     * @param bankAccNum the bankAccNum to set
     */
    public void setBankAccNum(String bankAccNum) {
        this.bankAccNum = bankAccNum;
    }

    /**
     * @return the bankAccType
     */
    public String getBankAccType() {
        return bankAccType;
    }

    /**
     * @param bankAccType the bankAccType to set
     */
    public void setBankAccType(String bankAccType) {
        this.bankAccType = bankAccType;
    }

    /**
     * @return the autoDisc
     */
    public int getAutoDisc() {
        return autoDisc;
    }

    /**
     * @param autoDisc the autoDisc to set
     */
    public void setAutoDisc(int autoDisc) {
        this.autoDisc = autoDisc;
    }

    /**
     * @return the discMtrxRow
     */
    public int getDiscMtrxRow() {
        return discMtrxRow;
    }

    /**
     * @param discMtrxRow the discMtrxRow to set
     */
    public void setDiscMtrxRow(int discMtrxRow) {
        this.discMtrxRow = discMtrxRow;
    }

    /**
     * @return the checkTerms
     */
    public int getCheckTerms() {
        return checkTerms;
    }

    /**
     * @param checkTerms the checkTerms to set
     */
    public void setCheckTerms(int checkTerms) {
        this.checkTerms = checkTerms;
    }

    /**
     * @return the useEmial
     */
    public int getUseEmial() {
        return useEmial;
    }

    /**
     * @param useEmial the useEmial to set
     */
    public void setUseEmial(int useEmial) {
        this.useEmial = useEmial;
    }

    /**
     * @return the exportDate
     */
    public Date getExportDate() {
        return exportDate;
    }

    /**
     * @param exportDate the exportDate to set
     */
    public void setExportDate(Date exportDate) {
        this.exportDate = exportDate;
    }

    /**
     * @return the cAccDescription
     */
    public String getcAccDescription() {
        return cAccDescription;
    }

    /**
     * @param cAccDescription the cAccDescription to set
     */
    public void setcAccDescription(String cAccDescription) {
        this.cAccDescription = cAccDescription;
    }

    /**
     * @return the cWebPage
     */
    public String getcWebPage() {
        return cWebPage;
    }

    /**
     * @param cWebPage the cWebPage to set
     */
    public void setcWebPage(String cWebPage) {
        this.cWebPage = cWebPage;
    }

    /**
     * @return the cBankRefNr
     */
    public String getcBankRefNr() {
        return cBankRefNr;
    }

    /**
     * @param cBankRefNr the cBankRefNr to set
     */
    public void setcBankRefNr(String cBankRefNr) {
        this.cBankRefNr = cBankRefNr;
    }

    /**
     * @return the iCurrencyId
     */
    public int getiCurrencyId() {
        return iCurrencyId;
    }

    /**
     * @param iCurrencyId the iCurrencyId to set
     */
    public void setiCurrencyId(int iCurrencyId) {
        this.iCurrencyId = iCurrencyId;
    }

    /**
     * @return the bStatPrint
     */
    public int getbStatPrint() {
        return bStatPrint;
    }

    /**
     * @param bStatPrint the bStatPrint to set
     */
    public void setbStatPrint(int bStatPrint) {
        this.bStatPrint = bStatPrint;
    }

    /**
     * @return the bStatEmail
     */
    public int getbStatEmail() {
        return bStatEmail;
    }

    /**
     * @param bStatEmail the bStatEmail to set
     */
    public void setbStatEmail(int bStatEmail) {
        this.bStatEmail = bStatEmail;
    }

    /**
     * @return the cStatEmailPass
     */
    public String getcStatEmailPass() {
        return cStatEmailPass;
    }

    /**
     * @param cStatEmailPass the cStatEmailPass to set
     */
    public void setcStatEmailPass(String cStatEmailPass) {
        this.cStatEmailPass = cStatEmailPass;
    }

    /**
     * @return the bForCurAcc
     */
    public int getbForCurAcc() {
        return bForCurAcc;
    }

    /**
     * @param bForCurAcc the bForCurAcc to set
     */
    public void setbForCurAcc(int bForCurAcc) {
        this.bForCurAcc = bForCurAcc;
    }

    /**
     * @return the iSettlementTermsID
     */
    public String getiSettlementTermsID() {
        return iSettlementTermsID;
    }

    /**
     * @param iSettlementTermsID the iSettlementTermsID to set
     */
    public void setiSettlementTermsID(String iSettlementTermsID) {
        this.iSettlementTermsID = iSettlementTermsID;
    }

    /**
     * @return the iEUCountryID
     */
    public String getiEUCountryID() {
        return iEUCountryID;
    }

    /**
     * @param iEUCountryID the iEUCountryID to set
     */
    public void setiEUCountryID(String iEUCountryID) {
        this.iEUCountryID = iEUCountryID;
    }

    /**
     * @return the iDefTaxTypeID
     */
    public String getiDefTaxTypeID() {
        return iDefTaxTypeID;
    }

    /**
     * @param iDefTaxTypeID the iDefTaxTypeID to set
     */
    public void setiDefTaxTypeID(String iDefTaxTypeID) {
        this.iDefTaxTypeID = iDefTaxTypeID;
    }

    /**
     * @return the repId
     */
    public String getRepId() {
        return repId;
    }

    /**
     * @param repId the repId to set
     */
    public void setRepId(String repId) {
        this.repId = repId;
    }

    /**
     * @return the mainAccLink
     */
    public String getMainAccLink() {
        return mainAccLink;
    }

    /**
     * @param mainAccLink the mainAccLink to set
     */
    public void setMainAccLink(String mainAccLink) {
        this.mainAccLink = mainAccLink;
    }

    /**
     * @return the cashDebtor
     */
    public int getCashDebtor() {
        return cashDebtor;
    }

    /**
     * @param cashDebtor the cashDebtor to set
     */
    public void setCashDebtor(int cashDebtor) {
        this.cashDebtor = cashDebtor;
    }

    /**
     * @return the iIncidentTypeID
     */
    public String getiIncidentTypeID() {
        return iIncidentTypeID;
    }

    /**
     * @param iIncidentTypeID the iIncidentTypeID to set
     */
    public void setiIncidentTypeID(String iIncidentTypeID) {
        this.iIncidentTypeID = iIncidentTypeID;
    }

    /**
     * @return the iBusTypeID
     */
    public String getiBusTypeID() {
        return iBusTypeID;
    }

    /**
     * @param iBusTypeID the iBusTypeID to set
     */
    public void setiBusTypeID(String iBusTypeID) {
        this.iBusTypeID = iBusTypeID;
    }

    /**
     * @return the iBusClassID
     */
    public String getiBusClassID() {
        return iBusClassID;
    }

    /**
     * @param iBusClassID the iBusClassID to set
     */
    public void setiBusClassID(String iBusClassID) {
        this.iBusClassID = iBusClassID;
    }

    /**
     * @return the iAgentID
     */
    public String getiAgentID() {
        return iAgentID;
    }

    /**
     * @param iAgentID the iAgentID to set
     */
    public void setiAgentID(String iAgentID) {
        this.iAgentID = iAgentID;
    }

    /**
     * @return the bTaxPrompt
     */
    public int getbTaxPrompt() {
        return bTaxPrompt;
    }

    /**
     * @param bTaxPrompt the bTaxPrompt to set
     */
    public void setbTaxPrompt(int bTaxPrompt) {
        this.bTaxPrompt = bTaxPrompt;
    }

    /**
     * @return the iARPriceListNameId
     */
    public String getiARPriceListNameId() {
        return iARPriceListNameId;
    }

    /**
     * @param iARPriceListNameId the iARPriceListNameId to set
     */
    public void setiARPriceListNameId(String iARPriceListNameId) {
        this.iARPriceListNameId = iARPriceListNameId;
    }

    /**
     * @return the bCODAccount
     */
    public int getbCODAccount() {
        return bCODAccount;
    }

    /**
     * @param bCODAccount the bCODAccount to set
     */
    public void setbCODAccount(int bCODAccount) {
        this.bCODAccount = bCODAccount;
    }

}

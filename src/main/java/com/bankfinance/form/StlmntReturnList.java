/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import java.util.Date;

/**
 *
 * @author ITESS
 */
public class StlmntReturnList {

    private int transactionId;
    private String transactionNo;
    private int debtorId;
    private int paymentType;
    private double paymentAmount;
    private int loanId;
    private int userId;
    private String accountNo;
    private int discountId;
    private int branchId;
    private String receiptNo;
    private int paymentDetails;
    private String remark;
    private int isDownPayment;
    private int isRebatePayment;
    private int chequeId;
    private int directDepositeId;
    private Date actionDate;
    private int isDelete;
    private Date paymentDate;
    private String amountText;
    private Date systemDate;
    private String payType;
    private String agreementNo;
    private String debtAccountNo;
    private String vehicleNo;

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public int getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(int debtorId) {
        this.debtorId = debtorId;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public int getDiscountId() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public int getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(int paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIsDownPayment() {
        return isDownPayment;
    }

    public void setIsDownPayment(int isDownPayment) {
        this.isDownPayment = isDownPayment;
    }

    public int getIsRebatePayment() {
        return isRebatePayment;
    }

    public void setIsRebatePayment(int isRebatePayment) {
        this.isRebatePayment = isRebatePayment;
    }

    public int getChequeId() {
        return chequeId;
    }

    public void setChequeId(int chequeId) {
        this.chequeId = chequeId;
    }

    public int getDirectDepositeId() {
        return directDepositeId;
    }

    public void setDirectDepositeId(int directDepositeId) {
        this.directDepositeId = directDepositeId;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getAmountText() {
        return amountText;
    }

    public void setAmountText(String amountText) {
        this.amountText = amountText;
    }

    public Date getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(Date systemDate) {
        this.systemDate = systemDate;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getDebtAccountNo() {
        return debtAccountNo;
    }

    public void setDebtAccountNo(String debtAccountNo) {
        this.debtAccountNo = debtAccountNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.Date;

/**
 *
 * @author Amidu-pc
 */
public class OtherPaymentForm {
    private String cusName;
    private String cusAddress;
    private String account;
    private int payType;
    private String chequeno;
    private int ChequeBank;
    private Date RealizeDate;
    private int Bank;
    private String bankaccountno;
    private double amount;
    private String amountText;
    private String description;

    public OtherPaymentForm() {
    }

    public OtherPaymentForm(String cusName, String cusAddress, String account, int payType, String chequeno, int ChequeBank, Date RealizeDate, int Bank, String bankaccountno, double amount, String amountText, String description) {     
        this.cusName = cusName;
        this.cusAddress = cusAddress;
        this.account = account;
        this.payType = payType;
        this.chequeno = chequeno;
        this.ChequeBank = ChequeBank;
        this.RealizeDate = RealizeDate;
        this.Bank = Bank;
        this.bankaccountno = bankaccountno;
        this.amount = amount;
        this.amountText = amountText;
        this.description = description;
    }

    
    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusAddress() {
        return cusAddress;
    }

    public void setCusAddress(String cusAddress) {
        this.cusAddress = cusAddress;
    }
    
    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the payType
     */
    public int getPayType() {
        return payType;
    }

    /**
     * @param payType the payType to set
     */
    public void setPayType(int payType) {
        this.payType = payType;
    }

    /**
     * @return the chequeno
     */
    public String getChequeno() {
        return chequeno;
    }

    /**
     * @param chequeno the chequeno to set
     */
    public void setChequeno(String chequeno) {
        this.chequeno = chequeno;
    }

    /**
     * @return the ChequeBank
     */
    public int getChequeBank() {
        return ChequeBank;
    }

    /**
     * @param ChequeBank the ChequeBank to set
     */
    public void setChequeBank(int ChequeBank) {
        this.ChequeBank = ChequeBank;
    }

    /**
     * @return the RealizeDate
     */
    public Date getRealizeDate() {
        return RealizeDate;
    }

    /**
     * @param RealizeDate the RealizeDate to set
     */
    public void setRealizeDate(Date RealizeDate) {
        this.RealizeDate = RealizeDate;
    }

    /**
     * @return the Bank
     */
    public int getBank() {
        return Bank;
    }

    /**
     * @param Bank the Bank to set
     */
    public void setBank(int Bank) {
        this.Bank = Bank;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBankaccountno() {
        return bankaccountno;
    }

    public void setBankaccountno(String bankaccountno) {
        this.bankaccountno = bankaccountno;
    }

    /**
     * @return the amountText
     */
    public String getAmountText() {
        return amountText;
    }

    /**
     * @param amountText the amountText to set
     */
    public void setAmountText(String amountText) {
        this.amountText = amountText;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

/**
 *
 * @author MsD
 */
public class DayEndStatus {

    private int branchId;
    private int status;

    public DayEndStatus(int branchId, int status) {
        this.branchId = branchId;
        this.status = status;
    }

    public DayEndStatus() {

    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}

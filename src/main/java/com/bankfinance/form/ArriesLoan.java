/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author IT
 */
@XmlRootElement(name = "ArriesLoan")
@XmlAccessorType (XmlAccessType.FIELD)
public class ArriesLoan {
    
    private Integer loanId;
    private Integer loanTypeId;
    private String loanType;
    private String agreementNo;
    private String bookNo;
    private Double arries;
    private Integer reInstallment;
    private Double loanInstallment;
    private Double interestPotion;
    private String systemDate;
    private String periodType;
    private Integer period;

    public ArriesLoan(){}
    
    /**
     * @return the loanId
     */
    public Integer getLoanId() {
        return loanId;
    }

    /**
     * @param loanId the loanId to set
     */
    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    /**
     * @return the loanTypeId
     */
    public Integer getLoanTypeId() {
        return loanTypeId;
    }

    /**
     * @param loanTypeId the loanTypeId to set
     */
    public void setLoanTypeId(Integer loanTypeId) {
        this.loanTypeId = loanTypeId;
    }

    /**
     * @return the loanType
     */
    public String getLoanType() {
        return loanType;
    }

    /**
     * @param loanType the loanType to set
     */
    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    /**
     * @return the agreementNo
     */
    public String getAgreementNo() {
        return agreementNo;
    }

    /**
     * @param agreementNo the agreementNo to set
     */
    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    /**
     * @return the bookNo
     */
    public String getBookNo() {
        return bookNo;
    }

    /**
     * @param bookNo the bookNo to set
     */
    public void setBookNo(String bookNo) {
        this.bookNo = bookNo;
    }

    /**
     * @return the arries
     */
    public Double getArries() {
        return arries;
    }

    /**
     * @param arries the arries to set
     */
    public void setArries(Double arries) {
        this.arries = arries;
    }

    /**
     * @return the reInstallment
     */
    public Integer getReInstallment() {
        return reInstallment;
    }

    /**
     * @param reInstallment the reInstallment to set
     */
    public void setReInstallment(Integer reInstallment) {
        this.reInstallment = reInstallment;
    }

    /**
     * @return the loanInstallment
     */
    public Double getLoanInstallment() {
        return loanInstallment;
    }

    /**
     * @param loanInstallment the loanInstallment to set
     */
    public void setLoanInstallment(Double loanInstallment) {
        this.loanInstallment = loanInstallment;
    }

    /**
     * @return the interestPotion
     */
    public Double getInterestPotion() {
        return interestPotion;
    }

    /**
     * @param interestPotion the interestPotion to set
     */
    public void setInterestPotion(Double interestPotion) {
        this.interestPotion = interestPotion;
    }

    /**
     * @return the systemDate
     */
    public String getSystemDate() {
        return systemDate;
    }

    /**
     * @param systemDate the systemDate to set
     */
    public void setSystemDate(String systemDate) {
        this.systemDate = systemDate;
    }

    /**
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }

    /**
     * @param periodType the periodType to set
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    /**
     * @return the period
     */
    public Integer getPeriod() {
        return period;
    }

    /**
     * @param period the period to set
     */
    public void setPeriod(Integer period) {
        this.period = period;
    }
    
}

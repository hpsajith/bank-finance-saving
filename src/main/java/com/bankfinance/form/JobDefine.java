/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import com.ites.bankfinance.model.ASubtab;
import java.util.List;

/**
 *
 * @author Mohan
 */
public class JobDefine {
    private int mainTabId;
    private String mainTabName;
    private String refPage;
    private String mainCode;
    private List<ASubtab> subList; 
    private boolean isActive;

    public JobDefine() {
    }
    
    public JobDefine(int mainTabId, String mainTabName, String refPage, String mainCode, List<ASubtab> subList, boolean isActive) {
        this.mainTabId = mainTabId;
        this.mainTabName = mainTabName;
        this.refPage = refPage;
        this.mainCode = mainCode;
        this.subList = subList;
        this.isActive = isActive;
    }

    public int getMainTabId() {
        return mainTabId;
    }

    public void setMainTabId(int mainTabId) {
        this.mainTabId = mainTabId;
    }

    public String getMainTabName() {
        return mainTabName;
    }

    public void setMainTabName(String mainTabName) {
        this.mainTabName = mainTabName;
    }

    public String getRefPage() {
        return refPage;
    }

    public void setRefPage(String refPage) {
        this.refPage = refPage;
    }

    public String getMainCode() {
        return mainCode;
    }

    public void setMainCode(String mainCode) {
        this.mainCode = mainCode;
    }

    public List<ASubtab> getSubList() {
        return subList;
    }

    public void setSubList(List<ASubtab> subList) {
        this.subList = subList;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
    
    
}

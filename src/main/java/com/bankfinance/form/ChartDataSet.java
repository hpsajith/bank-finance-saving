
package com.bankfinance.form;

import java.util.List;

/**
 *
 * @author IT
 */
public class ChartDataSet {
    
    private String label;
    private String fillColor;
    private String strokeColor;
    private String highlightFill;
    private String highlightStroke;
    private List<Double> data;

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the fillColor
     */
    public String getFillColor() {
        return fillColor;
    }

    /**
     * @param fillColor the fillColor to set
     */
    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * @return the strokeColor
     */
    public String getStrokeColor() {
        return strokeColor;
    }

    /**
     * @param strokeColor the strokeColor to set
     */
    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }

    /**
     * @return the highlightFill
     */
    public String getHighlightFill() {
        return highlightFill;
    }

    /**
     * @param highlightFill the highlightFill to set
     */
    public void setHighlightFill(String highlightFill) {
        this.highlightFill = highlightFill;
    }

    /**
     * @return the highlightStroke
     */
    public String getHighlightStroke() {
        return highlightStroke;
    }

    /**
     * @param highlightStroke the highlightStroke to set
     */
    public void setHighlightStroke(String highlightStroke) {
        this.highlightStroke = highlightStroke;
    }

    /**
     * @return the data
     */
    public List<Double> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<Double> data) {
        this.data = data;
    }
    
}

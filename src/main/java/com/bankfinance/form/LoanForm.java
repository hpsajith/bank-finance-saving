/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import java.math.BigDecimal;
import java.util.Date;

public class LoanForm {

    private Integer loanId;
    private BigDecimal loanAmount;
    private Double loanDownPayment;
    private Double loanInvestment;
    private Integer loanPeriod;
    private Integer periodType;
    private Double loanRate;
    private Double loanInstallment;
    private Double loanInterest;

    private Integer marketingOfficer;
    private Integer salesPerson;
    private Integer recoveryPerson;
    private Integer ced;
    private String comment1;
    private String comment2;
    private Integer debtorId;
    private Integer loanType;
    private String loanRateCode;
    private Integer loanMainType;

    private Integer[] guarantorID;
    private Integer[] chragesId;
    private Double[] chragesRate;
    private Double[] chragesAmount;
    private Integer[] properyId;
    private Integer proprtyType;

    private Integer branchId;

    private Date startDate;
    private Date dueDate;
    private String bookNo;
    private Integer dueDay;

    private Double[] capitalizeAmount;
    private Integer[] capitalizeId;

    private Integer loanRescheduleId;

    private Integer isGroupLoan;
    private Integer groupLoanID;
    private Integer[] groupDetorIds;

    private double[] groupDebtorAmount;
    private double[] groupDebtorPeriod;
    private double[] groupDebtorRate;
    private double[] groupDebtorInterest;
    private double[] groupDebtorTotalInterest;
    private double[] groupDebtorInstallment;
    private double[] groupDebtorTotal;

    public LoanForm() {
    }

    public LoanForm(Integer loanId, BigDecimal loanAmount, Double loanDownPayment, Double loanInvestment, Integer loanPeriod, Integer periodType, Double loanRate, Double loanInstallment, Double loanInterest, Integer marketingOfficer, Integer salesPerson, Integer recoveryPerson, Integer ced, String comment1, String comment2, Integer debtorId, Integer loanType, String loanRateCode, Integer loanMainType, Integer[] guarantorID, Integer[] chragesId, Double[] chragesRate, Double[] chragesAmount, Integer[] properyId, Integer proprtyType, Integer branchId, Date startDate, Date dueDate, String bookNo, Integer dueDay, Double[] capitalizeAmount, Integer[] capitalizeId, Integer loanRescheduleId, Integer isGroupLoan, Integer groupLoanID, Integer[] groupDetorIds, double[] groupDebtorAmount, double[] groupDebtorPeriod, double[] groupDebtorRate, double[] groupDebtorInterest, double[] groupDebtorTotalInterest, double[] groupDebtorInstallment, double[] groupDebtorTotal) {
        this.loanId = loanId;
        this.loanAmount = loanAmount;
        this.loanDownPayment = loanDownPayment;
        this.loanInvestment = loanInvestment;
        this.loanPeriod = loanPeriod;
        this.periodType = periodType;
        this.loanRate = loanRate;
        this.loanInstallment = loanInstallment;
        this.loanInterest = loanInterest;
        this.marketingOfficer = marketingOfficer;
        this.salesPerson = salesPerson;
        this.recoveryPerson = recoveryPerson;
        this.ced = ced;
        this.comment1 = comment1;
        this.comment2 = comment2;
        this.debtorId = debtorId;
        this.loanType = loanType;
        this.loanRateCode = loanRateCode;
        this.loanMainType = loanMainType;
        this.guarantorID = guarantorID;
        this.chragesId = chragesId;
        this.chragesRate = chragesRate;
        this.chragesAmount = chragesAmount;
        this.properyId = properyId;
        this.proprtyType = proprtyType;
        this.branchId = branchId;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.bookNo = bookNo;
        this.dueDay = dueDay;
        this.capitalizeAmount = capitalizeAmount;
        this.capitalizeId = capitalizeId;
        this.loanRescheduleId = loanRescheduleId;
        this.isGroupLoan = isGroupLoan;
        this.groupLoanID = groupLoanID;
        this.groupDetorIds = groupDetorIds;
        this.groupDebtorAmount = groupDebtorAmount;
        this.groupDebtorPeriod = groupDebtorPeriod;
        this.groupDebtorRate = groupDebtorRate;
        this.groupDebtorInterest = groupDebtorInterest;
        this.groupDebtorTotalInterest = groupDebtorTotalInterest;
        this.groupDebtorInstallment = groupDebtorInstallment;
        this.groupDebtorTotal = groupDebtorTotal;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Double getLoanDownPayment() {
        return loanDownPayment;
    }

    public void setLoanDownPayment(Double loanDownPayment) {
        this.loanDownPayment = loanDownPayment;
    }

    public Double getLoanInvestment() {
        return loanInvestment;
    }

    public void setLoanInvestment(Double loanInvestment) {
        this.loanInvestment = loanInvestment;
    }

    public Integer getLoanPeriod() {
        return loanPeriod;
    }

    public void setLoanPeriod(Integer loanPeriod) {
        this.loanPeriod = loanPeriod;
    }

    public Integer getPeriodType() {
        return periodType;
    }

    public void setPeriodType(Integer periodType) {
        this.periodType = periodType;
    }

    public Double getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(Double loanRate) {
        this.loanRate = loanRate;
    }

    public Integer getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(Integer salesPerson) {
        this.salesPerson = salesPerson;
    }

    public Integer getRecoveryPerson() {
        return recoveryPerson;
    }

    public void setRecoveryPerson(Integer recoveryPerson) {
        this.recoveryPerson = recoveryPerson;
    }

    public Integer getCed() {
        return ced;
    }

    public void setCed(Integer ced) {
        this.ced = ced;
    }

    public Integer getDebtorId() {
        return debtorId;
    }

    public String getComment1() {
        return comment1;
    }

    public void setComment1(String comment1) {
        this.comment1 = comment1;
    }

    public String getComment2() {
        return comment2;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public void setDebtorId(Integer debtorId) {
        this.debtorId = debtorId;
    }

    public Integer getLoanType() {
        return loanType;
    }

    public void setLoanType(Integer loanType) {
        this.loanType = loanType;
    }

    public Integer getLoanMainType() {
        return loanMainType;
    }

    public void setLoanMainType(Integer loanMainType) {
        this.loanMainType = loanMainType;
    }

    public Integer[] getGuarantorID() {
        return guarantorID;
    }

    public Integer getMarketingOfficer() {
        return marketingOfficer;
    }

    public void setMarketingOfficer(Integer marketingOfficer) {
        this.marketingOfficer = marketingOfficer;
    }

    public void setGuarantorID(Integer[] guarantorID) {
        this.guarantorID = guarantorID;
    }

    public Integer[] getChragesId() {
        return chragesId;
    }

    public void setChragesId(Integer[] chragesId) {
        this.chragesId = chragesId;
    }

    public Double[] getChragesRate() {
        return chragesRate;
    }

    public void setChragesRate(Double[] chragesRate) {
        this.chragesRate = chragesRate;
    }

    public Double[] getChragesAmount() {
        return chragesAmount;
    }

    public void setChragesAmount(Double[] chragesAmount) {
        this.chragesAmount = chragesAmount;
    }

    public Integer[] getProperyId() {
        return properyId;
    }

    public void setProperyId(Integer[] properyId) {
        this.properyId = properyId;
    }

    public Double getLoanInstallment() {
        return loanInstallment;
    }

    public void setLoanInstallment(Double loanInstallment) {
        this.loanInstallment = loanInstallment;
    }

    public Double getLoanInterest() {
        return loanInterest;
    }

    public void setLoanInterest(Double loanInterest) {
        this.loanInterest = loanInterest;
    }

    public Integer getProprtyType() {
        return proprtyType;
    }

    public void setProprtyType(Integer proprtyType) {
        this.proprtyType = proprtyType;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getBookNo() {
        return bookNo;
    }

    public void setBookNo(String bookNo) {
        this.bookNo = bookNo;
    }

    public Integer getDueDay() {
        return dueDay;
    }

    public void setDueDay(Integer dueDay) {
        this.dueDay = dueDay;
    }

    public String getLoanRateCode() {
        return loanRateCode;
    }

    public void setLoanRateCode(String loanRateCode) {
        this.loanRateCode = loanRateCode;
    }

    public Double[] getCapitalizeAmount() {
        return capitalizeAmount;
    }

    public void setCapitalizeAmount(Double[] capitalizeAmount) {
        this.capitalizeAmount = capitalizeAmount;
    }

    public Integer[] getCapitalizeId() {
        return capitalizeId;
    }

    public void setCapitalizeId(Integer[] capitalizeId) {
        this.capitalizeId = capitalizeId;
    }

    public Integer getLoanRescheduleId() {
        return loanRescheduleId;
    }

    public void setLoanRescheduleId(Integer loanRescheduleId) {
        this.loanRescheduleId = loanRescheduleId;
    }

    /**
     * @return the isGroupLoan
     */
    public Integer getIsGroupLoan() {
        return isGroupLoan;
    }

    /**
     * @param isGroupLoan the isGroupLoan to set
     */
    public void setIsGroupLoan(Integer isGroupLoan) {
        this.isGroupLoan = isGroupLoan;
    }

    /**
     * @return the groupLoanID
     */
    public Integer getGroupLoanID() {
        return groupLoanID;
    }

    /**
     * @param groupLoanID the groupLoanID to set
     */
    public void setGroupLoanID(Integer groupLoanID) {
        this.groupLoanID = groupLoanID;
    }

    public Integer[] getGroupDetorIds() {
        return groupDetorIds;
    }

    public void setGroupDetorIds(Integer[] groupDetorIds) {
        this.groupDetorIds = groupDetorIds;
    }

    public double[] getGroupDebtorAmount() {
        return groupDebtorAmount;
    }

    public void setGroupDebtorAmount(double[] groupDebtorAmount) {
        this.groupDebtorAmount = groupDebtorAmount;
    }

    public double[] getGroupDebtorPeriod() {
        return groupDebtorPeriod;
    }

    public void setGroupDebtorPeriod(double[] groupDebtorPeriod) {
        this.groupDebtorPeriod = groupDebtorPeriod;
    }

    public double[] getGroupDebtorRate() {
        return groupDebtorRate;
    }

    public void setGroupDebtorRate(double[] groupDebtorRate) {
        this.groupDebtorRate = groupDebtorRate;
    }

    public double[] getGroupDebtorInterest() {
        return groupDebtorInterest;
    }

    public void setGroupDebtorInterest(double[] groupDebtorInterest) {
        this.groupDebtorInterest = groupDebtorInterest;
    }

    public double[] getGroupDebtorTotalInterest() {
        return groupDebtorTotalInterest;
    }

    public void setGroupDebtorTotalInterest(double[] groupDebtorTotalInterest) {
        this.groupDebtorTotalInterest = groupDebtorTotalInterest;
    }

    public double[] getGroupDebtorInstallment() {
        return groupDebtorInstallment;
    }

    public void setGroupDebtorInstallment(double[] groupDebtorInstallment) {
        this.groupDebtorInstallment = groupDebtorInstallment;
    }

    public double[] getGroupDebtorTotal() {
        return groupDebtorTotal;
    }

    public void setGroupDebtorTotal(double[] groupDebtorTotal) {
        this.groupDebtorTotal = groupDebtorTotal;
    }

}

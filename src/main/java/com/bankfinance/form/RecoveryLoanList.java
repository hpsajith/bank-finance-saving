/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import com.ites.bankfinance.model.DebtorHeaderDetails;

/**
 *
 * @author ites
 */
public class RecoveryLoanList {

    private int loanId;
    private String debtorName;
    private String agreementNo;
    private String bookNo;
    private double arresAmount;
    private int letteId;
    private String letterName;
    private String dueDate;
    private String color;
    private double odi;
    private String address;
    private String telephone;
    private String comment1;
    private String comment2;
    private int id;
    private int recoverId = 0;
    private String vehicleNo;

    public RecoveryLoanList() {
    }

    public RecoveryLoanList(int loanId, String debtorName, String agreementNo, String bookNo, double arresAmount, int letteId, String letterName, String dueDate, String color, double odi, String address, String telephone, String comment1, String comment2, int id, int recoverId) {
        this.loanId = loanId;
        this.debtorName = debtorName;
        this.agreementNo = agreementNo;
        this.bookNo = bookNo;
        this.arresAmount = arresAmount;
        this.letteId = letteId;
        this.letterName = letterName;
        this.dueDate = dueDate;
        this.color = color;
        this.odi = odi;
        this.address = address;
        this.telephone = telephone;
        this.comment1 = comment1;
        this.comment2 = comment2;
        this.id = id;
        this.recoverId = recoverId;
    }

    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getBookNo() {
        return bookNo;
    }

    public void setBookNo(String bookNo) {
        this.bookNo = bookNo;
    }

    public double getArresAmount() {
        return arresAmount;
    }

    public void setArresAmount(double arresAmount) {
        this.arresAmount = arresAmount;
    }

    public int getLetteId() {
        return letteId;
    }

    public void setLetteId(int letteId) {
        this.letteId = letteId;
    }

    public String getLetterName() {
        return letterName;
    }

    public void setLetterName(String letterName) {
        this.letterName = letterName;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getOdi() {
        return odi;
    }

    public void setOdi(double odi) {
        this.odi = odi;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getComment1() {
        return comment1;
    }

    public void setComment1(String comment1) {
        this.comment1 = comment1;
    }

    public String getComment2() {
        return comment2;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRecoverId() {
        return recoverId;
    }

    public void setRecoverId(int recoverId) {
        this.recoverId = recoverId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

}

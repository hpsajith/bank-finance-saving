

package com.bankfinance.form;

/**
 *
 * @author IT
 */
public class RecOffCollection {
    
    private int recOffId;
    private String recOffName;
    private String contatcNo;
    private double receivable;
    private double assignedAmount;

    public RecOffCollection(){}
    
    /**
     * @return the recOffId
     */
    public int getRecOffId() {
        return recOffId;
    }

    /**
     * @param recOffId the recOffId to set
     */
    public void setRecOffId(int recOffId) {
        this.recOffId = recOffId;
    }

    /**
     * @return the recOffName
     */
    public String getRecOffName() {
        return recOffName;
    }

    /**
     * @param recOffName the recOffName to set
     */
    public void setRecOffName(String recOffName) {
        this.recOffName = recOffName;
    }

    /**
     * @return the contatcNo
     */
    public String getContatcNo() {
        return contatcNo;
    }

    /**
     * @param contatcNo the contatcNo to set
     */
    public void setContatcNo(String contatcNo) {
        this.contatcNo = contatcNo;
    }

    /**
     * @return the receivable
     */
    public double getReceivable() {
        return receivable;
    }

    /**
     * @param receivable the receivable to set
     */
    public void setReceivable(double receivable) {
        this.receivable = receivable;
    }

    /**
     * @return the assignedAmount
     */
    public double getAssignedAmount() {
        return assignedAmount;
    }

    /**
     * @param assignedAmount the assignedAmount to set
     */
    public void setAssignedAmount(double assignedAmount) {
        this.assignedAmount = assignedAmount;
    }
    
}

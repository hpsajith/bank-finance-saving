/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.util.Date;

/**
 *
 * @author ites
 */
public class PaymentVoucher {
    String voucherNo;
    double totalAmount;
    double payingAmount;
    int loanId;
    int paymentType;
    String chekNo;
    Date issueDate;
    Date expireDate;
    Integer bankAccount;
    String amountInTxt;
    int branchId;

    public PaymentVoucher() {
    }
    
    public PaymentVoucher(String voucherNo, double totalAmount, double payingAmount, int loanId, int paymentType, String chekNo, Date issueDate, Date expireDate, Integer bankAccount, String amountInTxt, int branchId) {
        this.voucherNo = voucherNo;
        this.totalAmount = totalAmount;
        this.payingAmount = payingAmount;
        this.loanId = loanId;
        this.paymentType = paymentType;
        this.chekNo = chekNo;
        this.issueDate = issueDate;
        this.expireDate = expireDate;
        this.bankAccount = bankAccount;
        this.amountInTxt = amountInTxt;
        this.branchId = branchId;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getPayingAmount() {
        return payingAmount;
    }

    public void setPayingAmount(double payingAmount) {
        this.payingAmount = payingAmount;
    }

    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public String getChekNo() {
        return chekNo;
    }

    public void setChekNo(String chekNo) {
        this.chekNo = chekNo;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(Integer bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getAmountInTxt() {
        return amountInTxt;
    }

    public void setAmountInTxt(String amountInTxt) {
        this.amountInTxt = amountInTxt;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

/**
 *
 * @author admin
 */
public class Mail {

    private int letterId;
    private int loanId;
    private String agreementNo;
    private String clientName;
    private String clientAddress;
    private String guarantor1;
    private String guarantor1Address;
    private String guarantor2;
    private String guarantor2Address;
    private String guarantor3;
    private String guarantor3Address;

    public Mail() {
    }

    public int getLetterId() {
        return letterId;
    }

    public void setLetterId(int letterId) {
        this.letterId = letterId;
    }

    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getGuarantor1() {
        return guarantor1;
    }

    public void setGuarantor1(String guarantor1) {
        this.guarantor1 = guarantor1;
    }

    public String getGuarantor1Address() {
        return guarantor1Address;
    }

    public void setGuarantor1Address(String guarantor1Address) {
        this.guarantor1Address = guarantor1Address;
    }

    public String getGuarantor2() {
        return guarantor2;
    }

    public void setGuarantor2(String guarantor2) {
        this.guarantor2 = guarantor2;
    }

    public String getGuarantor2Address() {
        return guarantor2Address;
    }

    public void setGuarantor2Address(String guarantor2Address) {
        this.guarantor2Address = guarantor2Address;
    }

    public String getGuarantor3() {
        return guarantor3;
    }

    public void setGuarantor3(String guarantor3) {
        this.guarantor3 = guarantor3;
    }

    public String getGuarantor3Address() {
        return guarantor3Address;
    }

    public void setGuarantor3Address(String guarantor3Address) {
        this.guarantor3Address = guarantor3Address;
    }

}

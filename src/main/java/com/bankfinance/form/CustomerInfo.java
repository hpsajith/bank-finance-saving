/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import java.util.Date;

/**
 *
 * @author User
 */
public class CustomerInfo {

    private String clientAddress;
    private String agreementNo;
    private String clientName;
    private String vehicleNo;
    private Double monthlyRental;
    private Integer dueDate;
    private Date rentalDate1st;
    private String gurantor1;
    private String gurantor1Address;
    private String gurantor2;
    private String gurantor2Address;
    private String gurantor3;
    private String gurantor3Address;
    private String nic;
    private String make;
    private String model;

    public CustomerInfo() {
    }

    public CustomerInfo(String clientAddress, String contactNo, String clientName, String vehicleNo, Double monthlyRental, Integer dueDate, Date rentalDate1st, String gurantor1, String gurantor1Address, String gurantor2, String gurantor2Address, String gurantor3, String gurantor3Address, String nic, String make, String model) {
        this.clientAddress = clientAddress;
        this.agreementNo = contactNo;
        this.clientName = clientName;
        this.vehicleNo = vehicleNo;
        this.monthlyRental = monthlyRental;
        this.dueDate = dueDate;
        this.rentalDate1st = rentalDate1st;
        this.gurantor1 = gurantor1;
        this.gurantor1Address = gurantor1Address;
        this.gurantor2 = gurantor2;
        this.gurantor2Address = gurantor2Address;
        this.gurantor3 = gurantor3;
        this.gurantor3Address = gurantor3Address;
        this.nic = nic;
        this.make = make;
        this.model = model;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public Double getMonthlyRental() {
        return monthlyRental;
    }

    public void setMonthlyRental(Double monthlyRental) {
        this.monthlyRental = monthlyRental;
    }

    public Integer getDueDate() {
        return dueDate;
    }

    public void setDueDate(Integer dueDate) {
        this.dueDate = dueDate;
    }

    public Date getRentalDate1st() {
        return rentalDate1st;
    }

    public void setRentalDate1st(Date rentalDate1st) {
        this.rentalDate1st = rentalDate1st;
    }

    public String getGurantor1() {
        return gurantor1;
    }

    public void setGurantor1(String gurantor1) {
        this.gurantor1 = gurantor1;
    }

    public String getGurantor1Address() {
        return gurantor1Address;
    }

    public void setGurantor1Address(String gurantor1Address) {
        this.gurantor1Address = gurantor1Address;
    }

    public String getGurantor2() {
        return gurantor2;
    }

    public void setGurantor2(String gurantor2) {
        this.gurantor2 = gurantor2;
    }

    public String getGurantor2Address() {
        return gurantor2Address;
    }

    public void setGurantor2Address(String gurantor2Address) {
        this.gurantor2Address = gurantor2Address;
    }

    public String getGurantor3Address() {
        return gurantor3Address;
    }

    public void setGurantor3Address(String gurantor3Address) {
        this.gurantor3Address = gurantor3Address;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getGurantor3() {
        return gurantor3;
    }

    public void setGurantor3(String gurantor3) {
        this.gurantor3 = gurantor3;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

/**
 *
 * @author IT
 */
public class MonthlyScheduleAccess {
    
    private int userId;
    private int userTypeId;
    private int branchId;
    private boolean nextMonthAcc;
    private boolean nextYearAcc;
    private boolean prvMonthAcc;
    private boolean prvYearAcc;

    public MonthlyScheduleAccess(){
        
    }
    
    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the userTypeId
     */
    public int getUserTypeId() {
        return userTypeId;
    }

    /**
     * @param userTypeId the userTypeId to set
     */
    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    /**
     * @return the branchId
     */
    public int getBranchId() {
        return branchId;
    }

    /**
     * @param branchId the branchId to set
     */
    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    /**
     * @return the nextMonthAcc
     */
    public boolean isNextMonthAcc() {
        return nextMonthAcc;
    }

    /**
     * @param nextMonthAcc the nextMonthAcc to set
     */
    public void setNextMonthAcc(boolean nextMonthAcc) {
        this.nextMonthAcc = nextMonthAcc;
    }

    /**
     * @return the nextYearAcc
     */
    public boolean isNextYearAcc() {
        return nextYearAcc;
    }

    /**
     * @param nextYearAcc the nextYearAcc to set
     */
    public void setNextYearAcc(boolean nextYearAcc) {
        this.nextYearAcc = nextYearAcc;
    }

    /**
     * @return the prvMonthAcc
     */
    public boolean isPrvMonthAcc() {
        return prvMonthAcc;
    }

    /**
     * @param prvMonthAcc the prvMonthAcc to set
     */
    public void setPrvMonthAcc(boolean prvMonthAcc) {
        this.prvMonthAcc = prvMonthAcc;
    }

    /**
     * @return the prvYearAcc
     */
    public boolean isPrvYearAcc() {
        return prvYearAcc;
    }

    /**
     * @param prvYearAcc the prvYearAcc to set
     */
    public void setPrvYearAcc(boolean prvYearAcc) {
        this.prvYearAcc = prvYearAcc;
    }
    
    
}

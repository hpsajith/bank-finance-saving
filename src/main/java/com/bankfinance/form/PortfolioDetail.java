/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MsD
 */
@XmlRootElement(name = "PortfolioDetail")
@XmlAccessorType(XmlAccessType.FIELD)
public class PortfolioDetail {

    @XmlElement(name = "companyName")
    private String companyName;

    @XmlElement(name = "branch")
    private String branch;

    @XmlElement(name = "printedUserName")
    private String printedUserName;

    @XmlElement(name = "printedDate")
    private String printedDate;

    @XmlElement(name = "followUpOffice")
    private String followUpOffice;

    @XmlElement(name = "product")
    private String product;

    @XmlElement(name = "dueDate")
    private String dueDate;

    @XmlElement(name = "initiateDateFrom")
    private String initiateDateFrom;

    @XmlElement(name = "loanAmount")
    private String loanAmount;

    @XmlElement(name = "grossRetal")
    private String grossRetal;

    @XmlElement(name = "capitalOutstanding")
    private String capitalOutstanding;

    @XmlElement(name = "rentalInArrears")
    private String rentalInArrears;

    @XmlElement(name = "PortfolioDetails")
    private List<PortfolioDetails> details = null;

    public PortfolioDetail() {
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getPrintedUserName() {
        return printedUserName;
    }

    public void setPrintedUserName(String printedUserName) {
        this.printedUserName = printedUserName;
    }

    public String getPrintedDate() {
        return printedDate;
    }

    public void setPrintedDate(String printedDate) {
        this.printedDate = printedDate;
    }

    public String getFollowUpOffice() {
        return followUpOffice;
    }

    public void setFollowUpOffice(String followUpOffice) {
        this.followUpOffice = followUpOffice;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getInitiateDateFrom() {
        return initiateDateFrom;
    }

    public void setInitiateDateFrom(String initiateDateFrom) {
        this.initiateDateFrom = initiateDateFrom;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getGrossRetal() {
        return grossRetal;
    }

    public void setGrossRetal(String grossRetal) {
        this.grossRetal = grossRetal;
    }

    public String getCapitalOutstanding() {
        return capitalOutstanding;
    }

    public void setCapitalOutstanding(String capitalOutstanding) {
        this.capitalOutstanding = capitalOutstanding;
    }

    public String getRentalInArrears() {
        return rentalInArrears;
    }

    public void setRentalInArrears(String rentalInArrears) {
        this.rentalInArrears = rentalInArrears;
    }

    public List<PortfolioDetails> getDetails() {
        return details;
    }

    public void setDetails(List<PortfolioDetails> details) {
        this.details = details;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}

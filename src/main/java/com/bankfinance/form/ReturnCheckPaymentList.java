/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ITESS
 */
public class ReturnCheckPaymentList implements Serializable{
     private Integer chequeId;
     private String transactionNo;
     private String chequeNo;
     private int bank;
     private Date transactionDate;
     private Date realizeDate;
     private Double amount;
     private String amountInTxt;
     private Integer userId;
     private Date actionDate;
     private boolean isReturn;
     private boolean isRealized;
     private Integer customerCode;
     
     private String agreementNo;
     private String debtorAccountNo;
     private String receiptNo;
     private String bankName;

    public Integer getChequeId() {
        return chequeId;
    }

    public void setChequeId(Integer chequeId) {
        this.chequeId = chequeId;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public int getBank() {
        return bank;
    }

    public void setBank(int bank) {
        this.bank = bank;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getRealizeDate() {
        return realizeDate;
    }

    public void setRealizeDate(Date realizeDate) {
        this.realizeDate = realizeDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getAmountInTxt() {
        return amountInTxt;
    }

    public void setAmountInTxt(String amountInTxt) {
        this.amountInTxt = amountInTxt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public boolean getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(boolean isReturn) {
        this.isReturn = isReturn;
    }

    public Integer getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(Integer customerCode) {
        this.customerCode = customerCode;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getDebtorAccountNo() {
        return debtorAccountNo;
    }

    public void setDebtorAccountNo(String debtorAccountNo) {
        this.debtorAccountNo = debtorAccountNo;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public boolean isIsRealized() {
        return isRealized;
    }

    public void setIsRealized(boolean isRealized) {
        this.isRealized = isRealized;
    }
     
     
}

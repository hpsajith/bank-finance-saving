package com.bankfinance.form;

public class RmvReceivable {

    private String facilityNo;
    private String facilityStatus;
    private String closeType;
    private String dueDate;
    private double vat;
    private double dueAmount;
    private double setttledAmmount;
    private double amountToSettele;
    private String client;

    /**
     * @return the facilityNo
     */
    public String getFacilityNo() {
        return facilityNo;
    }

    /**
     * @param facilityNo the facilityNo to set
     */
    public void setFacilityNo(String facilityNo) {
        this.facilityNo = facilityNo;
    }

    /**
     * @return the facilityStatus
     */
    public String getFacilityStatus() {
        return facilityStatus;
    }

    /**
     * @param facilityStatus the facilityStatus to set
     */
    public void setFacilityStatus(String facilityStatus) {
        this.facilityStatus = facilityStatus;
    }

    /**
     * @return the closeType
     */
    public String getCloseType() {
        return closeType;
    }

    /**
     * @param closeType the closeType to set
     */
    public void setCloseType(String closeType) {
        this.closeType = closeType;
    }

    /**
     * @return the dueDate
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the vat
     */
    public double getVat() {
        return vat;
    }

    /**
     * @param vat the vat to set
     */
    public void setVat(double vat) {
        this.vat = vat;
    }

    /**
     * @return the dueAmount
     */
    public double getDueAmount() {
        return dueAmount;
    }

    /**
     * @param dueAmount the dueAmount to set
     */
    public void setDueAmount(double dueAmount) {
        this.dueAmount = dueAmount;
    }

    /**
     * @return the setttledAmmount
     */
    public double getSetttledAmmount() {
        return setttledAmmount;
    }

    /**
     * @param setttledAmmount the setttledAmmount to set
     */
    public void setSetttledAmmount(double setttledAmmount) {
        this.setttledAmmount = setttledAmmount;
    }

 

    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return the amountToSettele
     */
    public double getAmountToSettele() {
        return amountToSettele;
    }

    /**
     * @param amountToSettele the amountToSettele to set
     */
    public void setAmountToSettele(double amountToSettele) {
        this.amountToSettele = amountToSettele;
    }

}

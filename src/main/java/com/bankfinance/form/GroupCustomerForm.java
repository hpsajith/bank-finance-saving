package com.bankfinance.form;

public class GroupCustomerForm {

    private Integer[] guarantorID;
    private Integer centeID;
    private Integer branchID;
    private Integer groupID;
  

    public GroupCustomerForm() {
    }

    public GroupCustomerForm(Integer[] guarantorID, Integer centeID, Integer branchID, Integer groupID) {
        this.guarantorID = guarantorID;
        this.centeID = centeID;
        this.branchID = branchID;
        this.groupID = groupID;
    }

    /**
     * @return the guarantorID
     */
    public Integer[] getGuarantorID() {
        return guarantorID;
    }

    /**
     * @param guarantorID the guarantorID to set
     */
    public void setGuarantorID(Integer[] guarantorID) {
        this.guarantorID = guarantorID;
    }

    /**
     * @return the centeID
     */
    public Integer getCenteID() {
        return centeID;
    }

    /**
     * @param centeID the centeID to set
     */
    public void setCenteID(Integer centeID) {
        this.centeID = centeID;
    }

    /**
     * @return the branchID
     */
    public Integer getBranchID() {
        return branchID;
    }

    /**
     * @param branchID the branchID to set
     */
    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    /**
     * @return the groupID
     */
    public Integer getGroupID() {
        return groupID;
    }

    /**
     * @param groupID the groupID to set
     */
    public void setGroupID(Integer groupID) {
        this.groupID = groupID;
    }

}

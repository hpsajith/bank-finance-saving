/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

/**
 *
 * @author ITESS
 */
public class ActiveContracts {

    private String agreementNo;
    private String name;
    private double amount = 0.00;
    private double interest = 0.00;
    private double totalInterest = 0.00;
    private double downPayment = 0.00;
    private double grossAmount = 0.00;
    private int period;
    private String sector;
    private String subsector;
    private String marketing;
    private String model;
    private String activeDate;
    private double forcedSaleValue = 0.00;
    private double marketValue = 0.00;
    private String make;
    private String year;

    /**
     * @return the agreementNo
     */
    public String getAgreementNo() {
        return agreementNo;
    }

    /**
     * @param agreementNo the agreementNo to set
     */
    public void setAgreementNo(String agreementNo) {
      
        this.agreementNo = agreementNo;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the interest
     */
    public double getInterest() {
        return interest;
    }

    /**
     * @param interest the interest to set
     */
    public void setInterest(double interest) {
        this.interest = interest;
    }

    /**
     * @return the totalInterest
     */
    public double getTotalInterest() {
        return totalInterest;
    }

    /**
     * @param totalInterest the totalInterest to set
     */
    public void setTotalInterest(double totalInterest) {
        this.totalInterest = totalInterest;
    }

    /**
     * @return the downPayment
     */
    public double getDownPayment() {
        return downPayment;
    }

    /**
     * @param downPayment the downPayment to set
     */
    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }

    /**
     * @return the grossAmount
     */
    public double getGrossAmount() {
        return grossAmount;
    }

    /**
     * @param grossAmount the grossAmount to set
     */
    public void setGrossAmount(double grossAmount) {
        this.grossAmount = grossAmount;
    }

    /**
     * @return the period
     */
    public int getPeriod() {
        return period;
    }

    /**
     * @param period the period to set
     */
    public void setPeriod(int period) {
        this.period = period;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    
    /**
     * @return the marketing
     */
    public String getMarketing() {
        return marketing;
    }

    /**
     * @param marketing the marketing to set
     */
    public void setMarketing(String marketing) {
        this.marketing = marketing;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the activeDate
     */
    public String getActiveDate() {
        return activeDate;
    }

    /**
     * @param activeDate the activeDate to set
     */
    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    /**
     * @return the forcedSaleValue
     */
    public double getForcedSaleValue() {
        return forcedSaleValue;
    }

    /**
     * @param forcedSaleValue the forcedSaleValue to set
     */
    public void setForcedSaleValue(double forcedSaleValue) {
        this.forcedSaleValue = forcedSaleValue;
    }

    /**
     * @return the marketValue
     */
    public double getMarketValue() {
        return marketValue;
    }

    /**
     * @param marketValue the marketValue to set
     */
    public void setMarketValue(double marketValue) {
        this.marketValue = marketValue;
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return getAgreementNo() ;
    }

    /**
     * @return the subsector
     */
    public String getSubsector() {
        return subsector;
    }

    /**
     * @param subsector the subsector to set
     */
    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

    
    
}

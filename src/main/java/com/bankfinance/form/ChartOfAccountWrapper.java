/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import com.ites.bankfinance.model.Chartofaccount;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author IT
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChartOfAccountWrapper {
    
    private List<Chartofaccount> accounts;

    /**
     * @return the accounts
     */
    public List<Chartofaccount> getAccounts() {
        return accounts;
    }

    /**
     * @param accounts the accounts to set
     */
    public void setAccounts(List<Chartofaccount> accounts) {
        this.accounts = accounts;
    }
    
}

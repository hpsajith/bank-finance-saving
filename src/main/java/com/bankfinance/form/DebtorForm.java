/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import com.ites.bankfinance.model.DebtorAssessBankDetails;
import com.ites.bankfinance.model.DebtorAssessRealestateDetails;
import com.ites.bankfinance.model.DebtorAssessVehicleDetails;
import com.ites.bankfinance.model.DebtorBusinessDetails;
import com.ites.bankfinance.model.DebtorDependentDetails;
import com.ites.bankfinance.model.DebtorEmploymentDetails;
import com.ites.bankfinance.model.DebtorLiabilityDetails;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DebtorForm {

    private String loanFullName;
    private String nameWithInitials;
    private String loanNic;
    private Date loanDob;
    private String loanAddress;
    private String loanPostAddress;
    private String loanTelephoneHome;
    private String loanTelephoneOffice;
    private String loanTelephoneMobile;
    private String loanSecretaryOffice;
    private String loanFax;
    private String loanEmail;
    private String loanPostOffice;
    private String loanPolisStation;
    private Integer loanIsDebtor;
    private String loanLoacationMap;
    private String loanImage;
    private Integer loanDebtorId;
    private String loanAccountNo;
    private int loanBranch;
    private String debtorAccountNo;

    private Integer dependentId;
    private String dependentName;
    private String dependentNic;
    private String dependentJob;
    private String dependentOccupation;
    private BigDecimal dependentNetSalary;
    private String dependentTelephone;
    private String dependentOfficeName;
    private String dependentOfficeAddress;
    private String dependentOfficeTelephone;
    private String memberNumber;
    private String jobInformation;
    private String centerDay;
    private String bankBranch;
    private String accountName;
    private Integer accountNo;

    private List<DebtorBusinessDetails> business;
    private List<DebtorEmploymentDetails> employement;
    private List<DebtorDependentDetails> Dependent;
    private List<DebtorLiabilityDetails> liability;
    private List<DebtorAssessBankDetails> bank;
    private List<DebtorAssessVehicleDetails> vehicle;
    private List<DebtorAssessRealestateDetails> land;

    private Integer userId;
    private Integer branchId;
    private Boolean isGenerate = false;

    public DebtorForm() {
    }

    public DebtorForm(String loanFullName, String nameWithInitials, String loanNic, Date loanDob, String loanAddress, String loanPostAddress, String loanTelephoneHome, String loanTelephoneOffice, String loanTelephoneMobile, String loanSecretaryOffice, String loanFax, String loanEmail, String loanPostOffice, String loanPolisStation, Integer loanIsDebtor, String loanLoacationMap, String loanImage, Integer loanDebtorId, Integer dependentId, String dependentName, String dependentNic, String dependentJob, String dependentOccupation, BigDecimal dependentNetSalary, String dependentTelephone, String dependentOfficeName, String dependentOfficeAddress, String dependentOfficeTelephone, List<DebtorBusinessDetails> business, List<DebtorEmploymentDetails> employement, List<DebtorDependentDetails> Dependent, List<DebtorLiabilityDetails> liability, List<DebtorAssessBankDetails> bank, List<DebtorAssessVehicleDetails> vehicle, List<DebtorAssessRealestateDetails> land, Integer userId, Integer branchId, String bankBranch, String accountName, Integer accountNo) {
        this.loanFullName = loanFullName;
        this.nameWithInitials = nameWithInitials;
        this.loanNic = loanNic;
        this.loanDob = loanDob;
        this.loanAddress = loanAddress;
        this.loanPostAddress = loanPostAddress;
        this.loanTelephoneHome = loanTelephoneHome;
        this.loanTelephoneOffice = loanTelephoneOffice;
        this.loanTelephoneMobile = loanTelephoneMobile;
        this.loanSecretaryOffice = loanSecretaryOffice;
        this.loanFax = loanFax;
        this.loanEmail = loanEmail;
        this.loanPostOffice = loanPostOffice;
        this.loanPolisStation = loanPolisStation;
        this.loanIsDebtor = loanIsDebtor;
        this.loanLoacationMap = loanLoacationMap;
        this.loanImage = loanImage;
        this.loanDebtorId = loanDebtorId;
        this.dependentId = dependentId;
        this.dependentName = dependentName;
        this.dependentNic = dependentNic;
        this.dependentJob = dependentJob;
        this.dependentOccupation = dependentOccupation;
        this.dependentNetSalary = dependentNetSalary;
        this.dependentTelephone = dependentTelephone;
        this.dependentOfficeName = dependentOfficeName;
        this.dependentOfficeAddress = dependentOfficeAddress;
        this.dependentOfficeTelephone = dependentOfficeTelephone;
        this.business = business;
        this.employement = employement;
        this.Dependent = Dependent;
        this.liability = liability;
        this.bank = bank;
        this.vehicle = vehicle;
        this.land = land;
        this.userId = userId;
        this.branchId = branchId;
        this.bankBranch = bankBranch;
        this.accountName = accountName;
        this.accountNo = accountNo;

    }

    public String getLoanFullName() {
        return loanFullName;
    }

    public void setLoanFullName(String loanFullName) {
        this.loanFullName = loanFullName;
    }

    public String getNameWithInitials() {
        return nameWithInitials;
    }

    public void setNameWithInitials(String nameWithInitials) {
        this.nameWithInitials = nameWithInitials;
    }

    public String getLoanNic() {
        return loanNic;
    }

    public void setLoanNic(String loanNic) {
        this.loanNic = loanNic;
    }

    public Date getLoanDob() {
        return loanDob;
    }

    public void setLoanDob(Date loanDob) {
        this.loanDob = loanDob;
    }

    public String getLoanAddress() {
        return loanAddress;
    }

    public void setLoanAddress(String loanAddress) {
        this.loanAddress = loanAddress;
    }

    public String getLoanPostAddress() {
        return loanPostAddress;
    }

    public void setLoanPostAddress(String loanPostAddress) {
        this.loanPostAddress = loanPostAddress;
    }

    public String getLoanTelephoneHome() {
        return loanTelephoneHome;
    }

    public void setLoanTelephoneHome(String loanTelephoneHome) {
        this.loanTelephoneHome = loanTelephoneHome;
    }

    public String getLoanTelephoneOffice() {
        return loanTelephoneOffice;
    }

    public void setLoanTelephoneOffice(String loanTelephoneOffice) {
        this.loanTelephoneOffice = loanTelephoneOffice;
    }

    public String getLoanTelephoneMobile() {
        return loanTelephoneMobile;
    }

    public void setLoanTelephoneMobile(String loanTelephoneMobile) {
        this.loanTelephoneMobile = loanTelephoneMobile;
    }

    public String getLoanSecretaryOffice() {
        return loanSecretaryOffice;
    }

    public void setLoanSecretaryOffice(String loanSecretaryOffice) {
        this.loanSecretaryOffice = loanSecretaryOffice;
    }

    public String getLoanFax() {
        return loanFax;
    }

    public void setLoanFax(String loanFax) {
        this.loanFax = loanFax;
    }

    public String getLoanEmail() {
        return loanEmail;
    }

    public void setLoanEmail(String loanEmail) {
        this.loanEmail = loanEmail;
    }

    public String getLoanPostOffice() {
        return loanPostOffice;
    }

    public void setLoanPostOffice(String loanPostOffice) {
        this.loanPostOffice = loanPostOffice;
    }

    public String getLoanPolisStation() {
        return loanPolisStation;
    }

    public void setLoanPolisStation(String loanPolisStation) {
        this.loanPolisStation = loanPolisStation;
    }

    public Integer getLoanIsDebtor() {
        return loanIsDebtor;
    }

    public void setLoanIsDebtor(Integer loanIsDebtor) {
        this.loanIsDebtor = loanIsDebtor;
    }

    public String getLoanLoacationMap() {
        return loanLoacationMap;
    }

    public void setLoanLoacationMap(String loanLoacationMap) {
        this.loanLoacationMap = loanLoacationMap;
    }

    public String getLoanImage() {
        return loanImage;
    }

    public void setLoanImage(String loanImage) {
        this.loanImage = loanImage;
    }

    public Integer getLoanDebtorId() {
        return loanDebtorId;
    }

    public void setLoanDebtorId(Integer loanDebtorId) {
        this.loanDebtorId = loanDebtorId;
    }

    public Integer getDependentId() {
        return dependentId;
    }

    public void setDependentId(Integer dependentId) {
        this.dependentId = dependentId;
    }

    public String getDependentName() {
        return dependentName;
    }

    public void setDependentName(String dependentName) {
        this.dependentName = dependentName;
    }

    public String getDependentNic() {
        return dependentNic;
    }

    public void setDependentNic(String dependentNic) {
        this.dependentNic = dependentNic;
    }

    public String getDependentJob() {
        return dependentJob;
    }

    public void setDependentJob(String dependentJob) {
        this.dependentJob = dependentJob;
    }

    public String getDependentOccupation() {
        return dependentOccupation;
    }

    public void setDependentOccupation(String dependentOccupation) {
        this.dependentOccupation = dependentOccupation;
    }

    public BigDecimal getDependentNetSalary() {
        return dependentNetSalary;
    }

    public void setDependentNetSalary(BigDecimal dependentNetSalary) {
        this.dependentNetSalary = dependentNetSalary;
    }

    public String getDependentTelephone() {
        return dependentTelephone;
    }

    public void setDependentTelephone(String dependentTelephone) {
        this.dependentTelephone = dependentTelephone;
    }

    public String getDependentOfficeName() {
        return dependentOfficeName;
    }

    public void setDependentOfficeName(String dependentOfficeName) {
        this.dependentOfficeName = dependentOfficeName;
    }

    public String getDependentOfficeAddress() {
        return dependentOfficeAddress;
    }

    public void setDependentOfficeAddress(String dependentOfficeAddress) {
        this.dependentOfficeAddress = dependentOfficeAddress;
    }

    public String getDependentOfficeTelephone() {
        return dependentOfficeTelephone;
    }

    public void setDependentOfficeTelephone(String dependentOfficeTelephone) {
        this.dependentOfficeTelephone = dependentOfficeTelephone;
    }

    public List<DebtorBusinessDetails> getBusiness() {
        return business;
    }

    public void setBusiness(List<DebtorBusinessDetails> business) {
        this.business = business;
    }

    public List<DebtorEmploymentDetails> getEmployement() {
        return employement;
    }

    public void setEmployement(List<DebtorEmploymentDetails> employement) {
        this.employement = employement;
    }

    public List<DebtorDependentDetails> getDependent() {
        return Dependent;
    }

    public void setDependent(List<DebtorDependentDetails> Dependent) {
        this.Dependent = Dependent;
    }

    public List<DebtorLiabilityDetails> getLiability() {
        return liability;
    }

    public void setLiability(List<DebtorLiabilityDetails> liability) {
        this.liability = liability;
    }

    public List<DebtorAssessBankDetails> getBank() {
        return bank;
    }

    public void setBank(List<DebtorAssessBankDetails> bank) {
        this.bank = bank;
    }

    public List<DebtorAssessVehicleDetails> getVehicle() {
        return vehicle;
    }

    public void setVehicle(List<DebtorAssessVehicleDetails> vehicle) {
        this.vehicle = vehicle;
    }

    public List<DebtorAssessRealestateDetails> getLand() {
        return land;
    }

    public void setLand(List<DebtorAssessRealestateDetails> land) {
        this.land = land;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getLoanAccountNo() {
        return loanAccountNo;
    }

    public void setLoanAccountNo(String loanAccountNo) {
        this.loanAccountNo = loanAccountNo;
    }

    public int getLoanBranch() {
        return loanBranch;
    }

    public void setLoanBranch(int loanBranch) {
        this.loanBranch = loanBranch;
    }

    public String getDebtorAccountNo() {
        return debtorAccountNo;
    }

    public void setDebtorAccountNo(String debtorAccountNo) {
        this.debtorAccountNo = debtorAccountNo;
    }

    public Boolean getIsGenerate() {
        return isGenerate;
    }

    public void setIsGenerate(Boolean isGenerate) {
        this.isGenerate = isGenerate;
    }

    public String getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber) {
        this.memberNumber = memberNumber;
    }

    public String getJobInformation() {
        return jobInformation;
    }

    public void setJobInformation(String jobInformation) {
        this.jobInformation = jobInformation;
    }

    public String getCenterDay() {
        return centerDay;
    }

    public void setCenterDay(String centerDay) {
        this.centerDay = centerDay;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Integer accountNo) {
        this.accountNo = accountNo;
    }
}

package com.bankfinance.form;

public class SavingNewAccForm {

    int savingAccId;
    int branchCodeId;
    String systemDateId;
    int debtorId;
    int savingTypeList;
    int savingSubTypesList;
    double subInterestRate;
    double savingAmount;
    String bookNo;

    public int getSavingAccId() {
        return savingAccId;
    }

    public void setSavingAccId(int savingAccId) {
        this.savingAccId = savingAccId;
    }

    public int getBranchCodeId() {
        return branchCodeId;
    }

    public void setBranchCodeId(int branchCodeId) {
        this.branchCodeId = branchCodeId;
    }

    public String getSystemDateId() {
        return systemDateId;
    }

    public void setSystemDateId(String systemDateId) {
        this.systemDateId = systemDateId;
    }

    public int getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(int debtorId) {
        this.debtorId = debtorId;
    }

    public int getSavingTypeList() {
        return savingTypeList;
    }

    public void setSavingTypeList(int savingTypeList) {
        this.savingTypeList = savingTypeList;
    }

    public int getSavingSubTypesList() {
        return savingSubTypesList;
    }

    public void setSavingSubTypesList(int savingSubTypesList) {
        this.savingSubTypesList = savingSubTypesList;
    }

    public double getSubInterestRate() {
        return subInterestRate;
    }

    public void setSubInterestRate(double subInterestRate) {
        this.subInterestRate = subInterestRate;
    }

    public double getSavingAmount() {
        return savingAmount;
    }

    public void setSavingAmount(double savingAmount) {
        this.savingAmount = savingAmount;
    }

    public String getBookNo() {
        return bookNo;
    }

    public void setBookNo(String bookNo) {
        this.bookNo = bookNo;
    }
}

package com.bankfinance.form;

import java.util.Date;

public class BatchPosting {

    private Date transactionDate;
    private String accountNo;
    private String module;
    private String transactionCode;
    private String refference;
    private String description;
    private String orderNumber;
    private double amountExcl;
    private int isDebit;
    private double amountIncl;
    private String projectCode;
    private String glContraCode;

    public BatchPosting() {
    }

    /**
     * @return the transactionDate
     */
    public Date getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the module
     */
    public String getModule() {
        return module;
    }

    /**
     * @param module the module to set
     */
    public void setModule(String module) {
        this.module = module;
    }

    /**
     * @return the transactionCode
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * @param transactionCode the transactionCode to set
     */
    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    /**
     * @return the refference
     */
    public String getRefference() {
        return refference;
    }

    /**
     * @param refference the refference to set
     */
    public void setRefference(String refference) {
        this.refference = refference;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the amountExcl
     */
    public double getAmountExcl() {
        return amountExcl;
    }

    /**
     * @param amountExcl the amountExcl to set
     */
    public void setAmountExcl(double amountExcl) {
        this.amountExcl = amountExcl;
    }

    /**
     * @return the isDebit
     */
    public int getIsDebit() {
        return isDebit;
    }

    /**
     * @param isDebit the isDebit to set
     */
    public void setIsDebit(int isDebit) {
        this.isDebit = isDebit;
    }

    /**
     * @return the amountIncl
     */
    public double getAmountIncl() {
        return amountIncl;
    }

    /**
     * @param amountIncl the amountIncl to set
     */
    public void setAmountIncl(double amountIncl) {
        this.amountIncl = amountIncl;
    }

    /**
     * @return the projectCode
     */
    public String getProjectCode() {
        return projectCode;
    }

    /**
     * @param projectCode the projectCode to set
     */
    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    /**
     * @return the glContraCode
     */
    public String getGlContraCode() {
        return glContraCode;
    }

    /**
     * @param glContraCode the glContraCode to set
     */
    public void setGlContraCode(String glContraCode) {
        this.glContraCode = glContraCode;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

/**
 *
 * @author Mohan
 */
public class ApprovalDetailsModel {
    
    private Integer app_id1;
    private Integer app_id2;
    private Integer app_id3;
    private String app_name1;
    private String app_name2;
    private String app_name3;
    private String comment1;
    private String comment2;
    private Integer approvalStatus;

    public ApprovalDetailsModel() {
    }

    public ApprovalDetailsModel(Integer app_id1, Integer app_id2, Integer app_id3, String app_name1, String app_name2, String app_name3, String comment1, String comment2, Integer approvalStatus) {
        this.app_id1 = app_id1;
        this.app_id2 = app_id2;
        this.app_id3 = app_id3;
        this.app_name1 = app_name1;
        this.app_name2 = app_name2;
        this.app_name3 = app_name3;
        this.comment1 = comment1;
        this.comment2 = comment2;
        this.approvalStatus = approvalStatus;
    }

    public Integer getApp_id1() {
        return app_id1;
    }

    public void setApp_id1(Integer app_id1) {
        this.app_id1 = app_id1;
    }

    public Integer getApp_id2() {
        return app_id2;
    }

    public void setApp_id2(Integer app_id2) {
        this.app_id2 = app_id2;
    }

    public Integer getApp_id3() {
        return app_id3;
    }

    public void setApp_id3(Integer app_id3) {
        this.app_id3 = app_id3;
    }

    public String getApp_name1() {
        return app_name1;
    }

    public void setApp_name1(String app_name1) {
        this.app_name1 = app_name1;
    }

    public String getApp_name2() {
        return app_name2;
    }

    public void setApp_name2(String app_name2) {
        this.app_name2 = app_name2;
    }

    public String getApp_name3() {
        return app_name3;
    }

    public void setApp_name3(String app_name3) {
        this.app_name3 = app_name3;
    }

    


    public String getComment1() {
        return comment1;
    }

    public void setComment1(String comment1) {
        this.comment1 = comment1;
    }

    public String getComment2() {
        return comment2;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public Integer getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }
    
    
}

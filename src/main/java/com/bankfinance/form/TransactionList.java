/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

/**
 *
 * @author ites
 */
public class TransactionList {

    private int setId;
    private String payment;
    private String receiptNo;
    private String description;
    private String paymentDate;

    public TransactionList() {
    }

    public TransactionList(int setId, String payment, String receiptNo, String description, String paymentDate) {
        this.setId = setId;
        this.payment = payment;
        this.receiptNo = receiptNo;
        this.description = description;
        this.paymentDate = paymentDate;
    }

    public int getSetId() {
        return setId;
    }

    public void setSetId(int setId) {
        this.setId = setId;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

/**
 *
 * @author Mohan
 */
public class OtherChargseModel {
    
    Integer id;
    String description;
    String rate;
    Double amount;
    Boolean isAdded; 
    Boolean isPrecentage;
    Double taxRate;
    Integer isDownPaymentCharge;
    Integer isPaid;
    Integer chargeID;

    public OtherChargseModel() {
    }
    
    public OtherChargseModel(Integer id, String description, String rate, Double amount, Boolean isAdded, Boolean isPrecentage, Double taxRate) {
        this.id = id;
        this.description = description;
        this.rate = rate;
        this.amount = amount;
        this.isAdded = isAdded;
        this.isPrecentage = isPrecentage;
        this.taxRate = taxRate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getIsAdded() {
        return isAdded;
    }

    public void setIsAdded(Boolean isAdded) {
        this.isAdded = isAdded;
    }

    public Boolean getIsPrecentage() {
        return isPrecentage;
    }

    public void setIsPrecentage(Boolean isPrecentage) {
        this.isPrecentage = isPrecentage;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Integer getIsDownPaymentCharge() {
        return isDownPaymentCharge;
    }

    public void setIsDownPaymentCharge(Integer isDownPaymentCharge) {
        this.isDownPaymentCharge = isDownPaymentCharge;
    }

    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public Integer getChargeID() {
        return chargeID;
    }

    public void setChargeID(Integer chargeID) {
        this.chargeID = chargeID;
    }
    
    
    
}

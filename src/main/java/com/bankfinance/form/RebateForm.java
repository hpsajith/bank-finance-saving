/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

/**
 *
 * @author MsD
 */
public class RebateForm {

    private Integer loanId;
    private String agreementNo;
    private String customerAccNo;
    private String debtorName;
    private String debtorNic;
    private Integer debtorTelephone;
    private String debtorAddress;

    public RebateForm() {
    }

    
    public RebateForm(Integer loanId, String agreementNo, String customerAccNo, String debtorName, String debtorNic, Integer debtorTelephone, String debtorAddress) {
        this.loanId = loanId;
        this.agreementNo = agreementNo;
        this.customerAccNo = customerAccNo;
        this.debtorName = debtorName;
        this.debtorNic = debtorNic;
        this.debtorTelephone = debtorTelephone;
        this.debtorAddress = debtorAddress;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getCustomerAccNo() {
        return customerAccNo;
    }

    public void setCustomerAccNo(String customerAccNo) {
        this.customerAccNo = customerAccNo;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public String getDebtorNic() {
        return debtorNic;
    }

    public void setDebtorNic(String debtorNic) {
        this.debtorNic = debtorNic;
    }

    public Integer getDebtorTelephone() {
        return debtorTelephone;
    }

    public void setDebtorTelephone(Integer debtorTelephone) {
        this.debtorTelephone = debtorTelephone;
    }

    public String getDebtorAddress() {
        return debtorAddress;
    }

    public void setDebtorAddress(String debtorAddress) {
        this.debtorAddress = debtorAddress;
    }
    
    

}

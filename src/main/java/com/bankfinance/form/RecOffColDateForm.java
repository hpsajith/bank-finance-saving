/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bankfinance.form;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author IT
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecOffColDateForm {
    
    private int recOffId;
    private int date;

    /**
     * @return the recOffId
     */
    public int getRecOffId() {
        return recOffId;
    }

    /**
     * @param recOffId the recOffId to set
     */
    public void setRecOffId(int recOffId) {
        this.recOffId = recOffId;
    }

    /**
     * @return the date
     */
    public int getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(int date) {
        this.date = date;
    }
    
}

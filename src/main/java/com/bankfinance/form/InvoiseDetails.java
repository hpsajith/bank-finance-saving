/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

/**
 *
 * @author Mohan
 */
public class InvoiseDetails {

    private Integer type;
    private Integer invoiceId;
    private String description;
    private double invoiceValue;
    private String articleWeight1;
    private String articleWeight2;
    private String articleCarrateRate;

    public InvoiseDetails() {
    }

    public InvoiseDetails(Integer type, Integer invoiceId, String description, double invoiceValue,String articleWeight1,String articleWeight2,String articleCarrateRate) {
        this.type = type;
        this.invoiceId = invoiceId;
        this.description = description;
        this.invoiceValue = invoiceValue;
        this.articleWeight1 = articleWeight1;
        this.articleWeight2 = articleWeight2;
        this.articleCarrateRate = articleCarrateRate;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(double invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getArticleWeight1() {
        return articleWeight1;
    }

    public void setArticleWeight1(String articleWeight1) {
        this.articleWeight1 = articleWeight1;
    }

    public String getArticleWeight2() {
        return articleWeight2;
    }

    public void setArticleWeight2(String articleWeight2) {
        this.articleWeight2 = articleWeight2;
    }

    public String getArticleCarrateRate() {
        return articleCarrateRate;
    }

    public void setArticleCarrateRate(String articleCarrateRate) {
        this.articleCarrateRate = articleCarrateRate;
    }
    
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankfinance.form;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MsD
 */
@XmlRootElement(name = "PortfolioDetail")
public class PortfolioDetails {

    private Integer serial_No;
    private String debtor_Code;
    private String debtor_Name;
    private String debtor_Nic;
    private String loan_Agreement_No;
    private String vehicle_No;
    private String facility_Type;
    private String loan_Date;
    private String loan_Amount;
    private String loan_Period;
    private String loan_End_Date;
    private String loan_Due_Date;
    private String advance_Payment_Amount;
    private String pre_Payment_Amount;
    private String total_Down_Payment;
    private String current_Month_Rental;
    private String capital_Outstanding;
    private String no_Of_Rental_In_Arrears;
    private String rental_Amount_In_Arrears;
    private String capital_Amount_In_Arrears;
    private String interest_Amount_In_Arrears;
    private String interest_In_Suspens_Amount;
    
    private String other_Charges_Arears;
    private String toatal_Arears_Amount;
    private String last_Payment_Date;
    private String last_Payment_Amount;
    private long arrears_Days;
    private String collection_On_Rental_During_Current_Month;
    private String collection_On_Arrears_During_Current_Month;
    private String over_Due_Collected_Amount;
    private String other_Charge_Collected_Amount;
    private String total_Collected_Amount;
    private String total_Over_Payment_Amount;
    private String future_Capital_Amount;
    private String future_Interest_Amount;
    private String bad_Debtor_Provision_Amount;
    private String loan_Status;
    private String contact_Person;
    private String telephone_No;
    private String marketing_Officer;
    private String follow_Up_Officer;
    private String engine_Number;
    private String chasis_No;
    private String insurance_Company;
    private String insurance_Assigned;
    private String vehicle_Class;
    private String vehicle_Type;
    private String loan_Interest_Rate;
    private int due_Day;
    private String odi_Arrears_Amount;
    private String debit_Odi_Chargers;
    private String credit_Odi_Chargers;
    private String wave_Off_Odi_Chargers;

    public PortfolioDetails() {
    }

    public Integer getSerial_No() {
        return serial_No;
    }

    public void setSerial_No(Integer serial_No) {
        this.serial_No = serial_No;
    }

    public String getDebtor_Code() {
        return debtor_Code;
    }

    public void setDebtor_Code(String debtor_Code) {
        this.debtor_Code = debtor_Code;
    }

    public String getDebtor_Name() {
        return debtor_Name;
    }

    public void setDebtor_Name(String debtor_Name) {
        this.debtor_Name = debtor_Name;
    }

    public String getDebtor_Nic() {
        return debtor_Nic;
    }

    public void setDebtor_Nic(String debtor_Nic) {
        this.debtor_Nic = debtor_Nic;
    }

    public String getLoan_Agreement_No() {
        return loan_Agreement_No;
    }

    public void setLoan_Agreement_No(String loan_Agreement_No) {
        this.loan_Agreement_No = loan_Agreement_No;
    }

    public String getVehicle_No() {
        return vehicle_No;
    }

    public void setVehicle_No(String vehicle_No) {
        this.vehicle_No = vehicle_No;
    }

    public String getFacility_Type() {
        return facility_Type;
    }

    public void setFacility_Type(String facility_Type) {
        this.facility_Type = facility_Type;
    }

    public String getLoan_Date() {
        return loan_Date;
    }

    public void setLoan_Date(String loan_Date) {
        this.loan_Date = loan_Date;
    }

    public String getLoan_Amount() {
        return loan_Amount;
    }

    public void setLoan_Amount(String loan_Amount) {
        this.loan_Amount = loan_Amount;
    }

    public String getLoan_Period() {
        return loan_Period;
    }

    public void setLoan_Period(String loan_Period) {
        this.loan_Period = loan_Period;
    }

    public String getLoan_End_Date() {
        return loan_End_Date;
    }

    public void setLoan_End_Date(String loan_End_Date) {
        this.loan_End_Date = loan_End_Date;
    }

    public String getLoan_Due_Date() {
        return loan_Due_Date;
    }

    public void setLoan_Due_Date(String loan_Due_Date) {
        this.loan_Due_Date = loan_Due_Date;
    }

    public String getAdvance_Payment_Amount() {
        return advance_Payment_Amount;
    }

    public void setAdvance_Payment_Amount(String advance_Payment_Amount) {
        this.advance_Payment_Amount = advance_Payment_Amount;
    }

    public String getPre_Payment_Amount() {
        return pre_Payment_Amount;
    }

    public void setPre_Payment_Amount(String pre_Payment_Amount) {
        this.pre_Payment_Amount = pre_Payment_Amount;
    }

    public String getTotal_Down_Payment() {
        return total_Down_Payment;
    }

    public void setTotal_Down_Payment(String total_Down_Payment) {
        this.total_Down_Payment = total_Down_Payment;
    }

    public String getCurrent_Month_Rental() {
        return current_Month_Rental;
    }

    public void setCurrent_Month_Rental(String current_Month_Rental) {
        this.current_Month_Rental = current_Month_Rental;
    }

    public String getCapital_Outstanding() {
        return capital_Outstanding;
    }

    public void setCapital_Outstanding(String capital_Outstanding) {
        this.capital_Outstanding = capital_Outstanding;
    }

    public String getNo_Of_Rental_In_Arrears() {
        return no_Of_Rental_In_Arrears;
    }

    public void setNo_Of_Rental_In_Arrears(String no_Of_Rental_In_Arrears) {
        this.no_Of_Rental_In_Arrears = no_Of_Rental_In_Arrears;
    }

    public String getRental_Amount_In_Arrears() {
        return rental_Amount_In_Arrears;
    }

    public void setRental_Amount_In_Arrears(String rental_Amount_In_Arrears) {
        this.rental_Amount_In_Arrears = rental_Amount_In_Arrears;
    }

    public String getCapital_Amount_In_Arrears() {
        return capital_Amount_In_Arrears;
    }

    public void setCapital_Amount_In_Arrears(String capital_Amount_In_Arrears) {
        this.capital_Amount_In_Arrears = capital_Amount_In_Arrears;
    }

    public String getInterest_Amount_In_Arrears() {
        return interest_Amount_In_Arrears;
    }

    public void setInterest_Amount_In_Arrears(String interest_Amount_In_Arrears) {
        this.interest_Amount_In_Arrears = interest_Amount_In_Arrears;
    }

    public String getInterest_In_Suspens_Amount() {
        return interest_In_Suspens_Amount;
    }

    public void setInterest_In_Suspens_Amount(String interest_In_Suspens_Amount) {
        this.interest_In_Suspens_Amount = interest_In_Suspens_Amount;
    }

    public String getOther_Charges_Arears() {
        return other_Charges_Arears;
    }

    public void setOther_Charges_Arears(String other_Charges_Arears) {
        this.other_Charges_Arears = other_Charges_Arears;
    }

    public String getToatal_Arears_Amount() {
        return toatal_Arears_Amount;
    }

    public void setToatal_Arears_Amount(String toatal_Arears_Amount) {
        this.toatal_Arears_Amount = toatal_Arears_Amount;
    }

    public String getLast_Payment_Date() {
        return last_Payment_Date;
    }

    public void setLast_Payment_Date(String last_Payment_Date) {
        this.last_Payment_Date = last_Payment_Date;
    }

    public String getLast_Payment_Amount() {
        return last_Payment_Amount;
    }

    public void setLast_Payment_Amount(String last_Payment_Amount) {
        this.last_Payment_Amount = last_Payment_Amount;
    }

    public long getArrears_Days() {
        return arrears_Days;
    }

    public void setArrears_Days(long arrears_Days) {
        this.arrears_Days = arrears_Days;
    }

    public String getCollection_On_Rental_During_Current_Month() {
        return collection_On_Rental_During_Current_Month;
    }

    public void setCollection_On_Rental_During_Current_Month(String collection_On_Rental_During_Current_Month) {
        this.collection_On_Rental_During_Current_Month = collection_On_Rental_During_Current_Month;
    }

    public String getCollection_On_Arrears_During_Current_Month() {
        return collection_On_Arrears_During_Current_Month;
    }

    public void setCollection_On_Arrears_During_Current_Month(String collection_On_Arrears_During_Current_Month) {
        this.collection_On_Arrears_During_Current_Month = collection_On_Arrears_During_Current_Month;
    }

    public String getOver_Due_Collected_Amount() {
        return over_Due_Collected_Amount;
    }

    public void setOver_Due_Collected_Amount(String over_Due_Collected_Amount) {
        this.over_Due_Collected_Amount = over_Due_Collected_Amount;
    }

    public String getOther_Charge_Collected_Amount() {
        return other_Charge_Collected_Amount;
    }

    public void setOther_Charge_Collected_Amount(String other_Charge_Collected_Amount) {
        this.other_Charge_Collected_Amount = other_Charge_Collected_Amount;
    }

    public String getTotal_Collected_Amount() {
        return total_Collected_Amount;
    }

    public void setTotal_Collected_Amount(String total_Collected_Amount) {
        this.total_Collected_Amount = total_Collected_Amount;
    }

    public String getTotal_Over_Payment_Amount() {
        return total_Over_Payment_Amount;
    }

    public void setTotal_Over_Payment_Amount(String total_Over_Payment_Amount) {
        this.total_Over_Payment_Amount = total_Over_Payment_Amount;
    }

    public String getFuture_Capital_Amount() {
        return future_Capital_Amount;
    }

    public void setFuture_Capital_Amount(String future_Capital_Amount) {
        this.future_Capital_Amount = future_Capital_Amount;
    }

    public String getFuture_Interest_Amount() {
        return future_Interest_Amount;
    }

    public void setFuture_Interest_Amount(String future_Interest_Amount) {
        this.future_Interest_Amount = future_Interest_Amount;
    }

    public String getBad_Debtor_Provision_Amount() {
        return bad_Debtor_Provision_Amount;
    }

    public void setBad_Debtor_Provision_Amount(String bad_Debtor_Provision_Amount) {
        this.bad_Debtor_Provision_Amount = bad_Debtor_Provision_Amount;
    }

    public String getLoan_Status() {
        return loan_Status;
    }

    public void setLoan_Status(String loan_Status) {
        this.loan_Status = loan_Status;
    }

    public String getContact_Person() {
        return contact_Person;
    }

    public void setContact_Person(String contact_Person) {
        this.contact_Person = contact_Person;
    }

    public String getTelephone_No() {
        return telephone_No;
    }

    public void setTelephone_No(String telephone_No) {
        this.telephone_No = telephone_No;
    }

    public String getMarketing_Officer() {
        return marketing_Officer;
    }

    public void setMarketing_Officer(String marketing_Officer) {
        this.marketing_Officer = marketing_Officer;
    }

    public String getFollow_Up_Officer() {
        return follow_Up_Officer;
    }

    public void setFollow_Up_Officer(String follow_Up_Officer) {
        this.follow_Up_Officer = follow_Up_Officer;
    }

    public String getEngine_Number() {
        return engine_Number;
    }

    public void setEngine_Number(String engine_Number) {
        this.engine_Number = engine_Number;
    }

    public String getChasis_No() {
        return chasis_No;
    }

    public void setChasis_No(String chasis_No) {
        this.chasis_No = chasis_No;
    }

    public String getInsurance_Company() {
        return insurance_Company;
    }

    public void setInsurance_Company(String insurance_Company) {
        this.insurance_Company = insurance_Company;
    }

    public String getInsurance_Assigned() {
        return insurance_Assigned;
    }

    public void setInsurance_Assigned(String insurance_Assigned) {
        this.insurance_Assigned = insurance_Assigned;
    }

    public String getVehicle_Class() {
        return vehicle_Class;
    }

    public void setVehicle_Class(String vehicle_Class) {
        this.vehicle_Class = vehicle_Class;
    }

    public String getVehicle_Type() {
        return vehicle_Type;
    }

    public void setVehicle_Type(String vehicle_Type) {
        this.vehicle_Type = vehicle_Type;
    }

    public String getLoan_Interest_Rate() {
        return loan_Interest_Rate;
    }

    public void setLoan_Interest_Rate(String loan_Interest_Rate) {
        this.loan_Interest_Rate = loan_Interest_Rate;
    }

    public int getDue_Day() {
        return due_Day;
    }

    public void setDue_Day(int due_Day) {
        this.due_Day = due_Day;
    }
    
    public String getOdi_Arrears_Amount() {
        return odi_Arrears_Amount;
    }

    public void setOdi_Arrears_Amount(String odi_Arrears_Amount) {
        this.odi_Arrears_Amount = odi_Arrears_Amount;
    }

    public String getDebit_Odi_Chargers() {
        return debit_Odi_Chargers;
    }

    public void setDebit_Odi_Chargers(String debit_Odi_Chargers) {
        this.debit_Odi_Chargers = debit_Odi_Chargers;
    }

    public String getCredit_Odi_Chargers() {
        return credit_Odi_Chargers;
    }

    public void setCredit_Odi_Chargers(String credit_Odi_Chargers) {
        this.credit_Odi_Chargers = credit_Odi_Chargers;
    }

    public String getWave_Off_Odi_Chargers() {
        return wave_Off_Odi_Chargers;
    }

    public void setWave_Off_Odi_Chargers(String wave_Off_Odi_Chargers) {
        this.wave_Off_Odi_Chargers = wave_Off_Odi_Chargers;
    }
    
    
}



package com.bankfinance.form;

import java.util.List;

/**
 *
 * @author IT
 */
public class ChartData {
    
    private List<String> labels;
    private List<ChartDataSet> datasets;

    /**
     * @return the labels
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * @param labels the labels to set
     */
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    /**
     * @return the datasets
     */
    public List<ChartDataSet> getDatasets() {
        return datasets;
    }

    /**
     * @param datasets the datasets to set
     */
    public void setDatasets(List<ChartDataSet> datasets) {
        this.datasets = datasets;
    }
    
}

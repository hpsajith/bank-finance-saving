package com.ites.bankfinance.daoImpl;

import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.dao.UtilityDao;
import com.ites.bankfinance.model.Announcement;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanMessage;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserType;
import com.ites.bankfinance.model.UmUserTypeAnnouncement;
import com.ites.bankfinance.model.UserMessage;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UtilityDaoImpl implements UtilityDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    UserDao userDao;

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public boolean saveOrUpdateAnnouncement(Announcement announcement) {
        boolean success = false;
        int userId = userDao.findByUserName();
        int branchId = announcement.getBranchId();
        String publishDate = dateFormat.format(Calendar.getInstance().getTime());
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            announcement.setMBranch(new MBranch(branchId));
            announcement.setUmUser(new UmUser(userId));
            for (int userType : announcement.getUserTypes()) {
                UmUserTypeAnnouncement userTypeAnnouncement = new UmUserTypeAnnouncement();
                userTypeAnnouncement.setAnnouncement(announcement);
                userTypeAnnouncement.setPublishDate(dateFormat.parse(publishDate));
                userTypeAnnouncement.setUmUserType(new UmUserType(userType));
                announcement.getUmUserTypeAnnouncements().add(userTypeAnnouncement);
            }
            if (announcement.getId() != null) {
                String sql = "delete from UmUserTypeAnnouncement where announcement = :announcement";
                session.createQuery(sql).setParameter("announcement", announcement).executeUpdate();
            }
            session.saveOrUpdate(announcement);
            tx.commit();
            success = true;
        } catch (ParseException | HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<Announcement> findAnnouncements() {
        List<Announcement> announcements = null;
        try {
            String sql = "from Announcement order by id desc";
            announcements = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return announcements;
    }

    @Override
    public List<Announcement> findAnnouncements(int userTypeId) {
        List<Announcement> announcements = null;
        try {
            String sql = "from Announcement as ann inner join fetch ann.umUserTypeAnnouncements as userTypeAn where userTypeAn.umUserType = :umUserType order by ann.id desc";
            announcements = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("umUserType", new UmUserType(userTypeId)).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return announcements;
    }

    @Override
    public List<Announcement> findAnnouncements(Date publishDate) {
        List<Announcement> announcements = null;
        try {
            String sql = "select distinct ann from Announcement as ann inner join ann.umUserTypeAnnouncements as userTypeAn where userTypeAn.publishDate = :publishDate order by ann.id desc";
            announcements = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("publishDate", publishDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return announcements;
    }

    @Override
    public List<Announcement> findAnnouncements(int userTypeId, Date publishDate) {
        List<Announcement> announcements = null;
        try {
            String sql = "from Announcement as ann inner join fetch ann.umUserTypeAnnouncements as userTypeAn where userTypeAn.umUserType = :umUserType and userTypeAn.publishDate = :publishDate order by ann.id desc";
            announcements = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("umUserType", new UmUserType(userTypeId)).
                    setParameter("publishDate", publishDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return announcements;
    }

    @Override
    public Announcement findAnnouncement(int announcementId) {
        Announcement announcement = null;
        try {
            String sql = "from Announcement where id = :id";
            announcement = (Announcement) sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("id", announcementId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return announcement;
    }

    @Override
    public boolean deleteAnnouncement(int announcementId) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "from Announcement where id = :id";
            Announcement announcement = (Announcement) session.createQuery(sql)
                    .setParameter("id", announcementId).uniqueResult();
            if (announcement != null) {
                session.delete(announcement);
                tx.commit();
                success = true;
            }
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<Announcement> findPreviousAnnouncements(Date publishDate) {
        List<Announcement> announcements = null;
        try {
            String sql = "select distinct ann from Announcement as ann inner join ann.umUserTypeAnnouncements as userTypeAn where userTypeAn.publishDate = :publishDate order by ann.id desc";
            announcements = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("publishDate", publishDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return announcements;
    }

    @Override
    public List<Announcement> findPreviousAnnouncements(int userTypeId, Date publishDate) {
        List<Announcement> announcements = null;
        try {
            String sql = "from Announcement as ann inner join fetch ann.umUserTypeAnnouncements as userTypeAn where userTypeAn.umUserType = :umUserType and userTypeAn.publishDate = :publishDate order by ann.id desc";
            announcements = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("umUserType", new UmUserType(userTypeId)).
                    setParameter("publishDate", publishDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return announcements;
    }

    @Override
    public boolean saveOrUpdateUserMessage(UserMessage userMessage) {
        boolean success = false;
        int loggedUserId = userDao.findByUserName();
        int userId = userMessage.getUserId();
        int branchId = userMessage.getBranchId();
        String publishDate = dateFormat.format(Calendar.getInstance().getTime());
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            userMessage.setActionTime(Calendar.getInstance().getTime());
            if (userMessage.getId() == null) {
                userMessage.setIsRead(false);
            }
            userMessage.setMBranch(new MBranch(branchId));
            userMessage.setPublishDate(dateFormat.parse(publishDate));
            userMessage.setUmUserByUId(new UmUser(loggedUserId));
            userMessage.setUmUserByUserId(new UmUser(userId));
            session.saveOrUpdate(userMessage);
            tx.commit();
            success = true;
        } catch (HibernateException | ParseException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<UserMessage> findUserMessages() {
        List<UserMessage> userMessages = null;
        try {
            String sql = "from UserMessage";
            userMessages = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userMessages;
    }

    @Override
    public List<UserMessage> findUserMessages(int userId) {
        List<UserMessage> userMessages = null;
        try {
            String sql = "from UserMessage where umUserByUserId = :umUserByUserId and isRead = :isRead order by id desc";
            userMessages = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("umUserByUserId", new UmUser(userId))
                    .setParameter("isRead", false).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userMessages;
    }

    @Override
    public List<UserMessage> findUserMessages(Date publishDate) {
        List<UserMessage> userMessages = null;
        try {
            String sql = "from UserMessage where publishDate = :publishDate";
            userMessages = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("publishDate", publishDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userMessages;
    }

    @Override
    public List<UserMessage> findUserMessages(int userId, Date publishDate) {
        List<UserMessage> userMessages = null;
        try {
            String sql = "from UserMessage where umUserByUserId = :umUserByUserId and publishDate = :publishDate";
            userMessages = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("umUserByUserId", new UmUser(userId))
                    .setParameter("publishDate", publishDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userMessages;
    }

    @Override
    public List<UserMessage> findUserMessages(Date sDate, Date eDate) {
        List<UserMessage> userMessages = null;
        try {
            userMessages = sessionFactory.getCurrentSession().createCriteria(UserMessage.class).
                    add(Restrictions.between("publishDate", sDate, eDate)).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userMessages;
    }

    @Override
    public UserMessage findUserMessage(int userMessageId) {
        UserMessage userMessage = null;
        try {
            String sql = "from UserMessage where id = :id";
            userMessage = (UserMessage) sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("id", userMessageId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userMessage;
    }

    @Override
    public boolean readUserMessage(int userMessageId) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "update UserMessage set isRead = :isRead where id = :id";
            session.createQuery(sql).setParameter("isRead", true).
                    setParameter("id", userMessageId).executeUpdate();
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public boolean deleteUserMessage(int userMessageId) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "delete from UserMessage where id = :id";
            session.createQuery(sql).setParameter("id", userMessageId).executeUpdate();
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public boolean saveOrUpdateLoanMessage(LoanMessage loanMessage) {
        boolean success = false;
        int userId = userDao.findByUserName();
        int loanId = loanMessage.getLoanId();
        int branchId = loanMessage.getBranchId();
        String publishDate = dateFormat.format(Calendar.getInstance().getTime());
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            loanMessage.setActionTime(Calendar.getInstance().getTime());
            loanMessage.setLoanHeaderDetails(new LoanHeaderDetails(loanId));
            loanMessage.setMBranch(new MBranch(branchId));
            loanMessage.setPublishDate(dateFormat.parse(publishDate));
            loanMessage.setUmUser(new UmUser(userId));
            if (loanMessage.getId() == null) {
                loanMessage.setIsActive(true);
            }
            session.saveOrUpdate(loanMessage);
            tx.commit();
            success = true;
        } catch (ParseException | HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<LoanMessage> findLoanMessages() {
        List<LoanMessage> loanMessages = null;
        try {
            String sql = "from LoanMessage";
            loanMessages = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanMessages;
    }

    @Override
    public List<LoanMessage> findLoanMessages(int loanId) {
        List<LoanMessage> loanMessages = null;
        try {
            String sql = "from LoanMessage where loanHeaderDetails = :loanHeaderDetails";
            loanMessages = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("loanHeaderDetails", new LoanHeaderDetails(loanId)).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanMessages;
    }

    @Override
    public List<LoanMessage> findLoanMessages(Date publishDate) {
        List<LoanMessage> loanMessages = null;
        try {
            String sql = "from LoanMessage where publishDate = :publishDate and isActive = :isActive";
            loanMessages = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("publishDate", publishDate).
                    setParameter("isActive", true).
                    list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanMessages;
    }

    @Override
    public List<LoanMessage> findLoanMessages(Date sDate, Date eDate) {
        List<LoanMessage> loanMessages = null;
        try {
            loanMessages = sessionFactory.getCurrentSession().createCriteria(LoanMessage.class).
                    add(Restrictions.between("publishDate", sDate, eDate)).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanMessages;
    }

    @Override
    public LoanMessage findLoanMessage(int loanMessageId) {
        LoanMessage loanMessage = null;
        try {
            String sql = "from LoanMessage where id = :id";
            loanMessage = (LoanMessage) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("id", loanMessageId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanMessage;
    }

    @Override
    public boolean deleteLoanMessage(int loanMessageId) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "delete from LoanMessage where id = :id";
            session.createQuery(sql).setParameter("id", loanMessageId).executeUpdate();
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.ites.bankfinance.dao.InsuraceDao;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.DayEndDate;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.InsuranceCompany;
import com.ites.bankfinance.model.InsuranceOtherCharge;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.InsuranceProcessDelete;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanPropertyLandDetails;
import com.ites.bankfinance.model.LoanPropertyOtherDetails;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.SettlementMain;
import com.ites.bankfinance.service.UserService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InsuraceDaoImpl implements InsuraceDao {

    public static final String insurancePaymentCode = "INSU";
    public static final String insurancePayment = "Insurance Payment";
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    UserService userService;

    @Autowired
    SettlementDaoImpl settDao;

    @Override
    public Boolean saveOrUpdateUserInsuraceCompany(InsuranceCompany company) {
        Boolean is_saved = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            company.setActionTime(date);
            company.setUserId(userId);
            company.setIsActive(true);
            company.setBccRate(company.getBccRate());
            company.setSrRate(company.getSrRate());
            session.saveOrUpdate(company);
            is_saved = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return is_saved;
    }

    @Override
    public List<InsuranceCompany> findInsuranceCompanyes() {
        List<InsuranceCompany> companys = null;
        try {
            String sql = "from InsuranceCompany";
            companys = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return companys;
    }

    @Override
    public InsuranceCompany findInsuranceCompanyID(int comId) {
        InsuranceCompany company = null;
        try {
            StatelessSession session = sessionFactory.openStatelessSession();
            company = (InsuranceCompany) session.createCriteria(InsuranceCompany.class).add(Restrictions.eq("comId", comId)).uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return company;
    }

    @Override
    public Boolean inActiveCompany(int comID) {
        Boolean inActive = false;
        InsuranceCompany company = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            company = (InsuranceCompany) session.createCriteria(InsuranceCompany.class).add(Restrictions.eq("comId", comID)).uniqueResult();
            company.setIsActive(false);
            session.saveOrUpdate(company);
            inActive = true;
        } catch (Exception e) {
            inActive = false;
        }
        transaction.commit();
        session.flush();
        session.close();
        return inActive;
    }

    @Override
    public Boolean activeCompany(int comID) {
        Boolean active = false;
        InsuranceCompany company = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            company = (InsuranceCompany) session.
                    createCriteria(InsuranceCompany.class).add(Restrictions.eq("comId", comID)).uniqueResult();
            company.setIsActive(true);
            session.saveOrUpdate(company);
            active = true;
        } catch (Exception e) {
            active = false;
        }
        transaction.commit();
        session.flush();
        session.close();
        return active;
    }

    @Override
    public List findLoanHeaderDetails() {
        List loanList = new ArrayList();
        Double land = 0.00, vehicle = 0.00, other = 0.00;
        try {
            //SELECT lh.Loan_Id,sl.Sub_Loan_Name,lh.Loan_Amount,dh.Debtor_Name,lh.Loan_Date FROM loan_header_details AS lh , m_sub_loan_type AS sl,debtor_header_details AS dh WHERE lh.Loan_Type = sl.Sub_Loan_Id AND sl.Is_Insurance = 1 AND dh.Debtor_Id = lh.Loan_Debtor_Id AND lh.Loan_IsIssue = 0
            String sql = "select lh.loanId,lh.loanAgreementNo,lh.loanAmount,lh.debtorHeaderDetails.nameWithInitial,"
                    + "lh.loanDate from LoanHeaderDetails as lh,MSubLoanType as sl "
                    + "where lh.loanType = sl.subLoanId and sl.isInsurance = 1 and lh.insuranceStatus = 0 and lh.loanIsIssue = 0 and lh.loanIsDelete = 0 order by lh.loanId desc";
            List headerDetailses = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (headerDetailses.size() > 0) {
                for (int i = 0; i < headerDetailses.size(); i++) {
                    int loanId = 0;
                    String loanName = "", debtorName = "";
                    Double loanAmount = 0.00;
                    String loanDate = null;

                    Object[] row = (Object[]) headerDetailses.get(i);
                    if (row[0] != null) {
                        loanId = Integer.parseInt(row[0].toString());
                    }
                    if (row[1] != null) {
                        loanName = row[1].toString();
                    }
                    if (row[2] != null) {
                        loanAmount = Double.valueOf(row[2].toString());
                    }
                    if (row[3] != null) {
                        debtorName = row[3].toString();
                    }
                    if (row[4] != null) {
                        loanDate = dateFormat.format(row[4]);
                    }
                    Object[] newRow = new Object[6];
                    newRow[0] = loanId;
                    newRow[1] = loanName;
                    newRow[2] = loanAmount;
                    newRow[3] = debtorName;
                    newRow[4] = loanDate;

                    String sq1 = "from LoanPropertyLandDetails where loanId = " + loanId;
                    List<LoanPropertyLandDetails> landProperty = sessionFactory.getCurrentSession().createQuery(sq1).list();

                    String sq2 = "from LoanPropertyOtherDetails where loanId = " + loanId;
                    List<LoanPropertyOtherDetails> otherDetails = sessionFactory.getCurrentSession().createQuery(sq2).list();

                    String sq3 = "from LoanPropertyVehicleDetails where loanId = " + loanId;
                    List<LoanPropertyVehicleDetails> vehicleDetails = sessionFactory.getCurrentSession().createQuery(sq3).list();

                    if (landProperty.size() > 0) {
                        land = landProperty.get(0).getLandPrice();
                    } else if (otherDetails.size() > 0) {
                        vehicle = otherDetails.get(0).getOtherPrice();
                    } else if (vehicleDetails.size() > 0) {
                        other = vehicleDetails.get(0).getVehiclePrice();
                    } else {
                    }
                    newRow[5] = land + vehicle + other;
                    loanList.add(newRow);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public List findInsuranceByAgreementNo(String agreementNo) {
        List loanList = new ArrayList();
        Double land = 0.00, vehicle = 0.00, other = 0.00;
        try {
            //SELECT lh.Loan_Id,sl.Sub_Loan_Name,lh.Loan_Amount,dh.Debtor_Name,lh.Loan_Date FROM loan_header_details AS lh , m_sub_loan_type AS sl,debtor_header_details AS dh WHERE lh.Loan_Type = sl.Sub_Loan_Id AND sl.Is_Insurance = 1 AND dh.Debtor_Id = lh.Loan_Debtor_Id AND lh.Loan_IsIssue = 0
            String sql = "select lh.loanId,sl.subLoanName,lh.loanAmount,lh.debtorHeaderDetails.nameWithInitial,"
                    + "lh.loanDate from LoanHeaderDetails as lh,MSubLoanType as sl "
                    + "where  (lh.loanAgreementNo ='" + agreementNo + "' OR  lh.loanBookNo='" + agreementNo + "') and lh.loanType = sl.subLoanId and sl.isInsurance = 1 and lh.loanIsDelete = 0";
            List headerDetailses = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (headerDetailses.size() > 0) {
                for (int i = 0; i < headerDetailses.size(); i++) {
                    int loanId = 0;
                    String loanName = "", debtorName = "";
                    Double loanAmount = 0.00;
                    String loanDate = null;

                    Object[] row = (Object[]) headerDetailses.get(i);
                    if (row[0] != null) {
                        loanId = Integer.parseInt(row[0].toString());
                    }
                    if (row[1] != null) {
                        loanName = row[1].toString();
                    }
                    if (row[2] != null) {
                        loanAmount = Double.valueOf(row[2].toString());
                    }
                    if (row[3] != null) {
                        debtorName = row[3].toString();
                    }
                    if (row[4] != null) {
                        loanDate = dateFormat.format(row[4]);
                    }
                    Object[] newRow = new Object[6];
                    newRow[0] = loanId;
                    newRow[1] = loanName;
                    newRow[2] = loanAmount;
                    newRow[3] = debtorName;
                    newRow[4] = loanDate;

                    String sq1 = "from LoanPropertyLandDetails where loanId = " + loanId;
                    List<LoanPropertyLandDetails> landProperty = sessionFactory.getCurrentSession().createQuery(sq1).list();

                    String sq2 = "from LoanPropertyOtherDetails where loanId = " + loanId;
                    List<LoanPropertyOtherDetails> otherDetails = sessionFactory.getCurrentSession().createQuery(sq2).list();

                    String sq3 = "from LoanPropertyVehicleDetails where loanId = " + loanId;
                    List<LoanPropertyVehicleDetails> vehicleDetails = sessionFactory.getCurrentSession().createQuery(sq3).list();

                    if (landProperty.size() > 0) {
                        land = landProperty.get(0).getLandPrice();
                    } else if (otherDetails.size() > 0) {
                        vehicle = otherDetails.get(0).getOtherPrice();
                    } else if (vehicleDetails.size() > 0) {
                        other = vehicleDetails.get(0).getVehiclePrice();
                    } else {
                    }
                    newRow[5] = land + vehicle + other;
                    loanList.add(newRow);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public InsuranceOtherCharge findInsOtherCharge() {
        InsuranceOtherCharge otherCharge = null;

        try {
            String sql = "from InsuranceOtherCharge where pk = 1";
            otherCharge = (InsuranceOtherCharge) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return otherCharge;
    }

    @Override
    public LoanHeaderDetails findLoanDetails(int loanID) {
        LoanHeaderDetails headerDetails = null;
        try {
            String sql = "from LoanHeaderDetails as lh where lh.loanId = " + loanID + "and lh.loanIsDelete = 0";
            headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

        } catch (Exception e) {
            return null;
        }
        return headerDetails;
    }

    @Override
    public MSubLoanType findSubLoanType(int loanId) {
        MSubLoanType loanType = null;
        LoanHeaderDetails details = null;
        try {
            details = findLoanDetails(loanId);
            String sql = "from MSubLoanType where subLoanId = " + details.getLoanType();
            loanType = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return loanType;
    }

    @Override
    public Boolean processInsurace(InsuraceProcessMain insurance) {
        Boolean is_saved = true;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        String insCode = "";
        int insu = 0;
        SettlementMain settlementMain = null;
        int userId = userService.findByUserName();
        int userLogBranchId = settDao.getUserLogedBranch(userId);
        Date systemDate = settDao.getSystemDate(userLogBranchId);

        Calendar c = Calendar.getInstance();
        Date date = c.getTime();

        try {
            if (insurance.getInsuId() != null) {
                String sql = "select insuCode from InsuraceProcessMain where insuId =" + insurance.getInsuId();
                insCode = session.createQuery(sql).uniqueResult().toString();
            } else {
                //SELECT * FROM insurace_process_main ORDER BY INSU_CODE DESC LIMIT 1
                String sql1 = "from InsuraceProcessMain order by insuId desc";
                InsuraceProcessMain insuranceCode = (InsuraceProcessMain) session.createQuery(sql1).setMaxResults(1).uniqueResult();
                if (insuranceCode != null) {
                    insu = Integer.parseInt(insuranceCode.getInsuCode().substring(4));
                    insu++;
                } else {
                    insu = 1;
                }
            }
            //3'rd party insurance
            if (insurance.getOtherInsu() == 1) {
                insurance.setActionTime(date);
                insurance.setInsuUserId(userId);
                if (insurance.getInsuId() != null) {
                    insurance.setInsuCode(insCode);
                } else {
                    insurance.setInsuCode(insurancePaymentCode + insu);
                }
                if (insurance.getIsOtherCompany() == null) {
                    insurance.setIsOtherCompany(false);
                } else {
                    insurance.setIsOtherCompany(true);
                }
                insurance.setBranchId(userLogBranchId);
                insurance.setInsuPremium(insurance.getInsuTotalPremium());
                insurance.setInsuOtherCharge(0.00);
                insurance.setIsOtherInsurance(true);
                insurance.setIsRegisted(false);
                session.saveOrUpdate(insurance);
            } else {
                insurance.setActionTime(date);
                insurance.setInsuUserId(userId);
                if (insurance.getInsuId() != null) {
                    insurance.setInsuCode(insCode);
                    insurance.setRenewCount(insurance.getRenewCount());
                } else {
                    insurance.setInsuCode(insurancePaymentCode + insu);
                    List insuList = sessionFactory.getCurrentSession().createCriteria(InsuraceProcessMain.class)
                            .add(Restrictions.eq("insuLoanId", insurance.getInsuLoanId())).list();
                    if (insuList.size() > 0) {
                        //renew insurance
                        InsuraceProcessMain renewC = (InsuraceProcessMain) insuList.get(insuList.size() - 1);
                        insurance.setRenewCount(renewC.getRenewCount() + 1);
                    }
                }
                if (insurance.getIsOtherCompany() == null) {
                    insurance.setIsOtherCompany(false);
                } else {
                    insurance.setIsOtherCompany(true);
                }
                insurance.setBranchId(userLogBranchId);
                insurance.setIsOtherInsurance(false);
                insurance.setIsRegisted(false);
                insurance.setIsPay(false);
                session.saveOrUpdate(insurance);
            }
            int insuId = insurance.getInsuId();
            int insuLoanID = insurance.getInsuLoanId();

            String transactionNo = insurance.getInsuCode();
            Boolean isOtherInsurance = insurance.getIsOtherInsurance();

            if (isOtherInsurance) {
                String sql3 = "from SettlementMain where transactionNoDbt ='" + transactionNo + "'";
                settlementMain = (SettlementMain) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                if (settlementMain != null) {
                    settlementMain.setDebtorId(insurance.getInsuDebtorId());
                    settlementMain.setLoanId(0);
                    settlementMain.setDescription(insurancePayment);
                    settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                    settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                    settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                    settlementMain.setIsPosting(false);
                    settlementMain.setUserId(userId);
                    settlementMain.setBranchId(userLogBranchId);
                    settlementMain.setSystemDate(systemDate);
                    session.saveOrUpdate(settlementMain);
                } else {
                    settlementMain = new SettlementMain();
                    settlementMain.setDebtorId(insurance.getInsuDebtorId());
                    settlementMain.setLoanId(0);
                    settlementMain.setDescription(insurancePayment);
                    settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                    settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                    settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                    settlementMain.setIsPosting(false);
                    settlementMain.setUserId(userId);
                    settlementMain.setSystemDate(systemDate);
                    settlementMain.setBranchId(userLogBranchId);
                    session.saveOrUpdate(settlementMain);
                }
            } else // upadate LoanHeaderDetails 
            if (insuId > 0) {
                String sql2 = "from LoanHeaderDetails where loanId =" + insuLoanID;
                LoanHeaderDetails headerDetails = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();
                headerDetails.setInsuranceStatus(1);
                session.saveOrUpdate(headerDetails);

                if (insurance.getIsOtherCompany() != true) {
                    // upadate SettlementMain
                    //SELECT * FROM settlement_main WHERE Transaction_No_Dbt = 'INSU1'
                    String sql3 = "from SettlementMain where transactionNoDbt ='" + transactionNo + "'";
                    settlementMain = (SettlementMain) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                    if (settlementMain != null) {
                        settlementMain.setDebtorId(insurance.getInsuDebtorId());
                        settlementMain.setLoanId(insurance.getInsuLoanId());
                        settlementMain.setDescription(insurancePayment);
                        settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                        settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                        settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                        settlementMain.setIsPosting(false);
                        settlementMain.setUserId(userId);
                        settlementMain.setSystemDate(systemDate);
                        settlementMain.setBranchId(userLogBranchId);
                        session.saveOrUpdate(settlementMain);
                    } else {
                        settlementMain = new SettlementMain();
                        settlementMain.setDebtorId(insurance.getInsuDebtorId());
                        settlementMain.setLoanId(insurance.getInsuLoanId());
                        settlementMain.setDescription(insurancePayment);
                        settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                        settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                        settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                        settlementMain.setIsPosting(false);
                        settlementMain.setUserId(userId);
                        settlementMain.setSystemDate(systemDate);
                        settlementMain.setBranchId(userLogBranchId);
                        session.saveOrUpdate(settlementMain);
                    }

                } else {
                    String sql = "from SettlementMain where transactionNoDbt = '" + transactionNo + "'";
                    settlementMain = (SettlementMain) session.createQuery(sql).uniqueResult();
                    if (settlementMain != null) {
                        session.delete(settlementMain);
                    }
                }
            }
        } catch (Exception e) {
            is_saved = false;
        }
        transaction.commit();
        session.flush();
        session.close();
        return is_saved;

    }

    @Override
    public List findProcessLoan() {
        List processList = new ArrayList();
        try {
            String sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                    + "lh.debtorHeaderDetails.nameWithInitial, im.insuCode, im.isPay"
                    + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                    + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and lh.loanIsDelete = 0 and im.isRegisted = 0 and im.insuStatus = 1 order by im.insuId desc";
            processList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return processList;
    }

    @Override
    public InsuraceProcessMain findInsuranceDetails(int insuId) {
        InsuraceProcessMain processMain = null;
        try {
            String sql = "from InsuraceProcessMain where insuId = " + insuId;
            processMain = (InsuraceProcessMain) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return processMain;
    }

    @Override
    public List findRenewInsurance() {
        List renewInsu = null;
        Date CurentDate = new Date();
        Date endDate = getStartRenewMonth();
        try {
            StatelessSession session = sessionFactory.openStatelessSession();
            renewInsu = session.createCriteria(InsuraceProcessMain.class).add(Restrictions.between("expiryDate", CurentDate, endDate)).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return renewInsu;
    }

    public Date getStartRenewMonth() {
        try {
            Date date = new Date();
            date.setMonth(date.getMonth() + 3);
            date.setDate(1);
            date.setHours(00);
            date.setMinutes(00);
            date.setSeconds(00);
            return date;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<MSubLoanType> findSubLoanType() {
        List<MSubLoanType> loanTypes = null;
        try {
            loanTypes = sessionFactory.getCurrentSession().createCriteria(MSubLoanType.class).list();
        } catch (Exception e) {
            return null;
        }
        return loanTypes;

    }

    @Override
    public List<LoanHeaderDetails> findLoanDetails() {
        List<LoanHeaderDetails> detailses = null;
        try {
            detailses = sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class).list();
            return detailses;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Boolean saveOrUpdateOtherCustomers(InsuranceOtherCustomers otherCustomers) {
        Boolean is_saved = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            if (otherCustomers.getInsuCustomerId() == null) {
                otherCustomers.setInsuCustAccountNo(generateInsuAccountNo());
            } else {
                otherCustomers.setInsuCustAccountNo(otherCustomers.getInsuCustAccountNo());
            }
            otherCustomers.setIsActive(true);
            otherCustomers.setActionTime(date);
            otherCustomers.setUserId(userId);
            session.saveOrUpdate(otherCustomers);
            is_saved = true;
            if (is_saved) {
                Chartofaccount chartofaccount = (Chartofaccount) sessionFactory.getCurrentSession().createCriteria(Chartofaccount.class)
                        .add(Restrictions.eq("accountNo", otherCustomers.getInsuCustAccountNo())).uniqueResult();
                if (chartofaccount != null) {
                    chartofaccount.setAcSubCode(1);
                    chartofaccount.setAccountNo(otherCustomers.getInsuCustAccountNo());
                    chartofaccount.setAccountName(otherCustomers.getInsuFullName());
                    chartofaccount.setRepresentDept(1);
                    chartofaccount.setAccountType(1);
                    chartofaccount.setCreatedUserId(userId);
                    chartofaccount.setCurrDate(date);
                    session.saveOrUpdate(chartofaccount);
                } else {
                    Chartofaccount chartofaccount1 = new Chartofaccount();
                    chartofaccount1.setAcSubCode(1);
                    chartofaccount1.setAccountNo(otherCustomers.getInsuCustAccountNo());
                    chartofaccount1.setAccountName(otherCustomers.getInsuFullName());
                    chartofaccount1.setRepresentDept(1);
                    chartofaccount1.setAccountType(1);
                    chartofaccount1.setCreatedUserId(userId);
                    chartofaccount1.setCurrDate(date);
                    session.saveOrUpdate(chartofaccount1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        transaction.commit();
        session.flush();
        session.close();
        return is_saved;
    }

    @Override
    public List<InsuranceOtherCustomers> findOtherCustomers() {
        List<InsuranceOtherCustomers> customerses = null;
        try {
            customerses = sessionFactory.getCurrentSession().createCriteria(
                    InsuranceOtherCustomers.class).addOrder(Order.desc("insuCustomerId")).list();
        } catch (Exception e) {
            return null;
        }
        return customerses;
    }

    @Override
    public InsuranceOtherCustomers findInsuranceOtherCustomer(int Id) {
        InsuranceOtherCustomers otherCustomers = null;
        try {
            otherCustomers = (InsuranceOtherCustomers) sessionFactory.getCurrentSession().createCriteria(
                    InsuranceOtherCustomers.class).add(Restrictions.eq("insuCustomerId", Id)).uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return otherCustomers;
    }

    public String generateInsuAccountNo() {
        String AccountNo = "";
        try {
            String sql = "select max(insuCustAccountNo) from InsuranceOtherCustomers";
            AccountNo = "I";
            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            String maxNo = "0";
            if (ob != null) {
                maxNo = ob.toString().substring(1).replaceAll("^0+", "");
            }
            int mm = Integer.parseInt(maxNo) + 1;

            int noLength = 7 - String.valueOf(mm).length();
            for (int i = 0; i < noLength; i++) {
                AccountNo = AccountNo + "0";
            }

            if (noLength > 0) {
                AccountNo = AccountNo + mm;
            } else {
                AccountNo = AccountNo + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AccountNo;
    }

    @Override
    public List findOtherInsurance() {
        List<InsuraceProcessMain> otherInsu = null;
        try {
            String sql = "from InsuraceProcessMain where isOtherInsurance = 1 order by insuId desc";
            otherInsu = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return otherInsu;
    }

    @Override
    public InsuraceProcessMain findOtherInsurance(int insuId) {
        InsuraceProcessMain processMain = null;
        try {
            processMain = (InsuraceProcessMain) sessionFactory.getCurrentSession().createCriteria(InsuraceProcessMain.class)
                    .add(Restrictions.eq("insuId", insuId)).uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return processMain;
    }

    @Override
    public List<MBranch> findBranches() {
        List<MBranch> branchs = null;
        try {
            branchs = sessionFactory.getCurrentSession().createCriteria(MBranch.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchs;
    }

    @Override
    public Boolean registedInsurance(int insuId) {
        Boolean isRegisted = false;
        Session session = null;
        Transaction tx = null;
        Calendar c = Calendar.getInstance();
        Date registerDate = c.getTime();
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            InsuraceProcessMain ipm = (InsuraceProcessMain) session.createCriteria(InsuraceProcessMain.class)
                    .add(Restrictions.eq("insuId", insuId)).uniqueResult();
            if (ipm != null) {
                ipm.setInsuStatus(2); // add to payment
                ipm.setIsRegisted(true);
                ipm.setRegistedDate(registerDate);
                session.saveOrUpdate(ipm);
                tx.commit();
                isRegisted = true;
            }
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return isRegisted;
    }

    @Override
    public boolean generateInsuranceVoucher(Integer[] loans) {
        boolean isGenerate = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "update InsuraceProcessMain set insuStatus = :insuStatus where insuLoanId in (:insuLoanId)";
            session.createQuery(sql).setParameter("insuStatus", 3).setParameterList("insuLoanId", loans).executeUpdate();
            tx.commit();
            isGenerate = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return isGenerate;
    }

    @Override
    public List findRegistedLoan() {
        List registedList = null;
        try {
            String sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvh.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                    + "lh.debtorHeaderDetails.nameWithInitial,"
                    + "im.insuStatus,im.isCommRec from InsuraceProcessMain as im,"
                    + "LoanHeaderDetails as lh, LoanPropertyVehicleDetails as lpvh where im.insuLoanId = lh.loanId and lh.loanId = lpvh.loanId and im.isRegisted = 1 and lh.loanIsDelete = 0 and im.insuStatus >= 2 ORDER BY im.insuId DESC";
            registedList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
        return registedList;
    }

    @Override
    public Boolean saveDiliverType(int insuId, int diliverType) {
        Boolean isDiliver = false;
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        InsuraceProcessMain main = null;
        try {
            main = (InsuraceProcessMain) sessionFactory.getCurrentSession().createCriteria(InsuraceProcessMain.class)
                    .add(Restrictions.eq("insuId", insuId)).uniqueResult();
            main.setIsDiliver(diliverType);
            session.saveOrUpdate(main);
            isDiliver = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
            session.flush();
            t.commit();
        }catch(Exception e){
            t.rollback();
            e.printStackTrace();
        }
        session.close();
        return isDiliver;
    }

    @Override
    public InsuraceProcessMain findDiluverInsurance(int insuId) {
        InsuraceProcessMain processMain = null;
        try {
            processMain = (InsuraceProcessMain) sessionFactory.getCurrentSession().createCriteria(InsuraceProcessMain.class)
                    .add(Restrictions.eq("insuId", insuId)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processMain;
    }

    @Override
    public Date findDayEndDate() {
        Date date = null;
        int userId = userService.findByUserName();
        int userLogBranchId = settDao.getUserLogedBranch(userId);
        try {
            //SELECT * FROM day_end_date WHERE Branch_Id = 1 ORDER BY Day_End_Id DESC LIMIT 1
            String sql = "from DayEndDate where branchId =" + userLogBranchId + "order by dayEndId desc";
            DayEndDate dayEndDate = (DayEndDate) sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();
            date = dayEndDate.getDayEndDate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public InsuraceProcessMain findByLoanId(int loanId) {
        InsuraceProcessMain insuraceProcessMain = null;
        try {
            String sql = "from InsuraceProcessMain where insuLoanId = :insuLoanId";
            insuraceProcessMain = (InsuraceProcessMain) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("insuLoanId", loanId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return insuraceProcessMain;
    }

    @Override
    public boolean addInsuraceCommission(InsuraceProcessMain insurance) {
        boolean save = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            int userId = userService.findByUserName();
            int userLogBranchId = settDao.getUserLogedBranch(userId);
            insurance.setInsuUserId(userId);
            insurance.setBranchId(userLogBranchId);
            insurance.setActionTime(Calendar.getInstance().getTime());
            session.saveOrUpdate(insurance);
            tx.commit();
            save = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return save;
    }

    @Override
    public List findInsuranceByAgreementNo(String agreementNo, int status) {
        List processList = new ArrayList();
        try {
            String sql = "";
            if (status == 1) {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and lh.loanAgreementNo like '%" + agreementNo + "%' and lh.loanIsDelete = 0 and im.isRegisted = 0 and im.insuStatus = 1 order by im.insuId desc";
            } else {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial,im.insuStatus,im.isCommRec"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and lh.loanAgreementNo like '%" + agreementNo + "%' and lh.loanIsDelete = 0 and im.isRegisted = 1 and im.insuStatus != 1 order by im.insuId desc";
            }
            processList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return processList;
    }

    @Override
    public List findInsuranceByPolicyNo(String policyNo, int status) {
        List processList = new ArrayList();
        try {
            String sql = "";
            if (status == 1) {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and im.insuCoverNoteNo like '%" + policyNo + "%' and lh.loanIsDelete = 0 and im.isRegisted = 0 and im.insuStatus = 1 order by im.insuId desc";
            } else {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial,im.insuStatus,im.isCommRec"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and im.insuCoverNoteNo like '%" + policyNo + "%' and lh.loanIsDelete = 0 and im.isRegisted = 1 and im.insuStatus != 1 order by im.insuId desc";
            }
            processList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return processList;
    }

    @Override
    public List findInsuranceByVehicleNo(String vehicleNo, int status) {
        List processList = new ArrayList();
        try {
            String sql = "";
            if (status == 1) {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and lpvd.vehicleRegNo like '%" + vehicleNo + "%' and lh.loanIsDelete = 0 and im.isRegisted = 0 and im.insuStatus = 1 order by im.insuId desc";
            } else {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial,im.insuStatus,im.isCommRec"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and lpvd.vehicleRegNo like '%" + vehicleNo + "%' and lh.loanIsDelete = 0 and im.isRegisted = 1 and im.insuStatus != 1 order by im.insuId desc";
            }
            processList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return processList;
    }

    @Override
    public List findInsuranceByDebtorName(String debtorName, int status) {
        List processList = new ArrayList();
        try {
            String sql = "";
            if (status == 1) {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and lh.debtorHeaderDetails.debtorName like '%" + debtorName + "%' and lh.loanIsDelete = 0 and im.isRegisted = 0 and im.insuStatus = 1 order by im.insuId desc";
            } else {
                sql = "select im.insuId,im.insuLoanId,lh.loanAgreementNo,im.insuCoverNoteNo,lpvd.vehicleRegNo,lh.loanAmount,im.insuPremium,im.insuValue,"
                        + "lh.debtorHeaderDetails.nameWithInitial,im.insuStatus,im.isCommRec"
                        + " from InsuraceProcessMain as im, LoanPropertyVehicleDetails as lpvd,"
                        + "LoanHeaderDetails as lh where im.insuLoanId = lh.loanId and lh.loanId = lpvd.loanId and lh.debtorHeaderDetails.debtorName like '%" + debtorName + "%' and lh.loanIsDelete = 0 and im.isRegisted = 1 and im.insuStatus != 1 order by im.insuId desc";
            }
            processList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return processList;
    }

    @Override
    public Boolean saveInsuranceDebitProcess(InsuraceProcessMain insurance) {
        Boolean is_saved = true;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        String insCode = "";
        int insu = 0;
        SettlementMain settlementMain = null;
        int userId = userService.findByUserName();
        int userLogBranchId = settDao.getUserLogedBranch(userId);
        Date systemDate = settDao.getSystemDate(userLogBranchId);

        Calendar c = Calendar.getInstance();
        Date date = c.getTime();

        try {
            if (insurance.getInsuId() != null) {
                String sql = "select insuCode from InsuraceProcessMain where insuId =" + insurance.getInsuId();
                insCode = session.createQuery(sql).uniqueResult().toString();
            } else {
                //SELECT * FROM insurace_process_main ORDER BY INSU_CODE DESC LIMIT 1
                String sql1 = "from InsuraceProcessMain order by insuId desc";
                InsuraceProcessMain insuranceCode = (InsuraceProcessMain) session.createQuery(sql1).setMaxResults(1).uniqueResult();
                if (insuranceCode != null) {
                    insu = Integer.parseInt(insuranceCode.getInsuCode().substring(4));
                    insu++;
                } else {
                    insu = 1;
                }
            }
            //3'rd party insurance
            if (insurance.getOtherInsu() == 1) {
                insurance.setActionTime(date);
                insurance.setInsuUserId(userId);
                if (insurance.getInsuId() != null) {
                    insurance.setInsuCode(insCode);
                } else {
                    insurance.setInsuCode(insurancePaymentCode + insu);
                }
                if (insurance.getIsOtherCompany() == null) {
                    insurance.setIsOtherCompany(false);
                } else {
                    insurance.setIsOtherCompany(true);
                }
                insurance.setBranchId(userLogBranchId);
                insurance.setInsuPremium(insurance.getInsuTotalPremium());
                insurance.setInsuOtherCharge(0.00);
                insurance.setIsOtherInsurance(true);
                insurance.setIsRegisted(false);
                session.saveOrUpdate(insurance);
            } else {
                insurance.setActionTime(date);
                insurance.setInsuUserId(userId);
                if (insurance.getInsuId() != null) {
                    insurance.setInsuCode(insCode);
                    insurance.setRenewCount(insurance.getRenewCount());
                } else {
                    insurance.setInsuCode(insurancePaymentCode + insu);
                    List insuList = sessionFactory.getCurrentSession().createCriteria(InsuraceProcessMain.class)
                            .add(Restrictions.eq("insuLoanId", insurance.getInsuLoanId())).list();
                    if (insuList.size() > 0) {
                        //renew insurance
                        InsuraceProcessMain renewC = (InsuraceProcessMain) insuList.get(insuList.size() - 1);
                        insurance.setRenewCount(renewC.getRenewCount() + 1);
                    }
                }
                if (insurance.getIsOtherCompany() == null) {
                    insurance.setIsOtherCompany(false);
                } else {
                    insurance.setIsOtherCompany(true);
                }
                insurance.setBranchId(userLogBranchId);
                insurance.setIsOtherInsurance(false);
                insurance.setIsRegisted(false);
                insurance.setIsPay(false);
                session.saveOrUpdate(insurance);
            }
            int insuId = insurance.getInsuId();
            int insuLoanID = insurance.getInsuLoanId();

            String transactionNo = insurance.getInsuCode();
            Boolean isOtherInsurance = insurance.getIsOtherInsurance();

            if (isOtherInsurance) {
                String sql3 = "from SettlementMain where transactionNoDbt ='" + transactionNo + "'";
                settlementMain = (SettlementMain) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                if (settlementMain != null) {
                    settlementMain.setDebtorId(insurance.getInsuDebtorId());
                    settlementMain.setLoanId(0);
                    settlementMain.setDescription(insurancePayment);
                    settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                    settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                    settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                    settlementMain.setIsPosting(false);
                    settlementMain.setUserId(userId);
                    settlementMain.setBranchId(userLogBranchId);
                    settlementMain.setSystemDate(systemDate);
                    session.saveOrUpdate(settlementMain);
                } else {
                    settlementMain = new SettlementMain();
                    settlementMain.setDebtorId(insurance.getInsuDebtorId());
                    settlementMain.setLoanId(0);
                    settlementMain.setDescription(insurancePayment);
                    settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                    settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                    settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                    settlementMain.setIsPosting(false);
                    settlementMain.setUserId(userId);
                    settlementMain.setSystemDate(systemDate);
                    settlementMain.setBranchId(userLogBranchId);
                    session.saveOrUpdate(settlementMain);
                }
            } else // upadate LoanHeaderDetails
            if (insuId > 0) {
                String sql2 = "from LoanHeaderDetails where loanId =" + insuLoanID;
                LoanHeaderDetails headerDetails = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();
                headerDetails.setInsuranceStatus(1);
                session.saveOrUpdate(headerDetails);

                if (insurance.getIsOtherCompany() != true) {
                    // upadate SettlementMain
                    //SELECT * FROM settlement_main WHERE Transaction_No_Dbt = 'INSU1'
                    String sql3 = "from SettlementMain where transactionNoDbt ='" + transactionNo + "'";
                    settlementMain = (SettlementMain) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                    if (settlementMain != null) {
                        settlementMain.setDebtorId(insurance.getInsuDebtorId());
                        settlementMain.setLoanId(insurance.getInsuLoanId());
                        settlementMain.setDescription(insurancePayment);
                        settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                        settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                        settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                        settlementMain.setIsPosting(false);
                        settlementMain.setUserId(userId);
                        settlementMain.setSystemDate(systemDate);
                        settlementMain.setBranchId(userLogBranchId);
                        session.saveOrUpdate(settlementMain);
                    } else {
                        settlementMain = new SettlementMain();
                        settlementMain.setDebtorId(insurance.getInsuDebtorId());
                        settlementMain.setLoanId(insurance.getInsuLoanId());
                        settlementMain.setDescription(insurancePayment);
                        settlementMain.setTransactionCodeDbt(insurancePaymentCode);
                        settlementMain.setTransactionNoDbt(insurance.getInsuCode());
                        settlementMain.setDbtAmount(Double.parseDouble(insurance.getInsuTotalPremium().toString()));
                        settlementMain.setIsPosting(false);
                        settlementMain.setUserId(userId);
                        settlementMain.setSystemDate(systemDate);
                        settlementMain.setBranchId(userLogBranchId);
                        session.saveOrUpdate(settlementMain);
                    }

                } else {
                    String sql = "from SettlementMain where transactionNoDbt = '" + transactionNo + "'";
                    settlementMain = (SettlementMain) session.createQuery(sql).uniqueResult();
                    if (settlementMain != null) {
                        session.delete(settlementMain);
                    }
                }
            }
        } catch (Exception e) {
            is_saved = false;
        }
        transaction.commit();
        session.flush();
        session.close();
        return is_saved;
    }

    @Override
    public Boolean saveOrUpdateInsuranceProcessDelete(InsuraceProcessMain insuraceProcessMain) {
        Boolean is_saved = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        InsuranceProcessDelete insuranceProcessDelete = null;

        if (insuraceProcessMain.getInsuId() != null) {
            try {
                Calendar calendar = Calendar.getInstance();
                Date date = calendar.getTime();
                int userId = userService.findByUserName();

                insuranceProcessDelete = new InsuranceProcessDelete();

                insuranceProcessDelete.setInsuId(insuraceProcessMain.getInsuId());
                insuranceProcessDelete.setInsuCode(insuraceProcessMain.getInsuCode());
                insuranceProcessDelete.setInsuLoanId(insuraceProcessMain.getInsuLoanId());
                insuranceProcessDelete.setInsuCoverNoteNo(insuraceProcessMain.getInsuCoverNoteNo());
                insuranceProcessDelete.setInsuValue(insuraceProcessMain.getInsuValue());
                insuranceProcessDelete.setInsuDebtorId(insuraceProcessMain.getInsuDebtorId());
                insuranceProcessDelete.setInsuLoanType(insuraceProcessMain.getInsuLoanType());
                insuranceProcessDelete.setInsuCompanyId(insuraceProcessMain.getInsuCompanyId());
                insuranceProcessDelete.setInsuBasicPremium(insuraceProcessMain.getInsuBasicPremium());
                insuranceProcessDelete.setInsuSrcc(insuraceProcessMain.getInsuSrcc());
                insuranceProcessDelete.setInsuPremium(insuraceProcessMain.getInsuPremium());
                insuranceProcessDelete.setInsuOtherCharge(insuraceProcessMain.getInsuOtherCharge());
                insuranceProcessDelete.setInsuTotalPremium(insuraceProcessMain.getInsuTotalPremium());
                insuranceProcessDelete.setInsuCommissionRecDate(insuraceProcessMain.getInsuCommissionRecDate());
                insuranceProcessDelete.setInsuCommissionCheqNo(insuraceProcessMain.getInsuCommissionCheqNo());
                insuranceProcessDelete.setInsuCommissionRecAmount(insuraceProcessMain.getInsuCommissionRecAmount());
                insuranceProcessDelete.setInsuCommissionDiffer(insuraceProcessMain.getInsuCommissionDiffer());
                insuranceProcessDelete.setInsuGracePeriod(insuraceProcessMain.getInsuGracePeriod());
                insuranceProcessDelete.setInsuUserId(insuraceProcessMain.getInsuUserId());
                insuranceProcessDelete.setIsOtherInsurance(insuraceProcessMain.getIsOtherInsurance());
                insuranceProcessDelete.setIsOtherCompany(insuraceProcessMain.getIsOtherCompany());
                insuranceProcessDelete.setVehicleNo(insuraceProcessMain.getVehicleNo());
                insuranceProcessDelete.setIsRegisted(insuraceProcessMain.getIsRegisted());
                insuranceProcessDelete.setIsDiliver(insuraceProcessMain.getIsDiliver());
                insuranceProcessDelete.setIsCommRec(insuraceProcessMain.getIsCommRec());
                insuranceProcessDelete.setInsuStatus(insuraceProcessMain.getInsuStatus());
                insuranceProcessDelete.setRegistedDate(insuraceProcessMain.getRegistedDate());
                insuranceProcessDelete.setOtherComName(insuraceProcessMain.getOtherComName());
                insuranceProcessDelete.setExpiryDate(insuraceProcessMain.getExpiryDate());
                insuranceProcessDelete.setIsPay(insuraceProcessMain.getIsPay());
                insuranceProcessDelete.setBranchId(insuraceProcessMain.getBranchId());
                insuranceProcessDelete.setRenewCount(insuraceProcessMain.getRenewCount());

                insuranceProcessDelete.setUserId(userId);
                insuranceProcessDelete.setActionTime(date);

                session.saveOrUpdate(insuranceProcessDelete);
                transaction.commit();
                is_saved = true;
            } catch (Exception e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                e.printStackTrace();
            } finally {
                session.flush();
                session.close();
            }
        }
        return is_saved;
    }

    @Override
    public Boolean deleteInsuranceProcessMainByInsuId(int insuId, String insuCode, int loanId) {
        Boolean is_delete = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            if (loanId > 0) {
                String sql1 = "FROM LoanHeaderDetails WHERE loanId = :loanId";
                List<LoanHeaderDetails> list = session.createQuery(sql1).setParameter("loanId", loanId).list();
                if (list.size() > 0) {
                    for (LoanHeaderDetails cheklist : list) {
                        if (cheklist.getLoanSpec() == 0 && cheklist.getLoanIsIssue() == 0 || cheklist.getLoanSpec() == null && cheklist.getLoanIsIssue() == null) {
                            if (insuId != 0 && insuCode != null) {
                                String sql2 = "delete from InsuraceProcessMain where insuId = :insuId and insuCode = :insuCode";
                                session.createQuery(sql2).setParameter("insuId", insuId).setParameter("insuCode", insuCode)
                                        .executeUpdate();

                                String sql3 = "delete from SettlementMain where transactionNoDbt = :transactionNoDbt";
                                session.createQuery(sql3).setParameter("transactionNoDbt", insuCode).executeUpdate();
                                transaction.commit();
                                is_delete = true;
                            }
                        } else {
                            is_delete = false;
                        }
                    }
                }
            }

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return is_delete;
    }

}

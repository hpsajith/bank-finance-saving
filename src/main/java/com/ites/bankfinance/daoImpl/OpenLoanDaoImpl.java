/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.OpenLoanForm;
import com.ites.bankfinance.dao.OpenLoanDao;
import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.LoanGuaranteeDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.OpenD;
import com.ites.bankfinance.model.OpenH;
import com.ites.bankfinance.model.Posting;
import com.ites.bankfinance.model.SettlementMain;
import com.ites.bankfinance.model.UmUserLog;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ites
 */
@Repository
public class OpenLoanDaoImpl implements OpenLoanDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDao userDao;

    Calendar cal = Calendar.getInstance();
    int userId;

    @Override
    public void addOpenLoan(OpenLoanForm formData) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        userId = userDao.findByUserName();

        //debtor
        int debtorId = 0;
        String debtorName = "";
        String debtorNic = "";
        String debtorAddress = "";
        int debTele = 0;
        int debMobi = 0;
        int debOff = 0;
        
        if(!formData.getDebtorName().equals(null) && !formData.getDebtorName().equals("")){
            debtorName = formData.getDebtorName();
        }
        if(!formData.getDebtorNic().equals(null) && !formData.getDebtorNic().equals("")){
            debtorNic = formData.getDebtorNic();
        }
        if(!formData.getDebtorAdress().equals(null) && !formData.getDebtorAdress().equals("")){
            debtorAddress = formData.getDebtorAdress();
        }
        if(formData.getDebtorTele1()!=null){
            debMobi = formData.getDebtorTele1();
        }
        if(formData.getDebtorTele2()!=null){
            debTele = formData.getDebtorTele2();
        }
        if(formData.getDebtorTele3()!=null){
            debOff = formData.getDebtorTele3();
        }
        

        debtorId = saveCustomer(1, debtorName, debtorNic, debtorAddress, debTele, debMobi, debOff);

        //gurantor
        int gurantorId = 0, gurantorId2 = 0;

        String guarantorName = "", guarantorName2 = "";
        String guarantorNic = "", guarantorNic2 = "";
        String guarantorAddress = "", guarantorAddress2 = "";
        int guarantorTele = 0, guarantorTele2 = 0;
        int guarantorMobi = 0, guarantorMobi2 = 0;
        int guarantorOff = 0, guarantorOff2 = 0;

        if (formData.getGurantor1Name() != null) {
            guarantorName = formData.getGurantor1Name();
        }
        if (formData.getGurantor1Nic() != null) {
            guarantorNic = formData.getGurantor1Nic();
        }
        if (formData.getGurantor1Adress() != null) {
            guarantorAddress = formData.getGurantor1Adress();
        }
        if (formData.getGurantor1Tele1() != null) {
            guarantorMobi = formData.getGurantor1Tele1();
        }
        if (formData.getGurantor1Tele2() != null) {
            guarantorTele = formData.getGurantor1Tele2();
        }
        if (formData.getGurantor1Tele3() != null) {
            guarantorOff = formData.getGurantor1Tele3();
        }

        if (formData.getGurantor2Name() != null) {
            guarantorName2 = formData.getGurantor2Name();
        }
        if (formData.getGurantor2Nic() != null) {
            guarantorNic2 = formData.getGurantor2Nic();
        }
        if (formData.getGurantor2Adress() != null) {
            guarantorAddress2 = formData.getGurantor2Adress();
        }
        if (formData.getGurantor2Tele1() != null) {
            guarantorMobi2 = formData.getGurantor2Tele1();
        }
        if (formData.getGurantor2Tele2() != null) {
            guarantorTele2 = formData.getGurantor2Tele2();
        }
        if (formData.getGurantor2Tele3() != null) {
            guarantorOff2 = formData.getGurantor2Tele3();
        }

        //add gurantor 1
        if (!guarantorName.equals("")) {
            gurantorId = saveCustomer(0, guarantorName, guarantorNic, guarantorAddress, guarantorTele, guarantorMobi, guarantorOff);
        }

        //add gurantor 2
        if (!guarantorName2.equals("")) {
            gurantorId2 = saveCustomer(0, guarantorName2, guarantorNic2, guarantorAddress2, guarantorTele2, guarantorMobi2, guarantorOff2);
        }

        String inputPattern = "EEE MMM d HH:mm:ss zzz yyyy";
        String outputPattern = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        try {
            //save loan header details
            double rate = 0.00, installment = 0.00, interest = 0.00, downPayment = 0.00, investment = 0.00;
            BigDecimal loanAmount = null;
            double l_amount = 0.00;
            int period = 0, dueDay = 0;
            int loanType = 0; //loan type-NDC old
            int periodType = 0; //period type- daily
            Date laps = null, dueDate = null, startDate = null, enterDate = null;
            String bookNo = "";
            int loan_spec = 0;
            int branchId = 1;
            boolean hasInstallment = true;

            branchId = formData.getBranch();
            periodType = formData.getPeriodType();
            loanType = formData.getLoanType();

            //loan spec
            if (periodType == 5) {
                loan_spec = 1;
            } else if (periodType == 6) {
                loan_spec = 2;
            } else if (periodType == 7) {
                loan_spec = 3;
            } else if (periodType == 8) {
                loan_spec = 4;
            } else {
                loan_spec = 0;
            }

            if (formData.getLoanAmount() != null) {
                loanAmount = new BigDecimal(formData.getLoanAmount());
            }
            if (formData.getDownPayment() != null) {
                downPayment = formData.getDownPayment();
            }
            if (formData.getInvestment() != null) {
                investment = formData.getInvestment();
            }
            if (formData.getInstallment() != null) {
                installment = formData.getInstallment();
            }
            if (formData.getRate() != null) {
                rate = formData.getRate();
            }
            if (formData.getMonthInterest() != null) {
                interest = formData.getMonthInterest();
            }
            if (formData.getNoOfInstallment() != 0) {
                period = formData.getNoOfInstallment();
            }
            if (formData.getBookNo() != null) {
                bookNo = formData.getBookNo();
            }

            String du = "";
            if (formData.getLapsDate() != null) {
                laps = formData.getLapsDate();
            }

            if (laps != null) {
                if (period == 90) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(laps);
                    c.add(Calendar.DATE, -period);
                    startDate = new Date(c.getTime().getTime());

                    Calendar c2 = Calendar.getInstance();
                    c2.setTime(startDate);
                    c2.add(Calendar.DATE, 1);
                    dueDate = new Date(c2.getTime().getTime());

                } else {
                    Calendar c = Calendar.getInstance();
                    c.setTime(laps);
                    c.add(Calendar.MONTH, -period);
                    startDate = new Date(c.getTime().getTime());

                    Calendar c2 = Calendar.getInstance();
                    c2.setTime(startDate);
                    c2.add(Calendar.MONTH, 1);
                    dueDate = new Date(c2.getTime().getTime());
                }
            }

            if (formData.getDueDate() != 0) {
                dueDay = formData.getDueDate();
            }

            String today = outputFormat.format(cal.getTime());

            //generate agreement no
            String agrementNo = createLoanAgreementNo(loanType);

            String sqld = "from DebtorHeaderDetails where debtorId=" + debtorId;
            DebtorHeaderDetails loanDeb = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sqld).uniqueResult();
            String dedtorAcc = loanDeb.getDebtorAccountNo();
            
            LoanHeaderDetails newLoan = new LoanHeaderDetails();
            newLoan.setDebtorHeaderDetails(loanDeb);
            newLoan.setLoanAmount(loanAmount);
            newLoan.setLoanDownPayment(downPayment);
            newLoan.setLoanInvestment(investment);
            newLoan.setLoanInterestRate(rate);
            newLoan.setLoanInstallment(installment);
            newLoan.setLoanInterest(interest);
            newLoan.setLoanType(loanType);
            newLoan.setDueDay(dueDay);
            newLoan.setLoanAgreementNo(agrementNo);
            newLoan.setLoanBookNo(bookNo);
            newLoan.setLoanStartDate(startDate);
            newLoan.setLoanDate(outputFormat.parse(today));
            newLoan.setUserId(userId);
            newLoan.setBranchId(branchId);
            newLoan.setActionTime(cal.getTime());
            newLoan.setLoanSpec(loan_spec);
            newLoan.setLoanPeriod(period);
            newLoan.setLoanPeriodType(periodType);
            sessionFactory.getCurrentSession().save(newLoan);
            int loanId = newLoan.getLoanId();

            if (loanId > 0) {
                if (gurantorId > 0) {
                    saveLoanGuarantor(loanId, gurantorId, 1);
                }
                if (gurantorId2 > 0) {
                    saveLoanGuarantor(loanId, gurantorId2, 2);
                }
                if (loan_spec == 0) {
                    saveLoanInstallment(formData, loanId);
                }
                saveOpenBalances(formData, loanId);
                posting(formData, loanId, debtorId, dedtorAcc); 
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            tx.commit();
            session.flush();
            session.clear();
            session.close();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public int saveCustomer(int db, String guarantorName, String guarantorNic, String guarantorAddress, int guarantorTele, int guarantorMobi, int guarantorOff) {
        int gurantorId = 0;
        try {

            if (!guarantorNic.equals("")) {
                String sql1 = "from DebtorHeaderDetails where debtorNic='" + guarantorNic + "'";
                List<DebtorHeaderDetails> debtors = sessionFactory.getCurrentSession().createQuery(sql1).list();
                if (debtors.size() > 0) {
                    gurantorId = debtors.get(0).getDebtorId();
                    if (db == 1) {
                        DebtorHeaderDetails updateDebtor = debtors.get(0);
                        if (!updateDebtor.getDebtorIsDebtor()) {
                            String accNo = generateDebtorAccount();
                            updateDebtor.setDebtorAccountNo(accNo);
                            updateDebtor.setDebtorIsDebtor(true);
                            sessionFactory.getCurrentSession().update(updateDebtor);
                        }
                    }
                }
            }
            if (gurantorId == 0) {
                DebtorHeaderDetails newDebtor = new DebtorHeaderDetails();
                newDebtor.setNameWithInitial(guarantorName);
                newDebtor.setDebtorName(guarantorName);
                newDebtor.setDebtorNic(guarantorNic);
                newDebtor.setDebtorPersonalAddress(guarantorAddress);
                newDebtor.setDebtorTelephoneHome(String.valueOf(guarantorTele));
                newDebtor.setDebtorTelephoneMobile(String.valueOf(guarantorMobi));
                newDebtor.setDebtorTelephoneWork(String.valueOf(guarantorOff));
                if (db == 0) {
                    newDebtor.setDebtorIsDebtor(false);
                } else {
                    newDebtor.setDebtorIsDebtor(true);
                    String accNo = generateDebtorAccount();
                    newDebtor.setDebtorAccountNo(accNo);
                }
                newDebtor.setUserId(userId);
                newDebtor.setBranchId(1);
                newDebtor.setActionTime(cal.getTime());
                sessionFactory.getCurrentSession().save(newDebtor);
                gurantorId = newDebtor.getDebtorId();
            }
        } catch (Exception e) {
            System.out.println("Exception save debtor");
            e.printStackTrace();
        }
        return gurantorId;
    }

    public String generateDebtorAccount() {

        String AccountNo = "";
        try {
            String sql = "select max(debtorAccountNo) from DebtorHeaderDetails";
            AccountNo = "D";
            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            String maxNo = "0";
            if (ob != null) {
                maxNo = ob.toString().substring(1).replaceAll("^0+", "");
            }
            int mm = Integer.parseInt(maxNo) + 1;

            int noLength = 7 - String.valueOf(mm).length();
            for (int i = 0; i < noLength; i++) {
                AccountNo = AccountNo + "0";
            }

            if (noLength > 0) {
                AccountNo = AccountNo + mm;
            } else {
                AccountNo = AccountNo + 1;
            }
        } catch (Exception e) {
            System.out.println("Loan Save Fail");
            e.printStackTrace();
        }
        return AccountNo;
    }

    public void saveLoanGuarantor(int loanId, int gaId, int order) {
        try {
            LoanGuaranteeDetails guarantor = new LoanGuaranteeDetails();
            guarantor.setLoanId(loanId);
            guarantor.setDebtorId(gaId);
            guarantor.setGurantorOrder(order);
            guarantor.setActionTime(cal.getTime());
            guarantor.setUserId(userId);
            sessionFactory.getCurrentSession().save(guarantor);

        } catch (Exception e) {
            System.out.println("Gurantor " + order + " Save Fail");
            e.printStackTrace();
        }

    }

    public String createLoanAgreementNo(int loanType) {
        String agreementNo = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yy");
            String formattedYear = sdf.format(Calendar.getInstance().getTime());

            String sql1 = "select m.subLoanCode from MSubLoanType m where m.subLoanId=" + loanType;
            String code = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult().toString();
            agreementNo = agreementNo + code + "/";
            String sql2 = "SELECT  MAX(loanAgreementNo) AS maxx FROM LoanHeaderDetails WHERE loanAgreementNo LIKE '" + code + "/%' AND loanAgreementNo LIKE '%" + formattedYear + "'";
            Object mx = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            String maxNo = "";
            if (mx != null) {
                maxNo = mx.toString();
//                    String[] splitMax = maxNo.replaceAll("(?<=\\p{L})(?=\\d)", ":").split("(?<=\\d)(?=\\p{L})");
                String[] splitMax = maxNo.split("/");
                if (splitMax.length > 0) {
                    maxNo = splitMax[1];
                }
            }
            int a_no = 1;
            if (maxNo != null && maxNo != "") {
                String maxNo2 = maxNo.replaceAll("^0+", "");
                a_no = Integer.parseInt(maxNo2) + 1;
            }
            int lengthOfCode = 6 - String.valueOf(a_no).length();
            for (int i = 0; i < lengthOfCode; i++) {
                agreementNo = agreementNo + "0";
            }
            agreementNo = agreementNo + String.valueOf(a_no);
            agreementNo = agreementNo + "/" + formattedYear;
        } catch (Exception e) {
            System.out.println("Agreement no create fail");
            e.printStackTrace();
        }
        return agreementNo;
    }

    public void saveOpenBalances(OpenLoanForm formData, int loanId) {
        int paid_instlmnt = 0, remain_instlmnt = 0;
        double balance_interest = 0.00, total_receivble = 0.00, total_payment = 0.00, stock_balance = 0.00;
        double arres_out_odi = 0.00, total_arres = 0.00, over_payment = 0.00;
        double arres_instmnt = 0;
        try {

            paid_instlmnt = formData.getPaidInstallment();

            remain_instlmnt = formData.getRemainInstallment();

            if (formData.getSuspendInterest() != null) {
                balance_interest = formData.getSuspendInterest();
            }
            if (formData.getTotalReceible() != null) {
                total_receivble = formData.getTotalReceible();
            }
            if (formData.getTotalPyaments() != null) {
                total_payment = formData.getTotalPyaments();
            }
            if (formData.getStockbalance() != null) {
                stock_balance = formData.getStockbalance();
            }
            if (formData.getArresWithoutOdi() != null) {
                arres_out_odi = formData.getArresWithoutOdi();
            }
            if (formData.getOverPayment() != null) {
                over_payment = formData.getOverPayment();
            }

            arres_instmnt = formData.getAreasInstallment();

            if (formData.getTotalArresrs() != null) {
                total_arres = formData.getTotalArresrs();
            }

            String sql = "INSERT INTO open_h (Loan_Id,Paid_Installment,Remain_Installment,Suspend_Interest,Total_Receivble,Total_Payment,Stock_Balance,Arreas_Without_Odi,Over_Payment,Arreas_Installment,Arreas_Total,User_Id,Action_Time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            OpenH openh = new OpenH();
            openh.setLoanId(loanId);
            openh.setPaidInstallment(paid_instlmnt);
            openh.setRemainInstallment(remain_instlmnt);
            openh.setSuspendInterest(balance_interest);
            openh.setTotalReceivble(total_receivble);
            openh.setTotalPayment(total_payment);
            openh.setStockBalance(stock_balance);
            openh.setArreasWithoutOdi(arres_out_odi);
            openh.setArreasInstallment(arres_instmnt);
            openh.setArreasTotal(total_arres);
            openh.setUserId(userId);
            openh.setActionTime(cal.getTime());
            sessionFactory.getCurrentSession().save(openh);

            int openId = openh.getOpenId();

            double a_insurance = 0.00, a_odi = 0.00, a_regis = 0.00, a_seiz = 0.00, a_recov = 0.00;
            double a_letter = 0.00, a_valuation = 0.00, a_bank = 0.00, a_cheq = 0.00;
            double total_charge = 0.00;

            if (formData.getAresInsurance() != null) {
                a_insurance = formData.getAresInsurance();
                if (a_insurance > 0) {
                    saveOpend(a_insurance, openId, 1);
                    total_charge = total_charge + a_insurance;
                }
            }
            if (formData.getAresOdi() != null) {
                a_odi = formData.getAresOdi();
                if (a_odi > 0) {
                    saveOpend(a_odi, openId, 2);
                    total_charge = total_charge + a_odi;
                }
            }
            if (formData.getAresRegisraion() != null) {
                a_regis = formData.getAresRegisraion();
                if (a_regis > 0) {
                    saveOpend(a_regis, openId, 3);
                    total_charge = total_charge + a_regis;
                }
            }
            if (formData.getAresSeizing() != null) {
                a_seiz = formData.getAresSeizing();
                if (a_seiz > 0) {
                    saveOpend(a_seiz, openId, 4);
                    total_charge = total_charge + a_seiz;
                }
            }
            if (formData.getAresRecovery() != null) {
                a_recov = formData.getAresRecovery();
                if (a_recov > 0) {
                    saveOpend(a_recov, openId, 5);
                    total_charge = total_charge + a_recov;
                }
            }
            if (formData.getAresLetter() != null) {
                a_letter = formData.getAresLetter();
                if (a_letter > 0) {
                    saveOpend(a_letter, openId, 6);
                    total_charge = total_charge + a_letter;
                }
            }
            if (formData.getAresValuation() != null) {
                a_valuation = formData.getAresValuation();
                if (a_valuation > 0) {
                    saveOpend(a_valuation, openId, 7);
                    total_charge = total_charge + a_valuation;
                }
            }
            if (formData.getAresBank() != null) {
                a_bank = formData.getAresBank();
                if (a_bank > 0) {
                    saveOpend(a_bank, openId, 8);
                    total_charge = total_charge + a_bank;
                }
            }
            if (formData.getAresCheq() != null) {
                a_cheq = formData.getAresCheq();
                if (a_cheq > 0) {
                    saveOpend(a_cheq, openId, 9);
                    total_charge = total_charge + a_cheq;
                }
            }

            if (total_charge == 0.00) {
                if (arres_out_odi == 0.00) {
                    if (total_arres > 0) {
                        openh.setArreasTotal(total_arres);
                        sessionFactory.getCurrentSession().update(openh);
                    }
                }
            }

            //posting
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void posting(OpenLoanForm formData, int loanId, int debtorId, String dedtorAcc) {

        int remain_instlmnt = formData.getRemainInstallment();
        double installment = 0.00;
        if (formData.getInstallment() != null) {
            installment = formData.getInstallment();
        }

        double monthInterest = 0.00;
        if (formData.getMonthInterest() != null) {
            monthInterest = formData.getMonthInterest();
        }

        double suspendInterest = 0.00;
        if (formData.getSuspendInterest() != null) {
            suspendInterest = formData.getSuspendInterest();
        }

        double overPayment = 0.00;
        if (formData.getOverPayment() != null) {
            overPayment = formData.getOverPayment();
        }

        double totalAreas = 0.00;
        int arrId = 0;
        String sqlaa = "from OpenH where loanId=" + loanId;
        OpenH oh = (OpenH) sessionFactory.getCurrentSession().createQuery(sqlaa).uniqueResult();
        if (oh != null) {
            totalAreas = oh.getArreasTotal();
            arrId = oh.getOpenId();
        }

        int loanType = formData.getLoanType();
        double capital = installment - monthInterest;
        double remainBalance = 0.00;
        if (remain_instlmnt > 0) {
            remainBalance = capital * remain_instlmnt;
        }
        String susAccNo = findLoanAccountNo(loanType, "INTRS");
        String cashAccNo = "460002";

        String refNo = generateReferencNo(loanId);

        //suspend balance
        int settId = addToSettlement(debtorId, loanId, 2, "INTRS", suspendInterest, "open Balanace - Suspend ");
        String ref_sus = refNo + "/" + settId;
        addToPosting(new BigDecimal(suspendInterest), susAccNo, ref_sus, 1, "Open Balance - Suspend");
        addToPosting(new BigDecimal(suspendInterest), dedtorAcc, ref_sus, 2, "Open Balance - Suspend");

        //arears balance
        String ref_arears = refNo + "/" + arrId;
        addToPosting(new BigDecimal(totalAreas), cashAccNo, ref_arears, 1, "Open Balance - Arears");
        addToPosting(new BigDecimal(totalAreas), dedtorAcc, ref_arears, 2, "Open Balance - Arears");

        //loan balance                
        if (remainBalance > 0) {
            int setId2 = addToSettlement(debtorId, loanId, 2, "LOAN", remainBalance, "open Balanace - Loan ");
            String ref_loan = refNo + "/" + setId2;
            addToPosting(new BigDecimal(remainBalance), cashAccNo, ref_loan, 1, "Open Balance - loan");
            addToPosting(new BigDecimal(remainBalance), dedtorAcc, ref_loan, 2, "Open Balance - loan");
        }

        //over payment
        if (overPayment > 0) {
            double over_charg = Math.abs(overPayment);
            int setId = addToSettlement(debtorId, loanId, 1, "OVP", over_charg, "Open Balance-Over Payment");
            String refNo_open = refNo + setId;
            addToPosting(new BigDecimal(over_charg), dedtorAcc, refNo_open, 1, "Open Balance - Over Payment");
            addToPosting(new BigDecimal(over_charg), cashAccNo, refNo_open, 2, "Open Balance - Over Payment");
        }
    }

    public void saveOpend(double amount, int openId, int d_type) {
        try {
            OpenD opend = new OpenD();
            opend.setOpenId(openId);
            opend.setAmount(amount);
            opend.setArreasTypeId(d_type);
            opend.setUserId(userId);
            opend.setActionTime(cal.getTime());
            sessionFactory.getCurrentSession().save(opend);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void saveLoanInstallment(OpenLoanForm formData, int loanId) {
        try {

            double installment = 0.00, investment = 0.00, downPayment = 0.00, interest = 0.00;
            int paid_instlmnt = 0, remain_instlmnt = 0, period = 0;
            BigDecimal loanAmount = null;
            if (formData.getInstallment() != null) {
                installment = formData.getInstallment();
            }

            Double roundedValue = (Double) (Math.ceil(installment / 5d) * 5);

            if (formData.getLoanAmount() != null) {
                loanAmount = new BigDecimal(formData.getLoanAmount());
            }
            if (formData.getDownPayment() != null) {
                downPayment = formData.getDownPayment();
            }
            if (formData.getInvestment() != null) {
                investment = formData.getInvestment();
            }
            if (formData.getInstallment() != null) {
                installment = formData.getInstallment();
            }
            if (formData.getMonthInterest() != null) {
                interest = formData.getMonthInterest();
            }

            paid_instlmnt = formData.getPaidInstallment();
            remain_instlmnt = formData.getRemainInstallment();

            double principle_amount = installment - interest;
            Date endDate = null;
            String outputPattern = "yyyy-MM-dd";

            SimpleDateFormat sdf = new SimpleDateFormat(outputPattern);

            if (formData.getLapsDate() != null) {
                endDate = formData.getLapsDate();
            }

            period = formData.getNoOfInstallment();

            Calendar cald = Calendar.getInstance();
            int noOfInstallmnt = 0;
            if (period == 90) {
                cald.setTime(endDate);
                cald.add(Calendar.DATE, -remain_instlmnt);

                int module = remain_instlmnt % 7;

                noOfInstallmnt = remain_instlmnt / 7;

                double installment_0 = installment * 7;
                double interest_0 = interest * 7;
                double principle_amount_0 = principle_amount * 7;
                String paymentDate;
                for (int j = 1; j <= noOfInstallmnt; j++) {

                    cald.add(Calendar.DATE, 7);

                    paymentDate = sdf.format(cald.getTime());
                    Date dueDate = new java.sql.Date(sdf.parse(paymentDate).getTime());

                    LoanInstallment instl = new LoanInstallment();
                    instl.setInsPrinciple(principle_amount_0);
                    instl.setInsInterest(interest_0);
                    instl.setInsAmount(installment_0);
                    instl.setInsRoundAmount(installment_0);
                    instl.setLoanId(loanId);
                    instl.setInsDescription("Installment");
                    instl.setInsStatus(0);
                    instl.setDueDate(dueDate);
                    instl.setActionTime(cal.getTime());
                    sessionFactory.getCurrentSession().save(instl);
                }

                if (module > 0) {
                    double interst_1 = interest * module;
                    double installment_1 = installment * module;
                    double principle_amount_1 = principle_amount * module;
                    cald.add(Calendar.DATE, 7);

                    paymentDate = sdf.format(cald.getTime());
                    Date dueDate = new java.sql.Date(sdf.parse(paymentDate).getTime());

                    LoanInstallment instl = new LoanInstallment();
                    instl.setInsPrinciple(principle_amount_1);
                    instl.setInsInterest(interst_1);
                    instl.setInsAmount(installment_1);
                    instl.setInsRoundAmount(installment_1);
                    instl.setLoanId(loanId);
                    instl.setInsDescription("Installment");
                    instl.setInsStatus(0);
                    instl.setDueDate(dueDate);
                    instl.setActionTime(cal.getTime());
                    sessionFactory.getCurrentSession().save(instl);
                }

            } else {
                cald.setTime(endDate);
                cald.add(Calendar.MONTH, -remain_instlmnt);
                String paymentDate;
                for (int i = 1; i <= remain_instlmnt; i++) {
                    cald.add(Calendar.MONTH, 1);
                    paymentDate = sdf.format(cald.getTime());
                    Date dueDate = new java.sql.Date(sdf.parse(paymentDate).getTime());

                    LoanInstallment instl = new LoanInstallment();
                    instl.setInsPrinciple(principle_amount);
                    instl.setInsInterest(interest);
                    instl.setInsAmount(installment);
                    instl.setInsRoundAmount(installment);
                    instl.setLoanId(loanId);
                    instl.setInsDescription("Installment");
                    instl.setInsStatus(0);
                    instl.setDueDate(dueDate);
                    instl.setActionTime(cal.getTime());
                    sessionFactory.getCurrentSession().save(instl);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //find loan acocunt no
    public String findLoanAccountNo(int loanType, String accType) {
        String accNo = "";
        try {
            String sql = "select accountNo From ConfigChartofaccountLoan where subLoanType=" + loanType + " and accountType='" + accType + "'";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                accNo = obj.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accNo;
    }

    //posting data
    public void addToPosting(BigDecimal amount, String accountNo, String referenceNo, int cd, String desc) {
        int user_id = userDao.findByUserName();
        int branchId = getUserLogedBranch(userId);
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        Date dayEndDate = null;
        try {
            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                dayEndDate = obj;
            }

            Posting posting = new Posting();
            posting.setReferenceNo(referenceNo);
            posting.setAccountNo(accountNo);
            posting.setCreditDebit(cd);
            if (cd == 1) {
                posting.setCAmount(amount);
                posting.setDAmount(new BigDecimal(0.00));
            } else {
                posting.setDAmount(amount);
                posting.setCAmount(new BigDecimal(0.00));
            }
            posting.setAmount(amount);
            posting.setUserId(user_id);
            posting.setCurrDateTime(action_time);
            posting.setBranchId(branchId);
            posting.setAllocation(0);
            posting.setPostingFrom(1);
            posting.setPostingState(1);
            posting.setDate(dayEndDate);
            posting.setDescription("posting from Vijitha Finance");
            sessionFactory.getCurrentSession().save(posting);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //create refference no for posting
    public String generateReferencNo(int loanId) {
        try {
            Date action_time = Calendar.getInstance().getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String referenceNo = "";
            String datePart = "";
            String c_date = dateFormat.format(action_time);
            String splitDate[] = c_date.split("-");
            if (splitDate.length > 0) {
                for (int n = 0; n < splitDate.length; n++) {
                    datePart = datePart + splitDate[n];
                }
            }
            referenceNo = referenceNo + datePart + loanId;
            return referenceNo;
        } catch (Exception e) {
            return "";
        }

    }

    public int addToSettlement(int debtorId, int loanId, int cd, String code, double amount, String desc) {
        int settId = 0;
        Date systemDate = null;
        int branchId = getUserLogedBranch(userId);
        try {
            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                systemDate = obj;
            }

            SettlementMain settMain = new SettlementMain();
            settMain.setLoanId(loanId);
            settMain.setDebtorId(debtorId);
            if (cd == 1) {
                //crdt
                settMain.setTransactionNoDbt("");
                settMain.setTransactionCodeDbt("");
                settMain.setTransactionNoCrdt("LOAN" + loanId);
                settMain.setTransactionCodeCrdt(code);
                settMain.setCrdtAmount(amount);
                settMain.setDbtAmount(0.00);
            } else {
                //debit
                settMain.setTransactionNoCrdt("");
                settMain.setTransactionCodeCrdt("");
                settMain.setTransactionNoDbt("LOAN" + loanId);
                settMain.setTransactionCodeDbt(code);
                settMain.setDbtAmount(amount);
                settMain.setCrdtAmount(0.00);
            }
            settMain.setDescription(desc);
            settMain.setUserId(userId);
            settMain.setIsPosting(true);
            settMain.setBranchId(branchId);
            settMain.setSystemDate(systemDate);
            settMain.setActionDate(cal.getTime());
            sessionFactory.getCurrentSession().save(settMain);

            settId = settMain.getSettlementId();

        } catch (Exception e) {
            System.out.println("Fail settlement main");
            e.printStackTrace();
        }
        return settId;

    }

    public int getUserLogedBranch(int userId) {

        String sql1 = "from UmUserLog where userId='" + userId + "' order by pk desc ";
        UmUserLog obj;
        int branchId = 0;
        try {
            obj = (UmUserLog) sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();
            if (obj != null) {
                branchId = obj.getBranchId();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return branchId;
    }
}

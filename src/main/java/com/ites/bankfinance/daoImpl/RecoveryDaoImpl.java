/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.ApprovalDetailsModel;
import com.bankfinance.form.ArriesLoan;
import com.bankfinance.form.DirectDebitCharge;
import com.bankfinance.form.Mail;
import com.bankfinance.form.RecoveryHistory;
import com.bankfinance.form.RecoveryLoanList;
import com.bankfinance.form.ReturnCheckPaymentList;
import com.bankfinance.form.TransactionList;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.dao.LoanDao;
import com.ites.bankfinance.dao.RecoveryDao;
import com.ites.bankfinance.dao.SettlementDao;
import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.model.DayEndDate;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.LoanGuaranteeDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanOtherCharges;
import com.ites.bankfinance.model.LoanPropertyLandDetails;
import com.ites.bankfinance.model.LoanPropertyOtherDetails;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.MBankDetails;
import com.ites.bankfinance.model.MSeizerDetails;
import com.ites.bankfinance.model.MSubTaxCharges;
import com.ites.bankfinance.model.MSupplier;
import com.ites.bankfinance.model.OpenD;
import com.ites.bankfinance.model.Posting;
import com.ites.bankfinance.model.RecoveryLetterDetail;
import com.ites.bankfinance.model.RecoveryLetters;
import com.ites.bankfinance.model.RecoveryShift;
import com.ites.bankfinance.model.RecoveryVisitsDetails;
import com.ites.bankfinance.model.SeizeOrder;
import com.ites.bankfinance.model.SeizeOrderApproval;
import com.ites.bankfinance.model.SeizeYardRegister;
import com.ites.bankfinance.model.SeizeYardRegisterCheklist;
import com.ites.bankfinance.model.SettlementMain;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementPayments;
import com.ites.bankfinance.model.UmUserLog;
import com.ites.bankfinance.service.MasterDataService;
import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ITESS
 */
@Repository
public class RecoveryDaoImpl implements RecoveryDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDao userDao;

    @Autowired
    private SettlementDao settlementDao;

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private ReportDaoImpl reportDaoImpl;

    @Autowired
    private LoanDaoImpl loanDaoImpl;

    @Autowired
    private DayEndDaoImpl dayEndDaoImpl;

    @Autowired
    private UserDaoImpl userDaoImpl;

    @Autowired
    private AdminDaoImpl adminDaoImpl;

    @Autowired
    private DebtorDaoImpl debtorDaoImpl;

    @Autowired
    private MasterDataService masterDataService;

    private final String refTransCodeArrears = "ARRS";
    private final String seizeCode = "SEIZE";
    private final String seizeCommissionAccount = "SEIZE_COMM";

    DecimalFormat df = new DecimalFormat("#,###.00");

    private final String refNoInstallmentCode = "INS";
    private final String installment = "Installment";

    public static final String seizeOrderCode = "SEIZE";
    public static final String letterCode = "LETT";

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public List<viewLoan> getLoanListForDue(String name, String agreementNo, String nic, String due, int branch) {

        List<viewLoan> loanList = new ArrayList();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        java.util.Date dt = new java.util.Date();
//        SELECT * FROM day_end_date ORDER BY Day_End_Id DESC  
        String sql4 = "from DayEndDate where branchId = " + branch + " order by dayEndId desc";
        DayEndDate obj1 = (DayEndDate) sessionFactory.getCurrentSession().createQuery(sql4).setMaxResults(1).uniqueResult();
        Date systemEndDate = obj1.getDayEndDate();

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(systemEndDate);
            calendar.add(Calendar.DATE, +3);

            String nextDate = sdf.format(calendar.getTime());

            if (!nextDate.equals(due) && due != null) {
                nextDate = due;
            }

            int userId = userDao.findByUserName();

            String sql = null, sql1 = null;
            String str;

            if (name != null) {
                str = " and lh.debtorHeaderDetails.debtorName like '" + name + "%' and lh.debtorHeaderDetails.debtorIsActive=1 ";
            } else if (nic != null) {
                str = " and lh.debtorHeaderDetails.debtorNic like '" + nic + "%' and lh.debtorHeaderDetails.debtorIsActive=1";
            } else if (agreementNo != null) {
                str = " and (lh.loanAgreementNo like '" + agreementNo + "%' or lh.loanBookNo like '" + agreementNo + "%') and lh.debtorHeaderDetails.debtorIsActive=1";
            } else {
                str = "";
            }

            //********************* from loan installment list *************
//SELECT * FROM loan_installment WHERE Due_Date = '2015-05-07' AND loan_installment.Ins_Id NOT IN(SELECT Instmnt_Id FROM recovery_shift WHERE due_date <= '2015-05-07'  AND  '2015-05-07' <= shift_date)
//            li.insId NOT IN(SELECT instmntId FROM RecoveryShift WHERE dueDate <= '"+nextDate+"'  AND  '"+nextDate+"' <= shiftDate)
            sql = "select new com.bankfinance.form.viewLoan(lh.loanId , lh.loanPeriod , li.dueDate , lh.debtorHeaderDetails , lh.loanAgreementNo , li.insRoundAmount , lh.loanBookNo,li.insId,lh.loanStatus,lh.loanSpec) from LoanHeaderDetails as lh , LoanInstallment as li where lh.branchId=" + branch + " and lh.loanId = li.loanId and li.dueDate = '" + nextDate + "'  " + str;

            List<viewLoan> loan_h_list = (List<viewLoan>) sessionFactory.getCurrentSession().createQuery(sql).list();
            if (loan_h_list.size() > 0) {
                boolean contains = false;
                for (int i = 0; i < loan_h_list.size(); i++) {
                    sql1 = "from RecoveryShift where instmntId ='" + loan_h_list.get(i).getInstlmntId() + "' order by recoverId asc ";
                    RecoveryShift obj;
                    DebtorHeaderDetails debtorHeaderDetails = loan_h_list.get(i).getDebtorHeaderDetails();
                    viewLoan loan = new viewLoan();
                    String remark = "";
                    String shiftDate = null;
                    try {
                        obj = (RecoveryShift) sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();
                        if (obj != null) {
                            shiftDate = obj.getShiftDate();
                            remark = obj.getRemark();
                            loan.setMsgDateShifted(1);
                        } else {
                            loan.setMsgDateShifted(0);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    loan.setLoanId(loan_h_list.get(i).getLoanId());
                    loan.setDebtorName(debtorHeaderDetails.getNameWithInitial());
                    loan.setDebtorTelephone(Integer.parseInt(debtorHeaderDetails.getDebtorTelephoneMobile()));
                    loan.setDebtorAddress(debtorHeaderDetails.getDebtorPersonalAddress());
                    loan.setAgreementNo(loan_h_list.get(i).getAgreementNo());
                    loan.setNic(debtorHeaderDetails.getDebtorNic());
                    loan.setDueDate(loan_h_list.get(i).getDueDate());
                    loan.setLoanBookNo(loan_h_list.get(i).getLoanBookNo());
                    loan.setLoanAmount(loan_h_list.get(i).getLoanAmount());
                    loan.setShiftDate(shiftDate);
                    loan.setRemark(remark);
                    loan.setInstlmntId(loan_h_list.get(i).getInstlmntId());
                    loan.setLoanSpec(loan_h_list.get(i).getLoanSpec());
                    loan.setLoanStatus(loan_h_list.get(i).getLoanStatus());

                    contains = loanList.contains(loan);
                    if (!contains) {
                        loanList.add(loan);
                    }
                }
            }
            //********************** shifted loan list********************
            String sql2 = "from RecoveryShift where dueDate <>'" + nextDate + "' and '" + nextDate + "'=  shiftDate ";
//            String sql2 = "from RecoveryShift where shiftDate ='" + nextDate + "'";
            List<RecoveryShift> shiftDates = (List<RecoveryShift>) sessionFactory.getCurrentSession().createQuery(sql2).list();

            if (shiftDates != null) {
                for (int j = 0; j < shiftDates.size(); j++) {
                    String sql3 = "select new com.bankfinance.form.viewLoan(lh.loanId , lh.loanPeriod , li.dueDate , lh.debtorHeaderDetails , lh.loanAgreementNo , li.insRoundAmount , lh.loanBookNo , li.insId,lh.loanStatus,lh.loanSpec) from LoanHeaderDetails as lh , LoanInstallment as li where lh.branchId=" + branch + " and lh.loanId = li.loanId and li.insId = '" + shiftDates.get(j).getInstmntId() + "' " + str;

                    viewLoan loan_h_list2 = (viewLoan) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                    if (loan_h_list2 != null) {
                        DebtorHeaderDetails debtorHeaderDetails = loan_h_list2.getDebtorHeaderDetails();
                        viewLoan loan = new viewLoan();
                        String remark = "";
                        String shiftDate = null;

                        shiftDate = shiftDates.get(j).getShiftDate();
                        remark = shiftDates.get(j).getRemark();
//                                    Date shDate = sdf.parse(shiftDate);
//                                    Date duDate = sdf.parse(loan_h_list2.getDueDate().toString());

                        // shifted instalment msg    
                        loan.setMsgAfterShiftDate(true);
                        if (nextDate.equals(shiftDate)) {
                            loan.setMsgSameShiftDate(true);
                        }

                        loan.setLoanId(loan_h_list2.getLoanId());
                        loan.setDebtorName(debtorHeaderDetails.getNameWithInitial());
                        loan.setDebtorTelephone(Integer.parseInt(debtorHeaderDetails.getDebtorTelephoneMobile()));
                        loan.setDebtorAddress(debtorHeaderDetails.getDebtorPersonalAddress());
                        loan.setAgreementNo(loan_h_list2.getAgreementNo());
                        loan.setNic(debtorHeaderDetails.getDebtorNic());
                        loan.setDueDate(loan_h_list2.getDueDate());
                        loan.setLoanBookNo(loan_h_list2.getLoanBookNo());
                        loan.setLoanAmount(loan_h_list2.getLoanAmount());
                        loan.setRemark(remark);
                        loan.setShiftDate(shiftDate);
                        loan.setInstlmntId(loan_h_list2.getInstlmntId());
                        loan.setMsgDateShifted(1);
                        loan.setLoanSpec(loan_h_list2.getLoanSpec());
                        loan.setLoanStatus(loan_h_list2.getLoanStatus());

                        loanList.add(loan);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;

    }

    @Override
    public boolean addRecoveryShiftDate(RecoveryShift formData) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        boolean status = false;
        try {
            session.save(formData);
            status = true;
            tx.commit();
//            session.flush();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public double getInstalmentBalance(int loanId) {
        String refCodeInstalment = "INS";
        String refCodeODI = "ODI";

        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and ( transactionCodeCrdt like '" + refCodeInstalment + "' or transactionCodeCrdt like '" + refCodeODI + "' ) ";
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and ( transactionCodeDbt like '" + refCodeInstalment + "' or transactionCodeDbt like '" + refCodeODI + "' ) ";
        double creditSum = 0;
        double debitSum = 0;
        double balance = 0;
        Object ob1 = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
        if (ob1 != null) {
            creditSum = Double.parseDouble(ob1.toString());
        }
        Object ob2 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
        if (ob2 != null) {
            debitSum = Double.parseDouble(ob2.toString());
        }
        balance = debitSum - creditSum;
        return balance;
    }

    public List<viewLoan> getArrearsLoanListForDue(String name, String agreementNo, String nic, String due, int branch) {

        List<viewLoan> loanList = new ArrayList();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String sql4 = "from DayEndDate where branchId = " + branch + " order by dayEndId desc  ";
        DayEndDate obj1 = (DayEndDate) sessionFactory.getCurrentSession().createQuery(sql4).setMaxResults(1).uniqueResult();
        Date systemEndDate = obj1.getDayEndDate();

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(systemEndDate);
            calendar.add(Calendar.DATE, -10);

            String nextDate = sdf.format(calendar.getTime());

            if (!nextDate.equals(due) && due != null) {
                nextDate = due;
            }

            int userId = userDao.findByUserName();

            String sql = null, sql1 = null;
            String str;

            if (name != null) {
                str = " and lh.debtorHeaderDetails.debtorName like '" + name + "%' and lh.debtorHeaderDetails.debtorIsActive=1 ";
            } else if (nic != null) {
                str = " and lh.debtorHeaderDetails.debtorNic like '" + nic + "%' and lh.debtorHeaderDetails.debtorIsActive=1";
            } else if (agreementNo != null) {
                str = " and lh.loanAgreementNo like '" + agreementNo + "%' and lh.debtorHeaderDetails.debtorIsActive=1";
            } else {
                str = "";
            }

            //********************** shifted loan list********************
            String sql2 = "from RecoveryShift where dueDate <='" + nextDate + "' and '" + nextDate + "'<=  shiftDate ";
//            String sql2 = "from RecoveryShift where shiftDate ='" + nextDate + "'";
            List<RecoveryShift> shiftDates = (List<RecoveryShift>) sessionFactory.getCurrentSession().createQuery(sql2).list();

            if (shiftDates != null) {
                for (int j = 0; j < shiftDates.size(); j++) {

                    String sql3 = "select new com.bankfinance.form.viewLoan(lh.loanId , lh.loanPeriod , li.dueDate , lh.debtorHeaderDetails , lh.loanAgreementNo , li.insRoundAmount , lh.loanBookNo , li.insId,lh.loanStatus,lh.loanSpec) from LoanHeaderDetails as lh , LoanInstallment as li where lh.branchId=" + branch + " lh.loanId = li.loanId and li.insId = '" + shiftDates.get(j).getInstmntId() + "' " + str;

                    viewLoan loan_h_list2 = (viewLoan) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                    if (loan_h_list2 != null) {

                        //SELECT ((SELECT SUM(Amount) FROM open_d WHERE open_d.Open_Id = open_h.Open_Id)+ Arreas_Without_Odi) AS total FROM open_h WHERE Loan_Id = 1
                        String sql5 = "select ((select sum(od.amount) from com.ites.bankfinance.model.OpenD as od where od.openId = oh.openId)+ oh.arreasWithoutOdi) as total from com.ites.bankfinance.model.OpenH as oh where oh.loanId = '" + loan_h_list2.getLoanId() + "'";
                        //SELECT SUM(Crdt_Amount) AS paidArrears FROM settlement_main WHERE Loan_Id = 1 AND Transaction_Code_Crdt =' ARRS'
                        String sql6 = "select sum(crdtAmount) AS paidArrears FROM SettlementMain WHERE loanId = '" + loan_h_list2.getLoanId() + "' AND transactionCodeCrdt ='" + refTransCodeArrears + "'";
                        Object obj3 = sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();
                        Object obj4 = sessionFactory.getCurrentSession().createQuery(sql6).uniqueResult();
                        double totalArrears = 0;
                        double paidArrears = 0;
                        if (obj3 != null) {
                            totalArrears = Double.parseDouble(obj3.toString());
                        }
                        if (obj4 != null) {
                            paidArrears = Double.parseDouble(obj4.toString());
                        }
                        if (totalArrears > paidArrears) {
                            DebtorHeaderDetails debtorHeaderDetails = loan_h_list2.getDebtorHeaderDetails();
                            viewLoan loan = new viewLoan();
                            String remark = "";
                            String shiftDate = null;

                            shiftDate = shiftDates.get(j).getShiftDate();
                            remark = shiftDates.get(j).getRemark();
//                                    Date shDate = sdf.parse(shiftDate);
//                                    Date duDate = sdf.parse(loan_h_list2.getDueDate().toString());

                            // shifted instalment msg    
                            loan.setMsgAfterShiftDate(true);
                            if (nextDate.equals(shiftDate)) {
                                loan.setMsgSameShiftDate(true);
                            }

                            loan.setLoanId(loan_h_list2.getLoanId());
                            loan.setDebtorName(debtorHeaderDetails.getNameWithInitial());
                            loan.setDebtorTelephone(Integer.parseInt(debtorHeaderDetails.getDebtorTelephoneMobile()));
                            loan.setDebtorAddress(debtorHeaderDetails.getDebtorPersonalAddress());
                            loan.setAgreementNo(loan_h_list2.getAgreementNo());
                            loan.setNic(debtorHeaderDetails.getDebtorNic());
                            loan.setDueDate(loan_h_list2.getDueDate());
                            loan.setLoanBookNo(loan_h_list2.getLoanBookNo());
                            loan.setLoanAmount(loan_h_list2.getLoanAmount());
                            loan.setRemark(remark);
                            loan.setShiftDate(shiftDate);
                            loan.setInstlmntId(loan_h_list2.getInstlmntId());
                            loan.setMsgDateShifted(1);
                            loan.setLoanStatus(loan_h_list2.getLoanStatus());
                            loan.setLoanSpec(loan_h_list2.getLoanSpec());

                            loanList.add(loan);
                        }
                    }
                }
            }

            //********************* from loan installment list *************
//SELECT * FROM loan_installment WHERE Due_Date = '2015-05-07' AND loan_installment.Ins_Id NOT IN(SELECT Instmnt_Id FROM recovery_shift WHERE due_date <= '2015-05-07'  AND  '2015-05-07' <= shift_date)
//            li.insId NOT IN(SELECT instmntId FROM RecoveryShift WHERE dueDate <= '"+nextDate+"'  AND  '"+nextDate+"' <= shiftDate)
            sql = "select new com.bankfinance.form.viewLoan(lh.loanId , lh.loanPeriod , li.dueDate , lh.debtorHeaderDetails , lh.loanAgreementNo , li.insRoundAmount , lh.loanBookNo , li.insId,lh.loanStatus,lh.loanSpec) from LoanHeaderDetails as lh , LoanInstallment as li where lh.branchId=" + branch + " lh.loanId = li.loanId and li.dueDate = '" + nextDate + "' and li.insId NOT IN(SELECT instmntId FROM RecoveryShift WHERE dueDate <= '" + nextDate + "'  AND  '" + nextDate + "' <= shiftDate) " + str;

            List<viewLoan> loan_h_list = (List<viewLoan>) sessionFactory.getCurrentSession().createQuery(sql).list();
            if (loan_h_list.size() > 0) {
                boolean contains = false;
                for (int i = 0; i < loan_h_list.size(); i++) {

                    //SELECT ((SELECT SUM(Amount) FROM open_d WHERE open_d.Open_Id = open_h.Open_Id)+ Arreas_Without_Odi) AS total FROM open_h WHERE Loan_Id = 1
                    String sql5 = "select ((select sum(od.amount) from OpenD as od where od.openId = oh.openId)+ oh.arreasWithoutOdi) as total from OpenH as oh where oh.loanId = '" + loan_h_list.get(i).getLoanId() + "'";
                    //SELECT SUM(Crdt_Amount) AS paidArrears FROM settlement_main WHERE Loan_Id = 1 AND Transaction_Code_Crdt =' ARRS'
                    String sql6 = "select sum(crdtAmount) AS paidArrears FROM SettlementMain WHERE loanId = '" + loan_h_list.get(i).getLoanId() + "' AND transactionCodeCrdt ='" + refTransCodeArrears + "'";
                    Object obj3 = sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();
                    Object obj4 = sessionFactory.getCurrentSession().createQuery(sql6).uniqueResult();
                    double totalArrears = 0;
                    double paidArrears = 0;
                    if (obj3 != null) {
                        totalArrears = Double.parseDouble(obj3.toString());
                    }
                    if (obj4 != null) {
                        paidArrears = Double.parseDouble(obj4.toString());
                    }
                    if (totalArrears > paidArrears) {

                        sql1 = "from RecoveryShift where instmntId ='" + loan_h_list.get(i).getInstlmntId() + "' order by recoverId desc ";
                        RecoveryShift obj;
                        DebtorHeaderDetails debtorHeaderDetails = loan_h_list.get(i).getDebtorHeaderDetails();
                        viewLoan loan = new viewLoan();
                        String remark = "";
                        String shiftDate = null;

                        try {
                            obj = (RecoveryShift) sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();
                            if (obj != null) {
                                shiftDate = obj.getShiftDate();
                                remark = obj.getRemark();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        loan.setLoanId(loan_h_list.get(i).getLoanId());
                        loan.setDebtorName(debtorHeaderDetails.getNameWithInitial());
                        loan.setDebtorTelephone(Integer.parseInt(debtorHeaderDetails.getDebtorTelephoneMobile()));
                        loan.setDebtorAddress(debtorHeaderDetails.getDebtorPersonalAddress());
                        loan.setAgreementNo(loan_h_list.get(i).getAgreementNo());
                        loan.setNic(debtorHeaderDetails.getDebtorNic());
                        loan.setDueDate(loan_h_list.get(i).getDueDate());
                        loan.setLoanBookNo(loan_h_list.get(i).getLoanBookNo());
                        loan.setLoanAmount(loan_h_list.get(i).getLoanAmount());
                        loan.setRemark(remark);
                        loan.setShiftDate(shiftDate);
                        loan.setInstlmntId(loan_h_list.get(i).getInstlmntId());
                        loan.setMsgDateShifted(0);
                        loan.setLoanStatus(loan_h_list.get(i).getLoanStatus());
                        loan.setLoanSpec(loan_h_list.get(i).getLoanSpec());

                        contains = loanList.contains(loan);
                        if (!contains) {
                            loanList.add(loan);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;

    }

    public List getRecoverLoanB(String name, String agreementNo, String nic) {

        List loanList = new ArrayList();
        String str = "";
        try {

            if (name != null) {
                str = " and lh.debtorHeaderDetails.debtorName like '" + name + "%' and lh.debtorHeaderDetails.debtorIsActive=1 ";
            } else if (nic != null) {
                str = " and lh.debtorHeaderDetails.debtorNic like '" + nic + "%' and lh.debtorHeaderDetails.debtorIsActive=1";
            } else if (agreementNo != null) {
                str = " and lh.loanAgreementNo like '" + agreementNo + "%' and lh.debtorHeaderDetails.debtorIsActive=1";
            } else {
                str = "";
            }
            String sql2 = "SELECT MAX(Due_Date) FROM loan_installment WHERE Loan_Id=2042 AND Due_Date<'2015-09-16'";

            String sql = ""; //"select new com.bankfinance.form.viewLoan(lh.loanId , lh.loanPeriod , li.dueDate , lh.debtorHeaderDetails , lh.loanAgreementNo , li.insRoundAmount , lh.loanBookNo , li.insId) from LoanHeaderDetails as lh , LoanInstallment as li where lh.loanId = li.loanId and li.dueDate = '" + nextDate + "' and li.insId NOT IN(SELECT instmntId FROM RecoveryShift WHERE dueDate <= '" + nextDate + "'  AND  '" + nextDate + "' <= shiftDate) " + str;

            List<viewLoan> loan_h_list = (List<viewLoan>) sessionFactory.getCurrentSession().createQuery(sql).list();
            if (loan_h_list.size() > 0) {
                boolean contains = false;
                for (int i = 0; i < loan_h_list.size(); i++) {

                    //SELECT ((SELECT SUM(Amount) FROM open_d WHERE open_d.Open_Id = open_h.Open_Id)+ Arreas_Without_Odi) AS total FROM open_h WHERE Loan_Id = 1
                    String sql5 = "select ((select sum(od.amount) from OpenD as od where od.openId = oh.openId)+ oh.arreasWithoutOdi) as total from OpenH as oh where oh.loanId = '" + loan_h_list.get(i).getLoanId() + "'";
                    //SELECT SUM(Crdt_Amount) AS paidArrears FROM settlement_main WHERE Loan_Id = 1 AND Transaction_Code_Crdt =' ARRS'
                    String sql6 = "select sum(crdtAmount) AS paidArrears FROM SettlementMain WHERE loanId = '" + loan_h_list.get(i).getLoanId() + "' AND transactionCodeCrdt ='" + refTransCodeArrears + "'";
                    Object obj3 = sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();
                    Object obj4 = sessionFactory.getCurrentSession().createQuery(sql6).uniqueResult();
                    double totalArrears = 0;
                    double paidArrears = 0;
                    if (obj3 != null) {
                        totalArrears = Double.parseDouble(obj3.toString());
                    }
                    if (obj4 != null) {
                        paidArrears = Double.parseDouble(obj4.toString());
                    }
                    if (totalArrears > paidArrears) {

                        String sql1 = "from RecoveryShift where instmntId ='" + loan_h_list.get(i).getInstlmntId() + "' order by recoverId desc ";
                        RecoveryShift obj;
                        DebtorHeaderDetails debtorHeaderDetails = loan_h_list.get(i).getDebtorHeaderDetails();
                        viewLoan loan = new viewLoan();
                        String remark = "";
                        String shiftDate = null;

                        try {
                            obj = (RecoveryShift) sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();
                            if (obj != null) {
                                shiftDate = obj.getShiftDate();
                                remark = obj.getRemark();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        loan.setLoanId(loan_h_list.get(i).getLoanId());
                        loan.setDebtorName(debtorHeaderDetails.getNameWithInitial());
                        loan.setDebtorTelephone(Integer.parseInt(debtorHeaderDetails.getDebtorTelephoneMobile()));
                        loan.setDebtorAddress(debtorHeaderDetails.getDebtorPersonalAddress());
                        loan.setAgreementNo(loan_h_list.get(i).getAgreementNo());
                        loan.setNic(debtorHeaderDetails.getDebtorNic());
                        loan.setDueDate(loan_h_list.get(i).getDueDate());
                        loan.setLoanBookNo(loan_h_list.get(i).getLoanBookNo());
                        loan.setLoanAmount(loan_h_list.get(i).getLoanAmount());
                        loan.setRemark(remark);
                        loan.setShiftDate(shiftDate);
                        loan.setInstlmntId(loan_h_list.get(i).getInstlmntId());
                        loan.setMsgDateShifted(0);

                        contains = loanList.contains(loan);
                        if (!contains) {
                            loanList.add(loan);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;

    }

    @Override
    public RecoveryShift getRecoveryShift(Integer instmntId) {

        RecoveryShift recovery = new RecoveryShift();
        try {
            String sql1 = "from RecoveryShift where instmntId ='" + instmntId + "'";
            List<RecoveryShift> recoveryShift = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (recoveryShift.size() > 0) {
                int index = recoveryShift.size() - 1;
                recovery = recoveryShift.get(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recovery;
    }

    @Override
    public List getRecoveryShiftDates(int LoanId) {
        List<RecoveryShift> recoveryShift = new ArrayList();
        try {
            String sql1 = "from RecoveryShift where loanId =" + LoanId;
            recoveryShift = sessionFactory.getCurrentSession().createQuery(sql1).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recoveryShift;
    }

    @Override
    public List<ReturnCheckPaymentList> getCheckPayments(String fromDate, String toDate) {
        String sql = "from SettlementPayments where systemDate  between '" + fromDate + "' and '" + toDate + "' and paymentType='2' ";

        List<SettlementPayments> stlnmtPaymnt = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql).list();
        List<ReturnCheckPaymentList> returnList = new ArrayList();
        if (stlnmtPaymnt != null) {
            for (SettlementPayments obj : stlnmtPaymnt) {
                ReturnCheckPaymentList returnObj = new ReturnCheckPaymentList();
                returnObj.setActionDate(obj.getActionDate());
                returnObj.setReceiptNo(obj.getReceiptNo());
                String sql2 = "from SettlementPaymentDetailCheque where chequeId ='" + obj.getPaymentDetails() + "' ";
                SettlementPaymentDetailCheque checkDetlai = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                if (checkDetlai != null) {
                    returnObj.setBank(checkDetlai.getBank());
                    returnObj.setChequeId(checkDetlai.getChequeId());
                    returnObj.setChequeNo(checkDetlai.getChequeNo());
                    returnObj.setAmount(checkDetlai.getAmount());
                    returnObj.setIsReturn(false);
                    if (checkDetlai.getIsReturn() == 1) {
                        returnObj.setIsReturn(true);
                    }
                    returnObj.setTransactionDate(checkDetlai.getTransactionDate());
                    returnObj.setUserId(checkDetlai.getUserId());
                    returnObj.setRealizeDate(checkDetlai.getRealizeDate());
                    String sql4 = "from MBankDetails where id ='" + checkDetlai.getBank() + "' ";
                    MBankDetails bankD = (MBankDetails) sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
                    returnObj.setBankName(bankD.getBankName());
                }
                String sql3 = "from LoanHeaderDetails where loanId ='" + obj.getLoanId() + "' ";
                LoanHeaderDetails loanH = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                returnObj.setAgreementNo(loanH.getLoanAgreementNo());
                returnObj.setDebtorAccountNo(loanH.getDebtorHeaderDetails().getDebtorAccountNo());

                returnList.add(returnObj);
            }
        }
        return returnList;
    }

    @Override
    public boolean updateChequePayment(int checkDetaiId, String remark, int userId) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        String sql = "from SettlementPaymentDetailCheque where chequeId ='" + checkDetaiId + "' ";
        SettlementPaymentDetailCheque cheqD = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

        cheqD.setIsReturn(1);
        cheqD.setRemark(remark);
        cheqD.setChequeReturnBy(userId);

        session.update(cheqD);

        transaction.commit();
        session.close();

        return true;
    }

    @Override
    public double getOpenArrears(int loanId) {
        double openArres = 0.00;
        double arres = 0.00;
        try {
            String sql1 = "select arreasWithoutOdi,openId from OpenH where loanId='" + loanId + "' ";
            Object[] ob = (Object[]) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            double withoutodi = 0.00;
            if (ob != null) {
                withoutodi = Double.parseDouble(ob[0].toString());
                int openId = Integer.parseInt(ob[1].toString());
                String sql2 = "from OpenD as d where d.openId=" + openId;
                List ob2 = sessionFactory.getCurrentSession().createQuery(sql2).list();
                double amount = 0.00;
                if (ob2.size() > 0) {
                    for (int i = 0; i < ob2.size(); i++) {
                        OpenD od = (OpenD) ob2.get(i);
                        if (od.getAmount() != null) {
                            amount = amount + od.getAmount();
                        }
                    }
                }
                arres = amount;
            }

            openArres = withoutodi + arres;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return openArres;
    }

    @Override
    public List loadLetterPostingLoans() {
        Session session = null;
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        List loanList = new ArrayList();
        try {
            session = sessionFactory.openSession();
            //get all letters
            String sql1 = "From RecoveryLetters where isActive = 1";
            List allLetters = session.createQuery(sql1).list();
            if (allLetters.size() > 0) {
                RecoveryLetters lt1 = (RecoveryLetters) allLetters.get(0);
                double letterCharge = lt1.getCharge();
                double letterPeriod = lt1.getTimePeriod();
                double balancePeriod = 0.00;
                double balance = 0.00;
                double rental = 0.00;

                //1st letter (NOT - Notice of Termination)
                String sql = "from LoanHeaderDetails where loanIsDelete = 0 and loanSpec = 0 ";
                List<LoanHeaderDetails> list_monthLoan = session.createQuery(sql).list();
                for (LoanHeaderDetails lhd : list_monthLoan) {
                    int loanId = lhd.getLoanId();
                    balance = reportDaoImpl.findLoanBalance(loanId);
                    String sql6 = "from LoanInstallment where loanId = " + loanId;
                    LoanInstallment li = (LoanInstallment) session.createQuery(sql6).setMaxResults(1).uniqueResult();
                    if (li != null) {
                        rental = li.getInsRoundAmount();
                    }
                    balancePeriod = balance / rental;
                    if (balancePeriod >= letterPeriod) {
                        //has arrears                               
                        RecoveryLoanList loanR = new RecoveryLoanList();
                        loanR.setLoanId(loanId);
                        loanR.setArresAmount(Double.parseDouble(decimalFormat.format(balance)));
                        loanR.setAgreementNo(lhd.getLoanAgreementNo());
                        loanR.setDueDate(lhd.getLoanDate().toString());
                        loanR.setLetteId(lt1.getLetterId());
                        loanR.setLetterName(lt1.getLetterName());
                        loanR.setColor(lt1.getColor());
                        loanR.setDebtorName(lhd.getDebtorHeaderDetails().getNameWithInitial());
                        loanR.setOdi(letterCharge);
                        LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanId);
                        if (lpvd != null) {
                            loanR.setVehicleNo(lpvd.getVehicleRegNo());
                        }
                        loanList.add(loanR);
                    }
                }

                //remaining letters (LOT - Letter of Termination)
                if (allLetters.size() > 1) {
                    RecoveryLetters lt2 = (RecoveryLetters) allLetters.get(1);
                    String sql5 = "from RecoveryLetterDetail where letterId = " + lt2.getLetterId();
                    List<RecoveryLetterDetail> letter2List = session.createQuery(sql5).list();
                    for (RecoveryLetterDetail letter2 : letter2List) {
                        balance = reportDaoImpl.findLoanBalance(letter2.getLoanId());
                        RecoveryLoanList loanR = new RecoveryLoanList();
                        loanR.setLoanId(letter2.getLoanId());
                        loanR.setArresAmount(Double.parseDouble(decimalFormat.format(balance)));
                        if (letter2.getAgreementNo() != null) {
                            loanR.setAgreementNo(letter2.getAgreementNo());
                        }
                        loanR.setDueDate(dateFormat.format(letter2.getDueDate()));
                        loanR.setLetteId(lt2.getLetterId());
                        loanR.setLetterName(lt2.getLetterName());
                        loanR.setColor(lt2.getColor());
                        LoanHeaderDetails lhd = loanDao.findLoanHeaderDetails(letter2.getLoanId());
                        if (lhd != null) {
                            loanR.setDebtorName(lhd.getDebtorHeaderDetails().getNameWithInitial());
                        }
                        loanR.setOdi(letter2.getOdi());
                        LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(letter2.getLoanId());
                        if (lpvd != null) {
                            loanR.setVehicleNo(lpvd.getVehicleRegNo());
                        }
                        loanR.setRecoverId(letter2.getRecoveryLetterId());

                        loanList.add(loanR);
                    }
                }
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return loanList;
    }

    public double findLoanBalanace2(int loanId) {
        double balance = 0.00;

        try {
            double openArrears = getOpenArrears(loanId);

            String sql_c = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeCrdt='ARRS' OR transactionCodeCrdt='OTHR' OR transactionCodeCrdt='INSU' "
                    + "OR transactionCodeCrdt='DWNS' OR transactionCodeCrdt='OVP' OR transactionCodeCrdt='INS' "
                    + "OR transactionCodeCrdt='ODI') AND loanId=" + loanId;

            String sql_d = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeDbt='ARRS' OR transactionCodeDbt='OTHR' OR transactionCodeDbt='INSU' "
                    + "OR transactionCodeDbt='DWNS' OR transactionCodeDbt='OVP' OR transactionCodeDbt='INS' "
                    + "OR transactionCodeDbt='ODI') AND loanId=" + loanId;

            double creditSum = 0.00;
            double debitSum = 0.00;

            Object totalCredit = sessionFactory.getCurrentSession().createQuery(sql_c).uniqueResult();
            if (totalCredit != null) {
                creditSum = Double.parseDouble(totalCredit.toString());
            }
            Object totalDebit = sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (totalDebit != null) {
                debitSum = Double.parseDouble(totalDebit.toString());
            }
            double diff = debitSum - creditSum;
            balance = diff + openArrears;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return balance;
    }

    private int findArrearsInstallment(int loanId) {
        int noOfInstallment = 0;
        int paidInstallment = 0, totalInstallment = 0;
        try {
            String sqlc = "SELECT COUNT(*) FROM SettlementMain WHERE loanId=" + loanId + " AND transactionCodeCrdt='INS' ";
            String sqld = "SELECT COUNT(*) FROM SettlementMain WHERE loanId=" + loanId + " AND transactionCodeDbt='INS' ";
            Object obc = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (obc != null) {
                paidInstallment = Integer.parseInt(obc.toString());
            }
            Object obd = sessionFactory.getCurrentSession().createQuery(sqld).uniqueResult();
            if (obd != null) {
                totalInstallment = Integer.parseInt(obd.toString());
            }
            noOfInstallment = totalInstallment - paidInstallment;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return noOfInstallment;
    }

    @Override
    public Object[] findRemainigInstallment(int loanId) {
        Object[] ret = new Object[2];
        int noOfreminIns = 0;
        int paidInstallment = 0;
        try {
            String sqlc = "SELECT COUNT(*) FROM SettlementMain WHERE loanId=" + loanId + " AND transactionCodeCrdt='INS' ";
            Object obc = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (obc != null) {
                paidInstallment = Integer.parseInt(obc.toString());
            }

            String sql1 = "from LoanInstallment where loanId=" + loanId;
            List<LoanInstallment> insList = sessionFactory.getCurrentSession().createQuery(sql1).list();
            int noIns = insList.size();
            Date due = null;
            if (insList.size() > 0) {
                due = insList.get(0).getDueDate();
            }

            noOfreminIns = noIns - paidInstallment;
            ret[0] = noOfreminIns;
            ret[1] = due;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    @Override
    public DebtorHeaderDetails findDebtorByLoanId(int loanId) {
        DebtorHeaderDetails debtor = new DebtorHeaderDetails();
        try {
            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            debtor = loan.getDebtorHeaderDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtor;
    }

    @Override
    public boolean saveLetter(RecoveryLetterDetail letter) {
        Session session = null;
        Transaction tx = null;
        boolean save = false;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            int userId = userDao.findByUserName();
            int branchId = 0;
            String sql = "FROM UmUserLog WHERE userId=" + userId + " ORDER BY pk DESC";
            UmUserLog userLog = (UmUserLog) sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();
            if (userLog != null) {
                branchId = userLog.getBranchId();
            }
            int arrInstallment = findArrearsInstallment(letter.getLetterId());
            Date systemDate = settlementDao.getSystemDate(branchId);
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            letter.setActionTime(actionTime);
            letter.setUserId(userId);
            letter.setSystemDate(systemDate);
            letter.setArrearsInstallment(arrInstallment);

            if (letter.getLetterId() == 5) {
                cal.setTime(systemDate);
                cal.add(Calendar.DATE, 3);
                String ad = sdf.format(cal.getTime());
                letter.setAttendDate(sdf.parse(ad));
            }

            String sql5 = "from RecoveryLetters where letterId=" + letter.getLetterId();
            RecoveryLetters rL = (RecoveryLetters) sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();
            double odi = 0.00;
            if (rL != null) {
                odi = rL.getLetterOdi();
            }
            if (letter.getLetterStatus() == 1) {
                letter.setLetterCharge(letter.getLetterCharge());
            } else {
                letter.setLetterCharge(0.00);
            }
            letter.setOdi(odi);
            letter.setRemainDates(3);
            session.saveOrUpdate(letter);

            //posting
            String sql2 = "select loanType, debtorHeaderDetails.debtorAccountNo, debtorHeaderDetails.debtorId from LoanHeaderDetails where loanId=" + letter.getLoanId();
            Object[] ob2 = (Object[]) session.createQuery(sql2).uniqueResult();
            int loanType = 0;
            String debtorAcc = "";
            int debtorId = 0;
            if (ob2.length > 0) {
                loanType = Integer.parseInt(ob2[0].toString());
                debtorAcc = ob2[1].toString();
                debtorId = Integer.parseInt(ob2[2].toString());
            }
            if (letter.getLetterStatus() == 1) {
                if (letter.getLetterCharge() > 0.00) {
                    SettlementMain sett = new SettlementMain();
                    sett.setLoanId(letter.getLoanId());
                    sett.setDebtorId(debtorId);
                    sett.setTransactionCodeDbt("OTHR");
                    sett.setTransactionNoDbt("LETT" + letter.getLetterId());
                    sett.setDbtAmount(letter.getLetterCharge());
                    sett.setDescription("Letter post");
                    sett.setSystemDate(systemDate);
                    sett.setIsPosting(true);
                    sett.setIsPrinted(false);
                    sett.setBranchId(branchId);
                    sett.setDueDate(letter.getDueDate());
                    sett.setUserId(userId);
                    sett.setActionDate(actionTime);
                    session.save(sett);

                    int saveId = sett.getSettlementId();
                    String refNo = loanDaoImpl.generateReferencNo(letter.getLoanId(), branchId);
                    refNo = refNo + "/" + saveId;

                    String letterPostAccNo = loanDaoImpl.findLoanAccountNo(0, letterCode);
                    Integer subTaxId = findSubTaxDetails(letterPostAccNo);
                    LoanOtherCharges charges = new LoanOtherCharges();
                    charges.setLoanId(letter.getLoanId());
                    charges.setChargesId(subTaxId);
                    charges.setAccountNo(letterPostAccNo);
                    charges.setActualAmount(letter.getLetterCharge());
                    charges.setChargeAmount(letter.getLetterCharge());
                    charges.setUserId(userId);
                    charges.setIsDownPaymentCharge(0);
                    charges.setIsPaid(0);
                    charges.setActionTime(actionTime);
                    session.save(charges);

                    if (!debtorAcc.equals("") && !letterPostAccNo.equals("")) {
                        loanDaoImpl.addToPosting(debtorAcc, refNo, new BigDecimal(letter.getLetterCharge()), branchId, 2);
                        loanDaoImpl.addToPosting(letterPostAccNo, refNo, new BigDecimal(letter.getLetterCharge()), branchId, 1);
                    } else {
                        return false;
                    }
                }
            }
            tx.commit();
            save = true;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return save;
    }

    @Override
    public List<RecoveryHistory> findRecoveryLetterDetails(int loanId) {
        List<RecoveryHistory> records = new ArrayList();
        List dateList = new ArrayList();
        try {
            String sql = "select dueDate From RecoveryLetterDetail where loanId=" + loanId + " order by recoveryLetterId desc";
            List letterDates = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (letterDates.size() > 0) {
                for (int i = 0; i < letterDates.size(); i++) {
                    Date dt = (Date) letterDates.get(i);
                    dateList.add(dt);
                }
            }
            String sql1 = "select dueDate From RecoveryVisitsDetails where loanId=" + loanId + " order by id desc";
            List visitdates = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (visitdates.size() > 0) {
                for (int i = 0; i < visitdates.size(); i++) {
                    Date dt2 = (Date) visitdates.get(i);
                    dateList.add(dt2);
                }
            }

            HashSet<Date> hh = new HashSet<Date>(dateList);
            List<Date> newdateList = new ArrayList<Date>(hh);
            List<RecoveryVisitsDetails> newVisitDetail = new ArrayList();
            if (newdateList.size() > 0) {
                for (int n = 0; n < newdateList.size(); n++) {
                    String dueDate = dateFormat.format(newdateList.get(n));
                    String sql2 = "from RecoveryLetterDetail where loanId=" + loanId + " and dueDate='" + dueDate + "'";
                    List<RecoveryLetterDetail> letterDetails = sessionFactory.getCurrentSession().createQuery(sql2).list();

                    String sql3 = "from RecoveryVisitsDetails where loanId=" + loanId + " and dueDate='" + dueDate + "'";
                    List<RecoveryVisitsDetails> visitDetails = sessionFactory.getCurrentSession().createQuery(sql3).list();
                    if (visitDetails.size() > 0) {
                        for (int i = 0; i < visitDetails.size(); i++) {
                            RecoveryVisitsDetails vv = (RecoveryVisitsDetails) visitDetails.get(i);
                            String sql4 = "select empLname From Employee where empNo=" + vv.getAssignedOfficer();
                            Object offname = sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
                            if (offname != null) {
                                vv.setOfficerName(offname.toString());
                                newVisitDetail.add(vv);
                            }
                        }
                    }

                    RecoveryHistory rec = new RecoveryHistory();
                    rec.setDueDate(dueDate);
                    rec.setLetters(letterDetails);
                    rec.setVisits(newVisitDetail);

                    records.add(rec);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return records;
    }

    @Override
    public String findInvoiceDetails(int loanId) {
        String detail = "";
        try {
            String vehicleData = "";
            String sql1 = "from LoanPropertyVehicleDetails where loanId=" + loanId + " order by vehicleId desc ";
            List vehList = sessionFactory.getCurrentSession().createQuery(sql1).list();
            LoanPropertyVehicleDetails vehicle = null;
            if (vehList.size() > 0) {
                vehicle = (LoanPropertyVehicleDetails) vehList.get(0);
            }
            if (vehicle != null) {
                if (vehicle.getVehicleRegNo() != null) {
                    vehicleData = vehicle.getVehicleRegNo() + "  ";
                }
                if (vehicle.getVehicleType() != null) {
                    vehicleData = vehicleData + vehicle.getVehicleType() + " ";
                }
                if (vehicle.getVehicleModel() != null) {
                    vehicleData = vehicleData + vehicle.getVehicleModel() + " ";
                }
                if (vehicle.getVehicleDescription() != null) {
                    vehicleData = vehicleData + vehicle.getVehicleDescription();
                }
            }
            String landData = "";
            String sql2 = "from LoanPropertyLandDetails where loanId=" + loanId + " order by landId desc ";
            List landList = sessionFactory.getCurrentSession().createQuery(sql2).list();
            LoanPropertyLandDetails land = null;
            if (landList.size() > 0) {
                land = (LoanPropertyLandDetails) landList.get(0);
            }
            if (land != null) {
                if (land.getLandDeedNo() != null) {
                    landData = land.getLandDeedNo() + " ";
                }
                if (land.getLandDescription() != null) {
                    landData = landData + land.getLandDescription();
                }
            }

            String otherData = "";
            String sql3 = "from LoanPropertyOtherDetails where loanId=" + loanId + " order by otherId desc ";
            List otherList = sessionFactory.getCurrentSession().createQuery(sql3).list();

            LoanPropertyOtherDetails other = null;
            if (otherList.size() > 0) {
                other = (LoanPropertyOtherDetails) otherList.get(0);
            }
            if (other != null) {
                if (other.getOtherType() != null) {
                    otherData = other.getOtherType() + " ";
                }
                if (other.getOtherDescription() != null) {
                    otherData = otherData + other.getOtherDescription();
                }
            }

            String articleData = "";
            String sql4 = "select a.AType, l.articleAmount from LoanPropertyArticlesDetails as l,MArticleTypes as a where l.loanId=" + loanId + " and l.articleType=a.AId";
            List articleList = sessionFactory.getCurrentSession().createQuery(sql4).list();
            if (articleList.size() > 0) {
                for (int i = 0; i < articleList.size(); i++) {
                    if (!articleData.equals("")) {
                        articleData = articleData + " ,";
                    }
                    Object[] article = (Object[]) articleList.get(i);
                    if (article[0] != null) {
                        articleData = articleData + " " + article[0].toString();
                    }
                    if (article[1] != null) {
                        articleData = articleData + " -" + article[1].toString();
                    }
                }
            }
            detail = vehicleData + landData + otherData + articleData;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detail;
    }

    @Override
    public Date findPreviousDate(int letterId, int loanId) {
        Date prvDate = null;
        try {
            String sql = "select systemDate from RecoveryLetterDetail where loanId=" + loanId + " and letterId=" + letterId + " and letterStatus=3 and order by recoveryLetterId desc ";
            List datList = sessionFactory.getCurrentSession().createQuery(sql).list();
            Date obj = null;
            if (datList.size() > 0) {
                obj = (Date) datList.get(0);
            }
            if (obj != null) {
                prvDate = obj;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prvDate;
    }

    @Override
    public int findLetterTypeByLoanType(int loanId) {
        int type = 0;
        try {
            String sql = "select r.type from LoanHeaderDetails as loan,MLoanType as m, MSubLoanType as s, RecoveryLoanType as r"
                    + " where loan.loanId=" + loanId + " and loan.loanType=s.subLoanId and s.loanTypeId=m.loanTypeId and m.loanTypeId=r.loanTypeId";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                type = Integer.parseInt(obj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return type;
    }

    @Override
    public int saveOrUpdateRecoveryOfficer(int officerId, int loanId, String dueDate, int visitType, String comment) {
        int save = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd");

        try {
            RecoveryVisitsDetails recovery = null;
            Date due = sdf.parse(dueDate);
            String sql = "from RecoveryVisitsDetails where loanId=" + loanId + " and dueDate='" + dueDate + "' and visitType=" + visitType + " and visitStatus=1 order by id desc ";
            List recList = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (recList.size() > 0) {
                recovery = (RecoveryVisitsDetails) recList.get(recList.size() - 1);
            }

            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            int userId = userDao.findByUserName();
            int branchId = 0;
            String sql2 = "FROM UmUserLog WHERE userId=" + userId + " ORDER BY pk DESC";
            UmUserLog userLog = (UmUserLog) session.createQuery(sql2).setMaxResults(1).uniqueResult();
            if (userLog != null) {
                branchId = userLog.getBranchId();
            }
            Date systemDate = settlementDao.getSystemDate(branchId);

            int visitStatus = 0;
            if (officerId > 0) {
                visitStatus = 1;
            } else {
                visitStatus = 2;
            }

            if (recovery != null) {
                //update
                recovery.setAssignedOfficer(officerId);
                recovery.setLoanId(loanId);
                recovery.setSystemDate(systemDate);
                recovery.setActionTime(actionTime);
                recovery.setUserId(userId);
                recovery.setVisitType(visitType);
                recovery.setDueDate(due);
                recovery.setVisitStatus(visitStatus);
                recovery.setVisitComment(comment);
                session.update(recovery);
            } else {
                //save
                recovery = new RecoveryVisitsDetails();
                recovery.setAssignedOfficer(officerId);
                recovery.setLoanId(loanId);
                recovery.setSystemDate(systemDate);
                recovery.setActionTime(actionTime);
                recovery.setUserId(userId);
                recovery.setVisitType(visitType);
                recovery.setDueDate(due);
                recovery.setVisitStatus(visitStatus);
                recovery.setVisitComment(comment);
                session.save(recovery);
            }
            tx.commit();
            save = recovery.getId();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return save;
    }

    @Override
    public List searchRecoveryOfficer() {

        List fieldOfficerList = null;
        List<ApprovalDetailsModel> off_list = new ArrayList();
        int userId = userDao.findByUserName();
        try {

            String sql2 = "select emp.empNo, emp.empFname, emp.empLname from UmUser as usr, UmFieldOfficers as off, Employee as emp "
                    + " where emp.empNo = off.empId and usr.userId=off.managerId "
                    + " and usr.isActive=1 and usr.isDelete=0 and usr.userId=" + userId;
            fieldOfficerList = sessionFactory.getCurrentSession().createQuery(sql2).list();

            for (int i = 0; i < fieldOfficerList.size(); i++) {
                Object[] obj = (Object[]) fieldOfficerList.get(i);
                ApprovalDetailsModel app = new ApprovalDetailsModel();
                app.setApp_id1(Integer.parseInt(obj[0].toString()));
                app.setApp_name1(obj[2].toString());
                off_list.add(app);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return off_list;
    }

    @Override
    public List<RecoveryLetters> findAllLetters() {
        List<RecoveryLetters> letter = new ArrayList();
        try {
            String sql = "From RecoveryLetters";
            letter = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return letter;
    }

    public List findAllRecoveredLoans(Date systemDate) {
        List loanList = new ArrayList();
        try {
            String sql = "From RecoveryLetterDetail where systemDate='" + systemDate + "'";
            List letters = sessionFactory.getCurrentSession().createQuery(sql).list();

            String sql1 = "From RecoveryVisitsDetails where systemDate='" + systemDate + "'";
            List visits = sessionFactory.getCurrentSession().createQuery(sql1).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

    public List loadAllRecoverdLoans(Date dueDate, int userType, int type) {
        List loanList = new ArrayList();
        int userId = userDao.findByUserName();
        try {
            if (userType == 4) {
                String sql = "from RecoveryVisitsDetails where dueDate='" + dueDate + "' and assignedOfficer=" + userId + " and visitStatus=1";
                List visitDetails = sessionFactory.getCurrentSession().createQuery(sql).list();
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public List findVisitDetailsByuser(String dueDate) {
        List<RecoveryLoanList> loanList = new ArrayList();
        int userId = userDao.findByUserName();
        int userType = userDao.findUserTypeByUserId(userId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (userType == 4) {
                String sql = "select loan.debtorHeaderDetails, loan.loanAgreementNo, loan.loanId, vi.visitType,vi.dueDate, vi.id from RecoveryVisitsDetails as vi,LoanHeaderDetails as loan where vi.assignedOfficer=" + userId + " and vi.visitStatus=2 and vi.loanId=loan.loanId";
                List visitDetails = sessionFactory.getCurrentSession().createQuery(sql).list();
                if (visitDetails.size() > 0) {
                    for (int i = 0; i < visitDetails.size(); i++) {
                        Object[] visit = (Object[]) visitDetails.get(i);
                        int loanId = Integer.parseInt(visit[2].toString());
                        int visitType = Integer.parseInt(visit[3].toString());
                        String aggNo = visit[1].toString();
                        String due_Date = visit[4].toString();
                        DebtorHeaderDetails debtor = (DebtorHeaderDetails) visit[0];
                        double arres = findLoanBalanace2(loanId);

                        RecoveryLoanList rec = new RecoveryLoanList();
                        rec.setAgreementNo(aggNo);
                        rec.setArresAmount(arres);
                        rec.setDebtorName(debtor.getNameWithInitial());
                        rec.setAddress(debtor.getDebtorPersonalAddress());
                        rec.setTelephone(String.valueOf(debtor.getDebtorTelephoneMobile()));
                        rec.setLetteId(visitType);
                        rec.setLoanId(loanId);
                        rec.setDueDate(due_Date);
                        rec.setRecoverId(Integer.parseInt(visit[5].toString()));

                        loanList.add(rec);
                    }
                }
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public List<viewLoan> findSearchLoanList(String agNo, String cusName, String nicNo, String dueDate, String lapsDate, String vehicleNo) {
        List<viewLoan> loanList = new ArrayList();
        List<LoanHeaderDetails> loans = new ArrayList();
        try {
            String where = "";
            if (!agNo.equals("")) {
                where = "(lh.loanAgreementNo like '" + agNo + "%' or lh.loanBookNo like '%" + agNo + "%')";
                String sql1 = "from LoanHeaderDetails as lh where " + where;
                loans = sessionFactory.getCurrentSession().createQuery(sql1).list();
            } else if (!cusName.equals("")) {
                where = "lh.debtorHeaderDetails.debtorName like '%" + cusName + "%'";
                String sql1 = "from LoanHeaderDetails as lh where " + where;
                loans = sessionFactory.getCurrentSession().createQuery(sql1).list();
            } else if (!nicNo.equals("")) {
                where = "lh.debtorHeaderDetails.debtorNic ='" + nicNo + "'";
                String sql1 = "from LoanHeaderDetails as lh where " + where;
                loans = sessionFactory.getCurrentSession().createQuery(sql1).list();
            } else if (!dueDate.equals("")) {
                where = "li.dueDate='" + dueDate + "'";
                String sql1 = "select new com.ites.bankfinance.model.LoanHeaderDetails(lh.loanAgreementNo,lh.loanBookNo,lh.loanAmount,lh.loanId,lh.loanPeriod,lh.loanPeriodType,lh.loanDate,lh.debtorHeaderDetails) from LoanHeaderDetails as lh, LoanInstallment as li where " + where + " and lh.loanId=li.loanId";
                loans = sessionFactory.getCurrentSession().createQuery(sql1).list();
            } else if (!vehicleNo.equals("")) {
                String sql1 = "select lpv.loanId from LoanPropertyVehicleDetails as lpv where lpv.vehicleRegNo like :vehicleRegNo and lpv.isActive = :isActive";
                List loanIds = sessionFactory.getCurrentSession().createQuery(sql1).setParameter("vehicleRegNo", "%" + vehicleNo + "%")
                        .setParameter("isActive", true).list();
                if (loanIds != null) {
                    String sql2 = "from LoanHeaderDetails as lh where lh.loanId in (:loanIds)";
                    loans = sessionFactory.getCurrentSession().createQuery(sql2).setParameterList("loanIds", loanIds).list();
                }
            }

            if (!loans.isEmpty()) {
                for (int i = 0; i < loans.size(); i++) {
                    LoanHeaderDetails loanH = (LoanHeaderDetails) loans.get(i);
                    viewLoan loan = new viewLoan();
                    loan.setAgreementNo(loanH.getLoanAgreementNo());
                    loan.setLoanBookNo(loanH.getLoanBookNo());
                    loan.setLoanAmount(loanH.getLoanAmount().doubleValue());
                    loan.setLoanId(loanH.getLoanId());
                    int periodType = loanH.getLoanPeriodType();
                    String period = "";
                    if (periodType == 1) {
                        period = loanH.getLoanPeriod() + " Month";
                    } else if (periodType == 2) {
                        period = loanH.getLoanPeriod() + " Years";
                    } else if (periodType == 3) {
                        period = loanH.getLoanPeriod() + " Days";
                    } else if (periodType == 4) {
                        period = loanH.getLoanPeriod() + " Weeks";
                    } else if (periodType == 5) {
                        period = " Laps";
                    } else if (periodType == 6) {
                        period = " Legal";
                    } else {
                        period = " Seize";
                    }
                    loan.setLoanPeriod(period);
                    loan.setLoanDate(loanH.getLoanDate());
                    DebtorHeaderDetails debtor = loanH.getDebtorHeaderDetails();
                    loan.setDebtorName(debtor.getNameWithInitial());
                    loan.setDebtorAddress(debtor.getDebtorPersonalAddress());
                    loan.setLoanSpec(loanH.getLoanSpec());
//                    if (debtor.getDebtorTelephoneMobile() != null || !debtor.getDebtorTelephoneMobile().trim().equals("")) {
//                        loan.setDebtorTelephone(Integer.parseInt(debtor.getDebtorTelephoneMobile()));
//                    }
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanH.getLoanId());
                    if (lpvd != null) {
                        loan.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    loanList.add(loan);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public List loanTransactionDetails(int loanId) {
        List transList = new ArrayList();
        try {
            String sql2 = "select s.settlementId,s.crdtAmount,s.receiptNo,s.description,s.actionDate FROM SettlementMain s,LoanHeaderDetails l "
                    + "WHERE l.loanId=s.loanId AND s.loanId=" + loanId + " AND s.settlementId "
                    + "IN (SELECT settlementId FROM SettlementMain "
                    + "WHERE loanId=" + loanId + " AND (transactionCodeCrdt='INS' OR transactionCodeCrdt='OTHR' OR transactionCodeCrdt='OVP' OR transactionCodeCrdt='ODI'))"
                    + "ORDER BY s.loanId ASC, s.systemDate ASC";
            List settList = sessionFactory.getCurrentSession().createQuery(sql2).list();

            if (!settList.isEmpty()) {
                for (int i = 0; i < settList.size(); i++) {
                    int setId = 0;
                    double credit = 0.00;
                    String description = "", recptNo = "";
                    String paymentDate = "";
                    Object setObj[] = (Object[]) settList.get(i);
                    if (setObj[0] != null) {
                        setId = Integer.parseInt(setObj[0].toString());
                    }
                    if (setObj[1] != null && !setObj[1].toString().equals("")) {
                        credit = Double.parseDouble(setObj[1].toString());
                    }
                    if (setObj[2] != null) {
                        recptNo = setObj[2].toString();
                    }
                    if (setObj[3] != null) {
                        description = setObj[3].toString();
                    }
                    if (setObj[4] != null && !setObj[4].toString().equals("")) {
                        Date p = dateFormat.parse(setObj[4].toString());
                        paymentDate = dateFormat.format(p);
                    }

                    TransactionList trans = new TransactionList();
                    trans.setSetId(setId);
                    trans.setPayment(df.format(credit));
                    trans.setReceiptNo(recptNo);
                    trans.setDescription(description);
                    trans.setPaymentDate(paymentDate);
                    transList.add(trans);
                }
            }
        } catch (HibernateException | NumberFormatException | ParseException e) {
            e.printStackTrace();
        }
        return transList;
    }

    //add recovery Charges
    @Override
    public boolean saveOtherRecoveryCharge(int loanId, int chargeId, int supplierId, double amount, String dueDate) {
        boolean save = false;
        Session session = null;
        Transaction tx = null;

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sql = "from MSubTaxCharges where subTaxId=" + chargeId;
            MSubTaxCharges tax = (MSubTaxCharges) session.createQuery(sql).uniqueResult();

            String sql2 = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();

            String transNoDbt = "OTHR" + chargeId;
            double taxValue = 0.00;
            String taxDescription = "";
            String taxAccountNo = "";
            String debtorAccNo = "";

            if (tax != null) {
                taxValue = tax.getSubTaxValue();
                taxAccountNo = tax.getSubTaxAccountNo();
                taxDescription = tax.getSubTaxDescription();
                taxAccountNo = tax.getSubTaxAccountNo();
            }

            if (loan.getDebtorHeaderDetails() != null) {
                debtorAccNo = loan.getDebtorHeaderDetails().getDebtorAccountNo();
            }

            int userId = userDao.findByUserName();
            int branchId = settlementDao.getUserLogedBranch(userId);
            Date dt = settlementDao.getSystemDate(branchId);

            // add to loan other charge
            LoanOtherCharges loanOtherCharges = new LoanOtherCharges();
            loanOtherCharges.setChargeAmount(amount);
            loanOtherCharges.setChargesId(chargeId);
            loanOtherCharges.setLoanId(loanId);
            loanOtherCharges.setUserId(userId);
            loanOtherCharges.setIsDownPaymentCharge(0);
            loanOtherCharges.setIsPaid(0);
            loanOtherCharges.setIsDirectDebit(true);
            loanOtherCharges.setSupplierId(supplierId);
            loanOtherCharges.setActualAmount(taxValue);
            loanOtherCharges.setAccountNo(taxAccountNo);
            loanOtherCharges.setActionTime(Calendar.getInstance().getTime());
            session.save(loanOtherCharges);

            // add to settlement main
            SettlementMain main = new SettlementMain();
            main.setDebtorId(loan.getDebtorHeaderDetails().getDebtorId());
            main.setLoanId(loanId);
            main.setChargeId(chargeId);
            main.setTransactionCodeDbt("OTHR");
            main.setTransactionNoDbt(transNoDbt);
            main.setDescription(taxDescription);
            main.setDbtAmount(amount);
            main.setUserId(userId);
            main.setBranchId(branchId);
            main.setIsPosting(true);
            main.setSystemDate(dt);
            session.save(main);

            int settId = main.getSettlementId();
            if (!taxAccountNo.equals("") && !debtorAccNo.equals("")) {
                if (amount > 0) {
                    String refNo = loanDaoImpl.generateReferencNo(loanId, branchId) + "/" + settId;
                    loanDaoImpl.addToPosting(taxAccountNo, refNo, new BigDecimal(amount), branchId, 1);
                    loanDaoImpl.addToPosting(debtorAccNo, refNo, new BigDecimal(amount), branchId, 2);
                }
            }

            tx.commit();
            save = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return save;
    }

    @Override
    public RecoveryVisitsDetails findRecoveryVisitDetail(int recId, int loanId) {
        RecoveryVisitsDetails recovery = new RecoveryVisitsDetails();
        try {
            String sql = "from RecoveryVisitsDetails where id=" + recId + " and loanId=" + loanId;
            recovery = (RecoveryVisitsDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recovery;
    }

    @Override
    public boolean saveRecoveryManagerComment(int recId, String comment) {
        boolean flag = false;
        try {
            String sql = "from RecoveryVisitsDetails where id=" + recId;
            RecoveryVisitsDetails recovery = (RecoveryVisitsDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (recovery != null) {
                recovery.setFinalComment(comment);
                recovery.setVisitStatus(4);
                sessionFactory.getCurrentSession().update(recovery);
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    //Thushan
    //find Recovery Visit Details to given Loan Id
    @Override
    public RecoveryVisitsDetails findRecoveryVisitDetail(int loanId) {
        RecoveryVisitsDetails recovery = new RecoveryVisitsDetails();
        try {
            String sql = "from RecoveryVisitsDetails where loanId = " + loanId;
            recovery = (RecoveryVisitsDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recovery;
    }

    //save or update recovery visit details
    @Override
    public int saveOrUpdateRecoveryVisitDetails(RecoveryVisitsDetails recoveryVisitsDetails) {
        int id = 0;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(recoveryVisitsDetails);
            transaction.commit();
            id = recoveryVisitsDetails.getId();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return id;
    }

    //get recovery visit details by submit date
    @Override
    public List<RecoveryVisitsDetails> getBySubmitDate(Date sDate, Date eDate) {
        List<RecoveryVisitsDetails> rvds = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String sql = "SELECT rvd.id, rvd.loanId, rvd.dueDate, rvd.submitDate, rvd.visitComment, rvd.visitType, lo.loanAgreementNo FROM RecoveryVisitsDetails rvd, LoanHeaderDetails lo WHERE rvd.loanId = lo.loanId AND rvd.submitDate BETWEEN '" + sdf.format(sDate) + "' AND '" + sdf.format(eDate) + "' AND rvd.visitStatus = 3";
            List list = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (list.size() > 0) {
                for (Object object : list) {
                    Object[] o = (Object[]) object;
                    RecoveryVisitsDetails recVD = new RecoveryVisitsDetails();
                    recVD.setId(Integer.parseInt(o[0].toString()));
                    recVD.setLoanId(Integer.parseInt(o[1].toString()));
                    recVD.setDDate(o[2].toString());
                    recVD.setSDate(o[3].toString());
                    recVD.setVisitComment(o[4].toString());
                    recVD.setVisitType(Integer.parseInt(o[5].toString()));
                    recVD.setAggrNo(o[6].toString());

                    rvds.add(recVD);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return rvds;
    }

    @Override
    public List<ArriesLoan> getLoanArries(String date1, String date2) {
        List<ArriesLoan> arriesLoans = new ArrayList<>();
        try {
            String sql = "select lh.loanId,lh.loanAgreementNo,lh.loanBookNo, sl.subLoanId,sl.subLoanName,lh.loanInstallment,lh.loanInterest,sm.systemDate, lh.loanPeriod, mp.typeDescription from SettlementMain as sm,LoanHeaderDetails as lh, MSubLoanType as sl, MPeriodTypes as mp WHERE sm.transactionCodeCrdt in('ARRS', 'OTHR', 'OVP', 'INSU', 'DWNS', 'INS', 'ODI') and sm.loanId = lh.loanId and mp.typeId = lh.loanPeriodType and lh.loanType = sl.subLoanId and sm.systemDate between '" + date1 + "' and '" + date2 + "' group by sm.loanId";
            List loanList = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (loanList != null) {
                for (Object ls : loanList) {
                    Object[] loan = (Object[]) ls;
                    ArriesLoan arriesLoan = new ArriesLoan();
                    arriesLoan.setLoanId(Integer.parseInt(loan[0].toString()));
                    arriesLoan.setAgreementNo(String.valueOf(loan[1].toString()));
                    arriesLoan.setBookNo(String.valueOf(loan[2].toString()));
                    arriesLoan.setLoanTypeId(Integer.parseInt(loan[3].toString()));
                    arriesLoan.setLoanType(String.valueOf(loan[4].toString()));
                    arriesLoan.setSystemDate(String.valueOf(loan[7].toString()));
                    double arr = getArries(Integer.parseInt(loan[0].toString()), date1, date2);

                    DecimalFormat df = new DecimalFormat("#.00");
                    String arrFormat = df.format(arr);
                    arriesLoan.setArries(Double.parseDouble(arrFormat));
//                   arriesLoan.setArries(getArries(Integer.parseInt(loan[0].toString()), date1, date2));
                    arriesLoan.setReInstallment(getPaidInstalmentCount(Integer.parseInt(loan[0].toString()), date1, date2));
                    arriesLoan.setLoanInstallment(Double.parseDouble(loan[5].toString()));
                    arriesLoan.setInterestPotion(Double.parseDouble(loan[6].toString()));
                    arriesLoan.setPeriod(Integer.parseInt(loan[8].toString()));
                    arriesLoan.setPeriodType(loan[9].toString());
                    arriesLoans.add(arriesLoan);
                }
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return arriesLoans;
    }

    public double getArries(int loanId, String date1, String date2) {
        double balance = 0.00;

        try {
            double openArrears = getOpenArrears(loanId);

            String sql_c = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeCrdt='ARRS' OR transactionCodeCrdt='OTHR' OR transactionCodeCrdt='INSU' "
                    + "OR transactionCodeCrdt='DWNS' OR transactionCodeCrdt='OVP' OR transactionCodeCrdt='INS' "
                    + "OR transactionCodeCrdt='ODI' OR transactionCodeCrdt = 'RBT') AND loanId=" + loanId + " AND systemDate BETWEEN '" + date1 + "' AND '" + date2 + "'";

            String sql_d = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeDbt='ARRS' OR transactionCodeDbt='OTHR' OR transactionCodeDbt='INSU' "
                    + "OR transactionCodeDbt='DWNS' OR transactionCodeDbt='OVP' OR transactionCodeDbt='INS' "
                    + "OR transactionCodeDbt='ODI' OR transactionCodeDbt = 'RBT') AND loanId=" + loanId + "AND systemDate BETWEEN '" + date1 + "' AND '" + date2 + "'";

            double creditSum = 0.00;
            double debitSum = 0.00;

            Object totalCredit = sessionFactory.getCurrentSession().createQuery(sql_c).uniqueResult();
            if (totalCredit != null) {
                creditSum = Double.parseDouble(totalCredit.toString());
            }
            Object totalDebit = sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (totalDebit != null) {
                debitSum = Double.parseDouble(totalDebit.toString());
            }
            double diff = debitSum - creditSum;
            balance = diff + openArrears;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return balance;
    }

    public int getPaidInstalmentCount(int loanId, String date1, String date2) {
        String sql1 = "select count(sm.crdtAmount) from SettlementMain as sm where sm.loanId= " + loanId + " and sm.transactionCodeCrdt like '%" + refNoInstallmentCode + "%' and sm.description = '" + installment + "' and systemDate  between '" + date1 + "' and  '" + date2 + "'";
        int count1 = 0;
        int count2 = 0;
        int remaing = 0;
        Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
        if (ob != null) {
            count1 = Integer.parseInt(ob.toString());
        }

//        String sql2 = "select count(l.loanId) from LoanInstallment as l where l.loanId = " + loanId;
//        Object ob1 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
//        if (ob1 != null) {
//            count2 = Integer.parseInt(ob1.toString());
//        }
//        remaing = count2 - count1;
//        return remaing;
        return count1;
    }

    // Seize Module
    @Override
    public List<SeizeOrder> getSeizeOrders() {
        List<SeizeOrder> seizeOrders = new ArrayList<>();
        try {
            // find created seize orders
            String sql1 = "from SeizeOrder where isKeyIssue = :isKeyIssue and isRenew = :isRenew";
            List<SeizeOrder> createdSeizeOrders = (List<SeizeOrder>) sessionFactory.getCurrentSession()
                    .createQuery(sql1).setParameter("isKeyIssue", false)
                    .setParameter("isRenew", false).list();
            for (SeizeOrder createdSeizeOrder : createdSeizeOrders) {
                LoanHeaderDetails loanHeaderDetails = loanDaoImpl.findLoanHeaderByLoanId(createdSeizeOrder.getLoanId());
                if (loanHeaderDetails != null) {
                    createdSeizeOrder.setLoanAggNo(loanHeaderDetails.getLoanAgreementNo());
                    if (loanHeaderDetails.getDebtorHeaderDetails() != null) {
                        createdSeizeOrder.setDebtorName(loanHeaderDetails.getDebtorHeaderDetails().getDebtorName());
                    }
                    seizeOrders.add(createdSeizeOrder);
                }
            }
            // find new seize orders
            String sql2 = "SELECT loan.loanId, loan.loanAgreementNo, loan.loanInstallment, debtor.debtorId, debtor.debtorName "
                    + " FROM LoanHeaderDetails  AS loan, MLoanType AS loanType, MSubLoanType AS subLoanType, DebtorHeaderDetails AS debtor "
                    + " WHERE loan.loanType = subLoanType.subLoanId AND loanType.loanTypeId = subLoanType.loanTypeId "
                    + " AND loan.debtorHeaderDetails.debtorId = debtor.debtorId AND loanType.loanTypeCode IN ('LS','MB','HP')";
            List loanList = sessionFactory.getCurrentSession().createQuery(sql2).list();
            if (loanList.size() > 0) {
                for (Object loan : loanList) {
                    Object[] loanArr = (Object[]) loan;
                    int loanId = Integer.parseInt(loanArr[0].toString());
                    boolean exist = false;
                    for (SeizeOrder seizeOrder : seizeOrders) {
                        if (seizeOrder.getLoanId() == loanId) {
                            exist = true;
                        }
                    }
                    if (!exist) {
                        double loanInstallment = Double.parseDouble(loanArr[2].toString());
                        //System.out.println("Installment - " + loanInstallment);
                        double checkAmount = loanInstallment * 2;
                        //System.out.println("checkAMount-" + checkAmount);
                        int letterCount = 0;
                        String sql3 = "SELECT COUNT(*) FROM RecoveryLetterDetail WHERE loanId=" + loanId;
                        Object result1 = sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                        if (result1 != null) {
                            letterCount = Integer.parseInt(result1.toString());
                        }
                        //System.out.println("letterCount-" + letterCount);
                        if (letterCount >= 5) {
                            double loanArrears = dayEndDaoImpl.findBalanceWithOverPay(loanId);
                            //System.out.println("loanArrears-" + loanArrears);
                            if (loanArrears > checkAmount) {
                                int totInstallmentCount = 0;
                                int paidInstallmentCount = 0;
                                int remainInstallmentCount = 0;
                                String sql4 = "SELECT COUNT(*) FROM LoanInstallment WHERE loanId=" + loanId;
                                Object result2 = sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
                                if (result2 != null) {
                                    totInstallmentCount = Integer.parseInt(result2.toString());
                                }
                                //System.out.println("totInstallmentCount-" + totInstallmentCount);
                                String sql5 = "SELECT COUNT(*) FROM SettlementMain WHERE transactionCodeCrdt='" + refNoInstallmentCode + "' AND loanId=" + loanId;
                                Object result3 = sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();
                                if (result3 != null) {
                                    paidInstallmentCount = Integer.parseInt(result3.toString());
                                }
                                //System.out.println("paidInstallmentCount-" + paidInstallmentCount);
                                remainInstallmentCount = totInstallmentCount - paidInstallmentCount;
                                //System.out.println("remainInstallmentCount-" + remainInstallmentCount);

                                SeizeOrder seizeOrder = new SeizeOrder();
                                seizeOrder.setLoanId(loanId);
                                seizeOrder.setLoanAggNo(loanArr[1].toString());
                                seizeOrder.setDebtorId(Integer.parseInt(loanArr[3].toString()));
                                seizeOrder.setDebtorName(loanArr[4].toString());
                                seizeOrder.setArrearsRental(loanArr[2].toString() + " x " + remainInstallmentCount);

                                seizeOrders.add(seizeOrder);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return seizeOrders;
    }

    @Override
    public boolean saveOrUpdateSeizeOrder(SeizeOrder seizeOrder) {
        boolean sucess = false;
        int userId = userDao.findByUserName();
        Date actionTime = Calendar.getInstance().getTime();
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            if (seizeOrder.getPk() == null) { // generate seize order code
                int sCode = 0;
                String sql1 = "from SeizeOrder order by seizeCode desc";
                SeizeOrder so = (SeizeOrder) session.createQuery(sql1).setMaxResults(1).uniqueResult();
                if (so != null) {
                    sCode = Integer.parseInt(so.getSeizeCode().substring(5));
                    sCode++;
                } else {
                    sCode = 1;
                }
                seizeOrder.setSeizeCode(seizeOrderCode + sCode);
                // set renewal count
                int renewalCount = 0;
                List seizeList = sessionFactory.getCurrentSession().createCriteria(SeizeOrder.class)
                        .add(Restrictions.eq("loanId", seizeOrder.getLoanId())).list();
                if (seizeList.size() > 0) {
                    renewalCount = ((SeizeOrder) seizeList.get(seizeList.size() - 1)).getRenewalCount() + 1;
                }
                seizeOrder.setRenewalCount(renewalCount);
            }
            if (seizeOrder.getIsRenew()) { // check if seize order is re-new or not
                String rDate = dateFormat.format(actionTime);
                Date renewalDate = dateFormat.parse(rDate);
                seizeOrder.setRenewalDate(renewalDate);
            }
            seizeOrder.setCreatedBy(userId);
            seizeOrder.setActionTime(actionTime);
            seizeOrder.setUserId(userId);
            seizeOrder.setIsDelete(false);
            transaction = session.beginTransaction();
            session.saveOrUpdate(seizeOrder);
            transaction.commit();
            sucess = true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return sucess;
    }

    @Override
    public boolean activeSeizeOrder(int orderId, int loanId) {
        boolean success = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            String sql = "FROM SeizeOrder WHERE pk = :pk AND loanId = :loanId";
            SeizeOrder seizeOrder = (SeizeOrder) session.createQuery(sql).setParameter("pk", orderId)
                    .setParameter("loanId", loanId).uniqueResult();
            if (seizeOrder != null) {
                seizeOrder.setIsDelete(false);
                transaction = session.beginTransaction();
                session.update(seizeOrder);
                transaction.commit();
                success = true;
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return success;
    }

    @Override
    public boolean inActiveSeizeOrder(int orderId, int loanId) {
        boolean success = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            String sql = "FROM SeizeOrder WHERE pk = :pk AND loanId = :loanId";
            SeizeOrder seizeOrder = (SeizeOrder) session.createQuery(sql).setParameter("pk", orderId)
                    .setParameter("loanId", loanId).uniqueResult();
            if (seizeOrder != null) {
                seizeOrder.setIsDelete(true);
                transaction = session.beginTransaction();
                session.update(seizeOrder);
                transaction.commit();
                success = true;
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return success;
    }

    @Override
    public List<SeizeOrder> loadSeizeApproval() {
        List<SeizeOrder> seizeOrderApprovalList = new ArrayList<>();
        int userId = userDaoImpl.findByUserName();
        int userLogBranchId = settlementDao.getUserLogedBranch(userId);

        try {
            // find seize orders
            String sql4 = "from SeizeOrder where isDelete = 0 and branchId = " + userLogBranchId + " and isKeyIssue = 0";
            List<SeizeOrder> seizeOrders = (List<SeizeOrder>) sessionFactory.getCurrentSession().createQuery(sql4).list();

            for (SeizeOrder seizeOrder : seizeOrders) {
                int loanId = seizeOrder.getLoanId();
                int seizerId = seizeOrder.getSeizerId();
                String loanAggNo = "";
                String debtorName = "";
                String seizerName = "";
                if (loanId > 0) {
                    LoanHeaderDetails loanHeaderDetails = loanDaoImpl.findLoanHeaderByLoanId(loanId);
                    if (loanHeaderDetails != null) {
                        loanAggNo = loanHeaderDetails.getLoanAgreementNo();
                        if (loanHeaderDetails.getDebtorHeaderDetails() != null) {
                            debtorName = loanHeaderDetails.getDebtorHeaderDetails().getDebtorName();
                        }
                    }
                }
                if (seizerId > 0) {
                    String sql5 = "from MSeizerDetails where seizerId = :seizerId";
                    MSeizerDetails seizer = (MSeizerDetails) sessionFactory.getCurrentSession().createQuery(sql5)
                            .setParameter("seizerId", seizeOrder.getSeizerId()).uniqueResult();
                    if (seizer != null) {
                        seizerName = seizer.getSeizerName();
                    }
                }

                seizeOrder.setLoanAggNo(loanAggNo);
                seizeOrder.setDebtorName(debtorName);
                seizeOrder.setSerizerName(seizerName);

                seizeOrderApprovalList.add(seizeOrder);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seizeOrderApprovalList;
    }

    @Override
    public boolean saveOrUpdateSeizeOrderApproval(SeizeOrderApproval seizeOrderApproval) {
        boolean success = false;
        int userId = userDao.findByUserName();
        int userLogBranchId = settlementDao.getUserLogedBranch(userId);
        Date actionTime = Calendar.getInstance().getTime();
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            seizeOrderApproval.setActionTime(actionTime);
            seizeOrderApproval.setBranchId(userLogBranchId);
            seizeOrderApproval.setUserId(userId);
            transaction = session.beginTransaction();
            session.saveOrUpdate(seizeOrderApproval);
            if (seizeOrderApproval.getIssueId() > 0) {
                //update seize order details
                SeizeOrder seizeOrder = (SeizeOrder) session.createCriteria(SeizeOrder.class)
                        .add(Restrictions.eq("isDelete", false))
                        .add(Restrictions.eq("pk", seizeOrderApproval.getSeizeOrderId())).uniqueResult();
                if (seizeOrderApproval.getApprovalType() == 1) {
                    if (seizeOrderApproval.getIsReject() == 1) {
                        seizeOrder.setApproval_2(1);
                    } else {
                        seizeOrder.setApproval_2(0);
                    }
                } else if (seizeOrderApproval.getIsReject() == 1) {
                    seizeOrder.setApproval_1(1);
                } else {
                    seizeOrder.setApproval_1(0);
                }
                session.saveOrUpdate(seizeOrder);
            }
            transaction.commit();
            success = true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return success;
    }

    @Override
    public SeizeOrderApproval findSeizeOrderApproval(int seizeOrdeId, int type) {
        SeizeOrderApproval seizeOrderIssue = null;
        try {
            String sql = "from SeizeOrderApproval where seizeOrderId = :seizeOrderId and approvalType = :approvalType";
            seizeOrderIssue = (SeizeOrderApproval) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("seizeOrderId", seizeOrdeId)
                    .setParameter("approvalType", type)
                    .uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return seizeOrderIssue;
    }

    @Override
    public boolean keyIssue(int seizeOrderId) {
        boolean keyIssuse = false;
        String keyIssueDate = dateFormat.format(Calendar.getInstance().getTime());
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            String sql1 = "from SeizeOrder where pk = :pk";
            SeizeOrder seizeOrder = (SeizeOrder) session.createQuery(sql1)
                    .setParameter("pk", seizeOrderId).uniqueResult();
            if (seizeOrder != null) {
                seizeOrder.setKeyIssueDate(dateFormat.parse(keyIssueDate));
                seizeOrder.setIsKeyIssue(true);
                transaction = session.beginTransaction();
                session.update(seizeOrder);
                transaction.commit();
                keyIssuse = true;
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return keyIssuse;
    }

    @Override
    public List<SeizeYardRegister> getYardRegisterList() {
        List<SeizeYardRegister> yardRegisterList = new ArrayList<>();
        try {
            // find yard register list
            String sql1 = "from SeizeYardRegister";
            List<SeizeYardRegister> yardRegisters = sessionFactory.getCurrentSession().createQuery(sql1).list();
            for (SeizeYardRegister yardReister : yardRegisters) {
                int seizeOrderId = yardReister.getSeizeOrderId();
                String sql3 = "from SeizeOrder where pk = :pk";
                SeizeOrder seizeOrder = (SeizeOrder) sessionFactory.getCurrentSession().createQuery(sql3).
                        setParameter("pk", seizeOrderId).uniqueResult();
                if (seizeOrder != null) {
                    LoanHeaderDetails loanHeaderDetails = loanDaoImpl.findLoanHeaderByLoanId(seizeOrder.getLoanId());
                    if (loanHeaderDetails != null) {
                        yardReister.setLoanAggNo(loanHeaderDetails.getLoanAgreementNo());
                        if (loanHeaderDetails.getDebtorHeaderDetails() != null) {
                            yardReister.setDebtorName(loanHeaderDetails.getDebtorHeaderDetails().getDebtorName());
                        }
                    }
                    yardReister.setVehicleNo(seizeOrder.getVehicleNo());
                    String sql4 = "from MSeizerDetails where seizerId = :seizerId";
                    MSeizerDetails seizer = (MSeizerDetails) sessionFactory.getCurrentSession().createQuery(sql4)
                            .setParameter("seizerId", seizeOrder.getSeizerId()).uniqueResult();
                    if (seizer != null) {
                        yardReister.setYardPerson(seizer.getSeizerName());
                    }
                    yardReister.setSeizeCode(seizeOrder.getSeizeCode());

                    yardRegisterList.add(yardReister);
                }

            }

            // find seize order issue list (pending yard register list)
            String sql5 = "from SeizeOrder where isKeyIssue = :isKeyIssue and isRenew = :isRenew";
            List<SeizeOrder> seizeOrders = sessionFactory.getCurrentSession()
                    .createQuery(sql5).setParameter("isKeyIssue", true)
                    .setParameter("isRenew", false).list();

            for (SeizeOrder seizeOrder : seizeOrders) {
                int seizeOrderId = seizeOrder.getPk();
                String seizeCode = seizeOrder.getSeizeCode();
                boolean exist = false;
                for (SeizeYardRegister yardRegister : yardRegisterList) {
                    if (yardRegister.getSeizeOrderId() == seizeOrderId) {
                        exist = true;
                    }
                }
                if (!exist) {
                    int seizerId = 0;
                    int loanId = 0;
                    String loanAggNo = "";
                    String debtorName = "";
                    String vehicleNo = "";
                    String seizerName = "";
                    loanId = seizeOrder.getLoanId();
                    vehicleNo = seizeOrder.getVehicleNo();
                    LoanHeaderDetails loanHeaderDetails = loanDaoImpl.findLoanHeaderByLoanId(seizeOrder.getLoanId());
                    if (loanHeaderDetails != null) {
                        loanAggNo = loanHeaderDetails.getLoanAgreementNo();
                        if (loanHeaderDetails.getDebtorHeaderDetails() != null) {
                            debtorName = loanHeaderDetails.getDebtorHeaderDetails().getDebtorName();
                        }
                    }

                    String sql7 = "from MSeizerDetails where seizerId = :seizerId";
                    MSeizerDetails seizer = (MSeizerDetails) sessionFactory.getCurrentSession().createQuery(sql7)
                            .setParameter("seizerId", seizeOrder.getSeizerId()).uniqueResult();
                    if (seizer != null) {
                        seizerId = seizer.getSeizerId();
                        seizerName = seizer.getSeizerName();
                    }

                    SeizeYardRegister yardRegister = new SeizeYardRegister();
                    yardRegister.setDebtorName(debtorName);
                    yardRegister.setLoanAggNo(loanAggNo);
                    yardRegister.setLoanId(loanId);
                    yardRegister.setSeizeOrderId(seizeOrderId);
                    yardRegister.setVehicleNo(vehicleNo);
                    yardRegister.setYardPerson(seizerName);
                    yardRegister.setYardOfficerId(seizerId);
                    yardRegister.setSeizeCode(seizeCode);

                    yardRegisterList.add(yardRegister);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return yardRegisterList;
    }

    @Override
    public String saveOrUpdateYardRegister(SeizeYardRegister yardRegister) {
        String msg = "OK";
        int userId = userDao.findByUserName();
        Date actionTime = Calendar.getInstance().getTime();
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        int branchId = settlementDao.getUserLogedBranch(userId);
        Date systemDate = settlementDao.getSystemDate(branchId);
        Double seizingCharge = 0.00;
        String seizingAccNo = loanDaoImpl.findLoanAccountNo(0, seizeCode);
        String seizingCommAccNo = loanDaoImpl.findLoanAccountNo(0, seizeCommissionAccount);
        try {
            if (!seizingAccNo.equals("") && !seizingCommAccNo.equals("")) {
                transaction = session.beginTransaction();
                yardRegister.setActionTime(actionTime);
                yardRegister.setUserId(userId);
                session.saveOrUpdate(yardRegister);

                //~~~~~~~~~~~add to settlement and posting settlement~~~~~~//
                if (yardRegister.getYardId() > 0) {
                    LoanHeaderDetails headerDetails = (LoanHeaderDetails) session.createCriteria(LoanHeaderDetails.class)
                            .add(Restrictions.eq("loanId", yardRegister.getLoanId())).uniqueResult();
                    seizingCharge = yardRegister.getSeizerCharge() + yardRegister.getSeizingCommission();
                    SeizeOrder seizeOrder = (SeizeOrder) session.createCriteria(SeizeOrder.class)
                            .add(Restrictions.eq("pk", yardRegister.getSeizeOrderId())).uniqueResult();
                    SettlementMain sett = new SettlementMain();
                    sett.setLoanId(yardRegister.getLoanId());
                    sett.setDebtorId(headerDetails.getDebtorHeaderDetails().getDebtorId());
                    sett.setTransactionCodeDbt("OTHR");
                    sett.setTransactionNoDbt(seizeOrder.getSeizeCode());
                    sett.setDbtAmount(seizingCharge);
                    sett.setDescription("Seizing Charge");
                    sett.setSystemDate(systemDate);
                    sett.setIsPosting(true);
                    sett.setIsPrinted(false);
                    sett.setBranchId(branchId);
                    sett.setDueDate(headerDetails.getLoanDueDate());
                    sett.setUserId(userId);
                    sett.setActionDate(actionTime);
                    session.save(sett);

                    int settId = sett.getSettlementId();
                    String refNo = loanDaoImpl.generateReferencNo(yardRegister.getLoanId(), branchId);
                    refNo = refNo + "/" + settId;

                    String sql = "from MSeizerDetails where seizerId = " + seizeOrder.getSeizerId();
                    MSeizerDetails seizerDetails = (MSeizerDetails) sessionFactory.getCurrentSession()
                            .createQuery(sql).uniqueResult();

                    Integer subTaxId = findSubTaxDetails(seizingAccNo);
                    LoanOtherCharges charges = new LoanOtherCharges();
                    charges.setLoanId(headerDetails.getLoanId());
                    charges.setAccountNo(seizerDetails.getSeizerAccountNo());
                    charges.setChargesId(subTaxId);
                    charges.setActualAmount(seizingCharge);
                    charges.setChargeAmount(seizingCharge);
                    charges.setUserId(userId);
                    charges.setIsDownPaymentCharge(0);
                    charges.setIsPaid(0);
                    charges.setActionTime(actionTime);
                    session.save(charges);
                    String debtorAcc = headerDetails.getDebtorHeaderDetails().getDebtorAccountNo();

                    //Posting Seizing Charges
                    if (!debtorAcc.equals("")) {
                        loanDaoImpl.addToPosting(seizerDetails.getSeizerAccountNo(), refNo, new BigDecimal(yardRegister.getSeizerCharge()), branchId, 1);
                        loanDaoImpl.addToPosting(seizingCommAccNo, refNo, new BigDecimal(yardRegister.getSeizingCommission()), branchId, 1);
                        loanDaoImpl.addToPosting(debtorAcc, refNo, new BigDecimal(seizingCharge), branchId, 2);
                    } else {
                        msg = "Can't Find Account No For Debtor";
                    }
                }
            } else {
                msg = "Create Accounts For Seizing Charges";

            }
            transaction.commit();
        } catch (Exception e) {
            msg = "Error in Add to Yard Register";
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return msg;
    }

    @Override
    public SeizeYardRegister findYardRegister(int seizeOrdeId) {
        SeizeYardRegister yardRegister = null;
        try {
            String sql = "from SeizeYardRegister where seizeOrderId = :seizeOrderId";
            yardRegister = (SeizeYardRegister) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("seizeOrderId", seizeOrdeId).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return yardRegister;
    }

    @Override
    public boolean saveOrUpdateYardRegisterCheckList(SeizeYardRegisterCheklist yardRegisterCheklist) {
        boolean success = false;
        int userId = userDao.findByUserName();
        Date actionTime = Calendar.getInstance().getTime();
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            yardRegisterCheklist.setActionTime(actionTime);
            yardRegisterCheklist.setUserId(userId);
            transaction = session.beginTransaction();
            session.saveOrUpdate(yardRegisterCheklist);
            transaction.commit();
            success = true;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return success;
    }

    @Override
    public List<SeizeYardRegisterCheklist> getYardRegisterCheklists(int seizeOrdeId) {
        List<SeizeYardRegisterCheklist> cheklists = null;
        try {
            String sql = "from SeizeYardRegisterCheklist where seizeOrderId = :seizeOrderId and docTypeId =:docTypeId";
            cheklists = (List<SeizeYardRegisterCheklist>) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("seizeOrderId", seizeOrdeId)
                    .setParameter("docTypeId", 1)
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cheklists;
    }

    @Override
    public SeizeYardRegisterCheklist findYardRegisterCheklist(int docId, int seizeOrdeId) {
        SeizeYardRegisterCheklist yardRegisterCheklist = null;
        try {
            String sql = "from SeizeYardRegisterCheklist where docId = :docId and seizeOrderId = :seizeOrderId";
            yardRegisterCheklist = (SeizeYardRegisterCheklist) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("docId", docId)
                    .setParameter("seizeOrderId", seizeOrdeId).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return yardRegisterCheklist;
    }

    @Override
    public List<SeizeOrder> loadSeizeOrdersByAgreementNo(String agreementNo) {
        List<SeizeOrder> seizeOrders = new ArrayList<>();
        try {
            // find created seize orders
            String sql1 = "from SeizeOrder where isKeyIssue = :isKeyIssue and isRenew = :isRenew";
            List<SeizeOrder> createdSeizeOrders = (List<SeizeOrder>) sessionFactory.getCurrentSession()
                    .createQuery(sql1).setParameter("isKeyIssue", false)
                    .setParameter("isRenew", false).list();
            for (SeizeOrder createdSeizeOrder : createdSeizeOrders) {
                LoanHeaderDetails loanHeaderDetails = loanDaoImpl.findLoanHeaderByLoanId(createdSeizeOrder.getLoanId());
                if (loanHeaderDetails != null) {
                    createdSeizeOrder.setLoanAggNo(loanHeaderDetails.getLoanAgreementNo());
                    if (loanHeaderDetails.getDebtorHeaderDetails() != null) {
                        createdSeizeOrder.setDebtorName(loanHeaderDetails.getDebtorHeaderDetails().getDebtorName());
                    }
                    seizeOrders.add(createdSeizeOrder);
                }
            }
            // find new seize orders
            String sql2 = "SELECT loan.loanId, loan.loanAgreementNo, loan.loanInstallment, debtor.debtorId, debtor.debtorName "
                    + " FROM LoanHeaderDetails  AS loan, MLoanType AS loanType, MSubLoanType AS subLoanType, DebtorHeaderDetails AS debtor "
                    + " WHERE loan.loanType = subLoanType.subLoanId AND loanType.loanTypeId = subLoanType.loanTypeId "
                    + " AND loan.debtorHeaderDetails.debtorId = debtor.debtorId AND (loan.loanAgreementNo='" + agreementNo + "' OR loan.loanBookNo='" + agreementNo + "')";
            List loanList = sessionFactory.getCurrentSession().createQuery(sql2).list();
            if (loanList.size() > 0) {
                for (Object loan : loanList) {
                    Object[] loanArr = (Object[]) loan;
                    int loanId = Integer.parseInt(loanArr[0].toString());
                    boolean exist = false;
                    for (SeizeOrder seizeOrder : seizeOrders) {
                        if (seizeOrder.getLoanId() == loanId) {
                            exist = true;
                        }
                    }
                    if (!exist) {
                        double loanInstallment = Double.parseDouble(loanArr[2].toString());
                        //System.out.println("Installment - " + loanInstallment);
                        double checkAmount = loanInstallment * 2;
                        //System.out.println("checkAMount-" + checkAmount);
                        double loanArrears = dayEndDaoImpl.findBalanceWithOverPay(loanId);
                        //System.out.println("loanArrears-" + loanArrears);
                        if (loanArrears > checkAmount) {
                            int totInstallmentCount = 0;
                            int paidInstallmentCount = 0;
                            int remainInstallmentCount = 0;
                            String sql4 = "SELECT COUNT(*) FROM LoanInstallment WHERE loanId=" + loanId;
                            Object result2 = sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
                            if (result2 != null) {
                                totInstallmentCount = Integer.parseInt(result2.toString());
                            }
                            //System.out.println("totInstallmentCount-" + totInstallmentCount);
                            String sql5 = "SELECT COUNT(*) FROM SettlementMain WHERE transactionCodeCrdt='" + refNoInstallmentCode + "' AND loanId=" + loanId;
                            Object result3 = sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();
                            if (result3 != null) {
                                paidInstallmentCount = Integer.parseInt(result3.toString());
                            }
                            //System.out.println("paidInstallmentCount-" + paidInstallmentCount);
                            remainInstallmentCount = totInstallmentCount - paidInstallmentCount;
                            //System.out.println("remainInstallmentCount-" + remainInstallmentCount);

                            SeizeOrder seizeOrder = new SeizeOrder();
                            seizeOrder.setLoanId(loanId);
                            seizeOrder.setLoanAggNo(loanArr[1].toString());
                            seizeOrder.setDebtorId(Integer.parseInt(loanArr[3].toString()));
                            seizeOrder.setDebtorName(loanArr[4].toString());
                            seizeOrder.setArrearsRental(loanArr[2].toString() + " x " + remainInstallmentCount);

                            seizeOrders.add(seizeOrder);
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return seizeOrders;
    }

    @Override
    public SeizeYardRegisterCheklist getYardRegisterInspectionReport(int seizeOrdeId) {
        SeizeYardRegisterCheklist seizeYardRegisterReport = null;
        try {
            String sql = "from SeizeYardRegisterCheklist where seizeOrderId = :seizeOrderId and docTypeId = :docTypeId";
            seizeYardRegisterReport = (SeizeYardRegisterCheklist) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("seizeOrderId", seizeOrdeId)
                    .setParameter("docTypeId", 2)
                    .uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seizeYardRegisterReport;
    }

    public int findSubTaxDetails(String seizingAccNo) {
        int taxId = 0;
        try {
            MSubTaxCharges charges = (MSubTaxCharges) sessionFactory.getCurrentSession().createCriteria(MSubTaxCharges.class)
                    .add(Restrictions.eq("subTaxAccountNo", seizingAccNo)).uniqueResult();
            taxId = charges.getSubTaxId();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return taxId;
    }

    @Override
    public List<SeizeOrder> getRenewalSeizeOrders() {
        List<SeizeOrder> renewalSeizeOrders = new ArrayList<>();
        try {
            String sql = "from SeizeOrder where isKeyIssue = :isKeyIssue and isRenew = :isRenew";
            List<SeizeOrder> keyIssueList = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("isKeyIssue", true)
                    .setParameter("isRenew", false)
                    .list();
            if (keyIssueList.size() > 0) {
                for (SeizeOrder keyIssueSOrder : keyIssueList) {
                    String sql2 = "from SeizeYardRegister where seizeOrderId = :seizeOrderId";
                    SeizeYardRegister yardRegister = (SeizeYardRegister) sessionFactory.getCurrentSession().createQuery(sql2).
                            setParameter("seizeOrderId", keyIssueSOrder.getPk()).uniqueResult();
                    if (yardRegister == null) {
                        Date keyIssueDate = keyIssueSOrder.getKeyIssueDate();
                        Date toDayDate = Calendar.getInstance().getTime();
                        long diff = toDayDate.getTime() - keyIssueDate.getTime();
                        long diffDays = diff / (24 * 60 * 60 * 1000);
                        if (diffDays >= 14) { // check validity period (14 days)

                            SeizeOrder renewalSeizeOrder = new SeizeOrder();
                            renewalSeizeOrder.setApproval_1(keyIssueSOrder.getApproval_1());
                            renewalSeizeOrder.setApproval_2(keyIssueSOrder.getApproval_2());
                            renewalSeizeOrder.setArrearsRental(keyIssueSOrder.getArrearsRental());
                            renewalSeizeOrder.setDebtorId(keyIssueSOrder.getDebtorId());
                            renewalSeizeOrder.setIsDefault(keyIssueSOrder.getIsDefault());
                            renewalSeizeOrder.setIsDelete(keyIssueSOrder.getIsDelete());
                            renewalSeizeOrder.setIsKeyIssue(keyIssueSOrder.getIsKeyIssue());
                            renewalSeizeOrder.setIsRenew(true);
                            renewalSeizeOrder.setKeyIssueDate(keyIssueSOrder.getKeyIssueDate());
                            renewalSeizeOrder.setLoanId(keyIssueSOrder.getLoanId());
                            renewalSeizeOrder.setOrderDate(keyIssueSOrder.getOrderDate());
                            renewalSeizeOrder.setSeizerId(keyIssueSOrder.getSeizerId());
                            renewalSeizeOrder.setVehicleNo(keyIssueSOrder.getVehicleNo());
                            LoanHeaderDetails loanHeaderDetails = loanDaoImpl.findLoanHeaderByLoanId(keyIssueSOrder.getLoanId());
                            if (loanHeaderDetails != null) {
                                renewalSeizeOrder.setLoanAggNo(loanHeaderDetails.getLoanAgreementNo());
                                if (loanHeaderDetails.getDebtorHeaderDetails() != null) {
                                    renewalSeizeOrder.setDebtorName(loanHeaderDetails.getDebtorHeaderDetails().getDebtorName());
                                }
                            }
                            String sql3 = "from MSeizerDetails where seizerId = :seizerId";
                            MSeizerDetails seizer = (MSeizerDetails) sessionFactory.getCurrentSession().createQuery(sql3)
                                    .setParameter("seizerId", keyIssueSOrder.getSeizerId()).uniqueResult();
                            if (seizer != null) {
                                renewalSeizeOrder.setSerizerName(seizer.getSeizerName());
                            }

                            renewalSeizeOrders.add(renewalSeizeOrder);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return renewalSeizeOrders;
    }

    @Override
    public List<SeizeOrder> getSeizeOrderHistory(int loanId) {
        List<SeizeOrder> seizeOrderList = new ArrayList<>();
        try {
            String sql1 = "from SeizeOrder where loanId = :loanId";
            List<SeizeOrder> list = sessionFactory.getCurrentSession().createQuery(sql1)
                    .setParameter("loanId", loanId).list();
            if (list.size() > 0) {
                for (SeizeOrder seizeOrder : list) {
                    LoanHeaderDetails loanHeaderDetails = loanDaoImpl.findLoanHeaderByLoanId(seizeOrder.getLoanId());
                    if (loanHeaderDetails != null) {
                        seizeOrder.setLoanAggNo(loanHeaderDetails.getLoanAgreementNo());
                        if (loanHeaderDetails.getDebtorHeaderDetails() != null) {
                            seizeOrder.setDebtorName(loanHeaderDetails.getDebtorHeaderDetails().getDebtorName());
                        }
                    }
                    String sql2 = "from MSeizerDetails where seizerId = :seizerId";
                    MSeizerDetails seizer = (MSeizerDetails) sessionFactory.getCurrentSession().createQuery(sql2)
                            .setParameter("seizerId", seizeOrder.getSeizerId()).uniqueResult();
                    if (seizer != null) {
                        seizeOrder.setSerizerName(seizer.getSeizerName());
                    }

                    seizeOrderList.add(seizeOrder);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seizeOrderList;
    }

    @Override
    public boolean deleteYardRegister(int yardRegId) {
        boolean delete = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            // find Yard Register
            String sql1 = "from SeizeYardRegister where yardId = :yardId";
            SeizeYardRegister yardRegister = (SeizeYardRegister) session.createQuery(sql1).setParameter("yardId", yardRegId).uniqueResult();
            // find Seize Order 
            if (yardRegister != null) {
                Integer seizeOrderId = yardRegister.getSeizeOrderId();
                String sql2 = "from SeizeOrder where pk = :pk and isKeyIssue= :isKeyIssue and isRenew = :isRenew";
                SeizeOrder seizeOrder = (SeizeOrder) session.createQuery(sql2)
                        .setParameter("pk", seizeOrderId)
                        .setParameter("isKeyIssue", true)
                        .setParameter("isRenew", false)
                        .uniqueResult();
                // find SettlementMain
                if (seizeOrder != null) {
                    Integer loanId = seizeOrder.getLoanId();
                    Integer debtorId = seizeOrder.getDebtorId();
                    String sezCode = seizeOrder.getSeizeCode();
                    String sql3 = "from SettlementMain where loanId = :loanId and debtorId = :debtorId and transactionNoDbt = :transactionNoDbt";
                    SettlementMain settlementMain = (SettlementMain) session.createQuery(sql3)
                            .setParameter("loanId", loanId)
                            .setParameter("debtorId", debtorId)
                            .setParameter("transactionNoDbt", sezCode)
                            .uniqueResult();
                    // find Posting entries
                    if (settlementMain != null) {
                        Integer settlementId = settlementMain.getSettlementId();
                        DebtorHeaderDetails debtorHeaderDetails = loanDaoImpl.findDebtorHeader(debtorId);
                        // find Debtor Account No and Posting entry
                        if (debtorHeaderDetails != null) {
                            String debtorAccountNo = debtorHeaderDetails.getDebtorAccountNo();
                            String sql4 = "from Posting where referenceNo like :referenceNo and accountNo = :accountNo";
                            Posting debtorPosting = (Posting) session.createQuery(sql4).setParameter("referenceNo", "%" + settlementId)
                                    .setParameter("accountNo", debtorAccountNo).uniqueResult();
                            if (debtorPosting != null) {
                                session.delete(debtorPosting); // delete debtor posting entry
                            }
                        }
                        // find Seizing Account No and Posting entry
                        String seizingAccNo = loanDaoImpl.findLoanAccountNo(0, seizeCode);
                        String sql5 = "from Posting where referenceNo like :referenceNo and accountNo = :accountNo";
                        Posting seizeChargePosting = (Posting) session.createQuery(sql5).setParameter("referenceNo", "%" + settlementId)
                                .setParameter("accountNo", seizingAccNo).uniqueResult();
                        if (seizeChargePosting != null) {
                            session.delete(seizeChargePosting); // delete seize charge posting entry
                        }
                        // find Seizing Commision Account and Posting entry
                        String seizingCommAccNo = loanDaoImpl.findLoanAccountNo(0, seizeCommissionAccount);
                        String sql6 = "from Posting where referenceNo like :referenceNo and accountNo = :accountNo";
                        Posting seizeCommPosting = (Posting) session.createQuery(sql6).setParameter("referenceNo", "%" + settlementId)
                                .setParameter("accountNo", seizingCommAccNo).uniqueResult();
                        if (seizeCommPosting != null) {
                            session.delete(seizeCommPosting); // delete seize commission posting entry
                        }
                        // find Loan Other Charges
                        String sql7 = "from LoanOtherCharges where accountNo = :accountNo and loanId = :loanId";
                        LoanOtherCharges otherCharges = (LoanOtherCharges) session.createQuery(sql7).setParameter("accountNo", seizingAccNo)
                                .setParameter("loanId", loanId).uniqueResult();
                        if (otherCharges != null) {
                            session.delete(otherCharges); // delete loan other charges
                        }

                        session.delete(settlementMain); // delete settlement 
                    }
                    // delete Seize Order Approval
                    String sql8 = "delete from SeizeOrderApproval where seizeOrderId = :seizeOrderId";
                    session.createQuery(sql8).setParameter("seizeOrderId", seizeOrderId).executeUpdate();
                    // find Yard Register Check List and delete from storage
                    String sql9 = "from SeizeYardRegisterCheklist where seizeOrderId = :seizeOrderId";
                    List<SeizeYardRegisterCheklist> list = session.createQuery(sql9).setParameter("seizeOrderId", seizeOrderId).list();
                    if (list.size() > 0) {
                        for (SeizeYardRegisterCheklist cheklist : list) {
                            File file = new File(cheklist.getFilePath());
                            if (file.exists()) {
                                file.delete();
                            }
                        }
                    }
                    // delete Yard Register Check List from db
                    String sql10 = "delete from SeizeYardRegisterCheklist where seizeOrderId = :seizeOrderId";
                    session.createQuery(sql10).setParameter("seizeOrderId", seizeOrderId).executeUpdate();
                    // delete Yard Register
                    session.delete(yardRegister);
                    // update Seize Order
                    seizeOrder.setIsKeyIssue(false);
                    seizeOrder.setKeyIssueDate(null);
                    seizeOrder.setIsRenew(false);
                    seizeOrder.setApproval_1(0);
                    seizeOrder.setApproval_2(0);
                    session.update(seizeOrder);
                    transaction.commit();
                    delete = true;
                }
            }
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return delete;
    }

    @Override
    public boolean reverseYardRegister(int seizeOrderId) {
        boolean reverse = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            // find Seize Order
            String sql1 = "from SeizeOrder where pk = :pk and isKeyIssue= :isKeyIssue and isRenew = :isRenew";
            SeizeOrder seizeOrder = (SeizeOrder) session.createQuery(sql1)
                    .setParameter("pk", seizeOrderId)
                    .setParameter("isKeyIssue", true)
                    .setParameter("isRenew", false)
                    .uniqueResult();
            if (seizeOrder != null) {
                // delete Seize Order Approval
                String sql2 = "delete from SeizeOrderApproval where seizeOrderId = :seizeOrderId";
                session.createQuery(sql2).setParameter("seizeOrderId", seizeOrder.getPk()).executeUpdate();
                // update Seize Order
                seizeOrder.setIsKeyIssue(false);
                seizeOrder.setKeyIssueDate(null);
                seizeOrder.setIsRenew(false);
                seizeOrder.setApproval_1(0);
                seizeOrder.setApproval_2(0);
                session.update(seizeOrder);
                transaction.commit();
                reverse = true;
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return reverse;
    }

    @Override
    public List<LoanPropertyVehicleDetails> findByLoan(String agreementNo) {
        List<LoanPropertyVehicleDetails> vDetailses1 = new ArrayList<>();
        try {
            String sql1 = "select loanId from LoanHeaderDetails where loanAgreementNo like '%" + agreementNo + "%'";
            List loanIds = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (!loanIds.isEmpty()) {
                String sql2 = "from LoanPropertyVehicleDetails where loanId in (:loanId) and isActive = :isActive";
                List<LoanPropertyVehicleDetails> vDetailses2 = sessionFactory.getCurrentSession()
                        .createQuery(sql2)
                        .setParameterList("loanId", loanIds)
                        .setParameter("isActive", true).list();
                if (!vDetailses2.isEmpty()) {
                    for (LoanPropertyVehicleDetails vDetails : vDetailses2) {
                        int vMakeId = Integer.valueOf(vDetails.getVehicleMake());
                        int vModelId = Integer.valueOf(vDetails.getVehicleModel());
                        int vTypeId = Integer.valueOf(vDetails.getVehicleType());
                        vDetails.setLoanHeaderDetails(loanDaoImpl.findLoanHeaderDetails(vDetails.getLoanId()));
                        vDetails.setvMake(masterDataService.findVehicleMakeById(vMakeId));
                        vDetails.setvModel(masterDataService.findVehicleModelById(vModelId));
                        vDetails.setvType(masterDataService.findVehicleTypeById(vTypeId));
                        vDetailses1.add(vDetails);
                    }
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vDetailses1;
    }

    @Override
    public List<LoanPropertyVehicleDetails> findByVehicleNo(String vehicleNo) {
        List<LoanPropertyVehicleDetails> vDetailses1 = new ArrayList<>();
        try {
            String sql = "from LoanPropertyVehicleDetails where vehicleRegNo like :vehicleRegNo and isActive = :isActive";
            List<LoanPropertyVehicleDetails> vDetailses2 = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("vehicleRegNo", "%" + vehicleNo + "%")
                    .setParameter("isActive", true).list();
            if (!vDetailses2.isEmpty()) {
                for (LoanPropertyVehicleDetails vDetails : vDetailses2) {
                    int vMakeId = Integer.valueOf(vDetails.getVehicleMake());
                    int vModelId = Integer.valueOf(vDetails.getVehicleModel());
                    int vTypeId = Integer.valueOf(vDetails.getVehicleType());
                    vDetails.setLoanHeaderDetails(loanDaoImpl.findLoanHeaderDetails(vDetails.getLoanId()));
                    vDetails.setvMake(masterDataService.findVehicleMakeById(vMakeId));
                    vDetails.setvModel(masterDataService.findVehicleModelById(vModelId));
                    vDetails.setvType(masterDataService.findVehicleTypeById(vTypeId));
                    vDetailses1.add(vDetails);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vDetailses1;
    }

    @Override
    public List<LoanPropertyVehicleDetails> findByVehicleEngineNo(String engineNo) {
        List<LoanPropertyVehicleDetails> vDetailses1 = new ArrayList<>();
        try {
            String sql = "from LoanPropertyVehicleDetails where vehicleEngineNo like :vehicleEngineNo and isActive = :isActive";
            List<LoanPropertyVehicleDetails> vDetailses2 = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("vehicleEngineNo", "%" + engineNo + "%")
                    .setParameter("isActive", true).list();
            if (!vDetailses2.isEmpty()) {
                for (LoanPropertyVehicleDetails vDetails : vDetailses2) {
                    int vMakeId = Integer.valueOf(vDetails.getVehicleMake());
                    int vModelId = Integer.valueOf(vDetails.getVehicleModel());
                    int vTypeId = Integer.valueOf(vDetails.getVehicleType());
                    vDetails.setLoanHeaderDetails(loanDaoImpl.findLoanHeaderDetails(vDetails.getLoanId()));
                    vDetails.setvMake(masterDataService.findVehicleMakeById(vMakeId));
                    vDetails.setvModel(masterDataService.findVehicleModelById(vModelId));
                    vDetails.setvType(masterDataService.findVehicleTypeById(vTypeId));
                    vDetailses1.add(vDetails);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vDetailses1;
    }

    @Override
    public List<LoanPropertyVehicleDetails> findByVehicleChassisNo(String chassisNo) {
        List<LoanPropertyVehicleDetails> vDetailses1 = new ArrayList<>();
        try {
            String sql = "from LoanPropertyVehicleDetails where vehiclChassisNo like :vehiclChassisNo and isActive = :isActive";
            List<LoanPropertyVehicleDetails> vDetailses2 = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("vehiclChassisNo", "%" + chassisNo + "%")
                    .setParameter("isActive", true).list();
            if (!vDetailses2.isEmpty()) {
                for (LoanPropertyVehicleDetails vDetails : vDetailses2) {
                    int vMakeId = Integer.valueOf(vDetails.getVehicleMake());
                    int vModelId = Integer.valueOf(vDetails.getVehicleModel());
                    int vTypeId = Integer.valueOf(vDetails.getVehicleType());
                    vDetails.setLoanHeaderDetails(loanDaoImpl.findLoanHeaderDetails(vDetails.getLoanId()));
                    vDetails.setvMake(masterDataService.findVehicleMakeById(vMakeId));
                    vDetails.setvModel(masterDataService.findVehicleModelById(vModelId));
                    vDetails.setvType(masterDataService.findVehicleTypeById(vTypeId));
                    vDetailses1.add(vDetails);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vDetailses1;
    }

    @Override
    public List loadLetterPostingByLoan(String agreementNo) {
        List loanList = new ArrayList<>();
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        try {
            String sql = "from RecoveryLetterDetail where agreementNo like :agreementNo";
            List<RecoveryLetterDetail> rlds = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("agreementNo", "%" + agreementNo + "%").list();
            if (!rlds.isEmpty()) {
                for (RecoveryLetterDetail letterDetail : rlds) {
                    RecoveryLoanList loanR = new RecoveryLoanList();
                    loanR.setLoanId(letterDetail.getLoanId());
                    loanR.setArresAmount(reportDaoImpl.findLoanBalance(loanR.getLoanId()));
                    loanR.setAgreementNo(letterDetail.getAgreementNo());
                    loanR.setDueDate(dateFormat.format(letterDetail.getDueDate()));
                    loanR.setOdi(letterDetail.getLetterCharge());
                    LoanHeaderDetails lhd = loanDao.findLoanHeaderByLoanId(letterDetail.getLoanId());
                    if (lhd != null) {
                        loanR.setDebtorName(lhd.getDebtorHeaderDetails().getNameWithInitial());
                    }
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(letterDetail.getLoanId());
                    if (lpvd != null) {
                        loanR.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    loanR.setLetteId(letterDetail.getLetterId());
                    RecoveryLetters rl = findLetter(letterDetail.getLetterId());
                    if (rl != null) {
                        loanR.setLetterName(rl.getLetterName());
                        loanR.setColor(rl.getColor());
                    }
                    loanList.add(loanR);
                }
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public List loadLetterPostingByVehicle(String vehicleNo) {
        List loanList = new ArrayList<>();
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        try {
            String sql1 = "select loanId from LoanPropertyVehicleDetails where vehicleRegNo like :vehicleRegNo";
            List loanIds = sessionFactory.getCurrentSession().createQuery(sql1)
                    .setParameter("vehicleRegNo", "%" + vehicleNo + "%").list();
            if (!loanIds.isEmpty()) {
                String sql2 = "from RecoveryLetterDetail where loanId in (:loanId)";
                List<RecoveryLetterDetail> rlds = sessionFactory.getCurrentSession().createQuery(sql2)
                        .setParameterList("loanId", loanIds).list();
                if (!rlds.isEmpty()) {
                    for (RecoveryLetterDetail letterDetail : rlds) {
                        RecoveryLoanList loanR = new RecoveryLoanList();
                        loanR.setLoanId(letterDetail.getLoanId());
                        loanR.setArresAmount(reportDaoImpl.findLoanBalance(loanR.getLoanId()));
                        loanR.setAgreementNo(letterDetail.getAgreementNo());
                        loanR.setDueDate(dateFormat.format(letterDetail.getDueDate()));
                        loanR.setOdi(letterDetail.getLetterCharge());
                        LoanHeaderDetails lhd = loanDao.findLoanHeaderByLoanId(letterDetail.getLoanId());
                        if (lhd != null) {
                            loanR.setDebtorName(lhd.getDebtorHeaderDetails().getNameWithInitial());
                        }
                        LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(letterDetail.getLoanId());
                        if (lpvd != null) {
                            loanR.setVehicleNo(lpvd.getVehicleRegNo());
                        }
                        loanR.setLetteId(letterDetail.getLetterId());
                        RecoveryLetters rl = findLetter(letterDetail.getLetterId());
                        if (rl != null) {
                            loanR.setLetterName(rl.getLetterName());
                            loanR.setColor(rl.getColor());
                        }
                        loanList.add(loanR);
                    }
                }
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public RecoveryLetters findLetter(int letterId) {
        RecoveryLetters rl = null;
        try {
            String sql = "from RecoveryLetters where letterId = :letterId";
            rl = (RecoveryLetters) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("letterId", letterId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return rl;
    }

    @Override
    public List<DirectDebitCharge> findDirectDebitCharges(int loanId) {
        List<DirectDebitCharge> debitCharges = new ArrayList<>();
        try {
            String sql = "select loc.chargeId, sm.settlementId, loc.loanId, sm.systemDate, loc.supplierId, sm.description, sm.dbtAmount, loc.isPaid "
                    + "from SettlementMain as sm, LoanOtherCharges as loc where sm.loanId = loc.loanId and loc.isDirectDebit = 1"
                    + " and sm.chargeId = loc.chargesId and loc.loanId = " + loanId;
            List list = sessionFactory.getCurrentSession().createQuery(sql).list();
            for (Object ob1 : list) {
                Object[] ob2 = (Object[]) ob1;
                if (ob2 != null) {
                    DirectDebitCharge debitCharge = new DirectDebitCharge();
                    if (ob2[0] != null) {
                        debitCharge.setChargeId(Integer.parseInt(ob2[0].toString()));
                    }
                    if (ob2[1] != null) {
                        debitCharge.setSettlementId(Integer.parseInt(ob2[1].toString()));
                    }
                    if (ob2[2] != null) {
                        debitCharge.setLoanId(Integer.parseInt(ob2[2].toString()));
                    }
                    if (ob2[3] != null) {
                        debitCharge.setDebitDate(ob2[3].toString());
                    }
                    if (ob2[4] != null) {
                        int supplierId = Integer.parseInt(ob2[4].toString());
                        MSupplier supplier = adminDaoImpl.findSupplier(supplierId);
                        if (supplier != null) {
                            debitCharge.setSupplierName(supplier.getSupplierName());
                        }
                    }
                    if (ob2[5] != null) {
                        debitCharge.setDescription(ob2[5].toString());
                    }
                    if (ob2[6] != null) {
                        debitCharge.setDebitAmount(Double.parseDouble(ob2[6].toString()));
                    }
                    if (ob2[7] != null) {
                        debitCharge.setIsPaid(Integer.parseInt(ob2[7].toString()));
                    }

                    debitCharges.add(debitCharge);
                }
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return debitCharges;
    }

    @Override
    public boolean deleteDirectDebitCharge(int chargeId, int settlementId) {
        boolean flag = false;
        DateFormat dtf = new SimpleDateFormat("yyyyMMdd");
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql1 = "delete from LoanOtherCharges where chargeId = " + chargeId;
            session.createQuery(sql1).executeUpdate();
            String sql2 = "from SettlementMain where settlementId = " + settlementId;
            SettlementMain sm = (SettlementMain) session.createQuery(sql2).uniqueResult();
            if (sm != null) {
                int loanId = sm.getLoanId();
                String datePart = "";
                Date systemDate = sm.getSystemDate();
                if (systemDate != null) {
                    datePart = dtf.format(systemDate);
                }
                String referenceNo = datePart + loanId + "/" + settlementId;
                String sql3 = "delete from Posting where referenceNo = '" + referenceNo + "'";
                session.createQuery(sql3).executeUpdate();
                String sql4 = "delete from SettlementMain where settlementId = " + settlementId;
                session.createQuery(sql4).executeUpdate();
            }
            tx.commit();
            flag = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flag;
    }

    @Override
    public List<Mail> findMails(int letterTypeId) {
        List<Mail> mailList = new ArrayList<>();
        try {
            String qry1 = "from RecoveryLetterDetail where letterId = " + letterTypeId;
            List<RecoveryLetterDetail> letterDetails = sessionFactory.getCurrentSession().createQuery(qry1).list();
            for (RecoveryLetterDetail letterDetail : letterDetails) {
                LoanHeaderDetails lhd = loanDao.findLoanHeaderDetails(letterDetail.getLoanId());
                if (lhd != null) {
                    int loanId = lhd.getLoanId();
                    Mail mail = new Mail();
                    mail.setLoanId(loanId);
                    mail.setLetterId(letterDetail.getLetterId());
                    mail.setAgreementNo(lhd.getLoanAgreementNo());
                    mail.setClientName(lhd.getDebtorHeaderDetails().getDebtorName());
                    mail.setClientAddress(lhd.getDebtorHeaderDetails().getDebtorPersonalAddress());
                    String qry2 = "from LoanGuaranteeDetails where loanId = " + loanId;
                    List<LoanGuaranteeDetails> lgds = sessionFactory.getCurrentSession().createQuery(qry2).list();
                    if (lgds != null) {
                        switch (lgds.size()) {
                            case 3: {
                                DebtorHeaderDetails gr1 = debtorDaoImpl.findDebtor(lgds.get(0).getDebtorId());
                                if (gr1 != null) {
                                    mail.setGuarantor1(gr1.getNameWithInitial());
                                    mail.setGuarantor1Address(gr1.getDebtorPersonalAddress());
                                }
                                DebtorHeaderDetails gr2 = debtorDaoImpl.findDebtor(lgds.get(1).getDebtorId());
                                if (gr2 != null) {
                                    mail.setGuarantor2(gr2.getNameWithInitial());
                                    mail.setGuarantor2Address(gr2.getDebtorPersonalAddress());
                                }
                                DebtorHeaderDetails gr3 = debtorDaoImpl.findDebtor(lgds.get(2).getDebtorId());
                                if (gr3 != null) {
                                    mail.setGuarantor3(gr3.getNameWithInitial());
                                    mail.setGuarantor3Address(gr3.getDebtorPersonalAddress());
                                }
                                break;
                            }
                            case 2: {
                                DebtorHeaderDetails gr1 = debtorDaoImpl.findDebtor(lgds.get(0).getDebtorId());
                                if (gr1 != null) {
                                    mail.setGuarantor1(gr1.getNameWithInitial());
                                    mail.setGuarantor1Address(gr1.getDebtorPersonalAddress());
                                }
                                DebtorHeaderDetails gr2 = debtorDaoImpl.findDebtor(lgds.get(1).getDebtorId());
                                if (gr2 != null) {
                                    mail.setGuarantor2(gr2.getNameWithInitial());
                                    mail.setGuarantor2Address(gr2.getDebtorPersonalAddress());
                                }
                                break;
                            }
                            case 1: {
                                DebtorHeaderDetails gr1 = debtorDaoImpl.findDebtor(lgds.get(0).getDebtorId());
                                if (gr1 != null) {
                                    mail.setGuarantor1(gr1.getDebtorName());
                                    mail.setGuarantor1Address(gr1.getDebtorPersonalAddress());
                                }
                                break;
                            }
                            default:
                                mail.setGuarantor1("");
                                mail.setGuarantor1Address("");
                                mail.setGuarantor2("");
                                mail.setGuarantor2Address("");
                                mail.setGuarantor3("");
                                mail.setGuarantor3Address("");
                                break;
                        }
                    }
                    mailList.add(mail);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return mailList;
    }

    @Override
    public List<RecoveryLetters> findLetters(boolean flag) {
        List<RecoveryLetters> rls = null;
        try {
            String sql = "from RecoveryLetters where isActive = :isActive";
            rls = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("isActive", flag).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return rls;
    }

    @Override
    public List<viewLoan> findSearchLoanListforReport(String memNo) {
        List<viewLoan> loanList = new ArrayList();
        List<LoanHeaderDetails> loans = new ArrayList();
        try {
            String where = "";
            if (!memNo.equals("")) {
                where = "(lh.loanAgreementNo like '" + memNo + "%' or lh.loanBookNo like '%" + memNo + "%')";
                String sql1 = "from LoanHeaderDetails as lh where " + where;
                loans = sessionFactory.getCurrentSession().createQuery(sql1).list();
            }

            if (!loans.isEmpty()) {
                for (int i = 0; i < loans.size(); i++) {
                    LoanHeaderDetails loanH = (LoanHeaderDetails) loans.get(i);
                    viewLoan loan = new viewLoan();
                    loan.setAgreementNo(loanH.getLoanAgreementNo());
                    loan.setLoanBookNo(loanH.getLoanBookNo());
                    loan.setLoanAmount(loanH.getLoanAmount().doubleValue());
                    loan.setLoanId(loanH.getLoanId());
                    int periodType = loanH.getLoanPeriodType();
                    String period = "";
                    if (periodType == 1) {
                        period = loanH.getLoanPeriod() + " Month";
                    } else if (periodType == 2) {
                        period = loanH.getLoanPeriod() + " Years";
                    } else if (periodType == 3) {
                        period = loanH.getLoanPeriod() + " Days";
                    } else if (periodType == 4) {
                        period = loanH.getLoanPeriod() + " Weeks";
                    } else if (periodType == 5) {
                        period = " Laps";
                    } else if (periodType == 6) {
                        period = " Legal";
                    } else {
                        period = " Seize";
                    }
                    loan.setLoanPeriod(period);
                    loan.setLoanDate(loanH.getLoanDate());
                    DebtorHeaderDetails debtor = loanH.getDebtorHeaderDetails();
                    loan.setDebtorName(debtor.getNameWithInitial());
                    loan.setDebtorAddress(debtor.getDebtorPersonalAddress());
                    loan.setLoanSpec(loanH.getLoanSpec());
                    loan.setLoanBookNo(loanH.getLoanBookNo());
                    loan.setLoanCount(loanH.getLoanCount());
//                    if (debtor.getDebtorTelephoneMobile() != null || !debtor.getDebtorTelephoneMobile().trim().equals("")) {
//                        loan.setDebtorTelephone(Integer.parseInt(debtor.getDebtorTelephoneMobile()));
//                    }
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanH.getLoanId());
                    if (lpvd != null) {
                        loan.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    loanList.add(loan);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

}

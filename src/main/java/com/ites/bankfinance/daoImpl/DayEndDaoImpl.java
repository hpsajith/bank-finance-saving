/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.ites.bankfinance.dao.DayEndDao;
import com.ites.bankfinance.dao.SettlementDao;
import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.model.DayEndConfig;
import com.ites.bankfinance.model.DayEndDate;
import com.ites.bankfinance.model.DayEndFloatBalance;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.InsuranceLateDates;
import com.ites.bankfinance.model.InsuranceOtherCharge;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanOtherCharges;
import com.ites.bankfinance.model.MBankDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MOverDue;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.OpenD;
import com.ites.bankfinance.model.OpenH;
import com.ites.bankfinance.model.Posting;
import com.ites.bankfinance.model.RebateDetail;
import com.ites.bankfinance.model.Serverdetails;
import com.ites.bankfinance.model.SettlementCharges;
import com.ites.bankfinance.model.SettlementMain;
import com.ites.bankfinance.model.SettlementPaymentDetailBankDeposite;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementPaymentOther;
import com.ites.bankfinance.model.SettlementPayments;
import com.ites.bankfinance.model.SmsDetails;
import com.ites.bankfinance.model.SmsType;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserBranch;
import com.ites.bankfinance.model.UmUserLog;
import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Uththara
 */
@Repository
public class DayEndDaoImpl implements DayEndDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDao userDao;

    @Autowired
    private SettlementDao settlmentDao;

    @Autowired
    private LoanDaoImpl loanDaoImpl;

    @Autowired
    private ReportDaoImpl reportDaoImpl;

    @Autowired
    private RecoveryDaoImpl recoveryDaoImpl;

    @Autowired
    private AdminDaoImpl adminDaoImpl;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    String odiMsg = "";
    String installMsg = "";
    String cheqMsg = "";
    String reverseOdiMsg = "";

    private final String REBATE_REF_CODE = "RBT";
    private final String CASH_IN_HAND_ACC_CODE = "CASH_IN_HAND";
    public final String CHEQUE_IN_HAND_ACC_CODE = "CHEQUE_IN_HAND";
    private final String INSTALLMENT_COLLECTION_ACC_CODE = "RECE";
    public final String CASH_BOOK_ACC_CODE = "PAYBLE";
    public final String EXSS_MONEY_ACCOUNT = "EXSS";
    private final String OVP_ACCOUNT = "OVP";
    public static final String INSURANCE_LATE_CHARGE_CODE = "ILC";
    DecimalFormat df2 = new DecimalFormat("###.##");

    @Override
    public String dayEndProcess(int branchId) {
        Date dayEndDate = null, dueDate = null;
        String msg = "OK";

        try {
            int USERID = userDao.findByUserName();

            if (branchId == 0) {
                findBranchByUser(USERID);
            }

            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                dayEndDate = obj;
            }
            changeBlockStatus(dayEndDate, true);
            String sql2 = "SELECT dp.processId FROM DayEndProcess AS dp WHERE dp.processActive=1 and dp.processId NOT IN"
                    + " (SELECT dc.processId FROM DayEndConfig AS dc WHERE dc.dayEndDate = '" + dayEndDate + "' AND dc.branchId = " + branchId + " AND dc.processStatus=1)";

            List pendingProcess = sessionFactory.getCurrentSession().createQuery(sql2).list();
            if (pendingProcess.size() > 0) {
                boolean backupBefore = backupDataWithDatabase(0, branchId);
                if (!backupBefore) {
                    return "Cannot BackUp Database";
                }
                boolean doneIns = false, doneOdi = false;
                for (int i = 0; i < pendingProcess.size(); i++) {
                    Object obj2 = pendingProcess.get(i);
                    if (obj2 != null) {
                        int procId = Integer.parseInt(obj2.toString());
                        if (procId == 1) {
                            //calculate installment
                            boolean proc1 = calculateInstallment(branchId, dayEndDate);
                            if (proc1) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                                doneIns = true;
                            } else {
                                if (!installMsg.equals("")) {
                                    msg = installMsg;
                                } else {
                                    msg = "Rental calculation failure...!";
                                }
                                installMsg = "";
                                return msg;
                            }
                        } else if (procId == 2) {
                            //calculate odi
                            boolean proc1 = calculateOdi(branchId, dayEndDate);
                            if (proc1) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                                doneOdi = true;
                            } else {
                                if (!odiMsg.equals("")) {
                                    msg = odiMsg;
                                } else {
                                    msg = "ODI calculation failure...!";
                                }
                                odiMsg = "";
                                return msg;
                            }
//                        } else if (procId == 3) { //create other day endProcess
////                            if (doneIns && doneOdi) {
//                                boolean proc1 = findLoanOverPayment(branchId, dayEndDate);
//                                if (proc1) {
//                                    updateProcessStatus(procId, dayEndDate, branchId);
//                                } else {
//                                    msg = "Add OverPayments Process Failed";
//                                    return msg;
//                                }
//                            }
                        } else if (procId == 4) {
                            //posting other insurance
                            boolean proc1 = postingInsurance(branchId, dayEndDate);
                            if (proc1) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return "Posting Insurance Error";
                            }
                        } else if (procId == 5) {
                            //posting other payments
                            boolean proc1 = postingOtherPayment(branchId, dayEndDate);
                            if (proc1) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return "Posting Error In other Payments";
                            }
                        } else if (procId == 6) {
                            boolean proc1 = postingReturnCheq(branchId, dayEndDate);
                            if (proc1) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                if (!cheqMsg.equals("")) {
                                    msg = cheqMsg;
                                } else {
                                    msg = "Cannot Posting Return Cheqs";
                                }
                                cheqMsg = "";
                                return msg;
                            }
                        } else if (procId == 7) {
                            // posting Settlement
                            String proc1 = postingSettlement(branchId, dayEndDate);
                            if (proc1.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return proc1;
                            }
                        } else if (procId == 8) {
                            String postingMsg = balancePosting(branchId, dayEndDate);
                            if (postingMsg.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                //String aa = findPartialTransaction(postingMsg);
                                return postingMsg;
                            }
                        } else if (procId == 10) {
                            //posting ODI Adjustments
                            boolean proc10 = postingAdjustment(branchId, dayEndDate);
                            if (proc10) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                msg = reverseOdiMsg;
                                reverseOdiMsg = "";
                                return msg;
                            }

                            //check Loan Laps Date change to the Loan Status
                        } else if (procId == 11) {
                            //Ganarate Loan 'LapsDate' Using dayEndDate
                            boolean proc10 = changeLoanLapsStatus(branchId, dayEndDate);
                            if (proc10) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                msg = "Laps Date Create Process Fail";
                                return msg;
                            }
                        } else if (procId == 12) {
                            // posting Rebate
                            String proc12 = postingRebate(branchId, dayEndDate);
                            if (proc12.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return proc12;
                            }
                        } else if (procId == 13) {
                            // posting Rebate
                            String proc13 = postingRebateSettlement(branchId, dayEndDate);
                            if (proc13.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return proc13;
                            }
                            // Create Debtor's BirthDay Wishes
                        } else if (procId == 14) {
                            String proc14 = createDebtorBirthDayWishes(branchId, dayEndDate);
                            if (proc14.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return proc14;
                            }
                        } else if (procId == 15) {
                            String proc15 = calculateDayEndFloatBalance(branchId, dayEndDate);
                            if (proc15.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return proc15;
                            }
                        } else if (procId == 16) {
                            String proc16 = calculateInsuranceLateChargers(branchId, dayEndDate);
                            if (proc16.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return proc16;
                            }
                        } else if (procId == 17) {
                            String proc17 = calculateInstamentHalfPayments(branchId, dayEndDate);
                            if (proc17.equals("OK")) {
                                updateProcessStatus(procId, dayEndDate, branchId);
                            } else {
                                return proc17;
                            }
                        }
                    }
                }
            } else {
                msg = "OK";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }

    private void updateProcessStatus(int procId, Date ded, int branchId) {
        Session session = null;
        Transaction tx = null;
        int usetId = userDao.findByUserName();
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            DayEndConfig dec = new DayEndConfig();
            dec.setProcessId(procId);
            dec.setProcessStatus(true);
            dec.setDayEndDate(ded);
            dec.setBranchId(branchId);
            dec.setUserId(usetId);
            session.save(dec);

            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                tx.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Cannot rollback transaction" + ex);
            }
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
    }

    //calculated odi amounts for day
    private boolean calculateOdi(int branchId, Date dayEndDate) {
        Session session = null;
        Transaction tx = null;

        boolean retMsg = false;
        Date odueDate = null;
        Date odiDate = null;
        int period1 = 0;
        int diff = 0;

        double rate = 0.00;
        int userId = userDao.findByUserName();
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sql0 = "select period, rate from MOverDue where id=1";
            Object ob[] = (Object[]) session.createQuery(sql0).uniqueResult();
            if (ob != null) {
                period1 = Integer.parseInt(ob[0].toString());
            }
            int period = period1 - 1;
            String overd = "", odiDay = "";
            if (dayEndDate != null) {
                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(dayEndDate);
                //cal2.add(Calendar.DATE, -period);
                overd = sdf.format(cal2.getTime());
                odueDate = sdf.parse(overd);

                Calendar cal3 = Calendar.getInstance();
                cal3.setTime(dayEndDate);
                cal3.add(Calendar.DATE, 1);
                odiDay = sdf.format(cal3.getTime());
                odiDate = sdf.parse(odiDay);

            }

            String sql = "SELECT ins.insId, ins.loanId, loan.debtorHeaderDetails.debtorId,loan.loanPeriodType,ins.dueDate FROM LoanInstallment AS ins,"
                    + " LoanHeaderDetails AS loan WHERE ins.loanId = loan.loanId AND loan.branchId = " + branchId
                    + " AND ins.dueDate <= '" + overd + "' AND ins.isPay < 1";
            List insLoans = session.createQuery(sql).list();
            if (insLoans.size() > 0) {
                for (int i = 0; i < insLoans.size(); i++) {
                    Object[] obj = (Object[]) insLoans.get(i);
                    double creditAmount = 0.00;
                    double debitAmount = 0.00;
                    double paidArreasAmount = 0.00;
                    int instId = Integer.parseInt(obj[0].toString());
                    int loanId = Integer.parseInt(obj[1].toString());
                    int debtorId = Integer.parseInt(obj[2].toString());
                    int periodType = Integer.parseInt(obj[3].toString());
                    String dueDate = obj[4].toString() + " 00:00:00";
                    String sysDate = sdf.format(dayEndDate) + " 00:00:00";
                    Date dueDate1 = sdf2.parse(dueDate);
                    Date sysDate1 = sdf2.parse(sysDate);
                    long l = sysDate1.getTime() - dueDate1.getTime();
                    diff = (int) (l / (24 * 60 * 60 * 1000));
                    String sql_loan = "from MOverDue as o where o.periodType=" + periodType;
                    MOverDue odue = (MOverDue) session.createQuery(sql_loan).uniqueResult();
                    if (odue != null) {
                        rate = odue.getRate();
                    }
                    //paid ins,odi, ovp
                    String sqlc = "SELECT SUM(crdtAmount) FROM SettlementMain AS se WHERE (se.transactionCodeCrdt='INS' OR transactionCodeCrdt='ODI' OR transactionCodeCrdt='OVP') AND Loan_Id=" + loanId;
                    Object credit = session.createQuery(sqlc).uniqueResult();
                    if (credit != null) {
                        creditAmount = Double.valueOf(credit.toString());
                    }
                    //to be paid ins, odi
                    String sqld = "SELECT SUM(dbtAmount) FROM SettlementMain AS se WHERE (se.transactionCodeDbt='INS' OR transactionCodeDbt='ODI' OR transactionCodeDbt='OVP') AND Loan_Id=" + loanId;
                    Object debit = session.createQuery(sqld).uniqueResult();
                    if (debit != null) {
                        debitAmount = Double.valueOf(debit.toString());
                    }
                    //paid arrears
                    String sqla = "SELECT SUM(crdtAmount) FROM SettlementMain AS se WHERE transactionCodeCrdt='ARRS' AND Loan_Id=" + loanId;
                    Object paidArres = session.createQuery(sqla).uniqueResult();
                    if (paidArres != null) {
                        paidArreasAmount = Double.valueOf(paidArres.toString());
                    }

                    //add open arears
                    String sql_arr = "from OpenH where loanId=" + loanId;
                    OpenH open_h = (OpenH) session.createQuery(sql_arr).uniqueResult();
                    double arraesOdi = 0.00;
                    double arraesInsu = 0.00;
                    double totalArears = 0.00;
                    double aresWithoutOdi = 0.00;
                    double otherTotalArears = 0.00;
                    if (open_h != null) {
                        int openId = open_h.getOpenId();
                        if (open_h.getArreasWithoutOdi() != null) {
                            aresWithoutOdi = open_h.getArreasWithoutOdi();
                        }
                        if (open_h.getArreasTotal() != null) {
                            totalArears = open_h.getArreasTotal();
                        }

                        String sql_aard = "from OpenD where openId=" + openId + " and arreasTypeId=2";
                        OpenD arrDetail = (OpenD) session.createQuery(sql_aard).uniqueResult();
                        if (arrDetail != null) {
                            arraesOdi = arrDetail.getAmount();
                        }
                        String sql_aarInsu = "from OpenD where openId=" + openId + " and arreasTypeId=1";
                        OpenD arrDetailInsu = (OpenD) session.createQuery(sql_aarInsu).uniqueResult();
                        if (arrDetailInsu != null) {
                            arraesInsu = arrDetailInsu.getAmount();
                        }

                        String sql_aarSum = "select sum(amount) as tot from OpenD where openId=" + openId;
                        Object arrDetailSum = session.createQuery(sql_aarSum).uniqueResult();
                        if (arrDetailSum != null) {
                            otherTotalArears = Double.parseDouble(arrDetailSum.toString());
                        }
                    }

                    if (paidArreasAmount < otherTotalArears) {
                        if (paidArreasAmount <= arraesInsu) {
                            debitAmount = debitAmount + arraesOdi;
                        } else {
                            paidArreasAmount = paidArreasAmount - arraesInsu;
                            if (paidArreasAmount <= arraesOdi) {
                                debitAmount = debitAmount + arraesOdi - paidArreasAmount;
                            }
                        }
                        paidArreasAmount = 0.00;
                    } else {
                        paidArreasAmount = paidArreasAmount - otherTotalArears;
                    }

                    if (paidArreasAmount > 0) {
                        if (paidArreasAmount < aresWithoutOdi) {
                            debitAmount = debitAmount + aresWithoutOdi - paidArreasAmount;
                        }
                    } else {
                        debitAmount = debitAmount + aresWithoutOdi;
                    }

                    double arears = debitAmount - creditAmount;

                    double overDue = 0.00;
                    if (arears > 0) {
                        if (diff < period) {
                            overDue = 0.00;
                        } else if (diff < 30) {
                            if (diff == period) {
                                overDue = (arears * rate * period1) / 3000;
                            } else {
                                overDue = (arears * rate) / 3000;
                            }
                        } else {
                            overDue = 0.00;
                        }
                    }
                    String crdNo = "INS" + instId;
                    if (overDue > 0) {
                        SettlementMain sett = new SettlementMain();
                        sett.setLoanId(loanId);
                        sett.setDebtorId(debtorId);
                        sett.setTransactionNoDbt(crdNo);
                        sett.setTransactionCodeDbt("ODI");
                        sett.setDbtAmount(overDue);
                        sett.setIsPosting(true);
                        sett.setIsPrinted(false);
                        sett.setDueDate(odiDate);
                        sett.setDescription("ODI Charges");
                        sett.setUserId(userId);
                        sett.setBranchId(branchId);
                        sett.setSystemDate(dayEndDate);
                        session.save(sett);

                        //posting
                        int settId = sett.getSettlementId();
                        String sql4 = "from LoanHeaderDetails where loanId=" + loanId;
                        LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql4).uniqueResult();

                        int loanType = loan.getLoanType();
                        String debtorAcc = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                        String odiAcc = loanDaoImpl.findLoanAccountNo(loanType, "ODI");
                        String postingRefNo = loanDaoImpl.generateReferencNo(loanId, branchId);

                        postingRefNo = postingRefNo + "/" + settId;
                        odiMsg = "";
                        if (!odiAcc.equals("") && !debtorAcc.equals("") && debtorAcc != null) {
                            loanDaoImpl.addToPosting(odiAcc, postingRefNo, new BigDecimal(overDue), branchId, 1);
                            loanDaoImpl.addToPosting(debtorAcc, postingRefNo, new BigDecimal(overDue), branchId, 2);
                            retMsg = true;
                        } else {
                            String loanTypeName = findLoanType(loanType);
                            odiMsg = "Create Account for ODI - " + loanTypeName + "-" + loanId;
                            retMsg = false;
                            return retMsg;
                        }
                    } else {
                        retMsg = true;
                    }
                }
            } else {
                retMsg = true;
            }
            System.out.println("whereee");





            //-----Start Arrears New

            Date dueDate = null;
            String dued = "";

            if (dayEndDate != null) {
                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(dayEndDate);
                cal2.add(Calendar.DATE, 1);
                dued = sdf.format(cal2.getTime());
                dueDate = sdf.parse(dued);
            }

            int i=0;
            String sqlIns = "SELECT ins.insId, ins.loanId, loan.debtorHeaderDetails.debtorId,loan.loanPeriodType,ins.dueDate,ins.isArrears,ins.isPay,ins.insInterest,loan.loanType FROM LoanInstallment AS ins,"
                    + " LoanHeaderDetails AS loan WHERE ins.loanId = loan.loanId AND loan.branchId = " + branchId
                    + " AND ins.dueDate <= '" + dued + "' AND ins.isArrears <> 0 order by ins.loanId,ins.insId ";
             List insList = session.createQuery(sqlIns).list();
            if (insList.size() > 0) {
                Object[] obj = (Object[]) insList.get(0);

                double arrearsAmt = 0.00;
                int loanId = Integer.parseInt(obj[1].toString());
                int isArrears = Integer.parseInt(obj[5].toString());
                ArrayList<Integer> loanInstallments = new ArrayList<Integer>();
                LoanInstallment lnIns =null;
                int isPay = 0;

                for (i = 0; i < insList.size(); i++) {
                    String postingRefNo = loanDaoImpl.generateReferencNo(loanId, branchId);
                    String ArrearsAcc = loanDaoImpl.findLoanAccountNo(Integer.parseInt(obj[8].toString()), "INTSUSP");
                    String earnedAcc = loanDaoImpl.findLoanAccountNo(Integer.parseInt(obj[8].toString()), "INTR");
                    obj = (Object[]) insList.get(i);
                    int instId = Integer.parseInt(obj[0].toString());
                    if (Integer.parseInt(obj[1].toString()) == loanId) {
                        System.out.println("same loan id=" + Integer.parseInt(obj[0].toString()) + " loanId=" + loanId);
                        if (isArrears != 2) {
                            if (Integer.parseInt(obj[6].toString()) < 1 && Integer.parseInt(obj[5].toString()) == 1) {
                                isPay++;
                                arrearsAmt+=Double.parseDouble(obj[7].toString());
                                loanInstallments.add(instId);
                            }else{isArrears=2;}
                            if (isPay == period1) {
                                isArrears = 2;
                                for (int j = 0; j < loanInstallments.size(); j++) {
                                    lnIns= (LoanInstallment) session.createQuery("FROM LoanInstallment WHERE insId ="+loanInstallments.get(j)).uniqueResult();
                                    lnIns.setIsArrears(2);
                                }
                                loanDaoImpl.addToPostingArrears(ArrearsAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 1, dueDate);
                                loanDaoImpl.addToPostingArrears(earnedAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 2, dueDate);
                                arrearsAmt=0.0;
                                session.update(lnIns);
                            }

                            } else if (isArrears == 2) {
                                if (Integer.parseInt(obj[6].toString()) < 1 && Integer.parseInt(obj[5].toString()) == 1) {
                                    isArrears = 2;

                                    lnIns= (LoanInstallment) session.createQuery("FROM LoanInstallment WHERE insId ="+ Integer.parseInt(obj[0].toString())).uniqueResult();
                                    lnIns.setIsArrears(2);
                                    session.update(lnIns);
                                    //update individual hashmap arrears
                                    //posting
                                    arrearsAmt=Double.parseDouble(obj[7].toString());
                                    loanDaoImpl.addToPostingArrears(ArrearsAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 1, dueDate);
                                    loanDaoImpl.addToPostingArrears(earnedAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 2, dueDate);
                                }
                            }
                        } else {
                            loanId = Integer.parseInt(obj[1].toString());
                            isPay = 0;
                            arrearsAmt = 0.0;
                            loanInstallments = new ArrayList<Integer>();
                            isArrears = Integer.parseInt(obj[5].toString());
                            System.out.println("next loan id=" + Integer.parseInt(obj[0].toString()) + " loanId=" + loanId);

                            if (isArrears != 2) {
                                if (Integer.parseInt(obj[6].toString()) < 1 && Integer.parseInt(obj[5].toString()) == 1) {
                                    isPay++;
                                    arrearsAmt+=Double.parseDouble(obj[7].toString());
                                    loanInstallments.add(instId);
                                }
                                if (isPay == period1) {
                                    isArrears = 2;
                                    for (int j = 0; j < loanInstallments.size(); j++) {
                                        lnIns= (LoanInstallment) session.createQuery("FROM LoanInstallment WHERE insId ="+loanInstallments.get(j)).uniqueResult();
                                        lnIns.setIsArrears(2);
                                        //posting
                                    }
                                    loanDaoImpl.addToPostingArrears(ArrearsAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 1, dueDate);
                                    loanDaoImpl.addToPostingArrears(earnedAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 2, dueDate);
                                    session.update(lnIns);
                                }
                            } else if (isArrears == 2) {
                                if (Integer.parseInt(obj[6].toString()) < 1 && Integer.parseInt(obj[5].toString()) == 1) {
                                    isArrears = 2;
                                    lnIns= (LoanInstallment) session.createQuery("FROM LoanInstallment WHERE insId ="+ Integer.parseInt(obj[0].toString())).uniqueResult();
                                    lnIns.setIsArrears(2);
                                    session.update(lnIns);
                                    //update individual hashmap arrears
                                    //posting
                                    arrearsAmt=Double.parseDouble(obj[7].toString());
                                    loanDaoImpl.addToPostingArrears(ArrearsAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 1, dueDate);
                                    loanDaoImpl.addToPostingArrears(earnedAcc, postingRefNo, new BigDecimal(arrearsAmt), branchId, 2, dueDate);
                                }
                            }
                        }
                    }
                }

            //-----End Arrears New







            tx.commit();
        } catch (Exception e) {
            retMsg = false;
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }

        return retMsg;
    }

    private boolean calculateCharges(int branchId) {
        return true;
    }

    private boolean calculateInstallment(int branchId, Date dayEndDate) {
        Session session = null;
        Transaction tx = null;

        boolean retMsg = false;
        Date dueDate = null;
        String dued = "";
        int userId = userDao.findByUserName();
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            if (dayEndDate != null) {
                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(dayEndDate);
                cal2.add(Calendar.DATE, 1);
                dued = sdf.format(cal2.getTime());
                dueDate = sdf.parse(dued);
            }

            String sql = "SELECT ins.insId, ins.loanId, ins.insRoundAmount, loan.debtorHeaderDetails.debtorId, ins.insInterest, ins.insAmount FROM LoanInstallment AS ins,"
                    + " LoanHeaderDetails AS loan WHERE ins.loanId = loan.loanId AND loan.branchId = " + branchId
                    + " AND ins.dueDate = '" + dued + "' AND ins.insStatus=0";

            List insLoans = session.createQuery(sql).list();
            if (insLoans.size() > 0) {
                for (int i = 0; i < insLoans.size(); i++) {
                    Object obj[] = (Object[]) insLoans.get(i);
                    int instId = Integer.parseInt(obj[0].toString());
                    String instNo = "INS" + instId;
                    int loanId = Integer.parseInt(obj[1].toString());
                    double amount = Double.valueOf(obj[2].toString());//round amount
                    int debtorId = Integer.parseInt(obj[3].toString());
                    double interestAmount = Double.parseDouble(obj[4].toString());
                    double realAmount = Double.parseDouble(obj[5].toString());//interest+principle
                    double insCapital = amount;

//                    double insCapital = amount - interestAmount;
                    LoanHeaderDetails headerDetails = (LoanHeaderDetails) session.createCriteria(LoanHeaderDetails.class
                    )
                            .add(Restrictions.eq("loanId", loanId)).uniqueResult();
                    if (headerDetails.getLoanSpec() == 4 || headerDetails.getLoanSpec() == 5) {
                        retMsg = true;
                    } else {
                        SettlementMain sett = new SettlementMain();
                        sett.setLoanId(loanId);
                        sett.setDebtorId(debtorId);
                        sett.setTransactionNoDbt(instNo);
                        sett.setTransactionCodeDbt("INS");
                        sett.setDbtAmount(amount);
                        sett.setIsPosting(true);
                        sett.setIsPrinted(false);
                        sett.setDueDate(dueDate);
                        sett.setDescription("Installment");
                        sett.setUserId(userId);
                        sett.setBranchId(branchId);
                        sett.setSystemDate(dayEndDate);
                        session.save(sett);

                        int setId = sett.getSettlementId();
//                        String sql1 = "from LoanHeaderDetails where loanId=" + loanId;
//                        LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql1).uniqueResult();
                        int loanType = headerDetails.getLoanType();
                        //posting interest to suspend account
                        String refNo = loanDaoImpl.generateReferencNo(loanId, branchId);
                        refNo = refNo + "/" + setId;
                        String int_accNo = loanDaoImpl.findLoanAccountNo(loanType, "INTR");
                        String int_sus_accNo = loanDaoImpl.findLoanAccountNo(loanType, "INTRS");

                        String int_debt_accNo = loanDaoImpl.findLoanAccountNo(loanType, "DEBT");
                        String int_recr_accNo = loanDaoImpl.findLoanAccountNo(loanType, "RECE");
                        String ovp_accNo = loanDaoImpl.findLoanAccountNo(loanType, "OVP");

                        if (!int_accNo.equals("") && !int_sus_accNo.equals("")) {
                            loanDaoImpl.addToInsPosting(int_accNo, refNo, new BigDecimal(interestAmount), branchId, 1);
//                            loanDaoImpl.addToInsPosting(int_debt_accNo, refNo, new BigDecimal(interestAmount), branchId, 2);
                            loanDaoImpl.addToInsPosting(int_sus_accNo, refNo, new BigDecimal(interestAmount), branchId, 2);
                            retMsg = true;
                        }
                        if (!int_debt_accNo.equals("") && !int_recr_accNo.equals("")) {
                            loanDaoImpl.addToInsPosting(int_debt_accNo, refNo, new BigDecimal(insCapital), branchId, 1);
                            loanDaoImpl.addToInsPosting(int_recr_accNo, refNo, new BigDecimal(insCapital), branchId, 2);
                            retMsg = true;
                        } else {
                            installMsg = "Create Account No for Interest Suspend or Interest " + loanId + "-" + findLoanType(loanType);
                            retMsg = false;
                            return retMsg;
                        }

                        //                        xxxxxx
                        double overPayAmount = settlmentDao.getOverPayAmount(loanId);
                        if (overPayAmount > 0) {
                            if (!int_debt_accNo.equals("") && !ovp_accNo.equals("")) {
                                loanDaoImpl.addToInsPosting(int_recr_accNo, refNo, new BigDecimal(overPayAmount), branchId, 1);
                                loanDaoImpl.addToInsPosting(ovp_accNo, refNo, new BigDecimal(overPayAmount), branchId, 2);
                                retMsg = true;
                            }
                        }

                        double diff = amount - realAmount;
                        String exssMonyAccount = loanDaoImpl.findLoanAccountNo(0, EXSS_MONEY_ACCOUNT);
                        if (diff != 0) {
                            if (!headerDetails.getDebtorHeaderDetails().getDebtorAccountNo().equals("") && !exssMonyAccount.equals("")) {
                                loanDaoImpl.addToPosting(headerDetails.getDebtorHeaderDetails().getDebtorAccountNo(), refNo, new BigDecimal(diff), branchId, 1);
                                loanDaoImpl.addToPosting(exssMonyAccount, refNo, new BigDecimal(diff), branchId, 2);//excess money
                                retMsg = true;
                            } else {
                                retMsg = false;
                                return retMsg;
                            }
                        }
                    }

                }
            } else {
                retMsg = true;
            }
            tx.commit();
        } catch (Exception e) {
            retMsg = false;
            try {
                tx.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("cannot rollback transaction-calculate odi" + ex);
            }

        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }

        return retMsg;
    }

    private boolean postingInsurance(int branchId, Date dayEnd) {
        boolean flag = true;
        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "from SettlementMain where systemDate='" + dayEnd + "' and isPosting=0 and transactionCodeDbt='INSU'";
            List insList = session.createQuery(sql).list();
            if (insList.size() > 0) {
                for (int i = 0; i < insList.size(); i++) {
                    SettlementMain sett = (SettlementMain) insList.get(i);
                    int settId = sett.getSettlementId();
                    String insCode = sett.getTransactionNoDbt();
                    String sql2 = "from InsuraceProcessMain where insuCode='" + insCode + "'";
                    InsuraceProcessMain insurance = (InsuraceProcessMain) session.createQuery(sql2).uniqueResult();
                    if (insurance != null) {
                        int debtorId = insurance.getInsuDebtorId();
                        double insCharge = insurance.getInsuPremium();
                        //double comCharge = insurance.getInsuOtherCharge();
                        double totalIns = insurance.getInsuTotalPremium();

                        if (insurance.getIsOtherInsurance()) {
                            //other insurance
                            String sql4 = "from InsuranceOtherCustomers where insuCustomerId=" + debtorId;
                            InsuranceOtherCustomers insOther = (InsuranceOtherCustomers) session.createQuery(sql4).uniqueResult();
                            if (insOther != null) {
                                String othrAcc = insOther.getInsuCustAccountNo();
                                String refCode = insCode + "/" + settId;
                                String sql5 = "from InsuranceOtherCharge where pk=1";
                                InsuranceOtherCharge other = (InsuranceOtherCharge) session.createQuery(sql5).uniqueResult();
                                String accNo_other = other.getAccountNo();
                                if (!accNo_other.equals("") && !othrAcc.equals("")) {
                                    loanDaoImpl.addToPosting(accNo_other, refCode, new BigDecimal(insCharge), branchId, 1);
                                    loanDaoImpl.addToPosting(othrAcc, refCode, new BigDecimal(totalIns), branchId, 2);
                                    sett.setIsPosting(true);
                                    session.update(sett);
                                } else {
                                    System.out.println("Can't Find InsuranceOtherCustomers Account No Or Other Insurance Account No (Debtor Id = " + debtorId);
                                    return false;
                                }
                            }
                        } else {
                            //loan insurance
                            String sql3 = "from LoanHeaderDetails where loanId=" + insurance.getInsuLoanId();
                            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql3).uniqueResult();
                            if (loan != null) {
                                String debtorAcc = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                                String refNo = loanDaoImpl.generateReferencNo(loan.getLoanId(), branchId);
                                String refNo_insu = refNo + "/" + settId;
                                String acc_ins_pay = loanDaoImpl.findLoanAccountNo(loan.getLoanType(), "INS_P");
                                //String acc_ins_com = loanDaoImpl.findLoanAccountNo(loan.getLoanType(), "INS_C");

                                if (!acc_ins_pay.equals("") && !debtorAcc.equals("")) {
                                    if (totalIns > 0.00) {
                                        loanDaoImpl.addToPosting(acc_ins_pay, refNo_insu, new BigDecimal(totalIns), branchId, 1);
                                        loanDaoImpl.addToPosting(debtorAcc, refNo_insu, new BigDecimal(totalIns), branchId, 2);
//                                        if (comCharge > 0.00) {
//                                            loanDaoImpl.addToPosting(acc_ins_com, refNo_insu, new BigDecimal(comCharge), branchId, 1);
//                                        }
                                    }
                                    sett.setIsPosting(true);
                                    session.update(sett);
                                } else {
                                    System.out.println("Can't find Account No this Loan Id--" + insurance.getInsuLoanId());
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flag;
    }

    private boolean postingAdjustment(int branchId, Date dayEnd) {
        boolean retMsg = true;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            //posting odi
            String sql1 = "from SettlementMain where isPosting=0 and branchId=" + branchId + " and systemDate='" + dayEnd + "' and transactionCodeDbt='ODI'";
            List odiReverseList = session.createQuery(sql1).list();
            if (odiReverseList.size() > 0) {
                for (int i = 0; i < odiReverseList.size(); i++) {
                    SettlementMain sett = (SettlementMain) odiReverseList.get(i);
                    double odiAmount = sett.getDbtAmount();
                    double overDue = 0.00;
                    int c = 0, d = 0;
                    if (odiAmount < 0) {
                        c = 1;
                        d = 2;
                    } else {
                        c = 2;
                        d = 1;
                    }
                    overDue = Math.abs(odiAmount);
                    int settId = sett.getSettlementId();
                    int loanId = sett.getLoanId();
                    String sql4 = "from LoanHeaderDetails where loanId=" + loanId;
                    LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql4).uniqueResult();
                    int loanType = loan.getLoanType();
                    String debtorAcc = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                    String odiAcc = loanDaoImpl.findLoanAccountNo(loanType, "ODI");
                    String postingRefNo = loanDaoImpl.generateReferencNo(loanId, branchId);
                    postingRefNo = postingRefNo + "/" + settId;
                    reverseOdiMsg = "";

                    if (!odiAcc.equals("") && !debtorAcc.equals("")) {
                        if (overDue > 0.00) {
                            loanDaoImpl.addToPosting(odiAcc, postingRefNo, new BigDecimal(overDue), branchId, d);
                            loanDaoImpl.addToPosting(debtorAcc, postingRefNo, new BigDecimal(overDue), branchId, c);
                        }
                        retMsg = true;
                    } else {
                        String loanTypeName = findLoanType(loanType);
                        reverseOdiMsg = "Create Account for ODI - " + loanTypeName;
                        retMsg = false;
                        return retMsg;
                    }

                }
            }
            //posting adjustments other
//            String sqlj = "from SettlementMain where isPosting=0 and branchId=" + branchId + " and systemDate='" + dayEnd + "' and transactionCodeDbt='ADJ'";
//            List adjReversList = session.createQuery(sqlj).list();
//            if (adjReversList.size() > 0) {
//                for (int j = 0; j < adjReversList.size(); j++) {
//                    SettlementMain settj = (SettlementMain) adjReversList.get(j);
//                    
//                }
//            }

        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            reverseOdiMsg = "Odi Reverse Process Not Success";
            retMsg = false;
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return retMsg;
    }

    //POSTING CHEQUE RETURN
    private boolean postingReturnCheq(int branchId, Date dayEnd) {
        boolean msg = true;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sql = "FROM SettlementMain WHERE isPosting=0 AND systemDate='" + dayEnd + "' AND branchId=" + branchId + "  AND Transaction_Code_Dbt NOT IN('" + REBATE_REF_CODE + "') and  dbtAmount IS NOT NULL";
            List returncheq = session.createQuery(sql).list();
            if (returncheq.size() > 0) {
                for (int i = 0; i < returncheq.size(); i++) {
                    SettlementMain sellt = (SettlementMain) returncheq.get(i);
                    String transNo = sellt.getTransactionNoDbt();
                    int debtorId = sellt.getDebtorId();
                    int loanId = sellt.getLoanId();
                    int setId = sellt.getSettlementId();
                    double chargeAmount = sellt.getDbtAmount();
                    String trDCode = sellt.getTransactionCodeDbt();

                    String refNo = loanDaoImpl.generateReferencNo(loanId, branchId);
                    refNo = refNo + "/" + setId;

                    String sql3 = "select debtorAccountNo from DebtorHeaderDetails where debtorId=" + debtorId;
                    Object ob = session.createQuery(sql3).uniqueResult();
                    String debAccountNo = "";
                    if (ob != null) {
                        debAccountNo = ob.toString();
                    }
                    String cashInHandAccNo = loanDaoImpl.findLoanAccountNo(0, CASH_IN_HAND_ACC_CODE);

                    String sql2 = "from SettlementPaymentDetailCheque where transactionNo='" + transNo + "'";
                    List cheqsList = session.createQuery(sql2).list();
                    if (cheqsList.size() > 0) {
                        SettlementPaymentDetailCheque cheq = (SettlementPaymentDetailCheque) cheqsList.get(0);
                        if (cheq != null) {
                            int cheqStatus = cheq.getIsReturn();
//                            double cheqAmount = cheq.getAmount();
                            if (cheqStatus == 1) {
                                double amount = cheq.getAmount();
//                                if (!trDCode.equals("OTHR")) {
                                if (!debAccountNo.equals("") && !cashInHandAccNo.equals("")) {
                                    if (chargeAmount > 0.00) {
                                        loanDaoImpl.addToPosting(debAccountNo, refNo, new BigDecimal(chargeAmount), branchId, 2);
                                        loanDaoImpl.addToPosting(cashInHandAccNo, refNo, new BigDecimal(chargeAmount), branchId, 1);//excess money
                                    }
                                } else {
                                    msg = false;
                                    if (debAccountNo.equals("")) {
                                        cheqMsg = "Create Account No for Debtor" + debtorId;
                                    } else {
                                        cheqMsg = "Can't Find Cash In Hand Account (CASH_IN_HAND_ACC_CODE)";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            msg = false;
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return msg;
    }

    private String postingSettlement(int branchId, Date dayEnd) {
        String msg = "";
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sql = "FROM SettlementMain WHERE isPosting=0 AND systemDate = '" + dayEnd + "' AND branchId=" + branchId + " AND Transaction_Code_Crdt NOT IN('" + REBATE_REF_CODE + "') and crdtAmount IS NOT NULL GROUP BY receiptNo";
            List transactionList = session.createQuery(sql).list();

            if (transactionList.size() > 0) {
                for (int i = 0; i < transactionList.size(); i++) {
                    SettlementMain sett = (SettlementMain) transactionList.get(i);
                    int settId = sett.getSettlementId();
                    String refNo = sett.getReceiptNo();
                    String transNo = sett.getTransactionNoCrdt();
                    if (refNo != null && !refNo.equals("")) {

                        double bulkAmount = 0.00;
//                    String sql8 = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE receiptNo='" + refNo + "' GROUP BY receiptNo";
//                    Object c_sum = session.createQuery(sql8).uniqueResult();
//                    if (c_sum != null) {
//                        bulkAmount = Double.valueOf(c_sum.toString());
//                    }

                        String sql_ch = "SELECT SUM(charge) FROM SettlementCharges WHERE billNo='" + refNo + "' and reffNo = '" + transNo + "'";
                        Object ch_sum = session.createQuery(sql_ch).uniqueResult();
                        if (ch_sum != null) {
                            bulkAmount = Double.valueOf(ch_sum.toString());
                        }

                        String sql1 = "from SettlementCharges where billNo = '" + refNo + "' and reffNo = '" + transNo + "'";
                        List detailList = session.createQuery(sql1).list();

                        double cashSum = 0.00, cheqSum = 0.00, bankSum = 0.00;
                        String sql3 = "SELECT SUM(amount) FROM SettlementPaymentDetailCash WHERE transactionNo='" + transNo + "'";
                        Object cash = session.createQuery(sql3).uniqueResult();
                        if (cash != null) {
                            cashSum = Double.valueOf(cash.toString());
                        }

                        String sql4 = "SELECT SUM(amount) FROM SettlementPaymentDetailCheque WHERE transactionNo='" + transNo + "'";
                        Object cheq = session.createQuery(sql4).uniqueResult();
                        if (cheq != null) {
                            cheqSum = Double.valueOf(cheq.toString());
                        }

                        String sql5 = "SELECT SUM(amount) FROM SettlementPaymentDetailBankDeposite WHERE transactionNo='" + transNo + "'";
                        Object bank = session.createQuery(sql5).uniqueResult();
                        if (bank != null) {
                            bankSum = Double.valueOf(bank.toString());
                        }

                        double totalPayment = cashSum + cheqSum + bankSum;
                        int loanId = sett.getLoanId();
                        String sql2 = "from LoanHeaderDetails where loanId=" + loanId;
                        LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();

                        String sql_ovp = "FROM SettlementMain WHERE isPosting=0 AND systemDate = '" + dayEnd + "' AND transactionCodeCrdt='OVP' AND receiptNo='" + refNo + "' and transactionNoCrdt = '" + transNo + "'";
                        SettlementMain transactionList_ovp = (SettlementMain) session.createQuery(sql_ovp).uniqueResult();
                        double overPayment = 0.00;
                        int ovp_settId = 0;
                        if (transactionList_ovp != null) {
                            overPayment = transactionList_ovp.getCrdtAmount();
                            ovp_settId = transactionList_ovp.getSettlementId();
                        }
                        double debtAmount = 0.00;
                        int debtId = 0;
                        if (bulkAmount > 0) {
                            if (totalPayment >= bulkAmount) {
                                System.out.println("Valid Payments...");
                            } else {
                                String sql_de_over = "SELECT SUM(dbtAmount) as dbt, settlementId FROM SettlementMain WHERE receiptNo='" + refNo + "' AND transactionCodeDbt='OVP'";
                                Object[] debtSum = (Object[]) session.createQuery(sql_de_over).uniqueResult();
                                if (debtSum.length > 0) {
                                    if (debtSum[0] != null) {
                                        debtAmount = Double.valueOf(debtSum[0].toString());
                                        debtId = Integer.parseInt(debtSum[1].toString());
                                        String debAccNo_ = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                                        String postingRefNo_ = loanDaoImpl.generateReferencNo(loanId, branchId);
                                        postingRefNo_ = postingRefNo_ + "/" + debtId;

                                        String cashInHandAccNo = loanDaoImpl.findLoanAccountNo(0, CASH_IN_HAND_ACC_CODE);
                                        if (!debAccNo_.equals("") && cashInHandAccNo.equals("")) {
                                            if (debtAmount > 0.00) {
                                                loanDaoImpl.addToPosting(debAccNo_, postingRefNo_, new BigDecimal(debtAmount), branchId, 2);
                                                loanDaoImpl.addToPosting(cashInHandAccNo, postingRefNo_, new BigDecimal(debtAmount), branchId, 1);
                                            }
                                        } else if (debAccNo_.equals("")) {
                                            msg = "Create Account No for--" + debtId;
                                        } else {
                                            msg = "Can't Find Cash In Hand Account (CASH_IN_HAND_ACC_CODE)";
                                        }
                                    }
                                    totalPayment = totalPayment + debtAmount;
                                }
                                if (totalPayment >= bulkAmount) {
                                    System.out.println("valid over payment");
                                } else {
                                    System.out.println("Invalid Payments...");
                                    msg = "Posting Error: " + "\n" + "Payments are not settle for the Transaction No:" + transNo;
                                    return msg;
                                }
                            }
                            overPayment = totalPayment - bulkAmount;
                        } else if (bulkAmount == 0) {
                            overPayment = totalPayment;
                        }
                        double insAndOvp = totalPayment + overPayment;

                        String postingRefNo = loanDaoImpl.generateReferencNo(loanId, branchId);
                        postingRefNo = postingRefNo + "/" + settId;
                        if (detailList.size() > 0) {
                            SettlementCharges detail = null;
                            double amount = 0.00;
                            for (int j = 0; j < detailList.size(); j++) {
                                detail = (SettlementCharges) detailList.get(j);
                                amount += detail.getCharge();
                            }
                            String code = detail.getChargeType();
                            int loanTypeId = loan.getLoanType();
                            String debtorAccount = detail.getDebtorAccNo();

                            String insCollection = loanDaoImpl.findLoanAccountNo(loanTypeId, INSTALLMENT_COLLECTION_ACC_CODE);
                            String cashBook = loanDaoImpl.findLoanAccountNo(loanTypeId, CASH_BOOK_ACC_CODE);
                            System.out.println("cashInHandAccNo ##########---->> " + insCollection);
                            System.out.println("chequeInHand ##########---->> " + cashBook);
                            bulkAmount = bulkAmount - amount;

                            if (!debtorAccount.equals("") && !insCollection.equals("") && !cashBook.equals("")) {
//                                    loanDaoImpl.addToPosting(debtorAccount, postingRefNo, new BigDecimal(amount), branchId, 1);
                                loanDaoImpl.addToPosting(insCollection, postingRefNo, new BigDecimal(amount), branchId, 1);
                                if (cashSum >= amount) {
                                    //posting to cash account
                                    cashSum = cashSum - amount;
                                    loanDaoImpl.addToPosting(cashBook, postingRefNo, new BigDecimal(totalPayment), branchId, 2);
                                    amount = 0.00;
//                                break;
                                } else {
                                    if (cashSum != 0) {
                                        loanDaoImpl.addToPosting(insCollection, postingRefNo, new BigDecimal(cashSum), branchId, 2);
                                        amount = amount - cashSum;
                                        cashSum = 0.00;
                                    }
                                    if (amount > 0) {
                                        if (cheqSum >= amount) {
                                            //posting cheq account
                                            cheqSum = cheqSum - amount;
                                            loanDaoImpl.addToPosting(cashBook, postingRefNo, new BigDecimal(amount), branchId, 2);
                                            amount = 0.00;
//                                        break;
                                        } else {
                                            if (cheqSum != 0) {
                                                loanDaoImpl.addToPosting(cashBook, postingRefNo, new BigDecimal(cheqSum), branchId, 2);
                                                amount = amount - cheqSum;
                                                cheqSum = 0.00;
                                            }
                                            if (amount > 0) {
                                                if (bankSum >= amount) {
                                                    loanDaoImpl.addToPosting(insCollection, postingRefNo, new BigDecimal(amount), branchId, 2);
                                                    bankSum = bankSum - amount;
                                                    amount = 0.00;
//                                                break;
                                                } else if (bankSum != 0) {
                                                    loanDaoImpl.addToPosting(insCollection, postingRefNo, new BigDecimal(bankSum), branchId, 2);
                                                    amount = amount - bankSum;
                                                    bankSum = 0.00;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                msg = "Can't Find debtorAccount" + detail.getDebtorId() + " Or cashInHandAccNo or chequeInHand Account No";
                            }

                            if (amount == 0) {
                                System.out.println("No Balance. Completed");
                            } else {
                                System.out.println("Error: Balance is:" + amount);
                                msg = "Posting Error:" + "\n" + "Remainig balance for the Transaction No:" + transNo + " and Code:" + code;
//                                return msg;
                            }
//                            }
                        }
                        if (transactionList_ovp != null) {
                            int loanTypeId = loan.getLoanType();
                            String cashInHandAccNo = loanDaoImpl.findLoanAccountNo(loanTypeId, CASH_IN_HAND_ACC_CODE);
                            String debAccNo = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                            String chequeInHand = loanDaoImpl.findLoanAccountNo(loanTypeId, CHEQUE_IN_HAND_ACC_CODE);
                            String overPayAcc = loanDaoImpl.findLoanAccountNo(loanTypeId, OVP_ACCOUNT);
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxx
//                            if (!cashInHandAccNo.equals("") && !debAccNo.equals("") && !overPayAcc.equals("")) {
                            if (!overPayAcc.equals("")) {
                                loanDaoImpl.addToPosting(overPayAcc, postingRefNo, new BigDecimal(overPayment), branchId, 1);
                                if (cashSum >= overPayment) {
                                    //posting to cash account
                                    cashSum = cashSum - overPayment;
//                                    loanDaoImpl.addToPosting(overPayAcc, postingRefNo, new BigDecimal(overPayment), branchId, 1);
                                    overPayment = 0.00;
//                            break;
                                } else {
                                    if (cashSum != 0) {
//                                        loanDaoImpl.addToPosting(cashInHandAccNo, postingRefNo, new BigDecimal(cashSum), branchId, 2);
                                        overPayment = overPayment - cashSum;
                                        cashSum = 0.00;
                                    }
                                    if (overPayment > 0) {
                                        if (cheqSum >= overPayment) {
                                            //posting cheq account
                                            cheqSum = cheqSum - overPayment;
//                                            loanDaoImpl.addToPosting(chequeInHand, postingRefNo, new BigDecimal(overPayment), branchId, 2);
                                            overPayment = 0.00;
//                                    break;
                                        } else {
                                            if (cheqSum != 0) {
//                                                loanDaoImpl.addToPosting(chequeInHand, postingRefNo, new BigDecimal(cheqSum), branchId, 2);
                                                overPayment = overPayment - cheqSum;
                                                cheqSum = 0.00;
                                            }
                                            if (overPayment > 0) {
                                                if (bankSum >= overPayment) {
//                                                    loanDaoImpl.addToPosting(cashInHandAccNo, postingRefNo, new BigDecimal(overPayment), branchId, 2);
                                                    bankSum = bankSum - overPayment;
                                                    overPayment = 0.00;
//                                            break;
                                                } else if (bankSum != 0) {
//                                                    loanDaoImpl.addToPosting(cashInHandAccNo, postingRefNo, new BigDecimal(bankSum), branchId, 2);
                                                    overPayment = overPayment - bankSum;
                                                    bankSum = 0.00;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                msg = "Can't Find debtorAccount" + loan.getDebtorHeaderDetails().getDebtorId() + " Or cashInHandAccNo or chequeInHand Account No";
                            }

                        } else {
                            bulkAmount = 0.00;
                        }

                        if (bulkAmount == 0) {
                            System.out.println("Completed Tranaction No:" + transNo);
                        } else {
                            System.out.println("Error: Tranaction " + transNo + " not Completed. Balance=" + bulkAmount);
//                        msg = "Posting Error:" + "\n" + " Transaction No:" + transNo + " is not completed...!";
//                        return msg;
                        }

                        String sqlLi = "FROM SettlementMain WHERE receiptNo='" + refNo + "'";
                        List trans_list = session.createQuery(sqlLi).list();
                        for (int t = 0; t < trans_list.size(); t++) {
                            SettlementMain tran = (SettlementMain) trans_list.get(t);
                            tran.setIsPosting(true);
                            session.update(tran);
                        }
                        System.out.println("settlement update....");
                    }
                }

            }

            tx.commit();
        } catch (Exception ep) {
            ep.printStackTrace();

            try {
                tx.rollback();
            } catch (Exception ex) {
                System.out.println("cannot rollback transaction-calculate odi" + ex);
            }
            return "Error Posting";
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }

        return "OK";
    }

    private String balancePosting(int branchId, Date dayEnd) {
        String posting_msg = "";
        try {
            String sql = "SELECT SUM(CAmount)-SUM(DAmount), referenceNo FROM Posting WHERE date='" + dayEnd + "' AND branchId=" + branchId + " GROUP BY Reference_No";
            List cr_db_balanceList = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (cr_db_balanceList.size() > 0) {
                for (int i = 0; i < cr_db_balanceList.size(); i++) {
                    Object[] obj = (Object[]) cr_db_balanceList.get(i);
                    if (obj != null) {
                        double balance = Double.valueOf(obj[0].toString());
                        if (balance != 0) {
                            //posting error
                            String ref_No = obj[1].toString();
                            return ref_No;
                        } else {
                            //posting balance success
                            posting_msg = "OK";
                        }
                    }
                }
            } else {
                posting_msg = "OK";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return posting_msg;
    }

    private boolean UpdateDayEnd(int branchId, Date endDate) {
        boolean ret = false;
        Date next_day = null;

        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql2 = "SELECT dp.processId FROM DayEndProcess AS dp WHERE dp.processActive=1 and dp.processId NOT IN"
                    + " (SELECT dc.processId FROM DayEndConfig AS dc WHERE dc.dayEndDate = '" + endDate + "' AND dc.branchId = " + branchId + " AND dc.processStatus=1)";
            List processList = session.createQuery(sql2).list();
            if (processList.size() == 1) {
                if (endDate != null) {
                    Calendar cal2 = Calendar.getInstance();
                    cal2.setTime(endDate);
                    cal2.add(Calendar.DATE, 1);
                    String dd = sdf.format(cal2.getTime());
                    next_day = sdf.parse(dd);
                }
                if (next_day != null) {
                    DayEndDate day = new DayEndDate();
                    day.setDayEndDate(next_day);
                    day.setBranchId(branchId);
                    day.setActionTime(Calendar.getInstance().getTime());
                    session.save(day);
                    ret = true;
                }
            } else {
                ret = false;
            }
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return ret;
    }

    private String findPartialTransaction(String refNo) {
        String[] refString = refNo.split("/");
        String date = refString[0];
        String loanId = refString[1];
        String recptNo = refString[2];
        String errorMsg = "";
        try {
            String sql0 = "from LoanHeaderDetails where Loan_Id=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
            String aggrementNo = "", debtorName = "";
            if (loan != null) {
                aggrementNo = loan.getLoanAgreementNo();
                debtorName = loan.getDebtorHeaderDetails().getNameWithInitial();
            }
            String sql1 = "from SettlementReceiptHeader where";
            errorMsg = "Transaction failed for Loan No:" + aggrementNo + " , to " + debtorName + " ";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return errorMsg;
    }

    @Override
    public List findConfigJobs() {
        List jobList = null;
        try {
            String sql = "from DayEndProcess where processActive=1";
            jobList = sessionFactory.getCurrentSession().createQuery(sql).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jobList;
    }

    @Override
    public String getLastDayEnd(int branchId) {
        String lastDay = "";
        try {
            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (ob != null) {
                lastDay = ob.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastDay;
    }

    public int findBranchByUser(int userId) {
        int branchId = 0;
        String sql = "FROM UmUserLog WHERE userId=" + userId + " ORDER BY pk DESC";
        UmUserLog userLog = (UmUserLog) sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();
        if (userLog != null) {
            branchId = userLog.getBranchId();
        }
        return branchId;
    }

    public void changeBlockStatus(Date endDate, boolean isBlock) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        int usetId = userDao.findByUserName();
        try {
            int branchId = findBranchByUser(usetId);
            String sql = "FROM UmUserBranch WHERE branchId = " + branchId;
            List<UmUserBranch> users = session.createQuery(sql).list();
            if (users.size() > 0) {
                for (int i = 0; i < users.size(); i++) {
                    UmUserBranch ul = (UmUserBranch) users.get(i);
                    if (ul.getUserId() != usetId) {
                        String sql5 = "FROM UmUserBranch WHERE userId=" + ul.getUserId() + " AND branchId!=" + branchId;
                        List userBranch = session.createQuery(sql5).list();
                        if (userBranch.size() > 0) {
                            ul.setIsBlock(isBlock);
                            session.update(ul);
                        } else {
                            String sql2 = "FROM UmUser WHERE userId=" + ul.getUserId();
                            UmUser um = (UmUser) session.createQuery(sql2).uniqueResult();
                            if (um != null) {
                                um.setIsBlock(isBlock);
                                session.update(um);
                            }
                        }
                        String sql3 = "FROM UmUserLog WHERE User_Id=" + usetId + " AND Branch_Id =" + branchId + " AND Log_Date = '" + endDate + "'";
                        List<UmUserLog> userLog = session.createQuery(sql3).list();
                        if (userLog.size() > 0) {
                            for (int j = 0; j < userLog.size(); j++) {
                                UmUserLog log = (UmUserLog) userLog.get(j);
                                log.setUserBlock(isBlock);
                                session.update(log);
                                log = null;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
    }

    public boolean backupDataWithDatabase(int a, int branch) throws Exception {
        String path = "";
        String state = "";

        if (a == 0) {
            state = "Before";
        } else {
            state = "After";
        }

        String dumpExePath = "";
        String host = "localhost";
        String port = "3306";
        String user = "itesgfinance";
        String password = "itesg@finance";
        String database = "axa_bank_finance";
        String backupPath = "";
        boolean status = false;

        String sql = "from Serverdetails where id=1";
        Serverdetails server = (Serverdetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        if (server != null) {
            dumpExePath = server.getDumppath();
            backupPath = server.getRestorepath() + branch + "\\";
        }

        try {
            Process p = null;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            Date date = new Date();

            String dir = backupPath;
            File directory = new File(dir);
            boolean success = false;
            if (directory.exists()) {
                System.out.println("Directory already exists ...");
            } else {
                System.out.println("Directory not exists, creating now");
                directory.mkdir();
            }

            String filepath = state + "_BANK" + "-(" + dateFormat.format(date) + ").sql";
            path = filepath;
            String batchCommand = "";
            if (password != "") {
                //Backup with database
                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            } else {
                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            }

            Runtime runtime = Runtime.getRuntime();
            p = runtime.exec(batchCommand);
            int processComplete = p.waitFor();

            if (processComplete == 0) {
                status = true;
                System.out.println("Backup ok........");
            } else {
                status = false;
                System.out.println("Backup fails........");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return status;
    }

    public String findLoanType(int subloanType) {
        String name = "";
        String sql = "from MSubLoanType where subLoanId=" + subloanType;
        MSubLoanType subLoan = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        if (subLoan != null) {
            name = subLoan.getSubLoanName();
        }
        return name;
    }

    public boolean postingOtherPayment(int branchId, Date dayenddate) {
        try {
            Session session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            String sql = "from SettlementPaymentOther WHERE branchId=" + branchId + " AND SystemDate='" + dayenddate + "' AND IsPosting=0";
            System.out.println("after query");
            List<SettlementPaymentOther> sp = session.createQuery(sql).list();
            String cashInHandAccNo = loanDaoImpl.findLoanAccountNo(0, CASH_IN_HAND_ACC_CODE);
            for (SettlementPaymentOther spo : sp) {
                if (!spo.getAccountNo().equals("") && !cashInHandAccNo.equals("")) {
                    if (spo.getPaymentAmount() > 0.00) {
                        loanDaoImpl.addToPosting(cashInHandAccNo, spo.getTransactionNo(), BigDecimal.valueOf(spo.getPaymentAmount()), spo.getBranchId(), 2);
                        loanDaoImpl.addToPosting(spo.getAccountNo(), spo.getTransactionNo(), BigDecimal.valueOf(spo.getPaymentAmount()), spo.getBranchId(), 1);
                    }
                } else {
                    return false;
                }
                spo.setIsPosting(1);
                session.update(spo);
            }
            tx.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //testing
    public boolean deletePosting(int branch, Date dayEndDate) {
        boolean flag = true;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            System.out.println("dayEndDate" + dayEndDate);
            String sql = "From SettlementMain where systemDate='" + dayEndDate + "' and branchId=" + branch + " and isPosting=0";
            List paymentList = session.createQuery(sql).list();
            if (paymentList.size() > 0) {
                for (int i = 0; i < paymentList.size(); i++) {
                    SettlementMain payment = (SettlementMain) paymentList.get(i);
                    int s_id = payment.getSettlementId();
                    String sql2 = "from Posting where referenceNo like '%/" + s_id + "'";
                    List postingList = session.createQuery(sql2).list();
                    if (postingList.size() > 0) {
                        for (int j = 0; j < postingList.size(); j++) {
                            Posting pos = (Posting) postingList.get(j);
                            session.delete(pos);
                        }
                    }
                }
            }
            tx.commit();
        } catch (Exception e) {
            flag = false;
            tx.rollback();
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return flag;
    }

    @Override
    public String correctPosting(String sDate, String eDate) {
        String msg = "OK";
        try {
            int uId = userDao.findByUserName();
            int branchId = findBranchByUser(uId);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = sdf.parse(sDate);
            Date endDate = sdf.parse(eDate);

            Calendar sd = Calendar.getInstance();
            sd.setTime(startDate);

            Calendar ed = Calendar.getInstance();
            ed.setTime(endDate);

            java.sql.Date date_a = null;

            for (Date i = sd.getTime(); !sd.after(ed); sd.add(Calendar.DATE, 1), i = sd.getTime()) {
                date_a = new java.sql.Date(sdf.parse(sdf.format(i)).getTime());
                System.out.println("aa>" + date_a);
                boolean abc = deletePosting(branchId, date_a);
                String pp = "";
                if (abc) {
                    pp = postingSettlement2(branchId, date_a);
                }
                if (!pp.equals("OK")) {
                    msg = pp;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            msg = "Error Complete";
        }
        return msg;
    }

    public void addToPosting2(Date dayEndDate, String accountNo, String referenceNo, BigDecimal amount, int branchId, int cd) {
        int user_id = 3;
//        int user_id = userDao.findByUserName();
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        try {
            if (!accountNo.equals("")) {
                Posting posting = new Posting();
                posting.setReferenceNo(referenceNo);
                posting.setAccountNo(accountNo);
                posting.setCreditDebit(cd);
                if (cd == 1) {
                    posting.setCAmount(amount);
                    posting.setDAmount(new BigDecimal(0.00));
                } else {
                    posting.setDAmount(amount);
                    posting.setCAmount(new BigDecimal(0.00));
                }
                posting.setAmount(amount);
                posting.setUserId(user_id);
                posting.setCurrDateTime(action_time);
                posting.setBranchId(branchId);
                posting.setAllocation(0);
                posting.setPostingFrom(1);
                posting.setPostingState(1);
                posting.setDate(dayEndDate);
                posting.setDescription("posting from Vijitha Finance");
                sessionFactory.getCurrentSession().save(posting);
                System.out.println("Posted");
            } else {
                System.out.println("Cannot Posting");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //test settlement
    private String postingSettlement2(int branchId, Date dayEnd) {
        String msg = "";
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sql = "FROM SettlementMain WHERE isPosting=0 AND systemDate = '" + dayEnd + "' AND branchId=" + branchId + " AND crdtAmount IS NOT NULL GROUP BY receiptNo";
            List transactionList = session.createQuery(sql).list();

            if (transactionList.size() > 0) {
                for (int i = 0; i < transactionList.size(); i++) {
                    SettlementMain sett = (SettlementMain) transactionList.get(i);
                    int settId = sett.getSettlementId();
                    String refNo = sett.getReceiptNo();
                    String transNo = sett.getTransactionNoCrdt();

                    double bulkAmount = 0.00;
//                    String sql8 = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE receiptNo='" + refNo + "' GROUP BY receiptNo";
//                    Object c_sum = session.createQuery(sql8).uniqueResult();
//                    if (c_sum != null) {
//                        bulkAmount = Double.valueOf(c_sum.toString());
//                    }

                    String sql_ch = "SELECT SUM(charge) FROM SettlementCharges WHERE billNo='" + refNo + "'";
                    Object ch_sum = session.createQuery(sql_ch).uniqueResult();
                    if (ch_sum != null) {
                        bulkAmount = Double.valueOf(ch_sum.toString());
                    }

                    String sql1 = "from SettlementCharges where billNo = '" + refNo + "'";
                    List detailList = session.createQuery(sql1).list();

                    double cashSum = 0.00, cheqSum = 0.00, bankSum = 0.00;
                    String sql3 = "SELECT SUM(amount) FROM SettlementPaymentDetailCash WHERE transactionNo='" + transNo + "'";
                    Object cash = session.createQuery(sql3).uniqueResult();
                    if (cash != null) {
                        cashSum = Double.valueOf(cash.toString());
                    }

                    String sql4 = "SELECT SUM(amount) FROM SettlementPaymentDetailCheque WHERE transactionNo='" + transNo + "'";
                    Object cheq = session.createQuery(sql4).uniqueResult();
                    if (cheq != null) {
                        cheqSum = Double.valueOf(cheq.toString());
                    }

                    String sql5 = "SELECT SUM(amount) FROM SettlementPaymentDetailBankDeposite WHERE transactionNo='" + transNo + "'";
                    Object bank = session.createQuery(sql5).uniqueResult();
                    if (bank != null) {
                        bankSum = Double.valueOf(bank.toString());
                    }

                    double totalPayment = cashSum + cheqSum + bankSum;

                    int loanId = sett.getLoanId();

                    String sql2 = "from LoanHeaderDetails where loanId=" + loanId;
                    LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();

                    String sql_ovp = "FROM SettlementMain WHERE isPosting=0 AND systemDate = '" + dayEnd + "' AND transactionCodeCrdt='OVP' AND receiptNo='" + refNo + "'";
                    SettlementMain transactionList_ovp = (SettlementMain) session.createQuery(sql_ovp).uniqueResult();
                    double overPayment = 0.00;
                    int ovp_settId = 0;
                    if (transactionList_ovp != null) {
                        overPayment = transactionList_ovp.getCrdtAmount();
                        ovp_settId = transactionList_ovp.getSettlementId();
                    }
                    double debtAmount = 0.00;
                    int debtId = 0;
                    if (bulkAmount > 0) {
                        if (totalPayment >= bulkAmount) {
                            System.out.println("Valid Payments...");
                        } else {
                            String sql_de_over = "SELECT SUM(dbtAmount) as dbt, settlementId FROM SettlementMain WHERE receiptNo='" + refNo + "' AND transactionCodeDbt='OVP'";
                            Object[] debtSum = (Object[]) session.createQuery(sql_de_over).uniqueResult();
                            if (debtSum.length > 0) {
                                if (debtSum[0] != null) {
                                    debtAmount = Double.valueOf(debtSum[0].toString());
                                    debtId = Integer.parseInt(debtSum[1].toString());
                                    String debAccNo_ = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                                    String postingRefNo_ = loanDaoImpl.generateReferencNo(loanId, branchId);
                                    postingRefNo_ = postingRefNo_ + "/" + debtId;
                                    String cashInHandAccNo = loanDaoImpl.findLoanAccountNo(0, CASH_IN_HAND_ACC_CODE);
                                    if (!debAccNo_.equals("") && !cashInHandAccNo.equals("")) {
                                        if (debtAmount > 0.00) {
                                            addToPosting2(dayEnd, debAccNo_, postingRefNo_, new BigDecimal(debtAmount), branchId, 2);
                                            addToPosting2(dayEnd, cashInHandAccNo, postingRefNo_, new BigDecimal(debtAmount), branchId, 1);
                                        }
                                    } else {
                                        msg = "Can't Find Acoount No " + loan.getDebtorHeaderDetails().getDebtorId() + "or cashInHandAccNo";
                                    }
                                }
                                totalPayment = totalPayment + debtAmount;
                            }
                            if (totalPayment >= bulkAmount) {
                                System.out.println("valid over payment");
                            } else {
                                System.out.println("Invalid Payments...");
                                msg = "Posting Error: " + "\n" + "Payments are not settle for the Transaction No:" + transNo;
                                return msg;
                            }
                        }
                        overPayment = totalPayment - bulkAmount;
                    } else if (bulkAmount == 0) {
                        overPayment = totalPayment;
                    }

                    String postingRefNo = loanDaoImpl.generateReferencNo(loanId, branchId);
                    postingRefNo = postingRefNo + "/" + settId;
                    String cashInHandAccNo = loanDaoImpl.findLoanAccountNo(0, CASH_IN_HAND_ACC_CODE);
                    if (detailList.size() > 0) {
                        for (int j = 0; j < detailList.size(); j++) {
                            SettlementCharges detail = (SettlementCharges) detailList.get(j);
                            String code = detail.getChargeType();

                            String debtorAccount = detail.getDebtorAccNo();
                            double amount = detail.getCharge();

                            bulkAmount = bulkAmount - amount;
                            if (!debtorAccount.equals("")) {
                                addToPosting2(dayEnd, debtorAccount, postingRefNo, new BigDecimal(amount), branchId, 1);
                            }
                            if (cashSum >= amount) {
                                //posting to cash account
                                cashSum = cashSum - amount;
                                addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(amount), branchId, 2);
                                amount = 0.00;
//                                break;
                            } else {
                                if (cashSum != 0) {
                                    addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(cashSum), branchId, 2);
                                    amount = amount - cashSum;
                                    cashSum = 0.00;
                                }
                                if (amount > 0) {
                                    if (cheqSum >= amount) {
                                        //posting cheq account
                                        cheqSum = cheqSum - amount;
                                        addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(amount), branchId, 2);
                                        amount = 0.00;
//                                        break;
                                    } else {
                                        if (cheqSum != 0) {
                                            addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(cheqSum), branchId, 2);
                                            amount = amount - cheqSum;
                                            cheqSum = 0.00;
                                        }
                                        if (amount > 0) {
                                            if (bankSum >= amount) {
                                                addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(amount), branchId, 2);
                                                bankSum = bankSum - amount;
                                                amount = 0.00;
//                                                break;
                                            } else if (bankSum != 0) {
                                                addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(bankSum), branchId, 2);
                                                amount = amount - bankSum;
                                                bankSum = 0.00;
                                            }
                                        }
                                    }
                                }
                            }
                            if (amount == 0) {
                                System.out.println("No Balance. Completed");
                            } else {
                                System.out.println("Error: Balance is:" + amount);
                                msg = "Posting Error:" + "\n" + "Remainig balance for the Transaction No:" + transNo + " and Code:" + code;
//                                return msg;
                            }
                        }
                    }
                    if (transactionList_ovp != null) {
                        String debAccNo = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                        addToPosting2(dayEnd, debAccNo, postingRefNo, new BigDecimal(overPayment), branchId, 1);
                        if (cashSum >= overPayment) {
                            //posting to cash account
                            cashSum = cashSum - overPayment;
                            addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(overPayment), branchId, 2);
                            overPayment = 0.00;
//                            break;
                        } else {
                            if (cashSum != 0) {
                                addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(cashSum), branchId, 2);
                                overPayment = overPayment - cashSum;
                                cashSum = 0.00;
                            }
                            if (overPayment > 0) {
                                if (cheqSum >= overPayment) {
                                    //posting cheq account
                                    cheqSum = cheqSum - overPayment;
                                    addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(overPayment), branchId, 2);
                                    overPayment = 0.00;
//                                    break;
                                } else {
                                    if (cheqSum != 0) {
                                        addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(cheqSum), branchId, 2);
                                        overPayment = overPayment - cheqSum;
                                        cheqSum = 0.00;
                                    }
                                    if (overPayment > 0) {
                                        if (bankSum >= overPayment) {
                                            addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(overPayment), branchId, 2);
                                            bankSum = bankSum - overPayment;
                                            overPayment = 0.00;
//                                            break;
                                        } else if (bankSum != 0) {
                                            addToPosting2(dayEnd, cashInHandAccNo, postingRefNo, new BigDecimal(bankSum), branchId, 2);
                                            overPayment = overPayment - bankSum;
                                            bankSum = 0.00;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        bulkAmount = 0.00;
                    }

                    if (bulkAmount == 0) {
                        System.out.println("Completed Tranaction No:" + transNo);
                    } else {
                        System.out.println("Error: Tranaction " + transNo + " not Completed. Balance=" + bulkAmount);
//                        msg = "Posting Error:" + "\n" + " Transaction No:" + transNo + " is not completed...!";
//                        return msg;
                    }

                    String sqlLi = "FROM SettlementMain WHERE receiptNo='" + refNo + "'";
                    List trans_list = session.createQuery(sqlLi).list();
                    for (int t = 0; t < trans_list.size(); t++) {
                        SettlementMain tran = (SettlementMain) trans_list.get(t);
                        tran.setIsPosting(true);
                        session.update(tran);
                    }
                    System.out.println("settlement update....");
                }
            }

            tx.commit();
        } catch (Exception ep) {
            ep.printStackTrace();

            try {
                tx.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("cannot rollback transaction-calculate odi" + ex);
            }
            return "Error Posting";
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return "OK";
    }

    //add overpayments to arrears
    @Override
    public String findLoanOverPayment(int branchId) {
        Session session = null;
        Transaction tx = null;

        String retMsg = "OK";
        Date dueDate = null;
        String dued = "";
        Date odueDate = null;
        int period = 0;
//        double rate = 0.00;
        try {
            int userId = userDao.findByUserName();
            Date dayEndDate = null;

            String sqld = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date objd = (Date) sessionFactory.getCurrentSession().createQuery(sqld).uniqueResult();
            if (objd != null) {
                dayEndDate = objd;
            }

            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sqlp = "from DayEndConfig where dayEndDate='" + dayEndDate + "' and branchId=" + branchId + " and processId=3 and processStatus=1";
            List config = session.createQuery(sqlp).list();
            if (config.isEmpty()) {
                if (dayEndDate != null) {
                    Calendar cal2 = Calendar.getInstance();
                    cal2.setTime(dayEndDate);
                    cal2.add(Calendar.DATE, 1);
                    dued = sdf.format(cal2.getTime());
                    dueDate = sdf.parse(dued);
                }
                String sql0 = "select period, rate from MOverDue where id=1";
                Object ob[] = (Object[]) session.createQuery(sql0).uniqueResult();
                if (ob != null) {
                    period = Integer.parseInt(ob[0].toString());
//                rate = Double.valueOf(ob[1].toString());
                }
                period = period - 1;
                String overd = "";
                if (dayEndDate != null) {
                    Calendar cal2 = Calendar.getInstance();
                    cal2.setTime(dayEndDate);
                    cal2.add(Calendar.DATE, -period);
                    overd = sdf.format(cal2.getTime());
                    odueDate = sdf.parse(overd);
                }

                String sql = "SELECT ins.insId, ins.loanId, loan.debtorHeaderDetails FROM LoanInstallment AS ins,"
                        + " LoanHeaderDetails AS loan WHERE ins.loanId = loan.loanId AND loan.branchId = " + branchId + " AND ins.insStatus=0 and ins.isPay = 0"
                        + " AND (ins.dueDate = '" + overd + "' OR ins.dueDate = '" + dued + "') ";

                List insLoans = session.createQuery(sql).list();
                if (insLoans.size() > 0) {
                    for (int i = 0; i < insLoans.size(); i++) {
                        Object obj[] = (Object[]) insLoans.get(i);
                        int instId = Integer.parseInt(obj[0].toString());
                        String instNo = "INS" + instId;
                        int loanId = Integer.parseInt(obj[1].toString());
                        DebtorHeaderDetails debtor = (DebtorHeaderDetails) obj[2];
                        double overPayment = settlmentDao.getOverPayAmount(loanId);
                        if (overPayment > 0) {
                            addOverpaymentAdjustment(0.00, overPayment, loanId, instNo, debtor, userId, "");
                        }
                    }
                }
                updateProcessStatus(3, dayEndDate, branchId);
                boolean backupAfter = backupDataWithDatabase(1, branchId);
                changeBlockStatus(dayEndDate, false);

            }
            tx.commit();
            List isPostingBalance = checkPostingBalance(branchId);
//            if (!isPostingBalance.isEmpty()) {
//                String msg = "Finance Posting Not Balancing For Reference no - ";
//                String refNo = "";
//                for (int i = 0; i < isPostingBalance.size(); i++) {
//                    Object obj[] = (Object[]) isPostingBalance.get(i);
//                    refNo = refNo + " # " + obj[1].toString();
//                }
//                retMsg = msg + refNo;
//            } else {
//                updateProcessStatus(17, dayEndDate, branchId);
//                boolean dateId = UpdateDayEnd(branchId, dayEndDate);
//                if (dateId) {
//                    updateProcessStatus(9, dayEndDate, branchId);
//                }
//            }
            updateProcessStatus(17, dayEndDate, branchId);
            boolean dateId = UpdateDayEnd(branchId, dayEndDate);
            if (dateId) {
                updateProcessStatus(9, dayEndDate, branchId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            retMsg = "Add Over Payment Process Failed";
        } finally {
            session.flush();
            session.clear();
            session.close();
        }
        return retMsg;
    }

    public void addOverpaymentAdjustment(double totalPayment, double overPayAmount, int loanId, String newTransactionNo, DebtorHeaderDetails debtor, int userid, String receiptNo) {
        String refNoInstallmentCode = "INS";
        String refNoODICode = "ODI";
        String refNoInstallmentCodeDscrpt = "Installment";
        String refNoRentalInterCode = "RNTI";
        String refNoRentalDiffCode = "EXSS";
        String refNoRentalCode = "RNT";
        String refNoODICodeDscrpt = "Over Due Charge";
        String refNoOverPayCode = "OVP";
        String refNoOverPayCodeDescrpt = "Over Payment add to Installment";
        boolean status = false;
        double updateOverPayAmount = 0;

        int userId = userDao.findByUserName();
        int userLogBranchId = settlmentDao.getUserLogedBranch(userId);
        Date systemDate = settlmentDao.getSystemDate(userLogBranchId);
        List<SettlementMain> installmentListWithOdi = getInstalmntWithODI(loanId);

        LoanInstallment loanInstallment = settlmentDao.findLoanInstallment(loanId);

        double interest = 0;
        double rental = 0;
        double diff = 0;

        if (loanInstallment != null) {
            interest = loanInstallment.getInsInterest();
            rental = loanInstallment.getInsPrinciple();
            diff = loanInstallment.getInsRoundAmount() - (rental + interest);
        }
        if (diff < 0.00) {
            diff = 0.00;
        }
        double totalRental = interest + rental + diff;

        double paidInstalment = 0;
        double totalInstallment = 0;
        double paidOdi = 0;
        double totalOdi = 0;

        if (!installmentListWithOdi.isEmpty()) {
            for (int i = 0; i < installmentListWithOdi.size(); i++) {
                String debtCode = null;
                String credtCode = null;
                debtCode = installmentListWithOdi.get(i).getTransactionCodeDbt();
                if (debtCode != null) {
                    if (debtCode.equals(refNoInstallmentCode)) {
                        totalInstallment = totalInstallment + installmentListWithOdi.get(i).getDbtAmount();
                    } else if (debtCode.equals(refNoODICode)) {
                        totalOdi = totalOdi + installmentListWithOdi.get(i).getDbtAmount();
                    }
                }
                credtCode = installmentListWithOdi.get(i).getTransactionCodeCrdt();
                if (credtCode != null) {
                    if (credtCode.equals(refNoInstallmentCode)) {
                        paidInstalment = paidInstalment + installmentListWithOdi.get(i).getCrdtAmount();
                    } else if (credtCode.equals(refNoODICode)) {
                        paidOdi = paidOdi + installmentListWithOdi.get(i).getCrdtAmount();
                    }
                }
            }
        }

        double intallmentBalance = (totalInstallment + totalOdi) - (paidInstalment + paidOdi);

        //************************ Add Over Pay  ***********      
        double payAmount = 0;

        totalPayment = totalPayment + overPayAmount; //************Add Over Pay                
        if (intallmentBalance <= totalPayment) {
            payAmount = intallmentBalance;
            updateOverPayAmount = totalPayment - payAmount;
            if (overPayAmount > 0) {
                settlmentDao.addOverPayDebit(loanId, overPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, userLogBranchId, systemDate);
                overPayAmount = 0;
            }
            if (0 <= updateOverPayAmount) {
                settlmentDao.addOverPayAmount(loanId, updateOverPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, userLogBranchId, systemDate);
                updateOverPayAmount = 0;
            }
        } else if (totalPayment < intallmentBalance && 0 < totalPayment) {
            payAmount = totalPayment;
            if (overPayAmount > 0) {
                settlmentDao.addOverPayDebit(loanId, overPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, userLogBranchId, systemDate);
                overPayAmount = 0;
            }
        }

        if (0 < intallmentBalance) {
            double paidCurrentOdi = 0;
            if (0 < totalOdi - paidOdi) {
                SettlementMain settlementMain = new SettlementMain();
                settlementMain.setTransactionCodeCrdt(refNoODICode);
                settlementMain.setDescription(refNoODICodeDscrpt);
                settlementMain.setTransactionNoCrdt(newTransactionNo);
                if (totalOdi - paidOdi < payAmount) {
                    paidCurrentOdi = totalOdi - paidOdi;
                    settlementMain.setCrdtAmount(paidCurrentOdi);
                } else {
                    paidCurrentOdi = payAmount;
                    settlementMain.setCrdtAmount(paidCurrentOdi);
                }
                settlementMain.setLoanId(loanId);
                settlementMain.setDebtorId(debtor.getDebtorId());
                settlementMain.setUserId(userid);
                settlementMain.setReceiptNo(receiptNo);
                settlementMain.setIsPosting(false);
                settlementMain.setSystemDate(systemDate);
                settlementMain.setBranchId(userLogBranchId);
                status = settlmentDao.addSettlementMain(settlementMain);

                SettlementCharges settlementCharges = new SettlementCharges();

                settlementCharges.setBillNo(receiptNo);
                settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                settlementCharges.setDebtorId(debtor.getDebtorId());
                settlementCharges.setLoanId(loanId);
                settlementCharges.setReffNo(newTransactionNo);
                settlementCharges.setUserId(userid);
                settlementCharges.setSystemDate(systemDate);
                settlementCharges.setBranchId(userLogBranchId);

                if (totalOdi - paidOdi < payAmount) {
                    paidCurrentOdi = totalOdi - paidOdi;
                    settlementCharges.setCharge(paidCurrentOdi);
                } else {
                    paidCurrentOdi = payAmount;
                    settlementCharges.setCharge(paidCurrentOdi);
                }

                settlementCharges.setChargeType(refNoODICode);

                status = settlmentDao.addChargeDetails(settlementCharges);
            }

            SettlementMain settlementMain = new SettlementMain();
            settlementMain.setTransactionCodeCrdt(refNoInstallmentCode);
            settlementMain.setDescription(refNoInstallmentCodeDscrpt);
            settlementMain.setTransactionNoCrdt(newTransactionNo);
            settlementMain.setCrdtAmount(payAmount - (paidCurrentOdi));
            settlementMain.setLoanId(loanId);
            settlementMain.setDebtorId(debtor.getDebtorId());
            settlementMain.setUserId(userid);
            settlementMain.setReceiptNo(receiptNo);
            settlementMain.setIsPosting(false);
            settlementMain.setSystemDate(systemDate);
            settlementMain.setBranchId(userLogBranchId);

            status = settlmentDao.addSettlementMain(settlementMain);

            //update Loaninstalment isPay Status
            double remainingInstallmentBalance = settlementMain.getCrdtAmount();
            settlmentDao.updateLoanInstallment(loanId, remainingInstallmentBalance, systemDate, settlementMain.getTransactionCodeCrdt(), settlementMain.getCrdtAmount(), receiptNo, payAmount);

            double postChargeAmount = payAmount - (paidCurrentOdi);

            for (int j = 0; 0 < postChargeAmount; j++) {
                SettlementCharges settlementCharges = new SettlementCharges();

                settlementCharges.setBillNo(receiptNo);
                settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                settlementCharges.setDebtorId(debtor.getDebtorId());
                settlementCharges.setLoanId(loanId);
                settlementCharges.setReffNo(newTransactionNo);
                settlementCharges.setUserId(userid);
                settlementCharges.setSystemDate(systemDate);
                settlementCharges.setBranchId(userLogBranchId);

                if (totalRental <= 0.00) {
                    settlementCharges.setCharge(postChargeAmount);
                    settlementCharges.setChargeType(refNoRentalCode);
                    postChargeAmount = 0.00;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                } else if (interest + rental + diff <= postChargeAmount) {
                    settlementCharges.setCharge(rental);
                    settlementCharges.setChargeType(refNoRentalCode);
                    postChargeAmount = postChargeAmount - rental;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                    settlementCharges.setCharge(interest);
                    settlementCharges.setChargeType(refNoRentalInterCode);
                    postChargeAmount = postChargeAmount - interest;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                    settlementCharges.setCharge(diff);
                    settlementCharges.setChargeType(refNoRentalDiffCode);
                    postChargeAmount = postChargeAmount - diff;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                } else if (interest + rental <= postChargeAmount && postChargeAmount < interest + rental + diff) {
                    settlementCharges.setCharge(rental);
                    settlementCharges.setChargeType(refNoRentalCode);
                    postChargeAmount = postChargeAmount - rental;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                    settlementCharges.setCharge(interest);
                    settlementCharges.setChargeType(refNoRentalInterCode);
                    postChargeAmount = postChargeAmount - interest;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                    settlementCharges.setCharge(postChargeAmount);
                    settlementCharges.setChargeType(refNoRentalDiffCode);
                    postChargeAmount = 0;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                } else if (interest < postChargeAmount && postChargeAmount < rental) {
                    settlementCharges.setCharge(interest);
                    settlementCharges.setChargeType(refNoRentalInterCode);
                    postChargeAmount = postChargeAmount - interest;
                    status = settlmentDao.addChargeDetails(settlementCharges);

                    settlementCharges.setCharge(postChargeAmount);
                    settlementCharges.setChargeType(refNoRentalCode);
                    postChargeAmount = 0;
                    status = settlmentDao.addChargeDetails(settlementCharges);
                } else {
                    if (interest < postChargeAmount) {
                        settlementCharges.setCharge(interest);
                        postChargeAmount = postChargeAmount - interest;
                    } else {
                        settlementCharges.setCharge(postChargeAmount);
                        postChargeAmount = 0;
                    }
                    settlementCharges.setChargeType(refNoRentalInterCode);
                    status = settlmentDao.addChargeDetails(settlementCharges);
                }
            }
        }
        if (0 < updateOverPayAmount) {
            settlmentDao.addOverPayAmount(loanId, updateOverPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, userLogBranchId, systemDate);
        }
    }
    //close

    public List<SettlementMain> getInstalmntWithODI(int loanId) {
        List<SettlementMain> loanInstallment = null;
        String refCodeInstalment = "INS";
        String refCodeODI = "ODI";
        String sql1 = "from SettlementMain where loanId=" + loanId + " and ( transactionCodeDbt = '" + refCodeInstalment + "' "
                + " or transactionCodeCrdt = '" + refCodeInstalment + "' or transactionCodeDbt = '" + refCodeODI + "' or transactionCodeCrdt = '" + refCodeODI + "') ";
        try {
            loanInstallment = sessionFactory.getCurrentSession().createQuery(sql1).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanInstallment;
    }

    //posting cheqs to finance which are credit for cash account
    @Override
    public boolean postingCheque(String code, int id) {
        boolean msg = true;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sql1 = "select c.transactionNo, c.chequeNo, c.amount, b.bankAccountNo from SettlementPaymentDetailCheque c,ConfigChartofaccountBank b where c.transactionNo like '" + code + "%' and c.chequeId<=" + id + " and c.bank=b.bankId";
            List cheqList = session.createQuery(sql1).list();
            if (cheqList.size() > 0) {
                for (int i = 0; i < cheqList.size(); i++) {
                    Object[] cheq = (Object[]) cheqList.get(i);
                    String transNo = cheq[0].toString();
                    String chqNo = cheq[1].toString();
                    double amount = Double.parseDouble(cheq[2].toString());
                    String account = cheq[3].toString();
                    System.out.println("vvvv-" + transNo);
                    String chequeInHand = loanDaoImpl.findLoanAccountNo(0, CHEQUE_IN_HAND_ACC_CODE);
                    String sql2 = "from SettlementMain where transactionNoCrdt='" + transNo + "' group by transactionNoCrdt";
                    SettlementMain sett = (SettlementMain) session.createQuery(sql2).uniqueResult();
                    if (sett != null) {
                        int loanId = sett.getLoanId();
                        int setId = sett.getSettlementId();
                        int branchId = sett.getBranchId();
                        String refNo = loanDaoImpl.generateReferencNo(loanId, branchId);
                        refNo = refNo + "/" + setId + "/" + chqNo;
                        if (!account.equals("") && !chequeInHand.equals("")) {
                            if (amount > 0.00) {
                                loanDaoImpl.addToPosting(account, refNo, new BigDecimal(amount), branchId, 1);
                                loanDaoImpl.addToPosting(chequeInHand, refNo, new BigDecimal(amount), branchId, 2);
                            }
                        } else {
                            msg = false;
                        }
                    }
                }
            }
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            msg = false;
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return msg;
    }

    @Override
    public boolean calLapsDate() {
        boolean msg = true;
        List<LoanInstallment> installments = null;
        try {
            String sql = "from LoanInstallment group by loanId";
            installments = sessionFactory.getCurrentSession().createQuery(sql).list();

            if (installments.size() > 0 || !installments.isEmpty()) {
                for (int i = 0; i < installments.size(); i++) {
                    int loanId = installments.get(i).getLoanId();
                    if (loanId > 0) {
                        loanDaoImpl.updateLoanLapsDate(loanId);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            msg = false;
        }
        return msg;
    }

    @Override
    public boolean calNullInstallment() {
        Boolean msg = true;
        List<LoanHeaderDetails> headerDetails = null;
        try {
            String sql = "from LoanHeaderDetails where loanLapsDate is null";
            headerDetails = sessionFactory.getCurrentSession().createQuery(sql).list();

            if (headerDetails.size() > 0 || !headerDetails.isEmpty()) {
                for (int i = 0; i < headerDetails.size(); i++) {
                    int loanId = headerDetails.get(i).getLoanId();
                    if (loanId > 0) {
                        saveLoan_Installment(loanId);
                    }
                }

            }

        } catch (Exception e) {
            msg = false;
            e.printStackTrace();
        }
        return msg;
    }

    public void saveLoan_Installment(int loanId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            int userId = userDao.findByUserName();
            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();

            int period = loan.getLoanPeriod();
            int periodType = loan.getLoanPeriodType();

            double investment = loan.getLoanInvestment();
            double interest = loan.getLoanInterest();
            Double installment = loan.getLoanInstallment();

            int noOfInstallmnt = 0;
            if (periodType == 1) {
                noOfInstallmnt = period;
            } else if (periodType == 2) {
                noOfInstallmnt = period * 12;
            } else if (periodType == 3) {
                int pp = period / 30;
                double dailyInt = (interest * pp) / period;
                interest = dailyInt * 7;
                installment = installment * 7;
                int module = period % 7;
                if (module > 0) {
                    noOfInstallmnt = (period / 7) + 1;
                } else {
                    noOfInstallmnt = period / 7;
                }
            } else if (periodType == 4) {
                noOfInstallmnt = period;
            }

            double principle = investment / noOfInstallmnt;
            Double roundedValue = (Double) (Math.ceil(installment / 5d) * 5);
            double crctInst = principle + interest;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            Calendar cal2 = Calendar.getInstance();
            if (loan.getLoanDueDate() != null) {
                cal2.setTime(loan.getLoanDueDate());
            } else {
                int month = cal.get(Calendar.MONTH) + 1;
                int year = cal.get(Calendar.YEAR);
                int day = loan.getDueDay();
                String due = year + "-" + month + "-" + day;
                Date due_date = new java.sql.Date(sdf.parse(due).getTime());
                cal2.setTime(due_date);
                if (periodType == 3 || periodType == 4 || periodType == 5 || periodType == 6) {
                    cal2.add(Calendar.DATE, 7);
                } else {
                    cal2.add(Calendar.MONTH, 1);
                }
            }
            String paymentDate;
            for (int i = 1; i <= noOfInstallmnt; i++) {
                LoanInstallment instl = new LoanInstallment();
                paymentDate = sdf.format(cal2.getTime());
                instl.setInsPrinciple(principle);
                instl.setInsInterest(interest);
                instl.setInsAmount(crctInst);
                instl.setInsRoundAmount(roundedValue);
                instl.setLoanId(loan.getLoanId());
                instl.setInsDescription("Installment");
                instl.setInsStatus(0);
                instl.setDueDate(sdf.parse(paymentDate));
                instl.setActionTime(actionTime);
                instl.setUserId(userId);
                session.save(instl);
                instl = null;

                if (periodType == 3 || periodType == 4) {
                    cal2.add(Calendar.DATE, 7);
                } else {
                    cal2.add(Calendar.MONTH, 1);
                }
            }

            //save rounduperror account
            double roundErrorValue = (roundedValue - installment) * noOfInstallmnt;
//            addToPosting("", "", new BigDecimal(roundErrorValue), loan.getBranchId(), 1);

            //add other charges to loan chargde
//           addLoanToSettlement(loanId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        // session.flush();
        session.close();
    }

    private List<LoanHeaderDetails> findLapsLoanList(int branchId, int day) {
        List<LoanHeaderDetails> detailses = null;
        try {
            String sql = "from LoanHeaderDetails where branchId = " + branchId + " and loanLapsDate like'%" + day + "'";
            detailses = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return detailses;
    }

    private List<RebateDetail> findRebateLoanList(int branchId, Date dayEndDate) {
        List<RebateDetail> rebateDetail = null;
        try {
            rebateDetail = sessionFactory.getCurrentSession().createCriteria(RebateDetail.class)
                    .add(Restrictions.eq("branchId", branchId))
                    .add(Restrictions.eq("systemDate", dayEndDate)).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return rebateDetail;
    }

    private String postingRebate(int branchId, Date dayEndDate) {
        String msg = "";
        List<RebateDetail> rebateLoanlist = null;
        Double discountAmount = 0.00;
        try {
            rebateLoanlist = findRebateLoanList(branchId, dayEndDate);
            for (int r = 0; r < rebateLoanlist.size(); r++) {
                int loanId = rebateLoanlist.get(r).getLoanId();
                discountAmount = rebateLoanlist.get(r).getDiscountAmount();
                String rebateCode = rebateLoanlist.get(r).getRebateCode();
                String refNo = loanDaoImpl.generateReferencNo(loanId, branchId);

                LoanHeaderDetails headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).uniqueResult();
                int loanType = headerDetails.getLoanType();

                String int_sus_accNo = loanDaoImpl.findLoanAccountNo(loanType, "INTRS");
                String rebateAcc = loanDaoImpl.findLoanAccountNo(0, REBATE_REF_CODE);
                SettlementMain settlementMain = (SettlementMain) sessionFactory.getCurrentSession().createCriteria(SettlementMain.class)
                        .add(Restrictions.eq("loanId", loanId))
                        .add(Restrictions.eq("transactionNoDbt", rebateCode)).uniqueResult();
                refNo = refNo + "/" + settlementMain.getSettlementId();

                if (discountAmount > 0) {
                    if (!int_sus_accNo.equals("") && !rebateAcc.equals("")) {
                        //Transer The rebate Amount From Suspend Interest  Acc to Rebate Acc
                        loanDaoImpl.addToPosting(rebateAcc, refNo, new BigDecimal(discountAmount), branchId, 1);
                        loanDaoImpl.addToPosting(int_sus_accNo, refNo, new BigDecimal(discountAmount), branchId, 2);
                    } else {
                        msg = "Create Account For Loan Rebate";
                        return msg;
                    }
                    String debtorAcc = headerDetails.getDebtorHeaderDetails().getDebtorAccountNo();

                    //Rebate Acc Transer to Debtor's Acc
                    if (!rebateAcc.equals("") && !debtorAcc.equals("")) {
                        loanDaoImpl.addToPosting(rebateAcc, refNo, new BigDecimal(discountAmount), branchId, 2);
                        loanDaoImpl.addToPosting(debtorAcc, refNo, new BigDecimal(discountAmount), branchId, 1);
                    } else {
                        msg = "Create Account for Debtor -" + loanId;
                        return msg;
                    }
                }
                //implememt posting fully paid

                settlementMain.setIsPosting(true);
                sessionFactory.getCurrentSession().update(settlementMain);
                settlementMain = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            msg = "Can't Posting Rebate-";
        }
        return "OK";
    }

    private boolean changeLoanLapsStatus(int branchId, Date dayEndDate) {
        Boolean proc11 = true;
        Calendar c = Calendar.getInstance();
        c.setTime(dayEndDate);
        c.add(Calendar.DATE, 1);
        Date lapsDay = c.getTime();
        c.setTime(lapsDay);
        int day = c.get(Calendar.DAY_OF_MONTH);

        c.add(Calendar.DATE, 1);
        Date checkDate = c.getTime();

        List<LoanHeaderDetails> lapsLoanList = findLapsLoanList(branchId, day);
        Double balance = 0.00;
        try {
            for (int t = 0; t < lapsLoanList.size(); t++) {
                balance = findBalanceWithOverPay(lapsLoanList.get(t).getLoanId());
                DecimalFormat df = new DecimalFormat("0.00");
                String formate = df.format(balance);
                double finalValue = Double.parseDouble(formate);
                LoanHeaderDetails details = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                        .add(Restrictions.eq("loanId", lapsLoanList.get(t).getLoanId())).uniqueResult();
                Date lapsDate = details.getLoanLapsDate();

                if (lapsDate.before(checkDate)) {
                    if (finalValue == 0) {
                        details.setLoanSpec(4);// Loan is fully Paid
                    } else if (details.getLoanSpec() == 0) {
                        details.setLoanSpec(1); // Loan is Laps  
                    }
                }
                sessionFactory.getCurrentSession().update(details);
                details = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Can't change Loan Laps Status");
            proc11 = false;
        }
        return proc11;

    }

    private String postingRebateSettlement(int branchId, Date dayEndDate) {
        String msg = "";
        List<SettlementMain> settlementMain = null;
        String cashInHandAccNo = loanDaoImpl.findLoanAccountNo(0, CASH_IN_HAND_ACC_CODE);
        String chequeInHand = loanDaoImpl.findLoanAccountNo(0, CHEQUE_IN_HAND_ACC_CODE);
        try {
            settlementMain = sessionFactory.getCurrentSession().createCriteria(SettlementMain.class)
                    .add(Restrictions.eq("branchId", branchId))
                    .add(Restrictions.eq("transactionCodeCrdt", REBATE_REF_CODE))
                    .add(Restrictions.eq("systemDate", dayEndDate)).list();
            for (int i = 0; i < settlementMain.size(); i++) {
                SettlementMain main = (SettlementMain) settlementMain.get(i);
                int loanId = main.getLoanId();
                int settId = main.getSettlementId();
                String transNo = main.getTransactionNoCrdt();
                String refNo2 = loanDaoImpl.generateReferencNo(loanId, branchId);
                refNo2 = refNo2 + "/" + settId;
                String debtorAcc = findDebtorAccNo(loanId);
                Double rebateAmount = main.getCrdtAmount();
                SettlementPayments settlementPayment = (SettlementPayments) sessionFactory.getCurrentSession().createCriteria(SettlementPayments.class)
                        .add(Restrictions.eq("transactionNo", transNo))
                        .add(Restrictions.eq("loanId", loanId)).uniqueResult();
                //find Payment Type
                int paymentType = settlementPayment.getPaymentType();
                String tranActionNo = settlementPayment.getTransactionNo();
                //only cash Payment
                if (paymentType == 1) {
                    //posting-cash_in_hand_account
                    if (!cashInHandAccNo.equals("") && !debtorAcc.equals("")) {
                        if (rebateAmount > 0.00) {
                            loanDaoImpl.addToPosting(cashInHandAccNo, refNo2, new BigDecimal(rebateAmount), branchId, 2);
                            loanDaoImpl.addToPosting(debtorAcc, refNo2, new BigDecimal(rebateAmount), branchId, 1);
                        }
                    } else {
                        if (cashInHandAccNo.equals("")) {
                            msg = "Assing Account No Cash In Hand Account-";
                        } else {
                            msg = "Can't Find Debtor Account No";
                        }
                        return msg;
                    }
                    // only Cheque Deposit
                } else if (paymentType == 2) {
                    SettlementPaymentDetailCheque paymentDetailCheque = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createCriteria(SettlementPaymentDetailCheque.class)
                            .add(Restrictions.eq("transactionNo", tranActionNo)).uniqueResult();
                    refNo2 = refNo2 + "/" + paymentDetailCheque.getChequeNo();
                    if (!debtorAcc.equals("") && !chequeInHand.equals("")) {
                        if (rebateAmount > 0.00) {
                            loanDaoImpl.addToPosting(chequeInHand, refNo2, new BigDecimal(rebateAmount), branchId, 2);
                            loanDaoImpl.addToPosting(debtorAcc, refNo2, new BigDecimal(rebateAmount), branchId, 1);
                        }
                    } else {
                        if (chequeInHand.equals("")) {
                            msg = "Assing Account No Cheques In Hand Account-";
                        } else {
                            msg = "Can't Find Debtor Account No";
                        }
                        return msg;
                    }
                    //only Direct Bank Deposit
                } else if (paymentType == 3) {
                    SettlementPaymentDetailBankDeposite bankDeposite = (SettlementPaymentDetailBankDeposite) sessionFactory.getCurrentSession().createCriteria(SettlementPaymentDetailBankDeposite.class)
                            .add(Restrictions.eq("transactionNo", tranActionNo)).uniqueResult();
                    //select Bank Details
                    String bankAccounNo = findBankAccNo(bankDeposite.getBankId());
                    if (!bankAccounNo.equals("") && !debtorAcc.equals("")) {
                        if (rebateAmount > 0.00) {
                            loanDaoImpl.addToPosting(bankAccounNo, refNo2, new BigDecimal(rebateAmount), branchId, 2);
                            loanDaoImpl.addToPosting(debtorAcc, refNo2, new BigDecimal(rebateAmount), branchId, 1);
                        }
                    } else {
                        if (bankAccounNo.equals("")) {
                            msg = "Can't find Bank Account No";
                        } else {
                            msg = "Can't Find Debtor Account No";
                        }
                        return msg;
                    }
                }
                main.setIsPosting(true);
                sessionFactory.getCurrentSession().update(main);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "OK";
    }

    private String findDebtorAccNo(int loanId) {
        String debtorAccNo = "";

        try {
            LoanHeaderDetails headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            debtorAccNo = headerDetails.getDebtorHeaderDetails().getDebtorAccountNo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return debtorAccNo;
    }

    private String findBankAccNo(int bankId) {
        String accountNo = null;

        try {
            MBankDetails bankDetails = (MBankDetails) sessionFactory.getCurrentSession().createCriteria(MBankDetails.class
            )
                    .add(Restrictions.eq("id", bankId)).uniqueResult();
            accountNo = bankDetails.getAccountNo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return accountNo;
    }

    private String createDebtorBirthDayWishes(int branchId, Date dayEndDate) {
        String msg = "";
        List<DebtorHeaderDetails> debtorHeaderDetails = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar c = Calendar.getInstance();
            Date actionTime = c.getTime();
            c.setTime(dayEndDate);
            c.add(Calendar.DATE, 1);
            Date date = c.getTime();
            c.setTime(date);
            String dd = sdf.format(date);
            String[] mm = dd.split("-");
            String month = mm[1];
            String day = mm[2];
            String birthDay = month + "-" + day;

            String sql = "from DebtorHeaderDetails where branchId = " + branchId + " and debtorIsDebtor = 1 and debtorDob like '%" + birthDay + "'";
            debtorHeaderDetails = sessionFactory.getCurrentSession().createQuery(sql).list();

            for (DebtorHeaderDetails debtorHeaderDetail : debtorHeaderDetails) {
                if (!debtorHeaderDetail.getDebtorTelephoneMobile().equals("") && debtorHeaderDetail.getDebtorTelephoneMobile() != null) {
                    String message = "MANY HAPPY RETURNS OF THE DAY...!!! WIJITHA FINANCE LIMITED.";
                    SmsDetails smsDetails = new SmsDetails();
                    smsDetails.setBranchId(branchId);
                    smsDetails.setIsDelete(false);
                    smsDetails.setIsSendTo(false);
                    smsDetails.setSmsDebtorName(debtorHeaderDetail.getDebtorName());
                    if (!debtorHeaderDetail.getDebtorTelephoneMobile().equals("") && debtorHeaderDetail.getDebtorTelephoneMobile() != null) {
                        smsDetails.setSmsDebtorNumber(Integer.parseInt(debtorHeaderDetail.getDebtorTelephoneMobile()));
                    }
                    smsDetails.setSmsLoanId(0);
                    smsDetails.setSmsType(new SmsType(1));
                    smsDetails.setActionTime(actionTime);
                    smsDetails.setSmsDescription(message);
                    session.save(smsDetails);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            msg = "Can't Create birthDay Wishes";
            transaction.rollback();
        } finally {
            transaction.commit();
            session.flush();
            session.close();

        }
        return "OK";
    }

    public Double findBalanceWithOverPay(int loanId) {
        double balance = 0.00;

        try {
            double openArrears = reportDaoImpl.getOpenArrears(loanId);

            String sql_c = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeCrdt='ARRS' OR transactionCodeCrdt='OTHR' OR transactionCodeCrdt='INSU' "
                    + "OR transactionCodeCrdt='DWNS' OR transactionCodeCrdt='OVP' OR transactionCodeCrdt='INS' "
                    + "OR transactionCodeCrdt='ODI' OR transactionCodeCrdt = 'RBT') AND loanId=" + loanId;

            String sql_d = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeDbt='ARRS' OR transactionCodeDbt='OTHR' OR transactionCodeDbt='INSU' "
                    + "OR transactionCodeDbt='DWNS' OR transactionCodeDbt='OVP' OR transactionCodeDbt='INS' "
                    + "OR transactionCodeDbt='ODI' OR transactionCodeDbt='RBT') AND loanId=" + loanId;

            double creditSum = 0.00;
            double debitSum = 0.00;

            Object totalCredit = sessionFactory.getCurrentSession().createQuery(sql_c).uniqueResult();
            if (totalCredit != null) {
                creditSum = Double.parseDouble(totalCredit.toString());
            }
            Object totalDebit = sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (totalDebit != null) {
                debitSum = Double.parseDouble(totalDebit.toString());
            }
            double diff = debitSum - creditSum;
            balance = diff + openArrears;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return balance;
    }

    private String calculateDayEndFloatBalance(int branchId, Date dayEndDate) {
        String msg = "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(dayEndDate);
        cal.add(Calendar.DATE, -1);
        Date prevDate = cal.getTime();
        double t_cheqIn = 0.00;
        double t_cashIn = 0.00;
        double t_prevDayCashIn = 0.00;
        double t_prevDayChequeIn = 0.00;
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // get total chash-in
            String sql1 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + branchId + " "
                    + "AND systemDate='" + dayEndDate + "' AND paymentType=1";
            Object loanCash = session.createQuery(sql1).uniqueResult();
            if (loanCash != null) {
                t_cashIn = Double.parseDouble(loanCash.toString());
            }

            String sql2 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE paymentType = 1 AND branchId =" + branchId
                    + " AND systemDate ='" + dayEndDate + "'";
            Object otherCash = session.createQuery(sql2).uniqueResult();
            if (otherCash != null) {
                t_cashIn = t_cashIn + Double.parseDouble(otherCash.toString());
            }

            String sql3 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId =" + branchId + " AND"
                    + " voucherDate='" + dayEndDate + "' AND voucherCatogery NOT IN (9) AND loanId=0 AND voucherNo LIKE 'O%'";
            Object cashOut = session.createQuery(sql3).uniqueResult();
            if (cashOut != null) {
                t_cashIn = t_cashIn - Double.parseDouble(cashOut.toString());
            }

            // get total cheque-in
            String sql4 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + branchId + " "
                    + "AND systemDate='" + dayEndDate + "' AND paymentType=2";
            Object loanCheq = session.createQuery(sql4).uniqueResult();
            if (loanCheq != null) {
                t_cheqIn = Double.parseDouble(loanCheq.toString());
            }

            String sql5 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE branchId=" + branchId + " "
                    + "AND systemDate='" + dayEndDate + "' AND paymentType=2";
            Object otherCheq = session.createQuery(sql5).uniqueResult();
            if (otherCheq != null) {
                t_cheqIn = t_cheqIn + Double.parseDouble(otherCheq.toString());
            }

            String sql6 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId=" + branchId + " AND voucherDate='" + dayEndDate + "'"
                    + " AND voucherCatogery=9";
            Object cheqOut = session.createQuery(sql6).uniqueResult();
            if (cheqOut != null) {
                t_cheqIn = t_cheqIn - Double.parseDouble(cheqOut.toString());
            }

            String sql7 = "FROM DayEndFloatBalance WHERE branchId=" + branchId + " "
                    + "AND dayEndDate='" + sdf.format(prevDate) + "'";
            DayEndFloatBalance prevBalance = (DayEndFloatBalance) sessionFactory.getCurrentSession().createQuery(sql7).uniqueResult();
            if (prevBalance != null) {
                t_prevDayCashIn = prevBalance.getCashBalance();
                t_prevDayChequeIn = prevBalance.getChequeBalance();
            }

            t_cashIn = t_cashIn + t_prevDayCashIn;
            t_cheqIn = t_cheqIn + t_prevDayChequeIn;

            DayEndFloatBalance dayEndFloatBalance = new DayEndFloatBalance();
            dayEndFloatBalance.setActionTime(Calendar.getInstance().getTime());
            dayEndFloatBalance.setBranchId(branchId);
            dayEndFloatBalance.setCashBalance(t_cashIn);
            dayEndFloatBalance.setChequeBalance(t_cheqIn);
            dayEndFloatBalance.setDayEndDate(dayEndDate);

            session.saveOrUpdate(dayEndFloatBalance);
            tx.commit();
            msg = "OK";
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            msg = "Can't Calculate Cashier Balances";
        } finally {
            session.flush();
            session.close();
        }
        return msg;
    }

    @Override
    public boolean calculateBalances() {
        boolean success = false;
        Calendar cal = Calendar.getInstance();
        Date sDate = null;
        Date eDate = null;
        Date prevDate = null;
        int branchId = 0;
        List<Date> dates = new ArrayList<>();
        List<DayEndFloatBalance> balances = new ArrayList<>();
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            List<MBranch> branches = adminDaoImpl.findBranch();
            String sql1 = "SELECT dayEndDate FROM DayEndDate ORDER BY dayEndId ASC";
            Object obj1 = session.createQuery(sql1).setMaxResults(1).uniqueResult();
            if (obj1 != null) {
                sDate = (Date) obj1;
            }
            String sql2 = "SELECT dayEndDate FROM DayEndDate ORDER BY dayEndId DESC";
            Object obj2 = session.createQuery(sql2).setMaxResults(1).uniqueResult();
            if (obj2 != null) {
                eDate = (Date) obj2;
            }
            if (sDate != null && eDate != null) {
                cal.setTime(sDate);
                while (cal.getTime().before(eDate)) {
                    dates.add(cal.getTime());
                    cal.add(Calendar.DATE, 1);
                }
            }
            for (MBranch mBranch : branches) {
                branchId = mBranch.getBranchId();
                for (Date dayEndDate : dates) {
                    cal.setTime(dayEndDate);
                    cal.add(Calendar.DATE, -1);
                    prevDate = cal.getTime();

                    double t_cheqIn = 0.00;
                    double t_cheqOut = 0.00;
                    double t_cashIn = 0.00;
                    double t_cashOut = 0.00;
                    double t_prevDayCashIn = 0.00;
                    double t_prevDayCashOut = 0.00;
                    double t_prevDayChequeIn = 0.00;
                    double t_prevDayChequeOut = 0.00;

                    // get total chash-in
                    String sql3 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + branchId + " "
                            + "AND systemDate='" + sdf.format(dayEndDate) + "' AND paymentType=1";
                    Object loanCash = session.createQuery(sql3).uniqueResult();
                    if (loanCash != null) {
                        t_cashIn = Double.parseDouble(loanCash.toString());
                    }

                    String sql4 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE paymentType = 1 AND branchId =" + branchId
                            + " AND systemDate ='" + sdf.format(dayEndDate) + "'";
                    Object otherCash = session.createQuery(sql4).uniqueResult();
                    if (otherCash != null) {
                        t_cashIn = t_cashIn + Double.parseDouble(otherCash.toString());
                    }

                    String sql5 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId =" + branchId + " AND"
                            + " voucherDate='" + sdf.format(dayEndDate) + "' AND voucherCatogery NOT IN (9) AND loanId=0 AND voucherNo LIKE 'O%'";
                    Object cashOut = session.createQuery(sql5).uniqueResult();
                    if (cashOut != null) {
                        t_cashOut = Double.parseDouble(cashOut.toString());
                    }

                    // get total cheque-in
                    String sql6 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + branchId + " "
                            + "AND systemDate='" + sdf.format(dayEndDate) + "' AND paymentType=2";
                    Object loanCheq = session.createQuery(sql6).uniqueResult();
                    if (loanCheq != null) {
                        t_cheqIn = Double.parseDouble(loanCheq.toString());
                    }

                    String sql7 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE branchId=" + branchId + " "
                            + "AND systemDate='" + sdf.format(dayEndDate) + "' AND paymentType=2";
                    Object otherCheq = session.createQuery(sql7).uniqueResult();
                    if (otherCheq != null) {
                        t_cheqIn = t_cheqIn + Double.parseDouble(otherCheq.toString());
                    }

                    String sql8 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId=" + branchId + " AND voucherDate='" + sdf.format(dayEndDate) + "'"
                            + " AND voucherCatogery=9";
                    Object cheqOut = session.createQuery(sql8).uniqueResult();
                    if (cheqOut != null) {
                        t_cheqOut = Double.parseDouble(cheqOut.toString());
                    }

                    // get total cash-in for previous day
                    String sql9 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + branchId + " "
                            + "AND systemDate='" + sdf.format(prevDate) + "' AND paymentType=1";
                    Object prevDayLoanCash = session.createQuery(sql9).uniqueResult();
                    if (prevDayLoanCash != null) {
                        t_prevDayCashIn = Double.parseDouble(prevDayLoanCash.toString());
                    }

                    String sql10 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE paymentType = 1 AND branchId =" + branchId
                            + " AND systemDate ='" + sdf.format(prevDate) + "'";
                    Object prevDayOtherCash = session.createQuery(sql10).uniqueResult();
                    if (prevDayOtherCash != null) {
                        t_prevDayCashIn = t_prevDayCashIn + Double.parseDouble(prevDayOtherCash.toString());
                    }

                    String sql11 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId =" + branchId + " AND"
                            + " voucherDate='" + sdf.format(prevDate) + "' AND voucherCatogery NOT IN (9) AND loanId=0 AND voucherNo LIKE 'O%'";
                    Object prevDayCashOut = session.createQuery(sql11).uniqueResult();
                    if (prevDayCashOut != null) {
                        t_prevDayCashOut = Double.parseDouble(prevDayCashOut.toString());
                    }

                    // get total cheque-in for previous day
                    String sql12 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + branchId + " "
                            + "AND systemDate='" + sdf.format(prevDate) + "' AND paymentType=2";
                    Object prevDayLoanCheq = session.createQuery(sql12).uniqueResult();
                    if (prevDayLoanCheq != null) {
                        t_prevDayChequeIn = Double.parseDouble(prevDayLoanCheq.toString());
                    }

                    String sql13 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE branchId=" + branchId + " "
                            + "AND systemDate='" + sdf.format(prevDate) + "' AND paymentType=2";
                    Object prevDayOtherCheq = session.createQuery(sql13).uniqueResult();
                    if (prevDayOtherCheq != null) {
                        t_prevDayChequeIn = t_prevDayChequeIn + Double.parseDouble(prevDayOtherCheq.toString());
                    }

                    String sql14 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId=" + branchId + " AND voucherDate='" + sdf.format(prevDate) + "'"
                            + " AND voucherCatogery=9";
                    Object prevDayCheqOut = session.createQuery(sql14).uniqueResult();
                    if (prevDayCheqOut != null) {
                        t_prevDayChequeOut = Double.parseDouble(prevDayCheqOut.toString());
                    }

                    t_cashIn = (t_cashIn + t_prevDayCashIn) - (t_cashOut + t_prevDayCashOut);
                    t_cheqIn = (t_cheqIn + t_prevDayChequeIn) - (t_cheqOut + t_prevDayChequeOut);

                    DayEndFloatBalance dayEndFloatBalance = new DayEndFloatBalance();
                    dayEndFloatBalance.setActionTime(Calendar.getInstance().getTime());
                    dayEndFloatBalance.setBranchId(branchId);
                    dayEndFloatBalance.setCashBalance(t_cashIn);
                    dayEndFloatBalance.setChequeBalance(t_cheqIn);
                    dayEndFloatBalance.setDayEndDate(dayEndDate);

                    balances.add(dayEndFloatBalance);
                }
            }

            if (!balances.isEmpty()) {
                tx = session.beginTransaction();
                for (int i = 0; i < balances.size(); i++) {
                    DayEndFloatBalance balance = balances.get(i);
                    session.save(balance);
                    if (i % 20 == 0) { //20(batch-size),Release memory after batch inserts.
                        session.flush();
                        session.clear();
                    }
                }
                tx.commit();
                success = true;
            }
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return success;
    }

    @Override
    public List<DayEndDate> dayEndProcessBranches() {
        List<DayEndDate> dayEndDates = null;
        try {
            String sql = "select branchId,max(dayEndDate) from DayEndDate group by branchId";
            dayEndDates = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dayEndDates;
    }

    private String calculateInsuranceLateChargers(int branchId, Date dayEndDate) {
        String msg = "";
        List<InsuraceProcessMain> main = null;
        Session session = null;
        Transaction transaction = null;
        int userId = userDao.findByUserName();
        int diff = 0;
        Calendar c = Calendar.getInstance();
        Date actionTime = c.getTime();

        Calendar cal = Calendar.getInstance();
        cal.setTime(dayEndDate);
        cal.add(Calendar.DATE, -1);
        String date = sdf.format(cal.getTime());

        int gracePeroid = 0;

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Date systemDate = sdf.parse(date);
            InsuranceLateDates lateDates = (InsuranceLateDates) session.createCriteria(InsuranceLateDates.class).uniqueResult();

            main = session.createCriteria(InsuraceProcessMain.class).add(Restrictions.eq("branchId", branchId))
                    .add(Restrictions.eq("isPay", false))
                    .add(Restrictions.le("insuGracePeriod", systemDate)).list();

            for (int i = 0; i < main.size(); i++) {
                InsuraceProcessMain processMain = main.get(i);
                String startDate = sdf.format(processMain.getActionTime()) + " 00:00:00";
                String sysDate = sdf.format(systemDate) + " 00:00:00";
                Date startDate1 = sdf2.parse(startDate);
                Date sysDate1 = sdf2.parse(sysDate);
                long l = sysDate1.getTime() - startDate1.getTime();
                diff = (int) l / (24 * 60 * 60 * 1000);
                SettlementMain sett = new SettlementMain();
                sett.setLoanId(processMain.getInsuLoanId());
                sett.setDebtorId(processMain.getInsuDebtorId());
                sett.setTransactionCodeDbt("OTHR");
                sett.setTransactionNoDbt("ILC" + processMain.getInsuId());
                Double lateCharge = 0.00;
                gracePeroid = findInsuGraceDays(processMain.getActionTime(), processMain.getInsuGracePeriod());
                if (diff == gracePeroid) {
                    lateCharge = Double.valueOf(df2.format(processMain.getInsuTotalPremium() * lateDates.getRate() * gracePeroid / 3000));
                } else {
                    lateCharge = Double.valueOf(df2.format(processMain.getInsuTotalPremium() * lateDates.getRate() / 3000));
                }

                sett.setDbtAmount(lateCharge);
                sett.setDescription("Insurance Late Charge");
                sett.setSystemDate(dayEndDate);
                sett.setIsPosting(true);
                sett.setIsPrinted(false);
                sett.setBranchId(branchId);
                sett.setUserId(userId);
                sett.setActionDate(actionTime);
                session.save(sett);

                int saveId = sett.getSettlementId();
                String refNo = loanDaoImpl.generateReferencNo(processMain.getInsuLoanId(), branchId);
                refNo = refNo + "/" + saveId;

                String insuLateChargeAccount = loanDaoImpl.findLoanAccountNo(0, INSURANCE_LATE_CHARGE_CODE);
                Integer subTaxId = recoveryDaoImpl.findSubTaxDetails(insuLateChargeAccount);
                LoanOtherCharges charges = new LoanOtherCharges();
                charges.setLoanId(processMain.getInsuLoanId());
                charges.setChargesId(subTaxId);
                charges.setAccountNo(insuLateChargeAccount);
                charges.setActualAmount(lateCharge);
                charges.setChargeAmount(lateCharge);
                charges.setUserId(userId);
                charges.setIsDownPaymentCharge(0);
                charges.setIsPaid(0);
                charges.setActionTime(actionTime);
                session.save(charges);
                String debtorAcc = findDebtorAccNo(processMain.getInsuLoanId());

                if (!debtorAcc.equals("") && !insuLateChargeAccount.equals("")) {
                    if (lateCharge > 0.00) {
                        loanDaoImpl.addToPosting(debtorAcc, refNo, new BigDecimal(lateCharge), branchId, 2);
                        loanDaoImpl.addToPosting(insuLateChargeAccount, refNo, new BigDecimal(lateCharge), branchId, 1);
                    }
                } else {
                    return "Can't Find Account No for Loan Id" + processMain.getInsuLoanId() + "or Insurance Late Charge Account No";
                }
            }
            msg = "OK";
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            msg = "Insurance Late Chargers Calculation Fail";
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return msg;
    }

    private Boolean updateInstalmentHalfPayments(int insId, int loanId, Date dayEndDate) {
        Session session = null;
        Transaction transaction = null;
        Boolean updated = false;
        Double creditAmount = 0.00;
        Double debitAmount = 0.00;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            LoanInstallment li = (LoanInstallment) session.createCriteria(LoanInstallment.class).add(Restrictions.eq("insId", insId))
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            if (li != null) {
                //paid ins,odi, ovp
                String sqlc = "SELECT SUM(crdtAmount) FROM SettlementMain AS se WHERE se.transactionCodeCrdt='INS' AND Loan_Id=" + loanId;
                Object credit = session.createQuery(sqlc).uniqueResult();
                if (credit != null) {
                    creditAmount = Double.valueOf(credit.toString());
                }
                //to be paid ins, odi
                String sqld = "SELECT SUM(dbtAmount) FROM SettlementMain AS se WHERE se.transactionCodeDbt='INS' AND Loan_Id=" + loanId;
                Object debit = session.createQuery(sqld).uniqueResult();
                if (debit != null) {
                    debitAmount = Double.valueOf(debit.toString());
                }
                Double balance = debitAmount - creditAmount;
                if (balance == 0.00) {
                    li.setIsPay(1);
                    li.setIsArrears(0);
                    session.update(li);
                }
            }
            updated = true;
            transaction.commit();
        } catch (Exception e) {
            updated = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return updated;
    }

    //Instalment half Payment Process
    private String calculateInstamentHalfPayments(int branchId, Date dayEndDate) {
        Session session = null;
        Transaction transaction = null;
        String msg = "OK";
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Boolean update = false;
            List installments = null;
            String sql1 = "select ins.insId,ins.loanId from LoanInstallment as ins,LoanHeaderDetails as lh where lh.loanId = ins.loanId and lh.branchId = '" + branchId + "' and ins.isPay = -1";
            installments = session.createQuery(sql1).list();

            for (int i = 0; i < installments.size(); i++) {
                Object[] ins = (Object[]) installments.get(i);
                int insId = Integer.parseInt(ins[0].toString());
                int loanId = Integer.parseInt(ins[1].toString());
                update = updateInstalmentHalfPayments(insId, loanId, dayEndDate);
                if (!update) {
                    msg = "Instalment Half Payment Calculation Process Fail";
                }
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            msg = "Instalment Half Payment Calculation Process Fail";

        } finally {
            session.flush();
            session.close();
        }
        return msg;
    }

    private int findInsuGraceDays(Date actionTime, Date insuGracePeriod) {
        int gracePeriod = 0;
        try {
            String d1 = sdf.format(actionTime) + " 00:00:00";
            String d2 = sdf.format(insuGracePeriod) + " 00:00:00";
            Date date1 = sdf2.parse(d1);
            Date date2 = sdf2.parse(d2);
            long l = date2.getTime() - date1.getTime();
            gracePeriod = (int) l / (24 * 60 * 60 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gracePeriod;
    }

    private List checkPostingBalance(int branchId) {
        List unBalanceList = new ArrayList();
        try {
            //posting balance quary
            //String sql = "SELECT Date_,Reference_No,SUM(C_Amount)-SUM(D_Amount) AS Bal FROM posting WHERE Posting_State=1 AND Branch_Id = "+branchId+"  GROUP BY Reference_No HAVING Bal != 0.00";
            String sql = "select p.date,p.referenceNo,SUM(p.CAmount)-SUM(p.DAmount) AS bal from Posting p where p.branchId = " + branchId + " group by p.referenceNo HAVING SUM(p.CAmount)-SUM(p.DAmount) != 0.00";

            unBalanceList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unBalanceList;
    }

}

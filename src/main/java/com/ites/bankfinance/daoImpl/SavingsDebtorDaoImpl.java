package com.ites.bankfinance.daoImpl;

import com.ites.bankfinance.dao.SavingsDebtorDao;
import com.ites.bankfinance.savingsModel.DebtorHeaderDetails;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by AXA-FINANCE on 5/24/2018.
 */
@Repository
public class SavingsDebtorDaoImpl implements SavingsDebtorDao {

    @Autowired
    SessionFactory savingsSessionFactory;

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    UserService userService;

    @Autowired
    LoanService loanService;

    @Autowired
    DebtorDaoImpl debtorDaoImpl;

    @Override
    public int saveDebtorDetails(DebtorHeaderDetails loanForm){

        if(!loanService.findByNicNo(loanForm.getDebtorNic(), loanForm.getBranchId())){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        int debtorId = 0;
        try {
            //user Id
            int userId = userService.findByUserName();

            //current date
            Calendar calendar = Calendar.getInstance();
            Date actionTime = calendar.getTime();

            //add debtor basic details
            com.ites.bankfinance.model.DebtorHeaderDetails debtor = new com.ites.bankfinance.model.DebtorHeaderDetails();
            if (loanForm.getDebtorId() != null) {
                debtor.setDebtorId(loanForm.getDebtorId());
            }
            debtor.setDebtorName(loanForm.getDebtorName());
            debtor.setNameWithInitial(loanForm.getDebtorNameWithIni());
            debtor.setDebtorNic(loanForm.getDebtorNic());
            debtor.setDebtorDob(loanForm.getDebtorDob());
            debtor.setDebtorPersonalAddress(loanForm.getDebtorPersonalAddress());
            debtor.setDebtorFormalAddress(loanForm.getDebtorFormalAddress());
            debtor.setDebtorTelephoneHome(loanForm.getDebtorTelephoneHome());
            debtor.setDebtorTelephoneMobile(loanForm.getDebtorTelephoneMobile());
            debtor.setDebtorTelephoneWork(loanForm.getDebtorTelephoneWork());
            debtor.setDebtorFax(loanForm.getDebtorFax());
            debtor.setDebtorEmail(loanForm.getDebtorEmail());
            debtor.setDebtorAgOffice(loanForm.getDebtorAgOffice());
            debtor.setDebtorPostOffice(loanForm.getDebtorPostOffice());
            debtor.setDebtorPoliceStation(loanForm.getDebtorPoliceStation());
            debtor.setMemberNumber(loanForm.getMemberNumber());
            debtor.setJobInformation(loanForm.getJobInformation());
            debtor.setCenterDay(loanForm.getCenterDay());
            debtor.setBankBranch(loanForm.getBankBranch());
            debtor.setAccountName(loanForm.getAccountName());
            debtor.setAccountNo(loanForm.getAccountNo());
            if (loanForm.getDebtorIsDebtor()== null || loanForm.getDebtorIsDebtor() == false ) {
                debtor.setDebtorIsDebtor(false);
                debtor.setDebtorAccountNo("");
            } else {
                debtor.setDebtorIsDebtor(true);
                if (loanForm.getDebtorAccountNo()==null || loanForm.getDebtorAccountNo().isEmpty() ) {
                    String AccountNo = debtorDaoImpl.generateDebtorAccount();
                    debtor.setDebtorAccountNo(AccountNo);
                } else {
                    debtor.setDebtorAccountNo(loanForm.getDebtorAccountNo());
                }
            }
            debtor.setDebtorMap(loanForm.getDebtorMap());
            debtor.setDebtorImage(loanForm.getDebtorImage());
            debtor.setBranchId(loanForm.getBranchId());
            debtor.setUserId(userId);
            debtor.setActionTime(actionTime);
            session.saveOrUpdate(debtor);

            tx.commit();
            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        }

        Session session = savingsSessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        int debtorId = 0;
        try {
            //user Id
            int userId = userService.findByUserName();

            //current date
            Calendar calendar = Calendar.getInstance();
            Date actionTime = calendar.getTime();

            //add debtor basic details
            DebtorHeaderDetails debtor = new DebtorHeaderDetails();
            if (loanForm.getDebtorId() != null) {
                debtor.setDebtorId(loanForm.getDebtorId());
            }
            debtor.setDebtorName(loanForm.getDebtorName());
            debtor.setDebtorNameWithIni(loanForm.getDebtorNameWithIni());
            debtor.setDebtorNic(loanForm.getDebtorNic());
            debtor.setDebtorDob(loanForm.getDebtorDob());
            debtor.setDebtorPersonalAddress(loanForm.getDebtorPersonalAddress());
            debtor.setDebtorFormalAddress(loanForm.getDebtorFormalAddress());
            debtor.setDebtorTelephoneHome(loanForm.getDebtorTelephoneHome());
            debtor.setDebtorTelephoneMobile(loanForm.getDebtorTelephoneMobile());
            debtor.setDebtorTelephoneWork(loanForm.getDebtorTelephoneWork());
            debtor.setDebtorFax(loanForm.getDebtorFax());
            debtor.setDebtorEmail(loanForm.getDebtorEmail());
            debtor.setDebtorAgOffice(loanForm.getDebtorAgOffice());
            debtor.setDebtorPostOffice(loanForm.getDebtorPostOffice());
            debtor.setDebtorPoliceStation(loanForm.getDebtorPoliceStation());
            debtor.setMemberNumber(loanForm.getMemberNumber());
            debtor.setJobInformation(loanForm.getJobInformation());
            debtor.setCenterDay(loanForm.getCenterDay());
            debtor.setBankBranch(loanForm.getBankBranch());
            debtor.setAccountName(loanForm.getAccountName());
            debtor.setAccountNo(loanForm.getAccountNo());
            debtor.setDebtorNationality(loanForm.getDebtorNationality());
            debtor.setDebtorIsDebtor(true);
            debtor.setDebtorIsActive(true);
            debtor.setDebtorMap(loanForm.getDebtorMap());
            debtor.setDebtorImage(loanForm.getDebtorImage());
            debtor.setBranchId(loanForm.getBranchId());
            debtor.setUserId(userId);
            debtor.setActionTime(actionTime);
            session.saveOrUpdate(debtor);

            debtorId = debtor.getDebtorId();

            tx.commit();
            session.flush();
//            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtorId;
    }

    @Override
    public void updateDebtorDetails(DebtorHeaderDetails debHeadDetails) {
        Session session = savingsSessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            Calendar cal = Calendar.getInstance();
            Date action_time = cal.getTime();
            int userId = userService.findByUserName();
            debHeadDetails.setActionTime(action_time);
            debHeadDetails.setUserId(userId);
            debHeadDetails.setDebtorIsActive(true);
            debHeadDetails.setDebtorIsDebtor(true);
            session.saveOrUpdate(debHeadDetails);


            if(loanService.findByNicNo(debHeadDetails.getDebtorNic(), debHeadDetails.getBranchId())){
                Session sessionAxa = sessionFactory.openSession();
                Transaction txx = sessionAxa.beginTransaction();
                int debtorId = 0;
                try {
                    //user Id
                    int usrId = userService.findByUserName();

                    //current date
                    Calendar calendar = Calendar.getInstance();
                    Date actionTime = calendar.getTime();

                    //add debtor basic details
                    com.ites.bankfinance.model.DebtorHeaderDetails debtor = new com.ites.bankfinance.model.DebtorHeaderDetails();
                    if (debHeadDetails.getDebtorId() != null) {
                        debtor.setDebtorId(debHeadDetails.getDebtorId());
                    }
                    debtor.setDebtorName(debHeadDetails.getDebtorName());
                    debtor.setNameWithInitial(debHeadDetails.getDebtorNameWithIni());
                    debtor.setDebtorNic(debHeadDetails.getDebtorNic());
                    debtor.setDebtorDob(debHeadDetails.getDebtorDob());
                    debtor.setDebtorPersonalAddress(debHeadDetails.getDebtorPersonalAddress());
                    debtor.setDebtorFormalAddress(debHeadDetails.getDebtorFormalAddress());
                    debtor.setDebtorTelephoneHome(debHeadDetails.getDebtorTelephoneHome());
                    debtor.setDebtorTelephoneMobile(debHeadDetails.getDebtorTelephoneMobile());
                    debtor.setDebtorTelephoneWork(debHeadDetails.getDebtorTelephoneWork());
                    debtor.setDebtorFax(debHeadDetails.getDebtorFax());
                    debtor.setDebtorEmail(debHeadDetails.getDebtorEmail());
                    debtor.setDebtorAgOffice(debHeadDetails.getDebtorAgOffice());
                    debtor.setDebtorPostOffice(debHeadDetails.getDebtorPostOffice());
                    debtor.setDebtorPoliceStation(debHeadDetails.getDebtorPoliceStation());
                    debtor.setMemberNumber(debHeadDetails.getMemberNumber());
                    debtor.setJobInformation(debHeadDetails.getJobInformation());
                    debtor.setCenterDay(debHeadDetails.getCenterDay());
                    debtor.setBankBranch(debHeadDetails.getBankBranch());
                    debtor.setAccountName(debHeadDetails.getAccountName());
                    debtor.setAccountNo(debHeadDetails.getAccountNo());
                    debtor.setDebtorMap(debHeadDetails.getDebtorMap());
                    debtor.setDebtorImage(debHeadDetails.getDebtorImage());
                    debtor.setBranchId(debHeadDetails.getBranchId());
                    debtor.setUserId(usrId);
                    debtor.setActionTime(actionTime);
                    sessionAxa.saveOrUpdate(debtor);

                    txx.commit();
                    sessionAxa.flush();
                    sessionAxa.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
//        session.close();
    }

    @Override
    public List findByNic(String nic, Object bID) {
        List debtorDetails = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorNic like '%" + nic + "%' and debtorIsActive=1 and branchId = " + bID + "";
            debtorDetails = savingsSessionFactory.openSession().createQuery(sql).list();
            return debtorDetails;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List findByName(String name, Object bID) {
        List debtors = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorName like '%" + name + "%' and debtorIsActive=1 and branchId = " + bID + " ";
            debtors = savingsSessionFactory.openSession().createQuery(sql).list();
            return debtors;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public DebtorHeaderDetails findDebtorById(int Id){
        DebtorHeaderDetails debtors = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorId = " + Id + " ";

            debtors = (DebtorHeaderDetails) savingsSessionFactory.openSession().createQuery(sql).uniqueResult();
            return debtors;
        } catch (Exception e) {
            return null;
        }
    }

    public String generateDebtorAccount() {
        String AccountNo = "";
        try {
            String sql = "select max(debtorAccountNo) from DebtorHeaderDetails";
            AccountNo = "IFD";
            Object ob = savingsSessionFactory.openSession().createQuery(sql).uniqueResult();
            String maxNo = "0";
            if (ob != null) {
                maxNo = ob.toString().substring(3).replaceAll("^0+", "");
            }
            int mm = Integer.parseInt(maxNo) + 1;

            int noLength = 7 - String.valueOf(mm).length();
            for (int i = 0; i < noLength; i++) {
                AccountNo = AccountNo + "0";
            }

            if (noLength > 0) {
                AccountNo = AccountNo + mm;
            } else {
                AccountNo = AccountNo + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AccountNo;
    }
}

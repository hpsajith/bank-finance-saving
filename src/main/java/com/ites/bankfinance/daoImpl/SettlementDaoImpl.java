/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.BatchCustomer;
import com.bankfinance.form.BatchPosting;
import com.bankfinance.form.LoanPosting;
import com.bankfinance.form.OtherChargseModel;
import com.bankfinance.form.PaymentDetails;
import com.bankfinance.form.RebateForm;
import com.bankfinance.form.RebateQuotation;
import com.bankfinance.form.StlmntReturnList;
import com.ites.bankfinance.dao.DebtorDao;
import com.ites.bankfinance.dao.SettlementDao;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import com.ites.bankfinance.utils.FileZip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author ITESS
 */
@Repository
public class SettlementDaoImpl implements SettlementDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    private UserService userService;

    @Autowired
    private SettlmentService settlmentService;

    @Autowired
    private LoanDaoImpl loanDao;

    @Autowired
    private DayEndDaoImpl daoImpl;

    @Autowired
    private MasterDataService masterDataService;

    @Autowired
    private DebtorDao debtorDao;

    @Autowired
    private LoanService loanService;

    private final String refNoInstallmentCode = "INS";
    private final String installment = "Installment";
    private static final String RBT_CODE = "RBT";
    private final String TRANSFER_CODE = "TRANF";

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private DecimalFormat df = new DecimalFormat("##,###.00");

    @Override
    public DebtorHeaderDetails getDebtorDetails(Integer loanId) {
        DebtorHeaderDetails debtorHeaderDetails = null;

        try {
            String sql1 = " from LoanHeaderDetails where loanId=" + loanId + " ";
            LoanHeaderDetails loanHeaderDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            debtorHeaderDetails = loanHeaderDetails.getDebtorHeaderDetails();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtorHeaderDetails;
    }

    @Override
    public boolean addCashPaymentDetails(int userId, double payAmount, String transactioId) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        SettlementPaymentDetailCash settlementPaymentDetailCash = new SettlementPaymentDetailCash();

        settlementPaymentDetailCash.setAmount(payAmount);
        settlementPaymentDetailCash.setTransactionNo(transactioId);
        settlementPaymentDetailCash.setActionTime(new Date());
        settlementPaymentDetailCash.setUserId(userId);

        session.save(settlementPaymentDetailCash);

        tx.commit();
//        session.flush();
        session.close();
        return true;
    }

    @Override
    public String getTransactionIdMax(String code) {

        String maxtransactionNo = "0";
        String maxTransNoDelete = "0";
        String sql = "select transactionNo from SettlementPayments where transactionNo like '" + code + "%' order by transactionId desc";
        Object obj = sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();

        String sql1 = "select transactionNo from SettlementPaymentDelete where transactionNo like '" + code + "%' order by deleteId desc";
        Object obj1 = sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();

        int id = 0;
        if (obj != null) {
            maxtransactionNo = obj.toString();
            id = Integer.parseInt(maxtransactionNo.substring(code.length()));

            if (obj1 != null) {
                maxTransNoDelete = obj1.toString();

                int id2 = Integer.parseInt(maxTransNoDelete.substring(code.length()));
                if (id < id2) {
                    id = id2;
                }
            }
            id++;
            maxtransactionNo = code + id;
        } else {
            maxtransactionNo = code + maxtransactionNo;
        }
        return maxtransactionNo;
    }

    @Override
    public MBankDetails getBankDetails(int bankId) {

        Session session = sessionFactory.openSession();
        MBankDetails bank;
        try {
            String sql1 = " from MBankDetails where id ='" + bankId + "' ";
            bank = (MBankDetails) session.createQuery(sql1).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return bank;
    }

    @Override
    public boolean addChequePaymentDetails(SettlementPaymentDetailCheque formData) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean status = false;
        try {
            session.save(formData);
            status = true;
            tx.commit();
//            session.flush();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public SettlementPaymentDetailCheque getSettlementPaymentDetailChequeDetails(String voucherNo) {
        Session session=sessionFactory.openSession();
        SettlementPaymentDetailCheque sv=null;
        try {
            String sql = "from SettlementPaymentDetailCheque where transactionNo='" + voucherNo + "'";
            sv= (SettlementPaymentDetailCheque) session.createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return sv;

    }

    @Override
    public SettlementVoucher getSettlementVoucherDetailsbyLoanId(int loanId) {
        Session session=sessionFactory.openSession();
        SettlementVoucher sv=null;
        try {
            String sql = "from SettlementVoucher where loanId='" + loanId + "'";
             sv= (SettlementVoucher) session.createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return sv;
    }

    //    @Override
//    public List findAllCharges(int loanId){
//    List<SettlementCharges> allCharges;
//        try {
//            String sql = "from SettlementCharges where loanId="+loanId;
//            allCharges = (List<SettlementCharges>)sessionFactory.getCurrentSession().createQuery(sql).list();
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException(e);
//        }
//        return allCharges;
//    }
    @Override
    public List findPayTypes() {
        List<SettlementPaymentTypes> allCharges;
        try {
            String sql = "from SettlementPaymentTypes";
            allCharges = (List<SettlementPaymentTypes>) sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return allCharges;
    }

    @Override
    public List findBankList() {
        List<MBankDetails> mBankDetailList;
        try {
            String sql = "from MBankDetails";
            mBankDetailList = (List<MBankDetails>) sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return mBankDetailList;
    }

    @Override
    public List getPaidLoanInstallmnetList(int loanId) {

        String sql1 = "from SettlementMain where loanId='" + loanId + "' and  transactionCodeCrdt like '" + refNoInstallmentCode + "'";
        List<SettlementMain> loanInstallment = null;
        try {
            loanInstallment = sessionFactory.getCurrentSession().createQuery(sql1).list();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return loanInstallment;
    }

    @Override
    public boolean addSettlmentPayment(SettlementPayments settlementPayments) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(settlementPayments);
            tx.commit();
//            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean addBankDepositePaymentDetails(SettlementPaymentDetailBankDeposite formData) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean status = false;
        try {
            session.save(formData);
            status = true;
            tx.commit();
//            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public String getAccountNo(int paytype) {

        String accNo = null;
        try {
            String sql = "from SettlementPaymentTypes where paymentTypeId='" + paytype + "'";
            SettlementPaymentTypes obj = (SettlementPaymentTypes) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            accNo = obj.getAccNo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return accNo;
    }

    @Override
    public DebtorHeaderDetails findDebtorByLoanId(int loanId) {
        DebtorHeaderDetails obj = null;
        LoanHeaderDetails ob = null;
        try {
            String sql1 = "from LoanHeaderDetails where loanId='" + loanId + "'";
            ob = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                obj = ob.getDebtorHeaderDetails();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;

    }

    @Override
    public String getReceiptNo(String refCode) {

        String codeH = refCode;
        String maxRefNo = "00000000";
        List<SettlementReceiptDetail> obj = null;
        Object obj1;
        Integer refNo = null;
        try {
//            String sql1 ="from SettlementRefCodeConfig where typeNo='"+ refNo +"'";
//            SettlementRefCodeConfig ob = (SettlementRefCodeConfig) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();            
//            codeH = ob.getDescription();

            String sql1 = "select max(refNo) from SettlementReceiptDetail where rcptNo like '" + codeH + "%'";
            obj1 = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (obj1 != null) {
                refNo = Integer.parseInt(obj1.toString());
            }

            String sql2 = "from SettlementReceiptDetail where refNo = '" + refNo + "'";
            obj = (List<SettlementReceiptDetail>) sessionFactory.getCurrentSession().createQuery(sql2).list();

            if (obj != null) {
                for (int i = 0; i < obj.size(); i++) {
                    maxRefNo = obj.get(i).getRcptNo();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String number = "";
        int size = codeH.length();
        int maxNo = Integer.parseInt(maxRefNo.substring(size));
        maxNo++;
        codeH = codeH.trim();
        number = String.valueOf(maxNo);
        System.out.println("Code Creatorttt=" + number);
        if (number.length() == 1) {
            number = codeH + "000000" + number;
        } else if (number.length() == 2) {
            number = codeH + "00000" + number;
        } else if (number.length() == 3) {
            number = codeH + "0000" + number;
        } else if (number.length() == 4) {
            number = codeH + "000" + number;
        } else if (number.length() == 5) {
            number = codeH + "00" + number;
        } else if (number.length() == 6) {
            number = codeH + "0" + number;
        } else if (number.length() == 7) {
            number = codeH + number;
        } else if (number.length() > 7) {
            number = codeH + number;
        }
        return number;
    }

    @Override
    public boolean addSettlementReceiptDetail(SettlementReceiptDetail settlementReceiptDetail) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean status = false;
        try {
            if (settlementReceiptDetail != null) {
                session.save(settlementReceiptDetail);
                status = true;
                tx.commit();
//                session.flush();
                session.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public int getCashPaymentDetailsId() {
        int maxDetailId = 0;
        try {
            String sql0 = "select max(detailId) from SettlementPaymentDetailCash";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
            if (obj != null) {
                maxDetailId = Integer.parseInt(obj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxDetailId;
    }

    @Override
    public int getChequePaymentDetailsId() {
        int maxId = 0;
        try {
            String sql0 = "select max(chequeId) from SettlementPaymentDetailCheque";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
            if (obj != null) {
                maxId = Integer.parseInt(obj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxId;
    }

    @Override
    public int getBankDepositePaymentDetailsId() {
        int maxId = 0;
        try {
            String sql0 = "select max(bankDepositId) from SettlementPaymentDetailBankDeposite";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
            if (obj != null) {
                maxId = Integer.parseInt(obj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxId;
    }

    @Override
    public String getMaxSerialNo() {
        String codeH = "";
        int maxId = 0;
        try {
            String sql0 = "select max(serielNo) from SettlementReceiptDetail";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
            if (obj != null) {
                maxId = Integer.parseInt(obj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        maxId++;
        String number = "";
        int maxNo = maxId;
        number = String.valueOf(maxNo);
        System.out.println("Code Creatorttt=" + number);
        if (number.length() == 1) {
            number = codeH + "000000" + number;
        } else if (number.length() == 2) {
            number = codeH + "00000" + number;
        } else if (number.length() == 3) {
            number = codeH + "0000" + number;
        } else if (number.length() == 4) {
            number = codeH + "000" + number;
        } else if (number.length() == 5) {
            number = codeH + "00" + number;
        } else if (number.length() == 6) {
            number = codeH + "0" + number;
        } else if (number.length() == 7) {
            number = codeH + number;
        } else if (number.length() > 7) {
            number = codeH + number;
        }
        return number;
    }

    @Override
    public List<SettlementRefCodeConfig> getRefCode() {
        String sql1 = "from SettlementRefCodeConfig ";
        List<SettlementRefCodeConfig> ob = (List<SettlementRefCodeConfig>) sessionFactory.getCurrentSession().createQuery(sql1).list();
        return ob;
    }

    @Override
    public boolean addSettlementMain(SettlementMain settlementMain) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean status = false;
        try {
            session.save(settlementMain);
            status = true;
            tx.commit();
//            session.flush();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public boolean addSettlementMain2(SettlementMain settlementMain, String account) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean status = false;
        try {
            String code = "";
            String[] acc = account.split("-");
            int id = 0;
            String odiAcc = "";
            int loanId = settlementMain.getLoanId();
            String taxName = "";
            String sql4 = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql4).uniqueResult();
            int loanType = loan.getLoanType();
            String debtorAcc = loan.getDebtorHeaderDetails().getDebtorAccountNo();

            if (acc[1].equals("O")) {
                id = Integer.parseInt(acc[0]);
                String sqlO = "from MSubTaxCharges where subTaxId=" + id;
                MSubTaxCharges tax = (MSubTaxCharges) session.createQuery(sqlO).uniqueResult();
                if (tax != null) {
                    odiAcc = tax.getSubTaxAccountNo();
                    taxName = tax.getSubTaxDescription();
                }
            } else {
                id = Integer.parseInt(acc[0]);
                if (id == 1) {
                    code = "DWN";
                    taxName = "Down Payment";
                    odiAcc = loanDao.findLoanAccountNo(loanType, code);
                } else if (id == 2) {
                    code = "PAYBLE";
                    taxName = "Installment";
                    odiAcc = loanDao.findLoanAccountNo(loanType, code);
                } else if (id == 3) {
                    code = "INTRS";
                    taxName = "Interest";
                    odiAcc = loanDao.findLoanAccountNo(loanType, code);
                } else {
                    odiAcc = debtorAcc;
                    taxName = "Over Payment";
                }
            }
            settlementMain.setDescription("ADJ-" + taxName);
            session.save(settlementMain);

            double adjAmount = 0.00;
            int c = 0, d = 0;
            if (settlementMain.getDbtAmount() != null) {
                adjAmount = settlementMain.getDbtAmount();
                c = 2;
                d = 1;
            } else {
                adjAmount = settlementMain.getCrdtAmount();
                c = 1;
                d = 2;
            }

            int settId = settlementMain.getSettlementId();
            int branchId = settlementMain.getBranchId();

            String postingRefNo = loanDao.generateReferencNo(loanId, branchId);
            postingRefNo = postingRefNo + "/" + settId;

            if (!odiAcc.equals("") && !debtorAcc.equals("")) {
                loanDao.addToPosting(odiAcc, postingRefNo, new BigDecimal(adjAmount), branchId, d);
                loanDao.addToPosting(debtorAcc, postingRefNo, new BigDecimal(adjAmount), branchId, c);
                status = true;
            } else {
//              String loanTypeName = loanDao.findLoanType(loanType);
//              System.out.println("Create Account for ADJUSTMENT - " + loanTypeName);
                status = false;

            }

            status = true;
            tx.commit();
//            session.flush();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public List<ReturnList> findOpenningBalance(int loanId) {
        //SELECT * FROM open_d,open_arreas_type,open_h WHERE open_h.Open_Id=open_d.Open_Id AND open_arreas_type.Type_Id=open_d.Arreas_Type_Id AND Loan_Id =24    
        OpenH ob = null;
        List<ReturnList> OpenArreasDetail = null;
        String sql1 = "from OpenH where  loanId='" + loanId + "'";
        try {
            ob = (OpenH) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            // ReturnList(int arreasTypeId, Double amount, String arreasDescription)
//        String sql2 ="from OpenD where  openId='"+ob.getOpenId()+"'";
            if (ob != null) {
                String sql2 = "select new com.ites.bankfinance.model.ReturnList(OD.arreasTypeId,OD.amount,OAT.arreasDescription) from OpenD as OD ,OpenArreasType as OAT where OD.arreasTypeId=OAT.typeId  and OD.openId='" + ob.getOpenId() + "'";
                OpenArreasDetail = sessionFactory.getCurrentSession().createQuery(sql2).list();
                ;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return OpenArreasDetail;
    }

    @Override
    public double getOpenningBalanceWithoputODI(int loanId) {
        String sql1 = "select arreasWithoutOdi from OpenH where loanId='" + loanId + "' ";
        Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
        double asdf = 0;
        if (ob != null) {
            asdf = Double.parseDouble(ob.toString());
        }
        return asdf;
    }

    @Override
    public double findTotalCharges(int loanId) {
        String sql1 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' ";
        double dbtAmount = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                dbtAmount = Double.parseDouble(ob.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbtAmount;
    }

    @Override
    public double findTotalPayment(int loanId) {
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' ";
        double crdtAmount = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                crdtAmount = Double.parseDouble(ob.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return crdtAmount;
    }

    @Override
    public double getTotalDownPayment(int loanId) {

//        String refCodeDownPayment = "DWNS";
//         String sql1 ="select sum(dbtAmount) from SettlementMain where loanId='"+loanId+"' and transactionCodeDbt like '"+refCodeDownPayment+"%' ";
//    double debtAmount = 0;    
//        try{
//            String ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult().toString();             
//            debtAmount = Double.parseDouble(ob);
//        }catch(NullPointerException e){
//            e.printStackTrace();
//        }   
        double debtAmount = 0;
        String sql1 = "select loanDownPayment from LoanHeaderDetails where loanId='" + loanId + "'";
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                debtAmount = Double.parseDouble(ob.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtAmount;
    }

    @Override
    public double getDownPaymentBalance(int loanId) {
        String refCodeDownPayment = "DWNS";
        String sql1 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeDownPayment + "' ";
        String sql2 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeDownPayment + "' ";
        double debtAmount = 0;
        double credtAmount = 0;
        double balance = 0;
        try {
            Object ob1 = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob1 != null) {
                debtAmount = Double.parseDouble(ob1.toString());
            }
            Object ob2 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob2 != null) {
                credtAmount = Double.parseDouble(ob2.toString());
            }
            balance = debtAmount - credtAmount;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return balance;
    }

    @Override
    public double getPaidArrears(int loanId) {
        double paidArrears = 0;
        String refCodeArrearPayment = "ARRS";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeArrearPayment + "' ";
        double credtAmount = 0;
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeArrearPayment + "' ";
        double debtAmount = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                credtAmount = Double.parseDouble(ob.toString());
            }
            Object ob1 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob1 != null) {
                debtAmount = Double.parseDouble(ob1.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        paidArrears = credtAmount - debtAmount;
        return paidArrears;
    }

    @Override
    public double getPaidOtherCharges(int loanId) {
        String refCodeOtherCharge = "OTHR";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeOtherCharge + "' ";
        double credtAmount = 0;
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeOtherCharge + "' ";
        double debtAmount = 0;
        double paidOtherCharge = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                credtAmount = Double.parseDouble(ob.toString());
            }
            Object ob1 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob1 != null) {
                debtAmount = Double.parseDouble(ob1.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        paidOtherCharge = credtAmount - debtAmount;
        return paidOtherCharge;
    }

    @Override
    public double getPaidDownPayment(int loanId) {
        String refCodeDownPayment = "DWNS";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeDownPayment + "' ";
        double credtAmount = 0;
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeDownPayment + "' ";
        double debtAmount = 0;
        double paidDownPayment = 0;

        String sql3 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt = 'DWN' ";
        double debtAmount2 = 0;
        double initialDownPaymentCharge = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                credtAmount = Double.parseDouble(ob.toString());
            }

            Object ob3 = sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
            if (ob3 != null) {
                initialDownPaymentCharge = Double.parseDouble(ob3.toString());
            }
            if (initialDownPaymentCharge == credtAmount) {
                paidDownPayment = credtAmount;

            }

            Object ob1 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob1 != null) {
                debtAmount = Double.parseDouble(ob1.toString());
            }
            if (debtAmount < initialDownPaymentCharge) {
                paidDownPayment = credtAmount;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return paidDownPayment;
    }

    @Override
    public double getPaidInsuarance(int loanId) {
        String refCodeInsuarance = "INSU";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeInsuarance + "' ";
        double credtAmount = 0;
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeInsuarance + "' ";
        double debtAmount = 0;
        double paidInsuarance = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                credtAmount = Double.parseDouble(ob.toString());
            }
            Object ob1 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob1 != null) {
                debtAmount = Double.parseDouble(ob1.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        paidInsuarance = credtAmount - debtAmount;
        return paidInsuarance;
    }

    @Override
    public double getTotalInsuarance(int loanId) {
        String refCodeInsuarance = "INSU";

        String sql1 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeInsuarance + "' ";
        double debtAmount = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                debtAmount = Double.parseDouble(ob.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtAmount;
    }

    @Override
    public List getDebitInstallmnetList(int loanId) {
        String refCodeInstalment = "INS";
        String sql1 = "from SettlementMain where loanId='" + loanId + "' and  transactionCodeDbt like '" + refCodeInstalment + "'";
        List<SettlementMain> loanInstallment = null;
        try {
            loanInstallment = sessionFactory.getCurrentSession().createQuery(sql1).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanInstallment;
    }

    @Override
    public List<InstallmentReturnList> getDebitCreditInstallmentList(int loanId) {
        String refCodeInstalment = "INS";
        String refCodeODI = "ODI";
        String sql1 = "from SettlementMain where loanId='" + loanId + "' and ( transactionCodeDbt like '" + refCodeInstalment + "' or transactionCodeDbt like '" + refCodeODI + "' ) ";
        String sql2 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeInstalment + "'";
        String sql3 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeODI + "'";

        List<InstallmentReturnList> loanInstallment = new ArrayList();

        List<SettlementMain> debitInstallmentList = null;

        String str = "0";
        String str2 = "0";

        try {
            debitInstallmentList = sessionFactory.getCurrentSession().createQuery(sql1).list();
            Object ob = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob != null) {
                str = ob.toString();
            }
            Object ob2 = sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
            if (ob2 != null) {
                str2 = ob2.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//        double creditSum = Double.parseDouble(str) - Double.parseDouble(str2);
        double creditSumIns = Double.parseDouble(str);
        double creditSumOdi = Double.parseDouble(str2);

        for (int i = 0; i < debitInstallmentList.size(); i++) {
            InstallmentReturnList installmentReturnList = new InstallmentReturnList();
            double debitAmount = debitInstallmentList.get(i).getDbtAmount();
            String code = debitInstallmentList.get(i).getTransactionCodeDbt();
            if (code.equals(refCodeInstalment)) {
                if (debitAmount <= creditSumIns && debitAmount > 0) {
                    double paidAmount = debitAmount;
                    installmentReturnList.setCharge(debitAmount);
                    installmentReturnList.setPaid(paidAmount);
                    installmentReturnList.setBalance(new BigDecimal(debitAmount - paidAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    installmentReturnList.setDue(debitInstallmentList.get(i).getDueDate());
                    installmentReturnList.setDescription(debitInstallmentList.get(i).getDescription());
                    creditSumIns = new BigDecimal(creditSumIns - paidAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                } else if (debitAmount < 0) {
                    installmentReturnList.setCharge(debitAmount);
                    installmentReturnList.setPaid(0);
                    installmentReturnList.setBalance(new BigDecimal(debitAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    installmentReturnList.setDue(debitInstallmentList.get(i).getDueDate());
                    installmentReturnList.setDescription(debitInstallmentList.get(i).getDescription());
                } else {
                    double paidAmount = creditSumIns;
                    installmentReturnList.setCharge(debitAmount);
                    installmentReturnList.setPaid(paidAmount);
                    installmentReturnList.setBalance(new BigDecimal(debitAmount - paidAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    installmentReturnList.setDue(debitInstallmentList.get(i).getDueDate());
                    installmentReturnList.setDescription(debitInstallmentList.get(i).getDescription());

                    creditSumIns = 0;
                }
            } else if (code.equals(refCodeODI)) {
                if (debitAmount <= creditSumOdi && debitAmount > 0) {
                    double paidAmount = debitAmount;
                    installmentReturnList.setCharge(debitAmount);
                    installmentReturnList.setPaid(paidAmount);
                    installmentReturnList.setBalance(new BigDecimal(debitAmount - paidAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    installmentReturnList.setDue(debitInstallmentList.get(i).getDueDate());
                    installmentReturnList.setDescription(debitInstallmentList.get(i).getDescription());
                    creditSumOdi = new BigDecimal(creditSumOdi - paidAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                } else if (debitAmount < 0) {
                    installmentReturnList.setCharge(debitAmount);
                    installmentReturnList.setPaid(0);
                    installmentReturnList.setBalance(new BigDecimal(debitAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    installmentReturnList.setDue(debitInstallmentList.get(i).getDueDate());
                    installmentReturnList.setDescription(debitInstallmentList.get(i).getDescription());
                } else {
                    double paidAmount = creditSumOdi;
                    installmentReturnList.setCharge(debitAmount);
                    installmentReturnList.setPaid(paidAmount);
                    installmentReturnList.setBalance(new BigDecimal(debitAmount - paidAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    installmentReturnList.setDue(debitInstallmentList.get(i).getDueDate());
                    installmentReturnList.setDescription(debitInstallmentList.get(i).getDescription());

                    creditSumOdi = 0;
                }
            }

            loanInstallment.add(installmentReturnList);
        }

        return loanInstallment;
    }

    @Override
    public double getOverPayAmount(int loanId) {
        String refCodeOverPay = "OVP";
//        String sql1 = "select crdtAmount from SettlementMain where loanId='" + loanId + "' and  transactionCodeCrdt = '" + refCodeOverPay + "' order by settlementId desc ";
//        Object loanOverPayAmount = "0";
//        double overPayAmount = 0;
//        try {
//            loanOverPayAmount = sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();
//            if (loanOverPayAmount != null) {
//                overPayAmount = Double.parseDouble(loanOverPayAmount.toString());
//            }
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }

        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeOverPay + "' ";
        double credtAmount = 0;
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeOverPay + "' ";
        double debtAmount = 0;
        double overPayAmount = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                credtAmount = Double.parseDouble(ob.toString());
            }
            Object ob1 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob1 != null) {
                debtAmount = Double.parseDouble(ob1.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        overPayAmount = credtAmount - debtAmount;
        return overPayAmount;
    }

    @Override
    public void addOverPayAmount(int loanId, double totalPayment, int userid, int debtorId, String newTransactionNo, String code, String receiptNo, String codeDescrpt, int branchId, Date systemDate) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        SettlementMain settlementMain = null;
//        String sql1 = "from SettlementMain where loanId='" + loanId + "' and  transactionCodeCrdt = '" + refCodeOverPay + "'";
        try {
//            Object obj = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
//            if(obj!=null){
//                settlementMain=(SettlementMain)obj;
//            }else{
            settlementMain = new SettlementMain();
//            }
            settlementMain.setCrdtAmount(totalPayment);
            settlementMain.setDebtorId(debtorId);
            settlementMain.setReceiptNo(receiptNo);
            settlementMain.setTransactionCodeCrdt(code);
            settlementMain.setTransactionNoCrdt(newTransactionNo);
            settlementMain.setUserId(userid);
            settlementMain.setLoanId(loanId);
            settlementMain.setDescription(codeDescrpt);
            settlementMain.setIsPosting(false);
            settlementMain.setBranchId(branchId);
            settlementMain.setSystemDate(systemDate);
            session.save(settlementMain);

            tx.commit();
//            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean updateLoanHeaderIsDownPaymntPaid(int loanId, double initialDownPayment) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        boolean status = false;
        String refCodeDownPaymentSus = "DWNS";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and  transactionCodeCrdt = '" + refCodeDownPaymentSus + "'";

        String sql2 = "from LoanHeaderDetails where loanId='" + loanId + "' ";
        try {
            double paidIntialDown = 0;
            Object creditSum = session.createQuery(sql1).uniqueResult();
            if (creditSum != null) {
                paidIntialDown = Double.parseDouble(creditSum.toString());
            }
            if (initialDownPayment <= paidIntialDown) {
                LoanHeaderDetails loanHeaderDetails = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();
                if (loanHeaderDetails != null) {
                    loanHeaderDetails.setLoanIsPaidDown(1);
                    session.update(loanHeaderDetails);
                    tx.commit();
                    status = true;
                }
            }
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return status;
    }

    public LoanInstallment findLoanInstallment(int loanId) {
        String sql1 = "from LoanInstallment where loanId='" + loanId + "' ";
        List<LoanInstallment> loanInstallment = null;
        LoanInstallment Installment = null;
        try {
            loanInstallment = (List<LoanInstallment>) sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (loanInstallment.size() > 0) {
                Installment = loanInstallment.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Installment;

    }

    @Override
    public boolean addChargeDetails(SettlementCharges settlementCharges) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        boolean status = false;
        try {
            if (settlementCharges != null) {
                session.save(settlementCharges);
                //            sessionFactory.getCurrentSession().save(settlementCharges);
                status = true;
                tx.commit();
//                session.flush();
                session.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public boolean updateInsuaranceStatus(int loanId, int status) {
        Session session = null;
        Transaction tx = null;

        String sql1 = "from LoanHeaderDetails where loanId='" + loanId + "' ";
        LoanHeaderDetails loanHeaderDetails = null;
        boolean st = false;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            loanHeaderDetails = (LoanHeaderDetails) session.createQuery(sql1).uniqueResult();
            if (loanHeaderDetails != null) {
                loanHeaderDetails.setInsuranceStatus(status);
                session.update(loanHeaderDetails);
                tx.commit();
                st = true;
            }
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return st;
    }

    @Override
    public List<SettlementMain> getInstalmntWithODI(int loanId) {
        String refCodeInstalment = "INS";
        String refCodeODI = "ODI";
        String sql1 = "from SettlementMain where loanId='" + loanId + "' and ( transactionCodeDbt like '" + refCodeInstalment + "' "
                + " or transactionCodeCrdt like '" + refCodeInstalment + "' or transactionCodeDbt like '" + refCodeODI + "' or transactionCodeCrdt like '" + refCodeODI + "') ";
        List<SettlementMain> loanInstallment = null;
        try {
            loanInstallment = sessionFactory.getCurrentSession().createQuery(sql1).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanInstallment;
    }

    @Override
    public int getUserLogedBranch(int userId) {
        Session session=sessionFactory.openSession();
        String sql1 = "from UmUserLog where userId='" + userId + "' order by pk desc ";
        UmUserLog obj;
        int branchId = 0;
        try {
            obj = (UmUserLog) session.createQuery(sql1).setMaxResults(1).uniqueResult();
//            obj = (UmUserLog) sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();/Annr
            if (obj != null) {
                branchId = obj.getBranchId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return branchId;
    }

    @Override
    public Date getSystemDate(int branchId) {
        Session session=sessionFactory.openSession();
        String sql1 = "from DayEndDate where branchId = '" + branchId + "' order by dayEndId desc ";
        DayEndDate obj;
        Date date = null;
        try {
            obj = (DayEndDate) session.createQuery(sql1).setMaxResults(1).uniqueResult();
//            obj = (DayEndDate) sessionFactory.getCurrentSession().createQuery(sql1).setMaxResults(1).uniqueResult();
            if (obj != null) {
                date = obj.getDayEndDate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return date;
    }

    @Override
    public List<StlmntReturnList> getPaymentList(Date systemDate, int userBranchId) {
        List<SettlementPayments> obj;
        List<StlmntReturnList> returnList = new ArrayList();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String sdate = sdf.format(systemDate);
        try {
            String sql1 = " from SettlementPayments where systemDate = '" + systemDate + "' and branchId = '" + userBranchId + "' ";
            obj = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql1).list();

            for (SettlementPayments ob : obj) {
                StlmntReturnList stlnmt = new StlmntReturnList();
                stlnmt.setAccountNo(ob.getAccountNo());
                String sql2 = " from LoanHeaderDetails where loanId = '" + ob.getLoanId() + "'";
                LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                stlnmt.setAgreementNo(loan.getLoanAgreementNo());
                stlnmt.setDebtAccountNo(loan.getDebtorHeaderDetails().getDebtorAccountNo());
                stlnmt.setAmountText(ob.getAmountText());
                stlnmt.setBranchId(ob.getBranchId());
                stlnmt.setActionDate(ob.getActionDate());
                stlnmt.setChequeId(ob.getChequeId());
                stlnmt.setLoanId(ob.getLoanId());
                String sql3 = " from SettlementPaymentTypes where paymentTypeId = '" + ob.getPaymentType() + "'";
                SettlementPaymentTypes peyType = (SettlementPaymentTypes) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                stlnmt.setPayType(peyType.getPaymentType());
                stlnmt.setPaymentDetails(ob.getPaymentDetails());
                stlnmt.setReceiptNo(ob.getReceiptNo());
                stlnmt.setRemark(ob.getRemark());
                stlnmt.setSystemDate(ob.getSystemDate());
                stlnmt.setTransactionId(ob.getTransactionId());
                stlnmt.setTransactionNo(ob.getTransactionNo());
                stlnmt.setUserId(ob.getUserId());
                stlnmt.setPaymentAmount(ob.getPaymentAmount());
                returnList.add(stlnmt);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return returnList;
    }

    @Override
    public SettlementPayments getPaymentDetail(int transactionId) {
        SettlementPayments obj;
        try {
            String sql1 = " from SettlementPayments where transactionId = '" + transactionId + "' ";
            obj = (SettlementPayments) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return obj;
    }

    @Override
    public boolean addSettlementPaymentDelete(SettlementPayments settlementPayments, SettlementPaymentDeleteForm sttlmntDeleteDeleteForm) {
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean status = false;

        SettlementPaymentDelete settlementPaymentDelete = new SettlementPaymentDelete();
        if (settlementPayments != null) {
            settlementPaymentDelete.setPaymentAmount(settlementPayments.getPaymentAmount());
            settlementPaymentDelete.setTransactionNo(settlementPayments.getTransactionNo());
            settlementPaymentDelete.setAmountText(settlementPayments.getAmountText());
            settlementPaymentDelete.setDebtorId(settlementPayments.getDebtorId());
            settlementPaymentDelete.setReceiptNo(settlementPayments.getReceiptNo());
            settlementPaymentDelete.setPaymentType(settlementPayments.getPaymentType());
            settlementPaymentDelete.setPaymentDetailId(settlementPayments.getPaymentDetails());
            settlementPaymentDelete.setIsDelete(true);
            settlementPaymentDelete.setSystemDate(dt);
            settlementPaymentDelete.setLoanId(settlementPayments.getLoanId());
            settlementPaymentDelete.setPaymentDate(settlementPayments.getPaymentDate());
            settlementPaymentDelete.setRemark(sttlmntDeleteDeleteForm.getReason());
            settlementPaymentDelete.setUserId(userId);
            settlementPaymentDelete.setBranchId(userLogBranchId);
            session.save(settlementPaymentDelete);

            status = true;
        }
        tx.commit();
//        session.flush();
        session.close();
        return status;
    }

    @Override
    public boolean removeStlMainChargePayment(String receiptNo) {

        boolean status = false;
        List<SettlementMain> list1;
        List<SettlementCharges> list2;
        List<SettlementPayments> list3;
        try {
            String sql1 = " from SettlementMain where receiptNo = '" + receiptNo + "' ";
            String sql2 = " from SettlementCharges where billNo = '" + receiptNo + "' ";
            String sql3 = " from SettlementPayments where receiptNo = '" + receiptNo + "' ";
            list1 = (List<SettlementMain>) sessionFactory.getCurrentSession().createQuery(sql1).list();
            list2 = (List<SettlementCharges>) sessionFactory.getCurrentSession().createQuery(sql2).list();
            list3 = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql3).list();
            if (list1 != null) {
                for (SettlementMain obj : list1) {
                    if (obj.getTransactionCodeCrdt().equals(refNoInstallmentCode)) {
                        removeInstalmentPayStatus(obj.getLoanId(), obj.getCrdtAmount());
                    }
                    sessionFactory.getCurrentSession().delete(obj);
                    status = true;
                }
            }
            if (list2 != null) {
                for (SettlementCharges obj : list2) {
                    sessionFactory.getCurrentSession().delete(obj);
                    status = true;
                }
            }
            if (list3 != null) {
                for (SettlementPayments obj : list3) {
                    sessionFactory.getCurrentSession().delete(obj);
                    status = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return status;
    }

    @Override
    public double getInstalmentBalance(int loanId) {
        String refCodeInstalment = "INS";
        String refCodeODI = "ODI";

        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and ( transactionCodeCrdt like '" + refCodeInstalment + "' or transactionCodeCrdt like '" + refCodeODI + "' ) ";
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and ( transactionCodeDbt like '" + refCodeInstalment + "' or transactionCodeDbt like '" + refCodeODI + "' ) ";

        double creditSum = 0;
        double debitSum = 0;
        double balance = 0;
        Object ob1 = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
        if (ob1 != null) {
            creditSum = Double.parseDouble(ob1.toString());
        }
        Object ob2 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
        if (ob2 != null) {
            debitSum = Double.parseDouble(ob2.toString());
        }
        balance = debitSum - creditSum;
        return balance;
    }

    @Override
    public void addOverPayDebit(int loanId, double overPayAmount, int userid, Integer debtorId, String newTransactionNo, String refNoOverPayCode, String receiptNo, String refNoOverPayCodeDescrpt, int userLogBranchId, Date systemDate) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        SettlementMain settlementMain = null;
//        String sql1 = "from SettlementMain where loanId='" + loanId + "' and  transactionCodeCrdt = '" + refCodeOverPay + "'";
        try {
//            Object obj = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
//            if(obj!=null){
//                settlementMain=(SettlementMain)obj;
//            }else{
            settlementMain = new SettlementMain();
//            }
            settlementMain.setDbtAmount(overPayAmount);
            settlementMain.setDebtorId(debtorId);
            settlementMain.setReceiptNo(receiptNo);
            settlementMain.setTransactionCodeDbt(refNoOverPayCode);
            settlementMain.setTransactionNoDbt(newTransactionNo);
            settlementMain.setUserId(userid);
            settlementMain.setLoanId(loanId);
            settlementMain.setDescription(refNoOverPayCodeDescrpt);
            settlementMain.setIsPosting(false);
            settlementMain.setBranchId(userLogBranchId);
            settlementMain.setSystemDate(systemDate);
            session.save(settlementMain);

            tx.commit();
//            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List findOtherChargesByLoanId(int loanId) {
        List charges = null;
        List<OtherChargseModel> chargesList = new ArrayList();
        try {
            String sql = "select l.chargeRate,l.chargeAmount,m.subTaxDescription,l.chargesId,l.isDownPaymentCharge,l.isPaid,l.chargeId  From LoanOtherCharges as l, MSubTaxCharges as m where l.loanId='" + loanId + "' and l.chargesId=m.subTaxId";
            charges = sessionFactory.getCurrentSession().createQuery(sql).list();

            if (charges.size() > 0) {
                for (int i = 0; i < charges.size(); i++) {
                    Object[] obj = (Object[]) charges.get(i);
                    OtherChargseModel other = new OtherChargseModel();
                    String description = "";
                    String rate = "";
                    Integer isDownPaymentCharge = 0;
                    Integer isPaid = 0;
                    Integer chargeID = 0;
                    double amount = 0.00;
                    int id = 0;

                    if (obj[0] != null) {
                        rate = obj[0].toString();
                    }
                    if (obj[1] != null) {
                        amount = Double.parseDouble(obj[1].toString());
                    }
                    if (obj[2] != null) {
                        description = obj[2].toString();
                    }
                    if (obj[3] != null) {
                        id = Integer.parseInt(obj[3].toString());
                    }
                    if (obj[4] != null) {
                        isDownPaymentCharge = Integer.parseInt(obj[4].toString());
                    }
                    if (obj[5] != null) {
                        isPaid = Integer.parseInt(obj[5].toString());
                    }
                    if (obj[6] != null) {
                        chargeID = Integer.parseInt(obj[6].toString());
                    }

                    other.setId(id);
                    other.setRate(rate);
                    other.setAmount(amount);
                    other.setDescription(description);
                    other.setIsDownPaymentCharge(isDownPaymentCharge);
                    other.setIsPaid(isPaid);
                    other.setChargeID(chargeID);
                    chargesList.add(other);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return chargesList;
    }

    @Override
    public boolean updatePaidStatus(Integer chargeID) {
        boolean status = false;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        String sql1 = "from LoanOtherCharges where chargeId='" + chargeID + "'";
        LoanOtherCharges charges = (LoanOtherCharges) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
        try {
            charges.setIsPaid(1);
            session.update(charges);
            status = true;
            tx.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public String getAdjustTransCode(String refNoAdjustment) {
        String maxtransactionNo = "0";
        String maxTransNoDelete = "0";
        String sql = "select transactionNoDbt from SettlementMain where transactionCodeDbt like '" + refNoAdjustment + "%' order by settlementId desc";
        Object obj = sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();

        int id = 0;
        if (obj != null) {
            maxtransactionNo = obj.toString();
            String id2 = maxtransactionNo.replaceAll("[^0-9.]", "");
//            id = Integer.parseInt(maxtransactionNo.substring(refNoAdjustment.length()));
            if (id2 != "") {
                id = Integer.parseInt(id2);
            }
            id++;
            maxtransactionNo = refNoAdjustment + id;
        } else {
            maxtransactionNo = refNoAdjustment + maxtransactionNo;
        }
        return maxtransactionNo;
    }

    @Override
    public double getOdiBalance(int loanId) {
        String refCodeODI = "ODI";

        String sql1 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeODI + "' ";
        double debtAmount = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                debtAmount = Double.parseDouble(ob.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtAmount;

    }

    @Override
    public double getPaidOdiBalance(int loanId) {
        String refCodeODI = "ODI";
        double creditAmount = 0.00;
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeODI + "' ";
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                creditAmount = Double.parseDouble(ob.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return creditAmount;
    }

    @Override
    public UmUser checkUserPrivileges(String uname, String pword) {
        UmUser user = null;
        String sql3 = "From Employee where userName='" + uname + "'";
        Employee emp = (Employee) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();

        int uId = 0;
        if (emp != null) {
            String sql4 = "From UmUser where empId=" + emp.getEmpNo();
            user = (UmUser) sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();

        }

        return user;
    }

    @Override
    public double[] getTotalBalancesForDayEnd(Date systemDate, int loginBranchID) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(systemDate);
        cal.add(Calendar.DATE, -1);
        Date prevDate = cal.getTime();
        double[] totalAmounts = new double[13];
        double t_payments = 0.00;
        double t_other = 0.00;
        double t_voucher = 0.00;
        double t_cheqIn = 0.00;
        double t_cashIn = 0.00;
        double t_bank = 0.00;
        double t_open = 0.00;
        double t_prevDayCashIn = 0.00;
        double t_prevDayChequeIn = 0.00;
        double t_dirctFundCash = 0.00;
        double t_dirctFundCheq = 0.00;
        double t_mdFundCash = 0.00;
        double t_mdFundCheq = 0.00;
        try {
            String sql1 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + loginBranchID + " "
                    + "AND systemDate='" + systemDate + "'";
            Object totalPayments = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (totalPayments != null) {
                t_payments = Double.parseDouble(totalPayments.toString());
            }

            String sql2 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE systemDate='" + systemDate + "' AND branchId=" + loginBranchID;
            Object totalOther = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (totalOther != null) {
                t_other = Double.parseDouble(totalOther.toString());
            }

            String sql3 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId=" + loginBranchID + " AND voucherDate='" + systemDate + "' and voucherCatogery<>1";
            Object totalVoucher = sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
            if (totalVoucher != null) {
                t_voucher = Double.parseDouble(totalVoucher.toString());
            }

            String sql4 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + loginBranchID + " "
                    + "AND systemDate='" + systemDate + "' AND paymentType=2";
            Object loanCheq = sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
            if (loanCheq != null) {
                t_cheqIn = Double.parseDouble(loanCheq.toString());
            }

            String sql5 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE branchId=" + loginBranchID + " "
                    + "AND systemDate='" + systemDate + "' AND paymentType=2";
            Object otherCheq = sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();
            if (otherCheq != null) {
                t_cheqIn = t_cheqIn + Double.parseDouble(otherCheq.toString());
            }

            String sql6 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId=" + loginBranchID + " AND voucherDate='" + systemDate + "'"
                    + " AND voucherCatogery=9";
            Object cheqOut = sessionFactory.getCurrentSession().createQuery(sql6).uniqueResult();
            if (cheqOut != null) {
                t_cheqIn = t_cheqIn - Double.parseDouble(cheqOut.toString());
            }

            String sql7 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + loginBranchID + " "
                    + "AND systemDate='" + systemDate + "' AND paymentType=3";
            Object totalBnk = sessionFactory.getCurrentSession().createQuery(sql7).uniqueResult();
            if (totalBnk != null) {
                t_bank = Double.parseDouble(totalBnk.toString());
            }

            String sql8 = "SELECT cashBalance FROM DayEndFloatBalance WHERE branchId=" + loginBranchID + " "
                    + "AND dayEndDate='" + dateFormat.format(prevDate) + "'";
            Object prevDayLoanCash = sessionFactory.getCurrentSession().createQuery(sql8).uniqueResult();
            if (prevDayLoanCash != null) {
                t_prevDayCashIn = Double.parseDouble(prevDayLoanCash.toString());
            }

//            String sql9 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE paymentType = 1 AND branchId =" + loginBranchID
//                    + " AND systemDate ='" + dateFormat.format(prevDate) + "'";
//            Object prevDayOtherCash = sessionFactory.getCurrentSession().createQuery(sql9).uniqueResult();
//            if (prevDayOtherCash != null) {
//                t_prevDayCashIn = t_prevDayCashIn + Double.parseDouble(prevDayOtherCash.toString());
//            }
//
//            String sql10 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId =" + loginBranchID + " AND"
//                    + " voucherDate='" + dateFormat.format(prevDate) + "' AND voucherCatogery NOT IN (9) AND loanId=0 AND voucherNo LIKE 'O%'";
//            Object prevDayCashOut = sessionFactory.getCurrentSession().createQuery(sql10).uniqueResult();
//            if (prevDayCashOut != null) {
//                t_prevDayCashIn = t_prevDayCashIn - Double.parseDouble(prevDayCashOut.toString());
//            }
            String sql11 = "SELECT chequeBalance FROM DayEndFloatBalance WHERE branchId=" + loginBranchID + " "
                    + "AND dayEndDate='" + dateFormat.format(prevDate) + "'";
            Object prevDayLoanCheq = sessionFactory.getCurrentSession().createQuery(sql11).uniqueResult();
            if (prevDayLoanCheq != null) {
                t_prevDayChequeIn = Double.parseDouble(prevDayLoanCheq.toString());
            }

//            String sql12 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE branchId=" + loginBranchID + " "
//                    + "AND systemDate='" + dateFormat.format(prevDate) + "' AND paymentType=2";
//            Object prevDayOtherCheq = sessionFactory.getCurrentSession().createQuery(sql12).uniqueResult();
//            if (prevDayOtherCheq != null) {
//                t_prevDayChequeIn = t_prevDayChequeIn + Double.parseDouble(prevDayOtherCheq.toString());
//            }
//
//            String sql13 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId=" + loginBranchID + " AND voucherDate='" + dateFormat.format(prevDate) + "'"
//                    + " AND voucherCatogery=9";
//            Object prevDayCheqOut = sessionFactory.getCurrentSession().createQuery(sql13).uniqueResult();
//            if (prevDayCheqOut != null) {
//                t_prevDayChequeIn = t_prevDayChequeIn - Double.parseDouble(prevDayCheqOut.toString());
//            }
            String sql14 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + loginBranchID + " "
                    + "AND systemDate='" + systemDate + "' AND paymentType=1";
            Object loanCash = sessionFactory.getCurrentSession().createQuery(sql14).uniqueResult();
            if (loanCash != null) {
                t_cashIn = Double.parseDouble(loanCash.toString());
            }

            String sql15 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE paymentType = 1 AND branchId =" + loginBranchID
                    + " AND systemDate ='" + systemDate + "'";
            Object otherCash = sessionFactory.getCurrentSession().createQuery(sql15).uniqueResult();
            if (otherCash != null) {
                t_cashIn = t_cashIn + Double.parseDouble(otherCash.toString());
            }

            String sql16 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId =" + loginBranchID + " AND"
                    + " voucherDate='" + systemDate + "' AND voucherCatogery NOT IN (9) AND loanId=0 AND voucherNo LIKE 'O%'";
            Object cashOut = sessionFactory.getCurrentSession().createQuery(sql16).uniqueResult();
            if (cashOut != null) {
                t_cashIn = t_cashIn - Double.parseDouble(cashOut.toString());
            }

            String sql17 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE systemDate = '" + systemDate + "'"
                    + " AND branchId = " + loginBranchID + " AND PaymentType = 1 AND AccountNo = '500075'";
            Object dirctFundCash = sessionFactory.getCurrentSession().createQuery(sql17).uniqueResult();
            if (dirctFundCash != null) {
                t_dirctFundCash = Double.parseDouble(dirctFundCash.toString());
            }

            String sql18 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE systemDate = '" + systemDate + "'"
                    + " AND branchId = " + loginBranchID + " AND PaymentType = 2 AND AccountNo = '500075'";
            Object dirctFundCheq = sessionFactory.getCurrentSession().createQuery(sql18).uniqueResult();
            if (dirctFundCheq != null) {
                t_dirctFundCheq = Double.parseDouble(dirctFundCheq.toString());
            }

            String sql19 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE branchId=" + loginBranchID + " "
                    + "AND systemDate='" + systemDate + "' AND PaymentType=3";
            Object totalOtherBnk = sessionFactory.getCurrentSession().createQuery(sql19).uniqueResult();
            if (totalOtherBnk != null) {
                t_bank = t_bank + Double.parseDouble(totalOtherBnk.toString());
            }

            String sql20 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE systemDate = '" + systemDate + "'"
                    + " AND branchId = " + loginBranchID + " AND PaymentType = 1 AND AccountNo = '500042'";
            Object mdFundCash = sessionFactory.getCurrentSession().createQuery(sql20).uniqueResult();
            if (mdFundCash != null) {
                t_mdFundCash = Double.parseDouble(mdFundCash.toString());
            }
            String sql21 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE systemDate = '" + systemDate + "'"
                    + " AND branchId = " + loginBranchID + " AND PaymentType = 2 AND AccountNo = '500042'";
            Object mdFundCheq = sessionFactory.getCurrentSession().createQuery(sql21).uniqueResult();
            if (mdFundCheq != null) {
                t_mdFundCheq = Double.parseDouble(mdFundCheq.toString());
            }

            t_voucher = (t_voucher + t_bank);
            totalAmounts[0] = t_payments;
            totalAmounts[1] = t_other;
            totalAmounts[2] = t_voucher;
            totalAmounts[3] = t_cheqIn;
            totalAmounts[4] = t_open;
            totalAmounts[5] = t_prevDayCashIn;
            totalAmounts[6] = t_prevDayChequeIn;
            totalAmounts[7] = t_cashIn;
            totalAmounts[8] = t_dirctFundCash;
            totalAmounts[9] = t_dirctFundCheq;
            totalAmounts[10] = t_bank;
            totalAmounts[11] = t_mdFundCash;
            totalAmounts[12] = t_mdFundCheq;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalAmounts;
    }

    private double getPreviousDayPhysicalBalance(Date prevsDate, int loginBranchID) {

        double physicalBalance = 0.00;
        double t_payments = 0.00;
        double t_other = 0.00;
        double t_voucher = 0.00;
        try {
            String sql1 = "SELECT SUM(paymentAmount) FROM SettlementPayments WHERE branchId=" + loginBranchID + " "
                    + "AND systemDate='" + dateFormat.format(prevsDate) + "'";
            Object totalPayments = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (totalPayments != null) {
                t_payments = Double.parseDouble(totalPayments.toString());
            }

            String sql2 = "SELECT SUM(paymentAmount) FROM SettlementPaymentOther WHERE systemDate='" + dateFormat.format(prevsDate) + "' AND branchId=" + loginBranchID;
            Object totalOther = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (totalOther != null) {
                t_other = Double.parseDouble(totalOther.toString());
            }

            String sql3 = "SELECT SUM(amount) FROM SettlementVoucher WHERE branchId=" + loginBranchID + " AND voucherDate='" + dateFormat.format(prevsDate) + "' and voucherCatogery<>1";
            Object totalVoucher = sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
            if (totalVoucher != null) {
                t_voucher = Double.parseDouble(totalVoucher.toString());
            }

            physicalBalance = (t_payments + t_other) - t_voucher;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return physicalBalance;
    }

    @Override
    public boolean updateLoanHeaderIsDownPaymntPaid(int loanId, double initialOtherCharge, String othr) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        boolean status = false;
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and  transactionCodeCrdt = '" + othr + "'";

        String sql2 = "from LoanHeaderDetails where loanId='" + loanId + "' ";
        try {
            double paidIntialOthr = 0;
            Object creditSum = session.createQuery(sql1).uniqueResult();
            if (creditSum != null) {
                paidIntialOthr = Double.parseDouble(creditSum.toString());
            }
            if (initialOtherCharge <= paidIntialOthr) {
                LoanHeaderDetails loanHeaderDetails = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();
                loanHeaderDetails.setLoanIsPaidDown(1);
                session.update(loanHeaderDetails);
            }
            status = true;
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return status;
    }

    @Override
    public List<InstallmentReturnList> findAdjustmentList(int loanId, String refNoAdjustment) {
        String sql1 = "from SettlementMain where loanId='" + loanId + "' and  transactionCodeDbt like '" + refNoAdjustment + "'  ";
        List<InstallmentReturnList> loanInstallment = new ArrayList();
        try {
            List<SettlementMain> adjstList = sessionFactory.getCurrentSession().createQuery(sql1).list();

            if (adjstList != null) {
                for (int i = 0; i < adjstList.size(); i++) {
                    InstallmentReturnList installmentReturnList = new InstallmentReturnList();

                    installmentReturnList.setCharge(Math.round(adjstList.get(i).getDbtAmount() * 100) / 100);
                    installmentReturnList.setPaid(0);
                    installmentReturnList.setDue(null);
                    installmentReturnList.setBalance(Math.round(adjstList.get(i).getDbtAmount() * 100) / 100);
                    installmentReturnList.setDescription(adjstList.get(i).getDescription());

                    loanInstallment.add(installmentReturnList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanInstallment;
    }

    @Override
    public double getAdjustmntBalance(int loanId, String refNoAdjustment) {
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refNoAdjustment + "' ";
        double credtAmount = 0;
        String sql2 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refNoAdjustment + "' ";
        double debtAmount = 0;
        double adjustBalanece = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                credtAmount = Double.parseDouble(ob.toString());
            }
            Object ob1 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob1 != null) {
                debtAmount = Double.parseDouble(ob1.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        adjustBalanece = debtAmount - credtAmount;
        return adjustBalanece;
    }

    @Override
    public double getTotalInterest(int loanId, String refNoAdjustment) {
        String sql1 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refNoAdjustment + "' ";

        double totalInterest = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                totalInterest = Double.parseDouble(ob.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return totalInterest;
    }

    @Override
    public int getInstalmentCount(int loanId, String refNoInstallmentCode) {
        String sql1 = "select count(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refNoInstallmentCode + "' ";
        int count = 0;
        Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
        if (ob != null) {
            count = Integer.parseInt(ob.toString());
        }
        return count;
    }

    @Override
    public int getLoanIdByReceiptNo(String receiptNo) {
        String sql1 = "from SettlementPayments where receiptNo='" + receiptNo + "'";
        int loanId = 0;
        List<SettlementPayments> objList = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql1).list();
        if (objList != null) {
            loanId = objList.get(0).getLoanId();
        }
        return loanId;
    }

    @Override
    public RebateForm findRebateCustomerList(String agreementNo) {
        List rebateList = new ArrayList<>();
        RebateForm rebateForm = null;
        try {
            String sql = "select lh.loanId,lh.loanAgreementNo,dh.debtorAccountNo,dh.debtorName,dh.debtorNic,dh.debtorTelephoneMobile,dh.debtorPersonalAddress from LoanHeaderDetails as lh,DebtorHeaderDetails as dh where lh.loanId = dh.debtorId and (lh.loanAgreementNo = '" + agreementNo + "' or lh.loanBookNo = '" + agreementNo + "') and lh.loanIsDelete = 0";
            rebateList = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (rebateList.size() == 1) {
                for (int i = 0; i < rebateList.size(); i++) {
                    rebateForm = new RebateForm();
                    Object[] data = (Object[]) rebateList.get(i);
                    rebateForm.setLoanId(Integer.parseInt(data[0].toString()));
                    rebateForm.setAgreementNo(data[1].toString());
                    rebateForm.setCustomerAccNo(data[2].toString());
                    rebateForm.setDebtorName(data[3].toString());
                    rebateForm.setDebtorNic(data[4].toString());
                    rebateForm.setDebtorTelephone(Integer.parseInt(data[5].toString()));
                    rebateForm.setDebtorAddress(data[6].toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return rebateForm;
    }

    @Override
    public Double findRemainingInterestAmount(int loanID) {
        Double reInterest = 0.00;

        try {

            List<LoanInstallment> loanInstallment = sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanID))
                    .add(Restrictions.eq("isPay", 0)).list();
            for (LoanInstallment li : loanInstallment) {
                reInterest = reInterest + li.getInsInterest();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reInterest;
    }

    @Override
    public int getInstalmentCount(int loanId) {
        int count = 0;
        try {
            List<LoanInstallment> installments = sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("isPay", 0)).list();
            count = installments.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public Boolean saveRebateLoan(RebateDetail rebateDetails) {
        Boolean is_save = false;
        Session session = null;
        Transaction tx = null;
        String rebateCode = null;
        int rebateOrFullyPaidStatus = 0;
        int recode = 0;
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date SystemDate = getSystemDate(userLogBranchId);
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            if (rebateDetails.getRebateId() != null) {
                String sql = "select rebateCode from RebateDetail where rebateId = " + rebateDetails.getRebateId() + "";
                rebateCode = session.createQuery(sql).uniqueResult().toString();

            } else {
                String sql1 = "select rebateCode from RebateDetail order by rebateId desc";
                Object ob = session.createQuery(sql1).setMaxResults(1).uniqueResult();
                if (ob != null) {
                    String code = ob.toString();
                    recode = Integer.parseInt(code.substring(3));
                    recode++;
                } else {
                    recode = 1;
                }
            }
            if (rebateDetails.getRebateId() != null) {
                rebateDetails.setRebateCode(rebateCode);
            } else {
                rebateDetails.setRebateCode(RBT_CODE + recode);
            }
            if (rebateDetails.getIsFullyPaid() == false) {
                rebateDetails.setIsRebate(true);
            }
//            rebateDetails.setIsRebate(false);
            rebateDetails.setIsDelete(false);
            rebateDetails.setIsApproved(0);
            if (rebateDetails.getDiscountRate() > 35) {
                rebateDetails.setApprovalStatus(1);
            }
            rebateDetails.setActionTime(date);
            rebateDetails.setSystemDate(SystemDate);
            rebateDetails.setUserId(userId);
            rebateDetails.setBranchId(userLogBranchId);
            session.saveOrUpdate(rebateDetails);
            String sql2 = "delete from RebateApprovals as r where r.rebateLoanId = " + rebateDetails.getLoanId() + "";
            session.createQuery(sql2).executeUpdate();
            tx.commit();
            is_save = true;

            if (rebateDetails.getIsFullyPaid() == true) {
                rebateOrFullyPaidStatus = 4;
            } else if (rebateDetails.getIsRebate() == true) {
                rebateOrFullyPaidStatus = 5;
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
            updateLoanHeaderStatus(rebateDetails.getLoanId(), rebateOrFullyPaidStatus);
        }
        return is_save;

    }

    @Override
    public boolean saveSMS(SmsDetails smsDetails) {
        boolean success = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        Date actionTime = Calendar.getInstance().getTime();
        int userId = userService.findByUserName();
        smsDetails.setUserId(userId);
        smsDetails.setActionTime(actionTime);
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(smsDetails);
            transaction.commit();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return success;
    }

    @Override
    public boolean removeSMS(int loanId, String recptNo) {
        boolean delete = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            String sql = "from SmsDetails where smsLoanId=" + loanId + " AND smsReceiptNo='" + recptNo + "' AND isSendTo=0";
            SmsDetails smsDetails = (SmsDetails) session.createQuery(sql).uniqueResult();
            if (smsDetails != null) {
                smsDetails.setIsDelete(true);
                transaction = session.beginTransaction();
                session.saveOrUpdate(smsDetails);
                transaction.commit();
                delete = true;

            }
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return delete;
    }

    @Override
    public List<RebateDetail> getRebateDetails() {
        List<RebateDetail> rebateDetails = new ArrayList<>();
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date SystemDate = getSystemDate(userLogBranchId);
        try {
            String sql = "from RebateDetail where isDelete = 0 AND isRebate = 0 AND systemDate = '" + SystemDate + "' and branchId = " + userLogBranchId + "";
            List<RebateDetail> list = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (list.size() > 0) {
                for (RebateDetail rebateDetail : list) {
                    Integer loanId = rebateDetail.getLoanId();
                    LoanHeaderDetails loanHeaderDetails = loanDao.findLoanHeaderByLoanId(loanId);
                    if (loanHeaderDetails != null) {
                        rebateDetail.setLoanAggNo(loanHeaderDetails.getLoanAgreementNo());
                        rebateDetail.setLoanBookNo(loanHeaderDetails.getLoanBookNo());
                    }
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanId);
                    if (lpvd != null) {
                        rebateDetail.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    rebateDetails.add(rebateDetail);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rebateDetails;
    }

    @Override
    public boolean deleteRebateLoan(int loanId) {
        boolean delete = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            // find rebate details by loanId and delete status
            RebateDetail rebateDetail = (RebateDetail) session.createQuery("from RebateDetail where loanId = :loanId AND isDelete = :isDelete")
                    .setParameter("loanId", loanId).setParameter("isDelete", false).uniqueResult();
            if (rebateDetail != null) {
                // update delete status
                rebateDetail.setIsDelete(true);
                session.update(rebateDetail);
                // delete debit entry from settlementmain
                session.createQuery("delete from SettlementMain where loanId = :loanId AND transactionNoDbt = :transactionNoDbt")
                        .setParameter("loanId", loanId).setParameter("transactionNoDbt", rebateDetail.getRebateCode())
                        .executeUpdate();
                delete = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            transaction.commit();
            session.flush();
            session.close();
        }
        return delete;
    }

    @Override
    public boolean saveOrUpdateLoanCheque(LoanCheques loanCheques) {
        boolean success = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            loanCheques.setIsPayment(false);
            loanCheques.setActionTime(Calendar.getInstance().getTime());
            loanCheques.setUserId(userService.findByUserName());
            transaction = session.beginTransaction();
            session.saveOrUpdate(loanCheques);
            transaction.commit();
            success = true;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return success;
    }

    @Override
    public List<LoanCheques> getLoanChequeses() {
        List<LoanCheques> lcs = new ArrayList<>();
        try {
            String sql = "from LoanCheques";
            List<LoanCheques> list = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (list.size() > 0) {
                for (LoanCheques loanCheq : list) {
                    loanCheq.setRemainChequeAmount(loanCheq.getChequeAmount());
                    lcs.add(loanCheq);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return lcs;
    }

    @Override
    public List<LoanCheques> getLoanChequeses(int loanId) {
        List<LoanCheques> lcs = new ArrayList<>();
        try {
            String sql = "from LoanCheques where loanId=" + loanId;
            List<LoanCheques> loanCheqs = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (loanCheqs.size() > 0) {
                for (LoanCheques lc : loanCheqs) {
                    double totCheqAmount = lc.getChequeAmount();
                    String sql2 = "from SettlementPaymentDetailCheque where chequeNo='" + lc.getChequeNo() + "'";
                    List<SettlementPaymentDetailCheque> detailCheques = sessionFactory.getCurrentSession().createQuery(sql2).list();
                    if (detailCheques.size() > 0) {
                        double detailCheqAmount = 0.00;
                        for (SettlementPaymentDetailCheque detailCheque : detailCheques) {
                            detailCheqAmount = detailCheqAmount + detailCheque.getAmount();
                        }
                        if (detailCheqAmount < totCheqAmount) {
                            double remainCheqAmount = totCheqAmount - detailCheqAmount;
                            lc.setRemainChequeAmount(remainCheqAmount);
                            lcs.add(lc);
                        }
                    } else {
                        lc.setRemainChequeAmount(lc.getChequeAmount());
                        lcs.add(lc);
                    }
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return lcs;
    }

    @Override
    public boolean deleteLoanCheque(int chequeId, int loanId) {
        boolean success = false;
        try {
            String sql = "delete from LoanCheques where id=" + chequeId + " AND loanId=" + loanId;
            int executeUpdate = sessionFactory.getCurrentSession().createQuery(sql).executeUpdate();
            if (executeUpdate > 0) {
                success = true;
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean setPaidLoanCheque(int chequeId, int loanId) {
        boolean success = false;
        try {
            String sql = "update LoanCheques set isPayment=:isPayment where pdChequeId=:pdChequeId AND loanId=:loanId";
            int executeUpdate = sessionFactory.getCurrentSession().createQuery(sql).setParameter("isPayment", true)
                    .setParameter("pdChequeId", chequeId)
                    .setParameter("loanId", loanId).executeUpdate();
            if (executeUpdate > 0) {
                success = true;
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public double getRebateBalance(int loanId) {
        double rebateBalance = 0;
        String sql1 = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + RBT_CODE + "'  ";
        double dbtAmount = 0;
        String sql2 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + RBT_CODE + "'  ";
        double crdtAmount = 0;
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                dbtAmount = Double.parseDouble(ob.toString());
            }
            Object ob2 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (ob2 != null) {
                crdtAmount = Double.parseDouble(ob2.toString());
            }
            rebateBalance = dbtAmount - crdtAmount;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return rebateBalance;
    }

    @Override
    public boolean updateLoanHeaderStatus(int loanId, int type) {
        boolean status = false;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql1 = " from LoanHeaderDetails where loanId=" + loanId + " ";
            LoanHeaderDetails loanHeaderDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (loanHeaderDetails != null) {
                loanHeaderDetails.setLoanSpec(type);
                session.update(loanHeaderDetails);
                status = true;
                tx.commit();
                session.close();
            }

            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public boolean updateRebateDetail(int loanId) {
        boolean status = false;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql1 = " from RebateDetail where loanId=" + loanId + " ";
            RebateDetail rebateDetail = (RebateDetail) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (rebateDetail != null) {
                rebateDetail.setIsRebate(true);
                session.update(rebateDetail);
                status = true;
                tx.commit();
                session.close();
            }

            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public Double getLoanChequeTotAmount(int loanId) {
        Double totAmount = 0.00;
        try {
            String sql = "SELECT SUM(lc.chequeAmount) FROM LoanCheques AS lc, MBankDetails AS mb WHERE lc.bankId = mb.id AND lc.loanId =" + loanId
                    + " AND lc.isPayment = 0";
            Object result = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (result != null) {
                totAmount = Double.valueOf(result.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totAmount;
    }

    @Override
    public List<RebateDetail> loadRebateApprovalList() {
        List<RebateDetail> rebateApprovalList = new ArrayList<>();
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        try {
            String sql = "from RebateDetail where isApproved  < 2 and isDelete = 0 and branchId = " + userLogBranchId + "";
            List<RebateDetail> list = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (list.size() > 0) {
                for (RebateDetail rebateDetail : list) {
                    Integer loanId = rebateDetail.getLoanId();
                    LoanHeaderDetails loanHeaderDetails = loanDao.findLoanHeaderByLoanId(loanId);
                    if (loanHeaderDetails != null) {
                        rebateDetail.setLoanAggNo(loanHeaderDetails.getLoanAgreementNo());
                        rebateDetail.setLoanBookNo(loanHeaderDetails.getLoanBookNo());
                    }
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanId);
                    if (lpvd != null) {
                        rebateDetail.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    rebateApprovalList.add(rebateDetail);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return rebateApprovalList;
    }

    @Override
    public int findPaidInstallmentCount(int loanID) {
        int paidCount = 0;
        try {
            // String sql1 = "select count(sm.crdtAmount) from SettlementMain as sm where sm.loanId= " + loanID + " and sm.transactionCodeCrdt like '%" + refNoInstallmentCode + "%' and sm.description = '" + installment + "'";
            List<LoanInstallment> loanInstallment = sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanID))
                    .add(Restrictions.eq("isPay", 1)).list();
            paidCount = loanInstallment.size();
//            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
//            if (ob != null) {
//                paidCount = Integer.parseInt(ob.toString());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return paidCount;
    }

    @Override
    public RebateApprovals findRebateApprovals(int loanId, int type) {
        RebateApprovals approvals = null;
        try {
            approvals = (RebateApprovals) sessionFactory.getCurrentSession().createCriteria(RebateApprovals.class)
                    .add(Restrictions.eq("rebateLoanId", loanId))
                    .add(Restrictions.eq("approvalStatus", type)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return approvals;

    }

    @Override
    public Boolean saveRebateApproval(RebateApprovals approvals) {
        Boolean isSave = true;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Date actionTime = Calendar.getInstance().getTime();
            int userId = userService.findByUserName();
            int userLogBranchId = settlmentService.getUserLogedBranch(userId);
            Date SystemDate = getSystemDate(userLogBranchId);
            approvals.setActionTime(actionTime);
            approvals.setUserId(userId);
            approvals.setBranchId(userLogBranchId);
            session.saveOrUpdate(approvals);
            //update RebateDetails
            if (approvals.getRebateAppId() > 0 && approvals.getIsReject() == 1) {
                RebateDetail rebateDetail = (RebateDetail) session.createCriteria(RebateDetail.class)
                        .add(Restrictions.eq("loanId", approvals.getRebateLoanId()))
                        .add(Restrictions.eq("isDelete", false)).uniqueResult();
                if (rebateDetail.getApprovalStatus() == 0) {
                    //only approval 1
                    rebateDetail.setIsApproved(2);
                } else {
                    // must be approval 1 and 2
                    rebateDetail.setIsApproved(1);
                    if (approvals.getApprovalStatus() == 1) {
                        rebateDetail.setIsApproved(2);
                    }
                }
                session.saveOrUpdate(rebateDetail);
                //update SettlmentMain
                if (rebateDetail.getIsApproved() == 2) {
                    LoanHeaderDetails headerDetails = (LoanHeaderDetails) session.createCriteria(LoanHeaderDetails.class)
                            .add(Restrictions.eq("loanId", rebateDetail.getLoanId())).uniqueResult();
                    if (headerDetails != null) {
                        // add rebate payment to settlement
                        SettlementMain sett1 = new SettlementMain();
                        sett1.setLoanId(headerDetails.getLoanId());
                        sett1.setDebtorId(headerDetails.getDebtorHeaderDetails().getDebtorId());
                        sett1.setDbtAmount(rebateDetail.getRebateAmount());
                        sett1.setTransactionCodeDbt(RBT_CODE);
                        sett1.setTransactionNoDbt(rebateDetail.getRebateCode());
                        sett1.setDescription("Rebate Payment");
                        sett1.setActionDate(actionTime);
                        sett1.setIsPosting(false);
                        sett1.setBranchId(userLogBranchId);
                        sett1.setSystemDate(SystemDate);
                        sett1.setIsPrinted(false);
                        sett1.setUserId(userId);
                        session.save(sett1);

                        if (rebateDetail.getCharge() > 0) {
                            // add rebate charge to loan
                            String rbtAccNo = loanDao.findLoanAccountNo(0, RBT_CODE);
                            Integer subTaxId = findSubTaxDetails(rbtAccNo);
                            LoanOtherCharges charges = new LoanOtherCharges();
                            charges.setLoanId(headerDetails.getLoanId());
                            charges.setChargesId(subTaxId);
                            charges.setAccountNo(rbtAccNo);
                            charges.setActualAmount(rebateDetail.getCharge());
                            charges.setChargeAmount(rebateDetail.getCharge());
                            charges.setUserId(userId);
                            charges.setIsDownPaymentCharge(0);
                            charges.setIsPaid(0);
                            charges.setActionTime(actionTime);
                            session.save(charges);

                            // add rebate charge to settlement
                            SettlementMain sett2 = new SettlementMain();
                            sett2.setLoanId(headerDetails.getLoanId());
                            sett2.setDebtorId(headerDetails.getDebtorHeaderDetails().getDebtorId());
                            sett2.setDbtAmount(rebateDetail.getCharge());
                            sett2.setTransactionCodeDbt("OTHR");
                            sett2.setTransactionNoDbt("OTHR" + subTaxId);
                            sett2.setDescription("Rebate Charge");
                            sett2.setActionDate(actionTime);
                            sett2.setIsPosting(false);
                            sett2.setBranchId(userLogBranchId);
                            sett2.setSystemDate(SystemDate);
                            sett2.setIsPrinted(false);
                            sett2.setUserId(userId);
                            session.save(sett2);

                            // rebate charge posting goes here
                        }
                    }
                }
            } else {
                RebateDetail rebateDetail = (RebateDetail) session.createCriteria(RebateDetail.class)
                        .add(Restrictions.eq("loanId", approvals.getRebateLoanId()))
                        .add(Restrictions.eq("isDelete", false)).uniqueResult();
                if (approvals.getIsReject() == 0) {
                    rebateDetail.setIsApproved(0);
                }
                session.saveOrUpdate(rebateDetail);
            }
        } catch (Exception e) {
            e.printStackTrace();
            isSave = false;
            tx.rollback();
        } finally {
            tx.commit();
            session.flush();
            session.close();
        }
        return isSave;

    }

    @Override
    public RebateDetail findRebateDetails(int loanId) {
        RebateDetail detail = null;
        try {
            detail = (RebateDetail) sessionFactory.getCurrentSession().createCriteria(RebateDetail.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("isDelete", false))
                    .add(Restrictions.eq("isRebate", false)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detail;
    }

    @Override
    public boolean isInitialPaymentDone(int loanId) {
        boolean status = false;
        LoanHeaderDetails detail = null;
        try {
            detail = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();

            if (detail.getLoanIsPaidDown() != 0) {
                status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public LoanHeaderDetails findLoanHeaderDetails(int printLoanId) {
        LoanHeaderDetails details = null;
        try {
            details = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                    .add(Restrictions.eq("loanId", printLoanId)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return details;
    }

    @Override
    public RebateDetail findRebateDetailsAfterRebate(int loanId) {
        RebateDetail detail = null;
        try {
            detail = (RebateDetail) sessionFactory.getCurrentSession().createCriteria(RebateDetail.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("isDelete", false))
                    .add(Restrictions.eq("isRebate", true)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detail;
    }

    @Override
    public SettlementPayments getPaymentDetail(String transactionNo) {
        SettlementPayments obj;
        try {
            String sql1 = " from SettlementPayments where transactionNo = '" + transactionNo + "' ";
            obj = (SettlementPayments) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return obj;
    }

    @Override
    public List<LoanInstallment> findPaybleInstalmentsList(int loanId, Date systemDate) {
        List<LoanInstallment> list = new ArrayList<>();
        try {
            String sql1 = "from LoanInstallment where dueDate <='" + systemDate + "' and  loanId ='" + loanId + "'";
            list = sessionFactory.getCurrentSession().createQuery(sql1).list();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public boolean saveOrUpdateDebtorDeposit(DebtorDeposit debtorDeposit) {
        boolean success = false;
        int userId = userService.findByUserName();
        String current_date = dateFormat.format(Calendar.getInstance().getTime());
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            if (debtorDeposit.getDepositId() == null) {
                debtorDeposit.setUserId(userId);
                debtorDeposit.setActionTime(Calendar.getInstance().getTime());
                debtorDeposit.setDepositDate(dateFormat.parse(current_date));
                session.saveOrUpdate(debtorDeposit);
            } else {
                String sql1 = "update DebtorDeposit set depositAmount = depositAmount + " + debtorDeposit.getDepositAmount() + " where depositId = " + debtorDeposit.getDepositId();
                session.createQuery(sql1).executeUpdate();
            }
            tx.commit();
            success = true;
        } catch (ParseException | HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<DebtorDeposit> findDebtorDeposits(int branchId) {
        List<DebtorDeposit> debtorDeposits = new ArrayList<>();
        try {
            String sql1 = "from DebtorDeposit where isNew = 1 and branchId = " + branchId;
            List<DebtorDeposit> deposits = sessionFactory.getCurrentSession().createQuery(sql1).list();
            for (DebtorDeposit debtorDeposit : deposits) {
                DebtorHeaderDetails dhd = loanDao.findDebtorHeader(debtorDeposit.getDebtorId());
                if (dhd != null) {
                    debtorDeposit.setDebtorName(dhd.getNameWithInitial());
                }
                debtorDeposits.add(debtorDeposit);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return debtorDeposits;
    }

    @Override
    public DebtorDeposit findDebtorDeposit(int depositId) {
        DebtorDeposit debtorDeposit = null;
        try {
            String sql1 = "from DebtorDeposit where depositId = " + depositId;
            debtorDeposit = (DebtorDeposit) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (debtorDeposit != null) {
                DebtorHeaderDetails dhd = loanDao.findDebtorHeader(debtorDeposit.getDebtorId());
                if (dhd != null) {
                    debtorDeposit.setDebtorName(dhd.getNameWithInitial());
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return debtorDeposit;
    }

    @Override
    public List<DebtorDeposit> findDebtorDeposits(int debtorId, String accNo) {
        List<DebtorDeposit> debtorDeposits = null;
        try {
            String sql = "from DebtorDeposit where debtorId = " + debtorId + " AND accountNo = '" + accNo + "' AND isNew = 1";
            debtorDeposits = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return debtorDeposits;
    }

    @Override
    public boolean updateDebtorDeposit(int debtorId, String accNo) {
        boolean update = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "update DebtorDeposit set isNew = 0 where debtorId = " + debtorId + " AND accountNo = '" + accNo + "'";
            session.createQuery(sql).executeUpdate();
            tx.commit();
            update = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return update;
    }

    @Override
    public boolean updateInsuaranceProcessIsPay(int loanId) {
        boolean status = false;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql1 = " from InsuraceProcessMain where insuLoanId=" + loanId + " ";
            InsuraceProcessMain insuraceProcessMain = (InsuraceProcessMain) session.createQuery(sql1).uniqueResult();
            if (insuraceProcessMain != null) {
                insuraceProcessMain.setIsPay(true);
                session.update(insuraceProcessMain);
            }
            status = true;
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return status;
    }

    @Override
    public boolean updateRemainingInsuraceProcessMainList(int loanId, double insuranceBalance) {
        boolean status = false;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        double remainingBalance = insuranceBalance;
        try {
            String sql1 = " from InsuraceProcessMain where insuLoanId=" + loanId + " ORDER BY DESC  ";
            List<InsuraceProcessMain> insuraceProcessMain = (List<InsuraceProcessMain>) session.createQuery(sql1).list();
            for (int i = 0; i < insuraceProcessMain.size(); i++) {
                if (insuraceProcessMain.get(i).getInsuTotalPremium() < remainingBalance) {
                    insuraceProcessMain.get(i).setIsPay(true);
                    insuraceProcessMain.set(i, insuraceProcessMain.get(i));
                    session.update(insuraceProcessMain.get(i));
                    remainingBalance = remainingBalance - insuraceProcessMain.get(i).getInsuTotalPremium();
                }
            }
            status = true;
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return status;
    }

    @Override
    public void updateLoanInstallment(int loanId, double remainingInstallmentBalance, Date systemDate, String transCodeCredit, double crdAmt, String receiptNo, double payAmount) {
        List<LoanInstallment> list = new ArrayList<>();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Date actionTime = Calendar.getInstance().getTime();
        double balance = 0;
        try {
            String sql1 = "from LoanInstallment where dueDate <='" + systemDate + "' and  loanId ='" + loanId + "' and isPay  < 1";
            list = (List<LoanInstallment>) sessionFactory.getCurrentSession().createQuery(sql1).list();
            double balanceAmount = 0.0;
            balanceAmount = remainingInstallmentBalance;

            String loanAmtWithInt = "SELECT SUM(insRoundAmount) FROM LoanInstallment WHERE loanId ='" + loanId + "'";
            double loanWithIntAmt = (double) sessionFactory.getCurrentSession().createQuery(loanAmtWithInt).uniqueResult();

            String lastDebBalAmt = "FROM LoanInstallment WHERE debtorBalance > 0 AND loanId ='" + loanId + "'  ORDER BY insId DESC";
            LoanInstallment li = (LoanInstallment) sessionFactory.getCurrentSession().createQuery(lastDebBalAmt).setMaxResults(1).uniqueResult();
            if (li != null) {
                balance = li.getDebtorBalance() - payAmount;
            } else {
                balance = loanWithIntAmt - payAmount;
            }

            for (int i = 0; i < list.size(); i++) {
                double c = 0, paidHalfPay = 0;
                LoanInstallment installment = list.get(i);

                if (transCodeCredit.equalsIgnoreCase("INS") && crdAmt > 0) {
                    if (installment.getIsPay() == -1) {
                        String sql2 = "SELECT SUM(s.crdtAmount) FROM SettlementMain as s WHERE s.transactionCodeCrdt = 'INS' AND s.loanId = " + loanId + " and  s.settlementId != (SELECT max(sm.settlementId) FROM SettlementMain sm WHERE sm.transactionCodeCrdt = 'INS' AND sm.loanId = " + loanId + ") ";
                        double b = (double) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();

                        String sql3 = "SELECT SUM(lo.insRoundAmount) from LoanInstallment as lo where lo.loanId ='" + loanId + "' and lo.isPay in ('-1','1')";
                        double a = (double) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();

                        c = a - b;
                        paidHalfPay = installment.getInsRoundAmount() - c;
                    }
                    double currentPaid = balanceAmount + paidHalfPay;
                    double paindIntr = 0.00;
                    double paidCapital = 0.00;

                    if (balanceAmount < (installment.getInsRoundAmount() - paidHalfPay)) {
                        installment.setIsPay(-1);
                        installment.setIsArrears(1);
                        for (int j = 0; i < 1; j++) {
                            installment.setReceiptNo(receiptNo);
                            installment.setPayAmount(payAmount);
                            installment.setPayDate(systemDate);
                            installment.setDebtorBalance(balance);
                            break;
                        }
                        if (currentPaid < installment.getInsRoundAmount()) {
                            if (installment.getInsInterest() <= currentPaid) {
                                System.out.println("Paid Intr    ====>>>> " + installment.getInsInterest());
                                System.out.println("Paid Capital ====>>>> " + (currentPaid - installment.getInsInterest()));
                                double totPaid = installment.getInsInterest() + currentPaid - installment.getInsInterest();
                                System.out.println("Total Paid ====>>>> " + totPaid);
                                installment.setPaidInterest(installment.getInsInterest());
                                installment.setPaidCapital((currentPaid - installment.getInsInterest()));
                                installment.setPaidTotal(totPaid);
                            } else {
                                System.out.println("Paid Intr in Else ---->>>  " + currentPaid);
                                System.out.println("Paid Capital in Else ---->>>  " + 0);
                                double totPaid = currentPaid + 0;
                                System.out.println("Total Paid ---->>>  " + totPaid);

                                installment.setPaidInterest(currentPaid);
                                installment.setPaidCapital(0.00);
                                installment.setPaidTotal(totPaid);
                            }
                        }
                        session.update(installment);
                        break;
                    } else if (balanceAmount == (installment.getInsRoundAmount() - paidHalfPay)) {
                        installment.setIsPay(1);
                        installment.setIsArrears(0);

                        for (int j = 0; i < 1; j++) {
                            installment.setReceiptNo(receiptNo);
                            installment.setPayAmount(payAmount);
                            installment.setPayDate(systemDate);
                            installment.setDebtorBalance(balance);
                            break;
                        }

                        System.out.println("pAID iNTR   ======>>> " + installment.getInsInterest());
                        System.out.println("pAID cAPITAL======>>> " + installment.getInsPrinciple());
                        double totPaid = installment.getInsInterest() + installment.getInsPrinciple();
                        System.out.println("Total pAID  ======>>> " + totPaid);

                        installment.setPaidInterest(installment.getInsInterest());
                        installment.setPaidCapital(installment.getInsPrinciple());
                        installment.setPaidTotal(totPaid);

                        session.update(installment);
                        break;
                    } else {
                        if (balanceAmount >= installment.getInsRoundAmount()) {
                            if (installment.getInsInterest() <= currentPaid) {
                                System.out.println("else Paid Intr    ====>>>> " + installment.getInsInterest());
                                installment.setPaidInterest(installment.getInsInterest());
                                currentPaid = currentPaid - installment.getInsInterest();
                            }
                            if (installment.getInsPrinciple() <= currentPaid) {
                                System.out.println("else Paid Capital ====>>>> " + (installment.getInsPrinciple()));
                                installment.setPaidCapital(installment.getInsPrinciple());
                                currentPaid = currentPaid - installment.getInsPrinciple();
                            }
                            double totPaid = (installment.getInsInterest() + installment.getInsPrinciple());
                            System.out.println("else Total Paid ====>>> " + totPaid);
                            installment.setPaidTotal(totPaid);

                        }
                        balanceAmount = balanceAmount - (installment.getInsRoundAmount() - paidHalfPay);

                        installment.setIsPay(1);
                        installment.setIsArrears(0);
                        for (int j = 0; i < 1; j++) {
                            installment.setReceiptNo(receiptNo);
                            installment.setPayAmount(payAmount);
                            installment.setPayDate(systemDate);
                            installment.setDebtorBalance(balance);
                            break;
                        }
                        session.update(installment);
                    }
                }
            }
            session.flush();
            session.clear();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void removeInstalmentPayStatus(int loanId, Double crdtAmount) {
        List<LoanInstallment> list = new ArrayList<>();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            list = (List<LoanInstallment>) sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.ge("isPay", -1))
                    .add(Restrictions.le("isPay", 1))
                    .add(Restrictions.ne("isPay", 0)).addOrder(Order.desc("dueDate")).list();
            for (int i = 0; i < list.size(); i++) {
                LoanInstallment loanInstallment = list.get(i);
                if (crdtAmount >= loanInstallment.getInsRoundAmount()) {
                    loanInstallment.setIsPay(0);
                    loanInstallment.setIsArrears(1);
                    session.update(loanInstallment);
                    crdtAmount = crdtAmount - loanInstallment.getInsRoundAmount();
                } else if (crdtAmount > 0) {
                    loanInstallment.setIsPay(-1);
                    loanInstallment.setIsArrears(1);
                    session.update(loanInstallment);
                }
            }
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public boolean saveOrUpdateLoanRescheduleDetails(LoanRescheduleDetails loanRescheduleDetails) {
        boolean success = false;
        int userId = userService.findByUserName();
        int branchId = loanRescheduleDetails.getBranchId();
        int parentLoanId = loanRescheduleDetails.getLoanId();
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "from LoanRescheduleDetails where loanHeaderDetailsByChildLoan = :loanHeaderDetailsByChildLoan";
            LoanRescheduleDetails lrd = (LoanRescheduleDetails) session.createQuery(sql)
                    .setParameter("loanHeaderDetailsByChildLoan", new LoanHeaderDetails(parentLoanId))
                    .uniqueResult();
            loanRescheduleDetails.setLoanHeaderDetailsByParentLoan(new LoanHeaderDetails(parentLoanId));
            if (lrd != null) {
                loanRescheduleDetails.setLoanHeaderDetailsByMasterLoan(lrd.getLoanHeaderDetailsByMasterLoan());
                loanRescheduleDetails.setLoanHeaderDetailsByChildLoan(null);
            } else {
                loanRescheduleDetails.setLoanHeaderDetailsByMasterLoan(new LoanHeaderDetails(parentLoanId));
                loanRescheduleDetails.setLoanHeaderDetailsByChildLoan(null);
            }
            loanRescheduleDetails.setActionTime(Calendar.getInstance().getTime());
            loanRescheduleDetails.setMBranch(new MBranch(branchId));
            loanRescheduleDetails.setUmUser(new UmUser(userId));
            loanRescheduleDetails.setIsReschedule(false);
            loanRescheduleDetails.setIsDelete(false);
            if (loanRescheduleDetails.getId() == null) {
                loanRescheduleDetails.setApproval(0);
            }
            session.saveOrUpdate(loanRescheduleDetails);
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<LoanRescheduleDetails> findLoanRescheduleDetails() {
        List<LoanRescheduleDetails> loanRescheduleDetails = null;
        try {
            String sql = "from LoanRescheduleDetails where isReschedule = :isReschedule";
            loanRescheduleDetails = sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("isReschedule", false).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanRescheduleDetails;
    }

    @Override
    public boolean activeLoanRescheduleDetails(int rescheduleId) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "update LoanRescheduleDetails set isDelete = :isDelete where id = :id";
            session.createQuery(sql).setParameter("isDelete", false).setParameter("id", rescheduleId).executeUpdate();
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public boolean inActiveLoanRescheduleDetails(int rescheduleId) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "update LoanRescheduleDetails set isDelete = :isDelete where id = :id";
            session.createQuery(sql).setParameter("isDelete", true).setParameter("id", rescheduleId).executeUpdate();
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public LoanRescheduleDetails findLoanRescheduleDetail(int rescheduleId) {
        LoanRescheduleDetails loanRescheduleDetails = null;
        try {
            String sql = "from LoanRescheduleDetails where id = :id";
            loanRescheduleDetails = (LoanRescheduleDetails) sessionFactory.getCurrentSession()
                    .createQuery(sql).setParameter("id", rescheduleId)
                    .uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanRescheduleDetails;
    }

    @Override
    public boolean changeLoanRescheduleDetailStatus(int rescheduleId, int childLoanId) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "update LoanRescheduleDetails set isReschedule = :isReschedule , loanHeaderDetailsByChildLoan = :loanHeaderDetailsByChildLoan where id = :id";
            session.createQuery(sql).setParameter("isReschedule", true)
                    .setParameter("loanHeaderDetailsByChildLoan", new LoanHeaderDetails(childLoanId))
                    .setParameter("id", rescheduleId)
                    .executeUpdate();
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<SettlementMain> findByLoanId(int loanId) {
        List<SettlementMain> settlementList = null;
        try {
            String sql = "from SettlementMain where loanId = :loanId and isPosting = :isPosting";
            settlementList = sessionFactory.getCurrentSession().createQuery(sql).setParameter("loanId", loanId)
                    .setParameter("isPosting", true).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return settlementList;
    }

    @Override
    public List<Posting> findByReferenceNo(String referenceNo) {
        List<Posting> postingList = null;
        try {
            String sql = "from Posting where referenceNo = :referenceNo";
            postingList = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("referenceNo", referenceNo).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return postingList;
    }

    @Override
    public List<LoanPosting> findLoanPostings(int loanId) {
        List<LoanPosting> loanPostingList = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        try {
            List<SettlementMain> settlementList = findByLoanId(loanId);
            String referenceNo = "";
            String datePart = "";
            String accountName = "";
            for (SettlementMain sm : settlementList) {
                Integer settlementId = sm.getSettlementId();
                Date systemDate = sm.getSystemDate();
                if (systemDate != null) {
                    datePart = df.format(systemDate);
                }
                referenceNo = datePart + loanId + "/" + settlementId;
                List<Posting> postingList = findByReferenceNo(referenceNo);
                for (Posting ps : postingList) {
                    boolean startsWith = ps.getAccountNo().startsWith("IFD");
                    if (startsWith) {
                        DebtorHeaderDetails debtor = loanDao.findDebtorHeader(ps.getAccountNo());
                        if (debtor != null) {
                            accountName = debtor.getNameWithInitial();
                        }
                    } else {
                        Chartofaccount chartofaccount = masterDataService.findAccount(ps.getAccountNo());
                        if (chartofaccount != null) {
                            accountName = chartofaccount.getAccountName();
                        }
                    }
                    LoanPosting loanPosting = new LoanPosting();
                    loanPosting.setAccountName(accountName);
                    loanPosting.setAccountNo(ps.getAccountNo());
                    loanPosting.setAmount(ps.getAmount());
                    loanPosting.setCreditAmount(ps.getCAmount());
                    loanPosting.setDebitAmount(ps.getDAmount());
                    loanPosting.setPostingDate(ps.getDate());
                    loanPostingList.add(loanPosting);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanPostingList;
    }

    @Override
    public double getLoanPaidOtherCharges(int loanId) {
        double paidOtherCharge = 0.00;
        String refCodeOtherCharge = "OTHR";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeOtherCharge + "' ";
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                paidOtherCharge = Double.parseDouble(ob.toString());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return paidOtherCharge;
    }

    @Override
    public double getLoanPaidInsurenceCharges(int loanId) {
        double paidInsurenceCharge = 0.00;
        String refCodeInsuarance = "INSU";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeInsuarance + "' ";
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                paidInsurenceCharge = Double.parseDouble(ob.toString());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return paidInsurenceCharge;
    }

    @Override
    public double getLoanPaidInstallments(int loanId) {
        double paidInstallments = 0.00;
        String refCodeInstallments = "INS";
        String sql1 = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeInstallments + "' ";
        try {
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                paidInstallments = Double.parseDouble(ob.toString());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return paidInstallments;
    }

    @Override
    public File batchPostingToZip(Date startDate, Date endDate) {
        File zipFile = null;
        DateFormat dtf = new SimpleDateFormat("yyyyMMdd");
        String sDate = dtf.format(startDate);
        String eDate = dtf.format(endDate);
        String srcFile = "C:\\tmpFile\\Posting_" + sDate + "_" + eDate;
        String outputZipFile = "C:\\tmpFile\\Posting_" + sDate + "_" + eDate + ".zip";
        File reportFile = new File(srcFile);
        if (!reportFile.exists()) {
            if (reportFile.mkdir()) {
                if (batchPostingToCsv(startDate, endDate, srcFile)) {
                    processBatchCustomers(srcFile);
                    FileZip.zipIt(srcFile, outputZipFile);
                    zipFile = new File(outputZipFile);
                }
            }
        } else if (updatePostingDownload(startDate)) {
            zipFile = new File(outputZipFile);
        }
        return zipFile;
    }

    @Override
    public List<LoanHeaderDetails> findIssueLoans() {
        List<LoanHeaderDetails> issueLoans = null;
        try {
            String sql = "from LoanHeaderDetails where loanIsIssue = :loanIsIssue and loanIsActive = :loanIsActive";
            issueLoans = sessionFactory.getCurrentSession().createQuery(sql).setParameter("loanIsIssue", 2)
                    .setParameter("loanIsActive", true).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return issueLoans;
    }

    @Override
    public List<SettlementMain> findSettlements(int loanId, Date startDate) {
        List<SettlementMain> settlementList = null;
        List<String> creditTransCodes = Arrays.asList(new String[]{"DWNS", "OTHR", "INSU", "OVP", "INS", "ODI"});
        List<String> debitTransCodes = Arrays.asList(new String[]{"INTRS", "OTHR", "INSU", "INS"});
        try {
            String sql = "from SettlementMain where (transactionCodeCrdt in (:transactionCodeCrdt) or transactionCodeDbt in (:transactionCodeDbt)) and isPosting = :isPosting and systemDate = :systemDate and loanId = :loanId";
            settlementList = sessionFactory.getCurrentSession().createQuery(sql).setParameter("loanId", loanId)
                    .setParameter("isPosting", true)
                    .setParameterList("transactionCodeCrdt", creditTransCodes)
                    .setParameterList("transactionCodeDbt", debitTransCodes)
                    .setParameter("systemDate", startDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return settlementList;
    }

    private List<SettlementMain> findSettlements(int loanId, Date sDate, Date eDate) {
        List<SettlementMain> settlementList = null;
        List<String> creditTransCodes = Arrays.asList(new String[]{"DWNS", "OTHR", "INSU", "OVP", "INS", "ODI"});
        List<String> debitTransCodes = Arrays.asList(new String[]{"INTRS", "OTHR", "INSU", "INS"});
        try {
//            String sql = "from SettlementMain where (transactionCodeCrdt in (:transactionCodeCrdt) or transactionCodeDbt in (:transactionCodeDbt)) and isPosting = :isPosting and systemDate = :systemDate and loanId = :loanId";
            String sql = "from SettlementMain where (transactionCodeCrdt in (:transactionCodeCrdt) or transactionCodeDbt in (:transactionCodeDbt)) and isPosting = :isPosting and systemDate BETWEEN :startDate AND :endDate and loanId = :loanId";
            settlementList = sessionFactory.getCurrentSession().createQuery(sql).setParameter("loanId", loanId)
                    .setParameter("isPosting", true)
                    .setParameterList("transactionCodeCrdt", creditTransCodes)
                    .setParameterList("transactionCodeDbt", debitTransCodes)
                    .setParameter("startDate", sDate)
                    .setParameter("endDate", eDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return settlementList;
    }

    private List<BatchPosting> findBatchPostings(String referenceNo) {
        List<BatchPosting> batchPostings = new ArrayList<>();
        try {
            // find credit entries
            String sql1 = "from Posting as ps where ps.referenceNo = :referenceNo and ps.amount > :amount and ps.creditDebit = :creditDebit";
            List<Posting> creditEntries = (List<Posting>) sessionFactory.getCurrentSession().createQuery(sql1)
                    .setParameter("referenceNo", referenceNo)
                    .setParameter("amount", BigDecimal.ZERO)
                    .setParameter("creditDebit", 1).list();
            // find debit entries
            String sql2 = "from Posting as ps where ps.referenceNo = :referenceNo and ps.amount > :amount and ps.creditDebit = :creditDebit";
            List<Posting> debtiEntries = (List<Posting>) sessionFactory.getCurrentSession().createQuery(sql2)
                    .setParameter("referenceNo", referenceNo)
                    .setParameter("amount", BigDecimal.ZERO)
                    .setParameter("creditDebit", 2).list();
            if (creditEntries.size() == debtiEntries.size()) {
                for (int i = 0; i < creditEntries.size(); i++) {
                    BatchPosting bp = new BatchPosting();
                    Posting creditEntry = creditEntries.get(i);
                    Posting debtitEntry = debtiEntries.get(i);
                    if (creditEntry.getAccountNo().startsWith("IFD")) {
                        PostingContraCode pcc = getPostingContraCode(debtitEntry.getAccountNo());
                        if (pcc != null) {
                            bp.setAccountNo(creditEntry.getAccountNo());
                            bp.setIsDebit(0);
                            bp.setModule("AR");
                            bp.setTransactionCode(pcc.getGlContraCode());
                            bp.setDescription(pcc.getDescription());
                            bp.setGlContraCode("");
                            bp.setAmountExcl(creditEntry.getAmount().doubleValue());
                            bp.setAmountIncl(creditEntry.getAmount().doubleValue());
                            bp.setRefference(creditEntry.getReferenceNo());
                            batchPostings.add(bp);
                        }
                    } else if (debtitEntry.getAccountNo().startsWith("IFD")) {
                        PostingContraCode pcc = getPostingContraCode(creditEntry.getAccountNo());
                        if (pcc != null) {
                            bp.setAccountNo(debtitEntry.getAccountNo());
                            bp.setIsDebit(1);
                            bp.setModule("AR");
                            bp.setTransactionCode(pcc.getGlContraCode());
                            bp.setDescription(pcc.getDescription());
                            bp.setGlContraCode("");
                            bp.setAmountExcl(debtitEntry.getAmount().doubleValue());
                            bp.setAmountIncl(debtitEntry.getAmount().doubleValue());
                            bp.setRefference(debtitEntry.getReferenceNo());
                            batchPostings.add(bp);
                            if (pcc.getGlContraCode().equals("EP-INSURANCE.REC")) {
                                BatchPosting bpp = new BatchPosting();
                                bpp.setAccountNo("J020");
                                bpp.setIsDebit(0);
                                bpp.setModule("AP");
                                bpp.setTransactionCode("EP-INSURANCE.PAY");
                                bpp.setDescription(pcc.getDescription());
                                bpp.setGlContraCode("");
                                bpp.setAmountExcl(debtitEntry.getAmount().doubleValue());
                                bpp.setAmountIncl(debtitEntry.getAmount().doubleValue());
                                bpp.setRefference(debtitEntry.getReferenceNo());
                                batchPostings.add(bpp);
                            } else if (pcc.getGlContraCode().equals("EP-REGISTRATION.REC")) {
                                BatchPosting bpp = new BatchPosting();
                                bpp.setAccountNo("RMV0001");
                                bpp.setIsDebit(0);
                                bpp.setModule("AP");
                                bpp.setTransactionCode("EP-REGISTRATION.PAY");
                                bpp.setDescription(pcc.getDescription());
                                bpp.setGlContraCode("");
                                bpp.setAmountExcl(debtitEntry.getAmount().doubleValue());
                                bpp.setAmountIncl(debtitEntry.getAmount().doubleValue());
                                bpp.setRefference(debtitEntry.getReferenceNo());
                                batchPostings.add(bpp);
                            }
                        }
                    }
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return batchPostings;
    }

    private boolean batchPostingToCsv(Date sDate, Date eDate, String srcFilePath) {
        boolean isExport = false;
        DateFormat dtf = new SimpleDateFormat("yyyyMMdd");
        List<BatchPosting> batchPostingsToReport = new ArrayList<>();
        List<PostingDownload> saveList = new ArrayList<>();
        try {
            // create directories for (2W,MO)
            String srcFilePath1 = srcFilePath + "\\" + "2W";
            String srcFilePath2 = srcFilePath + "\\" + "MO";
            File directory1 = new File(srcFilePath1);
            File directory2 = new File(srcFilePath2);
            if (directory1.mkdir() && directory2.mkdir()) {
                // find issue loans
                List<LoanHeaderDetails> findIssueLoans = findIssueLoans();
                for (LoanHeaderDetails loan : findIssueLoans) {
                    // find settlements - payments
                    Integer loanId = loan.getLoanId();
                    List<SettlementMain> settlementList = findSettlements(loanId, sDate, eDate);
                    String referenceNo = "";
                    String datePart = "";
                    String orderNo = "";
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanId);

                    if (lpvd != null) {
                        String agreementNo = "|" + loan.getLoanAgreementNo();
                        String chassisNo = "";
                        if (lpvd.getVehiclChassisNo() != null) {
                            chassisNo = "|" + lpvd.getVehiclChassisNo();
                        }
                        orderNo = loanId + agreementNo + chassisNo;
                    }

                    for (SettlementMain sm : settlementList) {
                        Integer settlementId = sm.getSettlementId();
                        Date systemDate = sm.getSystemDate();
                        if (systemDate != null) {
                            datePart = dtf.format(systemDate);
                        }
                        referenceNo = datePart + loanId + "/" + settlementId;
                        // find batch postings with given refference no
                        List<BatchPosting> batchPostings = findBatchPostings(referenceNo);
                        for (BatchPosting bp : batchPostings) {
                            bp.setOrderNumber("");
                            bp.setProjectCode("BU133");
                            bp.setTransactionDate(systemDate);
                            batchPostingsToReport.add(bp);
                            // add to posting download
                            PostingDownload pd = new PostingDownload();
                            pd.setDownloadDate(systemDate);
                            pd.setLoanId(loanId);
                            pd.setReferenceNo(referenceNo);
                            bp.setOrderNumber(orderNo);
                            saveList.add(pd);
                        }
                    }
                    // generate report (used Jasper Reports API 4.7.0)
                    if (!batchPostingsToReport.isEmpty()) {
                        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/batchPosting/batchPosting.jrxml");
                        JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, new JRBeanCollectionDataSource(batchPostingsToReport));
                        // export report to csv
                        if (jasperPrint != null) {
                            LoanHeaderDetails lhd = loanDao.findLoanHeaderDetails(loanId);
                            DebtorHeaderDetails debtor = lhd.getDebtorHeaderDetails();
                            MBranch branch = masterDataService.findBranch(lhd.getBranchId());
                            if (branch != null) {
                                String branchCode = branch.getBranchCode();
                                if (!branchCode.isEmpty()) {
                                    String csvFilePath = "";
                                    if (branchCode.endsWith("2W")) {
                                        csvFilePath = srcFilePath1 + "\\" + debtor.getDebtorAccountNo() + "-" + loanId + ".csv";
                                    } else {
                                        csvFilePath = srcFilePath2 + "\\" + debtor.getDebtorAccountNo() + "-" + loanId + ".csv";
                                    }
                                    if (!csvFilePath.isEmpty()) {
                                        File csvFile = new File(csvFilePath);
                                        JRCsvExporter csvExporter = new JRCsvExporter();
                                        jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                                        csvExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                                        csvExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, new FileOutputStream(csvFile));
                                        csvExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                                        csvExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                                        csvExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                                        csvExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                                        csvExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                                        csvExporter.exportReport();
                                        // save to posting download table
                                        savePostingDownload(saveList);
                                        // clear collections
                                        batchPostingsToReport.clear();
                                        saveList.clear();
                                        isExport = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (JRException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return isExport;
    }

    private boolean savePostingDownload(List<PostingDownload> postingDownloads) {
        boolean sucess = false;
        Session session = null;
        Transaction tx = null;
        Date actionTime = Calendar.getInstance().getTime();
        int userId = userService.findByUserName();
        int branchId = getUserLogedBranch(userId);
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            for (int i = 0; i < postingDownloads.size(); i++) {
                PostingDownload pd = postingDownloads.get(i);
                pd.setActionTime(actionTime);
                pd.setBranchId(branchId);
                pd.setDownloadCount(0);
                pd.setUserId(userId);
                session.save(pd);
                if (i % 20 == 0) { //20(batch-size),Release memory after batch inserts.
                    session.flush();
                    session.clear();
                }
            }
            tx.commit();
            sucess = true;
        } catch (HibernateException e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return sucess;
    }

    private boolean updatePostingDownload(Date systemDate) {
        boolean sucess = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql = "update PostingDownload ps set ps.downloadCount = ps.downloadCount + 1 where ps.downloadDate = :downloadDate";
            session.createQuery(sql).setParameter("downloadDate", systemDate).executeUpdate();
            tx.commit();
            sucess = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return sucess;
    }

    @Override
    public List<StlmntReturnList> getPaymentList(String sDate, String eDate, int userBranchId) {
        List<SettlementPayments> obj;
        List<StlmntReturnList> returnList = new ArrayList();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String sdate = sdf.format(systemDate);
        try {
            String sql1 = "from SettlementPayments where systemDate between '" + sDate + "' and '" + eDate + "' and branchId = '" + userBranchId + "' ";
            obj = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql1).list();

            for (SettlementPayments ob : obj) {
                StlmntReturnList stlnmt = new StlmntReturnList();
                stlnmt.setAccountNo(ob.getAccountNo());
                String sql2 = " from LoanHeaderDetails where loanId = '" + ob.getLoanId() + "'";
                LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                stlnmt.setAgreementNo(loan.getLoanAgreementNo());
                stlnmt.setDebtAccountNo(loan.getDebtorHeaderDetails().getDebtorAccountNo());
                stlnmt.setAmountText(ob.getAmountText());
                stlnmt.setBranchId(ob.getBranchId());
                stlnmt.setActionDate(ob.getActionDate());
                stlnmt.setChequeId(ob.getChequeId());
                stlnmt.setLoanId(ob.getLoanId());
                String sql3 = " from SettlementPaymentTypes where paymentTypeId = '" + ob.getPaymentType() + "'";
                SettlementPaymentTypes peyType = (SettlementPaymentTypes) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                stlnmt.setPayType(peyType.getPaymentType());
                stlnmt.setPaymentDetails(ob.getPaymentDetails());
                stlnmt.setReceiptNo(ob.getReceiptNo());
                stlnmt.setRemark(ob.getRemark());
                stlnmt.setSystemDate(ob.getSystemDate());
                stlnmt.setTransactionId(ob.getTransactionId());
                stlnmt.setTransactionNo(ob.getTransactionNo());
                stlnmt.setUserId(ob.getUserId());
                stlnmt.setPaymentAmount(ob.getPaymentAmount());
                LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(ob.getLoanId());
                if (lpvd != null) {
                    stlnmt.setVehicleNo(lpvd.getVehicleRegNo());
                }
                returnList.add(stlnmt);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return returnList;
    }

    private PostingContraCode getPostingContraCode(String accountNo) {
        PostingContraCode pcc = null;
        try {
            String sql = "from PostingContraCode where accountNo = :accountNo";
            pcc = (PostingContraCode) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("accountNo", accountNo).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return pcc;
    }

    private List<BatchCustomer> getBatchCustomers() {
        List<BatchCustomer> batchCustomers = new ArrayList<>();
        try {
            // find debtors
            List<DebtorHeaderDetails> debtors = debtorDao.findDebtors(true, false);
            String[] phyAddress = new String[]{"", "", "", "", ""};
            String[] postAddress = new String[]{"", "", "", "", ""};
            for (DebtorHeaderDetails debtor : debtors) {
                // split address into tokens
                String personalAddress = debtor.getDebtorPersonalAddress();
                if (!personalAddress.equals("")) {
                    String[] tokens = personalAddress.split(",");
                    System.arraycopy(tokens, 0, phyAddress, 0, tokens.length);
                    System.arraycopy(tokens, 0, postAddress, 0, tokens.length);
                }
                BatchCustomer batchCustomer = new BatchCustomer();
                batchCustomer.setId(debtor.getDebtorId());
                batchCustomer.setAccountNo(debtor.getDebtorAccountNo());
                batchCustomer.setCustomerName(debtor.getDebtorName());
                batchCustomer.setCustomerTitle("");
                batchCustomer.setCustomerIni("");
                batchCustomer.setContactPerson(debtor.getNameWithInitial());
                batchCustomer.setPhysicalAddress1(phyAddress[0]);
                batchCustomer.setPhysicalAddress2(phyAddress[1]);
                batchCustomer.setPhysicalAddress3(phyAddress[2]);
                batchCustomer.setPhysicalAddress4(phyAddress[3]);
                batchCustomer.setPhysicalAddress5(phyAddress[4]);
                batchCustomer.setPhysicalPC("");
                batchCustomer.setAddressee("");
                batchCustomer.setPostalAddress1(postAddress[0]);
                batchCustomer.setPostalAddress2(postAddress[1]);
                batchCustomer.setPostalAddress3(postAddress[2]);
                batchCustomer.setPostalAddress4(postAddress[3]);
                batchCustomer.setPostalAddress5(postAddress[4]);
                batchCustomer.setPostPC("");
                batchCustomer.setDeliveredTo("");
                batchCustomer.setTelephone1(debtor.getDebtorTelephoneHome());
                batchCustomer.setTelephone2(debtor.getDebtorTelephoneMobile());
                batchCustomer.setFax1(debtor.getDebtorFax());
                batchCustomer.setFax2("");
                batchCustomer.setiClassId("VEHICLE-IFL"); // not change
                batchCustomer.setAccountTerms(0); // not change
                batchCustomer.setCt(1); // not change
                batchCustomer.setTaxNumber("");
                batchCustomer.setRegistration("");
                batchCustomer.setiAreasID("");
                batchCustomer.setCreditLimit(0); // not change
                batchCustomer.setInterestRate(0); // not change
                batchCustomer.setDiscount(0); // not change
                batchCustomer.setOnHold(0); // not change
                batchCustomer.setBfOpenType(0); // not change
                batchCustomer.setEmail("");
                batchCustomer.setBankLink("");
                batchCustomer.setBranchCode("");
                batchCustomer.setBankAccNum("");
                batchCustomer.setBankAccType("");
                batchCustomer.setAutoDisc(0); // not change
                batchCustomer.setDiscMtrxRow(0); // not change
                batchCustomer.setCheckTerms(1); // not change
                batchCustomer.setUseEmial(0); // not change
                batchCustomer.setExportDate(Calendar.getInstance().getTime());
                batchCustomer.setcAccDescription("");
                batchCustomer.setcWebPage("");
                batchCustomer.setcBankRefNr("");
                batchCustomer.setiCurrencyId(0); // not change
                batchCustomer.setbStatPrint(1); // not change
                batchCustomer.setbStatEmail(0); // not change
                batchCustomer.setcStatEmailPass("");
                batchCustomer.setbForCurAcc(0); // not change
                batchCustomer.setiSettlementTermsID("");
                batchCustomer.setiEUCountryID("");
                batchCustomer.setiDefTaxTypeID("");
                batchCustomer.setRepId("VRIFL"); // not change
                batchCustomer.setMainAccLink("");
                batchCustomer.setCashDebtor(0); // not change
                batchCustomer.setiIncidentTypeID("");
                batchCustomer.setiBusTypeID("");
                batchCustomer.setiBusClassID("");
                batchCustomer.setiAgentID("");
                batchCustomer.setbTaxPrompt(1); // not change
                batchCustomer.setiARPriceListNameId("Price List 1"); // not change
                batchCustomer.setbCODAccount(0); // not change
                batchCustomers.add(batchCustomer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return batchCustomers;
    }

    private void processBatchCustomers(String srcFilePath) {
        List<BatchCustomer> twList = new ArrayList<>();
        List<BatchCustomer> moList = new ArrayList<>();
        srcFilePath = srcFilePath + "\\Customer";
        File dir = new File(srcFilePath);
        String twCsvFilePath = srcFilePath + "\\TW.csv";
        String moCsvFilePath = srcFilePath + "\\MO.csv";
        try {
            if (dir.mkdir()) {
                List<BatchCustomer> batchCustomers = getBatchCustomers();
                if (!batchCustomers.isEmpty()) {
                    for (BatchCustomer batchCustomer : batchCustomers) {
                        DebtorHeaderDetails dhd = debtorDao.findDebtor(batchCustomer.getId());
                        if (dhd != null) {
                            MBranch branch = masterDataService.findBranch(dhd.getBranchId());
                            if (branch != null) {
                                String branchCode = branch.getBranchCode();
                                if (!branchCode.isEmpty()) {
                                    if (branchCode.endsWith("2W")) {
                                        twList.add(batchCustomer);
                                    } else {
                                        moList.add(batchCustomer);
                                    }
                                }
                            }
                        }
                    }
                    if (!twList.isEmpty()) {
                        batchCustomersToCsv(twList, twCsvFilePath);
                        setAsGenerate(twList);
                    }
                    if (!moList.isEmpty()) {
                        batchCustomersToCsv(moList, moCsvFilePath);
                        setAsGenerate(moList);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void batchCustomersToCsv(List<BatchCustomer> batchCustomers, String csvFilePath) {
        try {
            // generate report (used Jasper Reports API 4.7.0)
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/batchCustomers/batchCustomers.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, new JRBeanCollectionDataSource(batchCustomers));
            if (jasperPrint != null) {
                File csvFile = new File(csvFilePath);
                JRCsvExporter csvExporter = new JRCsvExporter();
                jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                csvExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                csvExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, new FileOutputStream(csvFile));
                csvExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                csvExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                csvExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                csvExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                csvExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                csvExporter.exportReport();
            }
        } catch (JRException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean setAsGenerate(List<BatchCustomer> batchCustomers) {
        boolean sucess = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            for (int i = 0; i < batchCustomers.size(); i++) {
                int debtorId = batchCustomers.get(i).getId();
                String sql = "from DebtorHeaderDetails where debtorId = " + debtorId;
                DebtorHeaderDetails dhd = (DebtorHeaderDetails) session.createQuery(sql).uniqueResult();
                if (dhd != null) {
                    dhd.setIsGenerate(true);
                    session.update(dhd);
                    if (i % 20 == 0) { //20(batch-size),Release memory after batch inserts.
                        session.flush();
                        session.clear();
                    }
                }
            }
            tx.commit();
            sucess = true;
        } catch (HibernateException e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return sucess;
    }

    @Override
    public boolean isLoanChequesExist(Date systemDate) {
        boolean exist = false;
        try {
            String sql = "from LoanCheques where chequeDate = :chequeDate and isPayment = :isPayment";
            List list = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("chequeDate", systemDate)
                    .setParameter("isPayment", false).list();
            if (!list.isEmpty()) {
                exist = true;
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return exist;
    }

    @Override
    public List<StlmntReturnList> getPaymentListForReceiptNo(String receiptNo, int userBranchId) {
        List<SettlementPayments> obj;
        List<StlmntReturnList> returnList = new ArrayList();
        try {
            String sql1 = "from SettlementPayments where receiptNo like '%" + receiptNo + "%' and branchId = '" + userBranchId + "' ";
            obj = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql1).list();
            for (SettlementPayments ob : obj) {
                StlmntReturnList stlnmt = new StlmntReturnList();
                stlnmt.setAccountNo(ob.getAccountNo());
                String sql2 = " from LoanHeaderDetails where loanId = '" + ob.getLoanId() + "'";
                LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                stlnmt.setAgreementNo(loan.getLoanAgreementNo());
                stlnmt.setDebtAccountNo(loan.getDebtorHeaderDetails().getDebtorAccountNo());
                stlnmt.setAmountText(ob.getAmountText());
                stlnmt.setBranchId(ob.getBranchId());
                stlnmt.setActionDate(ob.getActionDate());
                stlnmt.setChequeId(ob.getChequeId());
                stlnmt.setLoanId(ob.getLoanId());
                String sql3 = " from SettlementPaymentTypes where paymentTypeId = '" + ob.getPaymentType() + "'";
                SettlementPaymentTypes peyType = (SettlementPaymentTypes) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                stlnmt.setPayType(peyType.getPaymentType());
                stlnmt.setPaymentDetails(ob.getPaymentDetails());
                stlnmt.setReceiptNo(ob.getReceiptNo());
                stlnmt.setRemark(ob.getRemark());
                stlnmt.setSystemDate(ob.getSystemDate());
                stlnmt.setTransactionId(ob.getTransactionId());
                stlnmt.setTransactionNo(ob.getTransactionNo());
                stlnmt.setUserId(ob.getUserId());
                stlnmt.setPaymentAmount(ob.getPaymentAmount());
                returnList.add(stlnmt);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return returnList;
    }

    @Override
    public List<StlmntReturnList> getPaymentListForAgNo(String agNo, int userBranchId) {
        List<SettlementPayments> obj;
        List<StlmntReturnList> returnList = new ArrayList();
        try {
            String sql1 = "select loanId from LoanHeaderDetails where loanAgreementNo like '%" + agNo + "%'";
            List loanIds = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (!loanIds.isEmpty()) {
                String sql2 = "from SettlementPayments where loanId in (:loanId) and branchId = :branchId";
                obj = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql2)
                        .setParameterList("loanId", loanIds).setParameter("branchId", userBranchId).list();
                for (SettlementPayments ob : obj) {
                    StlmntReturnList stlnmt = new StlmntReturnList();
                    stlnmt.setAccountNo(ob.getAccountNo());
                    String sql3 = " from LoanHeaderDetails where loanId = '" + ob.getLoanId() + "'";
                    LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                    stlnmt.setAgreementNo(loan.getLoanAgreementNo());
                    stlnmt.setDebtAccountNo(loan.getDebtorHeaderDetails().getDebtorAccountNo());
                    stlnmt.setAmountText(ob.getAmountText());
                    stlnmt.setBranchId(ob.getBranchId());
                    stlnmt.setActionDate(ob.getActionDate());
                    stlnmt.setChequeId(ob.getChequeId());
                    stlnmt.setLoanId(ob.getLoanId());
                    String sql4 = " from SettlementPaymentTypes where paymentTypeId = '" + ob.getPaymentType() + "'";
                    SettlementPaymentTypes peyType = (SettlementPaymentTypes) sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
                    stlnmt.setPayType(peyType.getPaymentType());
                    stlnmt.setPaymentDetails(ob.getPaymentDetails());
                    stlnmt.setReceiptNo(ob.getReceiptNo());
                    stlnmt.setRemark(ob.getRemark());
                    stlnmt.setSystemDate(ob.getSystemDate());
                    stlnmt.setTransactionId(ob.getTransactionId());
                    stlnmt.setTransactionNo(ob.getTransactionNo());
                    stlnmt.setUserId(ob.getUserId());
                    stlnmt.setPaymentAmount(ob.getPaymentAmount());
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(ob.getLoanId());
                    if (lpvd != null) {
                        stlnmt.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    returnList.add(stlnmt);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return returnList;
    }

    @Override
    public List<StlmntReturnList> getPaymentListForVehNo(String vehNo, int userBranchId) {
        List<SettlementPayments> obj;
        List<StlmntReturnList> returnList = new ArrayList();
        try {
            String sql1 = "select loanId from LoanPropertyVehicleDetails where vehicleRegNo like '%" + vehNo + "%'";
            List loanIds = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (!loanIds.isEmpty()) {
                String sql2 = "from SettlementPayments where loanId in (:loanId) and branchId = :branchId";
                obj = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql2)
                        .setParameterList("loanId", loanIds).setParameter("branchId", userBranchId).list();
                for (SettlementPayments ob : obj) {
                    StlmntReturnList stlnmt = new StlmntReturnList();
                    stlnmt.setAccountNo(ob.getAccountNo());
                    String sql3 = " from LoanHeaderDetails where loanId = '" + ob.getLoanId() + "'";
                    LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                    stlnmt.setAgreementNo(loan.getLoanAgreementNo());
                    stlnmt.setDebtAccountNo(loan.getDebtorHeaderDetails().getDebtorAccountNo());
                    stlnmt.setAmountText(ob.getAmountText());
                    stlnmt.setBranchId(ob.getBranchId());
                    stlnmt.setActionDate(ob.getActionDate());
                    stlnmt.setChequeId(ob.getChequeId());
                    stlnmt.setLoanId(ob.getLoanId());
                    String sql4 = " from SettlementPaymentTypes where paymentTypeId = '" + ob.getPaymentType() + "'";
                    SettlementPaymentTypes peyType = (SettlementPaymentTypes) sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
                    stlnmt.setPayType(peyType.getPaymentType());
                    stlnmt.setPaymentDetails(ob.getPaymentDetails());
                    stlnmt.setReceiptNo(ob.getReceiptNo());
                    stlnmt.setRemark(ob.getRemark());
                    stlnmt.setSystemDate(ob.getSystemDate());
                    stlnmt.setTransactionId(ob.getTransactionId());
                    stlnmt.setTransactionNo(ob.getTransactionNo());
                    stlnmt.setUserId(ob.getUserId());
                    stlnmt.setPaymentAmount(ob.getPaymentAmount());
                    LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(ob.getLoanId());
                    if (lpvd != null) {
                        stlnmt.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    returnList.add(stlnmt);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return returnList;
    }

    @Override
    public List<StlmntReturnList> getPaymentListForPayMode(int payMode, int userBranchId) {
        List<SettlementPayments> obj;
        List<StlmntReturnList> returnList = new ArrayList();
        try {
            String sql1 = "from SettlementPayments where paymentType = " + payMode + " and branchId = '" + userBranchId + "' ";
            obj = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql1).list();
            for (SettlementPayments ob : obj) {
                StlmntReturnList stlnmt = new StlmntReturnList();
                stlnmt.setAccountNo(ob.getAccountNo());
                String sql2 = " from LoanHeaderDetails where loanId = '" + ob.getLoanId() + "'";
                LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                stlnmt.setAgreementNo(loan.getLoanAgreementNo());
                stlnmt.setDebtAccountNo(loan.getDebtorHeaderDetails().getDebtorAccountNo());
                stlnmt.setAmountText(ob.getAmountText());
                stlnmt.setBranchId(ob.getBranchId());
                stlnmt.setActionDate(ob.getActionDate());
                stlnmt.setChequeId(ob.getChequeId());
                stlnmt.setLoanId(ob.getLoanId());
                String sql3 = " from SettlementPaymentTypes where paymentTypeId = '" + ob.getPaymentType() + "'";
                SettlementPaymentTypes peyType = (SettlementPaymentTypes) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                stlnmt.setPayType(peyType.getPaymentType());
                stlnmt.setPaymentDetails(ob.getPaymentDetails());
                stlnmt.setReceiptNo(ob.getReceiptNo());
                stlnmt.setRemark(ob.getRemark());
                stlnmt.setSystemDate(ob.getSystemDate());
                stlnmt.setTransactionId(ob.getTransactionId());
                stlnmt.setTransactionNo(ob.getTransactionNo());
                stlnmt.setUserId(ob.getUserId());
                stlnmt.setPaymentAmount(ob.getPaymentAmount());
                LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(ob.getLoanId());
                if (lpvd != null) {
                    stlnmt.setVehicleNo(lpvd.getVehicleRegNo());
                }
                returnList.add(stlnmt);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return returnList;
    }

    public int findSubTaxDetails(String accNo) {
        int taxId = 0;
        try {
            MSubTaxCharges charges = (MSubTaxCharges) sessionFactory.getCurrentSession().createCriteria(MSubTaxCharges.class)
                    .add(Restrictions.eq("subTaxAccountNo", accNo)).uniqueResult();
            taxId = charges.getSubTaxId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taxId;
    }

    @Override
    public RebateQuotation getRebateQuotation(int loanId) {
        RebateQuotation rebateQuotation = new RebateQuotation();
        double totalBalance = 0.00;
        double dateBalance = 0.00;
        double futureCapital = 0.00;
        double futureInterest = 0.00;
        String rebateValiddate = "";
        Calendar cal = Calendar.getInstance();
        LoanHeaderDetails lhd = loanDao.findLoanHeaderDetails(loanId);
        if (lhd != null) {
            DebtorHeaderDetails debtor = lhd.getDebtorHeaderDetails();
            if (debtor != null) {
                rebateQuotation.setDebtorName(debtor.getNameWithInitial());
            }
            rebateQuotation.setEpfNo("");
            rebateQuotation.setFacilityNo(lhd.getLoanAgreementNo());
        }
        LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanId);
        if (lpvd != null) {
            rebateQuotation.setVehicleNo(lpvd.getVehicleRegNo());
        }
        RebateDetail rd = findRebateDetails(loanId);
        if (rd != null) {
            totalBalance = rd.getTotalBalance();
            futureCapital = rd.getRemainingBalance();
            futureInterest = rd.getRemainingInterest();
            dateBalance = totalBalance - (futureCapital + futureInterest);
            cal.setTime(rd.getSystemDate());
            cal.add(Calendar.MONTH, +1);
            Date valiDate = cal.getTime();
            rebateValiddate = dateFormat.format(valiDate);
            rebateQuotation.setLoanBalance(totalBalance);
            rebateQuotation.setDateBalance(dateBalance);
            rebateQuotation.setFutureCapital(futureCapital);
            rebateQuotation.setFutureInterest(futureInterest);
            rebateQuotation.setRebateRate(rd.getDiscountRate());
            rebateQuotation.setDiscount(rd.getDiscountAmount());
            rebateQuotation.setRebateAmount(rd.getRebateAmount());
            rebateQuotation.setRebateAmountWithTax(rd.getRebateAmount());
            rebateQuotation.setRentalOutstanding(0.00);
            rebateQuotation.setInterestCharge(0.00);
            rebateQuotation.setRebateInterestCharge(0.00);
            rebateQuotation.setOdiWithTax(0.00);
            rebateQuotation.setTransferFee(0.00);
            rebateQuotation.setTransferFeeWithTax(0.00);
            rebateQuotation.setInsuranceCharges(0.00);
            rebateQuotation.setSeizingCharges(0.00);
            rebateQuotation.setParkingCharges(0.00);
            rebateQuotation.setValuationCharges(0.00);
            rebateQuotation.setLegalCharges(0.00);
            rebateQuotation.setOtherCharges(rd.getCharge());
            rebateQuotation.setRepaymentBalance(0.00);
            rebateQuotation.setPpWithTaxt(0.00);
            rebateQuotation.setOvpBalance(rd.getTotalReceivable());
            rebateQuotation.setSecDeposit(0.00);
            rebateQuotation.setForegoneDeposit(rd.getTotalReceivable());
            rebateQuotation.setTotRecoverable(rd.getTotalReceivable());
            rebateQuotation.setRebateValidDate(rebateValiddate);
        }
        return rebateQuotation;
    }

    @Override
    public boolean addSettlementTransfer(SettlementTransfer st) {
        boolean flag = false;
        Session session = null;
        Transaction tx = null;
        Date actionTime = Calendar.getInstance().getTime();
        int userId = userService.findByUserName();
        int branchId = getUserLogedBranch(userId);
        Date systemDate = getSystemDate(branchId);
        int debtroId = 0;
        LoanHeaderDetails lhd = loanDao.findLoanHeaderDetails(st.getTransferId());
        if (lhd != null) {
            debtroId = lhd.getDebtorHeaderDetails().getDebtorId();
        }
        String maxTranferNo = findMaxTransferNo();
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            // add to settlement main (over payment deduct from transfer)
            SettlementMain sm1 = new SettlementMain();
            sm1.setActionDate(actionTime);
            sm1.setBranchId(branchId);
            sm1.setCrdtAmount(-st.getTransferAmount());
            sm1.setDebtorId(debtroId);
            sm1.setDescription("Over Payment Transfer");
            sm1.setIsPosting(true);
            sm1.setLoanId(st.getTransferId());
            sm1.setSystemDate(systemDate);
            sm1.setTransactionCodeCrdt("OVP");
            sm1.setTransactionNoCrdt(maxTranferNo);
            sm1.setUserId(userId);
            session.save(sm1);

            // add to settlement main (over payment add to transferee)
            SettlementMain sm2 = new SettlementMain();
            sm2.setActionDate(actionTime);
            sm2.setBranchId(branchId);
            sm2.setCrdtAmount(st.getTransferAmount());
            sm2.setDebtorId(debtroId);
            sm2.setDescription("Over Payment Transferee");
            sm2.setIsPosting(true);
            sm2.setLoanId(st.getTransfereeId());
            sm2.setSystemDate(systemDate);
            sm2.setTransactionCodeCrdt("OVP");
            sm2.setTransactionNoCrdt(maxTranferNo);
            sm2.setUserId(userId);
            session.save(sm2);

            // add to settlement transfer
            st.setActionTime(actionTime);
            st.setBranchId(branchId);
            st.setSystemDate(systemDate);
            st.setTransactionNo(maxTranferNo);
            st.setTransferDate(systemDate);
            st.setUserId(userId);
            session.save(st);

            tx.commit();
            flag = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flag;
    }

    private String findMaxTransferNo() {
        String maxTransferNo = "1";
        try {
            String sql = "select transactionNo from SettlementTransfer order by id desc";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();
            int id = 0;
            if (obj != null) {
                maxTransferNo = obj.toString();
                id = Integer.parseInt(maxTransferNo.substring(TRANSFER_CODE.length()));
                id++;
                maxTransferNo = TRANSFER_CODE + id;
            } else {
                maxTransferNo = TRANSFER_CODE + maxTransferNo;
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return maxTransferNo;
    }

    @Override
    public List<SettlementMain> loadRefundTable(String sDate, String eDate, int userLogBranchId) {
        List<SettlementMain> paylist = null;
        List<SettlementMain> list2 = new ArrayList<>();
        try {
            String sql = "FROM SettlementMain WHERE (transactionCodeDbt='OVP' OR transactionCodeCrdt='OVP') GROUP BY loanId HAVING IFNULL(SUM(crdtAmount),0)>IFNULL(SUM(dbtAmount),0) AND actionDate BETWEEN '" + sDate + "' AND '" + eDate + "'";
            //paylist = (List<SettlementMain>) sessionFactory.getCurrentSession().createQuery(sql).list();
            String sql2 = "from LoanHeaderDetails where loanId in (:loanId) ";
            paylist = sessionFactory.getCurrentSession().createQuery(sql).list();

            for (SettlementMain sm : paylist) {
                LoanHeaderDetails loanHeaderDetails = loanDao.findLoanHeaderDetails(sm.getLoanId());
                sm.setLoanHeaderDetails(loanHeaderDetails);
                Double ovpAmount = getOverpayAmt(sm.getLoanId());
                sm.setOverPaymentAmt(ovpAmount);
                list2.add(sm);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list2;
    }

    @Override
    public List<SettlementMain> loadRefundTablebyAgNo(String AgreementNo, int userLogBranchId) {
        List<SettlementMain> list1 = new ArrayList<>();
        try {
            String sql = "FROM SettlementMain WHERE loanId IN(SELECT loanId FROM LoanHeaderDetails WHERE loanAgreementNo LIKE '%" + AgreementNo + "%' ) AND(transactionCodeDbt='OVP' OR transactionCodeCrdt='OVP') GROUP BY loanId HAVING IFNULL(SUM(crdtAmount),0)>IFNULL(SUM(dbtAmount),0)";
            List<SettlementMain> list2 = sessionFactory.getCurrentSession().createQuery(sql).list();
            for (SettlementMain sm : list2) {
                LoanHeaderDetails loanHeaderDetails = loanDao.findLoanHeaderDetails(sm.getLoanId());
                sm.setLoanHeaderDetails(loanHeaderDetails);
                Double ovpAmount = getOverpayAmt(sm.getLoanId());
                sm.setOverPaymentAmt(ovpAmount);
                list1.add(sm);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list1;
    }

    @Override
    public List<SettlementMain> loadRefundTablebyVehNo(String vehicleNo, int userLogBranchId) {
        List<SettlementMain> list1 = new ArrayList<>();
        try {
            String sql1 = "SELECT loanId FROM LoanPropertyVehicleDetails WHERE vehicleRegNo LIKE '%" + vehicleNo + "%'";
            List loanIds = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (!loanIds.isEmpty()) {
                String sql2 = "FROM SettlementMain WHERE loanId IN(:loanIds) AND(transactionCodeDbt='OVP' OR transactionCodeCrdt='OVP') GROUP BY loanId HAVING IFNULL(SUM(crdtAmount),0)>IFNULL(SUM(dbtAmount),0)";
                List<SettlementMain> list2 = sessionFactory.getCurrentSession().createQuery(sql2).
                        setParameterList("loanIds", loanIds).list();
                for (SettlementMain sm : list2) {
                    LoanHeaderDetails loanHeaderDetails = loanDao.findLoanHeaderDetails(sm.getLoanId());
                    sm.setLoanHeaderDetails(loanHeaderDetails);
                    Double ovpAmount = getOverpayAmt(sm.getLoanId());
                    sm.setOverPaymentAmt(ovpAmount);
                    list1.add(sm);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list1;
    }

    @Override
    public List<SettlementMain> loadRefundTablebyCustomer(String customerName, int userLogBranchId) {
        List<SettlementMain> list1 = new ArrayList<>();
        try {
            String sql1 = "SELECT loanId FROM LoanHeaderDetails WHERE debtorHeaderDetails.debtorName LIKE '%" + customerName + "%'";
            List loanIds = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (!loanIds.isEmpty()) {
                String sql2 = "FROM SettlementMain WHERE loanId IN(:loanIds) AND(transactionCodeDbt='OVP' OR transactionCodeCrdt='OVP') GROUP BY loanId HAVING IFNULL(SUM(crdtAmount),0)>IFNULL(SUM(dbtAmount),0)";
                List<SettlementMain> list2 = sessionFactory.getCurrentSession().createQuery(sql2).
                        setParameterList("loanIds", loanIds).list();
                for (SettlementMain sm : list2) {
                    LoanHeaderDetails loanHeaderDetails = loanDao.findLoanHeaderDetails(sm.getLoanId());
                    sm.setLoanHeaderDetails(loanHeaderDetails);
                    Double ovpAmount = getOverpayAmt(sm.getLoanId());
                    sm.setOverPaymentAmt(ovpAmount);
                    list1.add(sm);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list1;
    }

    @Override
    public SettlementMain getSettlementLoanheaderDebtorDetailsByLoanID(int loanId) {
        SettlementMain sm = new SettlementMain();
        try {
            String sql = "FROM SettlementMain WHERE loanId=" + loanId + " AND (transactionCodeDbt='OVP' OR transactionCodeCrdt='OVP') GROUP BY loanId HAVING IFNULL(SUM(crdtAmount),0)>IFNULL(SUM(dbtAmount),0)";
            sm = (SettlementMain) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            LoanHeaderDetails loanHeaderDetails = loanDao.findLoanHeaderDetails(loanId);
            sm.setLoanHeaderDetails(loanHeaderDetails);
            Double ovpAmount = getOverpayAmt(sm.getLoanId());
            sm.setOverPaymentAmt(ovpAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sm;
    }

    @Override
    public Double getOverpayAmt(int loanid) {
        Double amount = 0.00;
        try {
            String sql = "SELECT IFNULL(SUM(crdtAmount),0)-IFNULL(SUM(dbtAmount),0) FROM SettlementMain WHERE loanId =" + loanid + " AND (transactionCodeCrdt='OVP' OR transactionCodeDbt='OVP' )";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            amount = Double.parseDouble(obj.toString());
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return amount;
    }

    @Override
    public boolean saveRefund(SettlementRefund sr) {
        boolean result = false;
        Session session = null;
        Transaction tx = null;
        try {
            sr.setUserId(userService.findByUserName());
            Date date = new Date();
            sr.setActionDate(date);
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.save(sr);
            tx.commit();
            result = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    @Override
    public boolean updateSettlementMainonRefund(int loanId, Double refundAmount, String receiptNo, Date sysDate) {
        boolean result = false;
        try {
            LoanHeaderDetails loanData = loanDao.findLoanHeaderDetails(loanId);
            String debtorAcc = loanData.getDebtorHeaderDetails().getDebtorAccountNo();
            SettlementMain sm = new SettlementMain();
            int userLogBranchId = settlmentService.getUserLogedBranch(userService.findByUserName());
            sm.setUserId(userService.findByUserName());
            sm.setBranchId(userLogBranchId);
            sm.setDbtAmount(refundAmount);
            sm.setSystemDate(sysDate);
            sm.setIsPosting(false);
            sm.setTransactionCodeDbt("OVP");
            sm.setDebtorId(loanData.getDebtorHeaderDetails().getDebtorId());
            sm.setLoanId(loanId);
            //sm.setReceiptNo(receiptNo);
            Session session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.save(sm);
            int settId = sm.getSettlementId();
            String postingRefNo = loanDao.generateReferencNo(loanId, userLogBranchId);
            postingRefNo = postingRefNo + "/" + settId;
            System.out.println(postingRefNo);
            System.out.println(settId);
            System.out.println(debtorAcc);
            String sql = "select accountNo from Posting where referenceNo like '%/" + sm.getSettlementId() + "' group by accountNo";
//            loanDao.addToPosting(odiAcc, postingRefNo, new BigDecimal(refundAmount), userLogBranchId, 1);
            List<String> accNoList = (List<String>) sessionFactory.getCurrentSession().createQuery(sql).list();
            if (accNoList.isEmpty()) {
                System.out.println("empty list");
            }
            //System.out.println("Index0 "+accNoList.get(1));
            for (String accNo : accNoList) {
                if (accNo.equals(debtorAcc)) {
                    loanDao.addToPosting(accNo, postingRefNo, new BigDecimal(refundAmount), userLogBranchId, 2);
                    System.out.println(accNo);
                } else {
                    System.out.println("true");
                    loanDao.addToPosting(accNo, postingRefNo, new BigDecimal(refundAmount), userLogBranchId, 1);
                    System.out.println(accNo);
                }
            }
            System.out.println("Executed");
            tx.commit();
            session.close();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

//  ======================================================================
//  ================================  ODI ================================
//  ======================================================================
    @Override
    public File batchPostingODIToZip(Date startDate, Date endDate, int vType) {
        File zipFile = null;
        DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
        String sDate = dtf.format(startDate);
        String eDate = dtf.format(endDate);

        String vehicleType = "";
        if (vType == 1) {
            vehicleType = "_MO";
        } else if (vType == 2) {
            vehicleType = "_2W";
        } else if (vType == 3) {
            vehicleType = "_MO_2W";
        }

        String srcFile = "C:\\tmpFile\\Posting_ODI_" + sDate + "_To_" + eDate + vehicleType;
        String outputZipFile = "C:\\tmpFile\\Posting_ODI_" + sDate + "_To_" + eDate + vehicleType + ".zip";

        File reportFile = new File(srcFile);
        if (!reportFile.exists()) {
            if (reportFile.mkdir()) {
                if (batchPostingODIToCsv(startDate, endDate, srcFile, vType)) {
//                    processBatchCustomers(srcFile);
                    FileZip.zipIt(srcFile, outputZipFile);
                    zipFile = new File(outputZipFile);
                }
            }
        } else {
            zipFile = new File(outputZipFile);
        }
        return zipFile;
    }

    private boolean batchPostingODIToCsv(Date startDate, Date endDate, String srcFilePath, int vType) {
        boolean isExport = false;
//        DateFormat dtf = new SimpleDateFormat("yyyyMMdd");
        DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
        List<BatchPosting> batchPostingsODIToReport = new ArrayList<>();
        List<PostingDownload> saveList = new ArrayList<>();
        String lhdsList = "0,";
        String sDate = dtf.format(startDate);
        String eDate = dtf.format(endDate);

        String referenceNo = "";
        String datePart = "";
        String orderNo = "";
        try {
            String srcFilePath1 = srcFilePath + "\\" + "ODI";

            File directory = new File(srcFilePath1);
            if (directory.mkdir()) {
                List<LoanHeaderDetails> findIssueLoans = findIssueLoans();
                for (LoanHeaderDetails loan : findIssueLoans) {
                    Integer loanId = loan.getLoanId();
                    List<SettlementMain> settlementList = findODISettlements(loanId, sDate, eDate);

                    LoanHeaderDetails lhd = loanDao.findLoanHeaderDetails(loanId);
                    lhdsList += lhd.getLoanId() + ",";

                    for (SettlementMain sm : settlementList) {
                        Integer settlementId = sm.getSettlementId();
                        Date systemDate = sm.getSystemDate();
                        if (systemDate != null) {
                            datePart = dtf.format(systemDate);
                        }
                        referenceNo = datePart + loanId + "/" + settlementId;
                        // find batch postings with given refference no
                        List<BatchPosting> batchPostings = findBatchPostings(referenceNo);

                        for (BatchPosting bp : batchPostings) {

                            bp.setOrderNumber(orderNo);
                            bp.setProjectCode("BU133");
                            bp.setTransactionDate(systemDate);
                            if (!bp.getTransactionCode().equals("EP-ODI")) {
                                batchPostingsODIToReport.add(bp);
                            }
                            // add to posting download
                            PostingDownload pd = new PostingDownload();
                            pd.setDownloadDate(systemDate);
                            pd.setLoanId(loanId);
                            pd.setReferenceNo(referenceNo);
                            saveList.add(pd);
                        }
                    }

                }
            }
            List<BatchPosting> batchPostingsODI = findBatchPostingsODI(sDate, eDate, vType);
            for (BatchPosting bp : batchPostingsODI) {
                batchPostingsODIToReport.add(bp);
            }
            if (!batchPostingsODIToReport.isEmpty()) {
                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/batchPosting/batchPosting.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, new JRBeanCollectionDataSource(batchPostingsODIToReport));
                // export report to csv
                if (jasperPrint != null) {
                    lhdsList = lhdsList.substring(0, lhdsList.length() - 1);
                    List<LoanHeaderDetails> lhd = loanDao.findLoanHeaderDetailsList(lhdsList);
                    for (LoanHeaderDetails loanHd : lhd) {
                        DebtorHeaderDetails debtor = loanHd.getDebtorHeaderDetails();
                        String vehicleType = "";
                        switch (vType) {
                            case 1:
                                vehicleType = "_MO";
                                break;
                            case 2:
                                vehicleType = "_2W";
                                break;
                            case 3:
                                vehicleType = "_MO_2W";
                                break;
                            default:
                                break;
                        }
                        String csvFilePath = srcFilePath1 + "\\" + "ODI_Chargers_" + sDate + "_To_" + eDate + vehicleType + ".csv";
                        if (!csvFilePath.isEmpty()) {
                            File csvFile = new File(csvFilePath);
                            JRCsvExporter csvExporter = new JRCsvExporter();
                            jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                            csvExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                            csvExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, new FileOutputStream(csvFile));
                            csvExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                            csvExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                            csvExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                            csvExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                            csvExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                            csvExporter.exportReport();
                            // save to posting download table
//                            savePostingDownload(saveList);
                            // clear collections
                            batchPostingsODIToReport.clear();
                            saveList.clear();
                            isExport = true;
                        }
                        break;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isExport;
    }

    private List<SettlementMain> findODISettlements(Integer loanId, String startDate, String endDate) {
        Date sDate = null, eDate = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            sDate = formatter.parse(startDate);
            eDate = formatter.parse(endDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<SettlementMain> settlementList = null;
        List<String> creditTransCodes = Arrays.asList(new String[]{"ODI"});
        List<String> debitTransCodes = Arrays.asList(new String[]{"ODI"});
        try {
            String sql = "from SettlementMain where (transactionCodeCrdt in (:transactionCodeCrdt) or transactionCodeDbt in (:transactionCodeDbt)) and isPosting = :isPosting and systemDate BETWEEN :startDate AND :endDate and loanId = :loanId";
            settlementList = sessionFactory.getCurrentSession().createQuery(sql).setParameter("loanId", loanId)
                    .setParameter("isPosting", true)
                    .setParameterList("transactionCodeCrdt", creditTransCodes)
                    .setParameterList("transactionCodeDbt", debitTransCodes)
                    .setParameter("startDate", sDate)
                    .setParameter("endDate", eDate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return settlementList;
    }

    private List<BatchPosting> findBatchPostingsODI(String startDate, String endDate, int vType) {
        List<BatchPosting> batchPostingsOdi = new ArrayList<>();
        List<SettlementMain> odiDbtAmount = new ArrayList<>();
        Map<Integer, String> loanPropertyDt = new HashMap<>();
        Map<String, Integer> refNo = new HashMap<>();
        List<SettlementMain> odiWaveOffList = new ArrayList<>();
        DateFormat dtf = new SimpleDateFormat("yyyyMMdd");
        String loanIds = "0,", referenceNoList = "'";
        List<String> refList = new ArrayList<>();

        try {
            List<LoanHeaderDetails> findIssueLoans = findIssueLoans();
            for (LoanHeaderDetails loan : findIssueLoans) {
                Integer loanId = loan.getLoanId();
                String orderNo = "";
                LoanPropertyVehicleDetails lpvd = loanDao.findVehicleByLoanId(loanId);
                if (lpvd != null) {
                    String agreementNo = "|" + loan.getLoanAgreementNo();
                    String chassisNo = "";
                    if (lpvd.getVehiclChassisNo() != null) {
                        chassisNo = "|" + lpvd.getVehiclChassisNo();
                    }
                    orderNo = loanId + agreementNo + chassisNo;
                    loanPropertyDt.put(loanId, orderNo);
                }
                loanIds += loanId + ",";
            }
            loanIds = loanIds.substring(0, loanIds.length() - 1);

            String sql1 = "from SettlementMain WHERE loanId in(" + loanIds + ") and transactionCodeDbt like 'ODI' and description like 'ODI Charges' and systemDate >= '" + startDate + "' AND systemDate <= '" + endDate + "' ORDER BY loanId";
            odiDbtAmount = (List<SettlementMain>) sessionFactory.getCurrentSession().createQuery(sql1).list();

            String sql2 = "from SettlementMain WHERE loanId in(" + loanIds + ") and transactionCodeDbt like 'ODI' and description like 'ODI Adjustment' and systemDate  BETWEEN '" + startDate + "' AND '" + endDate + "' ORDER BY loanId";
            odiWaveOffList = (List<SettlementMain>) sessionFactory.getCurrentSession().createQuery(sql2).list();

            for (SettlementMain odiDbt : odiDbtAmount) {
                String datePart = null, referenceNo = null;
                Integer settlementId = odiDbt.getSettlementId();
                Date systemDate = odiDbt.getSystemDate();
                if (systemDate != null) {
                    datePart = dtf.format(systemDate);
                }
                referenceNo = datePart + odiDbt.getLoanId() + "/" + settlementId;
                refNo.put(referenceNo, odiDbt.getLoanId());
                referenceNoList += referenceNo + "','";
                refList.add(referenceNo);
            }

            for (SettlementMain odiWaveOff : odiWaveOffList) {
                String datePart = null, referenceNo = null;
                Integer settlementId = odiWaveOff.getSettlementId();
                Date systemDate = odiWaveOff.getSystemDate();
                if (systemDate != null) {
                    datePart = dtf.format(systemDate);
                }
                referenceNo = datePart + odiWaveOff.getLoanId() + "/" + settlementId;
                refNo.put(referenceNo, odiWaveOff.getLoanId());
                referenceNoList += referenceNo + "','";
                refList.add(referenceNo);
            }

//            referenceNoList = referenceNoList.substring(0, loanIds.length() - 2);
            String sqlCrd = "from Posting as ps where ps.referenceNo in (:referenceNo) and ps.amount > :amount and ps.creditDebit = :creditDebit and date BETWEEN '" + startDate + "' AND '" + endDate + "'";
            List<Posting> creditEntries = (List<Posting>) sessionFactory.getCurrentSession().createQuery(sqlCrd)
                    .setParameterList("referenceNo", refList)
                    .setParameter("amount", BigDecimal.ZERO)
                    .setParameter("creditDebit", 1).list();

            String sqlDbt = "from Posting as ps where ps.referenceNo in (:referenceNo) and ps.amount > :amount and ps.creditDebit  = :creditDebit and date BETWEEN '" + startDate + "' AND '" + endDate + "'";
            List<Posting> debtiEntries = (List<Posting>) sessionFactory.getCurrentSession().createQuery(sqlDbt)
                    .setParameterList("referenceNo", refList)
                    .setParameter("amount", BigDecimal.ZERO)
                    .setParameter("creditDebit", 2).list();

            for (int i = 0; i < debtiEntries.size(); i++) {
                Posting creditEntry = creditEntries.get(i);
                Posting debtitEntry = debtiEntries.get(i);
                if (creditEntry.getAccountNo().startsWith("IFD")) {
                    PostingContraCode pcc = getPostingContraCode(debtitEntry.getAccountNo());
                    if (pcc != null) {
                        BatchPosting bp = new BatchPosting();
                        String refNum = creditEntry.getReferenceNo();
                        String[] output = refNum.split("/");
                        String splitRefNo = output[0];
                        int c = Integer.parseInt(splitRefNo.substring(8));
                        for (Map.Entry<Integer, String> entry : loanPropertyDt.entrySet()) {
                            int key = entry.getKey();
                            String value = entry.getValue();

                            String[] outArr = value.split("/", 3);
                            String outArr1 = outArr[1];
                            String moOr2w = outArr1.substring(2, 4);
                            if (moOr2w.equalsIgnoreCase("MO") && (vType == 1 || vType == 3)) {
                                if (c == key) {
                                    bp.setAccountNo(creditEntry.getAccountNo());
                                    bp.setTransactionDate(debtitEntry.getDate());
                                    bp.setIsDebit(1);
                                    bp.setModule("AR");
                                    bp.setTransactionCode(pcc.getGlContraCode());
                                    bp.setDescription(pcc.getDescription());
                                    bp.setGlContraCode("");
                                    bp.setRefference(creditEntry.getReferenceNo());
                                    bp.setAmountExcl(Double.parseDouble(creditEntry.getCAmount().toString()));
                                    bp.setAmountIncl(Double.parseDouble(creditEntry.getCAmount().toString()));
                                    bp.setProjectCode("BU133");
                                    bp.setOrderNumber(value);
//                                    bp.setGlContraCode("MO");
                                    batchPostingsOdi.add(bp);
                                }
                            } else if (moOr2w.equalsIgnoreCase("2W") && (vType == 2 || vType == 3)) {
                                if (c == key) {
                                    bp.setAccountNo(creditEntry.getAccountNo());
                                    bp.setTransactionDate(debtitEntry.getDate());
                                    bp.setIsDebit(1);
                                    bp.setModule("AR");
                                    bp.setTransactionCode(pcc.getGlContraCode());
                                    bp.setDescription(pcc.getDescription());
                                    bp.setGlContraCode("");
                                    bp.setRefference(creditEntry.getReferenceNo());
                                    bp.setAmountExcl(Double.parseDouble(creditEntry.getCAmount().toString()));
                                    bp.setAmountIncl(Double.parseDouble(creditEntry.getCAmount().toString()));
                                    bp.setProjectCode("BU133");
                                    bp.setOrderNumber(value);
//                                    bp.setGlContraCode("MO");
                                    batchPostingsOdi.add(bp);
                                }
                            }
                        }
                    }
                } else if (debtitEntry.getAccountNo().startsWith("IFD")) {
                    PostingContraCode pcc = getPostingContraCode(creditEntry.getAccountNo());
                    if (pcc != null) {
                        BatchPosting bp = new BatchPosting();
                        String a = debtitEntry.getReferenceNo();
                        String[] output = a.split("/");
                        String b = output[0];
                        int c = Integer.parseInt(b.substring(8));
                        for (Map.Entry<Integer, String> entry : loanPropertyDt.entrySet()) {
                            int key = entry.getKey();
                            String value = entry.getValue();

                            String[] outArr = value.split("/", 3);
                            String outArr1 = outArr[1];
                            String moOr2w = outArr1.substring(2, 4);
                            if (moOr2w.equalsIgnoreCase("MO") && (vType == 1 || vType == 3)) {
                                if (c == key) {
                                    bp.setTransactionDate(debtitEntry.getDate());
                                    bp.setAccountNo(debtitEntry.getAccountNo());
                                    bp.setIsDebit(0);
                                    bp.setModule("AR");
                                    bp.setTransactionCode(pcc.getGlContraCode());
                                    bp.setDescription(pcc.getDescription());
                                    bp.setGlContraCode("");
                                    bp.setRefference(debtitEntry.getReferenceNo());
                                    bp.setAmountExcl(Double.parseDouble(debtitEntry.getDAmount().toString()));
                                    bp.setAmountIncl(Double.parseDouble(debtitEntry.getDAmount().toString()));
                                    bp.setProjectCode("BU133");
                                    bp.setOrderNumber(value);
//                                    bp.setGlContraCode("MO");
                                    batchPostingsOdi.add(bp);
                                }
                            } else if (moOr2w.equalsIgnoreCase("2W") && (vType == 2 || vType == 3)) {
                                if (c == key) {
                                    bp.setTransactionDate(debtitEntry.getDate());
                                    bp.setAccountNo(debtitEntry.getAccountNo());
                                    bp.setIsDebit(0);
                                    bp.setModule("AR");
                                    bp.setTransactionCode(pcc.getGlContraCode());
                                    bp.setDescription(pcc.getDescription());
                                    bp.setGlContraCode("");
                                    bp.setRefference(debtitEntry.getReferenceNo());
                                    bp.setAmountExcl(Double.parseDouble(debtitEntry.getDAmount().toString()));
                                    bp.setAmountIncl(Double.parseDouble(debtitEntry.getDAmount().toString()));
                                    bp.setProjectCode("BU133");
                                    bp.setOrderNumber(value);
//                                    bp.setGlContraCode("2W");
                                    batchPostingsOdi.add(bp);
                                }
                            }
                        }
                    }
                }
            }

        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return batchPostingsOdi;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.ites.bankfinance.dao.OtherPaymentDao;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.SettlementPaymentOther;
import com.ites.bankfinance.model.SettlementVoucher;
import com.ites.bankfinance.model.SettlementVoucherCategory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Amidu-pc
 */
@Repository
public class OtherPaymentDaoImpl implements OtherPaymentDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    LoanDaoImpl loanimpl;

    @Override
    public List<Chartofaccount> findChtAccountsList() {
        List<Chartofaccount> chtAccountList = new ArrayList();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql = "from Chartofaccount";
            chtAccountList = (List<Chartofaccount>) session.createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return chtAccountList;
    }

    @Override
    public String addSettlmentPaymentOther(SettlementPaymentOther spo) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean status = false;
        String rno = "";
        try {
            if (spo != null) {
                session.save(spo);
                status = true;
                tx.commit();
                rno = spo.getRecieptNo();
            }
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        session.flush();
        session.close();
        return rno;
    }

    @Override
    public String getOtherTransactionIdMax(String code) {

        String maxtransactionNo = "1";
        String sql = "select transactionNo from SettlementPaymentOther where transactionNo like '" + code + "%' order by id desc";
        Object obj = sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();

        int id = 0;
        if (obj != null) {
            maxtransactionNo = obj.toString();
            id = Integer.parseInt(maxtransactionNo.substring(code.length()));
            id++;
            maxtransactionNo = code + id;
        } else {
            maxtransactionNo = code + maxtransactionNo;
        }
        return maxtransactionNo;
    }

    @Override
    public int saveIssuePayment(SettlementVoucher vouch) {
        int save = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Calendar cal = Calendar.getInstance();
        Date actionTime = cal.getTime();
        try {

            String v_code = loanimpl.createVoucherCode(3);
            vouch.setActionTime(actionTime);
            vouch.setVoucherNo(v_code);
            vouch.setLoanId(0);
            vouch.setVoucherType(1);
            session.save(vouch);
            save = vouch.getId();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return save;
    }

    @Override
    public List<SettlementVoucherCategory> getVoucherCategorys() {
        List<SettlementVoucherCategory> voucherCategorys = null;
        try {
            String sql = "from SettlementVoucherCategory";
            voucherCategorys = (List<SettlementVoucherCategory>) sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return voucherCategorys;
    }

    @Override
    public List<Chartofaccount> findChtAccountListByName(String accountName) {
        List<Chartofaccount> chartofaccounts = null;
        try {
            String sql = "from Chartofaccount where accountName like '%" + accountName + "%' and accountNo NOT IN(select accountNo from Chartofaccount where accountNo like 'I%')";
            chartofaccounts = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chartofaccounts;
    }

    @Override
    public List<InsuranceOtherCustomers> findChtAccountInsuranceOtherCustomer(String accountName) {
        List<InsuranceOtherCustomers> customerses = null;
        try {
            customerses = sessionFactory.getCurrentSession().createCriteria(InsuranceOtherCustomers.class)
                    .add(Restrictions.like("insuFullName", accountName, MatchMode.ANYWHERE)).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customerses;
    }

    @Override
    public InsuranceOtherCustomers findChtAccountDetails(String accName) {
        InsuranceOtherCustomers insuranceOtherCustomers = null;
        try {
            insuranceOtherCustomers = (InsuranceOtherCustomers) sessionFactory.getCurrentSession().createCriteria(InsuranceOtherCustomers.class)
                    .add(Restrictions.eq("insuCustAccountNo", accName)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return insuranceOtherCustomers;
    }

}

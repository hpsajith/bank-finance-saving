package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.SavingNewAccForm;
import com.ites.bankfinance.dao.SavingsDao;
import com.ites.bankfinance.savingsModel.DebtorHeaderDetails;
import com.ites.bankfinance.savingsModel.*;
import com.ites.bankfinance.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by AXA-FINANCE on 5/21/2018.
 */
@Repository
public class SavingsDaoImpl implements SavingsDao {

    @Autowired
    private SessionFactory savingsSessionFactory;

    @Autowired
    private UserService userService;

    @Autowired
    private SavingsDebtorDaoImpl savingsDebtorDaoImpl;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Boolean saveOrUpdateSavingsType(MSavingsType savingsType) {
        Boolean is_saved = false;
        Session session = savingsSessionFactory.openSession();
//        Session session = savingsSessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            savingsType.setActionTime(date);
            savingsType.setUserId(userId);
            savingsType.setIsActive(true);
            session.saveOrUpdate(savingsType);
            is_saved = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
//        session.close();
        return is_saved;
    }

    @Override
    public Boolean saveOrUpdateSubSavingsType(MSubSavingsType savingsType) {
        Boolean is_saved = false;
        Session session = savingsSessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            savingsType.setActionTime(date);
            savingsType.setUserId(userId);
            savingsType.setIsActive(true);
            session.saveOrUpdate(savingsType);
            is_saved = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
//        session.close();
        return is_saved;
    }

    @Override
    public List<MSavingsType> findSavingType() {
        List<MSavingsType> savingTypes = null;
        Session session = savingsSessionFactory.openSession();
        try {
            String sql = "from MSavingsType";
            savingTypes = session.getSessionFactory().getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return savingTypes;
    }

    @Override
    public List<MSubSavingsType> findSubSavingType() {
        List<MSubSavingsType> subSavingTypes = null;
        Session session = savingsSessionFactory.openSession();
        try {
            String sql = "from MSubSavingsType";
            subSavingTypes = session.getSessionFactory().getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return subSavingTypes;
    }

    @Override
    public List<MSubSavingsCharges> findSavingCharges() {
        List<MSubSavingsCharges> chargeses = null;
        try {
            String sql = "from MSubSavingsCharges ";
            chargeses = savingsSessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return chargeses;
    }

    @Override
    public int saveNewSavingsData(SavingNewAccForm formData) {
        String message = null;
        int savingsId = 0;
        Session session = savingsSessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean update = false;
        try {
            int userId = userService.findByUserName();

            Calendar calendar = Calendar.getInstance();
            Date actionTime = calendar.getTime();
            String current_date = dateFormat.format(actionTime);

            int debtorId = formData.getDebtorId();
            String sql = "from DebtorHeaderDetails where debtorId=" + debtorId;
            DebtorHeaderDetails debtor = (DebtorHeaderDetails) savingsSessionFactory.openSession().createQuery(sql).uniqueResult();
            if (debtor.getDebtorAccountNo() == null || debtor.getDebtorAccountNo().equals("")) {
                String accNo = savingsDebtorDaoImpl.generateDebtorAccount();
                debtor.setDebtorAccountNo(accNo);
                debtor.setDebtorIsDebtor(true);
            }

            MSavingAccountHeader headerData = new MSavingAccountHeader();
            if (formData.getSavingAccId() > 0) {
                headerData.setSavingAccId(formData.getSavingAccId());
                update = true;
            }

            headerData.setSavingRate(formData.getSubInterestRate());
            headerData.setSavingTypeId(formData.getSavingTypeList());
            headerData.setSavingSubTypeId(formData.getSavingSubTypesList());
            headerData.setSavingAmount(formData.getSavingAmount());
            headerData.setBranchId(formData.getBranchCodeId());
            headerData.setActionTime(current_date);
            headerData.setSystemDate(current_date);
            headerData.setDebtorId(debtorId);

            if (formData.getBookNo() != null) {
                headerData.setBookNo(formData.getBookNo());
            } else {
                headerData.setBookNo("0000 0000 0000");
            }

            session.saveOrUpdate(headerData);

            savingsId = headerData.getSavingAccId();

            message = "Saved Successfully";
            tx.commit();
            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return savingsId;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.DebtorForm;
import com.bankfinance.form.GroupCustomerForm;
import com.ites.bankfinance.dao.DebtorDao;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.service.UserService;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DebtorDaoImpl implements DebtorDao {
    
    @Autowired
    UserService userService;
    
    @Autowired
    SessionFactory sessionFactory;
    
    @Override
    public int saveDebtorDetails(DebtorForm loanForm) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        int debtorId = 0;
        try {
            //user Id
            int userId = userService.findByUserName();

            //current date
            Calendar calendar = Calendar.getInstance();
            Date actionTime = calendar.getTime();

            //add debtor basic details
            DebtorHeaderDetails debtor = new DebtorHeaderDetails();
            if (loanForm.getLoanDebtorId() != null) {
                debtor.setDebtorId(loanForm.getLoanDebtorId());
            }
            debtor.setDebtorName(loanForm.getLoanFullName());
            debtor.setNameWithInitial(loanForm.getNameWithInitials());
            debtor.setDebtorNic(loanForm.getLoanNic());
            debtor.setDebtorDob(loanForm.getLoanDob());
            debtor.setDebtorPersonalAddress(loanForm.getLoanAddress());
            debtor.setDebtorFormalAddress(loanForm.getLoanPostAddress());
            debtor.setDebtorTelephoneHome(loanForm.getLoanTelephoneHome());
            debtor.setDebtorTelephoneMobile(loanForm.getLoanTelephoneMobile());
            debtor.setDebtorTelephoneWork(loanForm.getLoanTelephoneOffice());
            debtor.setDebtorFax(loanForm.getLoanFax());
            debtor.setDebtorEmail(loanForm.getLoanEmail());
            debtor.setDebtorAgOffice(loanForm.getLoanSecretaryOffice());
            debtor.setDebtorPostOffice(loanForm.getLoanPostOffice());
            debtor.setDebtorPoliceStation(loanForm.getLoanPolisStation());
            debtor.setMemberNumber(loanForm.getMemberNumber());
            debtor.setJobInformation(loanForm.getJobInformation());
            debtor.setCenterDay(loanForm.getCenterDay());
            debtor.setBankBranch(loanForm.getBankBranch());
            debtor.setAccountName(loanForm.getAccountName());
            debtor.setAccountNo(loanForm.getAccountNo());
            if (loanForm.getLoanIsDebtor() == 0) {
                debtor.setDebtorIsDebtor(false);
                debtor.setDebtorAccountNo("");
            } else {
                debtor.setDebtorIsDebtor(true);
                if (loanForm.getLoanAccountNo().isEmpty()) {
                    String AccountNo = generateDebtorAccount();
                    debtor.setDebtorAccountNo(AccountNo);
                } else {
                    debtor.setDebtorAccountNo(loanForm.getLoanAccountNo());
                }
            }
            debtor.setDebtorMap(loanForm.getLoanLoacationMap());
            debtor.setDebtorImage(loanForm.getLoanImage());
            debtor.setBranchId(loanForm.getBranchId());
            debtor.setUserId(userId);
            debtor.setActionTime(actionTime);
            session.saveOrUpdate(debtor);
            
            debtorId = debtor.getDebtorId();

            //add loan dependent details
            DebtorDependentDetails loanDependentdetails = new DebtorDependentDetails();
            if (loanForm.getDependentId() != null) {
                loanDependentdetails.setDependentId(loanForm.getDependentId());
            }
            loanDependentdetails.setDebtorId(debtorId);
            loanDependentdetails.setDependentName(loanForm.getDependentName());
            loanDependentdetails.setDependentNic(loanForm.getDependentNic());
            loanDependentdetails.setDependentJob(loanForm.getDependentJob());
            loanDependentdetails.setDependentOccupation(loanForm.getDependentOccupation());
            loanDependentdetails.setDependentNetSalary(loanForm.getDependentNetSalary());
            loanDependentdetails.setDependentTelephone(loanForm.getDependentTelephone());
            loanDependentdetails.setDependentOfficeName(loanForm.getDependentOfficeName());
            loanDependentdetails.setDependentOfficeAddress(loanForm.getDependentOfficeAddress());
            loanDependentdetails.setDependentOfficeTelephone(loanForm.getDependentOfficeTelephone());
            loanDependentdetails.setActionTime(actionTime);
            loanDependentdetails.setUserId(userId);
            session.saveOrUpdate(loanDependentdetails);
            
            tx.commit();
            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtorId;
    }
    
    @Override
    public void saveLoanEmploymentDetails(DebtorEmploymentDetails employment) {
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        employment.setActionTime(action_time);
        employment.setUserId(userId);
        sessionFactory.getCurrentSession().saveOrUpdate(employment);
    }

    @Override
    public List<LoanHeaderDetails> findDebtorLoanAppStatus(int id) {
        List<LoanHeaderDetails> loanDetails = null;
        try {
            String sql = "from LoanHeaderDetails where debtorHeaderDetails.debtorId=" + id;
            loanDetails = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanDetails;
    }
    
    @Override
    public void saveLoanBusinessDetails(DebtorBusinessDetails business) {
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        business.setActionTime(action_time);
        business.setUserId(userId);
        sessionFactory.getCurrentSession().saveOrUpdate(business);
    }
    
    @Override
    public void saveLoanDependentDetails(DebtorDependentDetails dependent) {
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        dependent.setActionTime(action_time);
        dependent.setUserId(userId);
        sessionFactory.getCurrentSession().saveOrUpdate(dependent);
    }
    
    @Override
    public void saveLoanAssessBankDetails(DebtorAssessBankDetails bank) {
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        bank.setActionTime(action_time);
        bank.setUserId(userId);
        sessionFactory.getCurrentSession().saveOrUpdate(bank);
    }
    
    @Override
    public void saveLoanAssessVehicleDetails(DebtorAssessVehicleDetails vehicle) {
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        vehicle.setActionTime(action_time);
        vehicle.setUserId(userId);
        sessionFactory.getCurrentSession().saveOrUpdate(vehicle);
    }
    
    @Override
    public void saveLoanAssessLandDetails(DebtorAssessRealestateDetails land) {
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        land.setActionTime(action_time);
        land.setUserId(userId);
        sessionFactory.getCurrentSession().saveOrUpdate(land);
    }
    
    @Override
    public void saveLoanLiabilityDetails(DebtorLiabilityDetails liability) {
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        liability.setActionDate(action_time);
        liability.setUserId(userId);
        sessionFactory.getCurrentSession().saveOrUpdate(liability);
    }
    
    @Override
    public DebtorForm findDebtorOtherDetails(int debtorId) {
        DebtorForm debtor = new DebtorForm();
        try {
            String sql0 = "From DebtorHeaderDetails where debtorId=" + debtorId;
            DebtorHeaderDetails d_header = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
            debtor.setLoanFullName(d_header.getDebtorName());
            debtor.setNameWithInitials(d_header.getNameWithInitial());
            debtor.setLoanAddress(d_header.getDebtorPersonalAddress());
            debtor.setLoanPostAddress(d_header.getDebtorFormalAddress());
            debtor.setLoanNic(d_header.getDebtorNic());
            debtor.setLoanDob(d_header.getDebtorDob());
            debtor.setLoanTelephoneHome(d_header.getDebtorTelephoneHome());
            debtor.setLoanTelephoneOffice(d_header.getDebtorTelephoneWork());
            debtor.setLoanTelephoneMobile(d_header.getDebtorTelephoneMobile());
            debtor.setLoanPolisStation(d_header.getDebtorPoliceStation());
            debtor.setLoanPostOffice(d_header.getDebtorPostOffice());
            debtor.setLoanSecretaryOffice(d_header.getDebtorAgOffice());
            debtor.setLoanImage(d_header.getDebtorImage());
            debtor.setLoanLoacationMap(d_header.getDebtorMap());
            debtor.setLoanFax(d_header.getDebtorFax());
            debtor.setLoanEmail(d_header.getDebtorEmail());
            debtor.setLoanAccountNo(d_header.getDebtorAccountNo());
            debtor.setLoanBranch(d_header.getBranchId());
            debtor.setMemberNumber(d_header.getMemberNumber());
            debtor.setCenterDay(d_header.getCenterDay());
            debtor.setJobInformation(d_header.getJobInformation());
            debtor.setLoanIsDebtor(d_header.getDebtorIsDebtor() == true ? 1 : 0);
            debtor.setBankBranch(d_header.getBankBranch());
            debtor.setAccountName(d_header.getAccountName());
            debtor.setAccountNo(d_header.getAccountNo());
            
            String sql1 = "From DebtorDependentDetails where debtorId=" + debtorId;
            List<DebtorDependentDetails> dependent = sessionFactory.getCurrentSession().createQuery(sql1).list();
            debtor.setDependent(dependent);
            
            String sql2 = "From DebtorBusinessDetails where debtorId=" + debtorId;
            List<DebtorBusinessDetails> business = sessionFactory.getCurrentSession().createQuery(sql2).list();
            debtor.setBusiness(business);
            
            String sql3 = "From DebtorEmploymentDetails where debtorId=" + debtorId;
            List<DebtorEmploymentDetails> employee = sessionFactory.getCurrentSession().createQuery(sql3).list();
            debtor.setEmployement(employee);
            
            String sql4 = "From DebtorLiabilityDetails where debtorId=" + debtorId;
            List<DebtorLiabilityDetails> liability = sessionFactory.getCurrentSession().createQuery(sql4).list();
            debtor.setLiability(liability);
            
            String sql5 = "From DebtorAssessVehicleDetails where debtorId=" + debtorId;
            List<DebtorAssessVehicleDetails> vehicle = sessionFactory.getCurrentSession().createQuery(sql5).list();
            debtor.setVehicle(vehicle);
            
            String sql6 = "From DebtorAssessBankDetails where debtorId=" + debtorId;
            List<DebtorAssessBankDetails> bank = sessionFactory.getCurrentSession().createQuery(sql6).list();
            debtor.setBank(bank);
            
            String sql7 = "From DebtorAssessRealestateDetails where debtorId=" + debtorId;
            List<DebtorAssessRealestateDetails> land = sessionFactory.getCurrentSession().createQuery(sql7).list();
            debtor.setLand(land);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtor;
    }
    
    @Override
    public DebtorDependentDetails findDependent(int depedentId) {
        DebtorDependentDetails dependent = null;
        try {
            String sql = "from DebtorDependentDetails where dependentId=" + depedentId;
            dependent = (DebtorDependentDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dependent;
    }
    
    @Override
    public DebtorAssessRealestateDetails findAssetLand(int id) {
        DebtorAssessRealestateDetails land = null;
        try {
            String sql = "from DebtorAssessRealestateDetails where realEstateId=" + id;
            land = (DebtorAssessRealestateDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return land;
    }
    
    @Override
    public DebtorAssessVehicleDetails findAssetVehicle(int id) {
        DebtorAssessVehicleDetails vehicle = null;
        try {
            String sql = "from DebtorAssessVehicleDetails where vehicleId=" + id;
            vehicle = (DebtorAssessVehicleDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicle;
    }
    
    @Override
    public DebtorAssessBankDetails findAssetBank(int id) {
        DebtorAssessBankDetails bank = null;
        try {
            String sql = "from DebtorAssessBankDetails where bankId=" + id;
            bank = (DebtorAssessBankDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bank;
    }
    
    @Override
    public DebtorLiabilityDetails findLiability(int id) {
        DebtorLiabilityDetails liability = null;
        try {
            String sql = "from DebtorLiabilityDetails where liabilityId=" + id;
            liability = (DebtorLiabilityDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liability;
    }
    
    @Override
    public DebtorBusinessDetails findBusiness(int id) {
        DebtorBusinessDetails business = null;
        try {
            String sql = "from DebtorBusinessDetails where businessId=" + id;
            business = (DebtorBusinessDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return business;
    }
    
    @Override
    public DebtorEmploymentDetails findEmployment(int id) {
        DebtorEmploymentDetails employe = null;
        try {
            String sql = "from DebtorEmploymentDetails where companyId=" + id;
            employe = (DebtorEmploymentDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employe;
    }
    
    @Override
    public void updateDebtorDetails(DebtorHeaderDetails debHeadDetails) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            Calendar cal = Calendar.getInstance();
            Date action_time = cal.getTime();
            int userId = userService.findByUserName();
            debHeadDetails.setActionTime(action_time);
            debHeadDetails.setUserId(userId);
            session.saveOrUpdate(debHeadDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
    }
    
    @Override
    public boolean saveCustomerProfilePicture(int customerId, String filePath) {
        boolean save = false;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            if (customerId == 0) {
                String sql = "select max(debtorId) from DebtorHeaderDetails";
                Object obj = session.createQuery(sql).uniqueResult();
                if (obj != null) {
                    customerId = Integer.parseInt(obj.toString());
                }
            }
            
            String sql1 = "From DebtorHeaderDetails where debtorId=" + customerId;
            DebtorHeaderDetails debtor = (DebtorHeaderDetails) session.createQuery(sql1).uniqueResult();
            if (debtor != null) {
                debtor.setDebtorImage(filePath);
                session.update(debtor);
                save = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return save;
    }
    
    @Override
    public File findProfilePicById(int cusId) {
        String filePath = "";
        File file = null;
        try {
            if (cusId == 0) {
                String sql0 = "select max(debtorId) from DebtorHeaderDetails";
                Object obj = sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
                if (obj != null) {
                    cusId = Integer.parseInt(obj.toString());
                }
            }
            String sql = "from DebtorHeaderDetails where debtorId=" + cusId;
            DebtorHeaderDetails obj = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                filePath = obj.getDebtorImage();
                if (filePath != null && !filePath.equals("")) {
                    file = new File(filePath);
                } else {
                    String sql2 = "from MFilepath where id=1";
                    MFilepath defaultPath = (MFilepath) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                    filePath = defaultPath.getFilePath();
                    file = new File(filePath);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
    
    @Override
    public int findMaxCustomerId() {
        int cusId = 0;
        try {
            String sql0 = "select max(debtorId) from DebtorHeaderDetails";
            Object obj = sessionFactory.getCurrentSession().createQuery(sql0).uniqueResult();
            if (obj != null) {
                cusId = Integer.parseInt(obj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cusId;
    }
    
    public String generateDebtorAccount() {
        
        String AccountNo = "";
        try {
            String sql = "select max(debtorAccountNo) from DebtorHeaderDetails";
            AccountNo = "IFD";
            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            String maxNo = "0";
            if (ob != null) {
                maxNo = ob.toString().substring(3).replaceAll("^0+", "");
            }
            int mm = Integer.parseInt(maxNo) + 1;
            
            int noLength = 7 - String.valueOf(mm).length();
            for (int i = 0; i < noLength; i++) {
                AccountNo = AccountNo + "0";
            }
            
            if (noLength > 0) {
                AccountNo = AccountNo + mm;
            } else {
                AccountNo = AccountNo + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AccountNo;
    }
    
    @Override
    public List<DebtorHeaderDetails> findDebtorBuNic(String nicNo) {
        List<DebtorHeaderDetails> detailses = null;
        try {
            detailses = sessionFactory.getCurrentSession().createCriteria(DebtorHeaderDetails.class)
                    .add(Restrictions.eq("debtorNic", nicNo)).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detailses;
    }
    
    @Override
    public List<DebtorHeaderDetails> findDebtors(boolean isDebtor) {
        List<DebtorHeaderDetails> debtors = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorIsDebtor = :debtorIsDebtor";
            debtors = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("debtorIsDebtor", true).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return debtors;
    }
    
    @Override
    public List<DebtorHeaderDetails> findDebtors(boolean isDebtor, boolean isGenerate) {
        List<DebtorHeaderDetails> debtors = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorIsDebtor = :debtorIsDebtor and isGenerate = :isGenerate";
            debtors = sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("debtorIsDebtor", isDebtor)
                    .setParameter("isGenerate", isGenerate).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return debtors;
    }
    
    @Override
    public DebtorHeaderDetails findDebtor(int debtorId) {
        DebtorHeaderDetails dhd = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorId = :debtorId";
            dhd = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("debtorId", debtorId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dhd;
    }
    
    @Override
    public List<String> findDebtorNicNo(String pattern) {
        List<String> nicList = null;
        try {
            String sql = "select debtorNic from DebtorHeaderDetails where debtorNic like :debtorNic and debtorIsDebtor = :debtorIsDebtor";
            nicList = sessionFactory.getCurrentSession().createQuery(sql).setParameter("debtorNic", "%" + pattern + "%")
                    .setParameter("debtorIsDebtor", true).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return nicList;
    }
    
    @Override
    public List<DebtorHeaderDetails> findDebtorByName(String name) {
        List<DebtorHeaderDetails> dhds = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorName like :debtorName and debtorIsDebtor = :debtorIsDebtor";
            dhds = sessionFactory.getCurrentSession().createQuery(sql).setParameter("debtorName", "%" + name + "%")
                    .setParameter("debtorIsDebtor", true).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dhds;
    }
    
    @Override
    public List<DebtorHeaderDetails> findDebtorByAccNo(String accNo) {
        List<DebtorHeaderDetails> dhds = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorAccountNo like :debtorAccountNo and debtorIsDebtor = :debtorIsDebtor";
            dhds = sessionFactory.getCurrentSession().createQuery(sql).setParameter("debtorAccountNo", "%" + accNo + "%")
                    .setParameter("debtorIsDebtor", true).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dhds;
    }
    
    @Override
    public boolean saveGroupDebtor(GroupCustomerForm customerForm) {
        
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        int userId = userService.findByUserName();
        boolean status = false;
        
        if (customerForm != null) {
            for (Integer guarantorID : customerForm.getGuarantorID()) {
                DebtorGroupDetails debtorGroupDetails = new DebtorGroupDetails();
                debtorGroupDetails.setActionTime(action_time);
                debtorGroupDetails.setUserId(userId);
                debtorGroupDetails.setLoanId(0);
                debtorGroupDetails.setLoanStatus(false);
                debtorGroupDetails.setMBranchId(customerForm.getBranchID());
                debtorGroupDetails.setMCenterId(customerForm.getCenteID());
                debtorGroupDetails.setMGroupId(customerForm.getGroupID());
                debtorGroupDetails.setDebtorId(guarantorID);
                sessionFactory.getCurrentSession().saveOrUpdate(debtorGroupDetails);
                status = true;
            }
        }        
        return status;        
    }

    @Override
    public DebtorHeaderDetails findDebtorByMemberNo(String memberNo, Object bID) {
    DebtorHeaderDetails dhd = null;
        try {
            String sql = "from DebtorHeaderDetails where memberNumber = :memberNo and branchId = :bID";
            dhd = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("memberNo", memberNo).setParameter("bID", bID).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dhd;
    }

    
}

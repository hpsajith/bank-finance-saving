/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.ChartDataSet;
import com.bankfinance.form.DayPlanTargetModel;
import com.bankfinance.form.RecOffCollection;
import com.ites.bankfinance.dao.DayPlanDao;
import com.ites.bankfinance.model.DayPlanAssigntoRecoOfficer;
import com.ites.bankfinance.model.DayPlanAssingtoRecoManager;
import com.ites.bankfinance.model.DayPlanMonthSchedule;
import com.ites.bankfinance.model.DayPlanMonthTargetAssingCed;
import com.ites.bankfinance.model.MLoanType;
import com.ites.bankfinance.model.RecOffCollDates;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.UserService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DayPlanDaoImpl implements DayPlanDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    UserService userService;

    @Autowired
    MasterDataService masterDataService;

    @Autowired
    ReportDaoImpl reportDaoImpl;

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public DayPlanTargetModel calculateTarget(String m, int loanType, int branchId) {
        DayPlanTargetModel dptm = null;
        Date monthStart;
        Date monthEnd;
        double totDueAmount = 0.00, totArrsAmount = 0.00;
        try {
            monthStart = dateFormat.parse(m);
            String day = getLastDay(monthStart);
            monthEnd = dateFormat.parse(day);
            String sql1 = "SELECT SUM(li.insRoundAmount) FROM LoanInstallment AS li, LoanHeaderDetails AS lo, MLoanType AS mlt, MSubLoanType AS msl WHERE li.loanId = lo.loanId "
                    + "AND lo.loanType = msl.subLoanId AND msl.loanTypeId = mlt.loanTypeId AND li.dueDate BETWEEN '" + dateFormat.format(monthStart) + "' AND '" + dateFormat.format(monthEnd) + "' AND mlt.loanTypeId = " + loanType + " AND lo.branchId = " + branchId;
            Object due = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (due != null) {
                totDueAmount = Double.parseDouble(due.toString());
            }
            String sql2 = "SELECT lo.loanId FROM LoanHeaderDetails AS lo, MLoanType AS mlt, MSubLoanType msl WHERE mlt.loanTypeId = msl.loanTypeId AND "
                    + " msl.subLoanId = lo.loanType AND mlt.loanTypeId = " + loanType + " AND lo.branchId = " + branchId;
            List loanIdList = sessionFactory.getCurrentSession().createQuery(sql2).list();
            if (loanIdList.size() > 0) {
                for (Object obj : loanIdList) {
                    int loanId = Integer.parseInt(obj.toString());
                    totArrsAmount = totArrsAmount + reportDaoImpl.findLoanBalance(loanId);
                }
            }
            dptm = new DayPlanTargetModel();
            dptm.setLoanType(loanType);
            dptm.setArriesAmount(totArrsAmount);
            dptm.setDueAmount(totDueAmount);
            dptm.setMonthStart(monthStart);
            dptm.setMonthEnd(monthEnd);
        } catch (ParseException | HibernateException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        return dptm;
    }

    private String getLastDay(Date firstDay) {
        String lastDay = null;
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(firstDay);
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.DATE, -1);
            Date lastDayOfMonth = calendar.getTime();
            lastDay = dateFormat.format(lastDayOfMonth);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lastDay;
    }

    @Override
    public int saveOrUpdateMonthTarget(DayPlanMonthTargetAssingCed dpm) {
        int id = 0;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        dpm.setActionTime(Calendar.getInstance().getTime());
        int userId = userService.findByUserName();
        if (userId != 0) {
            dpm.setUserId(userId);
        }
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(dpm);
            transaction.commit();
            id = dpm.getPk();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return id;
    }

    @Override
    public void saveOrUpdateAssignTarget(DayPlanAssingtoRecoManager dpa) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        dpa.setActionTime(Calendar.getInstance().getTime());
        int userId = userService.findByUserName();
        if (userId != 0) {
            dpa.setAssingUser(userId);
        }
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(dpa);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public DayPlanMonthTargetAssingCed isExistDayPlan(int loanType, Date date, int branch) {
        DayPlanMonthTargetAssingCed dpm = null;
        try {
            String sql = "from DayPlanMonthTargetAssingCed where loanType = :loanType AND targetDate = :targetDate AND branchId = :branchId";
            dpm = (DayPlanMonthTargetAssingCed) sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("loanType", loanType).setParameter("targetDate", date)
                    .setParameter("branchId", branch).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dpm;
    }

    @Override
    public DayPlanAssingtoRecoManager isExistAssign(int targetId) {
        DayPlanAssingtoRecoManager dpa = null;
        try {
            String sql = "from DayPlanAssingtoRecoManager where targetCedId = " + targetId;
            dpa = (DayPlanAssingtoRecoManager) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dpa;
    }

    @Override
    public double getTargetForRecMan(int recManId, int branchId, Date tgtMon, int loanType) {
        double targetAmount = 0.00;
        double monthlyTarget = 0.00, assignedTarget = 0.00;
        try {
            String sql1 = "select dpt.monthTargetAmount from DayPlanMonthTargetAssingCed as dpt, DayPlanAssingtoRecoManager "
                    + "as dprm where dpt.pk = dprm.targetCedId and dpt.loanType = " + loanType + " and dpt.targetDate = '" + dateFormat.format(tgtMon) + "' and dpt.branchId = " + branchId
                    + " and dprm.recoveryManagerId = " + recManId;
            String sql2 = "select sum(dpto.targetAmount) from DayPlanAssigntoRecoOfficer as dpto where dpto.recoManagerId = " + recManId
                    + " and dpto.loanType = " + loanType + " and dpto.branchId = " + branchId + " and dpto.targetDate = '" + dateFormat.format(tgtMon) + "'";
            Object mTarget = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (mTarget != null) {
                monthlyTarget = Double.parseDouble(mTarget.toString());
            }
            Object aTarget = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (aTarget != null) {
                assignedTarget = Double.parseDouble(aTarget.toString());
            }
            targetAmount = monthlyTarget - assignedTarget;
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return targetAmount;
    }

    @Override
    public void saveOrUpdateAssignTargetToRecOff(DayPlanAssigntoRecoOfficer dparo) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(dparo);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public List<DayPlanAssigntoRecoOfficer> getAssignedList(int recManId, int branchId, Date tgtMon, int loanType) {
        List<DayPlanAssigntoRecoOfficer> dparos = null;
        try {
            String sql = "from DayPlanAssigntoRecoOfficer where recoManagerId = :rId and branchId = :brId and targetDate = :tgtDate and loanType = :loType";
            dparos = (List<DayPlanAssigntoRecoOfficer>) sessionFactory.getCurrentSession().createQuery(sql).setParameter("rId", recManId).
                    setParameter("brId", branchId).setParameter("tgtDate", tgtMon).
                    setParameter("loType", loanType).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dparos;
    }

    @Override
    public DayPlanAssigntoRecoOfficer isExistAssignRecOff(int recOffId, int branchId, Date tgtMon, int loanType) {
        DayPlanAssigntoRecoOfficer dparos = null;
        try {
            String sql = "from DayPlanAssigntoRecoOfficer where recoOffcId = :rId and branchId = :brId and targetDate = :tgtDate and loanType = :loType";
            dparos = (DayPlanAssigntoRecoOfficer) sessionFactory.getCurrentSession().createQuery(sql).setParameter("rId", recOffId).
                    setParameter("brId", branchId).setParameter("tgtDate", tgtMon).
                    setParameter("loType", loanType).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dparos;
    }

    @Override
    public List<MLoanType> getAssignedLoanTypes(int recManId, int branchId) {
        List<MLoanType> mlts = new ArrayList<>();
        try {
            String sql = "SELECT DISTINCT mlt.loanTypeId , mlt.loanTypeName FROM DayPlanMonthTargetAssingCed AS dpt, DayPlanAssingtoRecoManager AS dpr, MLoanType AS mlt WHERE dpt.pk = dpr.targetCedId "
                    + "AND dpt.loanType = mlt.loanTypeId AND dpr.recoveryManagerId = " + recManId + " AND dpt.branchId = " + branchId;
            List list = sessionFactory.getCurrentSession().createQuery(sql).list();
            for (Object ob : list) {
                Object[] o = (Object[]) ob;
                MLoanType mlt = new MLoanType();
                mlt.setLoanTypeId(Integer.parseInt(o[0].toString()));
                mlt.setLoanTypeName(o[1].toString());
                mlts.add(mlt);
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return mlts;
    }

    @Override
    public List<MLoanType> getAssignedLoanTypesToRecOff(int recOffId, int branchId) {
        List<MLoanType> mlts = new ArrayList<>();
        try {
            String sql = "SELECT distinct mlt.loanTypeId, mlt.loanTypeName FROM DayPlanAssigntoRecoOfficer AS dpr, MLoanType AS mlt WHERE dpr.loanType = mlt.loanTypeId "
                    + "AND dpr.branchId = " + branchId + " AND dpr.recoOffcId = " + recOffId;
            List list = sessionFactory.getCurrentSession().createQuery(sql).list();
            for (Object ob : list) {
                Object[] o = (Object[]) ob;
                MLoanType mlt = new MLoanType();
                mlt.setLoanTypeId(Integer.parseInt(o[0].toString()));
                mlt.setLoanTypeName(o[1].toString());
                mlts.add(mlt);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return mlts;
    }

    @Override
    public List<MLoanType> getAssignedLoanTypesToRecOff(int recOffId, int branchId, Date tgtDate) {
        List<MLoanType> mlts = new ArrayList<>();
        try {
            String sql = "SELECT mlt.loanTypeId, mlt.loanTypeName FROM DayPlanAssigntoRecoOfficer AS dpr, MLoanType AS mlt WHERE dpr.loanType = mlt.loanTypeId "
                    + "AND dpr.targetDate = '" + dateFormat.format(tgtDate) + "' AND dpr.branchId = " + branchId + " AND dpr.recoOffcId = " + recOffId;
            List list = sessionFactory.getCurrentSession().createQuery(sql).list();
            for (Object ob : list) {
                Object[] o = (Object[]) ob;
                MLoanType mlt = new MLoanType();
                mlt.setLoanTypeId(Integer.parseInt(o[0].toString()));
                mlt.setLoanTypeName(o[1].toString());
                mlts.add(mlt);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return mlts;
    }

    @Override
    public void saveOrUpdateDayPlanMonthSchedule(DayPlanMonthSchedule dpms) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(dpms);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public DayPlanMonthSchedule isExistMonthSchedule(int recOffId, int loanType, int branchId, Date tgtDate) {
        DayPlanMonthSchedule dpms = null;
        try {
            String sql = "from DayPlanMonthSchedule where recOffId = :recOff and loanTypeId = :loanType and branchId = :branch and targetDate = :tgtDate";
            dpms = (DayPlanMonthSchedule) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("recOff", recOffId)
                    .setParameter("loanType", loanType).setParameter("branch", branchId)
                    .setParameter("tgtDate", dateFormat.format(tgtDate)).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return dpms;
    }

    @Override
    public double getTargetForRecOff(int recOffId, int branchId, Date sDate, Date eDate, int loanType) {
        double targetAmount = 0.00;
        try {
            String sql = "SELECT SUM(target) FROM DayPlanMonthSchedule WHERE recOffId = " + recOffId + " AND loanTypeId = " + loanType + " AND branchId = " + branchId + " AND targetDate BETWEEN '" + dateFormat.format(sDate) + "' AND '" + dateFormat.format(eDate) + "'";
            Object tgtAmount = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (tgtAmount != null) {
                targetAmount = Double.parseDouble(tgtAmount.toString());
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        }
        return targetAmount;
    }

    @Override
    public List<DayPlanMonthSchedule> getMonthSchedule(int recOffId, int branchId, Date sDate, Date eDate, int loanType) {
        List<DayPlanMonthSchedule> schedule = null;
        try {
            String sql = "from DayPlanMonthSchedule where recOffId = :recOff and loanTypeId = :loanType and branchId = :branch and targetDate between :sDate and :eDate";
            schedule = (List<DayPlanMonthSchedule>) sessionFactory.getCurrentSession().createQuery(sql).setParameter("recOff", recOffId)
                    .setParameter("loanType", loanType).setParameter("branch", branchId)
                    .setParameter("sDate", dateFormat.format(sDate)).setParameter("eDate", dateFormat.format(eDate)).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return schedule;
    }

    @Override
    public void activeDayPlan(int id) {
        try {
            String sql = "from DayPlanMonthSchedule where pk = " + id;
            DayPlanMonthSchedule dpms = (DayPlanMonthSchedule) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (dpms != null) {
                dpms.setTarget(0.00);
                dpms.setCollection(0.00);
                dpms.setDifference(0.00);
                dpms.setReason("");
                dpms.setIsactive(true);
                saveOrUpdateDayPlanMonthSchedule(dpms);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void inActiveDayPlan(int id, String reason) {
        try {
            String sql = "from DayPlanMonthSchedule where pk = " + id;
            DayPlanMonthSchedule dpms = (DayPlanMonthSchedule) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (dpms != null) {
                dpms.setTarget(0.00);
                dpms.setCollection(0.00);
                dpms.setDifference(0.00);
                dpms.setReason(reason);
                dpms.setIsactive(false);
                saveOrUpdateDayPlanMonthSchedule(dpms);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<DayPlanMonthSchedule> getCollection(int loanType, int recOffId, int branchId, Date sDate, Date eDate) {
        List<DayPlanMonthSchedule> clc = new ArrayList<>();
        List<DayPlanMonthSchedule> monthSchedule = getMonthSchedule(recOffId, branchId, sDate, eDate, loanType);
        List<DayPlanMonthSchedule> monthCollection = getMonthCollection(loanType, recOffId, branchId, sDate, eDate);
        if (monthSchedule.isEmpty()) {
            clc = monthCollection;
        } else {
            if (monthCollection.isEmpty()) {
                clc = monthSchedule;
            } else {
                for (DayPlanMonthSchedule dpms : monthSchedule) {
                    for (DayPlanMonthSchedule dpmc : monthCollection) {
                        if (dpms.getTargetDate().equals(dpmc.getTargetDate())) {
                            dpms.setCollection(dpmc.getCollection());
                            clc.add(dpms);
                        } else {
                            clc.add(dpms);
                        }
                    }
                }
            }
        }
        return clc;
    }

    private List<DayPlanMonthSchedule> getMonthCollection(int loanType, int recOffId, int branchId, Date sDate, Date eDate) {
        List<DayPlanMonthSchedule> collection = new ArrayList<>();
        try {
            String sql1 = "from RecOffCollDates where empId = " + recOffId + " AND branchId = " + branchId;
            RecOffCollDates rocd = (RecOffCollDates) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (rocd != null) {
                List<String> clcDates = Arrays.asList(rocd.getCollectionDates().split(","));
                if (clcDates.size() > 0) {
                    String sql2 = "SELECT DATE(stl.paymentDate), SUM(stl.paymentAmount) FROM SettlementPayments AS stl, LoanHeaderDetails AS lh, MLoanType AS mlt, MSubLoanType AS mslt "
                            + "WHERE stl.loanId = lh.loanId AND lh.loanType = mslt.subLoanId AND mlt.loanTypeId = mslt.loanTypeId AND stl.paymentDate BETWEEN '" + dateFormat.format(sDate) + "%' AND '" + dateFormat.format(eDate) + "%' "
                            + "AND mlt.loanTypeId = " + loanType + " AND lh.branchId = stl.branchId AND lh.branchId = " + branchId + " AND lh.dueDay IN(";
                    for (String date : clcDates) {
                        if (!date.equals("")) {
                            if (clcDates.indexOf(date) == (clcDates.size() - 1)) {
                                sql2 = sql2 + date + ")";
                            } else {
                                sql2 = sql2 + date + ",";
                            }
                        }
                    }
                    sql2 = sql2 + " GROUP BY DATE(stl.paymentDate)";
                    List clcList = sessionFactory.getCurrentSession().createQuery(sql2).list();
                    if (clcList.size() > 0) {
                        for (Object ob : clcList) {
                            Object[] o = (Object[]) ob;
                            DayPlanMonthSchedule dpms = new DayPlanMonthSchedule();
                            dpms.setBranchId(branchId);
                            dpms.setTargetDate(o[0].toString());
                            dpms.setTarget(0.00);
                            dpms.setCollection(Double.parseDouble(o[1].toString()));
                            dpms.setDifference(0.00);
                            dpms.setRecOffId(recOffId);
                            dpms.setReason("");
                            dpms.setIsactive(true);
                            collection.add(dpms);
                        }
                    }
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return collection;
    }

    @Override
    public List<ChartDataSet> getRecManChartDataSet(int loanType, int recManId, int branchId, int type) {
        List<ChartDataSet> dataSet = new ArrayList<>();
        if (type == 1) { // fetch type is Monthly
            try {
                String sql1 = "SELECT dpms.targetDate , SUM(dpms.collection) FROM DayPlanMonthSchedule AS dpms, DayPlanAssigntoRecoOfficer AS dpro, DayPlanAssingtoRecoManager AS dprm WHERE dpms.recOffId = dpro.recoOffcId "
                        + "AND dpro.recoManagerId = dprm.recoveryManagerId AND dpms.loanTypeId = " + loanType + " AND dpms.branchId = " + branchId
                        + " AND dprm.recoveryManagerId = " + recManId + " GROUP BY DATE_FORMAT(dpms.targetDate, '%m')";
                List list1 = sessionFactory.getCurrentSession().createQuery(sql1).list();
                String sql2 = "SELECT dpm.targetDate,SUM(dprm.assingAmount) FROM DayPlanMonthTargetAssingCed AS dpm, DayPlanAssingtoRecoManager AS dprm WHERE dpm.pk = dprm.targetCedId"
                        + " AND dpm.loanType=" + loanType + " AND dpm.branchId =" + branchId + " AND dprm.recoveryManagerId =" + recManId + " GROUP BY DATE_FORMAT(dpm.targetDate, '%m')";
                List list2 = sessionFactory.getCurrentSession().createQuery(sql2).list();
                if (list1.size() > 0) {
                    ChartDataSet collectionChartDataSet = new ChartDataSet();
                    List<Double> data = new ArrayList<>();
                    for (Object oData : list1) {
                        Object[] oArr = (Object[]) oData;
                        data.add(Double.parseDouble(oArr[1].toString()));
                    }
                    // create collection chart data set
                    collectionChartDataSet.setLabel("Dataset 1");
                    collectionChartDataSet.setFillColor("#4485F6");
                    collectionChartDataSet.setHighlightFill("#4E7AC7");
                    collectionChartDataSet.setHighlightStroke("#4485F6");
                    collectionChartDataSet.setStrokeColor("#4485F6");
                    collectionChartDataSet.setData(data);
                    dataSet.add(collectionChartDataSet);
                }
                if (list2.size() > 0) {
                    ChartDataSet targetChartDataSet = new ChartDataSet();
                    List<Double> data = new ArrayList<>();
                    for (Object oData : list2) {
                        Object[] oArr = (Object[]) oData;
                        data.add(Double.parseDouble(oArr[1].toString()));
                    }
                    // create target chart data set
                    targetChartDataSet.setLabel("Dataset 2");
                    targetChartDataSet.setFillColor("#1AD190");
                    targetChartDataSet.setHighlightFill("#0EAF74");
                    targetChartDataSet.setHighlightStroke("#1AD190");
                    targetChartDataSet.setStrokeColor("#1AD190");
                    targetChartDataSet.setData(data);
                    dataSet.add(targetChartDataSet);
                }
            } catch (HibernateException | NumberFormatException e) {
                e.printStackTrace();
            }
        } else if (type == 2) { // fetch type is Daily
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Date startDate = calendar.getTime();
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            Date endDate = calendar.getTime();
            try {
                String sql3 = "SELECT dpms.targetDate, SUM(dpms.target), SUM(dpms.collection) FROM DayPlanMonthSchedule AS dpms, DayPlanAssigntoRecoOfficer AS dpro WHERE"
                        + " dpms.recOffId = dpro.recoOffcId AND dpms.loanTypeId = " + loanType + " AND dpms.branchId = " + branchId + " AND dpro.recoManagerId = " + recManId
                        + " AND dpms.targetDate BETWEEN '" + dateFormat.format(startDate) + "' AND '" + dateFormat.format(endDate) + "' GROUP BY dpms.targetDate";
                List list3 = sessionFactory.getCurrentSession().createQuery(sql3).list();
                if (list3.size() > 0) {
                    List<Double> collection = new ArrayList<>();
                    List<Double> target = new ArrayList<>();
                    for (Object oData : list3) {
                        Object[] oArr = (Object[]) oData;
                        target.add(Double.parseDouble(oArr[1].toString()));
                        collection.add(Double.parseDouble(oArr[2].toString()));
                    }
                    // create collection chart data set
                    ChartDataSet collectionChartDataSet = new ChartDataSet();
                    collectionChartDataSet.setLabel("Dataset 1");
                    collectionChartDataSet.setFillColor("#4485F6");
                    collectionChartDataSet.setHighlightFill("#4E7AC7");
                    collectionChartDataSet.setHighlightStroke("#4485F6");
                    collectionChartDataSet.setStrokeColor("#4485F6");
                    collectionChartDataSet.setData(collection);

                    // create target chart data set
                    ChartDataSet targetChartDatSet = new ChartDataSet();
                    targetChartDatSet.setLabel("Dataset 2");
                    targetChartDatSet.setFillColor("#1AD190");
                    targetChartDatSet.setHighlightFill("#0EAF74");
                    targetChartDatSet.setHighlightStroke("#1AD190");
                    targetChartDatSet.setStrokeColor("#1AD190");
                    targetChartDatSet.setData(target);

                    dataSet.add(collectionChartDataSet);
                    dataSet.add(targetChartDatSet);
                }
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
        return dataSet;
    }

    @Override
    public List<ChartDataSet> getRecOffChartDataSet(int loanType, int recOffId, int branchId, int type) {
        List<ChartDataSet> dataSet = new ArrayList<>();
        if (type == 1) { // fetch type is Monthly
            try {
                String sql1 = "SELECT dpms.targetDate,SUM(dpms.collection) FROM DayPlanMonthSchedule AS dpms WHERE dpms.loanTypeId =" + loanType + " AND "
                        + "dpms.recOffId =" + recOffId + " AND dpms.branchId =" + branchId + " GROUP BY DATE_FORMAT(dpms.targetDate, '%m')";
                List list1 = sessionFactory.getCurrentSession().createQuery(sql1).list();
                String sql2 = "SELECT dpms.targetDate,SUM(dpms.target) FROM DayPlanMonthSchedule AS dpms WHERE dpms.loanTypeId =" + loanType + " AND "
                        + "dpms.recOffId =" + recOffId + " AND dpms.branchId =" + branchId + " GROUP BY DATE_FORMAT(dpms.targetDate, '%m')";
                List list2 = sessionFactory.getCurrentSession().createQuery(sql2).list();
                if (list1.size() > 0) {
                    ChartDataSet collectionChartDataSet = new ChartDataSet();
                    List<Double> data = new ArrayList<>();
                    for (Object oData : list1) {
                        Object[] oArr = (Object[]) oData;
                        data.add(Double.parseDouble(oArr[1].toString()));
                    }
                    // create collection chart data set
                    collectionChartDataSet.setLabel("Dataset 1");
                    collectionChartDataSet.setFillColor("#4485F6");
                    collectionChartDataSet.setHighlightFill("#4E7AC7");
                    collectionChartDataSet.setHighlightStroke("#4485F6");
                    collectionChartDataSet.setStrokeColor("#4485F6");
                    collectionChartDataSet.setData(data);
                    dataSet.add(collectionChartDataSet);
                }
                if (list2.size() > 0) {
                    ChartDataSet targetChartDataSet = new ChartDataSet();
                    List<Double> data = new ArrayList<>();
                    for (Object oData : list2) {
                        Object[] oArr = (Object[]) oData;
                        data.add(Double.parseDouble(oArr[1].toString()));
                    }
                    // create target chart data set
                    targetChartDataSet.setLabel("Dataset 2");
                    targetChartDataSet.setFillColor("#1AD190");
                    targetChartDataSet.setHighlightFill("#0EAF74");
                    targetChartDataSet.setHighlightStroke("#1AD190");
                    targetChartDataSet.setStrokeColor("#1AD190");
                    targetChartDataSet.setData(data);
                    dataSet.add(targetChartDataSet);
                }
            } catch (HibernateException | NumberFormatException e) {
                e.printStackTrace();
            }
        } else if (type == 2) { // fetch type is Daily
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Date startDate = calendar.getTime();
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            Date endDate = calendar.getTime();
            try {
                List<DayPlanMonthSchedule> list = sessionFactory.getCurrentSession().createCriteria(DayPlanMonthSchedule.class).
                        add(Restrictions.eq("loanTypeId", loanType)).add(Restrictions.eq("branchId", branchId))
                        .add(Restrictions.eq("recOffId", recOffId))
                        .add(Restrictions.between("targetDate", dateFormat.format(startDate), dateFormat.format(endDate)))
                        .list();
                if (list.size() > 0) {
                    List<Double> collection = new ArrayList<>();
                    List<Double> target = new ArrayList<>();
                    for (DayPlanMonthSchedule schedule : list) {
                        collection.add(schedule.getCollection());
                        target.add(schedule.getTarget());
                    }
                    // create collection chart data set
                    ChartDataSet collectionChartDataSet = new ChartDataSet();
                    collectionChartDataSet.setLabel("Dataset 1");
                    collectionChartDataSet.setFillColor("#4485F6");
                    collectionChartDataSet.setHighlightFill("#4E7AC7");
                    collectionChartDataSet.setHighlightStroke("#4485F6");
                    collectionChartDataSet.setStrokeColor("#4485F6");
                    collectionChartDataSet.setData(collection);

                    // create target chart data set
                    ChartDataSet targetChartDatSet = new ChartDataSet();
                    targetChartDatSet.setLabel("Dataset 2");
                    targetChartDatSet.setFillColor("#1AD190");
                    targetChartDatSet.setHighlightFill("#0EAF74");
                    targetChartDatSet.setHighlightStroke("#1AD190");
                    targetChartDatSet.setStrokeColor("#1AD190");
                    targetChartDatSet.setData(target);

                    dataSet.add(collectionChartDataSet);
                    dataSet.add(targetChartDatSet);
                }
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
        return dataSet;
    }

    @Override
    public List<RecOffCollection> getRecoveryOfficersWithCollection(int loanType, int branchId, String month) {
        List<RecOffCollection> recOffCollections = new ArrayList<>();
        try {
            Date sDate = dateFormat.parse(month);
            String day = getLastDay(sDate);
            Date eDate = dateFormat.parse(day);
            List recOffList = masterDataService.findUsersByUserTypeAndBranch(4, branchId);
            if (recOffList.size() > 0) {
                for (Object recOff : recOffList) {
                    Object[] recOffArr = (Object[]) recOff;
                    int recOffId = Integer.parseInt(recOffArr[0].toString());
                    String sql1 = "from RecOffCollDates where empId = " + recOffId + " AND branchId = " + branchId;
                    RecOffCollDates rocd = (RecOffCollDates) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
                    if (rocd != null) {
                        List<String> clcDates = Arrays.asList(rocd.getCollectionDates().split(","));
                        if (clcDates.size() > 0) {
                            String sql2 = "SELECT SUM(stl.paymentAmount) FROM SettlementPayments AS stl, LoanHeaderDetails AS lh, MLoanType AS mlt, MSubLoanType AS mslt "
                                    + "WHERE stl.loanId = lh.loanId AND lh.loanType = mslt.subLoanId AND mlt.loanTypeId = mslt.loanTypeId AND stl.paymentDate BETWEEN '" + dateFormat.format(sDate) + "%' AND '" + dateFormat.format(eDate) + "%' "
                                    + "AND mlt.loanTypeId = " + loanType + " AND lh.branchId = stl.branchId AND lh.branchId = " + branchId + " AND lh.dueDay IN(";
                            for (String date : clcDates) {
                                if (!date.equals("")) {
                                    if (clcDates.indexOf(date) == (clcDates.size() - 1)) {
                                        sql2 = sql2 + date + ")";
                                    } else {
                                        sql2 = sql2 + date + ",";
                                    }
                                }
                            }
                            Object result = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                            double receivable = 0.00;
                            if (result != null) {
                                receivable = Double.valueOf(result.toString());
                            }
                            DayPlanAssigntoRecoOfficer assigned = isExistAssignRecOff(recOffId, branchId, dateFormat.parse(month), loanType);
                            double assignedAmount = 0.00;
                            if (assigned != null) {
                                assignedAmount = assigned.getTargetAmount();
                            }
                            RecOffCollection recOffCollection = new RecOffCollection();
                            recOffCollection.setRecOffId(recOffId);
                            recOffCollection.setRecOffName(recOffArr[1].toString());
                            recOffCollection.setContatcNo(recOffArr[2].toString());
                            recOffCollection.setReceivable(receivable);
                            recOffCollection.setAssignedAmount(assignedAmount);
                            recOffCollections.add(recOffCollection);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recOffCollections;
    }

    @Override
    public List<DayPlanMonthTargetAssingCed> getTargetAssingCeds(Date tgtDate, int branchId) {
        List<DayPlanMonthTargetAssingCed> targetAssingCeds = null;
        try {
            String sql = "from DayPlanMonthTargetAssingCed where targetDate='" + dateFormat.format(tgtDate) + "' AND branchId=" + branchId;
            targetAssingCeds = (List<DayPlanMonthTargetAssingCed>) sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return targetAssingCeds;
    }

    @Override
    public List getRecoveryOfficers(Date tgtDate, int branchId, int loanType) {
        List recoveryOfficers = null;
        try {
            String sql = "SELECT dprm.recoveryManagerId FROM DayPlanMonthTargetAssingCed AS dpmt, DayPlanAssingtoRecoManager AS dprm WHERE dpmt.pk = dprm.targetCedId "
                    + " AND dpmt.branchId=" + branchId + " AND dpmt.targetDate='" + dateFormat.format(tgtDate) + "' AND dpmt.loanType=" + loanType;
            recoveryOfficers = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recoveryOfficers;
    }

    @Override
    public double getRecoveryOfficerCollection(int loanType, int branchId, Date sDate, Date eDate) {
        double receivable = 0.00;
        int userId = userService.findByUserName();
        int recOffId = userService.findEmployeeByUserId(userId);
        try {
            String sql1 = "from RecOffCollDates where empId = " + recOffId + " AND branchId = " + branchId;
            RecOffCollDates rocd = (RecOffCollDates) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (rocd != null) {
                List<String> clcDates = Arrays.asList(rocd.getCollectionDates().split(","));
                if (clcDates.size() > 0) {
                    String sql2 = "SELECT SUM(stl.paymentAmount) FROM SettlementPayments AS stl, LoanHeaderDetails AS lh, MLoanType AS mlt, MSubLoanType AS mslt "
                            + "WHERE stl.loanId = lh.loanId AND lh.loanType = mslt.subLoanId AND mlt.loanTypeId = mslt.loanTypeId AND stl.paymentDate BETWEEN '" + dateFormat.format(sDate) + "%' AND '" + dateFormat.format(eDate) + "%' "
                            + "AND mlt.loanTypeId = " + loanType + " AND lh.branchId = stl.branchId AND lh.branchId = " + branchId + " AND lh.dueDay IN(";
                    for (String date : clcDates) {
                        if (!date.equals("")) {
                            if (clcDates.indexOf(date) == (clcDates.size() - 1)) {
                                sql2 = sql2 + date + ")";
                            } else {
                                sql2 = sql2 + date + ",";
                            }
                        }
                    }
                    Object result = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                    if (result != null) {
                        receivable = Double.valueOf(result.toString());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return receivable;
    }

    @Override
    public double[] getBalances(int loanType, int recOffId, int branchId, Date sDate, Date eDate) {
        double[] balances = new double[3];
        double totTarget = 0.00;
        double totCollection = 0.00;
        double totDifference = 0.00;
        try {
            String sql1 = "SELECT SUM(target), SUM(collection), SUM(difference) FROM DayPlanMonthSchedule WHERE recOffId = "+recOffId+" AND loanTypeId = "+loanType+" AND branchId = "+branchId
                    + " AND targetDate BETWEEN '"+dateFormat.format(sDate)+"' AND '"+dateFormat.format(eDate)+"'";
            Object result = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (result!=null) {
                Object[] data = (Object[])result;
                totTarget = Double.parseDouble(data[0].toString());
                totCollection = Double.parseDouble(data[1].toString());
                totDifference = Double.parseDouble(data[2].toString());
            }
            balances[0] = totTarget;
            balances[1] = totCollection;
            balances[2] = totDifference;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return balances;
    }

}

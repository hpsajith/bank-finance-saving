/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.AddJobsForm;
import com.bankfinance.form.RecOffColDateForm;
import com.ites.bankfinance.dao.AdminDao;
import com.ites.bankfinance.encription.Encription;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.UserService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author SOFT
 */
@Repository
public class AdminDaoImpl implements AdminDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    SessionFactory financeSessionFactory;

    @Autowired
    UserService userService;

    @Autowired
    MasterDataService masterDataService;

    @Override
    public void saveLoanType(MLoanType loanType) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            loanType.setActionTime(date);
            loanType.setUserId(userId);
            //sessionFactory.getCurrentSession().saveOrUpdate(loanType);
            session.saveOrUpdate(loanType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
    }

    @Override
    public MLoanType findLoanType(int id) {
        MLoanType loanType = null;
        try {
            String sql = "from MLoanType where loanTypeId=" + id;
            loanType = (MLoanType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanType;
    }

    @Override
    public List loadLoanType() {
        List addLoanTypeList = null;
        try {
            String sql = "";
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    sql = "from MLoanType";
                } else {
                    sql = "from MLoanType where isActive = 1";
                }
            }
            addLoanTypeList = sessionFactory.getCurrentSession().createQuery(sql).list();
            return addLoanTypeList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void saveSubLoantype(MSubLoanType subLoanType, int[] documentCheckListId, int[] documentIsCompulseryId, int[] otherChargesId, int[] chartOfAccounts, String[] chartOfAccountName) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            subLoanType.setActionTime(date);
            subLoanType.setUserId(userId);
            subLoanType.setMaxCount(50);
            session.saveOrUpdate(subLoanType);

            //save to DB Chart of Accounts
            if (chartOfAccountName.length > 0) {
                if (chartOfAccounts[0] > 0) {
                    String del = "delete from ConfigChartofaccountLoan where subLoanType = " + subLoanType.getSubLoanId();
                    session.createQuery(del).executeUpdate();

                    if (subLoanType.getIsInsurance()) { // Insurence Apply
                        for (int i = 0; i < chartOfAccountName.length; i++) {
                            ConfigChartofaccountLoan configChartofaccountLoan = new ConfigChartofaccountLoan();
                            if (chartOfAccountName[i] != null) {
                                switch (chartOfAccountName[i]) {
                                    case "INTR":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "RECE":
                                        configChartofaccountLoan.setAccountCategory(4);
                                        break;
                                    case "INTRS":
                                        configChartofaccountLoan.setAccountCategory(4);
                                        break;
                                    case "PAYBLE":
                                        configChartofaccountLoan.setAccountCategory(5);
                                        break;
                                    case "DWN":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "ODI":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "INS_P":
                                        configChartofaccountLoan.setAccountCategory(5);
                                        break;
                                    case "INS_C":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "DEBT":
                                        configChartofaccountLoan.setAccountCategory(5);
                                        break;
                                    case "OVP":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "INTSUSP":
                                        configChartofaccountLoan.setAccountCategory(2);
                                        break;
                                }
                            }
                            configChartofaccountLoan.setAccountNo(String.valueOf(chartOfAccounts[i]));
                            configChartofaccountLoan.setAccountType(chartOfAccountName[i]);
                            configChartofaccountLoan.setActionTime(date);
                            configChartofaccountLoan.setLoanType(subLoanType.getLoanTypeId());
                            configChartofaccountLoan.setSubLoanType(subLoanType.getSubLoanId());
                            configChartofaccountLoan.setUserId(userId);
                            session.saveOrUpdate(configChartofaccountLoan);
                        }
                    } else { // Insurence not Apply
                        for (int i = 0; i < chartOfAccountName.length; i++) {
                            ConfigChartofaccountLoan configChartofaccountLoan = new ConfigChartofaccountLoan();
                            if (chartOfAccountName[i] != null) {
                                switch (chartOfAccountName[i]) {
                                    case "INTR":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "RECE":
                                        configChartofaccountLoan.setAccountCategory(4);
                                        break;
                                    case "INTRS":
                                        configChartofaccountLoan.setAccountCategory(4);
                                        break;
                                    case "PAYBLE":
                                        configChartofaccountLoan.setAccountCategory(5);
                                        break;
                                    case "DWN":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "ODI":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "DEBT":
                                        configChartofaccountLoan.setAccountCategory(5);
                                        break;
                                    case "OVP":
                                        configChartofaccountLoan.setAccountCategory(1);
                                        break;
                                    case "INTSUSP":
                                        configChartofaccountLoan.setAccountCategory(2);
                                        break;
                                }
                            }
                            configChartofaccountLoan.setAccountNo(String.valueOf(chartOfAccounts[i]));
                            configChartofaccountLoan.setAccountType(chartOfAccountName[i]);
                            configChartofaccountLoan.setActionTime(date);
                            configChartofaccountLoan.setLoanType(subLoanType.getLoanTypeId());
                            configChartofaccountLoan.setSubLoanType(subLoanType.getSubLoanId());
                            configChartofaccountLoan.setUserId(userId);
                            session.saveOrUpdate(configChartofaccountLoan);
                        }
                    }
                } else {
                    String del = "delete from ConfigChartofaccountLoan where subLoanType = " + subLoanType.getSubLoanId();
                    session.createQuery(del).executeUpdate();
                }
            }

            //save to DB MSubLoanChecklist document check list        
            if (documentCheckListId.length > 0) {
                if (documentCheckListId[0] > 0) {
                    String del = "delete from MSubLoanChecklist where MSubLoanType.subLoanId = " + subLoanType.getSubLoanId();
                    session.createQuery(del).executeUpdate();

                    for (int i = 0; i < documentCheckListId.length; i++) {
                        MSubLoanChecklist mSubLoanChecklist = new MSubLoanChecklist();
                        mSubLoanChecklist.setMSubLoanType(subLoanType);
                        String sql = "from MCheckList where id =" + documentCheckListId[i];
                        MCheckList mCheckList = (MCheckList) session.createQuery(sql).uniqueResult();
                        mSubLoanChecklist.setMCheckList(mCheckList);
                        mSubLoanChecklist.setIsCompulsory(documentIsCompulseryId[i]);
                        mSubLoanChecklist.setActionTime(date);
                        mSubLoanChecklist.setUserId(userId);
                        session.saveOrUpdate(mSubLoanChecklist);
                    }
                } else {
                    String del = "delete from MSubLoanChecklist where MSubLoanType.subLoanId = " + subLoanType.getSubLoanId();
                    session.createQuery(del).executeUpdate();
                }
            }
            //save to MSubLoanOtherChargers other charges list
            if (otherChargesId.length > 0) {
                if (otherChargesId[0] > 0) {
                    String del = "delete from MSubLoanOtherChargers where MSubLoanType.subLoanId = " + subLoanType.getSubLoanId();
                    session.createQuery(del).executeUpdate();

                    for (int r = 0; r < otherChargesId.length; r++) {
                        MSubLoanOtherChargers chargers = new MSubLoanOtherChargers();
                        chargers.setActionTime(date);
                        chargers.setUserId(userId);
                        chargers.setMSubLoanType(subLoanType);
                        String sql = "from MSubTaxCharges where subTaxId =" + otherChargesId[r];
                        MSubTaxCharges subTaxCharges = (MSubTaxCharges) session.createQuery(sql).uniqueResult();
                        chargers.setMSubTaxCharges(subTaxCharges);
                        session.saveOrUpdate(chargers);
                    }
                } else {
                    String del = "delete from MSubLoanOtherChargers where MSubLoanType.subLoanId = " + subLoanType.getSubLoanId();
                    session.createQuery(del).executeUpdate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } catch (Throwable ex) {
            ex.printStackTrace();
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            transaction.commit();
            session.flush();
            session.close();
        }

    }

    @Override
    public List loadSubLoanType() {
        List subLoanTypeList = null;
        try {
            String sql = "";
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    sql = "from MSubLoanType";
                } else {
                    sql = "from MSubLoanType where isActive = 1";
                }
            }
            subLoanTypeList = sessionFactory.getCurrentSession().createQuery(sql).list();
            return subLoanTypeList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List loadUserType() {
        List userType = null;
        try {
            String sql = "from UmUserType";
            userType = sessionFactory.getCurrentSession().createQuery(sql).list();
            return userType;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean saveUser(UmUser user) {
        boolean flag = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        String userRole = "ROLE_ADMIN";
        UmUser uu = null;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encodePassword = passwordEncoder.encode(user.getPassword());

            Encription encrypt = new Encription();
            String sha1Password = encrypt.encryptPassword(user.getPassword());

            String sql = "from UmUser where userId = " + user.getUserId();
            uu = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            String username = "";
            if (uu != null) {
                username = uu.getUserName();
            }

            //find employee id
            String sql3 = "From Employee where userName='" + username + "'";
            Employee emp = (Employee) session.createQuery(sql3).uniqueResult();

            int uId = 0;
            if (emp != null) {
                String sql4 = "From UmUser where empId=" + emp.getEmpNo();
                user = (UmUser) session.createQuery(sql4).uniqueResult();
                if (user != null) {
                    user.setActionTime(date);
                    user.setUId(userId);
                    user.setPassword(encodePassword);
                    session.update(user);
                    uId = user.getUserId();

                    String sql_sp = "from UmSpecialPassword where userId=" + uId;
                    UmSpecialPassword special = (UmSpecialPassword) session.createQuery(sql_sp).uniqueResult();
                    if (special != null) {
                        special.setUserPassword(sha1Password);
                        session.update(special);
                    } else {
                        UmSpecialPassword special1 = new UmSpecialPassword();
                        special1.setUserId(uId);
                        special1.setUserPassword(sha1Password);
                        session.saveOrUpdate(special1);
                    }

                    String sql_roll = "from UmUserRole where userName= '" + username + "'";
                    UmUserRole umRoll = (UmUserRole) session.createQuery(sql_roll).uniqueResult();
                    if (umRoll != null) {
                        umRoll.setUserRole(userRole);
                        session.update(umRoll);
                    } else {
                        UmUserRole role = new UmUserRole();
                        role.setUserName(username);
                        role.setUserRole(userRole);
                        session.saveOrUpdate(role);
                    }

                }

            }
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        transaction.commit();

        session.flush();

        session.close();
        return flag;
    }

    @Override
    public List loadUsers() {
        List users = null;
        UmUser user = null;
        //SELECT u.User_ID,u.User_Name,ut.User_Type_Name FROM um_user AS u , um_user_type AS ut WHERE u.User_Type_ID = ut.User_Type_ID ORDER BY u.User_Name ASC
        try {
            int userId = userService.findByUserName();
            String sql1 = "from UmUser where userId = " + userId;
            user = (UmUser) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (user.getUserTypeId() == 18) {
                String sql = "select u.userId, u.userName, ut.userTypeName from UmUser as u,UmUserType as ut where u.userTypeId = ut.userTypeId and u.isActive = 1 ORDER BY u.userName ASC";
                users = sessionFactory.getCurrentSession().createQuery(sql).list();
                return users;

            }
            if (user.getUserTypeId() == 1) {
                String sql = "select u.userId, u.userName, ut.userTypeName from UmUser as u,UmUserType as ut where u.userTypeId = ut.userTypeId and u.isActive = 1 and ut.userTypeId !=18 ORDER BY u.userName ASC";
                users = sessionFactory.getCurrentSession().createQuery(sql).list();
                return users;

            }
            if (user.getUserId() == 7) {
                String sql = "select u.userId, u.userName, ut.userTypeName from UmUser as u,UmUserType as ut where u.userTypeId = ut.userTypeId and u.isActive = 1 ORDER BY u.userName ASC";
                users = sessionFactory.getCurrentSession().createQuery(sql).list();
                return users;
            } else {
                String sql = "select u.userId, u.userName, ut.userTypeName from UmUser as u,UmUserType as ut where u.userTypeId = ut.userTypeId and u.isActive = 1 and u.userId = " + user.getUserId();
                users = sessionFactory.getCurrentSession().createQuery(sql).list();
                return users;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List loadCheckList() {
        List checkList = null;
        try {
            String sql = "from MCheckList";
            checkList = sessionFactory.getCurrentSession().createQuery(sql).list();
            return checkList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MLoanType findMLoanType(int loanTypeId) {
        MLoanType loanType = null;
        try {
            String sql = "from MLoanType where loanTypeId= " + loanTypeId;
            loanType = (MLoanType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanType;
    }

    @Override
    public List<MSubLoanChecklist> findCheckList(int subLoanId) {
        List<MSubLoanChecklist> checkList = null;
        try {
            String sql = "from MSubLoanChecklist as mc where mc.MSubLoanType.subLoanId = " + subLoanId;
            checkList = (List<MSubLoanChecklist>) sessionFactory.getCurrentSession().createQuery(sql).list();
            return checkList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<MApproveLevels> loadAppLevel() {
        List<MApproveLevels> approveLevelses = null;
        try {
            String sql = "from MApproveLevels";
            approveLevelses = sessionFactory.getCurrentSession().createQuery(sql).list();
            return approveLevelses;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UmUserTypeApproval> findAppLevels() {
        List<UmUserTypeApproval> approvals = null;
        String sql = "";
        try {
            int userId = userService.findByUserName();
            int userType = userService.findUserType(userId);
            if (userType == 18) {
                sql = "select utl.pk,utl.userId from UmUserTypeApproval utl,UmUser u where utl.userId = u.userId group by utl.userId";
            } else {
                sql = "select utl.pk,utl.userId from UmUserTypeApproval utl,UmUser u where utl.userId = u.userId and u.userTypeId != 18  group by utl.userId";
            }
            approvals = sessionFactory.getCurrentSession().createQuery(sql).list();
            return approvals;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void saveOrUpdateAppLevels(UmUserTypeApproval userApproval, int[] appLevels) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            String del = "delete from UmUserTypeApproval where userId = " + userApproval.getUserId();
            session.createQuery(del).executeUpdate();
            if (appLevels.length > 0) {
                for (int i = 0; i < appLevels.length; i++) {
                    UmUserTypeApproval app = new UmUserTypeApproval();
                    Calendar calendar = Calendar.getInstance();
                    Date date = calendar.getTime();
                    int UserId = userService.findByUserName();
                    app.setActionTime(date);
                    app.setEdituserId(UserId);
                    String sql = "from MApproveLevels where appId = " + appLevels[i];
                    MApproveLevels levels = (MApproveLevels) session.createQuery(sql).uniqueResult();
                    app.setMApproveLevels(levels);
                    app.setUserId(userApproval.getUserId());
                    session.saveOrUpdate(app);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
    }

    @Override
    public void saveOrUpdateUserSubLoan(UmUserTypeLoan umUserTypeLoan, int[] subLoanIdList) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            if (subLoanIdList.length > 0) {
                if (subLoanIdList[0] > 0) {
                    for (int i = 0; i < subLoanIdList.length; i++) {
                        String del = "delete from UmUserTypeLoan where MSubLoanType.subLoanId = " + umUserTypeLoan.getPk();
                        session.createQuery(del).executeUpdate();

                        UmUserTypeLoan userTypeLoan = new UmUserTypeLoan();
                        Calendar calendar = Calendar.getInstance();
                        Date date = calendar.getTime();
                        int UserId = userService.findByUserName();
                        userTypeLoan.setActionTime(date);
                        userTypeLoan.setEditUserId(UserId);
                        String sql = "from MSubLoanType where subLoanId = " + subLoanIdList[i];
                        MSubLoanType typeLoan = (MSubLoanType) session.createQuery(sql).uniqueResult();
                        userTypeLoan.setMSubLoanType(typeLoan);
                        userTypeLoan.setBranchId(umUserTypeLoan.getBranchId());
                        userTypeLoan.setDepId(umUserTypeLoan.getDepId());
                        userTypeLoan.setUserId(umUserTypeLoan.getUserId());
                        session.saveOrUpdate(userTypeLoan);
                    }
                } else {
                    String del = "delete from UmUserTypeLoan where MSubLoanType.subLoanId = " + umUserTypeLoan.getPk();
                    session.createQuery(del).executeUpdate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
    }

    @Override
    public Integer saveOrUpdateEmployee(Employee employee) {
        Session session = null;
        Transaction tx = null;
        int empId = 0;
        UmUser umUser = new UmUser();

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            employee.setActionTime(date);
            employee.setUserId(userId);
            employee.setIsActive(true);
            session.saveOrUpdate(employee);
            empId = employee.getEmpNo();

            if (employee.getUserDefind() == 1) {
                String sql = "from UmUser where empId = " + employee.getEmpNo();
                UmUser isUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
                if (isUser != null) {
                    umUser.setUserId(isUser.getUserId());
                    umUser.setPassword(isUser.getPassword());
                }
                umUser.setEmpId(employee.getEmpNo());
                umUser.setUserTypeId(employee.getUserType());
                umUser.setUserName(employee.getUserName());
                umUser.setUId(userId);
                session.saveOrUpdate(umUser);
            }
            if (employee.getUserDefind() == 0) {
                String sql = "from UmUser where empId = " + employee.getEmpNo();
                UmUser isUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
                if (isUser != null) {
                    umUser.setUserId(isUser.getUserId());
                    umUser.setPassword(isUser.getPassword());
                }
                umUser.setEmpId(employee.getEmpNo());
                umUser.setUserTypeId(0);
                umUser.setUserName(employee.getUserName());
                umUser.setUId(userId);
                session.saveOrUpdate(umUser);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return empId;
    }

    @Override
    public void saveOrUpdateDocumetList(MCheckList checkList) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar c = Calendar.getInstance();
            Date date = c.getTime();
            int usetId = userService.findByUserName();
            checkList.setActionTime(date);
            checkList.setUserId(usetId);
            session.saveOrUpdate(checkList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
    }

    @Override
    public void saveUpdateOtherCharges(MSubTaxCharges charges) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            charges.setActionTime(date);
            charges.setUserId(userId);
            if (charges.getIsPrecentage() == null) {
                charges.setIsPrecentage(false);
            }
            session.saveOrUpdate(charges);
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
    }

    @Override
    public List<MSubTaxCharges> findOtherCharges() {
        List<MSubTaxCharges> chargeses = null;
        try {
            String sql = "from MSubTaxCharges";
            chargeses = sessionFactory.getCurrentSession().createQuery(sql).list();
            return chargeses;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MSubLoanType findMSubLoanType(int subLoanId) {
        MSubLoanType subLoanType = null;
        try {
            String sql = "from MSubLoanType where subLoanId = " + subLoanId;
            subLoanType = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subLoanType;
    }

    @Override
    public List<MSubLoanOtherChargers> findOtherChargeList(int subLoanId) {
        List<MSubLoanOtherChargers> loanOtherChargers = null;
        try {
            String sql = "from MSubLoanOtherChargers as mo where mo.MSubLoanType.subLoanId = " + subLoanId;
            loanOtherChargers = sessionFactory.getCurrentSession().createQuery(sql).list();
            return loanOtherChargers;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Employee> findEmployeeList() {
        List<Employee> employees = null;
        try {
            String sql = "from Employee order by empNo desc";
            employees = sessionFactory.getCurrentSession().createQuery(sql).list();
            return employees;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Employee findEmployee(int employeeId) {
        Employee employee = null;
        try {
            String sql = "from Employee where empNo = " + employeeId;
            employee = (Employee) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }

    @Override
    public UmUser findEmpUser(int employeeId) {
        UmUser umUser = null;
        try {
            String sql = "from UmUser where empId = " + employeeId;
            umUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return umUser;
    }

    @Override
    public List findUserName(String userName) {

        List users = new ArrayList<>();
        int depId = 1;
        try {
            //SELECT * FROM Employee WHERE (User_Name = 'admin' AND Dep_ID = '1')
            String sql = "from Employee where userName ='" + userName + "'";
            users = sessionFactory.getCurrentSession().createQuery(sql).list();
            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MBranch findActiveBranch(int branchId) {
        MBranch branchs = null;
        try {
            String sql = "from MBranch where branchId = " + branchId + " and isActive = 1";
            branchs = (MBranch) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            return branchs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<MBranch> findBranch() {
        List<MBranch> branchs = null;
        Session session = sessionFactory.openSession();
        try {
            String sql = "from MBranch where isActive = 1";
            branchs = (List<MBranch>) session.createQuery(sql).list();
//            branchs = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
            session.clear();
            session.close();
            return branchs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MWeightunits findWeightType() {
        MWeightunits weights = null;
        Session session = sessionFactory.openSession();
        try {
            String sql = "from MWeightunits";
            weights= (MWeightunits) session.createQuery(sql).uniqueResult();
//            branchs = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
            session.clear();
            session.close();
            return weights;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<MDepartment> finfDepartment(int branchId) {
        List<MDepartment> departments = null;
        try {
            String sql = "from MDepartment where branchId = " + branchId;
            departments = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return departments;

    }

    @Override
    public List<UmUser> findAllUser() {
        List<UmUser> users = null;
        try {
            String sql = "from UmUser where isActive = 1";
            users = sessionFactory.getCurrentSession().createQuery(sql).list();
            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UmUser> findUserWithOutSuperAdmin() {
        List<UmUser> umUsers = null;
        String sql = "";
        try {
            int userId = userService.findByUserName();
            int userType = userService.findUserType(userId);
            if (userType == 18) {
                sql = "from UmUser where isActive = 1";
            } else {
                sql = "from UmUser where isActive = 1 and userTypeId != 18";
            }
            umUsers = sessionFactory.getCurrentSession().createQuery(sql).list();
            return umUsers;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<MDepartment> findDepartmentList() {
        List<MDepartment> departments = null;
        try {
            String sql = "from MDepartment";
            departments = sessionFactory.getCurrentSession().createQuery(sql).list();
            return departments;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UmUserTypeApproval> findApproLevels(int userId) {
        List<UmUserTypeApproval> umUserTypeApproval = null;
        try {
            String sql = "from UmUserTypeApproval where userId = " + userId;
            umUserTypeApproval = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return umUserTypeApproval;
    }

    @Override
    public List<UmUserTypeLoan> findUserTypeSubLoans() {
        List<UmUserTypeLoan> typeLoans = null;
        String sql = "";
        try {
            int userId = userService.findByUserName();
            int userType = userService.findUserType(userId);
            if (userType == 18) {
                sql = "select utl.userId from UmUserTypeLoan utl,UmUser u where utl.userId = u.userId group by utl.userId";
            } else {
                sql = "select utl.userId from UmUserTypeLoan utl,UmUser u where utl.userId = u.userId and u.userTypeId != 18 group by utl.userId";
            }

            typeLoans = sessionFactory.getCurrentSession().createQuery(sql).list();
            return typeLoans;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MSubTaxCharges findOtherChargesList(int otherChargeId) throws Exception {
        MSubTaxCharges chargeses = null;
        try {
            String sql = "from MSubTaxCharges where subTaxId = " + otherChargeId;
            chargeses = (MSubTaxCharges) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            return chargeses;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int saveUserType(UmUserType userType) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        int typeId = 0;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            userType.setActionTime(date);
            if (userType.getIsBranch() == 0) {
                userType.setUserId(userId);
                userType.setNumOfBranch(0);
                session.saveOrUpdate(userType);
                typeId = userType.getUserTypeId();
            } else {
                userType.setUserId(userId);
                session.saveOrUpdate(userType);
                typeId = userType.getUserTypeId();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return typeId;
    }

    @Override
    public List<UmUserType> viewUserType() {
        List<UmUserType> umUserTypes = null;
        try {
            String sql = "from UmUserType";
            umUserTypes = sessionFactory.getCurrentSession().createQuery(sql).list();
            return umUserTypes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public UmUserType findEditUserType(int userTypeId) {
        UmUserType umUserType = null;
        try {
            String sql = "from UmUserType where userTypeId = " + userTypeId;
            umUserType = (UmUserType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            return umUserType;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<AMaintab> findMainTabs() {
        List<AMaintab> aMaintabs = new ArrayList();
        try {
            String sql = "from AMaintab where isActive = 1";
            aMaintabs = sessionFactory.getCurrentSession().createQuery(sql).list();
            return aMaintabs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ASubtab> findSubTabs(int mainTabId) {
        List<ASubtab> aSubtabs = null;
        try {
            String sql = "from ASubtab where mainTabId = " + mainTabId + " AND isActive = 1";
            aSubtabs = sessionFactory.getCurrentSession().createQuery(sql).list();
            return aSubtabs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int findMainTabBySubId(int subId) {
        int mainTabId = 0;
        try {
            String sql = "select su.mainTabId from ASubtab as su where su.subTabNo = " + subId;
            Object obj = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                mainTabId = Integer.parseInt(obj.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainTabId;
    }

    @Override
    public List<AddJobsForm> findJobList() {
        List<AddJobsForm> jobList = new ArrayList();
        int x = 0;
        try {
            List<AMaintab> mainTabs = findMainTabs();
            for (int i = 0; i < mainTabs.size(); i++) {
                List<ASubtab> subTabs = findSubTabs(mainTabs.get(i).getMainTabId());
                if (subTabs.isEmpty()) {
                    AddJobsForm addJobsForm = new AddJobsForm();
                    addJobsForm.setIndex(x);
                    addJobsForm.setTabId(mainTabs.get(i).getMainTabId());
                    addJobsForm.setTabName(mainTabs.get(i).getMainTabName());
                    addJobsForm.setIsMainTab(0);
                    jobList.add(addJobsForm);
                    addJobsForm = null;
                    x++;
                } else {
                    for (int s = 0; s < subTabs.size(); s++) {
                        AddJobsForm addJobsForm2 = new AddJobsForm();
                        addJobsForm2.setIndex(x);
                        addJobsForm2.setTabId(subTabs.get(s).getSubTabNo());
                        addJobsForm2.setTabName(subTabs.get(s).getSubTabName());
                        addJobsForm2.setIsMainTab(1);
                        jobList.add(addJobsForm2);
                        addJobsForm2 = null;
                        x++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jobList;
    }

    @Override
    public List<AddJobsForm> findDefineJobs(int userTypeId) {
        List<AJobDefine> aJobDefines = null;
        List<AddJobsForm> addJobsForms = new ArrayList();
        try {
            String sql = "from AJobDefine where refUserId = " + userTypeId;
            aJobDefines = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (aJobDefines.size() > 0) {
                for (int i = 0; i < aJobDefines.size(); i++) {
                    AddJobsForm addJobsForm = new AddJobsForm();
                    addJobsForm.setIndex(aJobDefines.get(i).getJobPk());
                    if (aJobDefines.get(i).getIsMainTab() == 0) {
                        addJobsForm.setTabId(aJobDefines.get(i).getMainTabId());
                        addJobsForm.setIsMainTab(0);
                    } else {
                        addJobsForm.setTabId(aJobDefines.get(i).getSubTabId());
                        addJobsForm.setIsMainTab(1);
                    }
                    addJobsForms.add(addJobsForm);
                    addJobsForm = null;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return addJobsForms;
    }

    @Override
    public boolean saveAddPages(AddJobsForm jobPages, String[] tabId, String[] isMain) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        boolean result = false;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();

            String del = "";
            if (jobPages.getIsUserType() == 1) {
                Integer empId = jobPages.getEmpId();
                UmUser customUser = findUserForEmpID(empId);
                if (customUser != null) {
                    del = "delete from AJobDefine where refUserId=" + customUser.getUserId() + " and isCustom=" + jobPages.getIsUserType();
                }
            } else {
                del = "delete from AJobDefine where refUserId=" + jobPages.getUserTypeId() + " and isCustom=" + jobPages.getIsUserType();
            }
            session.createQuery(del).executeUpdate();

            for (int i = 0; i < tabId.length; i++) {
                if (Integer.parseInt(isMain[i]) == 0) {
                    AJobDefine aJobDefine = new AJobDefine();
                    aJobDefine.setRefUserId(jobPages.getUserTypeId());
                    aJobDefine.setMainTabId(Integer.parseInt(tabId[i]));
                    aJobDefine.setSubTabId(0);
                    aJobDefine.setIsMainTab(0);
                    aJobDefine.setActionTime(date);
                    aJobDefine.setUserId(userId);
                    aJobDefine.setIsCustom(jobPages.getIsUserType());
                    session.saveOrUpdate(aJobDefine);
                } else {
                    AJobDefine aJobDefine = new AJobDefine();
                    aJobDefine.setRefUserId(jobPages.getUserTypeId());
                    int mainTabId = findMainTabBySubId(Integer.parseInt(tabId[i]));
                    aJobDefine.setMainTabId(mainTabId);
                    aJobDefine.setSubTabId(Integer.parseInt(tabId[i]));
                    aJobDefine.setIsMainTab(1);
                    aJobDefine.setActionTime(date);
                    aJobDefine.setUserId(userId);
                    aJobDefine.setIsCustom(jobPages.getIsUserType());
                    session.saveOrUpdate(aJobDefine);
                }
            }
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return result;
    }

    @Override
    public int findNumOfBranch(int userTypeId) {
        int branchs = 0;
        UmUserType umUserType = null;
        try {
            String sql = "from UmUserType where userTypeId = " + userTypeId;
            umUserType = (UmUserType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            branchs = umUserType.getNumOfBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchs;
    }

    @Override
    public boolean saveUserTypeBranch(UmUserBranch userBranch, int[] userTypeBranch) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        boolean result = false;
        UmUser umUser = null;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            String sql = "from UmUser where empId = " + userBranch.getEmpID();
            umUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            if (umUser.getUserTypeId() == 0) {
                String del = "delete from UmUserBranch where userId=" + umUser.getUserId() + " and isCustom = " + 1;
                session.createQuery(del).executeUpdate();
            } else {
                String del = "delete from UmUserBranch where userId=" + umUser.getUserId() + " and isCustom=" + 0;
                session.createQuery(del).executeUpdate();
            }

            for (int i = 0; i < userTypeBranch.length; i++) {
                if (umUser.getUserTypeId() == 0) {
                    UmUserBranch umUserBranch = new UmUserBranch();
                    umUserBranch.setActionTime(date);
                    umUserBranch.setUId(userId);
                    umUserBranch.setIsCustom(1);
                    umUserBranch.setUserId(umUser.getUserId());
                    umUserBranch.setBranchId(userTypeBranch[i]);
                    session.saveOrUpdate(umUserBranch);
                } else {
                    UmUserBranch umUserBranch = new UmUserBranch();
                    umUserBranch.setActionTime(date);
                    umUserBranch.setUId(userId);
                    umUserBranch.setIsCustom(0);
                    umUserBranch.setUserId(umUser.getUserId());
                    umUserBranch.setBranchId(userTypeBranch[i]);
                    session.saveOrUpdate(umUserBranch);
                }
            }
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return result;
    }

    @Override
    public UmUser findUserForEmpID(int empID) {
        UmUser umUser = null;
        try {
            String sql = "from UmUser where empId = " + empID;
            umUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return umUser;
    }

    @Override
    public List<MDepartment> findDepartment() {
        List<MDepartment> department = null;
        try {
            String sql = "from MDepartment";
            department = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return department;
    }

    @Override
    public boolean saveUserDepartment(int branchId, int empID, int[] departments) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        boolean result = false;
        UmUser umUser = null;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();

            String sql = "from UmUser where empId = " + empID;
            umUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            // delete Existing department
            String del = "delete from UmUserDepartment where userId=" + umUser.getUserId() + "and branchId = " + branchId;
            session.createQuery(del).executeUpdate();
            for (int i = 0; i < departments.length; i++) {
                UmUserDepartment userDepartment = new UmUserDepartment();
                userDepartment.setActionTime(date);
                userDepartment.setUId(userId);
                int user_Id = umUser.getUserId();
                userDepartment.setUserId(user_Id);
                userDepartment.setBranchId(branchId);
                int dep_Id = departments[i];
                userDepartment.setDepId(dep_Id);
                session.saveOrUpdate(userDepartment);
                userDepartment = null;
            }
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return result;
    }

    @Override
    public boolean saveFieldOfficer(UmFieldOfficers formData, int[] officerID) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        boolean result = false;
        UmUser umUser = null;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();

            String del = "delete from UmFieldOfficers where managerId=" + formData.getManagerId() + "and branchId = " + formData.getBranchId();
            session.createQuery(del).executeUpdate();

            for (int i = 0; i < officerID.length; i++) {
                String sql = "from UmUser where userId = " + officerID[i];
                umUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

                UmFieldOfficers officers = new UmFieldOfficers();
                officers.setUId(userId);
                officers.setActionTime(date);
                officers.setManagerId(formData.getManagerId());
                officers.setBranchId(formData.getBranchId());
                officers.setUserId(officerID[i]);
                officers.setEmpId(umUser.getEmpId());
                session.saveOrUpdate(officers);
            }
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return result;
    }

    @Override
    public Integer saveNewBranch(MBranch branch) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        int branchId = 0;
        
        List<MSection> findSections = masterDataService.findSections();
        MSection mSection = null;
        for(MSection obj : findSections){
            mSection = obj;
        }
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            branch.setActionTime(date);
            branch.setUserId(userId);
            branch.setIsActive(true);
            branch.setIsDelete(false);
            branch.setMSection(mSection);
            session.saveOrUpdate(branch);
            branchId = branch.getBranchId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return branchId;
    }

    @Override
    public List<UmUserBranch> findUserBranch(int empID) {
        List<UmUserBranch> branchs = null;
        try {
            UmUser umUser = findUserForEmpID(empID);

            String sql = "from UmUserBranch where userId = " + umUser.getUserId();
            branchs = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchs;
    }

    @Override
    public List<UmUser> finfUmUser() {
        List<UmUser> umUsers = null;
        UmUser user = null;
        try {
            int userId = userService.findByUserName();
            String sql1 = "from UmUser where userId = " + userId;
            user = (UmUser) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (user.getUserTypeId() == 18) {
                String sql = "from UmUser where isActive = 1 ";
                umUsers = sessionFactory.getCurrentSession().createQuery(sql).list();
                return umUsers;
            }
            if (user.getUserTypeId() == 1) {
                String sql = "from UmUser where isActive = 1 and userTypeId != 18";
                umUsers = sessionFactory.getCurrentSession().createQuery(sql).list();
                return umUsers;
            }
            if (user.getUserTypeId() == 7) {
                String sql = "from UmUser where isActive = 1 and userTypeId != 18";
                umUsers = sessionFactory.getCurrentSession().createQuery(sql).list();
                return umUsers;
            } else {
                String sql = "from UmUser where isActive = 1 and userTypeId != 18";
                umUsers = sessionFactory.getCurrentSession().createQuery(sql).list();
                return umUsers;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    //Must be UserType Id = 6 is RecoverManager
    @Override
    public List<UmUser> findRManagers() {
        List<UmUser> recoManagers = null;
        int recoveryManagerID = 6;
        try {
            String sql = "from UmUser where userTypeId = " + recoveryManagerID;
            recoManagers = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recoManagers;
    }

    @Override
    public List findManagerBranches(int managerUserId) {
        List managerBranch = null;
        try {
            String sql = "select b.branchId,b.branchName from MBranch as b,UmUserBranch as ub where ub.userId = " + managerUserId + " and ub.branchId = b.branchId";
            managerBranch = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return managerBranch;
    }

    @Override
    public List<UmUser> findOfficer() {
        List<UmUser> recoOfficer = null;
        int recoveryOfficerID = 4;
        try {
            String sql = "from UmUser where userTypeId = " + recoveryOfficerID;
            recoOfficer = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recoOfficer;
    }

    @Override
    public List<UmFieldOfficers> findRecoveryManageres() {
        List<UmFieldOfficers> fieldOfficerses = null;
        try {
            String sql = "from UmFieldOfficers uf group by uf.managerId";
            fieldOfficerses = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fieldOfficerses;
    }

    @Override
    public MBranch editBranch(int branchId) {
        MBranch mBranch = null;
        try {
            String sql = "from MBranch where branchId = " + branchId;
            mBranch = (MBranch) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mBranch;
    }

    @Override
    public Boolean inActiveBranch(int branchID) {
        MBranch inActiveBranch = null;
        Boolean inactive = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            String sql = "from MBranch where branchId = " + branchID;
            inActiveBranch = (MBranch) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            inActiveBranch.setIsActive(false);
            session.saveOrUpdate(inActiveBranch);
            inactive = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return inactive;
    }

    @Override
    public Boolean activeBranch(int branchID) {
        MBranch activeBranch = null;
        Boolean active = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            String sql = "from MBranch where branchId = " + branchID;
            activeBranch = (MBranch) session.createQuery(sql).uniqueResult();
//            activeBranch = (MBranch) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            activeBranch.setIsActive(true);
            session.saveOrUpdate(activeBranch);
            active = true;
            session.flush();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return active;
    }

    @Override
    public boolean saveDepartment(MDepartment department) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Boolean result = false;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            department.setActionTime(date);
            department.setUserId(userId);
            department.setIsActive(true);
            department.setIsDelete(false);
            session.saveOrUpdate(department);
            result = true;
            session.flush();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }
        session.close();
        return result;
    }

    @Override
    public Boolean inActiveEmployee(int empNo) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Boolean inActive = false;
        Employee employee = null;
        UmUser umUser = null;
        EmployeeHistory employeeHistory = new EmployeeHistory();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            String sql = "from Employee where empNo = " + empNo;
            employee = (Employee) session.createQuery(sql).uniqueResult();
            employee.setIsActive(false);
            session.saveOrUpdate(employee);

            String sql1 = "from UmUser where empId = " + empNo;
            umUser = (UmUser) session.createQuery(sql1).uniqueResult();
            umUser.setIsActive(false);
            session.saveOrUpdate(umUser);

            employeeHistory.setEmpNo(employee.getEmpNo());
            employeeHistory.setInActiveDate(date);
            employeeHistory.setUserId(userId);
            session.save(employeeHistory);
            transaction.commit();
            inActive = true;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return inActive;
    }

    @Override
    public Boolean activeEmployee(int empNo) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Boolean active = false;
        Employee employee = null;
        UmUser umUser = null;
        EmployeeHistory employeeHistory = new EmployeeHistory();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            String sql = "from Employee where empNo = " + empNo;
            employee = (Employee) session.createQuery(sql).uniqueResult();
            employee.setIsActive(true);
            session.saveOrUpdate(employee);

            String sql1 = "from UmUser where empId = " + empNo;
            umUser = (UmUser) session.createQuery(sql1).uniqueResult();
            umUser.setIsActive(true);
            session.saveOrUpdate(umUser);

            employeeHistory.setEmpNo(employee.getEmpNo());
            employeeHistory.setActiveDate(date);
            employeeHistory.setUserId(userId);
            session.save(employeeHistory);
            transaction.commit();
            active = true;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return active;
    }

    @Override
    public UmUser findUserName(int userId) {
        UmUser umUser = null;
        try {
            String sql = "from UmUser where isActive = 1 and userId = " + userId;
            umUser = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return umUser;
    }

    @Override
    public List<UmUserBranch> findUserBranches(Integer userId) {
        List<UmUserBranch> userBranches = null;
        try {
            String sql = "from UmUserBranch where userId = " + userId;
            userBranches = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userBranches;
    }

    @Override
    public String findBranchName(int branchId) {
        String branchName = "";
        MBranch mBranch = null;
        try {
            StatelessSession session = sessionFactory.openStatelessSession();
            mBranch = (MBranch) session.createCriteria(MBranch.class).add(Restrictions.eq("branchId", branchId)).uniqueResult();
            branchName = mBranch.getBranchName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchName;
    }

    @Override
    public List<ASubtab> findSubTabs() {
        List<ASubtab> aSubtabs = new ArrayList();
        try {
            String sql = "from ASubtab";
            aSubtabs = sessionFactory.getCurrentSession().createQuery(sql).list();
            return aSubtabs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int findLoanTypeBySubLoanType(MSubLoanType mSubLoanType) {
        int loanTypeId = 0;
        try {
            String sql = "from MLoanType where loanTypeId =" + mSubLoanType.getLoanTypeId();
            MLoanType loanType = (MLoanType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            loanTypeId = loanType.getLoanTypeId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanTypeId;
    }

    @Override
    public List<ConfigChartofaccountLoan> findConfigChartOfAccounts(int subLoanId) {
        List<ConfigChartofaccountLoan> configChartofaccountLoans = null;
        try {
            // String sql = "from ConfigChartofaccountLoan where sub_loan_type = " +subLoanId;
            configChartofaccountLoans = sessionFactory.getCurrentSession().createCriteria(ConfigChartofaccountLoan.class)
                    .add(Restrictions.eq("subLoanType", subLoanId)).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return configChartofaccountLoans;
    }

    @Override
    public Boolean checkSubLoanType(String subLoanType) {
        Boolean isExist = false;
        try {
            String sql = "from MSubLoanType where subLoanName = '" + subLoanType + "'";
            MSubLoanType mSubLoanType = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (mSubLoanType != null) {
                isExist = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isExist;
    }

    @Override
    public Integer saveNewSupplier(MSupplier mSupplier) {
        int supplierId = 0;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            mSupplier.setIsActive(true);
            mSupplier.setIsDelete(false);
            mSupplier.setActionTime(Calendar.getInstance().getTime());
            int userId = userService.findByUserName();
            if (userId != 0) {
                mSupplier.setUserId(userId);
            }
            session.saveOrUpdate(mSupplier);
            supplierId = mSupplier.getSupplierId();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return supplierId;
    }

    @Override
    public MSupplier findSupplier(int supplierId) {
        MSupplier mSupplier = null;
        try {
            String sql = "from MSupplier where supplierId = " + supplierId;
            mSupplier = (MSupplier) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return mSupplier;
    }

    @Override
    public List<MSupplier> loadSuppliers() {
        List<MSupplier> mSuppliers = null;
        try {
            String sql = "from MSupplier";
            mSuppliers = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (mSuppliers.size() > 0) {
                mSuppliers.remove(0);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return mSuppliers;
    }

    @Override
    public RecOffCollDates hasCollectionDates(int recOffId, int branchId) {
        RecOffCollDates rocd = null;
        try {
            String sql = "from RecOffCollDates where empId = :emp and branchId = :branch";
            rocd = (RecOffCollDates) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("emp", recOffId).setParameter("branch", branchId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return rocd;
    }

    @Override
    public List<RecOffCollDates> getCollectionDates(int branchId) {
        List<RecOffCollDates> rocds = null;
        try {
            String sql = "from RecOffCollDates where branchId = :branch";
            rocds = (List<RecOffCollDates>) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("branch", branchId).list();
            System.out.println(rocds);
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return rocds;
    }

    @Override
    public Integer saveOrUpdateCollectionDates(List<RecOffColDateForm> list, int branchId) {
        int id = 1;
        int userId = userService.findByUserName();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            if (list.size() > 0) {
                List<Integer> recOffIds = new ArrayList<>();
                for (RecOffColDateForm get : list) {
                    if (!recOffIds.isEmpty()) {
                        if (!recOffIds.contains(get.getRecOffId())) {
                            recOffIds.add(get.getRecOffId());
                        }
                    } else {
                        recOffIds.add(get.getRecOffId());
                    }
                }
                for (Integer recoffId : recOffIds) {
                    String clcDates = "";
                    Iterator<RecOffColDateForm> iterator = list.iterator();
                    while (iterator.hasNext()) {
                        RecOffColDateForm recOffColDateForm = iterator.next();
                        if (recOffColDateForm.getRecOffId() == recoffId) {
                            clcDates = clcDates + "," + String.valueOf(recOffColDateForm.getDate());
                        }
                    }
                    String sql = "delete from RecOffCollDates where empId =" + recoffId + " AND branchId =" + branchId;
                    session.createQuery(sql).executeUpdate();

                    RecOffCollDates rocd = new RecOffCollDates();
                    rocd.setActionTime(Calendar.getInstance().getTime());
                    rocd.setBranchId(branchId);
                    rocd.setCollectionDates(clcDates);
                    rocd.setEmpId(recoffId);
                    rocd.setUserId(userId);

                    session.saveOrUpdate(rocd);

                }
            }
        } catch (HibernateException e) {
            id = 0;
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return id;
    }

    @Override
    public MCheckList findDocument(int docId) {
        MCheckList mcl = null;
        try {
            String sql = "from MCheckList where id = " + docId;
            mcl = (MCheckList) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mcl;
    }

    @Override
    public List<UmUserTypeLoan> findUserTypeLoanList(int userId) {
        List<UmUserTypeLoan> userTypeLoans = null;
        try {
            String sql = "from UmUserTypeLoan where userId = " + userId;
            userTypeLoans = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userTypeLoans;
    }

    @Override
    public List<Chartofaccount> getChartOfAccounts() {
        List<Chartofaccount> chartofaccounts = new ArrayList<>();
        Session openSession = financeSessionFactory.openSession();
        try {
            String sql = "from Chartofaccount";
            List bankFinanceAccounts = sessionFactory.getCurrentSession().createQuery(sql).list();
            List financeAccounts = openSession.createQuery(sql).list();
            boolean isAccountExist = false;
            for (Object financeAcc : financeAccounts) {
                com.ites.finance.model.Chartofaccount fin = (com.ites.finance.model.Chartofaccount) financeAcc;
                for (Object bankFinanceAcc : bankFinanceAccounts) {
                    com.ites.bankfinance.model.Chartofaccount bfin = (com.ites.bankfinance.model.Chartofaccount) bankFinanceAcc;
                    if (fin.getAccountNo().equals(bfin.getAccountNo())) {
                        isAccountExist = true;
                        break;
                    } else {
                        isAccountExist = false;
                    }
                }
                if (!isAccountExist) {
                    Chartofaccount c = new Chartofaccount();
                    c.setAcSubCode(fin.getAcSubCode());
                    c.setAccountType(fin.getAccountType());
                    c.setDescription(fin.getDescription());
                    c.setAccountNo(fin.getAccountNo());
                    c.setAccountName(fin.getAccountName());
                    c.setRepresentDept(fin.getRepresentDept());
                    chartofaccounts.add(c);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            openSession.flush();
            openSession.close();
        }
        return chartofaccounts;
    }

    @Override
    public boolean saveChartOfAccounts(List<Chartofaccount> cs) {
        boolean sucess = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        Date currDate = Calendar.getInstance().getTime();
        int userName = userService.findByUserName();
        try {
            transaction = session.beginTransaction();
            for (int i = 0; i < cs.size(); i++) {
                Chartofaccount c = cs.get(i);
                c.setCreatedUserId(userName);
                c.setCurrDate(currDate);
                session.save(c);
                if (i % 20 == 0) { //20(batch-size),Release memory after batch inserts.
                    session.flush();
                    session.clear();
                }
            }
            transaction.commit();
            sucess = true;
        } catch (HibernateException e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return sucess;
    }

    @Override
    public List<MSeizerDetails> findSeizerDetailsList() {
        List<MSeizerDetails> detailses = null;
        try {
            detailses = sessionFactory.getCurrentSession().createCriteria(MSeizerDetails.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detailses;
    }

    @Override
    public Boolean inActiveSeizer(int seizerId) {
        Boolean inActive = false;
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {
            MSeizerDetails details = (MSeizerDetails) session.createCriteria(MSeizerDetails.class)
                    .add(Restrictions.eq("seizerId", seizerId)).uniqueResult();
            details.setIsActive(false);
            session.update(details);
            inActive = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            session.flush();
            t.commit();
        }catch(Exception e){
            t.rollback();
            e.printStackTrace();
        }
        session.close();
        return inActive;
    }

    @Override
    public Boolean activeSeizer(int seizerId) {
        Boolean active = false;
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {
            MSeizerDetails details = (MSeizerDetails) session.createCriteria(MSeizerDetails.class)
                    .add(Restrictions.eq("seizerId", seizerId)).uniqueResult();
            details.setIsActive(true);
            session.update(details);
            active = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
            session.flush();
            t.commit();
        }catch(Exception e){
            e.printStackTrace();
            t.rollback();
        }
        session.close();
        return active;
    }

    @Override
    public Boolean saveOrUpdateSeizerDetails(MSeizerDetails seizerDetails) {
        Boolean isSave = false;
        Session session = null;
        Transaction tx = null;
        int userId = userService.findByUserName();
        Date date = Calendar.getInstance().getTime();
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            if (seizerDetails.getSeizerAccountNo().isEmpty()) {
                // generate seizer account no
                String seizerAccountNo = generateSeizerAccountNo();
                seizerDetails.setSeizerAccountNo(seizerAccountNo);
                // create chart of account for seizer
                Chartofaccount chartofaccount = new Chartofaccount();
                chartofaccount.setAcSubCode(0);
                chartofaccount.setAccountNo(seizerAccountNo);
                chartofaccount.setAccountType(1);
                chartofaccount.setAccountName(seizerDetails.getSeizerName());
                chartofaccount.setRepresentDept(0);
                chartofaccount.setCreatedUserId(userId);
                chartofaccount.setCurrDate(date);
                session.saveOrUpdate(chartofaccount);
            }
            seizerDetails.setActionTime(date);
            seizerDetails.setUserId(userId);
            seizerDetails.setIsActive(true);
            session.saveOrUpdate(seizerDetails);
            tx.commit();
            isSave = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return isSave;
    }

    @Override
    public MSeizerDetails findSeizerDetailsList(int seizerID) {
        MSeizerDetails details = null;
        try {
            details = (MSeizerDetails) sessionFactory.getCurrentSession().createCriteria(MSeizerDetails.class)
                    .add(Restrictions.eq("seizerId", seizerID)).uniqueResult();

        } catch (Exception e) {
            return null;
        }
        return details;
    }

    @Override
    public boolean addOrRemoveModules(String[] modules, int status) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            if (status == 1) { // Active Modules
                for (String i : modules) {
                    String sql1 = "from ASubtab where subTabNo = " + i;
                    ASubtab aSubtab = (ASubtab) session.createQuery(sql1).uniqueResult();
                    if (aSubtab != null) {
                        String sql2 = "update AMaintab set isActive = 1 where mainTabId = " + aSubtab.getMainTabId();
                        session.createQuery(sql2).executeUpdate();
                        String sql3 = "update ASubtab set isActive = 1 where subTabNo = " + i;
                        session.createQuery(sql3).executeUpdate();
                    }
                }
            } else { // In-active Modules
                for (String i : modules) {
                    String sql1 = "from ASubtab where subTabNo = " + i;
                    ASubtab aSubtab = (ASubtab) session.createQuery(sql1).uniqueResult();
                    if (aSubtab != null) {
                        String sql2 = "update ASubtab set isActive = 0 where subTabNo = " + i;
                        session.createQuery(sql2).executeUpdate();
                        String sql3 = "delete from AJobDefine where subTabId = " + i;
                        session.createQuery(sql3).executeUpdate();
                    }
                }
            }
            tx.commit();
            success = tx.wasCommitted();
            if(success){

            }
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public boolean changeLoanType(int loanTypeId, int status) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql1;
            String sql2;
            if (status == 1) { // Active 
                sql1 = "update MLoanType set isActive = 1 where loanTypeId = " + loanTypeId;
                session.createQuery(sql1).executeUpdate();
                sql2 = "update MSubLoanType set isActive = 1 where loanTypeId = " + loanTypeId;
                session.createQuery(sql2).executeUpdate();
            } else {  // In-active
                sql1 = "update MLoanType set isActive = 0 where loanTypeId = " + loanTypeId;
                session.createQuery(sql1).executeUpdate();
                sql2 = "update MSubLoanType set isActive = 0 where loanTypeId = " + loanTypeId;
                session.createQuery(sql2).executeUpdate();
            }
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public boolean changeSubLoanType(int subLoanTypeId, int status) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String sql1;
            String sql2;
            String sql3;
            if (status == 1) { // Active 
                sql1 = "from MSubLoanType where subLoanId = " + subLoanTypeId;
                MSubLoanType mSubLoanType = (MSubLoanType) session.createQuery(sql1).uniqueResult();
                if (mSubLoanType != null) {
                    sql3 = "update MLoanType set isActive = 1 where loanTypeId = " + mSubLoanType.getLoanTypeId();
                    session.createQuery(sql3).executeUpdate();
                    sql2 = "update MSubLoanType set isActive = 1 where subLoanId = " + subLoanTypeId;
                    session.createQuery(sql2).executeUpdate();
                }
            } else {  // In-active
                sql1 = "update MSubLoanType set isActive = 0 where subLoanId = " + subLoanTypeId;
                session.createQuery(sql1).executeUpdate();
            }
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return success;
    }

    @Override
    public String findEmoloyeeLastName(Integer marketingOfficer) {
        String name = "";
        try {
            String sql = "select emp.empLname from Employee emp,UmUser um where emp.empNo = um.empId and um.userId = '" + marketingOfficer + "'";
            Object object = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (object != null) {
                name = object.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    @Override
    public MSystemValidation findSystemValidation() {
        MSystemValidation systemValidation = null;
        try {
            systemValidation = (MSystemValidation) sessionFactory.getCurrentSession().createCriteria(MSystemValidation.class).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return systemValidation;
    }

    @Override
    public int maxBranchCount() {
        int maxBarnchCount = 0;
        try {
            List<MBranch> branchs = sessionFactory.getCurrentSession().createCriteria(MBranch.class)
                    .add(Restrictions.eq("isActive", true)).list();
            maxBarnchCount = branchs.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxBarnchCount;
    }

    private String generateSeizerAccountNo() {
        String AccountNo = "S";
        String maxNo = "0";
        try {
            String sql = "select max(seizerAccountNo) from MSeizerDetails";
            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (ob != null) {
                maxNo = ob.toString().substring(1).replaceAll("^0+", "");
            }
            int mm = Integer.parseInt(maxNo) + 1;

            int noLength = 7 - String.valueOf(mm).length();
            for (int i = 0; i < noLength; i++) {
                AccountNo = AccountNo + "0";
            }

            if (noLength > 0) {
                AccountNo = AccountNo + mm;
            } else {
                AccountNo = AccountNo + 1;
            }
        } catch (NumberFormatException | HibernateException e) {
            e.printStackTrace();
        }
        return AccountNo;
    }

    @Override
    public Boolean saveOrUpdateYardDetails(MYardDetails yardDetails) {
        Boolean isSave = false;
        Session session = null;
        Transaction tx = null;

        int userId = userService.findByUserName();
        Date date = Calendar.getInstance().getTime();

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            yardDetails.setUserId(userId);
            yardDetails.setActionTime(date);
            yardDetails.setYardName(yardDetails.getYardName());
            yardDetails.setYardDescription(yardDetails.getYardDescription());
            yardDetails.setMaxVehicleCount(yardDetails.getMaxVehicleCount());

            session.saveOrUpdate(yardDetails);
            tx.commit();
            isSave = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return isSave;
    }

    @Override
    public List<MYardDetails> findYardDetailsList() {
        List<MYardDetails> detailses = null;
        try {
            detailses = sessionFactory.getCurrentSession().createCriteria(MYardDetails.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detailses;
    }

    @Override
    public MYardDetails findYardDetailsList(int yardID) {
        MYardDetails details = null;
        try {
            details = (MYardDetails) sessionFactory.getCurrentSession().createCriteria(MYardDetails.class)
                    .add(Restrictions.eq("yardId", yardID)).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return details;
    }

    @Override
    public Boolean inActiveYard(int yardId) {
        Boolean inActive = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            MYardDetails details = (MYardDetails) session.createCriteria(MYardDetails.class)
                    .add(Restrictions.eq("yardId", yardId)).uniqueResult();
            details.setIsActive(false);
            session.update(details);
            tx.commit();
            inActive = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return inActive;
    }

    @Override
    public Boolean activeYard(int yardId) {
        Boolean active = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            MYardDetails details = (MYardDetails) session.createCriteria(MYardDetails.class)
                    .add(Restrictions.eq("yardId", yardId)).uniqueResult();
            details.setIsActive(true);
            session.update(details);
            session.flush();
            tx.commit();
            active = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return active;
    }

    @Override
    public Boolean saveOrUpdateVehicleDetails(VehicleType vehicleType) {
        Boolean isSave = false;
        Session session = null;
        Transaction tx = null;

        int userId = userService.findByUserName();
        Date date = Calendar.getInstance().getTime();

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            vehicleType.setActionTime(date);
            vehicleType.setVehicleTypeName(vehicleType.getVehicleTypeName());
            vehicleType.setIsActive(true);

            session.saveOrUpdate(vehicleType);
            tx.commit();
            isSave = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return isSave;
    }

    @Override
    public List<VehicleType> findVehicleTypesList() {
        List<VehicleType> vehicleTypesList = null;
        try {
            vehicleTypesList = sessionFactory.getCurrentSession().createCriteria(VehicleType.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleTypesList;
    }

    @Override
    public VehicleType findVehicleTypeList(int vehicleTypeId) {
        VehicleType details = null;
        StatelessSession session = sessionFactory.openStatelessSession();
        try {
            details = (VehicleType) session.createCriteria(VehicleType.class)
                    .add(Restrictions.eq("vehicleType", vehicleTypeId)).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return details;
    }

    @Override
    public Boolean saveOrUpdateVehicleMakeDetails(VehicleMake vehicleMake) {

        Boolean isSave = false;
        Session session = null;
        Transaction tx = null;

        int userId = userService.findByUserName();
        Date date = Calendar.getInstance().getTime();

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            vehicleMake.setActionTime(date);
            vehicleMake.setIsActive(true);
            vehicleMake.setVehicleMakeName(vehicleMake.getVehicleMakeName());
            vehicleMake.setVehicleType(vehicleMake.getVehicleType());
            vehicleMake.setVehicleType(new VehicleType(vehicleMake.getVehicleTypeId()));

            session.saveOrUpdate(vehicleMake);
            tx.commit();
            isSave = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return isSave;
    }

    @Override
    public List<VehicleMake> findVehicleMakeList(int vehicleType) {
        List<VehicleMake> vehicleMakeList = null;
        try {
            String sql = "from VehicleMake where vehicleType = " + vehicleType;
            vehicleMakeList = sessionFactory.getCurrentSession().createQuery(sql).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleMakeList;
    }

    @Override
    public List<VehicleModel> findVehicleModelList(int vehicleTypeId) {
        List<VehicleModel> vehicleModelList = null;
        try {

            String sql = "from VehicleModel where vehicleType = " + vehicleTypeId;
            vehicleModelList = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleModelList;
    }

    @Override
    public Boolean saveOrUpdateVehicleModelDetail(VehicleModel vehicleModel) {
        Boolean isSave = false;
        Session session = null;
        Transaction tx = null;

        int userId = userService.findByUserName();
        Date date = Calendar.getInstance().getTime();

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            vehicleModel.setActionTime(date);
            vehicleModel.setIsActive(true);
            vehicleModel.setVehicleType(new VehicleType(vehicleModel.getVehicleTypeId()));
            vehicleModel.setVehicleMake(new VehicleMake(vehicleModel.getVehicleMakeId()));

            session.saveOrUpdate(vehicleModel);
            tx.commit();
            isSave = true;

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return isSave;
    }

    @Override
    public Boolean inActiveVehicle(int vehicleId) {
        Boolean inActive = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            VehicleType details = (VehicleType) session.createCriteria(VehicleType.class)
                    .add(Restrictions.eq("vehicleType", vehicleId)).uniqueResult();
            details.setIsActive(false);
            session.update(details);
            tx.commit();
            inActive = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return inActive;
    }

    @Override
    public Boolean activeVehicle(int vehiledId) {
        Boolean active = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            VehicleType details = (VehicleType) session.createCriteria(VehicleType.class)
                    .add(Restrictions.eq("vehicleType", vehiledId)).uniqueResult();
            details.setIsActive(true);
            session.update(details);
            session.flush();
            tx.commit();
            active = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return active;
    }

    @Override
    public List<MCenter> findCenter() {
        List<MCenter> center = null;
        try {
            String sql = "from MCenter";
            center = sessionFactory.getCurrentSession().createQuery(sql).list();

            return center;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Integer saveNewCenter(MCenter center) {
        System.out.println("====Process is been going throug Dao====");
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        int centerId = 0;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();
            center.setActionTime(date.toString());
            center.setUserId(userId);
            center.setIsActive(1);
            if (center.getBranchID() != 0) {
                center.setBranchID(center.getBranchID());
            }
            center.setCenterCode(center.getCenterCode());
            center.setCenterName(center.getCenterName());
            session.saveOrUpdate(center);
            centerId = center.getCenterID();

        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return centerId;
    }

    public Integer saveOrUpdateGoldValue(MWeightunits weightDetails){
        int weightID=0;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
    try {
        session.saveOrUpdate(weightDetails);
        weightID = weightDetails.getWeightId();
    }catch (Exception e){
        e.printStackTrace();
    }
        transaction.commit();
        session.flush();
        session.close();
        return weightID;
    }

    @Override
    public MCenter editCenter(int centerId) {
        MCenter mCenter = null;
        try {
            String sql = "from MCenter where centerID = " + centerId;
            mCenter = (MCenter) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mCenter;
    }

    @Override
    public boolean inActiveCenter(int centerID) {

        MCenter inActiveCenter = null;
        Boolean inactive = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            String sql = "from MCenter where centerID = " + centerID;
            inActiveCenter = (MCenter) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            inActiveCenter.setIsActive(0);
            session.saveOrUpdate(inActiveCenter);
            inactive = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return inactive;

    }

    @Override
    public Boolean activeCenter(int centerID) {

        MCenter center = null;
        Boolean active = false;
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {

            String sql = "from MCenter where centerID = " + centerID;
            center = (MCenter) session.createQuery(sql).uniqueResult();
//            center = (MCenter) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            center.setIsActive(1);
            session.saveOrUpdate(center);
            active = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
            session.flush();
            t.commit();
        }catch(Exception e){
            e.printStackTrace();
            t.rollback();
        }
//        session.clear();
        session.close();
        return active;

    }

    @Override
    public List<MGroup> findGroup() {

        List<MGroup> group = null;
        List<MCenter> center = null;
        try {
            String sql = "from MGroup";
            group = sessionFactory.getCurrentSession().createQuery(sql).list();

            return group;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer saveNewGroup(MGroup group) {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        int centerId = 0;
        try {

//            System.out.println("========================= branch ID in dao " + center.getBranchID());
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int userId = userService.findByUserName();

            group.setBranchID(group.getBranchID());
            group.setCenteID(group.getCenteID());
            group.setGroupName(group.getGroupName());
            group.setDetail(group.getDetail());
            group.setIsActive(1);
            group.setUserID(userId);
            group.setActionTime(date.toString());

//            group.setActionTime(date.toString());
//            group.setUserId(userId);
//            group.setIsActive(1);
//           
//                group.setBranchID(group.getBranchID());
//          
//            group.setCenterCode(group.getCenterCode());
//            group.setCenterName(group.getCenterName());
            session.saveOrUpdate(group);
            centerId = group.getGroupID();

        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return centerId;

    }

    @Override
    public MGroup editGroup(int groupID) {

        MGroup group = null;
        try {

            System.out.println("================== group ID : " + groupID);

            String sql = "from MGroup where groupID = " + groupID;
            group = (MGroup) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return group;

    }

    @Override
    public boolean inActiveGroup(int groupID) {

        MGroup inActiveGroup = null;
        Boolean inactive = false;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            String sql = "from MGroup where groupID = " + groupID;
            inActiveGroup = (MGroup) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            inActiveGroup.setIsActive(0);
            session.saveOrUpdate(inActiveGroup);
            inactive = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        transaction.commit();
        session.flush();
        session.close();
        return inactive;

    }

    @Override
    public Boolean activeGroup(int groupID) {
        MGroup group = null;
        Boolean active = false;
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();
        try {

            String sql = "from MGroup where groupID = " + groupID;
            group = (MGroup) session.createQuery(sql).uniqueResult();
//            group = (MGroup) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            group.setIsActive(1);
            session.saveOrUpdate(group);
            active = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
            session.flush();
            t.commit();
        }catch(Exception e){
            t.rollback();
            e.printStackTrace();
        }
        session.close();
        return active;
    }

    @Override
    public List<MCenter> findCenter(int branchID) {

        List<MCenter> center = null;
        try {
            String sql = "from MCenter where branchID='" + branchID + "'";
            center = sessionFactory.getCurrentSession().createQuery(sql).list();

            return center;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public List<MGroup> findGroup(int centerID) {
        List<MGroup> group = null;
        try {
            String sql = "from MGroup where centeID='" + centerID + "'";
            group = sessionFactory.getCurrentSession().createQuery(sql).list();

            return group;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<DebtorHeaderDetails> findGroupUsers(int groupID) {

        List<DebtorGroupDetails> debtorGroupDetailses = null;
        List<DebtorHeaderDetails> dhds = new ArrayList<>();
        DebtorHeaderDetails user = null;

        try {
            String sql = "from DebtorGroupDetails where mGroupId='" + groupID + "'";
            debtorGroupDetailses = sessionFactory.getCurrentSession().createQuery(sql).list();

            for (DebtorGroupDetails dt : debtorGroupDetailses) {
                String sql2 = " from  DebtorHeaderDetails where debtorId ='" + dt.getDebtorId() + "' ";
                user = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                dhds.add(user);
            }

            return dhds;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean changeAccountType(int accountTypeId, int status) {
        boolean success = false;
        return success;
    }

    @Override
    public boolean changeSubAccountType(int subAccountTypeId, int status) {
        boolean success = false;
        return success;
    }


}

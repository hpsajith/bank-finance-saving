/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.ActiveContracts;
import com.bankfinance.form.PortfolioDetail;
import com.bankfinance.form.PortfolioDetails;
import com.bankfinance.form.RmvReceivable;
import com.bankfinance.form.ServiceChargeReceivable;
import com.ites.bankfinance.dao.ReportDao;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.InsuranceCompany;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanInstalmentSchedule;
import com.ites.bankfinance.model.LoanOtherCharges;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.LoanRecoveryReport;
import com.ites.bankfinance.model.MCompany;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.OpenD;
import com.ites.bankfinance.model.SettlementCharges;
import com.ites.bankfinance.model.SettlementPayments;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.VehicleMake;
import com.ites.bankfinance.model.VehicleModel;
import com.ites.bankfinance.model.VehicleType;
import com.ites.bankfinance.service.ReportService;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReportDaoImpl implements ReportDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    ReportService reportService;

    @Autowired
    SettlementDaoImpl settlementDaoImpl;

    @Autowired
    RecoveryDaoImpl recoveryDaoImpl;

    @Autowired
    LoanDaoImpl loanDaoImpl;

    @Autowired
    AdminDaoImpl adminDaoImpl;

    @Autowired
    UserDaoImpl userDaoImpl;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public final String refCodeOther = "OTHR";
    public final String refCodeODI = "ODI";
    public final String refCodeINS = "INS";
    public final String refCodeRNT = "RNT";
    public final String refCodeRNTI = "RNTI";
    DecimalFormat df = new DecimalFormat("##,###.00");

    @Override
    public LoanHeaderDetails findByLoanDetails(String agreeNo) {
        LoanHeaderDetails headerDetails = new LoanHeaderDetails();
        try {
            headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                    .add(Restrictions.eq("loanAgreementNo", agreeNo)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return headerDetails;
    }

    @Override
    public List<SettlementCharges> findBySettlementChargees(int loanID) {
        List<SettlementCharges> chargeses = null;
        try {
            //SELECT * FROM settlement_charges WHERE Charge_Type = 'RNT' AND loan_Id = 966
            String sql = "from SettlementCharges as sc where sc.chargeType = 'RNT' and sc.loanId = " + loanID;
            chargeses = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return chargeses;
    }

    @Override
    public Double findLoanBalance(int loanId) {
        double balance = 0.00;

        try {
            double openArrears = getOpenArrears(loanId);

            String sql_c = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeCrdt='ARRS' OR transactionCodeCrdt='OTHR' OR transactionCodeCrdt='INSU' "
                    + "OR transactionCodeCrdt='DWNS' OR transactionCodeCrdt='OVP' OR transactionCodeCrdt='INS' "
                    + "OR transactionCodeCrdt='ODI' OR transactionCodeCrdt = 'RBT') AND loanId=" + loanId;

            String sql_d = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeDbt='ARRS' OR transactionCodeDbt='OTHR' OR transactionCodeDbt='INSU' "
                    + "OR transactionCodeDbt='DWNS' OR transactionCodeDbt='OVP' OR transactionCodeDbt='INS' "
                    + "OR transactionCodeDbt='ODI' OR transactionCodeDbt='RBT') AND loanId=" + loanId;

            double creditSum = 0.00;
            double debitSum = 0.00;

            Object totalCredit = sessionFactory.getCurrentSession().createQuery(sql_c).uniqueResult();
            if (totalCredit != null) {
                creditSum = Double.parseDouble(totalCredit.toString());
            }
            Object totalDebit = sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (totalDebit != null) {
                debitSum = Double.parseDouble(totalDebit.toString());
            }
            double diff = debitSum - creditSum;
            if (diff > 0) {
                balance = diff + openArrears;
            } else {
                balance = 0.00;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return balance;
    }

    public double getOpenArrears(int loanId) {
        double openArres = 0.00;
        double arres = 0.00;
        try {
            String sql1 = "select arreasWithoutOdi,openId from OpenH where loanId='" + loanId + "' ";
            Object[] ob = (Object[]) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            double withoutodi = 0.00;
            if (ob != null) {
                withoutodi = Double.parseDouble(ob[0].toString());
                int openId = Integer.parseInt(ob[1].toString());
                String sql2 = "from OpenD as d where d.openId=" + openId;
                List ob2 = sessionFactory.getCurrentSession().createQuery(sql2).list();
                double amount = 0.00;
                if (ob2.size() > 0) {
                    for (int i = 0; i < ob2.size(); i++) {
                        OpenD od = (OpenD) ob2.get(i);
                        if (od.getAmount() != null) {
                            amount = amount + od.getAmount();
                        }
                    }
                }
                arres = amount;
            }

            openArres = withoutodi + arres;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return openArres;
    }

    @Override
    public String findByInsuCompanyName(Integer insuComId) {
        String insuComName = null;
        try {
            String sql = "select comName from InsuranceCompany where comId = " + insuComId;
            insuComName = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return insuComName;
    }

    @Override
    public Double findTotalPayments(int loanID) {
        Double payments = 0.00;
        Double returnPayments = 0.00;
        Double totalPayments = 0.00;
        try {
            //SELECT SUM(Payment_Amount) FROM settlement_payment WHERE Loan_Id =1072
            String sql = "select sum(paymentAmount) from SettlementPayments where loanId = " + loanID;
            Object p = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (p != null) {
                payments = Double.parseDouble(p.toString());
            }

            //SELECT SUM(spdc.Amount) FROM settlement_payment_detail_cheque AS spdc,settlement_payment AS sp WHERE sp.Payment_Detail_Id = spdc.Cheque_Id AND sp.Loan_Id = 299 AND spdc.Is_Return
            String sql2 = "select sum(spdc.amount) from SettlementPayments as sp,SettlementPaymentDetailCheque as spdc where sp.paymentDetails = spdc.chequeId and  sp.loanId = " + loanID + " and spdc.isReturn = 1";
            Object rp = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
            if (rp != null) {
                returnPayments = Double.parseDouble(rp.toString());
            }
            totalPayments = payments - returnPayments;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalPayments;
    }

    @Override
    public List<Chartofaccount> findChartofaccountList() {
        List<Chartofaccount> chartofaccounts = null;
        try {
            chartofaccounts = sessionFactory.getCurrentSession().createCriteria(Chartofaccount.class).list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return chartofaccounts;
    }

    @Override
    public PortfolioDetail findPortfolioDetail(String agerementNo, String user_Name, String branch_Name) {
        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", followUpOffice = "", product = "", initiateDateFrom = "";
        Date dueDate = null;
        Double loanAmount = 0.00;
        Double grossRetal = 0.00;
        Double capitalOutstanding = 0.00;
        Double rentalInArrears = 0.00;
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setBranch(branch_Name);
            portfolioDetail.setPrintedUserName(user_Name);
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());

            LoanHeaderDetails headerDetails = findByLoanDetails(agerementNo);
            if (headerDetails != null) {
                debtorAccountNo = headerDetails.getDebtorHeaderDetails().getDebtorAccountNo();
                String qry = "from LoanHeaderDetails where debtorHeaderDetails.debtorAccountNo = :debtorAccountNo and loanIsDelete = :loanIsDelete";
                List<LoanHeaderDetails> loanList = session.createQuery(qry).setParameter("debtorAccountNo", debtorAccountNo).
                        setParameter("loanIsDelete", false).list();

                for (int i = 0; i < loanList.size(); i++) {
                    LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                    int loanId = details.getLoanId();
                    Double[] arrearsAmounts = getArrearsAmounts(loanId);
                    if (i == 0) {
                        if (!"".equals(findRecoveryOfficerByLoan(loanId))) {
                            followUpOffice = findRecoveryOfficerByLoan(loanId);
                        } else {
                            followUpOffice = "System User";
                        }
                        product = findLoanTypeName(details.getLoanType());
                        initiateDateFrom = details.getLoanStartDate() + " To " + details.getLoanLapsDate();
                        dueDate = details.getLoanDate();
                        loanAmount = details.getLoanAmount().doubleValue();
                        grossRetal = details.getLoanInstallment();
                        capitalOutstanding = getCapitalOutStanding(loanId);
                        rentalInArrears = arrearsAmounts[2];
                    }
                    //Loan Vehicle details
                    LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                            .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                    //Loan Insurance Details
                    String companyNames = "";
                    List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                            .add(Restrictions.eq("insuLoanId", loanId)).list();
                    for (InsuraceProcessMain main : processMain) {
                        if (processMain.size() == 1) {
                            companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                        } else {
                            companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                        }
                    }
                    portfolioDetail.setFollowUpOffice(followUpOffice);
                    portfolioDetail.setProduct(product);
                    portfolioDetail.setInitiateDateFrom(initiateDateFrom);
                    portfolioDetail.setDueDate(dateFormat.format(dueDate));
                    portfolioDetail.setLoanAmount(df.format(loanAmount));
                    portfolioDetail.setGrossRetal(df.format(grossRetal));
                    portfolioDetail.setCapitalOutstanding(df.format(capitalOutstanding));
                    portfolioDetail.setRentalInArrears(df.format(rentalInArrears));

                    ////create List PortfolioDetails//////
                    PortfolioDetails pd = new PortfolioDetails();
                    pd.setSerial_No(i + 1);
                    pd.setDebtor_Code(debtorAccountNo);
                    pd.setDebtor_Name(details.getDebtorHeaderDetails().getDebtorName());
                    pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                    pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                    pd.setFacility_Type(getLoanFacilityType(loanId));
                    if (details.getLoanStartDate() != null) {
                        pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                    }
                    pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                    pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                    if (details.getLoanStartDate() != null) {
                        pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                    }
                    if (details.getLoanStartDate() != null) {
                        pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                    }
                    pd.setAdvance_Payment_Amount(df.format(0.00));
                    pd.setPre_Payment_Amount(df.format(0.00));
                    pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                    pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                    pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                    pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                    pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                    pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                    pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));
                    pd.setInterest_In_Suspens_Amount(df.format(0.00));
                    pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));
                    pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId)));
                    pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));
                    lastPayment = getLastPaymentDetails(loanId);
                    if (lastPayment[0] != null && lastPayment[1] != null) {
                        pd.setLast_Payment_Date(lastPayment[0]);
                        pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                    }
                    pd.setArrears_Days(getArrearsDays(loanId));
                    pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                    pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                    pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                    pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                    pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId)));
                    pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                    Double[] loanFutureDetail = getLoanFutureDetails(loanId);
                    pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                    pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                    pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                    pd.setLoan_Status(getLoanStatus(details));
                    pd.setContact_Person("");
                    pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                    if (details.getLoanMarketingOfficer() != null) {
                        pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                    }
                    pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                    if (vehicleDetails != null) {
                        if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                            pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                        } else {
                            pd.setVehicle_No("");
                        }
                        if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                            pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                        } else {
                            pd.setEngine_Number("");
                        }
                        if (vehicleDetails.getVehiclChassisNo() != null) {
                            pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                        } else {
                            pd.setChasis_No("");
                        }
                        if (vehicleDetails.getVehicleMake() != null) {
                            pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                        } else {
                            pd.setVehicle_Class("");
                        }
                        if (vehicleDetails.getVehicleType() != null) {
                            pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                        } else {
                            pd.setVehicle_Type("");
                        }
                    }
                    pd.setInsurance_Company(companyNames);
                    pd.setInsurance_Assigned("");
                    pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                    pd.setDue_Day(details.getDueDay());
                    portfolioDetailses.add(pd);
                    pd = null;
                }
                portfolioDetail.setDetails(portfolioDetailses);
            }
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

    /* private String findRecoveryOfficerByLoan(int loanId) {
        String officerName = "";
        try {
            LoanRecoveryReport recoveryReport = (LoanRecoveryReport) sessionFactory.getCurrentSession().createCriteria(LoanRecoveryReport.class)
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            int officerId = recoveryReport.getEmpNo();
            UmUser user = (UmUser) sessionFactory.getCurrentSession().createCriteria(UmUser.class)
                    .add(Restrictions.eq("empId", officerId)).uniqueResult();
            officerName = user.getUserName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officerName;
    }*/
    private String findRecoveryOfficerByLoan(int loanId) {
        String officerName = "";
        try {
            if (loanId != 0) {
                LoanRecoveryReport recoveryReport = (LoanRecoveryReport) sessionFactory.getCurrentSession().createCriteria(LoanRecoveryReport.class)
                        .add(Restrictions.eq("loanId", loanId)).uniqueResult();

                if (recoveryReport != null) {
                    int officerId = recoveryReport.getEmpNo();
                    if (officerId != 0) {
                        UmUser user = (UmUser) sessionFactory.getCurrentSession().createCriteria(UmUser.class)
                                .add(Restrictions.eq("empId", officerId)).uniqueResult();
                        officerName = user.getUserName();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officerName;
    }

    private String findLoanTypeName(int loanType) {
        String loanTypeName = "";
        try {
            MSubLoanType subLoanType = (MSubLoanType) sessionFactory.getCurrentSession().createCriteria(MSubLoanType.class)
                    .add(Restrictions.eq("subLoanId", loanType)).uniqueResult();
            loanTypeName = subLoanType.getSubLoanName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanTypeName;
    }

    private Double getCapitalOutStanding(int loanId) {
        Double capital = 0.00;
        try {
            List<LoanInstallment> loanInstallments = sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("isPay", 0)).list();
            for (LoanInstallment li : loanInstallments) {
                capital = capital + li.getInsPrinciple();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return capital;
    }

    private String getLoanFacilityType(int loanId) {
        String type = "";
        try {
            List<LoanInstalmentSchedule> schedules = sessionFactory.getCurrentSession().createCriteria(LoanInstalmentSchedule.class)
                    .add(Restrictions.eq("loanId", loanId)).list();
            if (schedules.isEmpty()) {
                type = "Normal";
            } else {
                type = "Structured";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return type;
    }

    private Date getLoanDueDate(int loanId) {
        Date dueDate = null;
        try {            
                LoanInstallment installment = (LoanInstallment) sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                dueDate = installment.getDueDate();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dueDate;
    }

    @Override
    public double getNoOfRentalInArrears(int loanId) {
        double count = 0.00;
        double balance = findLoanBalance(loanId);
        double rental = 0.00;
        try {
            String sql = "from LoanInstallment where loanId = " + loanId;
            LoanInstallment li = (LoanInstallment) sessionFactory.getCurrentSession()
                    .createQuery(sql).setMaxResults(1).uniqueResult();
            if (li != null) {
                rental = li.getInsRoundAmount();
            }
            count = balance / rental;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public Double[] getArrearsAmounts(int loanId) {
        Double[] arrears = new Double[3];

        Double interestAmountInArrears = 0.00;
        Double capitalAmountInArrears = 0.00;
        Double rentalAmountInArrears = 0.00;

        int userId = userDaoImpl.findByUserName();
        int userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
        userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
        Date systemDate = settlementDaoImpl.getSystemDate(userLogBranchId);
        try {
            List<LoanInstallment> installments = sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanId))
                    //                    .add(Restrictions.eq("isPay", 0))
                    .add(Restrictions.or(Restrictions.eq("isPay", 0), Restrictions.eq("isPay", -1)))
                    //                    xxxxxxxxyyyyy
                    .add(Restrictions.lt("dueDate", systemDate)).list();
            for (LoanInstallment li : installments) {
                interestAmountInArrears = interestAmountInArrears + li.getInsInterest();
                capitalAmountInArrears = capitalAmountInArrears + li.getInsPrinciple();
                rentalAmountInArrears = rentalAmountInArrears + li.getInsAmount();
            }
            arrears[0] = interestAmountInArrears;
            arrears[1] = capitalAmountInArrears;
            arrears[2] = rentalAmountInArrears;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrears;
    }

    public Double getOdiArrearsAmount(int loanId) {
        Double odiCreditSum = 0.00;
        Double odiDebitSum = 0.00;

        Double arrears = 0.00;
        try {
            String sqld = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeODI + "' ";
            String sqlc = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeODI + "' ";

            Object obd = sessionFactory.getCurrentSession().createQuery(sqld).uniqueResult();
            Object obc = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (obd != null && obc != null) {
                odiDebitSum = Double.parseDouble(obd.toString());
                odiCreditSum = Double.parseDouble(obc.toString());
                arrears = odiDebitSum - odiCreditSum;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrears;
    }

    public Double getOdiArrearsAmount(int loanId, String initiateFromDate, String initiateToDate) {
        Double odiCreditSum = 0.00;
        Double odiDebitSum = 0.00;

        Double arrears = 0.00;
        try {
            String sqld = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeODI + "' and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "'";
            String sqlc = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeODI + "' and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "' ";

            Object obd = sessionFactory.getCurrentSession().createQuery(sqld).uniqueResult();
            Object obc = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (obd != null && obc != null) {
                odiDebitSum = Double.parseDouble(obd.toString());
                odiCreditSum = Double.parseDouble(obc.toString());
                arrears = odiDebitSum - odiCreditSum;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrears;
    }

    public Double getOtherChargersArrearsAmount(int loanId) {
        Double otherCreditSum = 0.00;
        Double otherDebitSum = 0.00;

        Double arrears = 0.00;
        try {
//            String sqld = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeOther + "'";
//            String sqlc = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeOther + "'";

            String sqld = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeOther + "' and systemDate BETWEEN '2017-01-01' and '2017-02-28'";
            String sqlc = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeOther + "' and systemDate BETWEEN '2017-01-01' and '2017-02-28'";

            Object obd = sessionFactory.getCurrentSession().createQuery(sqld).uniqueResult();
            Object obc = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (obd != null && obc != null) {
                otherDebitSum = Double.parseDouble(obd.toString());
                otherCreditSum = Double.parseDouble(obc.toString());
                arrears = otherDebitSum - otherCreditSum;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrears;
    }

    public Double getOtherChargersArrearsAmount(int loanId, String initiateFromDate, String initiateToDate) {
        Double otherCreditSum = 0.00;
        Double otherDebitSum = 0.00;

        Double arrears = 0.00;
        try {
            String sqld = "select sum(dbtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeDbt like '" + refCodeOther + "' and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "'";
            String sqlc = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeOther + "' and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "'";

            Object obd = sessionFactory.getCurrentSession().createQuery(sqld).uniqueResult();
            Object obc = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (obd != null && obc != null) {
                otherDebitSum = Double.parseDouble(obd.toString());
                otherCreditSum = Double.parseDouble(obc.toString());
                arrears = otherDebitSum - otherCreditSum;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrears;
    }

    private String[] getLastPaymentDetails(int loanId) {
        String[] lastPayment = new String[2];
        try {
            SettlementPayments payments = (SettlementPayments) sessionFactory.getCurrentSession().createCriteria(SettlementPayments.class)
                    .add(Restrictions.eq("loanId", loanId)).addOrder(Order.desc("paymentDate")).setMaxResults(1).uniqueResult();
            if (payments != null) {
                lastPayment[0] = dateFormat.format(payments.getPaymentDate());
                lastPayment[1] = String.valueOf(payments.getPaymentAmount());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastPayment;
    }

    private long getArrearsDays(int loanId) {
        long days = 0;
        int userId = userDaoImpl.findByUserName();
        int userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
        userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
        Date systemDate = settlementDaoImpl.getSystemDate(userLogBranchId);
        try {
            LoanInstallment installments = (LoanInstallment) sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("isPay", 0))
                    //                    .add(Restrictions.or(Restrictions.eq("isPay", 0), Restrictions.eq("isPay", -1)))
                    .add(Restrictions.lt("dueDate", systemDate)).addOrder(Order.asc("dueDate")).setMaxResults(1).uniqueResult();
            if (installments != null) {
                Date stDate = installments.getDueDate();
                days = systemDate.getTime() - stDate.getTime();
                days = (long) (days / (24 * 60 * 60 * 1000));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return days;
    }

    private Double getTotalColectedAmount(int loanId) {
        Double total = 0.00;
        try {
            String sql_c = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE "
                    + "(transactionCodeCrdt='ARRS' OR transactionCodeCrdt='OTHR' OR transactionCodeCrdt='INSU' "
                    + "OR transactionCodeCrdt='DWNS' OR transactionCodeCrdt='INS' "
                    + "OR transactionCodeCrdt='ODI' OR transactionCodeCrdt = 'RBT') AND loanId=" + loanId;

//            OR transactionCodeCrdt='OVP' 
            Object object = sessionFactory.getCurrentSession().createQuery(sql_c).uniqueResult();

            if (object != null) {
                total = Double.parseDouble(object.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return total;

    }

    private Double getTotalOverPayAmount(int loanId) {
        Double overPay = 0.00;
        Double crdtOverPay = 0.00;
        Double dbtOverPay = 0.00;
        try {
            String ovpCrdtAmt = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE loanId = '" + loanId + "' and transactionCodeCrdt = 'OVP'";
            String ovpDbtAmt = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE loanId = '" + loanId + "' and transactionCodeDbt = 'OVP'";

            Object ovpCrdtAmtObject = sessionFactory.getCurrentSession().createQuery(ovpCrdtAmt).uniqueResult();
            Object ovpDbtAmtObject = sessionFactory.getCurrentSession().createQuery(ovpDbtAmt).uniqueResult();
            if (ovpCrdtAmtObject != null) {
                crdtOverPay = Double.parseDouble(ovpCrdtAmtObject.toString());
            }
            if (ovpDbtAmtObject != null) {
                dbtOverPay = Double.parseDouble(ovpDbtAmtObject.toString());
            }
            overPay = crdtOverPay - dbtOverPay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return overPay;
    }

    public Double[] getLoanFutureDetails(int loanId) {
        Double[] future = new Double[2];
        Double fureInterestAmount = 0.00;
        Double fureCapitalAmount = 0.00;

//        xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        Double totalInterestAmount = 0.00;
        Double totalCapitalAmount = 0.00;
        try {

            List<LoanInstallment> installments = sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanId)).list();

            for (LoanInstallment li : installments) {
                totalInterestAmount = totalInterestAmount + li.getInsInterest();
                totalCapitalAmount = totalCapitalAmount + li.getInsPrinciple();
            }
            List<SettlementCharges> paidRentals = sessionFactory.getCurrentSession().createCriteria(SettlementCharges.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("chargeType", refCodeRNT)).list();
            List<SettlementCharges> paidInterest = sessionFactory.getCurrentSession().createCriteria(SettlementCharges.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("chargeType", refCodeRNTI)).list();

            Double totalPaidRental = 0.00;
            for (SettlementCharges l2 : paidRentals) {
                totalPaidRental = totalPaidRental + l2.getCharge();
            }
            Double totalPaidIntere = 0.00;
            for (SettlementCharges l3 : paidInterest) {
                totalPaidIntere = totalPaidIntere + l3.getCharge();
            }
            fureInterestAmount = totalInterestAmount - totalPaidIntere;
            fureCapitalAmount = totalCapitalAmount - totalPaidRental;

            future[0] = fureInterestAmount;
            future[1] = fureCapitalAmount;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return future;
    }

    private String getLoanStatus(LoanHeaderDetails details) {
        String status = "";
        int loanStatus = details.getLoanStatus();
        int loanSpac = details.getLoanSpec();

        switch (loanSpac) {
            case 0:
                if (loanStatus == 0) {
                    status = "Processing";
                } else if (loanStatus == 1) {
                    status = "On Going";
                } else {
                    status = "Rejected";
                }
                break;
            case 1:
                status = "Laps";
                break;
            case 2:
                status = "Legal";
                break;
            case 3:
                status = "Seize";
                break;
            case 4:
                status = "Full Paid";
                break;
            case 5:
                status = "Rebate";
                break;
            default:
                break;
        }

        return status;

    }

    private Double getOverDueCollectedAmount(int loanId) {
        Double overDue = 0.00;
        try {
            String sqlc = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt like '" + refCodeODI + "' ";

            Object object = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (object != null) {
                overDue = Double.parseDouble(object.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return overDue;
    }

    private Double getOtherChargeCollectedAmount(int loanId) {
        Double otherAmount = 0.00;
        try {
            String sqlc = "select sum(crdtAmount) from SettlementMain where loanId='" + loanId + "' and transactionCodeCrdt = 'OTHR'";
            Object object = sessionFactory.getCurrentSession().createQuery(sqlc).uniqueResult();
            if (object != null) {
                otherAmount = Double.parseDouble(object.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return otherAmount;
    }

    private String getInsuranceCompanyName(Integer insuCompanyId) {
        String name = "";
        try {
            InsuranceCompany company = (InsuranceCompany) sessionFactory.getCurrentSession().createCriteria(InsuranceCompany.class)
                    .add(Restrictions.eq("comId", insuCompanyId))
                    .add(Restrictions.eq("isActive", true)).uniqueResult();
            name = company.getComName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    private String getVehicleClass(String vehicleMake) {
        String name = "";
        int vehicleMakeId = Integer.parseInt(vehicleMake);
        try {
            VehicleMake make = (VehicleMake) sessionFactory.getCurrentSession().createCriteria(VehicleMake.class)
                    .add(Restrictions.eq("vehicleMake", vehicleMakeId))
                    .add(Restrictions.eq("isActive", true)).uniqueResult();
            name = make.getVehicleMakeName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    private String getVehicleType(String vehicleType) {
        String name = "";
        int vehicleTypeId = Integer.parseInt(vehicleType);
        try {
            VehicleType make = (VehicleType) sessionFactory.getCurrentSession().createCriteria(VehicleType.class)
                    .add(Restrictions.eq("vehicleType", vehicleTypeId))
                    .add(Restrictions.eq("isActive", true)).uniqueResult();
            name = make.getVehicleTypeName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    @Override
    public PortfolioDetail findPortfolioDetail(int branchId) {
        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", product = "";
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setPrintedUserName("");
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());
            portfolioDetail.setFollowUpOffice("System User");

            String qry = "";
            List<LoanHeaderDetails> loanList = null;
            if (branchId == 0) {
                qry = "from LoanHeaderDetails where loanIsIssue = :loanIsIssue";
                loanList = session.createQuery(qry).setParameter("loanIsIssue", 2).list();
                portfolioDetail.setBranch("ALL");
            } else {
                qry = "from LoanHeaderDetails where loanIsIssue = :loanIsIssue and branchId = :branchId";
                loanList = session.createQuery(qry).setParameter("loanIsIssue", 2).setParameter("branchId", branchId).list();
                portfolioDetail.setBranch(adminDaoImpl.findBranchName(branchId));
            }
            for (int i = 0; i < loanList.size(); i++) {
                LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                int loanId = details.getLoanId();
                Double[] arrearsAmounts = getArrearsAmounts(loanId);
                product = findLoanTypeName(details.getLoanType());
                portfolioDetail.setProduct(product);

                //Loan Vehicle details
                LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                //Loan Insurance Details
                String companyNames = "";
                List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                        .add(Restrictions.eq("insuLoanId", loanId)).list();
                for (InsuraceProcessMain main : processMain) {
                    if (processMain.size() == 1) {
                        companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                    } else {
                        companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                    }
                }

                ////create List PortfolioDetails//////
                PortfolioDetails pd = new PortfolioDetails();
                pd.setSerial_No(i + 1);
                pd.setDebtor_Code(debtorAccountNo);
                pd.setDebtor_Name(details.getDebtorHeaderDetails().getNameWithInitial());
                pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                pd.setFacility_Type(getLoanFacilityType(loanId));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                }
                pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                }
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                }
                pd.setAdvance_Payment_Amount(df.format(0.00));
                pd.setPre_Payment_Amount(df.format(0.00));
                pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));
                pd.setInterest_In_Suspens_Amount(df.format(0.00));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));
                pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId)));
                pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));
                lastPayment = getLastPaymentDetails(loanId);
                if (lastPayment[0] != null && lastPayment[1] != null) {
                    pd.setLast_Payment_Date(lastPayment[0]);
                    pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                }
                pd.setArrears_Days(getArrearsDays(loanId));
                pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId)));
                pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                Double[] loanFutureDetail = getLoanFutureDetails(loanId);
                pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                pd.setLoan_Status(getLoanStatus(details));
                pd.setContact_Person("");
                pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                if (details.getLoanMarketingOfficer() != null) {
                    pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                }
                pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                if (vehicleDetails != null) {
                    if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                        pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                    } else {
                        pd.setVehicle_No("");
                    }
                    if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                        pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                    } else {
                        pd.setEngine_Number("");
                    }
                    if (vehicleDetails.getVehiclChassisNo() != null) {
                        pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                    } else {
                        pd.setChasis_No("");
                    }
                    if (vehicleDetails.getVehicleMake() != null) {
                        pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                    } else {
                        pd.setVehicle_Class("");
                    }
                    if (vehicleDetails.getVehicleType() != null) {
                        pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                    } else {
                        pd.setVehicle_Type("");
                    }
                }
                pd.setInsurance_Company(companyNames);
                pd.setInsurance_Assigned("");
                pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                pd.setDue_Day(details.getDueDay());
                portfolioDetailses.add(pd);
                pd = null;
            }
            portfolioDetail.setDetails(portfolioDetailses);
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

    @Override
    public PortfolioDetail findPortfolioDetailByOfficerId(int officerId) {
        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", product = "";
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setBranch("ALL");
            portfolioDetail.setPrintedUserName("");
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());
            portfolioDetail.setFollowUpOffice("System User");

            String qry = "from LoanHeaderDetails where loanIsIssue = :loanIsIssue and loanMarketingOfficer = :loanMarketingOfficer";
            List<LoanHeaderDetails> loanList = session.createQuery(qry).setParameter("loanIsIssue", 2).
                    setParameter("loanMarketingOfficer", officerId).list();
            for (int i = 0; i < loanList.size(); i++) {
                LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                int loanId = details.getLoanId();
                Double[] arrearsAmounts = getArrearsAmounts(loanId);
                product = findLoanTypeName(details.getLoanType());
                portfolioDetail.setProduct(product);

                //Loan Vehicle details
                LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                //Loan Insurance Details
                String companyNames = "";
                List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                        .add(Restrictions.eq("insuLoanId", loanId)).list();
                for (InsuraceProcessMain main : processMain) {
                    if (processMain.size() == 1) {
                        companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                    } else {
                        companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                    }
                }

                ////create List PortfolioDetails//////
                PortfolioDetails pd = new PortfolioDetails();
                pd.setSerial_No(i + 1);
                pd.setDebtor_Code(debtorAccountNo);
                pd.setDebtor_Name(details.getDebtorHeaderDetails().getNameWithInitial());
                pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                pd.setFacility_Type(getLoanFacilityType(loanId));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                }
                pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                }
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                }
                pd.setAdvance_Payment_Amount(df.format(0.00));
                pd.setPre_Payment_Amount(df.format(0.00));
                pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));
                pd.setInterest_In_Suspens_Amount(df.format(0.00));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));
                pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId)));
                pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));
                lastPayment = getLastPaymentDetails(loanId);
                if (lastPayment[0] != null && lastPayment[1] != null) {
                    pd.setLast_Payment_Date(lastPayment[0]);
                    pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                }
                pd.setArrears_Days(getArrearsDays(loanId));
                pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId)));
                pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                Double[] loanFutureDetail = getLoanFutureDetails(loanId);
                pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                pd.setLoan_Status(getLoanStatus(details));
                pd.setContact_Person("");
                pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                if (details.getLoanMarketingOfficer() != null) {
                    pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                }
                pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                if (vehicleDetails != null) {
                    if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                        pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                    } else {
                        pd.setVehicle_No("");
                    }
                    if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                        pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                    } else {
                        pd.setEngine_Number("");
                    }
                    if (vehicleDetails.getVehiclChassisNo() != null) {
                        pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                    } else {
                        pd.setChasis_No("");
                    }
                    if (vehicleDetails.getVehicleMake() != null) {
                        pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                    } else {
                        pd.setVehicle_Class("");
                    }
                    if (vehicleDetails.getVehicleType() != null) {
                        pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                    } else {
                        pd.setVehicle_Type("");
                    }
                }
                pd.setInsurance_Company(companyNames);
                pd.setInsurance_Assigned("");
                pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                pd.setDue_Day(details.getDueDay());
                portfolioDetailses.add(pd);
                pd = null;
            }
            portfolioDetail.setDetails(portfolioDetailses);
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

    @Override
    public PortfolioDetail findPortfolioDetailByLoanType(int loanType) {
        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", product = "";
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setBranch("ALL");
            portfolioDetail.setPrintedUserName("");
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());
            portfolioDetail.setFollowUpOffice("System User");

            String qry = "from LoanHeaderDetails where loanIsIssue = :loanIsIssue and loanType = :loanType";
            List<LoanHeaderDetails> loanList = session.createQuery(qry).setParameter("loanIsIssue", 2).
                    setParameter("loanType", loanType).list();
            for (int i = 0; i < loanList.size(); i++) {
                LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                int loanId = details.getLoanId();
                Double[] arrearsAmounts = getArrearsAmounts(loanId);
                product = findLoanTypeName(details.getLoanType());
                portfolioDetail.setProduct(product);

                //Loan Vehicle details
                LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                //Loan Insurance Details
                String companyNames = "";
                List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                        .add(Restrictions.eq("insuLoanId", loanId)).list();
                for (InsuraceProcessMain main : processMain) {
                    if (processMain.size() == 1) {
                        companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                    } else {
                        companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                    }
                }

                ////create List PortfolioDetails//////
                PortfolioDetails pd = new PortfolioDetails();
                pd.setSerial_No(i + 1);
                pd.setDebtor_Code(debtorAccountNo);
                pd.setDebtor_Name(details.getDebtorHeaderDetails().getNameWithInitial());
                pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                pd.setFacility_Type(getLoanFacilityType(loanId));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                }
                pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                }
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                }
                pd.setAdvance_Payment_Amount(df.format(0.00));
                pd.setPre_Payment_Amount(df.format(0.00));
                pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));
                pd.setInterest_In_Suspens_Amount(df.format(0.00));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));
                pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId)));
                pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));

                //                ===== ODI Details =====
                String initiateFromDate = null, initiateToDate = null;

                pd.setDebit_Odi_Chargers(df.format(getDebitOdiChargers(loanId, initiateFromDate, initiateToDate)));
                pd.setCredit_Odi_Chargers(df.format(getCreditOdiChargers(loanId, initiateFromDate, initiateToDate)));
                pd.setWave_Off_Odi_Chargers(df.format(getWaveOffOdiChargers(loanId, initiateFromDate, initiateToDate)));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));

                lastPayment = getLastPaymentDetails(loanId);
                if (lastPayment[0] != null && lastPayment[1] != null) {
                    pd.setLast_Payment_Date(lastPayment[0]);
                    pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                }
                pd.setArrears_Days(getArrearsDays(loanId));
                pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId)));
                pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                Double[] loanFutureDetail = getLoanFutureDetails(loanId);
                pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                pd.setLoan_Status(getLoanStatus(details));
                pd.setContact_Person("");
                pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                if (details.getLoanMarketingOfficer() != null) {
                    pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                }
                pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                if (vehicleDetails != null) {
                    if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                        pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                    } else {
                        pd.setVehicle_No("");
                    }
                    if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                        pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                    } else {
                        pd.setEngine_Number("");
                    }
                    if (vehicleDetails.getVehiclChassisNo() != null) {
                        pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                    } else {
                        pd.setChasis_No("");
                    }
                    if (vehicleDetails.getVehicleMake() != null) {
                        pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                    } else {
                        pd.setVehicle_Class("");
                    }
                    if (vehicleDetails.getVehicleType() != null) {
                        pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                    } else {
                        pd.setVehicle_Type("");
                    }
                }
                pd.setInsurance_Company(companyNames);
                pd.setInsurance_Assigned("");
                pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                pd.setDue_Day(details.getDueDay());
                portfolioDetailses.add(pd);
                pd = null;
            }
            portfolioDetail.setDetails(portfolioDetailses);
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

    @Override
    public PortfolioDetail findPortfolioDetailByDueDay(int dueDay) {
        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", product = "";
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setBranch("ALL");
            portfolioDetail.setPrintedUserName("");
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());
            portfolioDetail.setFollowUpOffice("System User");

            String qry = "from LoanHeaderDetails where loanIsIssue = :loanIsIssue and dueDay = :dueDay";
            List<LoanHeaderDetails> loanList = session.createQuery(qry).setParameter("loanIsIssue", 2).
                    setParameter("dueDay", dueDay).list();
            for (int i = 0; i < loanList.size(); i++) {
                LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                int loanId = details.getLoanId();
                Double[] arrearsAmounts = getArrearsAmounts(loanId);
                product = findLoanTypeName(details.getLoanType());
                portfolioDetail.setProduct(product);

                //Loan Vehicle details
                LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                //Loan Insurance Details
                String companyNames = "";
                List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                        .add(Restrictions.eq("insuLoanId", loanId)).list();
                for (InsuraceProcessMain main : processMain) {
                    if (processMain.size() == 1) {
                        companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                    } else {
                        companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                    }
                }

                ////create List PortfolioDetails//////
                PortfolioDetails pd = new PortfolioDetails();
                pd.setSerial_No(i + 1);
                pd.setDebtor_Code(debtorAccountNo);
                pd.setDebtor_Name(details.getDebtorHeaderDetails().getNameWithInitial());
                pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                pd.setFacility_Type(getLoanFacilityType(loanId));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                }
                pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                }
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                }
                pd.setAdvance_Payment_Amount(df.format(0.00));
                pd.setPre_Payment_Amount(df.format(0.00));
                pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));
                pd.setInterest_In_Suspens_Amount(df.format(0.00));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));
                pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId)));
                pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));
                lastPayment = getLastPaymentDetails(loanId);
                if (lastPayment[0] != null && lastPayment[1] != null) {
                    pd.setLast_Payment_Date(lastPayment[0]);
                    pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                }
                pd.setArrears_Days(getArrearsDays(loanId));
                pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId)));
                pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                Double[] loanFutureDetail = getLoanFutureDetails(loanId);
                pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                pd.setLoan_Status(getLoanStatus(details));
                pd.setContact_Person("");
                pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                if (details.getLoanMarketingOfficer() != null) {
                    pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                }
                pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                if (vehicleDetails != null) {
                    if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                        pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                    } else {
                        pd.setVehicle_No("");
                    }
                    if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                        pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                    } else {
                        pd.setEngine_Number("");
                    }
                    if (vehicleDetails.getVehiclChassisNo() != null) {
                        pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                    } else {
                        pd.setChasis_No("");
                    }
                    if (vehicleDetails.getVehicleMake() != null) {
                        pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                    } else {
                        pd.setVehicle_Class("");
                    }
                    if (vehicleDetails.getVehicleType() != null) {
                        pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                    } else {
                        pd.setVehicle_Type("");
                    }
                }
                pd.setInsurance_Company(companyNames);
                pd.setInsurance_Assigned("");
                pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                pd.setDue_Day(details.getDueDay());
                portfolioDetailses.add(pd);
                pd = null;
            }
            portfolioDetail.setDetails(portfolioDetailses);
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

//    Sahan +++++++++++++++++
    @Override
    public PortfolioDetail findPortfolioDetailByInitiateDate(String initiateFromDate, String initiateToDate) {

        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", product = "";
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setBranch("ALL");
            portfolioDetail.setPrintedUserName("");
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());
            portfolioDetail.setFollowUpOffice("System User");
            String qry = "FROM LoanHeaderDetails WHERE loanStartDate BETWEEN '" + initiateFromDate + "' AND '" + initiateToDate + "'";
//            String qry = "FROM LoanHeaderDetails";
            List<LoanHeaderDetails> loanList = session.createQuery(qry).list();

            for (int i = 0; i < loanList.size(); i++) {
                LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                int loanId = details.getLoanId();
                Double[] arrearsAmounts = getArrearsAmounts(loanId);
                product = findLoanTypeName(details.getLoanType());
                portfolioDetail.setProduct(product);

                //Loan Vehicle details
                LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                //Loan Insurance Details
                String companyNames = "";
                List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                        .add(Restrictions.eq("insuLoanId", loanId)).list();
                for (InsuraceProcessMain main : processMain) {
                    if (processMain.size() == 1) {
                        companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                    } else {
                        companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                    }
                }

                ////create List PortfolioDetails//////
                PortfolioDetails pd = new PortfolioDetails();
                pd.setSerial_No(i + 1);
                pd.setDebtor_Code(debtorAccountNo);
                pd.setDebtor_Name(details.getDebtorHeaderDetails().getNameWithInitial());
                pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                pd.setFacility_Type(getLoanFacilityType(loanId));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                }
                pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                }
                if (details.getLoanStartDate() != null) {
                    System.out.println("date====================== " + loanId);
                    pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                }
                pd.setAdvance_Payment_Amount(df.format(0.00));
                pd.setPre_Payment_Amount(df.format(0.00));
                pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));

//                ===== ODI Details =====
                pd.setDebit_Odi_Chargers(df.format(getDebitOdiChargers(loanId, initiateFromDate, initiateToDate)));
                pd.setCredit_Odi_Chargers(df.format(getCreditOdiChargers(loanId, initiateFromDate, initiateToDate)));
                pd.setWave_Off_Odi_Chargers(df.format(getWaveOffOdiChargers(loanId, initiateFromDate, initiateToDate)));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId, initiateFromDate, initiateToDate)));

                pd.setInterest_In_Suspens_Amount(df.format(0.00));
                pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId, initiateFromDate, initiateToDate)));
                pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));
                lastPayment = getLastPaymentDetails(loanId);
                if (lastPayment[0] != null && lastPayment[1] != null) {
                    pd.setLast_Payment_Date(lastPayment[0]);
                    pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                }
                pd.setArrears_Days(getArrearsDays(loanId));
                pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId) + getTotalOverPayAmount(loanId)));
                pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                Double[] loanFutureDetail = getLoanFutureDetails(loanId);

                pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                pd.setLoan_Status(getLoanStatus(details));
                pd.setContact_Person("");
                pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                if (details.getLoanMarketingOfficer() != null) {
                    pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                }
                pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                if (vehicleDetails != null) {
                    if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                        pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                    } else {
                        pd.setVehicle_No("");
                    }
                    if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                        pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                    } else {
                        pd.setEngine_Number("");
                    }
                    if (vehicleDetails.getVehiclChassisNo() != null) {
                        pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                    } else {
                        pd.setChasis_No("");
                    }
                    if (vehicleDetails.getVehicleMake() != null) {
                        pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                    } else {
                        pd.setVehicle_Class("");
                    }
                    if (vehicleDetails.getVehicleType() != null) {
                        pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                    } else {
                        pd.setVehicle_Type("");
                    }
                }
                pd.setInsurance_Company(companyNames);
                pd.setInsurance_Assigned("");
                pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                pd.setDue_Day(details.getDueDay());
                portfolioDetailses.add(pd);
                pd = null;
            }
            portfolioDetail.setDetails(portfolioDetailses);
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

    @Override
    public PortfolioDetail findPortfolioDetailByLoanAmount(String loanAmount, String lagValue) {
        BigDecimal loanAmounts = new BigDecimal(loanAmount);
        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", product = "";
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setBranch("ALL");
            portfolioDetail.setPrintedUserName("");
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());
            portfolioDetail.setFollowUpOffice("System User");

            List<LoanHeaderDetails> loanList = null;
            if (lagValue.equals("1")) {
                String qry = "from LoanHeaderDetails where loanAmount > :loanAmount";
                loanList = session.createQuery(qry).setParameter("loanAmount", loanAmounts).list();
            } else if (lagValue.equals("2")) {
                String qry = "from LoanHeaderDetails where loanAmount < :loanAmount";
                loanList = session.createQuery(qry).setParameter("loanAmount", loanAmounts).list();
            }

            for (int i = 0; i < loanList.size(); i++) {
                LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                int loanId = details.getLoanId();
                Double[] arrearsAmounts = getArrearsAmounts(loanId);
                product = findLoanTypeName(details.getLoanType());
                portfolioDetail.setProduct(product);

                //Loan Vehicle details
                LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                //Loan Insurance Details
                String companyNames = "";
                List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                        .add(Restrictions.eq("insuLoanId", loanId)).list();
                for (InsuraceProcessMain main : processMain) {
                    if (processMain.size() == 1) {
                        companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                    } else {
                        companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                    }
                }

                ////create List PortfolioDetails//////
                PortfolioDetails pd = new PortfolioDetails();
                pd.setSerial_No(i + 1);
                pd.setDebtor_Code(debtorAccountNo);
                pd.setDebtor_Name(details.getDebtorHeaderDetails().getNameWithInitial());
                pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                pd.setFacility_Type(getLoanFacilityType(loanId));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                }
                pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                }
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                }
                pd.setAdvance_Payment_Amount(df.format(0.00));
                pd.setPre_Payment_Amount(df.format(0.00));
                pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));
                pd.setInterest_In_Suspens_Amount(df.format(0.00));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));
                pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId)));
                pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));
                lastPayment = getLastPaymentDetails(loanId);
                if (lastPayment[0] != null && lastPayment[1] != null) {
                    pd.setLast_Payment_Date(lastPayment[0]);
                    pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                }
                pd.setArrears_Days(getArrearsDays(loanId));
                pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId)));
                pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                Double[] loanFutureDetail = getLoanFutureDetails(loanId);
                pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                pd.setLoan_Status(getLoanStatus(details));
                pd.setContact_Person("");
                pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                if (details.getLoanMarketingOfficer() != null) {
                    pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                }
                pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                if (vehicleDetails != null) {
                    if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                        pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                    } else {
                        pd.setVehicle_No("");
                    }
                    if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                        pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                    } else {
                        pd.setEngine_Number("");
                    }
                    if (vehicleDetails.getVehiclChassisNo() != null) {
                        pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                    } else {
                        pd.setChasis_No("");
                    }
                    if (vehicleDetails.getVehicleMake() != null) {
                        pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                    } else {
                        pd.setVehicle_Class("");
                    }
                    if (vehicleDetails.getVehicleType() != null) {
                        pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                    } else {
                        pd.setVehicle_Type("");
                    }
                }
                pd.setInsurance_Company(companyNames);
                pd.setInsurance_Assigned("");
                pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                pd.setDue_Day(details.getDueDay());
                portfolioDetailses.add(pd);
                pd = null;
            }
            portfolioDetail.setDetails(portfolioDetailses);
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

    @Override
    public PortfolioDetail findPortfolioDetailByGrossRental(String grossRental, String grValue) {
        double grossRentals = Double.parseDouble(grossRental);
        Session session = null;
        PortfolioDetail portfolioDetail = new PortfolioDetail();
        List<PortfolioDetails> portfolioDetailses = new ArrayList<>();

        String debtorAccountNo = "", product = "";
        String[] lastPayment = null;

        try {
            session = sessionFactory.openSession();
            MCompany company = userDaoImpl.findCompanyDetails();
            portfolioDetail.setBranch("ALL");
            portfolioDetail.setPrintedUserName("");
            portfolioDetail.setPrintedDate(dateFormat.format(new Date()));
            portfolioDetail.setCompanyName(company.getComName());
            portfolioDetail.setFollowUpOffice("System User");

            List<LoanHeaderDetails> loanList = null;
            if (grValue.equals("1")) {
                String qry = "from LoanHeaderDetails where loanInstallment > :grossRentals";
                loanList = session.createQuery(qry).setParameter("grossRentals", grossRentals).list();
            } else if (grValue.equals("2")) {
                String qry = "from LoanHeaderDetails where loanInstallment < :grossRentals";
                loanList = session.createQuery(qry).setParameter("grossRentals", grossRentals).list();
            }

            for (int i = 0; i < loanList.size(); i++) {
                LoanHeaderDetails details = (LoanHeaderDetails) loanList.get(i);
                int loanId = details.getLoanId();
                Double[] arrearsAmounts = getArrearsAmounts(loanId);
                product = findLoanTypeName(details.getLoanType());
                portfolioDetail.setProduct(product);

                //Loan Vehicle details
                LoanPropertyVehicleDetails vehicleDetails = (LoanPropertyVehicleDetails) session.createCriteria(LoanPropertyVehicleDetails.class)
                        .add(Restrictions.eq("loanId", loanId)).setMaxResults(1).uniqueResult();
                //Loan Insurance Details
                String companyNames = "";
                List<InsuraceProcessMain> processMain = session.createCriteria(InsuraceProcessMain.class)
                        .add(Restrictions.eq("insuLoanId", loanId)).list();
                for (InsuraceProcessMain main : processMain) {
                    if (processMain.size() == 1) {
                        companyNames = getInsuranceCompanyName(main.getInsuCompanyId());
                    } else {
                        companyNames = companyNames + "/" + getInsuranceCompanyName(main.getInsuCompanyId());
                    }
                }

                ////create List PortfolioDetails//////
                PortfolioDetails pd = new PortfolioDetails();
                pd.setSerial_No(i + 1);
                pd.setDebtor_Code(debtorAccountNo);
                pd.setDebtor_Name(details.getDebtorHeaderDetails().getNameWithInitial());
                pd.setDebtor_Nic(details.getDebtorHeaderDetails().getDebtorNic());
                pd.setLoan_Agreement_No(details.getLoanAgreementNo());
                pd.setFacility_Type(getLoanFacilityType(loanId));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(details.getLoanStartDate()));
                }
                pd.setLoan_Amount(df.format(details.getLoanAmount().doubleValue()));
                pd.setLoan_Period(String.valueOf(details.getLoanPeriod()));
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_End_Date(dateFormat.format(details.getLoanLapsDate()));
                }
                if (details.getLoanStartDate() != null) {
                    pd.setLoan_Date(dateFormat.format(getLoanDueDate(loanId)));
                }
                pd.setAdvance_Payment_Amount(df.format(0.00));
                pd.setPre_Payment_Amount(df.format(0.00));
                pd.setTotal_Down_Payment(df.format(details.getLoanDownPayment()));
                pd.setCurrent_Month_Rental(df.format(details.getLoanInstallment()));
                pd.setCapital_Outstanding(df.format(getCapitalOutStanding(loanId)));
                pd.setNo_Of_Rental_In_Arrears(df.format(getNoOfRentalInArrears(loanId)));
                pd.setRental_Amount_In_Arrears(df.format(arrearsAmounts[2]));
                pd.setCapital_Amount_In_Arrears(df.format(arrearsAmounts[1]));
                pd.setInterest_Amount_In_Arrears(df.format(arrearsAmounts[0]));
                pd.setInterest_In_Suspens_Amount(df.format(0.00));
                pd.setOdi_Arrears_Amount(df.format(getOdiArrearsAmount(loanId)));
                pd.setOther_Charges_Arears(df.format(getOtherChargersArrearsAmount(loanId)));
                pd.setToatal_Arears_Amount(df.format(findLoanBalance(loanId)));
                lastPayment = getLastPaymentDetails(loanId);
                if (lastPayment[0] != null && lastPayment[1] != null) {
                    pd.setLast_Payment_Date(lastPayment[0]);
                    pd.setLast_Payment_Amount(df.format(Double.valueOf(lastPayment[1])));
                }
                pd.setArrears_Days(getArrearsDays(loanId));
                pd.setCollection_On_Rental_During_Current_Month(df.format(0.00));
                pd.setCollection_On_Arrears_During_Current_Month(df.format(0.00));
                pd.setOver_Due_Collected_Amount(df.format(getOverDueCollectedAmount(loanId)));
                pd.setOther_Charge_Collected_Amount(df.format(getOtherChargeCollectedAmount(loanId)));
                pd.setTotal_Collected_Amount(df.format(getTotalColectedAmount(loanId)));
                pd.setTotal_Over_Payment_Amount(df.format(getTotalOverPayAmount(loanId)));
                Double[] loanFutureDetail = getLoanFutureDetails(loanId);
                pd.setFuture_Interest_Amount(df.format(loanFutureDetail[0]));
                pd.setFuture_Capital_Amount(df.format(loanFutureDetail[1]));
                pd.setBad_Debtor_Provision_Amount(df.format(0.00));
                pd.setLoan_Status(getLoanStatus(details));
                pd.setContact_Person("");
                pd.setTelephone_No(details.getDebtorHeaderDetails().getDebtorTelephoneMobile());
                if (details.getLoanMarketingOfficer() != null) {
                    pd.setMarketing_Officer(adminDaoImpl.findEmoloyeeLastName(details.getLoanMarketingOfficer()));
                }
                pd.setFollow_Up_Officer(findRecoveryOfficerByLoan(loanId));
                if (vehicleDetails != null) {
                    if (!"".equals(vehicleDetails.getVehicleRegNo())) {
                        pd.setVehicle_No(vehicleDetails.getVehicleRegNo());
                    } else {
                        pd.setVehicle_No("");
                    }
                    if (!"".equals(vehicleDetails.getVehicleEngineNo())) {
                        pd.setEngine_Number(vehicleDetails.getVehicleEngineNo());
                    } else {
                        pd.setEngine_Number("");
                    }
                    if (vehicleDetails.getVehiclChassisNo() != null) {
                        pd.setChasis_No(vehicleDetails.getVehiclChassisNo());
                    } else {
                        pd.setChasis_No("");
                    }
                    if (vehicleDetails.getVehicleMake() != null) {
                        pd.setVehicle_Class(getVehicleClass(vehicleDetails.getVehicleMake()));
                    } else {
                        pd.setVehicle_Class("");
                    }
                    if (vehicleDetails.getVehicleType() != null) {
                        pd.setVehicle_Type(getVehicleType(vehicleDetails.getVehicleType()));
                    } else {
                        pd.setVehicle_Type("");
                    }
                }
                pd.setInsurance_Company(companyNames);
                pd.setInsurance_Assigned("");
                pd.setLoan_Interest_Rate(df.format(details.getLoanInterestRate()));
                pd.setDue_Day(details.getDueDay());
                portfolioDetailses.add(pd);
                pd = null;
            }
            portfolioDetail.setDetails(portfolioDetailses);
        } catch (HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return portfolioDetail;
    }

    @Override
    public List<ActiveContracts> getActiveContract(String date1, String date2) {
        List<ActiveContracts> contracts = new ArrayList();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
            Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
            List<LoanHeaderDetails> loans = session.createCriteria(LoanHeaderDetails.class).add(Restrictions.between("loanStartDate", d1, d2)).add(Restrictions.eq("loanIsIssue", 2)).list();
            if (loans != null) {
                for (LoanHeaderDetails loan : loans) {
                    ActiveContracts ac = new ActiveContracts();
                    ac.setAgreementNo(loan.getLoanAgreementNo());
                    ac.setName(loan.getDebtorHeaderDetails().getDebtorName());
                    ac.setAmount(Double.parseDouble(loan.getLoanAmount().toString()));
                    ac.setInterest(loan.getLoanInterestRate());
                    ac.setTotalInterest(loan.getLoanInterest());
                    ac.setDownPayment(loan.getLoanDownPayment());
                    ac.setPeriod(loan.getLoanPeriod());
                    ac.setActiveDate(loan.getLoanStartDate().toString());
                    ac.setSector(" ");
                    ac.setSubsector(" ");

                    Employee em = (Employee) session.load(Employee.class, loan.getLoanMarketingOfficer());
                    if (em != null) {
                        ac.setMarketing(em.getEmpFname());
                    }

                    double gross = 0.00;
                    String hql = "SELECT MAX(l.insAmount) FROM LoanInstallment l WHERE l.loanId='" + loan.getLoanId() + "' ";
                    Object insAmount = session.createQuery(hql).uniqueResult();
                    if (insAmount != null) {
                        gross = Double.parseDouble(insAmount.toString());
                    }
                    ac.setGrossAmount(gross);

                    String hqlVehicle = "from LoanPropertyVehicleDetails where loanId='" + loan.getLoanId() + "'";
                    LoanPropertyVehicleDetails vehicle = (LoanPropertyVehicleDetails) session.createQuery(hqlVehicle).uniqueResult();
                    if (vehicle != null) {
                        if (vehicle.getMarketPrice() != null) {
                            ac.setMarketValue(vehicle.getMarketPrice());
                        } else {
                            ac.setMarketValue(0.00);
                        }
                        if (vehicle.getVehiclePrice() != null) {
                            ac.setForcedSaleValue(vehicle.getVehiclePrice());
                        } else {
                            ac.setForcedSaleValue(0.00);
                        }
                        if (vehicle.getManufacturedYear() != null) {
                            ac.setYear(vehicle.getManufacturedYear());
                        } else {
                            ac.setYear("");
                        }
                    }
                    VehicleMake make = (VehicleMake) session.createCriteria(VehicleMake.class).add(Restrictions.eq("vehicleMake", Integer.parseInt(vehicle.getVehicleMake()))).uniqueResult();
                    if (make != null) {
                        ac.setMake(make.getVehicleMakeName());
                    }
                    VehicleModel model = (VehicleModel) session.createCriteria(VehicleModel.class).add(Restrictions.eq("vehicleModel", Integer.parseInt(vehicle.getVehicleModel()))).uniqueResult();
                    if (model != null) {
                        ac.setModel(model.getVehicleModelName());
                    }
                    contracts.add(ac);
                }
            }
        } catch (HibernateException | ParseException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return contracts;
    }

    @Override
    public List<ActiveContracts> getNotActiveContract(String date1, String date2) {
        List<ActiveContracts> contracts = new ArrayList();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
            Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
            List<LoanHeaderDetails> loans = session.createCriteria(LoanHeaderDetails.class).add(Restrictions.between("loanDate", d1, d2)).add(Restrictions.eq("loanIsIssue", 0)).list();
            if (loans != null) {
                for (LoanHeaderDetails loan : loans) {
                    ActiveContracts ac = new ActiveContracts();
                    ac.setAgreementNo("-");
                    ac.setName(loan.getDebtorHeaderDetails().getDebtorName());
                    ac.setAmount(Double.parseDouble(loan.getLoanAmount().toString()));
                    ac.setInterest(loan.getLoanInterestRate());
                    ac.setTotalInterest(loan.getLoanInterest());
                    ac.setDownPayment(loan.getLoanDownPayment());
                    ac.setPeriod(loan.getLoanPeriod());
                    ac.setSector(loan.getLoanDate().toString());
                    ac.setSubsector(" ");

                    if (loan.getLoanMarketingOfficer() != null) {
                        Employee em = (Employee) session.load(Employee.class, loan.getLoanMarketingOfficer());
                        ac.setMarketing(em.getEmpFname());
                    }

                    contracts.add(ac);
                }
            }
        } catch (ParseException | HibernateException | NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return contracts;
    }

    @Override
    public List<RmvReceivable> getRmvReceivableRecords(String date1, String date2) {
        List<RmvReceivable> receivables = new ArrayList();
        int charges_id = 3;  // for RMV
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
            Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
            List<LoanHeaderDetails> loans = session.createCriteria(LoanHeaderDetails.class).add(Restrictions.between("loanDate", d1, d2)).add(Restrictions.eq("loanIsIssue", 2)).list();
            if (loans != null) {
                for (LoanHeaderDetails loan : loans) {
                    RmvReceivable rr = new RmvReceivable();
                    rr.setFacilityNo(loan.getLoanAgreementNo());
                    // load status
                    if (loan.getLoanStatus() == 0) {
                        rr.setFacilityStatus("Rejected");
                    } else if (loan.getLoanStatus() == 1) {
                        rr.setFacilityStatus("Pending");
                    }
                    List<LoanOtherCharges> charges = session.createCriteria(LoanOtherCharges.class).add(Restrictions.eq("loanId", loan.getLoanId())).add(Restrictions.eq("chargesId", charges_id)).list();
                    // to be paid
                    double toBePaid = 0;
                    double paid = 0;
                    double due = 0;

                    for (LoanOtherCharges charge : charges) {
                        toBePaid += charge.getChargeAmount();
                    }

                    List<SettlementCharges> list = session.createCriteria(SettlementCharges.class).add(Restrictions.eq("loanId", loan.getLoanId())).add(Restrictions.eq("chargeType", "OTHR-3")).list();

                    for (SettlementCharges settlementCharges : list) {
                        paid += settlementCharges.getCharge();
                    }

                    due = toBePaid - paid;

                    rr.setAmountToSettele(toBePaid);
                    rr.setDueAmount(due);
                    rr.setSetttledAmmount(paid);
                    rr.setDueDate(loan.getLoanDueDate().toString());
                    rr.setClient("0");
                    rr.setVat(0);
                    rr.setCloseType("0");
                    rr.setClient(loan.getDebtorHeaderDetails().getDebtorName());

                    receivables.add(rr);
                }
            }
        } catch (ParseException | HibernateException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return receivables;
    }

    @Override
    public List<ServiceChargeReceivable> getServiceChargeReceivable(String date1, String date2) {
        List<ServiceChargeReceivable> receivables = new ArrayList();
        int charges_id = 1;  // for Service Charge
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
            Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
            List<LoanHeaderDetails> loans = session.createCriteria(LoanHeaderDetails.class).add(Restrictions.between("loanDate", d1, d2)).add(Restrictions.eq("loanIsIssue", 2)).list();
            if (loans != null) {
                for (LoanHeaderDetails loan : loans) {
                    ServiceChargeReceivable rr = new ServiceChargeReceivable();
                    rr.setFacilityNo(loan.getLoanAgreementNo());
                    // load status
                    if (loan.getLoanStatus() == 0) {
                        rr.setFacilityStatus("Rejected");
                    } else if (loan.getLoanStatus() == 1) {
                        rr.setFacilityStatus("Pending");
                    }
                    List<LoanOtherCharges> charges = session.createCriteria(LoanOtherCharges.class).add(Restrictions.eq("loanId", loan.getLoanId())).add(Restrictions.eq("chargesId", charges_id)).list();

                    double toBePaid = 0;
                    double paid = 0;
                    double due = 0;

                    for (LoanOtherCharges charge : charges) {
                        toBePaid += charge.getChargeAmount();
                    }

                    List<SettlementCharges> list = session.createCriteria(SettlementCharges.class).add(Restrictions.eq("loanId", loan.getLoanId())).add(Restrictions.eq("chargeType", "OTHR-3")).list();

                    for (SettlementCharges settlementCharges : list) {
                        paid += settlementCharges.getCharge();
                    }

                    due = toBePaid - paid;

                    rr.setAmountToSettele(toBePaid);
                    rr.setDueAmount(due);
                    rr.setSetttledAmmount(paid);
                    rr.setDueDate(loan.getLoanDueDate().toString());
                    rr.setClient("0");
                    rr.setVat(0);
                    rr.setCloseType("0");
                    rr.setClient(loan.getDebtorHeaderDetails().getDebtorName());

                    receivables.add(rr);
                }
            }
        } catch (ParseException | HibernateException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return receivables;
    }

    private double getDebitOdiChargers(int loanId, String initiateFromDate, String initiateToDate) {
        Double dbtOdiCharge = 0.00;
        try {
            int userId = userDaoImpl.findByUserName();
            int userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
            userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
            Date systemDate = settlementDaoImpl.getSystemDate(userLogBranchId);
            String sql = "";

            sql = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE loanId= " + loanId + " and transactionCodeDbt like 'ODI' and description like 'ODI Charges' ";

            if (initiateFromDate != null && initiateToDate != null) {
                sql += " and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "'";
            }

//            String sql = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE loanId= " + loanId + " and transactionCodeDbt like 'ODI' and description like 'ODI Charges' and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "'";
            Object odiChargers = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            if (odiChargers != null) {
                dbtOdiCharge = Double.parseDouble(odiChargers.toString());
            }
//            dbtOdiCharge = Double.parseDouble(odiChargers.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbtOdiCharge;
    }

    private Object getCreditOdiChargers(int loanId, String initiateFromDate, String initiateToDate) {
        Double cdtOdiCharge = 0.00;
        try {
            int userId = userDaoImpl.findByUserName();
            int userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
            userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
            Date systemDate = settlementDaoImpl.getSystemDate(userLogBranchId);
            String sql = "";

            sql = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE loanId = " + loanId + " and transactionCodeCrdt like 'ODI' and description like 'Over Due Charge' ";
            if (initiateFromDate != null && initiateToDate != null) {
                sql += " and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "'";
            }

            Object odiChargers = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            if (odiChargers != null) {
                cdtOdiCharge = Double.parseDouble(odiChargers.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cdtOdiCharge;
    }

    private Object getWaveOffOdiChargers(int loanId, String initiateFromDate, String initiateToDate) {
        Double waveOffOdiChargers = 0.00;
        try {
            int userId = userDaoImpl.findByUserName();
            int userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
            userLogBranchId = settlementDaoImpl.getUserLogedBranch(userId);
            Date systemDate = settlementDaoImpl.getSystemDate(userLogBranchId);
            String sql = "";

            sql = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE loanId = " + loanId + " and transactionCodeDbt like 'ODI' and description like 'ODI Adjustment' ";

            if (initiateFromDate != null && initiateToDate != null) {
                sql += " and systemDate BETWEEN '" + initiateFromDate + "' and '" + initiateToDate + "'";
            }

            Object odiChargers = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            if (odiChargers != null) {
                waveOffOdiChargers = Double.parseDouble(odiChargers.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return waveOffOdiChargers;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.ApprovalDetailsModel;
import com.bankfinance.form.CustomerInfo;
import com.bankfinance.form.GuranterDetailsModel;
import com.bankfinance.form.InvoiseDetails;
import com.bankfinance.form.LoanForm;
import com.bankfinance.form.OtherChargseModel;
import com.bankfinance.form.PaymentDetails;
import com.bankfinance.form.PaymentVoucher;
import com.bankfinance.form.RecoveryReportModel;
import com.bankfinance.form.VoucherDetails;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.dao.LoanDao;
import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.model.ConfigChartofaccountBank;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.LoanApprovedLevels;
import com.ites.bankfinance.model.LoanCapitalizeDetail;
import com.ites.bankfinance.model.LoanCheckList;
import com.ites.bankfinance.model.LoanGuaranteeDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanInstalmentSchedule;
import com.ites.bankfinance.model.LoanOtherCharges;
import com.ites.bankfinance.model.LoanPendingDates;
import com.ites.bankfinance.model.LoanPropertyArticlesDetails;
import com.ites.bankfinance.model.LoanPropertyLandDetails;
import com.ites.bankfinance.model.LoanPropertyOtherDetails;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.LoanRecoveryReport;
import com.ites.bankfinance.model.LoanRescheduleDetails;
import com.ites.bankfinance.model.LoanReverseDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MCaptilizeType;
import com.ites.bankfinance.model.MCheckList;
import com.ites.bankfinance.model.MCompany;
import com.ites.bankfinance.model.MGroup;
import com.ites.bankfinance.model.MPeriodTypes;
import com.ites.bankfinance.model.MSubLoanChecklist;
import com.ites.bankfinance.model.MSubLoanOtherChargers;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.MSubTaxCharges;
import com.ites.bankfinance.model.MSupplier;
import com.ites.bankfinance.model.Posting;
import com.ites.bankfinance.model.RecoveryLetterDetail;
import com.ites.bankfinance.model.RecoveryLetters;
import com.ites.bankfinance.model.SettlementChequeDetails;
import com.ites.bankfinance.model.SettlementMain;
import com.ites.bankfinance.model.SettlementPaymentDetailCash;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementVoucher;
import com.ites.bankfinance.model.SettlementVoucherDelete;
import com.ites.bankfinance.model.UmUserTypeApproval;
import com.ites.bankfinance.model.VehicleMake;
import com.ites.bankfinance.model.VehicleModel;
import com.ites.bankfinance.reportManager.DBFacade;
import com.ites.bankfinance.reportManager.ReportManager;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.SettlmentService;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class LoanDaoImpl implements LoanDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DebtorDaoImpl debImpl;

    @Autowired
    private SettlementDaoImpl setImpl;

    @Autowired
    private AdminDaoImpl adminDaoImpl;

    @Autowired
    private DebtorDaoImpl debtorDaoImpl;

    @Autowired
    private ReportDaoImpl reportDaoImpl;

    @Autowired
    private RecoveryDaoImpl recoveryDaoImpl;

    @Autowired
    private MasterDataService masterDataService;

    @Autowired
    private LoanService loanService;

    @Autowired
    SettlmentService settlmentService;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DecimalFormat df = new DecimalFormat("##,###.00");
    public static final DecimalFormat df2 = new DecimalFormat("###.##");

    //direct posting finance Account
    public final String CHEQUE_IN_HAND_ACC_CODE = "CHEQUE_IN_HAND";
    public final String PAYBLE_ACC_CODE = "PAYBLE";
    public final String DWN_ACC_CODE = "DWN";
    public final String EXSS_ACC_CODE = "EXSS";
    public final String INTRS_ACC_CODE = "INTRS";
    public final String INSURANCE_P_ACC_CODE = "INS_P";
    public final String INSURANCE_C_ACC_CODE = "INS_C";

    @Override
    public Map<String, List> findByLoanId(int loanId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List findByNic(String nic, Object bID) {
        List debtorDetails = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorNic like '%" + nic + "%' and debtorIsActive=1 and branchId = " + bID + "";
            debtorDetails = sessionFactory.getCurrentSession().createQuery(sql).list();
            return debtorDetails;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List findByName(String name, Object bID) {
        List debtors = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorName like '%" + name + "%' and debtorIsActive=1 and branchId = " + bID + " ";

            debtors = sessionFactory.getCurrentSession().createQuery(sql).list();
            return debtors;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Boolean findByNicNo(String nic, Object bID) {
        DebtorHeaderDetails debtorDetails = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorNic = '" + nic + "' and debtorIsActive=1 and branchId = " + bID + "";
            debtorDetails = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if(debtorDetails==null){
                return false;
            }else{
                return true;
            }
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List findByLoanNo(String loanNo, Object bID) {
        List debtors = new ArrayList();
        try {
            String bookNo = loanNo.replaceAll("-", "/");
            String sql2 = "SELECT loan.debtorHeaderDetails FROM LoanHeaderDetails as loan WHERE loan.debtorHeaderDetails.branchId = " + bID + " and loan.loanIsDelete!='1' AND (loan.loanBookNo LIKE '" + bookNo + "%' OR loan.loanAgreementNo LIKE '" + bookNo + "%') ";
            List debtorList = sessionFactory.getCurrentSession().createQuery(sql2).list();

            if (debtorList.size() > 0) {
                for (int i = 0; i < debtorList.size(); i++) {
                    DebtorHeaderDetails deb = (DebtorHeaderDetails) debtorList.get(i);
                    debtors.add(deb);
                }
            }
            return debtors;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List findByVehicleNo(String vehicleNo) {
        List debtors = new ArrayList();
        try {
            String sql1 = "select loanId from LoanPropertyVehicleDetails where vehicleRegNo like :vehicleRegNo";
            List loanIds = sessionFactory.getCurrentSession().createQuery(sql1)
                    .setParameter("vehicleRegNo", "%" + vehicleNo + "%").list();
            if (!loanIds.isEmpty()) {
                String sql2 = "SELECT loan.debtorHeaderDetails FROM LoanHeaderDetails as loan WHERE loan.loanIsDelete = :loanIsDelete AND loan.loanId in (:loanIds)";
                List debtorList = sessionFactory.getCurrentSession().createQuery(sql2)
                        .setParameter("loanIsDelete", false)
                        .setParameterList("loanIds", loanIds).list();
                if (debtorList.size() > 0) {
                    for (int i = 0; i < debtorList.size(); i++) {
                        DebtorHeaderDetails deb = (DebtorHeaderDetails) debtorList.get(i);
                        debtors.add(deb);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtors;
    }

    @Override
    public DebtorHeaderDetails findById(int id) {
        DebtorHeaderDetails debtor = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorId=" + id + " and debtorIsActive=1";
            debtor = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            return debtor;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List findLoansByDebtorId(int debtorId) {
        List<LoanHeaderDetails> loans = new ArrayList<>();
        try {
            String sql = "from DebtorHeaderDetails where debtorId=" + debtorId + " and debtorIsActive=1";
            DebtorHeaderDetails debtor = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            if (debtor != null) {
                String sql2 = "from LoanHeaderDetails where debtorHeaderDetails.debtorId=" + debtorId + " and loanIsDelete!='1' ";
                List<LoanHeaderDetails> list = sessionFactory.getCurrentSession().createQuery(sql2).list();
                for (LoanHeaderDetails loanHeaderDetails : list) {
                    int loanId = loanHeaderDetails.getLoanId();
                    LoanPropertyVehicleDetails vehicleDetails = findVehicleByLoanId(loanId);
                    if (vehicleDetails != null) {
                        loanHeaderDetails.setVehicleNo(vehicleDetails.getVehicleRegNo());
                        loans.add(loanHeaderDetails);
                    } else {
                        loans.add(loanHeaderDetails);
                    }
                }
            }
            return loans;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List findGuarantorByDebtorId(int debtorId) {
        try {
            List<viewLoan> g_loanList = new ArrayList();
            String sql0 = "select h.loanId, h.debtorHeaderDetails, h.loanAmount, h.loanPeriod, h.loanPeriodType,h.loanInterestRate, h.loanInstallment, h.loanType, h.loanAgreementNo, h.loanStatus, h.loanDate, h.loanSpec from LoanGuaranteeDetails as g, LoanHeaderDetails as h where g.loanId=h.loanId and g.debtorId = " + debtorId;
            List guarantorList = sessionFactory.getCurrentSession().createQuery(sql0).list();
            if (guarantorList.size() > 0) {
                for (int i = 0; i < guarantorList.size(); i++) {
                    Object[] obj = (Object[]) guarantorList.get(i);
                    DebtorHeaderDetails db = (DebtorHeaderDetails) obj[1];
                    viewLoan g_loan = new viewLoan();
                    g_loan.setDebtorName(db.getNameWithInitial());
                    g_loan.setDebtorAddress(db.getDebtorPersonalAddress());
                    double amount = 0.00;
                    if (obj[2] != null) {
                        amount = Double.parseDouble(obj[2].toString());
                    }
                    g_loan.setLoanAmount(amount);
                    int periodType = Integer.parseInt(obj[4].toString());
                    String p_type = "";
                    if (periodType == 1) {
                        p_type = "Month";
                    } else if (periodType == 2) {
                        p_type = "Year";
                    } else {
                        p_type = "Days";
                    }
                    String agNo = "";
                    if (obj[8] != null) {
                        agNo = obj[8].toString();
                    }
                    int loanType = Integer.parseInt(obj[7].toString());
                    String sql2 = "From MSubLoanType where subLoanId=" + loanType;
                    MSubLoanType subLoan = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                    int loanSpec = Integer.parseInt(obj[11].toString());
                    int loanStatus = Integer.parseInt(obj[9].toString());
                    String typeName = subLoan.getSubLoanName();
                    g_loan.setLoanType(typeName);
                    g_loan.setLoanPeriod(obj[3].toString() + " " + p_type);
                    g_loan.setAgreementNo(agNo);
                    g_loan.setLoanSpec(loanSpec);
                    g_loan.setLoanStatus(loanStatus);
                    g_loan.setLoanStatus(loanStatus);
                    g_loan.setBranchId(db.getBranchId());
                    g_loanList.add(g_loan);
                }
            }

            return g_loanList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int saveNewLoanData(LoanForm loanData) {
        String message = null;
        int loanId = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean update = false;
        try {
            int userId = userDao.findByUserName();

            Calendar calendar = Calendar.getInstance();
            Date actionTime = calendar.getTime();
            String current_date = dateFormat.format(actionTime);

            int debtorId = loanData.getDebtorId();
            String sql = "from DebtorHeaderDetails where debtorId=" + debtorId;
            DebtorHeaderDetails debtor = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (debtor.getDebtorAccountNo() == null || debtor.getDebtorAccountNo().equals("")) {
                String accNo = debImpl.generateDebtorAccount();
                debtor.setDebtorAccountNo(accNo);
                debtor.setDebtorIsDebtor(true);
            }

            LoanHeaderDetails headerData = new LoanHeaderDetails();
            if (loanData.getLoanId() != null) {
                headerData.setLoanId(loanData.getLoanId());
                update = true;
            }
            //double roundedValue = (Double) (Math.ceil(loanData.getLoanInstallment() / 5d) * 5);
            double downPayment = loanData.getLoanDownPayment();
            headerData.setLoanAmount(loanData.getLoanAmount());
            headerData.setLoanDownPayment(downPayment);
            headerData.setLoanInvestment(loanData.getLoanInvestment());
            headerData.setLoanPeriod(loanData.getLoanPeriod());
            headerData.setLoanPeriodType(loanData.getPeriodType());
            headerData.setLoanInterestRate(loanData.getLoanRate());
            headerData.setLoanType(loanData.getLoanType());
            headerData.setLoanRateCode(loanData.getLoanRateCode());
            headerData.setLoanDate(dateFormat.parse(current_date));
            headerData.setLoanInstallment(loanData.getLoanInstallment());
            headerData.setLoanInterest(loanData.getLoanInterest());
            headerData.setLoanMarketingOfficer(loanData.getMarketingOfficer());
            headerData.setLoanApproval1(loanData.getSalesPerson());
            headerData.setLoanApproval2(loanData.getRecoveryPerson());
            headerData.setLoanApproval3(loanData.getCed());
            headerData.setLoanApp1Comment(loanData.getComment1());
            headerData.setLoanApp2Comment(loanData.getComment2());
            headerData.setLoanBookNo(loanData.getBookNo());
            headerData.setLoanDueDate(loanData.getDueDate());

            String docSubmit = "from MSubLoanChecklist where MSubLoanType.subLoanId = " + loanData.getLoanType();
            List docList = sessionFactory.getCurrentSession().createQuery(docSubmit).list();
            if (!docList.isEmpty()) {
                if (loanData.getBranchId() == 1 || loanData.getBranchId() == 2) {
                    headerData.setLoanDocumentSubmission(3);
                } else {
                    headerData.setLoanDocumentSubmission(1);
                }
            } else {
                headerData.setLoanDocumentSubmission(3);
            }

            headerData.setDebtorHeaderDetails(debtor);
            headerData.setActionTime(actionTime);
            headerData.setUserId(userId);
            headerData.setBranchId(loanData.getBranchId());
            headerData.setDueDay(loanData.getDueDay());
            if (loanData.getStartDate() != null) {
                headerData.setLoanStartDate(loanData.getStartDate());
            }
            if (loanData.getDueDate() != null) {
                headerData.setLoanDueDate(loanData.getDueDate());
            }

            if (loanData.getBookNo() != null) {
                headerData.setLoanBookNo(loanData.getBookNo());
            } else {
                headerData.setLoanBookNo("");
            }

            session.saveOrUpdate(headerData);

            loanId = headerData.getLoanId();
            //set loan Guarantors
            int order = 1;

            String sql5 = "from LoanGuaranteeDetails where loanId=" + loanId;
            List<LoanGuaranteeDetails> g_list = session.createQuery(sql5).list();
            if (g_list.size() > 0) {
                for (int i = 0; i < g_list.size(); i++) {
                    LoanGuaranteeDetails guarantor = (LoanGuaranteeDetails) g_list.get(i);
                    session.delete(guarantor);
                }
            }

            if (loanData.getGuarantorID() != null) {
                if (loanData.getGuarantorID().length > 0) {
                    for (int i = 0; i < loanData.getGuarantorID().length; i++) {
                        order = order + i;
                        LoanGuaranteeDetails guarantor = new LoanGuaranteeDetails();
                        guarantor.setLoanId(loanId);
                        guarantor.setDebtorId(loanData.getGuarantorID()[i]);
                        guarantor.setGurantorOrder(order);
                        guarantor.setActionTime(actionTime);
                        guarantor.setUserId(userId);
                        session.saveOrUpdate(guarantor);
                    }
                }
            }
            //set loan Other Charges

            String sql6 = "from LoanOtherCharges where loanId=" + loanId + " and isDownPaymentCharge=1";
            List<LoanOtherCharges> other_list = session.createQuery(sql6).list();
            if (other_list.size() > 0) {
                for (int i = 0; i < other_list.size(); i++) {
                    LoanOtherCharges charg = (LoanOtherCharges) other_list.get(i);
                    session.delete(charg);
                }
            }
            double chargSum = 0.00;
            if (loanData.getChragesId() != null) {
                if (loanData.getChragesId().length > 0) {
                    for (int i = 0; i < loanData.getChragesId().length; i++) {
                        int c_id = loanData.getChragesId()[i];
                        String sql4 = "from MSubTaxCharges where subTaxId=" + c_id;
                        MSubTaxCharges subTax = (MSubTaxCharges) sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();

                        LoanOtherCharges charges = new LoanOtherCharges();
                        charges.setLoanId(loanId);
                        charges.setChargesId(c_id);
                        charges.setActualRate(subTax.getSubTaxRate());
                        if (subTax.getIsPrecentage()) {
                            double actual_amount = (loanData.getLoanInvestment() * subTax.getSubTaxRate()) / 100;
                            charges.setActualAmount(actual_amount);
                        } else {
                            double actual_amount = subTax.getSubTaxValue();
                            charges.setActualAmount(actual_amount);
                        }
                        charges.setChargeRate(loanData.getChragesRate()[i]);
                        double chargAmnt = 0.00;
                        if (loanData.getChragesAmount()[i] != null) {
                            chargAmnt = loanData.getChragesAmount()[i];
                        }
                        chargSum = chargSum + chargAmnt;
                        charges.setChargeAmount(chargAmnt);
                        charges.setAccountNo(subTax.getSubTaxAccountNo());
                        charges.setIsDownPaymentCharge(1);
                        charges.setActionTime(actionTime);
                        charges.setUserId(userId);
                        session.save(charges);
                    }
                }
            }
            //set other property
            if (loanData.getProperyId() != null) {
                for (int i = 0; i < loanData.getProperyId().length; i++) {
                    int type = loanData.getProprtyType();
                    if (type == 1) {
                        String sql2 = "From LoanPropertyVehicleDetails where vehicleId=" + loanData.getProperyId()[i];
                        LoanPropertyVehicleDetails property = (LoanPropertyVehicleDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    } else if (type == 2) {
                        String sql2 = "From LoanPropertyArticlesDetails where articleId=" + loanData.getProperyId()[i];
                        LoanPropertyArticlesDetails property = (LoanPropertyArticlesDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    } else if (type == 3) {
                        String sql2 = "From LoanPropertyLandDetails where landId=" + loanData.getProperyId()[i];
                        LoanPropertyLandDetails property = (LoanPropertyLandDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    } else if (type == 4) {
                        String sql2 = "From LoanPropertyOtherDetails where otherId=" + loanData.getProperyId()[i];
                        LoanPropertyOtherDetails property = (LoanPropertyOtherDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    }
                }
            }

            // set capitalize types
            if (loanData.getCapitalizeAmount() != null && loanData.getCapitalizeId() != null) {
                String sql1 = "delete from LoanCapitalizeDetail where loanHeaderDetails.loanId = " + loanId;
                session.createQuery(sql1).executeUpdate();
                for (int i = 0; i < loanData.getCapitalizeAmount().length; i++) {
                    int capitalizeId = loanData.getCapitalizeId()[i];
                    double amount = loanData.getCapitalizeAmount()[i];
                    LoanCapitalizeDetail capitalizeDetail = new LoanCapitalizeDetail();
                    capitalizeDetail.setAmount(amount);
                    capitalizeDetail.setLoanHeaderDetails(new LoanHeaderDetails(loanId));
                    capitalizeDetail.setMCaptilizeType(new MCaptilizeType(capitalizeId));
                    session.saveOrUpdate(capitalizeDetail);
                }
            }
            message = "Saved Successfully";
            tx.commit();
            session.flush();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanId;
    }

    @Override
    public List findLoanByUserType(int issuType, int branch, Date s_Date, Date e_Date) {
        List loanList = new ArrayList();
        try {
            int userId = userDao.findByUserName();
            int userType = userDao.findUserTypeByUserId(userId);
            List approvalList = userDao.findApproveLevelByUser(userId);
            List loanTypeList = userDao.findLoanTypesByUserId(userId);
            int empNo = userDao.findEmployeeByUserId(userId);

            String startDate = "";
            String endDate = "";
            Calendar cal = Calendar.getInstance();
            Date today = cal.getTime();
            if (e_Date == null) {
                endDate = dateFormat.format(today);
            } else {
                endDate = dateFormat.format(e_Date);
            }

            if (s_Date == null) {
                cal.add(Calendar.DATE, -3);
                startDate = dateFormat.format(cal.getTime());
            } else {
                startDate = dateFormat.format(s_Date);
            }
// sahan ekanayake 
            String sql1 = "";
            List loans = null;
            String issCond = "";
            Session session = sessionFactory.openSession();
            if (issuType == 0) {
                issCond = "loanIsIssue = 0";
            } else if (issuType == 1 || issuType == 2) {
//                issCond = " loanIsIssue = 1 and loanDate between '" + startDate + "' and '" + endDate + "'";
                issCond = "(loanIsIssue = 1 OR loanIsIssue = 2) and createVoucher = 0 and loanDate between '" + startDate + "' and '" + endDate + "'";
            } else if (issuType == 3) {
                issCond = " createVoucher = 3 and loanDate between '" + startDate + "' and '" + endDate + "'";
//                issCond = "(loanIsIssue = 1 OR loanIsIssue = 2) and loanDate between '" + startDate + "' and '" + endDate + "'";
            }

            if (userType == 1 || userType == 2 || userType == 7 || userType >= 10 || userType == 4) {
                sql1 = "from LoanHeaderDetails where loanIsActive=1 and loanIsDelete=0 and loanIsCancel=0 and " + issCond + " and loanDocumentSubmission>1 and branchId=" + branch + " order by loanId DESC";
                loans = session.createQuery(sql1).list();
//                loans = sessionFactory.getCurrentSession().createQuery(sql1).list();//Annr
            } else if (userType == 3 || userType == 8) {
                sql1 = "from LoanHeaderDetails where userId=" + userId + " and loanIsActive=1 and loanIsDelete=0 and loanIsCancel=0 and " + issCond + " and loanDocumentSubmission>1 and branchId=" + branch + " order by loanId DESC";
                loans = session.createQuery(sql1).list();
//                loans = sessionFactory.getCurrentSession().createQuery(sql1).list();//Annr
            } else {
                List<LoanHeaderDetails> loanList1 = new ArrayList();
                List<LoanHeaderDetails> loan1 = null, loan2 = null, loan3 = null;
                for (int i = 0; i < approvalList.size(); i++) {
                    UmUserTypeApproval app = (UmUserTypeApproval) approvalList.get(i);

                    if (app.getMApproveLevels().getAppId() == 1) {
                        String sql11 = "from LoanHeaderDetails where loanApproval1=" + userId + " and loanIsActive=1 and loanIsDelete=0 and loanIsCancel=0 and " + issCond + " and loanDocumentSubmission>1 and branchId=" + branch + " order by loanId DESC";
//                        loan1 = sessionFactory.getCurrentSession().createQuery(sql11).list();//Annr
                        loan1 = session.createQuery(sql11).list();
                    }
                    if (app.getMApproveLevels().getAppId() == 2) {
                        String sql12 = "from LoanHeaderDetails where loanApproval2=" + userId + " and loanIsActive=1 and loanIsDelete=0 and loanIsCancel=0 and " + issCond + " and loanDocumentSubmission>1 and branchId=" + branch + " order by loanId DESC";
//                        loan2 = sessionFactory.getCurrentSession().createQuery(sql12).list();//Annr
                        loan2 = session.createQuery(sql12).list();
                    }
                    if (app.getMApproveLevels().getAppId() == 3) {
                        String sql13 = "from LoanHeaderDetails where loanApproval3=" + userId + " and loanIsActive=1 and loanIsDelete=0 and loanIsCancel=0 and " + issCond + " and loanDocumentSubmission>1 and branchId=" + branch + " order by loanId DESC";
//                        loan3 = sessionFactory.getCurrentSession().createQuery(sql13).list();//Annr
                        loan3 = session.createQuery(sql13).list();
                    }
                }
                if (loan1 != null) {
                    loanList1.addAll(loan1);
                }
                if (loan2 != null) {
                    loanList1.addAll(loan2);
                }
                if (loan3 != null) {
                    loanList1.addAll(loan3);
                }

                Set hashList = new HashSet(loanList1);
                loans = new ArrayList<LoanHeaderDetails>(hashList);
            }

            if (loans != null) {
                for (int i = 0; i < loans.size(); i++) {
                    String name, address, rate, agreementNo = "", bookNo = "";
                    int loanId, loanType, periodType, period, app_status, app_level, app_document_status, isIssue;
                    double interest, installment;
                    Date loanDate = new Date();
                    double amount;
                    int app1 = 0, app2 = 0, app3 = 0;
                    String comment1 = "", comment2 = "";
                    LoanHeaderDetails loanHeader = (LoanHeaderDetails) loans.get(i);
                    if (loanHeader.getDebtorHeaderDetails().getNameWithInitial() != null) {
                        name = loanHeader.getDebtorHeaderDetails().getNameWithInitial();
                    } else {
                        name = "";
                    }
                    address = loanHeader.getDebtorHeaderDetails().getDebtorPersonalAddress();
                    loanId = loanHeader.getLoanId();
                    if (loanHeader.getLoanInvestment() != null) {
                        amount = loanHeader.getLoanInvestment();
                    } else {
                        amount = 0.00;
                    }
                    periodType = loanHeader.getLoanPeriodType();
                    period = loanHeader.getLoanPeriod();
                    loanType = loanHeader.getLoanType();
                    app_level = loanHeader.getLoanApprovalStatus();
                    app_status = loanHeader.getLoanStatus();
                    app_document_status = loanHeader.getLoanDocumentSubmission();
                    loanDate = loanHeader.getLoanDate();
                    isIssue = loanHeader.getLoanIsIssue();
                    if (loanHeader.getLoanAgreementNo() != null) {
                        agreementNo = loanHeader.getLoanAgreementNo();
                    }
                    if (loanHeader.getLoanBookNo() != null) {
                        bookNo = loanHeader.getLoanBookNo();
                    }
                    installment = loanHeader.getLoanInstallment();
                    if (loanHeader.getLoanApproval1() != null) {
                        app1 = loanHeader.getLoanApproval1();
                    }
                    if (loanHeader.getLoanApproval2() != null) {
                        app2 = loanHeader.getLoanApproval2();
                    }
                    if (loanHeader.getLoanApproval3() != null) {
                        app3 = loanHeader.getLoanApproval3();
                    }
                    if (loanHeader.getLoanApp1Comment() != null) {
                        comment1 = loanHeader.getLoanApp1Comment();
                    }
                    if (loanHeader.getLoanApp2Comment() != null) {
                        comment2 = loanHeader.getLoanApp2Comment();
                    }

//                } else {
//                    Object[] obj = (Object[]) loans.get(i);
//                    loanId = Integer.parseInt(obj[0].toString());
//                    DebtorHeaderDetails debtor = (DebtorHeaderDetails) obj[1];
//                    name = debtor.getDebtorName();
//                    address = debtor.getDebtorPersonalAddress();
//                    amount = BigDecimal.valueOf(Double.parseDouble(obj[2].toString()));
//                    period = Integer.parseInt(obj[3].toString());
//                    periodType = Integer.parseInt(obj[4].toString());
//                    rate = obj[5].toString();
//                    installment = Double.parseDouble(obj[6].toString());
//                    interest = Double.parseDouble(obj[7].toString());
//                    loanDate = (Date) obj[8];
//                    loanType = Integer.parseInt(obj[9].toString());
//                    app_level = Integer.parseInt(obj[10].toString());
//                    app_status = Integer.parseInt(obj[11].toString());
//                    app_document_status = Integer.parseInt(obj[12].toString());
//                    isIssue = Integer.parseInt(obj[13].toString());
//                    if (obj[14] != null) {
//                        agreementNo = obj[14].toString();
//                    }
//                }
                    viewLoan viewloan = new viewLoan();
                    viewloan.setDebtorHeaderDetails(loanHeader.getDebtorHeaderDetails());
                    viewloan.setDebtorName(name);
                    viewloan.setDebtorAddress(address);
                    viewloan.setLoanId(loanId);
                    viewloan.setLoanAmount(amount);
                    viewloan.setLoanInstallment(installment);
                    viewloan.setBranchId(loanHeader.getBranchId());
                    String newPeriod = "";
                    if (periodType == 1) {
                        newPeriod = "Month";
                    } else if (periodType == 2) {
                        newPeriod = "Year";
                    } else if (periodType == 3) {
                        newPeriod = "Days";
                    }
                    viewloan.setLoanPeriod(period + " " + newPeriod);

                    String sql2 = "select subLoanName from MSubLoanType where subLoanId=" + loanType;
//                    String loan_type = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult().toString();//Annr
                    String loan_type = session.createQuery(sql2).uniqueResult().toString();
                    viewloan.setLoanType(loan_type);
                    viewloan.setLoanApprovalStatus(app_level);
                    viewloan.setLoanStatus(app_status);
                    viewloan.setLoanDocumentSubmission(app_document_status);
                    viewloan.setLoanDate(loanDate);
                    viewloan.setDueDate(loanHeader.getLoanDueDate());
                    viewloan.setUserType(userType);
//                    viewloan.setAppLevelId(app_id);
                    viewloan.setLoanIsIssue(isIssue);
                    viewloan.setAgreementNo(agreementNo);
                    viewloan.setLoanBookNo(bookNo);

                    ApprovalDetailsModel u_approval = new ApprovalDetailsModel();
                    u_approval = findApprovalDetails(loanId);
                    viewloan.setUserApproval(u_approval);

                    String sql3 = "From LoanGuaranteeDetails where loanId=" + loanId;
//                    List<LoanGuaranteeDetails> guarntors = sessionFactory.getCurrentSession().createQuery(sql3).list();//Annr
                    List<LoanGuaranteeDetails> guarntors = session.createQuery(sql3).list();
                    List<DebtorHeaderDetails> guaranDetails = new ArrayList();
                    for (int j = 0; j < guarntors.size(); j++) {
                        DebtorHeaderDetails db = findById(guarntors.get(j).getDebtorId());
                        guaranDetails.add(db);
                    }
                    viewloan.setLoanGuarantor(guaranDetails);

                    String sql4 = "from LoanApprovedLevels where loanId=" + loanId;
//                    List<LoanApprovedLevels> loanApproval = sessionFactory.getCurrentSession().createQuery(sql4).list();//Annr
                    List<LoanApprovedLevels> loanApproval = session.createQuery(sql4).list();
                    viewloan.setLoanApprovals(loanApproval);
                    viewloan.setUserId(userId);
                    viewloan.setEmpId(empNo);



//                    MGroup group = (MGroup) sessionFactory.getCurrentSession().load(MGroup.class, loanHeader.getGroupId());
//                    System.out.println("================== group id : " + sessionFactory.getCurrentSession().load(MGroup.class, loanHeader.getGroupId()));
//                    System.out.println("================== group name : " + group.getGroupName());

//                    viewloan.setGroup(group.getGroupName());
                    loanList.add(viewloan);
                }
            }
            session.clear();
            session.close();
            return loanList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public viewLoan findLoanByLoanId(int loanId) {
        viewLoan loandata = null;
        String period = "";
        try {
            String sql = "From LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).list();
            loandata.setDebtorName(loan.getDebtorHeaderDetails().getDebtorName());
            loandata.setDebtorAddress(loan.getDebtorHeaderDetails().getDebtorPersonalAddress());
            loandata.setLoanAmount(loan.getLoanInvestment());
            int periodType = loan.getLoanPeriodType();
            if (periodType == 1) {
                period = "Month";
            } else if (periodType == 2) {
                period = "Year";
            } else if (periodType == 3) {
                period = "Days";
            }
            loandata.setLoanPeriod(loan.getLoanPeriod() + " " + period);
            int loanType = loan.getLoanType();
            String sql2 = "select subLoanName from MSubLoanType where subLoanId=" + loanType;
            String loan_type = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult().toString();
            loandata.setLoanType(loan_type);
            loandata.setLoanApprovalStatus(loan.getLoanApprovalStatus());
            loandata.setLoanStatus(loan.getLoanStatus());
            loandata.setLoanDate(loan.getLoanDate());

            String sql3 = "From LoanGuaranteeDetails where loanId=" + loan.getLoanId();
            List<LoanGuaranteeDetails> guarntors = sessionFactory.getCurrentSession().createQuery(sql3).list();
            List<DebtorHeaderDetails> guaranDetails = new ArrayList();
            for (int j = 0; j < guarntors.size(); j++) {
                DebtorHeaderDetails db = findById(guarntors.get(j).getDebtorId());
                guaranDetails.add(db);
            }
            loandata.setLoanGuarantor(guaranDetails);

            String sql4 = "From LoanOtherCharges where loanId=" + loan.getLoanId() + " and isDownPaymentCharge=1";
            List<LoanOtherCharges> charges = sessionFactory.getCurrentSession().createQuery(sql4).list();
            LoanOtherCharges[] chargesList = new LoanOtherCharges[charges.size()];
            for (int j = 0; j < guarntors.size(); j++) {
                chargesList[j] = charges.get(j);
            }
            loandata.setLoanOtherCharges(chargesList);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return loandata;
    }

    public int saveLoanApproval(LoanApprovedLevels loanApprove) {
        int approval = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            int userId = userDao.findByUserName();
            int usertype = userDao.findUserTypeByUserId(userId);
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            loanApprove.setUserId(userId);
            loanApprove.setActionTime(actionTime);
            String approveDate = dateFormat.format(actionTime);
            loanApprove.setApprovedDate(dateFormat.parse(approveDate));
            session.saveOrUpdate(loanApprove);

            approval = loanApprove.getPk();
            int appLevel = loanApprove.getApprovedLevel();
            String sql = "From LoanHeaderDetails where loanId=" + loanApprove.getLoanId();
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();
            if (appLevel == 1) {
                loan.setLoanApprovalStatus(2);
            } else if (appLevel == 2) {
                loan.setLoanApprovalStatus(3);
            } else if (appLevel == 3) {
                loan.setLoanApprovalStatus(4);
                if (loanApprove.getApprovedStatus()) {
                    loan.setLoanStatus(1);
                } else {
                    loan.setLoanStatus(2);
                }
            }
            session.update(loan);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return approval;
    }

    @Override
    public int saveLoanCheckList(int listId, int loanId, String path, String description) {
        int saved = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            int userId = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();

            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();

            String sql1 = "from MCheckList where id=" + listId;
            MCheckList list = (MCheckList) session.createQuery(sql1).uniqueResult();
            LoanCheckList checkList = new LoanCheckList();
            checkList.setLoanHeaderDetails(loan);
            checkList.setMCheckList(list);
            checkList.setFilePath(path);
            checkList.setDecription(description);
            checkList.setUserId(userId);
            checkList.setActionTime(actionTime);
            session.save(checkList);
            saved = checkList.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return saved;
    }

    @Override
    public int saveLoanPropertyVehicleDetails(LoanPropertyVehicleDetails formData) {
        int id = 0;
        try {
            int userId = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            formData.setActionTime(actionTime);
            formData.setUserId(userId);
            sessionFactory.getCurrentSession().saveOrUpdate(formData);
            id = formData.getVehicleId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public int saveLoanPropertyArticleDetails(LoanPropertyArticlesDetails formData) {
        int id = 0;
        try {
            int userId = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            formData.setActionTime(actionTime);
            formData.setUserId(userId);
            sessionFactory.getCurrentSession().saveOrUpdate(formData);
            id = formData.getArticleId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public int saveLoanPropertyLandDetails(LoanPropertyLandDetails formData) {
        int id = 0;
        try {
            int userId = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            formData.setActionTime(actionTime);
            formData.setUserId(userId);
            sessionFactory.getCurrentSession().saveOrUpdate(formData);
            id = formData.getLandId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public int saveLoanPropertyOtherDetails(LoanPropertyOtherDetails formData) {
        int id = 0;
        try {
            int userId = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            formData.setActionTime(actionTime);
            formData.setUserId(userId);
            sessionFactory.getCurrentSession().saveOrUpdate(formData);
            id = formData.getOtherId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public viewLoan findLoanStatus(int loanId) {
        viewLoan newLoan = new viewLoan();
        int userId = userDao.findByUserName();
        int approId = 0;
        List<UmUserTypeApproval> approval = userDao.findApproveLevelByUser(userId);
        for (int i = 0; i < approval.size(); i++) {
            UmUserTypeApproval userApp = (UmUserTypeApproval) approval.get(i);
            if (userApp.getMApproveLevels().getAppId() == 3) {
                approId = 3;
                break;
            } else if (userApp.getMApproveLevels().getAppId() == 2) {
                approId = 2;
                break;
            } else {
                approId = 1;
            }
        }

        try {
            String sql = "From LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            newLoan.setLoanApprovalStatus(loan.getLoanApprovalStatus());
            newLoan.setLoanStatus(loan.getLoanStatus());
            newLoan.setAppLevelId(approId);
            newLoan.setLoanDocumentSubmission(loan.getLoanDocumentSubmission());
            newLoan.setLoanIsIssue(loan.getLoanIsIssue());
            newLoan.setDueDate(loan.getLoanDueDate());
            newLoan.setLoanType(String.valueOf(loan.getLoanType()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newLoan;
    }

    //create Agreement NO and dueDate
    @Override
    public String createLoanAgreementNo(int loanId) {
        String agreementNo = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yy");
            String formattedYear = sdf.format(Calendar.getInstance().getTime());

            int userId = userDao.findByUserName();
            int userLogBranchId = setImpl.getUserLogedBranch(userId);
            Date systemDate = setImpl.getSystemDate(userLogBranchId);
            MSubLoanType subLoanType = null;
            int a_no = 0;
            String sql = "From LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan_header = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (loan_header.getLoanAgreementNo() == null || "".equals(loan_header.getLoanAgreementNo())) {
                LoanRescheduleDetails loanRescheduleDetails = (LoanRescheduleDetails) sessionFactory.getCurrentSession().createCriteria(LoanRescheduleDetails.class)
                        .add(Restrictions.eq("loanHeaderDetailsByChildLoan.loanId", loanId)).uniqueResult();
                if (loanRescheduleDetails == null) {
                    int loan_type = loan_header.getLoanType();
                    int branchId = loan_header.getBranchId();
                    String branchCode = "";
                    MBranch mBranch = (MBranch) sessionFactory.getCurrentSession().createCriteria(MBranch.class)
                            .add(Restrictions.eq("branchId", branchId)).uniqueResult();
                    if (mBranch != null) {
                        branchCode = mBranch.getBranchCode();
                    }
                    String code = "";
                    String sql1 = "from MSubLoanType where subLoanId=" + loan_type;
                    subLoanType = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
                    if (subLoanType != null) {
                        code = subLoanType.getSubLoanCode();
                        a_no = subLoanType.getMaxCount() + 1;
                        subLoanType.setMaxCount(a_no);
                    }
                    agreementNo = agreementNo + code + "/" + branchCode + "/" + formattedYear + "/";

                    // new agreement No
                    int lengthOfCode = 6 - String.valueOf(a_no).length();
                    for (int i = 0; i < lengthOfCode; i++) {
                        agreementNo = agreementNo + "0";
                    }
                    agreementNo = agreementNo + String.valueOf(a_no) + "/00";
                    loan_header.setLoanAgreementNo(agreementNo);
                    sessionFactory.getCurrentSession().update(subLoanType);
                } else {
                    agreementNo = "";
                    String sql2 = "From LoanHeaderDetails where loanId = " + loanRescheduleDetails.getLoanHeaderDetailsByMasterLoan().getLoanId();
                    LoanHeaderDetails masterLoan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                    if (masterLoan != null) {
                        String masterLoanAgNo = masterLoan.getLoanAgreementNo();
                        String str = masterLoanAgNo.substring(0, 17);
                        String sql3 = "from LoanRescheduleDetails where loanHeaderDetailsByMasterLoan.loanId = " + loanRescheduleDetails.getLoanHeaderDetailsByMasterLoan().getLoanId();
                        List list = sessionFactory.getCurrentSession().createQuery(sql3).list();
                        int size = list.size();
                        if (size < 10) {
                            agreementNo = str + "/0" + size;
                        } else {
                            agreementNo = str + "/" + size;
                        }
                    }
                    loan_header.setLoanAgreementNo(agreementNo);
                }
            } else {
                agreementNo = loan_header.getLoanAgreementNo();
            }
            loan_header.setLoanIsIssue(1);

            //add start date and 1st due date
            Calendar cal = Calendar.getInstance();
            cal.setTime(systemDate);
            String today = dateFormat.format(cal.getTime());
            loan_header.setLoanStartDate(dateFormat.parse(today));
            if (loan_header.getLoanDueDate() != null) {
            } else {
                Date due_date = null;
                int periodType = loan_header.getLoanPeriodType();
                if (periodType == 1 || periodType == 2) {
                    cal.add(Calendar.MONTH, 1);
                    int month = cal.get(Calendar.MONTH) + 1;
                    int year = cal.get(Calendar.YEAR);
                    int day = loan_header.getDueDay();

                    String due = year + "-" + month + "-" + day;

                    due_date = new java.sql.Date(dateFormat.parse(due).getTime());
                } else if (periodType == 3 || periodType == 4) {
                    cal.add(Calendar.DATE, 7);
                    due_date = new java.sql.Date(cal.getTime().getTime());
                }
                loan_header.setLoanDueDate(due_date);
            }
            sessionFactory.getCurrentSession().update(loan_header);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return agreementNo;
    }

    public LoanHeaderDetails searchLoanByLoanId(int loanId) {
        LoanHeaderDetails loan = null;
        try {
            String sql = "From LoanHeaderDetails where loanId=" + loanId;
            loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return loan;
    }

    @Override
    public List findOtherChargesByLoanId(int loanId) {
        List charges = null;
        List<OtherChargseModel> chargesList = new ArrayList();
        try {
            String sql = "select l.chargeRate,l.chargeAmount,m.subTaxDescription,l.chargesId,m.isPrecentage From LoanOtherCharges as l, MSubTaxCharges as m where l.loanId=" + loanId + " and l.chargesId=m.subTaxId ";
            charges = sessionFactory.getCurrentSession().createQuery(sql).list();

            if (charges.size() > 0) {
                for (int i = 0; i < charges.size(); i++) {
                    Object[] obj = (Object[]) charges.get(i);
                    OtherChargseModel other = new OtherChargseModel();
                    String description = "";
                    String rate = "";
                    int chargeId = 0;
                    double amount = 0.00;
                    int id = 0;
                    boolean isPrecentage = false;

                    if (obj[0] != null) {
                        rate = obj[0].toString();
                    }
                    if (obj[1] != null) {
                        amount = Double.parseDouble(obj[1].toString());
                    }
                    if (obj[2] != null) {
                        description = obj[2].toString();
                    }
                    if (obj[3] != null) {
                        chargeId = Integer.parseInt(obj[3].toString());
                    }
                    if (obj[4] != null) {
                        isPrecentage = Boolean.parseBoolean(obj[4].toString());
                    }
                    String sql1 = "from LoanHeaderDetails where loanId=" + loanId;
                    LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
                    int loan_type = 0;
                    if (loan != null) {
                        loan_type = loan.getLoanType();
                    }
                    String sql2 = "from MSubLoanOtherChargers as mm where mm.MSubLoanType.subLoanId=" + loan_type + " and mm.MSubTaxCharges.subTaxId=" + id;
                    MSubLoanOtherChargers msubcharg = (MSubLoanOtherChargers) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                    boolean isAdded = false;
                    if (msubcharg != null) {
                        isAdded = msubcharg.isIsAdded();
                    }
                    other.setId(i);
                    other.setRate(rate);
                    other.setAmount(amount);
                    other.setDescription(description);
                    other.setIsPrecentage(isPrecentage);
                    other.setIsAdded(isAdded);
                    other.setChargeID(chargeId);
                    chargesList.add(other);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return chargesList;
    }

    @Override
    public List findGuarantorByLoanId(int loanId) {
        List guarantors = new ArrayList();
        List<GuranterDetailsModel> guarantersList = new ArrayList<GuranterDetailsModel>();
        try {
            Session session = sessionFactory.getCurrentSession();
            String sql = "SELECT d.debtorId,d.debtorName,d.debtorNic,d.debtorPersonalAddress,d.debtorTelephoneMobile, l.gurantorOrder "
                    + "FROM DebtorHeaderDetails d,LoanGuaranteeDetails l "
                    + "WHERE l.debtorId=d.debtorId AND l.loanId=" + loanId;
            Query query = session.createQuery(sql);
            guarantors = query.list();

            for (int i = 0; i < guarantors.size(); i++) {
                Object obj[] = (Object[]) guarantors.get(i);
                GuranterDetailsModel tmpGuranter = new GuranterDetailsModel();
                String name = "";
                String nic = "";
                String address = "";
                String tp = "";
                int order = 0;

                if (obj[1] != null) {
                    name = obj[1].toString();
                }
                if (obj[2] != null) {
                    nic = obj[2].toString();
                }
                if (obj[3] != null) {
                    address = obj[3].toString();
                }
                if (obj[4] != null) {
                    tp = obj[4].toString();
                }
                if (obj[5] != null) {
                    order = Integer.parseInt(obj[5].toString());
                }

                tmpGuranter.setDebtId(Integer.parseInt(obj[0].toString()));
                tmpGuranter.setDebtName(name);
                tmpGuranter.setDebtNic(nic);
                tmpGuranter.setDebtAddress(address);
                tmpGuranter.setDebtTp(tp);
                tmpGuranter.setDebtOrder(order);

                guarantersList.add(tmpGuranter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return guarantersList;
    }

    @Override
    public LoanHeaderDetails findLoanHeaderByLoanId(int loanId) {
        LoanHeaderDetails loan = null;
        Session session= sessionFactory.openSession();
        try {
            String sql = "From LoanHeaderDetails where loanId=" + loanId;
            loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();
//            loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            if (loan.getLoanIsPaidDown() == 0) {
                double crdtAmount = 0.00;
                String sql2 = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE (transactionCodeCrdt='OTHR' OR transactionCodeCrdt='DWN' OR transactionCodeCrdt='DWNS') AND loanId=" + loanId;
                Object obj2 = session.createQuery(sql2).uniqueResult();
//                Object obj2 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();//Annr
                if (obj2 != null) {
                    crdtAmount = Double.parseDouble(obj2.toString());
                }
                double adj_dbt = 0.00;
                String sql6 = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE (transactionCodeDbt='OTHR' OR transactionCodeDbt='DWN' OR transactionCodeDbt='DWNS') AND transactionNoDbt LIKE 'ADJ%' AND loanId=" + loanId;
                Object obj6 = session.createQuery(sql6).uniqueResult();
//                Object obj6 = sessionFactory.getCurrentSession().createQuery(sql6).uniqueResult();//Annr
                if (obj6 != null) {
                    adj_dbt = Double.parseDouble(obj6.toString());
                }
                double ini_down = loan.getLoanDownPayment();
                double ini_other = findOtherChargeSum(loanId);
                double totalDebt = ini_down + ini_other + adj_dbt;
                double totalCrd = crdtAmount;
                if (totalDebt - totalCrd <= 0) {
                    loan.setLoanIsPaidDown(1);
                }
            }
            LoanHeaderDetails loan2 = findLoanBalance(loanId);
            loan.setVoucherNo(loan2.getVoucherNo());
            loan.setVoucherId(loan2.getVoucherId());
            loan.setPaidAmount(loan2.getPaidAmount());
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return loan;
    }

    @Override
    public List findOtherChargesByLoand(int loanId) {
        List charges = null;
        try {
            String sql = "select sub.subTaxDescription, loa.chargeRate, loa.chargeAmount From LoanOtherCharges as loa, MSubTaxCharges as sub where loa.loanId=" + loanId + " and loa.chargesId=sub.subTaxId and loa.isDownPaymentCharge=1";
            charges = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return charges;
    }

    @Override
    public PaymentDetails calculatePayments(LoanHeaderDetails loan) {
        Session session= sessionFactory.openSession();
        PaymentDetails payments = new PaymentDetails();
        DecimalFormat df = new DecimalFormat("###,###.00");
        try {
            int period = loan.getLoanPeriod();
            int periodType = loan.getLoanPeriodType();
            double rate = loan.getLoanInterestRate();

            int noOfInstallmnt = 0;
            int pp = 0;
            if (periodType == 1) {
                pp = period;
                noOfInstallmnt = period;
            } else if (periodType == 2) {
                pp = period * 12;
                noOfInstallmnt = period * 12;
            } else if (periodType == 3) {
                pp = period / 30;
                noOfInstallmnt = period / 7;
            }

            List charges = findOtherChargesByLoand(loan.getLoanId());
            double totalCharges = 0.00;
            double paidCharges = 0.00;

            String sqlD = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE transactionCodeDbt='OTHR' AND loanId=" + loan.getLoanId();
            Object objD = session.createQuery(sqlD).uniqueResult();
//            Object objD = sessionFactory.getCurrentSession().createQuery(sqlD).uniqueResult();//Annr

            String sqlC = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE transactionCodeCrdt='OTHR' AND loanId=" + loan.getLoanId();
            Object objC = session.createQuery(sqlC).uniqueResult();
//            Object objC = sessionFactory.getCurrentSession().createQuery(sqlC).uniqueResult();//Annr

            if (objD != null) {
                totalCharges = Double.parseDouble(objD.toString());
            }
            if (objC != null) {
                paidCharges = Double.parseDouble(objC.toString());
            }
            totalCharges = totalCharges - paidCharges;
            double downpayment = 0.00;
            String sql2 = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE transactionCodeDbt='DWN' AND loanId=" + loan.getLoanId();
            Object obj2 = session.createQuery(sql2).uniqueResult();
//            Object obj2 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();//annr
            if (obj2 != null) {
                downpayment = Double.parseDouble(obj2.toString());
            }

            BigDecimal bd = loan.getLoanAmount();
            double loanAmount = bd.doubleValue();
            double investment = loan.getLoanInvestment();

            double capitalizeAmount = 0.00;
            List<LoanCapitalizeDetail> capitalizeDetails = findLoanCapitalizeDetails(loan.getLoanId());
            for (LoanCapitalizeDetail capitalizeDetail : capitalizeDetails) {
                capitalizeAmount = capitalizeAmount + capitalizeDetail.getAmount();
            }
            loanAmount = loanAmount - capitalizeAmount;
            double amountWithCharges = investment - totalCharges;
            Double totalInterest = getTotalInterestAmount(loan.getLoanId());
            //String sql = "from LoanInstallment where loanId ="+loan.getLoanId()+"order by insId asc";


            LoanInstallment loanInstallment = (LoanInstallment) session.createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loan.getLoanId())).addOrder(Order.asc("insId")).setMaxResults(1).uniqueResult();

//            LoanInstallment loanInstallment = (LoanInstallment) sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
//                    .add(Restrictions.eq("loanId", loan.getLoanId())).addOrder(Order.asc("insId")).setMaxResults(1).uniqueResult();//Annr

            payments.setO_charges(totalCharges);
            payments.setLoanId(loan.getLoanId());
            payments.setLoanAmount(df.format(loanAmount));
            if (capitalizeAmount > 0) {
                payments.setCapitalizeAmount(df.format(capitalizeAmount));
            } else {
                payments.setCapitalizeAmount("" + capitalizeAmount);
            }
            payments.setInvestment(df.format(investment));
            if (downpayment > 0) {
                payments.setDownPayment(df.format(downpayment));
            } else {
                payments.setDownPayment("" + downpayment);
            }
            payments.setOthercharges(df.format(totalCharges));
            payments.setAmountWithCharges(df.format(amountWithCharges));
            payments.setInterest(df.format(loanInstallment.getInsInterest()));
            payments.setTotalInterest(df.format(totalInterest));
            payments.setRoundInstallment(df.format(loanInstallment.getInsRoundAmount()));
            payments.setInstallment(df.format(loanInstallment.getInsAmount()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return payments;
    }

    @Override
    public int saveChekDetails(SettlementChequeDetails formData) {
        int userId = userDao.findByUserName();
        int checkId = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            formData.setActionTime(actionTime);
            formData.setUserId(userId);
            session.save(formData);
            checkId = formData.getCheqId();
//          boolean msg = printCheckPayment(checkId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tx.commit();
        session.flush();
        session.close();
        return checkId;
    }

    public boolean printCheckPayment(int checkId) {
        Map<String, Object> params = new HashMap<String, Object>();

        boolean msg = false;

        try {
            DBFacade db = new DBFacade();
            Connection c = (Connection) db.connect();
            File reportJasper = null;
            ReportManager report = new ReportManager();
            URL url = this.getClass().getClassLoader().getResource("..//reports/voucher/Cheque.jasper");

            reportJasper = new File(url.getFile());
            params.put("REPORT_CONNECTION", c);
            String path = reportJasper.getPath();
            System.out.println("path" + path);
//            String path = ("/reports/voucher/") + "/";
//            params.put("SUBREPORT_DIR", path);
            params.put("Cheq_Id", String.valueOf(checkId));
            msg = report.generateReport(reportJasper, params, "check", c);

            c.close();
            c = null;
            reportJasper = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public int saveVoucher(int paymentType, String amount, int loanId) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        int voucherId = 0;

        String sql2 = "from LoanHeaderDetails where loanId=" + loanId;
        LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();
        double investment = loan.getLoanInvestment();
        double paidAmount = 0.00;
        double downPayment = loan.getLoanDownPayment();

        int user_id = userDao.findByUserName();
        int userLogBranchId = setImpl.getUserLogedBranch(user_id);
        Date dt = setImpl.getSystemDate(userLogBranchId);

        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();

        String newAmount = amount.replaceAll(",", "");
        double loanAmount = Double.parseDouble(newAmount);

        try {

            Date voucheDate = dateFormat.parse(dateFormat.format(action_time));
            if (downPayment > 0.00) {
                if (loan.getLoanIsPaidDown() == 0) {
                    //create payment voucher for downpayment
                    loanAmount = loanAmount - loan.getLoanDownPayment();
                    String voucherCode = createVoucherCode(paymentType);
                    SettlementVoucher voucher = new SettlementVoucher();
                    voucher.setLoanId(loanId);
                    voucher.setVoucherType(paymentType);
                    voucher.setAmount(loanAmount);
                    voucher.setVoucherDate(dt);
                    voucher.setVoucherNo(voucherCode);
                    voucher.setActionTime(action_time);
                    voucher.setUserId(user_id);
                    session.save(voucher);
                }
            }

            SettlementVoucher voucher = new SettlementVoucher();
            String voucherCode = createVoucherCode(paymentType);
            voucher.setLoanId(loanId);
            voucher.setVoucherType(paymentType);
            voucher.setAmount(loanAmount);
            voucher.setVoucherDate(dt);
            voucher.setVoucherNo(voucherCode);
            voucher.setActionTime(action_time);
            voucher.setUserId(user_id);
            session.save(voucher);

            voucherId = voucher.getId();
            if (voucherId > 0) {
                String sql3 = "select sum(amount) as paidSum from SettlementVoucher where loanId=" + loanId + " group by loanId";
                Object obj2 = sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                if (obj2 != null) {
                    paidAmount = Double.parseDouble(obj2.toString());
                }
                if (investment == paidAmount) {
                    loan.setLoanIsIssue(3);
                    session.update(loan);
                }
                //loan.setLoanPaymentType(paymentType);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return voucherId;
    }

    //create voucher no
    public String createVoucherCode(int paymentType) {
        String code = "", voucherCode = "";
        if (paymentType == 1) {
            //Check Payment
            code = "V";
        } else if (paymentType == 2) {
            //Cash Payment
            code = "V";
        } else if (paymentType == 3) {
            //Cash Payment
            code = "OV";
        }
        String sql = "SELECT MAX(voucherNo) AS maxx FROM SettlementVoucher WHERE voucherNo LIKE '" + code + "%'";
        Object mx = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        String maxNo = "";
        if (mx != null) {
            maxNo = mx.toString();
        }
        int v_no = 1;
        if (maxNo != null && maxNo != "") {
            String maxNo2 = maxNo.substring(3).replaceAll("^0+", "");
            System.out.println("maxNo2" + maxNo2);
            v_no = Integer.parseInt(maxNo2) + 1;
        }
        voucherCode = code;
        int lengthOfCode = 6 - String.valueOf(v_no).length();
        for (int i = 0; i < lengthOfCode; i++) {
            voucherCode = voucherCode + "0";
        }

        voucherCode = voucherCode + v_no;
        return voucherCode;
    }

    @Override
    public LoanPropertyVehicleDetails findVehicleById(int id) {
        LoanPropertyVehicleDetails vehicle = null;
        try {
            String sql = "from LoanPropertyVehicleDetails where vehicleId=" + id;
            vehicle = (LoanPropertyVehicleDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicle;
    }

    @Override
    public LoanPropertyArticlesDetails findArticleById(int id) {
        LoanPropertyArticlesDetails article = null;
        try {
            String sql = "from LoanPropertyArticlesDetails where articleId=" + id;
            article = (LoanPropertyArticlesDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return article;
    }

    @Override
    public LoanPropertyLandDetails findLandById(int id) {
        LoanPropertyLandDetails land = null;
        try {
            String sql = "from LoanPropertyLandDetails where landId=" + id;
            land = (LoanPropertyLandDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return land;
    }

    @Override
    public LoanPropertyOtherDetails findOtherById(int id) {
        LoanPropertyOtherDetails other = null;
        try {
            String sql = "from LoanPropertyOtherDetails where otherId=" + id;
            other = (LoanPropertyOtherDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return other;
    }

    @Override
    public List findLoanCheckListByLoanId(int loanId) {
        List documents = null;
        try {
            String sql = "from LoanCheckList as l where l.loanHeaderDetails.loanId=" + loanId;
            documents = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return documents;
    }

    @Override
    public File findDocumentById(int list_id) {
        String filePath = "";
        File file = null;
        try {
            String sql = "from LoanCheckList as l where l.id=" + list_id;
            LoanCheckList obj = (LoanCheckList) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                filePath = obj.getFilePath();
                file = new File(filePath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public String findDocumentNameById(int list_id) {
        String name = "";
        File file = null;
        try {
            String sql = "from LoanCheckList as l where l.id=" + list_id;
            LoanCheckList obj = (LoanCheckList) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                name = obj.getMCheckList().getListDescription();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    @Override
    public void updateDocumentSubmissionStaus(int loanId) {
        int loanTypeId = 0;
        try {
            String sql = "from LoanCheckList as l where l.loanHeaderDetails.loanId=" + loanId;
            List loanCheckList = sessionFactory.getCurrentSession().createQuery(sql).list();

            String sql1 = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (loan != null) {
                loanTypeId = loan.getLoanType();
            }
            String sql2 = "from MSubLoanChecklist as m where m.MSubLoanType.subLoanId=" + loanTypeId;
            List checkList = sessionFactory.getCurrentSession().createQuery(sql2).list();

            boolean hasDocument = false;

//            if (checkList.size() == loanCheckList.size()&&checkList.size()!=0) {
            if (checkList.size() == loanCheckList.size()&& checkList.size()!=0 && loanCheckList.size()!=0) {
                //upload all documents
                loan.setLoanDocumentSubmission(2);
                sessionFactory.getCurrentSession().update(loan);
            } else {
                for (int i = 0; i < checkList.size(); i++) {
                    MSubLoanChecklist l_check = (MSubLoanChecklist) checkList.get(i);
                    int checkId = l_check.getMCheckList().getId();
                    int isCompulsery = l_check.getIsCompulsory();
                    for (int j = 0; j < loanCheckList.size(); j++) {
                        LoanCheckList l_list = (LoanCheckList) loanCheckList.get(j);
                        int listId = l_list.getMCheckList().getId();
                        if (isCompulsery == 1) {
                            hasDocument = false;
                            if (checkId == listId) {
                                hasDocument = true;
                                break;
                            }
                        }
                    }
                }
                //upload all mandatory documents
                if (hasDocument) {
                    loan.setLoanDocumentSubmission(3);
                    sessionFactory.getCurrentSession().update(loan);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int saveRecoveryOfficerComment(LoanApprovedLevels app) {
        int id = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            int user_id = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date action_time = cal.getTime();
            app.setUserId(user_id);
            app.setActionTime(action_time);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        tx.commit();
        session.flush();
        session.close();
        return id;
    }

    @Override
    public List findApproveLevelByUser() {
        List approval = null;
        Session session=sessionFactory.openSession();
        try {
            int userId = userDao.findByUserName();
            int usertype = userDao.findUserTypeByUserId(userId);

            approval = null;
            String sql = "from UmUserTypeApproval where userId=" + userId;
            approval = session.createQuery(sql).list();
//            approval = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
//            if (approval != null) {
//                approveLevel = approval.get(0).getAppType();
//            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        session.clear();
        session.close();
        return approval;
    }

    @Override
    public LoanApprovedLevels findloanApproval(int loanId, int type) {
        LoanApprovedLevels approve = new LoanApprovedLevels();
        try {
            String sql = "from LoanApprovedLevels where loanId=" + loanId + " and approvedLevel=" + type;
            approve = (LoanApprovedLevels) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return approve;
    }

    @Override
    public LoanPendingDates findPendingDates(int loanId) {
        LoanPendingDates pending = null;
        try {
            String sql = "from LoanPendingDates where loanId=" + loanId;
            pending = (LoanPendingDates) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return pending;
    }

    @Override
    public int saveOrUpdatePendingDates(LoanPendingDates formData, int typeId) {
        int msg = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql = "from MPeriodTypes where typeId=" + typeId;
            MPeriodTypes type = (MPeriodTypes) session.createQuery(sql).uniqueResult();

            int user_id = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date action_time = cal.getTime();

            if (type.getTypeDescription().equalsIgnoreCase("month")) {
                cal.add(Calendar.MONTH, formData.getPeriod());
            } else if (type.getTypeDescription().equalsIgnoreCase("year")) {
                cal.add(Calendar.YEAR, formData.getPeriod());
            } else if (type.getTypeDescription().equalsIgnoreCase("days")) {
                cal.add(Calendar.DATE, formData.getPeriod());
            } else if (type.getTypeDescription().equalsIgnoreCase("weeks")) {
                cal.add(Calendar.WEEK_OF_YEAR, formData.getPeriod());
            } else {

            }

            Date endDate = cal.getTime();
            String eDate = dateFormat.format(endDate);

            formData.setUserId(user_id);
            formData.setActionTime(action_time);
            formData.setMPeriodTypes(type);
            formData.setEndDate(dateFormat.parse(eDate));
            session.saveOrUpdate(formData);

            msg = formData.getId();
            //update document submission status
            if (msg > 0) {
                String sql1 = "from LoanHeaderDetails where loanId=" + formData.getLoanId();
                LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql1).uniqueResult();
                if (loan != null) {
                    loan.setLoanDocumentSubmission(3);
                    session.saveOrUpdate(loan);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        tx.commit();
        session.flush();
        session.close();
        return msg;
    }

    @Override
    public LoanRecoveryReport findRecoveryReportById(int loanId) {
        LoanRecoveryReport recovery = null;
        try {
            String sql = "from LoanRecoveryReport as l where l.loanId=" + loanId;
            recovery = (LoanRecoveryReport) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return recovery;
    }

    @Override
    public ApprovalDetailsModel findApprovalDetails(int loanId) {
        ApprovalDetailsModel approve = new ApprovalDetailsModel();
        try {
            String sql = "from LoanHeaderDetails  where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            if (loan != null) {
                int app1_id = 0;
                if (loan.getLoanApproval1() != null) {
                    app1_id = userDao.findEmployeeByUserId(loan.getLoanApproval1());
                }
                Employee emp1 = userDao.findEmployeeById(app1_id);
                if (emp1 != null) {
                    approve.setApp_id1(app1_id);
                    approve.setApp_name1(emp1.getEmpLname());
                } else {
                    approve.setApp_id1(app1_id);
                }

                int app2_id = 0;
                if (loan.getLoanApproval2() != null) {
                    app2_id = userDao.findEmployeeByUserId(loan.getLoanApproval2());
                }
                Employee emp2 = userDao.findEmployeeById(app2_id);
                if (emp2 != null) {
                    approve.setApp_id2(app2_id);
                    approve.setApp_name2(emp2.getEmpLname());
                } else {
                    approve.setApp_id2(app2_id);
                }

                int app3_id = 0;
                if (loan.getLoanApproval3() != null) {
                    app3_id = userDao.findEmployeeByUserId(loan.getLoanApproval3());
                }
                Employee emp3 = userDao.findEmployeeById(app3_id);
                if (emp3 != null) {
                    approve.setApp_id3(app3_id);
                    approve.setApp_name3(emp3.getEmpLname());
                } else {
                    approve.setApp_id3(app3_id);
                }
                if (loan.getLoanApp1Comment() != null) {
                    approve.setComment1(loan.getLoanApp1Comment());
                } else {
                    approve.setComment1("");
                }
                if (loan.getLoanApp2Comment() != null) {
                    approve.setComment2(loan.getLoanApp2Comment());
                } else {
                    approve.setComment2("");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return approve;
    }

    @Override
    public int saveOrUpdateFieldOfficer(int loanId, int officerId) {
        int save = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql = "from LoanRecoveryReport where loanId=" + loanId;
            LoanRecoveryReport recovery = (LoanRecoveryReport) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();
            String assingDate = dateFormat.format(actionTime);
            int userId = userDao.findByUserName();

            if (recovery != null) {
                //update
                recovery.setEmpNo(officerId);
                recovery.setLoanId(loanId);
                recovery.setRecoveryStatus(1);
                recovery.setRecoveryDate(dateFormat.parse(assingDate));
                recovery.setActionTime(actionTime);
                recovery.setUserId(userId);
            } else {
                //save
                recovery = new LoanRecoveryReport();
                recovery.setEmpNo(officerId);
                recovery.setLoanId(loanId);
                recovery.setRecoveryStatus(1);
                recovery.setRecoveryDate(dateFormat.parse(assingDate));
                recovery.setActionTime(actionTime);
                recovery.setUserId(userId);
            }
            session.saveOrUpdate(recovery);
            save = recovery.getReportId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return save;
    }

    @Override
    public List findFiledOfficers(int loanId) {
        List fieldOfficerList = null;
        List<ApprovalDetailsModel> off_list = new ArrayList();
        try {
            String sql = "from LoanHeaderDetails as l where l.loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            int managerId = 0;
            if (loan.getLoanApproval2() != null) {
                managerId = loan.getLoanApproval2();
            }

            String sql2 = "select emp.empNo, emp.empFname, emp.empLname from UmUser as usr, UmFieldOfficers as off, Employee as emp "
                    + " where emp.empNo = off.empId and usr.userId=off.managerId "
                    + " and usr.isActive=1 and usr.isDelete=0 and usr.userId=" + managerId;
            fieldOfficerList = sessionFactory.getCurrentSession().createQuery(sql2).list();

            for (int i = 0; i < fieldOfficerList.size(); i++) {
                Object[] obj = (Object[]) fieldOfficerList.get(i);
                ApprovalDetailsModel app = new ApprovalDetailsModel();
                app.setApp_id1(Integer.parseInt(obj[0].toString()));
                app.setApp_name1(obj[2].toString());
                off_list.add(app);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return off_list;
    }

    //recovery part
    @Override
    public List<RecoveryReportModel> findrecoveryReport(int loanId) {
        List<RecoveryReportModel> cusDetails = new ArrayList();
        try {
            String sql = "From LoanHeaderDetails where loanId =" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            int debtorId = loan.getDebtorHeaderDetails().getDebtorId();

            String sql3 = "from LoanRecoveryReport as l where l.loanId=" + loanId;
            LoanRecoveryReport recovery = (LoanRecoveryReport) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();

            String cusComment = "", comment1 = "", comment2 = "", comment3 = "", comment4 = "";
            if (recovery != null) {
                cusComment = recovery.getCustomerComment();
                comment1 = recovery.getGuarantorComment1();
                comment2 = recovery.getGuarantorComment2();
                comment3 = recovery.getGuarantorComment3();
                comment4 = recovery.getGuarantorComment4();
            }

            RecoveryReportModel debtorData = findDebtorHistory(debtorId, loanId);
            debtorData.setTitle("Customer");
            debtorData.setComment(cusComment);
            cusDetails.add(debtorData);

            String sql2 = "from LoanGuaranteeDetails where loanId=" + loanId;
            List<LoanGuaranteeDetails> gurantors = sessionFactory.getCurrentSession().createQuery(sql2).list();

            if (gurantors.size() > 0) {
                for (int i = 0; i < gurantors.size(); i++) {
                    LoanGuaranteeDetails gurantor = (LoanGuaranteeDetails) gurantors.get(i);
                    int gId = gurantor.getDebtorId();
                    int order = gurantor.getGurantorOrder();
                    RecoveryReportModel gurantorData = findDebtorHistory(gId, loanId);
                    gurantorData.setTitle("Gurantor " + gurantor.getGurantorOrder());
                    if (order == 1) {
                        gurantorData.setComment(comment1);
                    } else if (order == 2) {
                        gurantorData.setComment(comment2);
                    } else if (order == 3) {
                        gurantorData.setComment(comment3);
                    } else {
                        gurantorData.setComment(comment4);
                    }
                    cusDetails.add(gurantorData);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cusDetails;
    }

    public RecoveryReportModel findDebtorHistory(int debID, int loanId) {
        RecoveryReportModel recovery = new RecoveryReportModel();
        try {
            String sql = "from LoanHeaderDetails where debtorHeaderDetails.debtorId=" + debID + " AND loanId<>" + loanId;
            List<LoanHeaderDetails> loanList = sessionFactory.getCurrentSession().createQuery(sql).list();
            List<viewLoan> view_loan = new ArrayList();
            if (loanList.size() > 0) {
                for (int i = 0; i < loanList.size(); i++) {
                    LoanHeaderDetails loan_h = (LoanHeaderDetails) loanList.get(i);
                    viewLoan loan = new viewLoan();
                    loan.setLoanId(loan_h.getLoanId());
                    loan.setLoanAmount(loan_h.getLoanInvestment());
                    loan.setLoanDate(loan_h.getLoanDate());
                    loan.setLoanStatus(loan_h.getLoanStatus());
                    int periodType = loan_h.getLoanPeriodType();
                    String p_type = "";
                    if (periodType == 1) {
                        p_type = "Month";
                    } else if (periodType == 2) {
                        p_type = "Year";
                    } else {
                        p_type = "Days";
                    }
                    loan.setLoanPeriod(loan_h.getLoanPeriod() + " " + p_type);

                    int loanType = loan_h.getLoanType();
                    String sql2 = "From MSubLoanType where subLoanId=" + loanType;
                    MSubLoanType subLoan = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();

                    String typeName = subLoan.getSubLoanName();
                    loan.setLoanType(typeName);
                    loan.setLoanSpec(loan_h.getLoanSpec());
                    loan.setAgreementNo(loan_h.getLoanAgreementNo());
                    view_loan.add(loan);
                }
            }

            List<viewLoan> g_loanList = new ArrayList();
            String sql0 = "select h.loanId, h.debtorHeaderDetails, h.loanAmount, h.loanPeriod, h.loanPeriodType,h.loanInterestRate, h.loanInstallment, h.loanType, h.loanAgreementNo, h.loanStatus, h.loanDate, h.loanSpec from LoanGuaranteeDetails as g, LoanHeaderDetails as h where g.loanId=h.loanId and g.debtorId = " + debID + " and g.loanId<>" + loanId;
            List guarantorList = sessionFactory.getCurrentSession().createQuery(sql0).list();
            if (guarantorList.size() > 0) {
                for (int i = 0; i < guarantorList.size(); i++) {
                    Object[] obj = (Object[]) guarantorList.get(i);
                    DebtorHeaderDetails db = (DebtorHeaderDetails) obj[1];
                    viewLoan g_loan = new viewLoan();
                    g_loan.setDebtorName(db.getNameWithInitial());
                    g_loan.setDebtorAddress(db.getDebtorPersonalAddress());
                    double amount = Double.parseDouble(obj[2].toString());
                    g_loan.setLoanAmount(amount);
                    int periodType = Integer.parseInt(obj[4].toString());
                    String p_type = "";
                    if (periodType == 1) {
                        p_type = "Month";
                    } else if (periodType == 2) {
                        p_type = "Year";
                    } else {
                        p_type = "Days";
                    }
                    String agNo = "";
                    if (obj[8] != null) {
                        agNo = obj[8].toString();
                    }
                    int loanType = Integer.parseInt(obj[7].toString());
                    String sql2 = "From MSubLoanType where subLoanId=" + loanType;
                    MSubLoanType subLoan = (MSubLoanType) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                    int loanSpec = Integer.parseInt(obj[11].toString());
                    int loanStatus = Integer.parseInt(obj[9].toString());
                    String typeName = subLoan.getSubLoanName();
                    g_loan.setLoanType(typeName);
                    g_loan.setLoanPeriod(obj[3].toString() + " " + p_type);
                    g_loan.setAgreementNo(agNo);
                    g_loan.setLoanSpec(loanSpec);
                    g_loan.setLoanStatus(loanStatus);
                    g_loanList.add(g_loan);
                }
            }
            String sql3 = "From DebtorHeaderDetails where debtorId=" + debID;
            DebtorHeaderDetails debtor1 = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
            if (debtor1 != null) {
                System.out.println("debtor not null");
            } else {
                System.out.println("debtor null");
            }
            recovery.setName(debtor1.getNameWithInitial());
            recovery.setNic(debtor1.getDebtorNic());
            if (debtor1.getDebtorPersonalAddress() != null) {
                recovery.setAddress(debtor1.getDebtorPersonalAddress());
            } else {
                recovery.setAddress("");
            }

            if (debtor1.getDebtorTelephoneMobile() != null) {
                recovery.setMobile(Integer.parseInt(debtor1.getDebtorTelephoneMobile()));
            } else {
                recovery.setMobile(0);
            }
            recovery.setCustmerId(debID);
            recovery.setLoanList(view_loan);
            recovery.setGuarantorList(g_loanList);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return recovery;
    }

    @Override
    public List findLoanListByFiledOfficer() {
        List<viewLoan> loanList = new ArrayList();
        int userId = userDao.findByUserName();
        try {
            String sql = "select loan.loanId,loan.debtorHeaderDetails,s.subLoanName, loan.loanStatus, rec.recoveryDate"
                    + " from LoanHeaderDetails as loan, UmUser as usr, LoanRecoveryReport as rec, MSubLoanType as s "
                    + " where s.subLoanId=loan.loanType and rec.loanId=loan.loanId and usr.empId=rec.empNo and usr.userId=" + userId;

            List loan_h_list = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (loan_h_list.size() > 0) {
                for (int i = 0; i < loan_h_list.size(); i++) {
                    viewLoan loan = new viewLoan();
                    Object[] obj = (Object[]) loan_h_list.get(i);
                    int loanId = Integer.parseInt(obj[0].toString());
                    String loanType = obj[2].toString();
                    int loanStatus = Integer.parseInt(obj[3].toString());
                    DebtorHeaderDetails deb = (DebtorHeaderDetails) obj[1];
                    Date r_date = (Date) obj[4];

                    String sql1 = "select d.debtorId,d.nameWithInitial,d.debtorTelephoneMobile,d.debtorPersonalAddress from DebtorHeaderDetails as d, LoanGuaranteeDetails as g where g.debtorId=d.debtorId and g.loanId=" + loanId;
                    List g_list = sessionFactory.getCurrentSession().createQuery(sql1).list();
                    List<DebtorHeaderDetails> guarantorList = new ArrayList();
                    if (g_list.size() > 0) {
                        for (int j = 0; j < g_list.size(); j++) {
                            Object[] obj2 = (Object[]) g_list.get(j);
                            DebtorHeaderDetails debtor = new DebtorHeaderDetails();
                            debtor.setDebtorId(Integer.parseInt(obj2[0].toString()));
                            if (obj2[1] != null) {
                                debtor.setNameWithInitial(obj2[1].toString());
                            } else {
                                debtor.setNameWithInitial("");
                            }
                            if (obj2[3] != null) {
                                debtor.setDebtorPersonalAddress(obj2[3].toString());
                            } else {
                                debtor.setDebtorPersonalAddress("");
                            }
                            if (obj2[2] != null) {
                                debtor.setDebtorTelephoneMobile((obj2[2].toString()));
                            }
                            guarantorList.add(debtor);
                            debtor = null;
                        }
                    }
                    loan.setLoanId(loanId);
                    loan.setLoanGuarantor(guarantorList);
                    loan.setLoanType(loanType);
                    loan.setDebtorName(deb.getNameWithInitial());
                    if (deb.getDebtorTelephoneMobile() != null && !deb.getDebtorTelephoneMobile().equals("")) {
                        loan.setDebtorTelephone(Integer.parseInt(deb.getDebtorTelephoneMobile()));
                    }
                    loan.setDebtorAddress(deb.getDebtorPersonalAddress());
                    loan.setLoanStatus(loanStatus);
                    loan.setLoanDate(r_date);
                    loanList.add(loan);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanList;
    }

    @Override
    public int saveRecoveryReport(int loanId, String[] comment) {
        int save = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        String comment1 = "", comment2 = "", comment3 = "", comment4 = "", cusComment = "";
        try {
            Calendar cal = Calendar.getInstance();
            Date today = cal.getTime();
            String dd = dateFormat.format(today);

            if (comment.length == 1) {
                cusComment = comment[0];
            } else if (comment.length == 2) {
                cusComment = comment[0];
                comment1 = comment[1];
            } else if (comment.length == 3) {
                cusComment = comment[0];
                comment1 = comment[1];
                comment2 = comment[2];
            } else if (comment.length == 4) {
                cusComment = comment[0];
                comment1 = comment[1];
                comment2 = comment[2];
                comment3 = comment[3];
            } else if (comment.length == 5) {
                cusComment = comment[0];
                comment1 = comment[1];
                comment2 = comment[2];
                comment3 = comment[3];
                comment4 = comment[4];
            }
            String sql = "from LoanRecoveryReport where loanId=" + loanId;
            LoanRecoveryReport recovery = (LoanRecoveryReport) session.createQuery(sql).uniqueResult();
            recovery.setCustomerComment(cusComment);
            recovery.setGuarantorComment1(comment1);
            recovery.setGuarantorComment2(comment2);
            recovery.setGuarantorComment3(comment3);
            recovery.setGuarantorComment4(comment4);
            recovery.setRecoverySubmitDate(dateFormat.parse(dd));
            recovery.setRecoveryStatus(2);
            session.saveOrUpdate(recovery);
            save = recovery.getReportId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return save;
    }

    @Override
    public List findInvoiceDetailsByLoanId(int loanId) {
        List<InvoiseDetails> invoiceList = new ArrayList();

        try {
            String sql1 = "from LoanPropertyVehicleDetails where loanId=" + loanId;
            List vehicleList = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (vehicleList.size() > 0) {
                for (int i = 0; i < vehicleList.size(); i++) {
                    LoanPropertyVehicleDetails vehicle = (LoanPropertyVehicleDetails) vehicleList.get(i);
                    String description = vehicle.getVehicleType() + " " + vehicle.getVehicleModel() + " " + vehicle.getVehicleDescription();
                    double price = vehicle.getVehiclePrice();
                    int id = vehicle.getVehicleId();
                    InvoiseDetails invoice = new InvoiseDetails();
                    invoice.setDescription(description);
                    invoice.setInvoiceValue(price);
                    invoice.setInvoiceId(id);
                    invoice.setType(1);
                    invoiceList.add(invoice);
                }
            }
            String sql2 = "from LoanPropertyLandDetails where loanId=" + loanId;
            List LandList = sessionFactory.getCurrentSession().createQuery(sql2).list();
            if (LandList.size() > 0) {
                for (int i = 0; i < LandList.size(); i++) {
                    LoanPropertyLandDetails land = (LoanPropertyLandDetails) LandList.get(i);
                    String description = land.getLandDeedNo() + " " + land.getLandPlace() + " " + land.getLandDescription();
                    double price = land.getLandPrice();
                    int id = land.getLandId();
                    InvoiseDetails invoice = new InvoiseDetails();
                    invoice.setDescription(description);
                    invoice.setInvoiceValue(price);
                    invoice.setInvoiceId(id);
                    invoice.setType(3);
                    invoiceList.add(invoice);
                }
            }
            String sql3 = "from LoanPropertyArticlesDetails where loanId=" + loanId;
            List articleList = sessionFactory.getCurrentSession().createQuery(sql3).list();
            if (articleList.size() > 0) {
                for (int i = 0; i < articleList.size(); i++) {
                    LoanPropertyArticlesDetails article = (LoanPropertyArticlesDetails) articleList.get(i);
                    String description = article.getArticleType() + " " + article.getArticleCarrateRate() + " " + article.getArticleIdentification();
                    double price = article.getArticlePrice();
                    int id = article.getArticleId();
                    String w1 = article.getArticleWeight1();
                    String w2 = article.getArticleWeight2();
                    String cRate = article.getArticleCarrateRate();
                    InvoiseDetails invoice = new InvoiseDetails();
                    invoice.setDescription(description);
                    invoice.setInvoiceValue(price);
                    invoice.setInvoiceId(id);
                    invoice.setType(2);
                    invoice.setArticleWeight1(w1);
                    invoice.setArticleWeight2(w2);
                    invoice.setArticleCarrateRate(cRate);
                    invoiceList.add(invoice);
                }
            }

            String sql4 = "from LoanPropertyOtherDetails where loanId=" + loanId;
            List otherList = sessionFactory.getCurrentSession().createQuery(sql4).list();
            if (otherList.size() > 0) {
                for (int i = 0; i < otherList.size(); i++) {
                    LoanPropertyOtherDetails other = (LoanPropertyOtherDetails) otherList.get(i);
                    String description = other.getOtherDescription() + " ";
                    double price = other.getOtherPrice();
                    int id = other.getOtherId();
                    InvoiseDetails invoice = new InvoiseDetails();
                    invoice.setDescription(description);
                    invoice.setInvoiceValue(price);
                    invoice.setInvoiceId(id);
                    invoice.setType(4);
                    invoiceList.add(invoice);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return invoiceList;
    }

    @Override
    public void setLoanIsIssueStatus(int loanId) {
        try {
            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            loan.setLoanIsIssue(1);
            sessionFactory.getCurrentSession().update(loan);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public LoanHeaderDetails findLoanBalance(int loanId) {
        Session session=sessionFactory.openSession();
//        SettlementVoucher voucher = new SettlementVoucher();
        LoanHeaderDetails loan = new LoanHeaderDetails();
        double paidAmountByCash = 0.00;
        double paidAmountByCheq = 0.00;
        double paidAmount = 0.00;
        String voucherNo = "";
        int voucherId = 0;
        try {
            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();
//            loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr

            String sql1 = "from SettlementVoucher where loanId=" + loanId;
            List vouchList = session.createQuery(sql1).list();
//            List vouchList = sessionFactory.getCurrentSession().createQuery(sql1).list();//Annr
            SettlementVoucher vouch = null;
            if (vouchList.size() > 0) {
                vouch = (SettlementVoucher) vouchList.get(0);
            }

            String sql2 = "select sum(cash.amount) as paidSum, voucher.voucherNo, voucher.id  from SettlementVoucher as voucher,SettlementPaymentDetailCash as cash  "
                    + " where voucher.voucherNo=cash.transactionNo and voucher.loanId=" + loanId + " and voucher.voucherType=1";
            Object[] obj2 = (Object[]) session.createQuery(sql2).uniqueResult();
//            Object[] obj2 = (Object[]) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();//Annr
            if (obj2.length > 0) {
                if (obj2[0] != null) {
                    paidAmountByCash = Double.parseDouble(obj2[0].toString());
                }
                if (obj2[1] != null) {
                    voucherNo = obj2[1].toString();
                }
                if (obj2[2] != null) {
                    voucherId = Integer.parseInt(obj2[2].toString());
                }
            }
            String sql3 = "select sum(cheq.amount) as paidSum, voucher.voucherNo, voucher.id  from SettlementVoucher as voucher,SettlementPaymentDetailCheque as cheq  "
                    + " where voucher.voucherNo=cheq.transactionNo and voucher.loanId=" + loanId + " and voucher.voucherType=1";
            Object[] obj3 = (Object[]) session.createQuery(sql3).uniqueResult();
//            Object[] obj3 = (Object[]) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();//Annr
            if (obj3[0] != null) {
                paidAmountByCheq = Double.parseDouble(obj3[0].toString());
            }
            if (obj3[1] != null) {
                voucherNo = obj3[1].toString();
            }
            if (obj3[2] != null) {
                voucherId = Integer.parseInt(obj3[2].toString());
            }
            String sql4 = "";
            double paidAmountByRecipt = 0.00;
            if (vouch != null) {
                sql4 = "SELECT SUM(crdtAmount) AS paidAmount FROM SettlementMain WHERE (transactionCodeCrdt='OTHR' OR transactionCodeCrdt='DWN') AND transactionNoCrdt NOT LIKE '" + vouch.getVoucherNo() + "' AND loanId=" + loanId;
            } else {
                sql4 = "SELECT SUM(crdtAmount) AS paidAmount FROM SettlementMain WHERE (transactionCodeCrdt='OTHR' OR transactionCodeCrdt='DWN') AND loanId=" + loanId;
            }
            Object obj4 = session.createQuery(sql4).uniqueResult();
//            Object obj4 = sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();//Annr
            if (obj4 != null) {
                paidAmountByRecipt = Double.parseDouble(obj4.toString());
            }

            paidAmount = paidAmountByCash + paidAmountByCheq;
            loan.setPaidAmount(paidAmount);
            loan.setVoucherNo(voucherNo);
            loan.setVoucherId(voucherId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return loan;
    }

    @Override
    public List findArticleDetailsById(int loanId) {
        List articleList = new ArrayList();
        try {
            String sql3 = "from LoanPropertyArticlesDetails where loanId=" + loanId;
            articleList = sessionFactory.getCurrentSession().createQuery(sql3).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return articleList;
    }

    @Override
    public int findLoanTypeBySubType(int loanType) {
        int mainTypeId = 0;
        try {
            String sql = "select loanTypeId from MSubLoanType where subLoanId = " + loanType;
            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (ob != null) {
                mainTypeId = Integer.parseInt(ob.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainTypeId;
    }

    //Save Installment
    @Override
    public void saveLoanInstallment(int loanId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<LoanInstallment> installments = new ArrayList<>();
        try {
            int userId = userDao.findByUserName();
            int userLogBranchId = setImpl.getUserLogedBranch(userId);
            Date systemDate = setImpl.getSystemDate(userLogBranchId);
            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();

            int period = loan.getLoanPeriod();
            int periodType = loan.getLoanPeriodType();

            double investment = loan.getLoanInvestment();
            double interest = loan.getLoanInterest();
            Double installment = loan.getLoanInstallment();
            Double totalInterest = 0.00;
            int noOfInstallmnt = 0;
            switch (periodType) {
                case 1:
                    noOfInstallmnt = period;
                    totalInterest = interest * noOfInstallmnt;
                    break;
                case 2:
                    noOfInstallmnt = period * 12;
                    totalInterest = interest * noOfInstallmnt;
                    break;
                case 3:
                    /*  Double pp = (double) period / 30;
                    double dailyInt = (interest * pp) / period;
                    interest = df.parse(df.format(dailyInt * 7)).doubleValue();
                    installment = df.parse(df.format(installment * 7)).doubleValue();
                    totalInterest = loan.getLoanInterest() * pp;
                    int module = period % 7;
                    if (module > 0) {
                        noOfInstallmnt = (period / 7) + 1;
                    } else {
                        noOfInstallmnt = period / 7;
                    }*/

                    if (loan.getLoanType() == 23) {
                        noOfInstallmnt = period;
                        totalInterest = interest * noOfInstallmnt;
                    } else {
                        Double pp = (double) period / 30;
                        double dailyInt = (interest * pp) / period;
                        interest = df.parse(df.format(dailyInt * 7)).doubleValue();
                        installment = df.parse(df.format(installment * 7)).doubleValue();
                        totalInterest = loan.getLoanInterest() * pp;
                        int module = period % 7;
                        if (module > 0) {
                            noOfInstallmnt = (period / 7) + 1;
                        } else {
                            noOfInstallmnt = period / 7;
                        }
                    }
                    break;
                case 4:
                    noOfInstallmnt = period;
                    totalInterest = interest * noOfInstallmnt;
                    break;
                default:
                    break;
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.setTime(systemDate);
            Date actionTime = cal.getTime();
            Calendar cal2 = Calendar.getInstance();

            if (loan.getLoanDueDate() != null) {
                cal2.setTime(loan.getLoanDueDate());
            } else {
                int month = cal.get(Calendar.MONTH) + 1;
                int year = cal.get(Calendar.YEAR);
                int day = loan.getDueDay();
                String due = year + "-" + month + "-" + day;
                Date due_date = new java.sql.Date(sdf.parse(due).getTime());
                cal2.setTime(due_date);
                if (periodType == 3 || periodType == 4) {
                    cal2.add(Calendar.DATE, 7);
                } else {
                    cal2.add(Calendar.MONTH, 1);
                }
            }
            String paymentDate;

            List<LoanInstalmentSchedule> schedule = sessionFactory.getCurrentSession().createCriteria(LoanInstalmentSchedule.class)
                    .add(Restrictions.eq("loanId", loanId)).list();

            if (schedule.isEmpty()) {
                //withOut Instalment Schedule
                //fix interest Calculation
                if ("FR".equals(loan.getLoanRateCode())) {
                    Double balance = (investment + totalInterest) - installment;
                    for (int i = 1; i <= noOfInstallmnt; i++) {
                        LoanInstallment instl = new LoanInstallment();
                        paymentDate = sdf.format(cal2.getTime());
                        instl.setInsInterest(interest);
                        instl.setInsAmount(installment);
                        instl.setInsRoundAmount(installment);
                        instl.setInsPrinciple(installment - interest);
//                        instl.setInsRoundAmount((Double) (Math.ceil(instl.getInsAmount() / 5d) * 5));
                        instl.setLoanId(loan.getLoanId());
                        instl.setInsDescription("Installment");
                        instl.setInsStatus(0);
                        instl.setLoanBalance(balance);
                        instl.setDueDate(sdf.parse(paymentDate));
                        instl.setActionTime(actionTime);
                        instl.setUserId(userId);
                        instl.setIsArrears(1);
                        session.save(instl);
                        installments.add(instl);
                        instl = null;
                        balance = balance - installment;

                        if (periodType == 3 || periodType == 4) {
                            cal2.add(Calendar.DATE, 7);
                        } else {
                            cal2.add(Calendar.MONTH, 1);
                        }
                    }
                    balanceToInstalmet(installments, investment, totalInterest);

//                    if (loan.getLoanType() != 0) {
//                        balanceToInstalmet(installments, investment, totalInterest);
//                    }
                    //+++++++++++++Reducing Rate Calculation++++++++++++++++++//
                } else {
                    //Double roundedValue = (Double) (Math.ceil(installment / 5d) * 5);
                    Double roundedValue = (Double) Math.ceil(installment);
                    Double loanIntRate = loan.getLoanInterestRate() / 1200;
                    Double balance = investment;
                    for (int i = 1; i <= noOfInstallmnt; i++) {
                        Double intrs = balance * loanIntRate;
                        Double principle = installment - intrs;
                        LoanInstallment instl = new LoanInstallment();
                        paymentDate = sdf.format(cal2.getTime());
                        instl.setInsPrinciple(principle);
                        instl.setInsInterest(intrs);
                        instl.setInsAmount(installment);
//                        instl.setInsRoundAmount(roundedValue);
                        instl.setInsRoundAmount(installment);
                        balance = balance - principle;
                        if (balance >= 1) {
                            instl.setLoanBalance(balance);
                        } else {
                            instl.setLoanBalance(0.00);
                        }
                        instl.setLoanId(loan.getLoanId());
                        instl.setInsDescription("Installment");
                        instl.setInsStatus(0);
                        instl.setDueDate(sdf.parse(paymentDate));
                        instl.setActionTime(actionTime);
                        instl.setUserId(userId);
                        instl.setIsArrears(1);
                        session.saveOrUpdate(instl);
                        instl = null;

                        if (periodType == 3 || periodType == 4) {
                            cal2.add(Calendar.DATE, 7);
                        } else {
                            cal2.add(Calendar.MONTH, 1);
                        }
                    }
                    // balanceToInstalmet(installments, investment, totalInterest);
                }
            } else //with Instalment Schedule
                if ("RR".equals(loan.getLoanRateCode())) {
                    for (LoanInstalmentSchedule lis : schedule) {
                        LoanInstallment loanInstallment = new LoanInstallment();
                        loanInstallment.setLoanId(lis.getLoanId());
                        loanInstallment.setInsInterest(lis.getMonthlyInterest());
                        loanInstallment.setInsPrinciple(lis.getMonthlyInstallmentPrincipal());
                        loanInstallment.setInsAmount(lis.getMonthlyInstallment());
                        loanInstallment.setInsRoundAmount(Math.ceil(lis.getMonthlyInstallment()));
                        loanInstallment.setLoanBalance(lis.getPrincipalAmount());
                        paymentDate = sdf.format(cal2.getTime());
                        loanInstallment.setDueDate(sdf.parse(paymentDate));
                        loanInstallment.setInsDescription("Installment");
                        loanInstallment.setInsStatus(0);
                        loanInstallment.setIsPay(0);
                        loanInstallment.setIsArrears(1);
                        loanInstallment.setActionTime(actionTime);
                        loanInstallment.setUserId(userId);
                        session.save(loanInstallment);
                        loanInstallment = null;
                        if (periodType == 3 || periodType == 4) {
                            cal2.add(Calendar.DATE, 7);
                        } else {
                            cal2.add(Calendar.MONTH, 1);
                        }
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.flush();
        tx.commit();
        // session.flush();
        session.close();
    }

    @Override
    public List<LoanInstallment> findScheduleList(int loanId) {
        LoanHeaderDetails loan = null;
        List<LoanInstallment> installmentsList = new ArrayList<>();
        try {
            loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            double investment = loan.getLoanInvestment();

            List<LoanInstalmentSchedule> schedule = sessionFactory.getCurrentSession().createCriteria(LoanInstalmentSchedule.class)
                    .add(Restrictions.eq("loanId", loanId)).list();
            if (!schedule.isEmpty()) {
                if ("RR".equals(loan.getLoanRateCode())) {
                    //redusing calculation
                    Double loanIntRate = loan.getLoanInterestRate() / 1200;
                    Double loanAmount = investment;
                    Double totalInsterest = calTotalInsterest(schedule, loanAmount, loanIntRate);

                    Double monthlyInstalment = calMonthlyInstalment(schedule, loanAmount, totalInsterest);
                    Double toatalInt = processMonthlyInstalment(schedule, df.parse(df.format(monthlyInstalment)).doubleValue(), loanAmount, loanIntRate);
                    Double monthlyInstalmentnew = calMonthlyInstalment(schedule, loanAmount, toatalInt);
                    finalMonthlyInstalmentProcess(schedule, df.parse(df.format(monthlyInstalmentnew)).doubleValue(), loanAmount);

                    LoanInstalmentSchedule sch = (LoanInstalmentSchedule) schedule.get(schedule.size() - 1);
                    Double oldBalance = sch.getPrincipalAmount();
                    Double newCapital = sch.getMonthlyInstallmentPrincipal() + oldBalance;
                    Double newInterest = sch.getMonthlyInstallment() - newCapital;
                    sch.setMonthlyInterest(df.parse(df.format(newInterest)).doubleValue());
                    sch.setMonthlyInstallmentPrincipal(df.parse(df.format(newCapital)).doubleValue());
                    sch.setPrincipalAmount(0.00);
                    sessionFactory.getCurrentSession().update(sch);
                    for (int i = 0; i < schedule.size(); i++) {
                        LoanInstalmentSchedule lis = schedule.get(i);
                        LoanInstallment loanInstallment = new LoanInstallment();
                        loanInstallment.setInsId(lis.getInsMonth());
                        loanInstallment.setInsInterest(lis.getMonthlyInterest());
                        loanInstallment.setInsPrinciple(lis.getMonthlyInstallmentPrincipal());
                        loanInstallment.setInsAmount(lis.getMonthlyInstallment());
                        loanInstallment.setInsRoundAmount((Double) (Math.ceil(lis.getMonthlyInstallment())));
                        loanInstallment.setLoanBalance(lis.getPrincipalAmount());
                        installmentsList.add(loanInstallment);
                        loanInstallment = null;
                    }
                }
            }
        } catch (Exception e) {
            return null;
        }
        return installmentsList;
    }

    //settlement part
    public void addLoanToSettlement(int loanId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {

            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();

            int userId = userDao.findByUserName();
            int branchId = setImpl.getUserLogedBranch(userId);

            Date systemDate = null;
            String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (obj != null) {
                systemDate = obj;
            }

            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();

            String sql2 = "from LoanOtherCharges where loanId=" + loanId + " and isDownPaymentCharge=1";
            List otherCharges = session.createQuery(sql2).list();

            DebtorHeaderDetails debtor = loan.getDebtorHeaderDetails();

            //set debtor debit data            
            SettlementMain sett = new SettlementMain();
            sett.setDebtorId(debtor.getDebtorId());
            sett.setLoanId(loanId);
            sett.setTransactionCodeDbt("INV");
            sett.setTransactionNoDbt("LOAN" + loanId);
            sett.setDescription("Investment");
            sett.setDbtAmount(loan.getLoanInvestment());
            sett.setActionDate(actionTime);
            sett.setSystemDate(systemDate);
            sett.setUserId(userId);
            session.save(sett);

            //posting 
            //create reference no
            Posting post = new Posting();
            post.setCurrDateTime(actionTime);
            post.setUserId(userId);
            session.save(post);

            //save down payment
            if (loan.getLoanDownPayment() > 0) {
                SettlementMain downpayment = new SettlementMain();
                downpayment.setDebtorId(debtor.getDebtorId());
                downpayment.setLoanId(loanId);
                downpayment.setTransactionCodeDbt("DWN");
                downpayment.setTransactionNoDbt("" + loanId);
                downpayment.setDescription("Down Payment");
                downpayment.setDbtAmount(loan.getLoanDownPayment());
                downpayment.setActionDate(actionTime);
                downpayment.setSystemDate(systemDate);
                downpayment.setUserId(userId);
                session.save(downpayment);
            }

            if (otherCharges.size() > 0) {
                for (int i = 0; i < otherCharges.size(); i++) {
                    LoanOtherCharges other = (LoanOtherCharges) otherCharges.get(i);
//                    String accNo = "";
//                    String sql3 = "select accountNo from ConfigChartofaccountCharge where chargeId=" + other.getChargesId() + " and subLoanType=" + loan.getLoanType();
//                    Object obj = session.createQuery(sql3).uniqueResult();
//                    if (obj != null) {
//                        accNo = obj.toString();
//                    }

                    SettlementMain sm = new SettlementMain();
                    sm.setDebtorId(debtor.getDebtorId());
                    sm.setLoanId(loanId);
                    sm.setTransactionCodeDbt("OTHR");
                    sm.setChargeId(other.getChargesId());
                    sm.setTransactionNoDbt("" + loanId);
                    sm.setDescription("other");
                    sm.setDbtAmount(other.getActualAmount());
                    sm.setSystemDate(systemDate);
                    sm.setActionDate(actionTime);
                    sm.setUserId(userId);

                    session.save(sm);

                    //generate reference no
//                    String referenceNO = "";
//                    String datePart = "";
//                    String c_date = dateFormat.format(actionTime);
//                    String splitDate[] = c_date.split("-");
//                    if (splitDate.length > 0) {
//                        for (int n = 0; n < splitDate.length; n++) {
//                            datePart = datePart + splitDate[n];
//                        }
//                    }
//                    referenceNO = referenceNO + datePart + "D" + debtor.getDebtorId() + "L" + loan.getLoanId() + "-" + other.getChargesId();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            tx.commit();
            session.flush();
            session.close();
        }
    }

    public void posting(int loanId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {

            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();

            int userId = userDao.findByUserName();

            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();

            String sql2 = "from LoanOtherCharges where loanId=" + loanId + " and isDownPaymentCharge=1";
            List otherCharges = session.createQuery(sql2).list();

            DebtorHeaderDetails debtor = loan.getDebtorHeaderDetails();

            //set debtor debit data            
            Posting post = new Posting();
            post.setCurrDateTime(actionTime);
            post.setUserId(userId);
            session.save(post);

            //save down payment
            if (loan.getLoanDownPayment() > 0) {
                SettlementMain downpayment = new SettlementMain();
                downpayment.setDebtorId(debtor.getDebtorId());
                downpayment.setLoanId(loanId);
                downpayment.setTransactionCodeDbt(debtor.getDebtorAccountNo());
                downpayment.setTransactionNoDbt("4");
                downpayment.setDescription("Down Payment");
                downpayment.setDbtAmount(loan.getLoanDownPayment());
                downpayment.setActionDate(actionTime);
                downpayment.setUserId(userId);
                session.save(downpayment);
            }

            if (otherCharges.size() > 0) {
                for (int i = 0; i < otherCharges.size(); i++) {
                    LoanOtherCharges other = (LoanOtherCharges) otherCharges.get(i);
//                    String accNo = "";
//                    String sql3 = "select accountNo from ConfigChartofaccountCharge where chargeId=" + other.getChargesId() + " and subLoanType=" + loan.getLoanType();
//                    Object obj = session.createQuery(sql3).uniqueResult();
//                    if (obj != null) {
//                        accNo = obj.toString();
//                    }

                    SettlementMain sm = new SettlementMain();
                    sm.setDebtorId(debtor.getDebtorId());
                    sm.setLoanId(loanId);
                    sm.setTransactionCodeDbt(debtor.getDebtorAccountNo());
                    sm.setChargeId(other.getChargesId());
                    sm.setTransactionNoDbt("9");
                    sm.setDescription("other");
                    sm.setDbtAmount(other.getActualAmount());
                    sm.setActionDate(actionTime);
                    sm.setUserId(userId);

                    session.save(sm);

                    //generate reference no
//                    String referenceNO = "";
//                    String datePart = "";
//                    String c_date = dateFormat.format(actionTime);
//                    String splitDate[] = c_date.split("-");
//                    if (splitDate.length > 0) {
//                        for (int n = 0; n < splitDate.length; n++) {
//                            datePart = datePart + splitDate[n];
//                        }
//                    }
//                    referenceNO = referenceNO + datePart + "D" + debtor.getDebtorId() + "L" + loan.getLoanId() + "-" + other.getChargesId();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            tx.commit();
            session.flush();
            session.close();
        }
    }

    @Override
    public String findLoanPeriodType(Integer loanPeriodTypeId) {
        String loanPeriodType = null;
        try {
            String sql = "from MPeriodTypes where typeId=" + loanPeriodTypeId;
            MPeriodTypes loanPeriodTypes = (MPeriodTypes) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (loanPeriodTypes != null) {
                loanPeriodType = loanPeriodTypes.getTypeDescription();
            } else {
                loanPeriodType = "";
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return loanPeriodType;
    }

    @Override
    public int[] createNewVoucher(PaymentVoucher formData) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        int voucherId = 0;
        int[] retValues = new int[2];
        int branchId = formData.getBranchId();
        SettlementVoucher voucher = new SettlementVoucher();
        boolean update = false;
        int user_id = userDao.findByUserName();

        Date systemDate = null;
        String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
        Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
        if (obj != null) {
            systemDate = obj;
        }

        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();

        String voucherCode, code = "";
        String sql = "";
        String voucher_no = "";
        String cash_ref1 = "";
        String cash_ref2 = "";
        String paybleAcc = "";
        String chequeInHandAcc = "";
        try {
            String sql4 = "from LoanHeaderDetails where loanId=" + formData.getLoanId();
            LoanHeaderDetails loan_h = (LoanHeaderDetails) session.createQuery(sql4).uniqueResult();

            paybleAcc = findLoanAccountNo(loan_h.getLoanType(), PAYBLE_ACC_CODE);
            chequeInHandAcc = findLoanAccountNo(0, CHEQUE_IN_HAND_ACC_CODE);
            String postingRefNo = generateReferencNo(loan_h.getLoanId(), branchId);
            if (loan_h != null) {
                loan_h.setCreateVoucher(3);
                session.update(loan_h);
            }

            int paymentType = formData.getPaymentType();
            if (formData.getVoucherNo() != null && !formData.getVoucherNo().equals("")) {
                //already issue a voucher
                String sql0 = "from SettlementVoucher where voucherNo='" + formData.getVoucherNo() + "'";
                SettlementVoucher vo = (SettlementVoucher) session.createQuery(sql0).uniqueResult();
                voucherId = vo.getId();
                voucher_no = formData.getVoucherNo();
                update = true;
                String sql1 = "from SettlementMain where loanId = " + loan_h.getLoanId() + "and transactionNoCrdt = '" + formData.getVoucherNo() + "'";
                SettlementMain settlementMain = (SettlementMain) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
                //ganarate posting ref No
                cash_ref2 = postingRefNo + "/" + settlementMain.getSettlementId();

                //posting 6 -- (2)
                if (paymentType == 1) {
                    if (!chequeInHandAcc.equals("")) {
                        addToPosting(chequeInHandAcc, cash_ref2, new BigDecimal(formData.getPayingAmount()), loan_h.getBranchId(), 1);
                    } else {
                        System.out.println("Can't Find Cheque In Hand Account");
                    }
                } else {
                    int bankId = formData.getBankAccount();
                    String cheqNo = formData.getChekNo();
                    cash_ref2 = cash_ref2 + "/" + cheqNo;
                    String accNo = "";
                    String sql_bnk = "from ConfigChartofaccountBank where bankId=" + bankId;
                    ConfigChartofaccountBank bankData = (ConfigChartofaccountBank) session.createQuery(sql_bnk).uniqueResult();
                    if (bankData != null) {
                        accNo = bankData.getBankAccountNo();
                    }
                    if (!accNo.equals("")) {
                        addToPosting(accNo, cash_ref2, new BigDecimal(formData.getPayingAmount()), loan_h.getBranchId(), 1);
                    }
                }
                //posting loan account(2)
                if (!paybleAcc.equals("")) {
                    addToPosting(paybleAcc, cash_ref2, new BigDecimal(formData.getPayingAmount()), loan_h.getBranchId(), 2);
                } else {
                    System.out.println("Can't Find Payble Account Account" + loan_h.getLoanId());
                }
            }
            /*else { // sahan ekanayake xxxxxxxxxxxxxxxxxxxxx
                //create new voucher           
                Date voucheDate = dateFormat.parse(dateFormat.format(action_time));

                code = "V";
                sql = "SELECT  MAX(voucherNo) AS maxx FROM SettlementVoucher WHERE voucherNo LIKE '" + code + "%'";
                Object mx = session.createQuery(sql).uniqueResult();
                String maxNo = "";
                if (mx != null) {
                    maxNo = mx.toString();
                }
                int v_no = 1;
                if (maxNo != null && maxNo != "") {
                    String maxNo2 = maxNo.substring(3).replaceAll("^0+", "");
                    System.out.println("maxNo2" + maxNo2);
                    v_no = Integer.parseInt(maxNo2) + 1;
                }
                voucherCode = code;
                int lengthOfCode = 6 - String.valueOf(v_no).length();
                for (int i = 0; i < lengthOfCode; i++) {
                    voucherCode = voucherCode + "0";
                }
                voucher.setLoanId(formData.getLoanId());
                voucher.setVoucherType(1);
                voucher.setAmount(formData.getTotalAmount());
                voucher.setVoucherDate(systemDate);
                voucherCode = voucherCode + v_no;
                voucher.setVoucherNo(voucherCode);
                voucher.setActionTime(action_time);
                voucher.setUserId(user_id);
                voucher.setBranchId(formData.getBranchId());
                session.save(voucher);
                voucherId = voucher.getId();
                voucher_no = voucher.getVoucherNo();

                DebtorHeaderDetails debtor = new DebtorHeaderDetails();
                if (loan_h != null) {
                    debtor = loan_h.getDebtorHeaderDetails();
                }

                //settlement
                double otherCharge = 0.00, downPaymnt = 0.00, amount = 0.00;

                otherCharge = findOtherChargeSum(loan_h.getLoanId());

                if (loan_h.getLoanDownPayment() != null) {
                    downPaymnt = loan_h.getLoanDownPayment();
                }

                //posting 6 --(1)
                double payingAmount = formData.getPayingAmount();

                int cash_id = addToSettlement(loan_h, "LOAN", voucherCode, loan_h.getLoanAmount().doubleValue(), 1, branchId);
                cash_ref1 = postingRefNo + "/" + cash_id;
                if (paymentType == 1) {
                    if (!chequeInHandAcc.equals("")) {
                        addToPosting(chequeInHandAcc, cash_ref1, new BigDecimal(payingAmount), loan_h.getBranchId(), 1);
                    } else {
                        System.out.println("Can't Find Cheque In Hand Account");
                    }
                } else {
                    int bankId = formData.getBankAccount();
                    String cheqNo = formData.getChekNo();
                    cash_ref1 = cash_ref1 + "/" + cheqNo;
                    String accNo = "";
                    String sql_bnk = "from ConfigChartofaccountBank where bankId=" + bankId;
                    ConfigChartofaccountBank bankData = (ConfigChartofaccountBank) session.createQuery(sql_bnk).uniqueResult();
                    if (bankData != null) {
                        accNo = bankData.getBankAccountNo();
                    }
                    addToPosting(accNo, cash_ref1, new BigDecimal(payingAmount), loan_h.getBranchId(), 1);
                }
                //posting loan account(1)
                if (!update) {
                    addToPosting(paybleAcc, cash_ref1, new BigDecimal(payingAmount - downPaymnt), loan_h.getBranchId(), 2);
                }
                if (downPaymnt > 0 && !update) {
                    if (loan_h.getLoanIsPaidDown() == 1) {
                        //has suspend                    
                        String refNo = "LOAN" + loan_h.getLoanId();
                        int dwns_id = addToSettlement(loan_h, "DWNS", refNo, downPaymnt, 2, branchId);
                    }
                    String downpymntAcc = findLoanAccountNo(loan_h.getLoanType(), DWN_ACC_CODE);
                    if (!downpymntAcc.equals("")) {
                        addToPosting(downpymntAcc, cash_ref1, new BigDecimal(downPaymnt), loan_h.getBranchId(), 2);
                    } else {
                        System.out.println("Can't Find DWN Account" + loan_h.getLoanId());
                    }
                }

                //add excess amount
                double excessMoney = findExcessAmount(formData.getLoanId());
                if (excessMoney > 0.00) {
                    String trnscode = "EXSS" + loan_h.getLoanId();
                    int excId = addToSettlement(loan_h, "LOAN", trnscode, excessMoney, 2, branchId);
                    String exc_ref = postingRefNo + "/" + excId;
                    String exssMoneyAcc = findLoanAccountNo(0, EXSS_ACC_CODE);
                    String debotorAcc = loan_h.getDebtorHeaderDetails().getDebtorAccountNo();
                    if (!exssMoneyAcc.equals("") && !debotorAcc.equals("")) {
                        addToPosting(exssMoneyAcc, exc_ref, new BigDecimal(excessMoney), loan_h.getBranchId(), 1);
                        addToPosting(debotorAcc, exc_ref, new BigDecimal(excessMoney), loan_h.getBranchId(), 2);
                    } else if (exssMoneyAcc.equals("")) {
                        System.out.println("Can't Find ExssMoney Account");
                    } else {
                        System.out.println("Can't Find Debtor Account");
                    }
                }

            }*/

            retValues[0] = voucherId;
            int saveId = 0;
            if (voucherId > 0) {
                if (paymentType == 1) {
                    //payment by cash
                    SettlementPaymentDetailCash cash = new SettlementPaymentDetailCash();
                    cash.setTransactionNo(voucher_no);
                    cash.setAmount(formData.getPayingAmount());
                    cash.setCurrencyNo(1);
                    cash.setExchangeRate(1.0);
                    cash.setUserId(user_id);
                    cash.setActionTime(action_time);
                    session.save(cash);
                    saveId = cash.getDetailId();
                } else if (paymentType == 2) {
                    // payment by cheque
                    SettlementPaymentDetailCheque cheq = new SettlementPaymentDetailCheque();
                    cheq.setTransactionNo(voucher_no);
                    cheq.setChequeNo(formData.getChekNo());
                    cheq.setAmount(formData.getPayingAmount());
                    cheq.setActionDate(action_time);
                    cheq.setUserId(user_id);
                    cheq.setTransactionDate(systemDate);
                    cheq.setRealizeDate(formData.getExpireDate());
                    cheq.setBank(formData.getBankAccount());
                    cheq.setAmountInTxt(formData.getAmountInTxt());
                    session.save(cheq);
                    saveId = cheq.getChequeId();
                }
            }
            retValues[1] = saveId;
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        session.flush();
        session.close();
        return retValues;
    }

    @Override
    public String createMyNewVoucher(int loanId, double totalAmount, String chekNo, int bankAccount, String chkDate, double payingAmount, String TxtChqAmt, int chbIsACPay) {
        //create new voucher
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();

        try {
            Date voucheDate = dateFormat.parse(dateFormat.format(action_time));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        int userId = userDao.findByUserName();
        int userLogBranchId = setImpl.getUserLogedBranch(userId);
        Date systemDate = setImpl.getSystemDate(userLogBranchId);

        Session session = sessionFactory.openSession();
        String voucherCode, code = "";
        String sql = "";
        String voucher_no = "";
        int voucherId = 0;
        SettlementVoucher voucher = new SettlementVoucher();

        String sql4 = "from LoanHeaderDetails where loanId=" + loanId;
        LoanHeaderDetails loan_h = (LoanHeaderDetails) session.createQuery(sql4).uniqueResult();
        code = "V";
        sql = "SELECT  MAX(voucherNo) AS maxx FROM SettlementVoucher WHERE voucherNo LIKE '" + code + "%'";
        Object mx = session.createQuery(sql).uniqueResult();
        String maxNo = "";
        if (mx != null) {
            maxNo = mx.toString();
        }
        int v_no = 1;
        if (maxNo != null && maxNo != "") {
            String maxNo2 = maxNo.substring(3).replaceAll("^0+", "");
            System.out.println("maxNo2" + maxNo2);
            v_no = Integer.parseInt(maxNo2) + 1;
        }
        voucherCode = code;
        int lengthOfCode = 6 - String.valueOf(v_no).length();
        for (int i = 0; i < lengthOfCode; i++) {
            voucherCode = voucherCode + "0";
        }
        voucher.setLoanId(loanId);
        voucher.setVoucherType(1);
        voucher.setAmount(totalAmount);
        voucher.setVoucherDate(systemDate);
        voucherCode = voucherCode + v_no;
        voucher.setVoucherNo(voucherCode);
        voucher.setActionTime(action_time);
        voucher.setUserId(1);
        voucher.setBranchId(userLogBranchId);
        session.save(voucher);
        voucherId = voucher.getId();
        voucher_no = voucher.getVoucherNo();

        DebtorHeaderDetails debtor = new DebtorHeaderDetails();
        if (loan_h != null) {
            debtor = loan_h.getDebtorHeaderDetails();
        }
        int paymentType = 2;
        int saveId = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date chequedate = null;

        try {
            chequedate = formatter.parse(chkDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (voucherId > 0) {
            if (paymentType == 1) {
                //payment by cash
                SettlementPaymentDetailCash cash = new SettlementPaymentDetailCash();
                cash.setTransactionNo(voucher_no);
                cash.setAmount(payingAmount);
                cash.setCurrencyNo(1);
                cash.setExchangeRate(1.0);
                cash.setUserId(userId);
                cash.setActionTime(action_time);
                session.save(cash);
                saveId = cash.getDetailId();
            } else if (paymentType == 2) {
                // payment by cheque
                SettlementPaymentDetailCheque cheq = new SettlementPaymentDetailCheque();
                cheq.setTransactionNo(voucher_no);
                cheq.setChequeNo(chekNo);
                cheq.setAmount(payingAmount);
                cheq.setActionDate(action_time);
                cheq.setUserId(userId);
                cheq.setTransactionDate(systemDate);
                cheq.setRealizeDate(chequedate);
                cheq.setBank(bankAccount);
                cheq.setAmountInTxt(TxtChqAmt);
                cheq.setChbIsACPay(chbIsACPay);
                session.save(cheq);
                saveId = cheq.getChequeId();
            }
        }

        session.flush();
        session.clear();
        session.close();
        return voucher_no;
    }

    //find loan acocunt no
    public String findLoanAccountNo(int loanType, String accType) {
        String accNo = "";
        Session session= sessionFactory.openSession();
        try {
            String sql = "select accountNo From ConfigChartofaccountLoan where subLoanType=" + loanType + " and accountType='" + accType + "'";
            Object obj = session.createQuery(sql).uniqueResult();
//            Object obj = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            if (obj != null) {
                accNo = obj.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Can't Find ChartOfAccount in ConfigChartofaccountLoan Table" + accType);
        }
        session.clear();
        session.close();
        return accNo;
    }

    //Installment Posting
    public void addToInsPosting(String accountNo, String referenceNo, BigDecimal amount, int branchId, int cd) {
        int user_id = userDao.findByUserName();
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        Date dayEndDate = null;
        try {
            MCompany company = userDao.findCompanyDetails();
            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            Date newDate = new Date(obj.getTime() + (1000 * 60 * 60 * 24));
            System.out.println("newDate --> " + newDate);
            if (obj != null) {
                dayEndDate = newDate;
            }

            if (!accountNo.equals("")) {
                BigDecimal bd; // the value you get
                double amt = amount.doubleValue(); // The double you want
                if (amt != 0) {
                    Posting posting = new Posting();
                    posting.setReferenceNo(referenceNo);
                    posting.setAccountNo(accountNo);
                    posting.setCreditDebit(cd);
                    if (cd == 1) {
                        posting.setCAmount(amount);
                        posting.setDAmount(new BigDecimal(0.00));
                    } else {
                        posting.setDAmount(amount);
                        posting.setCAmount(new BigDecimal(0.00));
                    }
                    posting.setAmount(amount);
                    posting.setUserId(user_id);
                    posting.setCurrDateTime(action_time);
                    posting.setBranchId(branchId);
                    posting.setAllocation(0);
                    posting.setPostingFrom(1);
                    posting.setPostingState(1);
                    posting.setDate(dayEndDate);
                    posting.setDescription("Posting from " + company.getComName() + " " + company.getComLname());
                    sessionFactory.getCurrentSession().save(posting);
                }

            } else {
                System.out.println("Cannot Posting");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //posting data
    public void addToPosting(String accountNo, String referenceNo, BigDecimal amount, int branchId, int cd) {
        Session session=sessionFactory.openSession();
        int user_id = userDao.findByUserName();
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        Date dayEndDate = null;
        try {
            MCompany company = userDao.findCompanyDetails();
            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) session.createQuery(sql).uniqueResult();
//            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            if (obj != null) {
                dayEndDate = obj;
            }

            if (!accountNo.equals("")) {
                BigDecimal bd; // the value you get
                double amt = amount.doubleValue(); // The double you want
                if (amt != 0) {
                    Posting posting = new Posting();
                    posting.setReferenceNo(referenceNo);
                    posting.setAccountNo(accountNo);
                    posting.setCreditDebit(cd);
                    if (cd == 1) {
                        posting.setCAmount(amount);
                        posting.setDAmount(new BigDecimal(0.00));
                    } else {
                        posting.setDAmount(amount);
                        posting.setCAmount(new BigDecimal(0.00));
                    }
                    posting.setAmount(amount);
                    posting.setUserId(user_id);
                    posting.setCurrDateTime(action_time);
                    posting.setBranchId(branchId);
                    posting.setAllocation(0);
                    posting.setPostingFrom(1);
                    posting.setPostingState(1);
                    posting.setDate(dayEndDate);
                    posting.setDescription("Posting from " + company.getComName() + " " + company.getComLname());
                    session.save(posting);
//                    sessionFactory.getCurrentSession().save(posting);Annr
                }
            } else {
                System.out.println("Cannot Post");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
    }


    //posting data
    public void addToPostingArrears(String accountNo, String referenceNo, BigDecimal amount, int branchId, int cd, Date dayEndDate) {
        Session session=sessionFactory.openSession();
        int user_id = userDao.findByUserName();
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
//        Date dayEndDate = null;
        try {
            MCompany company = userDao.findCompanyDetails();
//            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
//            Date obj = (Date) session.createQuery(sql).uniqueResult();
////            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
//            if (obj != null) {
//                dayEndDate = obj;
//            }

            if (!accountNo.equals("")) {
                BigDecimal bd; // the value you get
                double amt = amount.doubleValue(); // The double you want
                if (amt != 0) {
                    Posting posting = new Posting();
                    posting.setReferenceNo(referenceNo);
                    posting.setAccountNo(accountNo);
                    posting.setCreditDebit(cd);
                    if (cd == 1) {
                        posting.setCAmount(amount);
                        posting.setDAmount(new BigDecimal(0.00));
                    } else {
                        posting.setDAmount(amount);
                        posting.setCAmount(new BigDecimal(0.00));
                    }
                    posting.setAmount(amount);
                    posting.setUserId(user_id);
                    posting.setCurrDateTime(action_time);
                    posting.setBranchId(branchId);
                    posting.setAllocation(0);
                    posting.setPostingFrom(1);
                    posting.setPostingState(1);
                    posting.setDate(dayEndDate);
                    posting.setDescription("Posting from " + company.getComName() + " " + company.getComLname());
                    session.save(posting);
//                    sessionFactory.getCurrentSession().save(posting);Annr
                }
            } else {
                System.out.println("Cannot Post");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
    }

    //create refference no for posting
    public String generateReferencNo(int loanId, int branchId) {
        try {
            Date sysDate = setImpl.getSystemDate(branchId);
            String referenceNo = "";
            String datePart = "";
            String c_date = dateFormat.format(sysDate);
            String splitDate[] = c_date.split("-");
            if (splitDate.length > 0) {
                for (int n = 0; n < splitDate.length; n++) {
                    datePart = datePart + splitDate[n];
                }
            }
            referenceNo = referenceNo + datePart + loanId;
            return referenceNo;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    @Override

    public boolean processForPayment(int loanId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        boolean retValue = false;
        String sql = "from LoanHeaderDetails where loanId=" + loanId;
        LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();
        double downPayment = 0.00, investment = 0.00, otherChargers = 0.00;

        List otherChargesList = loanService.findOtherChargesByLoanId(loanId);//from loan other charges
        double totalOtherCharges = settlmentService.getSumOfOtherCherges(otherChargesList);

        if (loan.getLoanInvestment() != null) {

            investment = loan.getLoanInvestment() - totalOtherCharges;
        }

        setLoanCount(loanId, loan.getLoanBookNo());
        setLoanIsIssueStatus(loanId);
        String aggrementNo = createLoanAgreementNo(loanId);
        saveLoanInstallment(loanId);
        updateLoanLapsDate(loanId);
        //posting
        try {
            int user_id = userDao.findByUserName();
            int branchId = setImpl.getUserLogedBranch(user_id);

            Date systemDate = null;
            String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (obj != null) {
                systemDate = obj;
            }

            Calendar cal = Calendar.getInstance();
            Date action_time = cal.getTime();

            SettlementMain sett = new SettlementMain();
            sett.setDebtorId(loan.getDebtorHeaderDetails().getDebtorId());
            sett.setLoanId(loanId);
            sett.setTransactionCodeDbt("LOAN");
            sett.setTransactionCodeCrdt("");
            sett.setTransactionNoDbt("LOAN" + loanId);
            sett.setTransactionNoCrdt("");
            sett.setDescription("loan amount");
            sett.setDbtAmount(investment);
            sett.setCrdtAmount(0.00);
            sett.setUserId(user_id);
            sett.setActionDate(action_time);
            sett.setIsPrinted(false);
            sett.setIsPosting(true);
            sett.setBranchId(branchId);
            sett.setSystemDate(systemDate);
            session.save(sett);
            int settId = sett.getSettlementId();

            String referenceNo = generateReferencNo(loanId, branchId);
            referenceNo = referenceNo + "/" + settId;
            String debAccountNo = loan.getDebtorHeaderDetails().getDebtorAccountNo();
            String paybleAcc = findLoanAccountNo(loan.getLoanType(), PAYBLE_ACC_CODE);
            if (paybleAcc != null && !paybleAcc.equals("")) {
                if (debAccountNo != null && !debAccountNo.equals("")) {
                    addToPosting(paybleAcc, referenceNo, new BigDecimal(investment), loan.getBranchId(), 1);
                    addToPosting(debAccountNo, referenceNo, new BigDecimal(investment), loan.getBranchId(), 2);
                    retValue = true;
                } else {
                    retValue = false;
                    return retValue;
                }
            } else {
                retValue = false;
                return retValue;
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            session.flush();
            session.clear();
            session.close();
        }

        return retValue;
    }

    @Override
    public String updateLoanPaymentStatus(int loanId) {
        String isPost = "OK";
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();

            int userId = userDao.findByUserName();
            int branchId = setImpl.getUserLogedBranch(userId);

            int isIssue = loan.getLoanIsIssue();
            if (isIssue == 1) {
                double downPayment = 0.00, investment = 0.00;
                BigDecimal loanAmount = null;
                downPayment = loan.getLoanDownPayment();
                investment = loan.getLoanInvestment();
                loanAmount = loan.getLoanAmount();
                String refNo = generateReferencNo(loanId, branchId);
                String debtorAccNo = loan.getDebtorHeaderDetails().getDebtorAccountNo();

                //detor account
                if (debtorAccNo == null || debtorAccNo.equals("")) {
                    return "Create Account No for debtor " + loan.getDebtorHeaderDetails().getDebtorName();
                }

                //downpayment
                String reff = "LOAN" + loanId;
                if (downPayment > 0) {
                    int setId = addToSettlement(loan, "DWN", reff, downPayment, 2, branchId);
                    if (setId > 0) {
                        String refNo_d = "";
                        String downpymntAcc = findLoanAccountNo(loan.getLoanType(), DWN_ACC_CODE);
                        refNo_d = refNo + "/" + setId;
                        if (downpymntAcc != null && !downpymntAcc.equals("")) {
                            addToPosting(debtorAccNo, refNo_d, new BigDecimal(downPayment), loan.getBranchId(), 2);
                            addToPosting(downpymntAcc, refNo_d, new BigDecimal(downPayment), loan.getBranchId(), 1);
                        } else {
                            return "Create Account for Down Payment";
                        }
                    }
                }
                double totalInterest = getTotalInterestAmount(loan.getLoanId());
                int setId_r = addToSettlement(loan, "INTRS", reff, totalInterest, 2, branchId);
                if (setId_r > 0) {
                    String refNo_i = "";
                    String interestAcc = findLoanAccountNo(loan.getLoanType(), INTRS_ACC_CODE);
                    refNo_i = refNo + "/" + setId_r;
                    if (interestAcc != null && !interestAcc.equals("")) {
                        addToPosting(debtorAccNo, refNo_i, new BigDecimal(totalInterest), loan.getBranchId(), 2);
                        addToPosting(interestAcc, refNo_i, new BigDecimal(totalInterest), loan.getBranchId(), 1);
                    } else {
                        return "Create Account for Interest Suspend";
                    }
                }
                //other charges
                double otherCharge = findOtherChargeSum(loanId);

                if (otherCharge > 0) {
                    int setId_o = addToSettlement(loan, "OTHR", reff, otherCharge, 2, branchId);
                    if (setId_o > 0) {
                        String refNo_o = refNo + "/" + setId_o;
                        String sql3 = "SELECT su.subTaxAccountNo, oth.chargeAmount, su.commision, su.commisionAccountNo, su.subTaxDescription FROM LoanOtherCharges AS oth, MSubTaxCharges AS su WHERE oth.chargesId=su.subTaxId AND oth.loanId = " + loanId + " AND oth.isDownPaymentCharge=1";
                        List otherList = session.createQuery(sql3).list();
                        if (otherList.size() > 0) {
                            for (int i = 0; i < otherList.size(); i++) {
                                Object[] ob = (Object[]) otherList.get(i);
                                String accNo = ob[0].toString();
                                String commAccNo = "";
                                if (ob[3] != null) {
                                    commAccNo = ob[3].toString();
                                }
                                String accName = ob[4].toString();
                                double chargeAmount = Double.valueOf(ob[1].toString());
                                double chargeComm = 0.00;
                                if (ob[2] != null) {
                                    chargeComm = Double.valueOf(ob[2].toString());
                                }
                                if (accNo != null && !accNo.equals("") && !debtorAccNo.equals("")) {
                                    double balanceCharge = chargeAmount - chargeComm;
                                    if (chargeComm > 0.00) {
                                        if (commAccNo != null && !commAccNo.equals("")) {
                                            addToPosting(commAccNo, refNo_o, new BigDecimal(chargeComm), branchId, 1);
                                        } else {
                                            return "Create Account for " + accName;
                                        }
                                    }
                                    if (balanceCharge > 0.00) {
                                        addToPosting(accNo, refNo_o, new BigDecimal(balanceCharge), branchId, 1);
                                    }
                                    if (chargeAmount > 0.00) {
                                        addToPosting(debtorAccNo, refNo_o, new BigDecimal(chargeAmount), branchId, 2);
                                    }
                                } else {
                                    return "Create Account for charge/commision/Debtor" + accName;
                                }
                            }
                        }
                    }
                }
                //insuranse
                if (loan.getInsuranceStatus() == 1) {
                    String sqli = "SELECT sett.settlementId,ins.insuPremium,ins.insuOtherCharge FROM InsuraceProcessMain AS ins, SettlementMain AS sett WHERE ins.insuCode=sett.transactionNoDbt and ins.insuLoanId=" + loanId + " and ins.renewCount=0";
                    Object[] obi = (Object[]) session.createQuery(sqli).uniqueResult();
                    if (obi != null) {
                        int settId = Integer.parseInt(obi[0].toString());
                        String refNo_insu = refNo + "/" + settId;
                        String debtorAcc = loan.getDebtorHeaderDetails().getDebtorAccountNo();
                        String acc_ins_pay = findLoanAccountNo(loan.getLoanType(), INSURANCE_P_ACC_CODE);
                        //String acc_ins_com = findLoanAccountNo(loan.getLoanType(), INSURANCE_C_ACC_CODE);
                        double insCharge = Double.parseDouble(obi[1].toString());
                        //double comCharge = Double.parseDouble(obi[2].toString());
                        double totalIns = insCharge;
                        if (!acc_ins_pay.equals("") && !debtorAcc.equals("")) {
                            if (totalIns > 0.00) {
                                addToPosting(acc_ins_pay, refNo_insu, new BigDecimal(totalIns), branchId, 1);
                                addToPosting(debtorAcc, refNo_insu, new BigDecimal(totalIns), branchId, 2);
//                                if (comCharge > 0.00) {
//                                    addToPosting(acc_ins_com, refNo_insu, new BigDecimal(comCharge), branchId, 1);
//                                }
                            }
                            String sql_se = "from SettlementMain where settlementId=" + settId;
                            SettlementMain Settl = (SettlementMain) session.createQuery(sql_se).uniqueResult();
                            Settl.setIsPosting(true);
                            session.update(Settl);
                        } else {
                            return "Create Account for Insurance Income or Insurance Commision / Debtor";
                        }
                    }
                }
                isPost = "OK";
                loan.setLoanIsIssue(2);
                session.update(loan);
            } else {
                isPost = "OK";
            }
            session.flush();
            tx.commit();
        } catch (Exception e) {
            isPost = "Error Proccesing";
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.clear();
            session.close();
        }
        return isPost;
    }

    public int addToSettlement(LoanHeaderDetails loan, String code, String ref, double amount, int cd, int branchId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        int setId = 0;
        Date dayEndDate = null;
        try {
            int user_id = userDao.findByUserName();
            Calendar cal = Calendar.getInstance();
            Date action_time = cal.getTime();

            String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) session.createQuery(sql_d).uniqueResult();
//            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();//Annr
            if (obj != null) {
                dayEndDate = obj;
            }

            SettlementMain setl = new SettlementMain();
            setl.setLoanId(loan.getLoanId());
            setl.setDebtorId(loan.getDebtorHeaderDetails().getDebtorId());
            setl.setIsPrinted(false);
            if (cd == 1) {
                setl.setCrdtAmount(amount);
                setl.setTransactionCodeCrdt(code);
                setl.setTransactionNoCrdt(ref);
                setl.setDbtAmount(0.00);
                setl.setTransactionCodeDbt("");
                setl.setTransactionNoDbt("");
            } else {
                setl.setDbtAmount(amount);
                setl.setTransactionCodeDbt(code);
                setl.setTransactionNoDbt(ref);
                setl.setCrdtAmount(0.00);
                setl.setTransactionCodeCrdt("");
                setl.setTransactionNoCrdt("");
            }
            setl.setUserId(user_id);
            setl.setActionDate(action_time);
            setl.setDescription("Initial Loan");
            setl.setBranchId(branchId);
            setl.setSystemDate(dayEndDate);
            setl.setIsPosting(true);
            session.save(setl);
            setId = setl.getSettlementId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.flush();
        tx.commit();
        session.clear();
        session.close();
        return setId;
    }

    @Override
    public double findOtherChargeSum(int loanId) {
        Session session=sessionFactory.openSession();
        double totalOther = 0.00;
        try {
            String sql2 = "SELECT SUM(chargeAmount) FROM LoanOtherCharges WHERE loanId=" + loanId + " and isDownPaymentCharge=1";
            Object obj = session.createQuery(sql2).uniqueResult();
//            Object obj = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();//Annr
            if (obj != null && !obj.toString().equals("")) {
                totalOther = Double.parseDouble(obj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return totalOther;
    }

    @Override
    public int saveAdvancePayment(SettlementPaymentDetailCheque formData) {
        int ret = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            int user_id = userDao.findByUserName();
            int branch = setImpl.getUserLogedBranch(user_id);
            Date sysDate = setImpl.getSystemDate(branch);
            Calendar cal = Calendar.getInstance();
            Date action_time = cal.getTime();

            String sql = "from LoanHeaderDetails where loanId=" + formData.getCustomerCode();
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();
            int debtorId = loan.getDebtorHeaderDetails().getDebtorId();

            formData.setUserId(user_id);
            formData.setActionDate(action_time);
            formData.setCustomerCode(debtorId);
            formData.setIsReturn(0);
            formData.setTransactionDate(sysDate);
            session.save(formData);
            ret = formData.getChequeId();
//            if (ret > 0) {
//                loan.setLoanIsPaidDown(2);
//            }
            String reffNo = generateReferencNo(loan.getLoanId(), branch);
            String sqls = "from SettlementMain where transactionCodeCrdt='LOAN' and loanId=" + loan.getLoanId();
            SettlementMain sett = (SettlementMain) session.createQuery(sqls).uniqueResult();
            int settId = 0;
            if (sett != null) {
                settId = sett.getSettlementId();
            }

            int bankId = formData.getBank();
            String cheqNo = formData.getChequeNo();
            double amount = formData.getAmount();
            reffNo = reffNo + "/" + settId + "/" + cheqNo;
//            double downPaymnt = 0.00;
//            double amount = formData.getAmount();
//            if (loan.getLoanDownPayment() != null) {
//                downPaymnt = loan.getLoanDownPayment();
//            }
//            int setId = 0;
//            String reffNo = generateReferencNo(loan.getLoanId());;
//            if (downPaymnt > 0) {
//                SettlementMain setl = new SettlementMain();
//                setl.setLoanId(loan.getLoanId());
//                setl.setDebtorId(loan.getDebtorHeaderDetails().getDebtorId());
//                setl.setIsPrinted(false);
//                setl.setCrdtAmount(downPaymnt);
//                setl.setTransactionCodeCrdt("DWN");
//                setl.setTransactionNoCrdt(formData.getTransactionNo());
//                setl.setDbtAmount(0.00);
//                setl.setTransactionCodeDbt("");
//                setl.setTransactionNoDbt("");
//                setl.setUserId(user_id);
//                setl.setActionDate(action_time);
//                setl.setDescription("Initial Down Payment");
//                setl.setBranchId(branch);
//                setl.setSystemDate(dayEndDate);
//                setl.setIsPosting(true);
//                session.save(setl);
//                setId = setl.getSettlementId();
//                reffNo = reffNo + "/" + setId + "/" + cheqNo;
//                addToPosting(loan.getDebtorHeaderDetails().getDebtorAccountNo(), reffNo, new BigDecimal(downPaymnt), branch, 2);
//                amount = amount - downPaymnt;
//            }
//
//            if (amount > 0) {
//                SettlementMain set2 = new SettlementMain();
//                set2.setLoanId(loan.getLoanId());
//                set2.setDebtorId(loan.getDebtorHeaderDetails().getDebtorId());
//                set2.setIsPrinted(false);
//                set2.setCrdtAmount(amount);
//                set2.setTransactionCodeCrdt("OTHR");
//                set2.setTransactionNoCrdt(formData.getTransactionNo());
//                set2.setDbtAmount(0.00);
//                set2.setTransactionCodeDbt("");
//                set2.setTransactionNoDbt("");
//                set2.setUserId(user_id);
//                set2.setActionDate(action_time);
//                set2.setDescription("Initial Other Charges");
//                set2.setBranchId(branch);
//                set2.setSystemDate(dayEndDate);
//                set2.setIsPosting(true);
//                session.save(set2);
//                if (setId == 0) {
//                    setId = set2.getSettlementId();
//                    reffNo = reffNo + "/" + setId + "/" + cheqNo;
//                }
//                addToPosting(loan.getDebtorHeaderDetails().getDebtorAccountNo(), reffNo, new BigDecimal(amount), branch, 2);
//            }
            String paybleAcc = findLoanAccountNo(loan.getLoanType(), PAYBLE_ACC_CODE);
            String accNo = "";
            String sql_bnk = "from ConfigChartofaccountBank where bankId=" + bankId;
            ConfigChartofaccountBank bankData = (ConfigChartofaccountBank) session.createQuery(sql_bnk).uniqueResult();
            if (bankData != null) {
                accNo = bankData.getBankAccountNo();
            }
            addToPosting(paybleAcc, reffNo, new BigDecimal(amount), branch, 2);
            addToPosting(accNo, reffNo, new BigDecimal(formData.getAmount()), branch, 1);
            session.flush();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
        return ret;
    }

    @Override
    public List<MSubLoanType> findMsubLoans() {
        List<MSubLoanType> subLoanTypes = new ArrayList<>();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql = "from MSubLoanType";
            subLoanTypes = (List<MSubLoanType>) session.createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            tx.commit();
            session.flush();
            session.close();
        }
        return subLoanTypes;
    }

    @Override
    public int findLoanIdByAgreementNo(String agNo) {
        int loanId = 0;
        try {
            String bookNo = agNo.replaceAll("-", "/");
            String sql2 = "FROM LoanHeaderDetails as loan WHERE loan.loanBookNo LIKE '" + bookNo + "%' OR loan.loanAgreementNo LIKE '" + bookNo + "%'";
            List<LoanHeaderDetails> loans = sessionFactory.getCurrentSession().createQuery(sql2).list();
            if (loans.size() > 0) {
                loanId = loans.get(0).getLoanId();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return loanId;
    }

    @Override
    public List removeLoans(int branchId) {
        List removeLoans = null;
        try {
            //SELECT lh.Loan_Id,sl.Sub_Loan_Name,dh.Debtor_Name_with_ini,lh.Loan_Amount FROM loan_header_details AS lh, m_sub_loan_type AS sl,debtor_header_details AS dh WHERE Loan_IsIssue = 0 AND lh.Branch_Id = 1 AND sl.Sub_Loan_Id = lh.Loan_Type AND dh.Debtor_Id = lh.Loan_Debtor_Id
            String sql = "select lh.loanId,sl.subLoanName,dh.nameWithInitial,lh.loanAmount, lh.loanBookNo from LoanHeaderDetails as lh,MSubLoanType as sl,DebtorHeaderDetails as dh where lh.loanIsIssue = 0 and sl.subLoanId = lh.loanType and dh.debtorId = lh.debtorHeaderDetails.debtorId and lh.loanIsDelete = 0 and lh.branchId = " + branchId + "order by lh.loanId desc";
            removeLoans = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return removeLoans;
    }

    @Override
    public List deletedLoans(int branchId) {
        List removeLoans = null;
        try {
            String sql = "select lh.loanId,sl.subLoanName,dh.nameWithInitial,lh.loanAmount, lh.loanBookNo from LoanHeaderDetails as lh,MSubLoanType as sl,DebtorHeaderDetails as dh where lh.loanIsIssue = 0 and sl.subLoanId = lh.loanType and dh.debtorId = lh.debtorHeaderDetails.debtorId and lh.loanIsDelete = 1 and lh.branchId = " + branchId + "order by lh.loanId desc";
            removeLoans = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return removeLoans;
    }

    @Override
    public int removeLoan(int loanId) {
        int is_Remove = 0;
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
        LoanHeaderDetails details = null;

        try {
            details = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            if (details != null) {
                removeLoansUpdate(details);
                is_Remove = 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return is_Remove;
    }

    public int removeLoansUpdate(LoanHeaderDetails details) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        int is_Remove = 0;
        try {
            if (details.getLoanIsIssue() == 0) {
                details.setLoanIsDelete(true);
                details.setLoanSpec(-1);
                session.saveOrUpdate(details);
                is_Remove = 1;
            } else {
                is_Remove = 2;
            }
            session.flush();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            session.close();
        }
        return is_Remove;

    }

    @Override
    public List findVouchersByDate(String vdate) {
        List voucherList = new ArrayList();
        try {
            int user_id = userDao.findByUserName();
            int branch = setImpl.getUserLogedBranch(user_id);
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
            Date systemDate = null;
            if (vdate == null || vdate.equals("")) {
                String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branch;
                Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
                if (obj != null) {
                    systemDate = obj;
                }
            } else {
                systemDate = new java.sql.Date(sd.parse(vdate).getTime());
            }

            String sql = "select vou.id, vou.voucherNo, vou.amount, vou.voucherCatogery, vou.description, loan.loanAgreementNo from SettlementVoucher as vou, LoanHeaderDetails as loan where vou.voucherDate='" + systemDate + "' and vou.branchId=" + branch + " and vou.loanId=loan.loanId";
            List v_list = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (v_list.size() > 0) {
                for (int i = 0; i < v_list.size(); i++) {
                    Object[] vouche = (Object[]) v_list.get(i);
                    SettlementVoucher settVoucher = new SettlementVoucher();
                    settVoucher.setId(Integer.parseInt(vouche[0].toString()));
                    settVoucher.setVoucherNo(vouche[1].toString());
                    settVoucher.setAmount(Double.parseDouble(vouche[2].toString()));
                    settVoucher.setVoucherCatogery(Integer.parseInt(vouche[3].toString()));
                    settVoucher.setDescription(vouche[4].toString());
                    settVoucher.setAgreementNo(vouche[5].toString());
                    voucherList.add(settVoucher);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return voucherList;

    }

    @Override
    public List findPaymentByVoucher(int vId) {
        List paymentList = new ArrayList();
        try {
            String sql_v = "select voucherNo from SettlementVoucher where id=" + vId;
            String voucherNo = sessionFactory.getCurrentSession().createQuery(sql_v).uniqueResult().toString();

            String sql_cash = "from SettlementPaymentDetailCash where transactionNo='" + voucherNo + "'";
            List cashList = sessionFactory.getCurrentSession().createQuery(sql_cash).list();

            if (cashList.size() > 0) {
                for (int i = 0; i < cashList.size(); i++) {
                    SettlementPaymentDetailCash cash = (SettlementPaymentDetailCash) cashList.get(i);
                    int cashId = cash.getDetailId();
                    String transNo = cash.getTransactionNo();
                    double amount = cash.getAmount();

                    VoucherDetails vouch = new VoucherDetails();
                    vouch.setAmount(amount);
                    vouch.setTransId(cashId);
                    vouch.setTransactionNo(transNo);
                    vouch.setType("CASH");
                    paymentList.add(vouch);
                }
            }

            String sql_cheq = "from SettlementPaymentDetailCheque where transactionNo='" + voucherNo + "'";
            List cheqList = sessionFactory.getCurrentSession().createQuery(sql_cheq).list();

            if (cheqList.size() > 0) {
                for (int i = 0; i < cheqList.size(); i++) {
                    SettlementPaymentDetailCheque cheq = (SettlementPaymentDetailCheque) cheqList.get(i);
                    int cashId = cheq.getChequeId();
                    String transNo = cheq.getTransactionNo();
                    double amount = cheq.getAmount();
                    int bankId = cheq.getBank();
                    Date relizeDate = cheq.getRealizeDate();
                    String cheqNo = cheq.getChequeNo();

                    VoucherDetails vouch = new VoucherDetails();
                    vouch.setAmount(amount);
                    vouch.setTransId(cashId);
                    vouch.setTransactionNo(transNo);
                    vouch.setCheqNo(cheqNo);
                    vouch.setBank(bankId);
                    vouch.setRealizeDate(relizeDate);
                    vouch.setType("CHEQUE");
                    paymentList.add(vouch);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return paymentList;
    }

    @Override
    public List findInstallmentByLoanNo(String loanNo) {
        Session session= sessionFactory.openSession();
        List insList2 = new ArrayList();
        try {
            int user_id = userDao.findByUserName();
            int branch = setImpl.getUserLogedBranch(user_id);
            Date systemDate = null;
            String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branch;
            Date obj = (Date) session.createQuery(sql_d).uniqueResult();
//            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();//Annr
            if (obj != null) {
                systemDate = obj;
            }

            String bookNo = loanNo.replaceAll("-", "/");
            String sql2 = "FROM LoanHeaderDetails as loan WHERE loan.loanBookNo = '" + bookNo + "' OR loan.loanAgreementNo = '" + bookNo + "'";
            List loanList = session.createQuery(sql2).list();
//            List loanList = sessionFactory.getCurrentSession().createQuery(sql2).list();//Annr

            if (loanList.size() > 0) {
                LoanHeaderDetails loan = (LoanHeaderDetails) loanList.get(0);
                int loanId = loan.getLoanId();

                String sql = "from LoanInstallment where loanId=" + loanId;
                List insList = session.createQuery(sql).list();
//                List insList = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
                if (insList.size() > 0) {
                    for (int i = 0; i < insList.size(); i++) {
                        LoanInstallment ins = (LoanInstallment) insList.get(i);
                        Date dueDate = ins.getDueDate();
                        if (systemDate.after(dueDate) || systemDate.equals(dueDate)) {
                            ins.setInsStatus(1);
                        }
                        insList2.add(ins);
                    }
                }
            }
            session.clear();
            session.close();
            return insList2;
        } catch (Exception e) {
            session.clear();
            session.close();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean updateRental(LoanInstallment instlment) {
        boolean msg = false;
        Calendar cal = Calendar.getInstance();
        Date actionTime = cal.getTime();
        try {
            int user_id = userDao.findByUserName();
            String sql = "from LoanInstallment where insId=" + instlment.getInsId() + " and loanId=" + instlment.getLoanId();
            LoanInstallment newIns = (LoanInstallment) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (newIns != null) {
                newIns.setInsPrinciple(instlment.getInsPrinciple());
                newIns.setInsInterest(instlment.getInsInterest());
                newIns.setInsAmount(instlment.getInsAmount());
                newIns.setInsRoundAmount(instlment.getInsRoundAmount());
                newIns.setDueDate(instlment.getDueDate());
                newIns.setActionTime(actionTime);
                newIns.setUserId(user_id);
                int isChng = newIns.getIsChange() + 1;
                newIns.setIsChange(isChng);
                sessionFactory.getCurrentSession().update(newIns);
                msg = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }

    //Reverse approved Loan
    @Override
    public String reverseApprovedLoan(int loanId, String comment) {
        String msg = "OK";
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            int user_id = userDao.findByUserName();
            int branch = setImpl.getUserLogedBranch(user_id);
            Date systemDate = null;
            String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branch;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (obj != null) {
                systemDate = obj;
            }

            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();

            String sql1 = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql1).uniqueResult();
            if (loan != null) {
                String sql_pay = "from SettlementPayments where loanId=" + loanId;
                List payments = session.createQuery(sql_pay).list();
                if (payments.size() > 0) {
                    return "This Loan has already done payments. You cannot reverse this loan";
                } else {
                    int isPaid = loan.getLoanIsPaidDown();
                    int isIssue = loan.getLoanIsIssue();
                    int edit = loan.getLoanIsEdit();
                    if (isPaid > 0 && isIssue == 3) {
                        return "Loan cannot be reverse. It is already paid.";
                    } else {
                        String sql2 = "from SettlementMain where loanId=" + loanId + " and (transactionCodeDbt='INTRS' or transactionCodeDbt='DWN' or transactionCodeDbt='OTHR' or transactionCodeDbt='LOAN')";
                        List settmntList = session.createQuery(sql2).list();
                        if (settmntList.size() > 0) {
                            for (int i = 0; i < settmntList.size(); i++) {
                                SettlementMain set = (SettlementMain) settmntList.get(i);
                                int setId = set.getSettlementId();
                                String ref = loanId + "/" + setId;
                                String sql3 = "from Posting where referenceNo like '%" + ref + "'";
                                List postingList = session.createQuery(sql3).list();
                                if (postingList.size() > 0) {
                                    for (int j = 0; j < postingList.size(); j++) {
                                        Posting po = (Posting) postingList.get(j);
                                        session.delete(po);
                                    }
                                }
                                session.delete(set);
                            }
                        }

                        //delete installment
                        String sql4 = "from LoanInstallment where loanId=" + loanId;
                        List installList = session.createQuery(sql4).list();
                        if (installList.size() > 0) {
                            for (int k = 0; k < installList.size(); k++) {
                                LoanInstallment ins = (LoanInstallment) installList.get(k);
                                session.delete(ins);
                            }
                        }

                        //add to loan reverse history
                        LoanReverseDetails reverse = new LoanReverseDetails();
                        reverse.setLoanId(loanId);
                        reverse.setLoanInstallment(loan.getLoanInstallment());
                        reverse.setLoanInterest(loan.getLoanInterest());
                        reverse.setLoanRate(loan.getLoanInterestRate());
                        reverse.setLoanDownPayment(loan.getLoanDownPayment());
                        reverse.setLoanType(loan.getLoanType());
                        reverse.setLoanPeriod(loan.getLoanPeriod());
                        reverse.setLoanPeriodType(loan.getLoanPeriodType());
                        reverse.setLoanAmount(loan.getLoanAmount().doubleValue());
                        reverse.setLoanDueDay(loan.getDueDay());
                        reverse.setEditNo(edit + 1);
                        reverse.setEditOption(2);
                        reverse.setEditReason(comment);
                        reverse.setBranchId(branch);
                        reverse.setEditDate(systemDate);
                        reverse.setEditUser(user_id);
                        reverse.setActionDate(actionTime);
                        session.save(reverse);

                        //update loan status
                        loan.setLoanIsIssue(0);
                        loan.setLoanStartDate(null);
                        loan.setLoanDueDate(null);
                        loan.setLoanIsEdit((edit + 1));
                        session.update(loan);
                    }
                }
            }
            session.flush();
            tx.commit();
        } catch (Exception e) {
            msg = "Cannot Reverse Loan";
            tx.rollback();
        } finally {
            if (session != null) {
                // session.clear();
                session.close();
            }
        }
        return msg;
    }

    //reverse processing loan
    @Override
    public String reverseProcessLoan(int loanId, String comment) {
        String msg = "OK";
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            int user_id = userDao.findByUserName();
            int branch = setImpl.getUserLogedBranch(user_id);
            Date systemDate = null;
            String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branch;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (obj != null) {
                systemDate = obj;
            }
            Calendar cal = Calendar.getInstance();
            Date actionTime = cal.getTime();

            String sql1 = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql1).uniqueResult();
            if (loan != null) {
                int edit = loan.getLoanIsEdit();

                //remove approval
                String sql2 = "from LoanApprovedLevels where loanId=" + loanId;
                List approveList = session.createQuery(sql2).list();
                String apprvalLevel = "";
                if (approveList.size() > 0) {
                    for (int i = 0; i < approveList.size(); i++) {
                        LoanApprovedLevels appr = (LoanApprovedLevels) approveList.get(i);
                        int apprId = appr.getApprovedLevel();
                        session.delete(appr);
                    }
                }

                //remove recovery report
                String sql3 = "from LoanRecoveryReport where loanId=" + loanId;
                LoanRecoveryReport recover = (LoanRecoveryReport) session.createQuery(sql3).uniqueResult();
                if (recover != null) {
                    session.delete(recover);
                }

//                //remove Insurance
                InsuraceProcessMain processMain = findInsurance(loanId, 0);
                if (processMain != null) {
                    String sql = "from SettlementMain where loanId = " + loanId + " and transactionNoDbt = '" + processMain.getInsuCode() + "'";
                    SettlementMain settlementMain = (SettlementMain) session.createQuery(sql).uniqueResult();
                    if (settlementMain != null) {
                        String refNo = loanId + "/" + settlementMain.getSettlementId();
                        if (settlementMain.getIsPosting()) {
                            String del = "delete from Posting where referenceNo like '%" + refNo + "%'";
                            session.createQuery(del).executeUpdate();
                        }
                        session.delete(settlementMain);
                        session.delete(processMain);
                    }
                }
                if (loan.getLoanAgreementNo() != null && !"".equals(loan.getLoanAgreementNo())) {
                    //update subloan type maxCount
                    MSubLoanType subLoanType = adminDaoImpl.findMSubLoanType(loan.getLoanType());
                    subLoanType.setMaxCount(subLoanType.getMaxCount() - 1);
                    session.update(subLoanType);
                    loan.setLoanAgreementNo(null);
                }
                //update loan Status
                loan.setLoanApprovalStatus(1);
                loan.setInsuranceStatus(0);
                loan.setLoanStatus(0);
                loan.setLoanIsEdit((edit + 1));
                session.update(loan);

                //add loan reverse history
                LoanReverseDetails reverse = new LoanReverseDetails();
                reverse.setLoanId(loanId);
                reverse.setLoanInstallment(loan.getLoanInstallment());
                reverse.setLoanInterest(loan.getLoanInterest());
                reverse.setLoanRate(loan.getLoanInterestRate());
                reverse.setLoanDownPayment(loan.getLoanDownPayment());
                reverse.setLoanType(loan.getLoanType());
                reverse.setLoanPeriod(loan.getLoanPeriod());
                reverse.setLoanPeriodType(loan.getLoanPeriodType());
                reverse.setLoanAmount(loan.getLoanAmount().doubleValue());
                reverse.setLoanDueDay(loan.getDueDay());
                reverse.setEditNo(edit + 1);
                reverse.setEditOption(1);
                reverse.setEditReason(comment);
                reverse.setBranchId(branch);
                reverse.setEditDate(systemDate);
                reverse.setEditUser(user_id);
                reverse.setActionDate(actionTime);
                session.save(reverse);
            }
            session.flush();
            tx.commit();
        } catch (Exception e) {
            msg = "Cannot Reverse Loan";
            tx.rollback();
        } finally {
            if (session != null) {
                session.clear();
                session.close();
            }
        }
        return msg;
    }

    //find insurance by loan Id
    @Override
    public InsuraceProcessMain findInsuranceByLoanId(int loanId
    ) {
        Session session=sessionFactory.openSession();
        InsuraceProcessMain insurance = null;
        try {
            String sql = "from InsuraceProcessMain where insuLoanId=" + loanId + " and renewCount=0";
            List insList = session.createQuery(sql).list();
//            List insList = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
            if (insList.size() > 0) {
                insurance = (InsuraceProcessMain) insList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return insurance;
    }

    @Override
    public boolean removeVoucher(int vId
    ) {
        boolean msg = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            int userId = userDao.findByUserName();
            String sqlv = "from SettlementVoucher where id=" + vId;
            SettlementVoucher voucher = (SettlementVoucher) session.createQuery(sqlv).uniqueResult();
            if (voucher != null) {
                String voucherNo = voucher.getVoucherNo();
                String sql1 = "from SettlementPaymentDetailCash where transactionNo='" + voucherNo + "'";
                List cashList = session.createQuery(sql1).list();
                if (cashList.size() > 0) {
                    for (int i = 0; i < cashList.size(); i++) {
                        SettlementPaymentDetailCash cash = (SettlementPaymentDetailCash) cashList.get(i);
                        session.delete(cash);
                    }
                }

                String sql2 = "from SettlementPaymentDetailCheque where transactionNo='" + voucherNo + "'";
                List cheqList = session.createQuery(sql2).list();
                if (cheqList.size() > 0) {
                    for (int i = 0; i < cheqList.size(); i++) {
                        SettlementPaymentDetailCheque cheq = (SettlementPaymentDetailCheque) cheqList.get(i);
                        session.delete(cheq);
                    }
                }

                String sql3 = "from SettlementMain where transactionCodeCrdt='LOAN' and transactionNoCrdt='" + voucherNo + "'";
                List settList = session.createQuery(sql3).list();
                for (int i = 0; i < settList.size(); i++) {
                    SettlementMain sett = (SettlementMain) settList.get(i);
                    if (sett != null) {
                        int setId = sett.getSettlementId();
                        int loanId = sett.getLoanId();
                        String sql4 = "From Posting where referenceNo like '%" + loanId + "/" + setId + "'";
                        List postList = session.createQuery(sql4).list();
                        for (int j = 0; j < postList.size(); j++) {
                            Posting pos = (Posting) postList.get(j);
                            session.delete(pos);
                        }
                    }
                    session.delete(sett);
                }

                SettlementVoucherDelete deleteVoucher = new SettlementVoucherDelete();
                deleteVoucher.setLoanId(voucher.getLoanId());
                deleteVoucher.setAmount(voucher.getAmount());
                deleteVoucher.setBranchId(voucher.getBranchId());
                deleteVoucher.setVoucherCatogery(voucher.getVoucherCatogery());
                deleteVoucher.setVoucherDate(voucher.getVoucherDate());
                deleteVoucher.setVoucherNo(voucherNo);
                deleteVoucher.setVoucherType(voucher.getVoucherType());
                deleteVoucher.setDescription("Delete Voucher");
                deleteVoucher.setUserId(userId);
                deleteVoucher.setActionTime(Calendar.getInstance().getTime());
                session.save(deleteVoucher);

                session.delete(voucher);
            }
            session.flush();
            tx.commit();
            msg = true;
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.clear();
                session.close();
            }
        }
        return msg;
    }

    @Override
    public boolean updateCheqDetails(SettlementPaymentDetailCheque cheq
    ) {
        boolean msg = false;
        try {
            String sql = "from SettlementPaymentDetailCheque where chequeId=" + cheq.getChequeId();
            SettlementPaymentDetailCheque chqNew = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (chqNew != null) {
                chqNew.setBank(cheq.getBank());
                chqNew.setChequeNo(cheq.getChequeNo());
                chqNew.setRealizeDate(cheq.getRealizeDate());
                chqNew.setRemark(cheq.getRemark());
                sessionFactory.getCurrentSession().update(chqNew);
            }
            msg = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public boolean deletePayment(int transId, int type
    ) {
        boolean msg = false;
        try {
            if (type == 1) {
                String sql = "from SettlementPaymentDetailCash where detailId=" + transId;
                SettlementPaymentDetailCash cash = (SettlementPaymentDetailCash) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
                if (cash != null) {
                    sessionFactory.getCurrentSession().delete(cash);
                }
            } else {
                String sql = "from SettlementPaymentDetailCheque where chequeId=" + transId;
                SettlementPaymentDetailCheque cheq = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
                if (cheq != null) {
                    sessionFactory.getCurrentSession().delete(cheq);
                }
            }
            msg = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public Double[] findLoanInitialCharges(int loanId) {
        Session session=sessionFactory.openSession();
        Double[] valu = new Double[6];
        try {
            double dbtAmount = 0.00;
            String sql = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE (transactionCodeDbt='OTHR' OR transactionCodeDbt='DWN' OR transactionCodeDbt='DWNS' OR transactionCodeDbt = 'INSU') AND loanId=" + loanId;
            Object obj = session.createQuery(sql).uniqueResult();
//            Object obj = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            if (obj != null) {
                dbtAmount = Double.parseDouble(obj.toString());
            }
            double crdtAmount = 0.00;
            String sql2 = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE (transactionCodeCrdt='OTHR' OR transactionCodeCrdt='DWN' OR transactionCodeCrdt='DWNS' OR transactionCodeCrdt='INSU') AND loanId=" + loanId;
            Object obj2 = session.createQuery(sql2).uniqueResult();
//            Object obj2 = sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();//Annr
            if (obj2 != null) {
                crdtAmount = Double.parseDouble(obj2.toString());
            }
            double otherCharge = 0.00;
            String sqlD = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE transactionCodeDbt='OTHR' AND loanId=" + loanId;
            Object objD = session.createQuery(sqlD).uniqueResult();
//            Object objD = sessionFactory.getCurrentSession().createQuery(sqlD).uniqueResult();//Annr
            if (objD != null) {
                otherCharge = Double.parseDouble(objD.toString());
            }

            double paidCharge = 0.00;
            String sqlC = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE transactionCodeCrdt='OTHR' AND loanId=" + loanId;
            Object objC = session.createQuery(sqlC).uniqueResult();
//            Object objC = sessionFactory.getCurrentSession().createQuery(sqlC).uniqueResult();//Annr
            if (objC != null) {
                paidCharge = Double.parseDouble(objC.toString());
            }
            double downPayment = 0.00;
            String sql4 = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE transactionCodeDbt='DWN' AND loanId=" + loanId;
            Object obj4 = session.createQuery(sql4).uniqueResult();
//            Object obj4 = sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();//Annr
            if (obj4 != null) {
                downPayment = Double.parseDouble(obj4.toString());
            }

            double adj_crd = 0.00;
            String sql5 = "SELECT SUM(crdtAmount) FROM SettlementMain WHERE (transactionCodeCrdt='OTHR' OR transactionCodeCrdt='DWN' OR transactionCodeCrdt='DWNS') AND transactionNoCrdt LIKE 'ADJ%' AND loanId=" + loanId;
            Object obj5 = session.createQuery(sql5).uniqueResult();
//            Object obj5 = sessionFactory.getCurrentSession().createQuery(sql5).uniqueResult();//Annr
            if (obj5 != null) {
                adj_crd = Double.parseDouble(obj5.toString());
            }
            double adj_dbt = 0.00;
            String sql6 = "SELECT SUM(dbtAmount) FROM SettlementMain WHERE (transactionCodeDbt='OTHR' OR transactionCodeDbt='DWN' OR transactionCodeDbt='DWNS') AND transactionNoDbt LIKE 'ADJ%' AND loanId=" + loanId;
            Object obj6 = session.createQuery(sql6).uniqueResult();
//            Object obj6 = sessionFactory.getCurrentSession().createQuery(sql6).uniqueResult();//Annr
            if (obj6 != null) {
                adj_dbt = Double.parseDouble(obj6.toString());
            }
            valu[0] = dbtAmount - crdtAmount;
            valu[1] = otherCharge;
            valu[2] = downPayment;
            valu[3] = adj_dbt - adj_crd;
            valu[4] = paidCharge;
            valu[5] = crdtAmount;
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return valu;
    }

    @Override
    public int updateDueDate(String due, int loanId) {
        int flag = 0;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dueDate = sdf.parse(due);
            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();
            if (loan != null) {
                loan.setLoanDueDate(dueDate);
            }
            session.update(loan);
            session.flush();
            tx.commit();
            flag = 1;
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.clear();
                session.close();
            }
        }
        return flag;
    }

    public double findExcessAmount(int loanId) {
        Session session=sessionFactory.openSession();
        double excess = 0.00;
        try {
            String sql = "from LoanInstallment where loanId=" + loanId;
            List insList = session.createQuery(sql).list();
//            List insList = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
            double roundAmount = 0.00;
            double insAmount = 0.00;
            if (!insList.isEmpty()) {
                for (int i = 0; i < insList.size(); i++) {
                    LoanInstallment ins = (LoanInstallment) insList.get(i);
                    if (ins.getInsRoundAmount() != null) {
                        roundAmount = roundAmount + ins.getInsRoundAmount();
                    }
                    if (ins.getInsAmount() != null) {
                        insAmount = insAmount + ins.getInsAmount();
                    }
                }
                excess = roundAmount - insAmount;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return excess;
    }

    @Override
    public String findCheqRecievedName(int chekId, int cheqTo) {
        Session session=sessionFactory.openSession();
        String name = "";
        try {
            int loanId = 0;
            String sql = "select v.loanId from SettlementVoucher v, SettlementPaymentDetailCheque c where c.chequeId=" + chekId + " and c.transactionNo=v.voucherNo";
//            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            Object ob = session.createQuery(sql).uniqueResult();
            if (ob != null) {
                loanId = Integer.parseInt(ob.toString());
            }
            if (cheqTo == 0) {
                //to customer
                String sql1 = "from LoanHeaderDetails where loanId=" + loanId;
//                LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();//Annr
                LoanHeaderDetails loan = (LoanHeaderDetails) session.createQuery(sql1).uniqueResult();
                if (loan != null) {
                    name = loan.getDebtorHeaderDetails().getDebtorName();
                }
            } else {
                //to dealer
                int dealerId = 0;
                String sqla = "from LoanPropertyArticlesDetails where loanId=" + loanId + " order by articleId desc";
                List<LoanPropertyArticlesDetails> artcle = session.createQuery(sqla).list();
//                List<LoanPropertyArticlesDetails> artcle = sessionFactory.getCurrentSession().createQuery(sqla).list();//Annr
                if (artcle.size() > 0) {
                    dealerId = artcle.get(0).getDealerId();
                }

                String sqll = "from LoanPropertyLandDetails where loanId=" + loanId + " order by landId desc";
                List<LoanPropertyLandDetails> land = session.createQuery(sqll).list();
//                List<LoanPropertyLandDetails> land = sessionFactory.getCurrentSession().createQuery(sqll).list();//Annr
                if (land.size() > 0) {
                    dealerId = land.get(0).getDealerId();
                }

                String sqlo = "from LoanPropertyOtherDetails where loanId=" + loanId + " order by otherId desc";
                List<LoanPropertyOtherDetails> others = session.createQuery(sqlo).list();
//                List<LoanPropertyOtherDetails> others = sessionFactory.getCurrentSession().createQuery(sqlo).list()//Annr;
                if (others.size() > 0) {
                    dealerId = others.get(0).getDealerId();
                }

                String sqlv = "from LoanPropertyVehicleDetails where loanId=" + loanId + " order by vehicleId desc";
                List<LoanPropertyVehicleDetails> vehicle = session.createQuery(sqlv).list();
//                List<LoanPropertyVehicleDetails> vehicle = sessionFactory.getCurrentSession().createQuery(sqlv).list();//Annr
                if (vehicle.size() > 0) {
                    dealerId = vehicle.get(0).getDealerId();
                }
                if (dealerId > 0) {
                    String sql2 = "from MSupplier where supplierId=" + dealerId;
                    MSupplier supp = (MSupplier) session.createQuery(sql2).uniqueResult();
//                    MSupplier supp = (MSupplier) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();//Annr
                    name = supp.getSupplierName();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return name;
    }

    @Override
    public List createInstallmentList(LoanHeaderDetails loan) {
        List installmentList = new ArrayList();
        DecimalFormat df = new DecimalFormat("###,###.00");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String sql = "from LoanInstallment where loanId=" + loan.getLoanId();
            List<LoanInstallment> inslment = sessionFactory.getCurrentSession().createQuery(sql).list();
            Double totalPaid = 0.00, balance = 0.00, loss = 0.00, totalAmount = 0.00;

            int period = loan.getLoanPeriod();
            int periodType = loan.getLoanPeriodType();
            double rate = loan.getLoanInterestRate();

            Double pp = 0.00;
            if (periodType == 1) {
                pp = (double) period;
            } else if (periodType == 2) {
                pp = (double) period * 12;
            } else if (periodType == 3) {
                pp = (double) period / 30;
            }

            double investment = loan.getLoanInvestment();
            if (loan.getLoanRateCode() == "FR") {
                Double interest = 0.00;
                interest = (investment * rate) / 100;
                Double totalInterest = interest * pp;
                totalAmount = investment + totalInterest;
            } else {
                Double interest = (investment * rate) / 1200;
                Double totalInterest = interest * pp;
                totalAmount = investment + totalInterest;
            }
            for (int i = 0; i < inslment.size(); i++) {
                LoanInstallment ins_ = (LoanInstallment) inslment.get(i);
                double roundValue = ins_.getInsRoundAmount();
                double insallmentAmount = ins_.getInsAmount();
                Date dueDate = ins_.getDueDate();
                PaymentDetails insList = new PaymentDetails();
                totalPaid = totalPaid + roundValue;
                balance = totalAmount - totalPaid;

                if (balance < 0) {
                    loss = Math.abs(balance);
                    balance = 0.00;
                }
                insList.setPaymentDate(sdf.format(dueDate));
                insList.setInstallment(df.format(insallmentAmount));
                insList.setRoundInstallment(df.format(roundValue));
                insList.setTotal(df.format(totalPaid));
                insList.setBalance(df.format(balance));
                insList.setLossAmount(df.format(loss));
                installmentList.add(insList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return installmentList;
    }

    @Override
    public Double findLossAmount(int loanId) {
        Session session=sessionFactory.openSession();
        Double lossAmount = 0.00;
        Double totalPayAmount = 0.00;
        Double totalInstallment = 0.00;
        DecimalFormat df = new DecimalFormat("###,###.00");
        try {
            String sql = "from LoanInstallment where loanId=" + loanId;
            List<LoanInstallment> inslment = session.createQuery(sql).list();
//            List<LoanInstallment> inslment = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
            for (int i = 0; i < inslment.size(); i++) {
                LoanInstallment loanIns = (LoanInstallment) inslment.get(i);
                totalInstallment = totalInstallment + loanIns.getInsAmount();
                totalPayAmount = totalPayAmount + loanIns.getInsRoundAmount();
            }
            String loss = df.format(totalPayAmount - totalInstallment);
            lossAmount = Double.parseDouble(loss);
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return lossAmount;
    }

    @Override
    public DebtorHeaderDetails findDebtorHeader(int debtorId) {
        DebtorHeaderDetails detailses = null;

        try {
            detailses = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createCriteria(DebtorHeaderDetails.class
            )
                    .add(Restrictions.eq("debtorId", debtorId)).uniqueResult();
        } catch (Exception e) {
            return null;
        }
        return detailses;
    }

    //update LapsDate in LoanHeaderDetails
    public void updateLoanLapsDate(int loanId) {
        Date due = null;
//        Session session = sessionFactory.openSession();

        try {
//            List insList = session.createCriteria(LoanInstallment.class)
            List insList = sessionFactory.getCurrentSession().createCriteria(LoanInstallment.class)
                    .add(Restrictions.eq("loanId", loanId)).list();
            if (insList != null || !insList.isEmpty() || insList.size()!=0) {
                LoanInstallment ob = (LoanInstallment) insList.get(insList.size() - 1);
                due = ob.getDueDate();
            }
            Calendar c = Calendar.getInstance();

            c.setTime(due);

            c.add(Calendar.DATE, 1);
            Date lapsDay = c.getTime();
            String sql = "from LoanHeaderDetails where loanId = " + loanId;
//            LoanHeaderDetails details = (LoanHeaderDetails) session.createQuery(sql).uniqueResult();//Annr
            LoanHeaderDetails details = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            if (details != null) {
                details.setLoanLapsDate(lapsDay);
                sessionFactory.getCurrentSession().update(details);
//                session.update(details);
            }
//            session.flush();
//            session.clear();
//            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean findInsuranceStatus(int loanId) {
        Boolean isInsurance = true;
        try {
            LoanHeaderDetails headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class)
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            if (headerDetails.getLoanType() == 1 || headerDetails.getLoanType() == 2 || headerDetails.getLoanType() == 3 || headerDetails.getLoanType() == 4) {
                if (headerDetails.getInsuranceStatus() != 1) {
                    isInsurance = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isInsurance;
    }

    private InsuraceProcessMain findInsurance(int loanId, int reCount) {
        InsuraceProcessMain processMain = null;
        try {
            processMain = (InsuraceProcessMain) sessionFactory.getCurrentSession().createCriteria(InsuraceProcessMain.class
            )
                    .add(Restrictions.eq("insuLoanId", loanId))
                    .add(Restrictions.eq("renewCount", reCount)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processMain;
    }

    @Override
    public boolean deleteLoanPropertyArticleDetails(int articleId) {
        boolean isDelete = false;
        try {
            String sql = "delete from LoanPropertyArticlesDetails where articleId = :articleId";
            int delete = sessionFactory.getCurrentSession().createQuery(sql).setParameter("articleId", articleId)
                    .executeUpdate();
            if (delete > 0) {
                isDelete = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDelete;
    }

    @Override
    public List<LoanInstalmentSchedule> findInstalmentSchedule(int loanId) {
        List<LoanInstalmentSchedule> instalmentSchedules = new ArrayList<>();
        List list = null;

        try {
            LoanHeaderDetails headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class
            )
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            list = sessionFactory.getCurrentSession().createCriteria(LoanInstalmentSchedule.class
            )
                    .add(Restrictions.eq("loanId", loanId)).list();
            int period = headerDetails.getLoanPeriod();
            int periodType = headerDetails.getLoanPeriodType();
            int noOfInstallmnt = 0;
            if (periodType
                    == 1) {
                noOfInstallmnt = period;
            } else if (periodType
                    == 2) {
                noOfInstallmnt = period * 12;
            } else if (periodType
                    == 3) {
                int module = period % 7;
                if (module > 0) {
                    noOfInstallmnt = (period / 7) + 1;
                } else {
                    noOfInstallmnt = period / 7;
                }
            } else if (periodType
                    == 4) {
                noOfInstallmnt = period;
            }

            if (list.isEmpty()) {
                for (int i = 1; i <= noOfInstallmnt; i++) {
                    LoanInstalmentSchedule schedule = new LoanInstalmentSchedule();
                    schedule.setInsMonth(i);
                    schedule.setMonthlyInstallment(0.00);
                    instalmentSchedules.add(schedule);

                }
            } else {
                instalmentSchedules = sessionFactory.getCurrentSession().createCriteria(LoanInstalmentSchedule.class)
                        .add(Restrictions.eq("loanId", loanId)).list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instalmentSchedules;
    }

    @Override
    public String findTotalLoanAmountWithInterest(int loanId) {
        Double totalLoanAmount = 0.00;
        String total = "";

        try {
            LoanHeaderDetails headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class
            )
                    .add(Restrictions.eq("loanId", loanId)).uniqueResult();
            int period = headerDetails.getLoanPeriod();
            int periodType = headerDetails.getLoanPeriodType();
            int noOfInstallmnt = 0;
            if (periodType
                    == 1) {
                noOfInstallmnt = period;
            } else if (periodType
                    == 2) {
                noOfInstallmnt = period * 12;
            } else if (periodType
                    == 3) {
                int module = period % 7;
                if (module > 0) {
                    noOfInstallmnt = (period / 7) + 1;
                } else {
                    noOfInstallmnt = period / 7;
                }
            } else if (periodType == 4) {
                noOfInstallmnt = period;
            }
            Double totalInterest = headerDetails.getLoanInterest() * noOfInstallmnt;
            totalLoanAmount = headerDetails.getLoanInvestment() + totalInterest;

            List<LoanInstalmentSchedule> schedule = sessionFactory.getCurrentSession().createCriteria(LoanInstalmentSchedule.class)
                    .add(Restrictions.eq("loanId", loanId)).list();
            Double insTotal = 0.00;

            if (schedule.isEmpty()) {
                total = df.format(totalLoanAmount);
            } else {
                for (int i = 0; i < schedule.size(); i++) {
                    LoanInstalmentSchedule ls = (LoanInstalmentSchedule) schedule.get(i);
                    insTotal = insTotal + ls.getMonthlyInstallment();
                }
                totalLoanAmount = totalLoanAmount - insTotal;
                total = df.format(totalLoanAmount);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return total;
    }

    @Override
    public Boolean createSchedule(LoanInstalmentSchedule instalmentSchedule, int[] insMonths, Double[] insAmounts) {
        Boolean isUpdated = false;
        Session session = null;
        Transaction transaction = null;
        int userId = userDao.findByUserName();
        int branchId = setImpl.getUserLogedBranch(userId);
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Calendar calendar = Calendar.getInstance();
            Date actionTime = calendar.getTime();

            String del = "delete from LoanInstalmentSchedule where loanId = " + instalmentSchedule.getLoanId();
            session.createQuery(del).executeUpdate();
            for (int i = 0; i < insAmounts.length; i++) {
                LoanInstalmentSchedule schedule = new LoanInstalmentSchedule();
                schedule.setLoanId(instalmentSchedule.getLoanId());
                schedule.setInsMonth(i + 1);
                if (insAmounts[i] == 0.00) {
                    schedule.setMonthlyInstallment(0.00);
                    schedule.setStatus(0);
                } else {
                    schedule.setMonthlyInstallment(insAmounts[i]);
                    schedule.setStatus(1);
                }
                schedule.setBranchId(branchId);
                schedule.setUserId(userId);
                schedule.setActionTime(actionTime);
                session.save(schedule);
                schedule = null;
            }
            session.flush();
            transaction.commit();
            isUpdated = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return isUpdated;
    }

    @Override
    public LoanHeaderDetails findLoanHeaderDetails(int loanId) {
        LoanHeaderDetails headerDetails = null;

        try {
            headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class
            )
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("loanIsDelete", false)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return headerDetails;
    }

    @Override
    public List<MCaptilizeType> findCaptilizeTypes() {
        List<MCaptilizeType> captilizeTypes = null;
        try {
            String sql = "from MCaptilizeType";
            captilizeTypes = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return captilizeTypes;
    }

    @Override
    public MCaptilizeType findCaptilizeType(int capTypId) {
        MCaptilizeType captilizeType = null;
        try {
            String sql = "from MCaptilizeType where id = " + capTypId;
            captilizeType = (MCaptilizeType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return captilizeType;
    }

    @Override
    public boolean saveOrUpdateLoanCapitalizeDetail(LoanCapitalizeDetail loanCapitalizeDetail) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(loanCapitalizeDetail);
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public boolean saveOrUpdateLoanCapitalizeDetail(List<LoanCapitalizeDetail> loanCapitalizeDetails) {
        boolean success = false;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            for (int i = 0; i < loanCapitalizeDetails.size(); i++) {
                session.saveOrUpdate(loanCapitalizeDetails.get(i));
                if (i % 20 == 0) { //20(batch-size),Release memory after batch inserts.
                    session.flush();
                    session.clear();
                }
            }
            tx.commit();
            success = true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }

    @Override
    public List<LoanCapitalizeDetail> findLoanCapitalizeDetails(Integer loanId) {
        List<LoanCapitalizeDetail> loanCapitalizeDetails = null;
        Session session=sessionFactory.openSession();
        try {
            String sql = "from LoanCapitalizeDetail where loanHeaderDetails.loanId = " + loanId;
            loanCapitalizeDetails = session.createQuery(sql).list();
//            loanCapitalizeDetails = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return loanCapitalizeDetails;
    }

    @Override
    public LoanPropertyVehicleDetails findVehicleByLoanId(int loanId) {
        LoanPropertyVehicleDetails vehicle = null;
        try {
            String sql = "from LoanPropertyVehicleDetails where loanId=" + loanId;
            vehicle = (LoanPropertyVehicleDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicle;
    }

    @Override
    public int findLoanInstallmentCount(int loanId) {
        int count = 0;
        try {
            String sql = "from LoanInstallment where loanId = " + loanId;
            List list = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (list != null) {
                count = list.size();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public List<LoanGuaranteeDetails> findLoanGuranteeDetails(int loanId) {
        List<LoanGuaranteeDetails> detailses = null;
        try {
            detailses = sessionFactory.getCurrentSession().createCriteria(LoanGuaranteeDetails.class)
                    .add(Restrictions.eq("loanId", loanId))
                    .add(Restrictions.eq("guarantorIsActive", true)).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detailses;
    }

    @Override
    public List<LoanHeaderDetails> findLoanHeaderDetailseByAggNo(String aggNo) {
        List<LoanHeaderDetails> loanHeaderDetailses = null;
        try {
            String sql = "from LoanHeaderDetails where loanAgreementNo like '%" + aggNo + "%'";
            loanHeaderDetailses = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return loanHeaderDetailses;
    }

    private Double calTotalInsterest(List<LoanInstalmentSchedule> schedule, Double loanAmount, Double loanIntRate) {
        LoanInstalmentSchedule lonInstalmentPrv = null;
        Integer noOfInstallmnt = schedule.size();
        Double totalMonthlyInterest = 0.00;
        try {
            for (LoanInstalmentSchedule lonInstalment : schedule) {
                Double monthlyInterest = 0.00;
                Double monthlyInstallment = 0.00;
                if (lonInstalmentPrv == null) {
                    monthlyInterest = df.parse(df.format((loanAmount * loanIntRate))).doubleValue();
                    lonInstalment.setMonthlyInterest(monthlyInterest);
                    if (lonInstalment.getStatus() == 0) {
                        monthlyInstallment = df.parse(df.format((loanAmount * (loanIntRate)) / (1 - (Math.pow(1 / (1 + (loanIntRate)), (noOfInstallmnt)))))).doubleValue();
                        lonInstalment.setMonthlyInstallment(monthlyInstallment);
                        lonInstalment.setPrincipalAmount(df.parse(df.format(loanAmount - monthlyInstallment + monthlyInterest)).doubleValue());
                    } else {
                        lonInstalment.setPrincipalAmount(df.parse(df.format(loanAmount - lonInstalment.getMonthlyInstallment() + monthlyInterest)).doubleValue());
                    }
                } else {
                    monthlyInterest = df.parse(df.format((lonInstalmentPrv.getPrincipalAmount() * loanIntRate))).doubleValue();
                    lonInstalment.setMonthlyInterest(monthlyInterest);
                    if (lonInstalment.getStatus() == 0) {
                        monthlyInstallment = df.parse(df.format((lonInstalmentPrv.getPrincipalAmount() * (loanIntRate)) / (1 - (Math.pow(1 / (1 + (loanIntRate)), (noOfInstallmnt)))))).doubleValue();
                        lonInstalment.setMonthlyInstallment(monthlyInstallment);
                        lonInstalment.setPrincipalAmount(df.parse(df.format(lonInstalmentPrv.getPrincipalAmount() - monthlyInstallment + monthlyInterest)).doubleValue());
                    } else {
                        lonInstalment.setPrincipalAmount(df.parse(df.format(lonInstalmentPrv.getPrincipalAmount() - lonInstalment.getMonthlyInstallment() + monthlyInterest)).doubleValue());
                    }
                }

                totalMonthlyInterest += monthlyInterest;
                lonInstalmentPrv = lonInstalment;
                noOfInstallmnt--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return totalMonthlyInterest;
    }

    private Double calMonthlyInstalment(List<LoanInstalmentSchedule> schedule, Double loanAmount, Double totalInsterest) {
        Double totalLoanPayment = loanAmount + totalInsterest;
        Double totalDefinePyamet = 0.00;
        Integer noOfDefinePyamet = 0;
        Integer noOfInstallmnt = schedule.size();
        Double monthlyInstalment = 0.00;
        try {

            for (LoanInstalmentSchedule lonInstalment : schedule) {
                if (lonInstalment.getStatus() == 1) {
                    totalDefinePyamet += lonInstalment.getMonthlyInstallment();
                    noOfDefinePyamet++;
                }
            }
            monthlyInstalment = (totalLoanPayment - totalDefinePyamet) / (noOfInstallmnt - noOfDefinePyamet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return monthlyInstalment;
    }

    private Double processMonthlyInstalment(List<LoanInstalmentSchedule> schedule, double monthlyInstalment, Double loanAmount, Double loanIntRate) {
        LoanInstalmentSchedule lonInstalmentPrv = null;

        Double toatalInt = 0.00;
        try {
            for (LoanInstalmentSchedule lonInstalment : schedule) {
                Double monthlyInterest;
                if (lonInstalmentPrv == null) {
                    monthlyInterest = df.parse(df.format((loanAmount * loanIntRate))).doubleValue();
                    toatalInt += monthlyInterest;
                    lonInstalment.setMonthlyInterest(monthlyInterest);
                    if (lonInstalment.getStatus() == 0) {
                        lonInstalment.setMonthlyInstallment(monthlyInstalment);
                        lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(monthlyInstalment - lonInstalment.getMonthlyInterest())).doubleValue());
                        lonInstalment.setPrincipalAmount(df.parse(df.format(loanAmount - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                    } else {
                        lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(lonInstalment.getMonthlyInstallment() - lonInstalment.getMonthlyInterest())).doubleValue());
                        lonInstalment.setPrincipalAmount(df.parse(df.format(loanAmount - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                    }
                } else {
                    monthlyInterest = df.parse(df.format((lonInstalmentPrv.getPrincipalAmount() * loanIntRate))).doubleValue();
                    toatalInt += monthlyInterest;
                    lonInstalment.setMonthlyInterest(monthlyInterest);
                    if (lonInstalment.getStatus() == 0) {
                        lonInstalment.setMonthlyInstallment(monthlyInstalment);
                        lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(monthlyInstalment - lonInstalment.getMonthlyInterest())).doubleValue());
                        lonInstalment.setPrincipalAmount(df.parse(df.format(lonInstalmentPrv.getPrincipalAmount() - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                    } else {
                        lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(lonInstalment.getMonthlyInstallment() - lonInstalment.getMonthlyInterest())).doubleValue());
                        lonInstalment.setPrincipalAmount(df.parse(df.format(lonInstalmentPrv.getPrincipalAmount() - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                    }
                }
                lonInstalmentPrv = lonInstalment;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toatalInt;
    }

    private void finalMonthlyInstalmentProcess(List<LoanInstalmentSchedule> schedule, double monthlyInstalmentnew, Double loanAmount) {

        LoanInstalmentSchedule lonInstalmentPrv = null;
        try {
            for (LoanInstalmentSchedule lonInstalment : schedule) {
                if (lonInstalmentPrv == null) {
                    if (lonInstalment.getStatus() == 0) {
                        lonInstalment.setMonthlyInstallment(monthlyInstalmentnew);
                        lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(monthlyInstalmentnew - lonInstalment.getMonthlyInterest())).doubleValue());
                        lonInstalment.setPrincipalAmount(df.parse(df.format(loanAmount - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                    } else {
                        lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(lonInstalment.getMonthlyInstallment() - lonInstalment.getMonthlyInterest())).doubleValue());
                        lonInstalment.setPrincipalAmount(df.parse(df.format(loanAmount - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                    }

                } else if (lonInstalment.getStatus() == 0) {
                    lonInstalment.setMonthlyInstallment(monthlyInstalmentnew);
                    lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(monthlyInstalmentnew - lonInstalment.getMonthlyInterest())).doubleValue());
                    lonInstalment.setPrincipalAmount(df.parse(df.format(lonInstalmentPrv.getPrincipalAmount() - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                } else {
                    lonInstalment.setMonthlyInstallmentPrincipal(df.parse(df.format(lonInstalment.getMonthlyInstallment() - lonInstalment.getMonthlyInterest())).doubleValue());
                    lonInstalment.setPrincipalAmount(df.parse(df.format(lonInstalmentPrv.getPrincipalAmount() - lonInstalment.getMonthlyInstallmentPrincipal())).doubleValue());
                }

                lonInstalmentPrv = lonInstalment;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean clearInstalmentSchedule(int loanId) {
        Boolean isDelete = false;
        try {
            String del = "delete from LoanInstalmentSchedule where loanId = " + loanId;
            sessionFactory.getCurrentSession().createQuery(del).executeUpdate();
            isDelete = true;
        } catch (Exception e) {
            e.printStackTrace();
            isDelete = false;
        }
        return isDelete;
    }

    @Override
    public double getTotalInterestAmount(Integer loanId) {
        Double total = 0.00;
        Session session= sessionFactory.openSession();
        try {
            String sql = "select SUM(l.insInterest) from LoanInstallment l where l.loanId = " + loanId;
            Object object = session.createQuery(sql).uniqueResult();
//            Object object = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            if (object != null) {
                total = (Double) object;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return total;
    }

    @Override
    public DebtorHeaderDetails findDebtorHeader(String debtorAccount) {
        DebtorHeaderDetails debtor = null;
        try {
            String sql = "from DebtorHeaderDetails where debtorAccountNo = '" + debtorAccount + "'";
            debtor = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql)
                    .uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return debtor;
    }

    private void balanceToInstalmet(List<LoanInstallment> instl, double investment, Double totalInterest) {
        try {
            Double totInter = 0.00;
            Double totCapital = 0.00;
            for (LoanInstallment loanInstallment : instl) {
                totInter = totInter + loanInstallment.getInsInterest();
                totCapital = totCapital + loanInstallment.getInsPrinciple();
            }
            Double difTotalInterest = totInter - totalInterest;
            Double difTotalCapital = totCapital - investment;

            LoanInstallment li = (LoanInstallment) instl.get(instl.size() - 1);
            li.setInsInterest(df.parse(df.format(li.getInsInterest() - difTotalInterest)).doubleValue());
            li.setInsPrinciple(df.parse(df.format(li.getInsPrinciple() - difTotalCapital)).doubleValue());
            li.setInsAmount(df.parse(df.format(li.getInsPrinciple() + li.getInsInterest())).doubleValue());
            li.setInsRoundAmount(df.parse(df.format(li.getInsPrinciple() + li.getInsInterest())).doubleValue());
//            li.setInsRoundAmount((Double) (Math.ceil(li.getInsAmount() / 5d) * 5));
            sessionFactory.getCurrentSession().update(li);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<LoanPropertyVehicleDetails> findVehicleByRegNo(String vehicleRegNo) {
        List<LoanPropertyVehicleDetails> lpvds = null;
        try {
            String sql = "from LoanPropertyVehicleDetails where vehicleRegNo like :vehicleRegNo and isDelete = :isDelete";
            lpvds = sessionFactory.getCurrentSession().createQuery(sql).setParameter("vehicleRegNo", "%" + vehicleRegNo + "%")
                    .setParameter("isDelete", false).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return lpvds;
    }

    @Override
    public List<CustomerInfo> findLoanByIsIssuAndIsDelete() {
        List<LoanHeaderDetails> lhds = null;
        List<LoanGuaranteeDetails> guaranteeDetailse = null;
        List<LoanPropertyVehicleDetails> propertyVehicleDetails = null;
        List<CustomerInfo> customerInfo = new ArrayList<>();
        try {
            String sql = "FROM LoanHeaderDetails WHERE loanIsIssue = 2 AND loanIsDelete = 0 ";
            lhds = sessionFactory.getCurrentSession().createQuery(sql).list();

            if (lhds != null) {
                for (LoanHeaderDetails details : lhds) {
                    CustomerInfo cInfo = new CustomerInfo();
                    cInfo.setClientName(details.getDebtorHeaderDetails().getNameWithInitial());
                    cInfo.setNic(details.getDebtorHeaderDetails().getDebtorNic());
                    cInfo.setAgreementNo(details.getLoanAgreementNo());
                    cInfo.setClientAddress(details.getDebtorHeaderDetails().getDebtorPersonalAddress());
                    cInfo.setDueDate(details.getDueDay());
                    if (details.getLoanId() != null) {
                        String sql2 = "FROM LoanInstallment WHERE loanId = " + details.getLoanId() + " ORDER BY insId";
                        LoanInstallment installment = (LoanInstallment) sessionFactory.getCurrentSession().createQuery(sql2).setMaxResults(1).uniqueResult();
                        cInfo.setMonthlyRental(installment.getInsRoundAmount());
                        cInfo.setRentalDate1st(installment.getDueDate());

                        String sql3 = "FROM LoanGuaranteeDetails WHERE loanId = " + details.getLoanId() + " ";
                        guaranteeDetailse = sessionFactory.getCurrentSession().createQuery(sql3).list();
                        if (guaranteeDetailse != null) {
                            switch (guaranteeDetailse.size()) {
                                case 3: {
                                    DebtorHeaderDetails gr1 = debtorDaoImpl.findDebtor(guaranteeDetailse.get(0).getDebtorId());
                                    if (gr1 != null) {
                                        cInfo.setGurantor1(gr1.getDebtorName());
                                        cInfo.setGurantor1Address(gr1.getDebtorPersonalAddress());
                                    }
                                    DebtorHeaderDetails gr2 = debtorDaoImpl.findDebtor(guaranteeDetailse.get(1).getDebtorId());
                                    if (gr2 != null) {
                                        cInfo.setGurantor2(gr2.getDebtorName());
                                        cInfo.setGurantor2Address(gr2.getDebtorPersonalAddress());
                                    }
                                    DebtorHeaderDetails gr3 = debtorDaoImpl.findDebtor(guaranteeDetailse.get(2).getDebtorId());
                                    if (gr3 != null) {
                                        cInfo.setGurantor3(gr3.getDebtorName());
                                        cInfo.setGurantor3Address(gr3.getDebtorPersonalAddress());
                                    }
                                    break;
                                }
                                case 2: {
                                    DebtorHeaderDetails gr1 = debtorDaoImpl.findDebtor(guaranteeDetailse.get(0).getDebtorId());
                                    if (gr1 != null) {
                                        cInfo.setGurantor1(gr1.getDebtorName());
                                        cInfo.setGurantor1Address(gr1.getDebtorPersonalAddress());
                                    }
                                    DebtorHeaderDetails gr2 = debtorDaoImpl.findDebtor(guaranteeDetailse.get(1).getDebtorId());
                                    if (gr2 != null) {
                                        cInfo.setGurantor2(gr2.getDebtorName());
                                        cInfo.setGurantor2Address(gr2.getDebtorPersonalAddress());
                                    }
                                    break;
                                }
                                case 1: {
                                    DebtorHeaderDetails gr1 = debtorDaoImpl.findDebtor(guaranteeDetailse.get(0).getDebtorId());
                                    if (gr1 != null) {
                                        cInfo.setGurantor1(gr1.getDebtorName());
                                        cInfo.setGurantor1Address(gr1.getDebtorPersonalAddress());
                                    }
                                    break;
                                }
                                default:
                                    cInfo.setGurantor1("");
                                    cInfo.setGurantor1Address("");
                                    cInfo.setGurantor2("");
                                    cInfo.setGurantor2Address("");
                                    cInfo.setGurantor3("");
                                    cInfo.setGurantor3Address("");
                                    break;
                            }
                        }

                    }
                    String sql4 = "FROM LoanPropertyVehicleDetails WHERE loanId = " + details.getLoanId() + "";
                    propertyVehicleDetails = sessionFactory.getCurrentSession().createQuery(sql4).list();
                    if (propertyVehicleDetails != null) {
                        for (LoanPropertyVehicleDetails lpvd : propertyVehicleDetails) {
                            VehicleMake vehicleMake = masterDataService.findVehicleMakeById(Integer.parseInt(lpvd.getVehicleMake()));
                            VehicleModel vehicleModel = masterDataService.findVehicleModelById(Integer.parseInt(lpvd.getVehicleMake()));
                            cInfo.setVehicleNo(lpvd.getVehicleRegNo());
                            cInfo.setMake(vehicleMake.getVehicleMakeName());
                            cInfo.setModel(vehicleModel.getVehicleModelName());

                        }
                    }
                    customerInfo.add(cInfo);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return customerInfo;
    }

    @Override
    public List<LoanHeaderDetails> findLoanDetailsAndVehicleDetailsByAggNo(String aggNo) {
        List<LoanHeaderDetails> list = null;
        try {
            String sql1 = "FROM LoanHeaderDetails WHERE loanAgreementNo LIKE '%" + aggNo + "%'";
            list = sessionFactory.getCurrentSession().createQuery(sql1).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<LoanHeaderDetails> findLoanDetailsAndVehicleDetailsByVehicleNo(String vehicleNo) {
        List<LoanPropertyVehicleDetails> lpVehicleDetailsList = null;
        List<LoanHeaderDetails> list = new ArrayList<>();
        try {
            String sql1 = "FROM LoanPropertyVehicleDetails WHERE vehicleRegNo LIKE '%" + vehicleNo + "%'";
            lpVehicleDetailsList = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (lpVehicleDetailsList != null) {
                for (LoanPropertyVehicleDetails lpvd : lpVehicleDetailsList) {
                    String sql2 = "FROM LoanHeaderDetails WHERE loanId = " + lpvd.getLoanId();
                    LoanHeaderDetails lhd = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql2)
                            .uniqueResult();
                    if (lhd != null) {
                        lhd.setVehicleNo(lpvd.getVehicleRegNo());
                    }
                    list.add(lhd);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public boolean saveOrUpdateTerminateAgreementByLoanId(int loanId) {
        boolean is_saved = false;
        Session session = null;
        Transaction tx = null;
        LoanHeaderDetails loanHeaderDetails = null;

        if (loanId != 0) {
            try {
                session = sessionFactory.openSession();
                tx = session.beginTransaction();
                String sql1 = "UPDATE LoanHeaderDetails SET loanSpec = 6 WHERE loanId = " + loanId + "";
                session.createQuery(sql1).executeUpdate();

                String sql2 = "FROM LoanHeaderDetails  WHERE loanId = " + loanId + " ";
                loanHeaderDetails = (LoanHeaderDetails) session.createQuery(sql2).uniqueResult();

                RecoveryLetterDetail letterDetail = new RecoveryLetterDetail();
                letterDetail.setLoanId(loanId);
                letterDetail.setAgreementNo(loanHeaderDetails.getLoanAgreementNo());
                letterDetail.setLetterId(2);
                letterDetail.setDueDate(loanHeaderDetails.getLoanDueDate());
                letterDetail.setBalance(reportDaoImpl.findLoanBalance(loanId));
                letterDetail.setLetterStatus(1);
                RecoveryLetters letter = recoveryDaoImpl.findLetter(2);
                if (letter != null) {
                    letterDetail.setLetterCharge(letter.getCharge());
                }

                recoveryDaoImpl.saveLetter(letterDetail);
                session.flush();
                tx.commit();
                is_saved = true;
            } catch (Exception e) {
                if (tx != null) {
                    tx.rollback();
                }
                e.printStackTrace();
            } finally {

                session.close();
            }
        }
        return is_saved;
    }

    List<LoanHeaderDetails> findLoanHeaderDetailsList(String lhdsList) {
        LoanHeaderDetails headerDetails = null;
        Session session = null;
//        List<Integer> loanId = new ArrayList<Integer>();
//        for (int i = 0; i < lhdsList.size(); i++) {
//            loanId.add(lhdsList.get(i).getLoanId());
//        }
        session = sessionFactory.openSession();

        String sql = "from LoanHeaderDetails where loanIsDelete = false and loanId in (" + lhdsList + ")";
//        session.createQuery(sql).setParameterList("loanId", loanId);

        List<LoanHeaderDetails> headerDetailsList = session.createQuery(sql).list();

//        try {
//            headerDetails = (LoanHeaderDetails) sessionFactory.getCurrentSession().createCriteria(LoanHeaderDetails.class
//            )
//                    .add(Restrictions.in("loanId", loanIds))
//                    .add(Restrictions.eq("loanIsDelete", false)).uniqueResult();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return headerDetailsList;
    }

    @Override
    public int saveNewLoanDataGroup(LoanForm loanData) {

        Integer[] detorIds = loanData.getGroupDetorIds();
        double[] debtorAmount = loanData.getGroupDebtorAmount();
        double[] debtorPeriod = loanData.getGroupDebtorPeriod();
        double[] debtorRate = loanData.getGroupDebtorRate();
        double[] debtorInterest = loanData.getGroupDebtorInterest();
        double[] debtorTotalInterest = loanData.getGroupDebtorTotalInterest();
        double[] debtorInstallment = loanData.getGroupDebtorInstallment();
        double[] debtorTotal = loanData.getGroupDebtorTotal();

        int groupID = loanData.getGroupLoanID();
        String message = null;
        int loanId = 0;
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        boolean update = false;
        try {
            int userId = userDao.findByUserName();

            Calendar calendar = Calendar.getInstance();
            Date actionTime = calendar.getTime();
            String current_date = dateFormat.format(actionTime);

            LoanHeaderDetails headerData = null;

            int noOfDebtors = detorIds.length;

            for (int i = 0; i < detorIds.length; i++) {
                System.out.println("xxxxxxxxxxx " + detorIds[i]);

                int debtorId = detorIds[i];
                String sql = "from DebtorHeaderDetails where debtorId=" + debtorId;
//                DebtorHeaderDetails debtor = (DebtorHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

                Session ses = sessionFactory.openSession();

                DebtorHeaderDetails debtor = (DebtorHeaderDetails) ses.createQuery(sql).uniqueResult();

                if (debtor.getDebtorAccountNo() == null || debtor.getDebtorAccountNo().equals("")) {
                    String accNo = debImpl.generateDebtorAccount();
                    debtor.setDebtorAccountNo(accNo);
                    debtor.setDebtorIsDebtor(true);
                }

                headerData = new LoanHeaderDetails();
                if (loanData.getLoanId() != null) {
                    headerData.setLoanId(loanData.getLoanId());
                    update = true;
                }

//                double loadAmount = loanData.getLoanAmount().doubleValue() / noOfDebtors;
//                double downPayment = loanData.getLoanDownPayment() / noOfDebtors;
//                double loanInvestment = loanData.getLoanInvestment() / noOfDebtors;
//                double loanInstallment = loanData.getLoanInstallment() / noOfDebtors;
//                double loanInterest = loanData.getLoanInterest() / noOfDebtors;
                double loadAmount = debtorAmount[i];
                double downPayment = 0;
                double loanInvestment = debtorAmount[i];
                double loanInstallment = debtorInstallment[i];
                double loanInterest = debtorTotalInterest[i];

                headerData.setLoanAmount(new BigDecimal(loadAmount));
                headerData.setLoanDownPayment(downPayment);
                headerData.setLoanInvestment(loanInvestment);
                headerData.setLoanPeriod(loanData.getLoanPeriod());
                headerData.setLoanPeriodType(loanData.getPeriodType());
                headerData.setLoanInterestRate(loanData.getLoanRate());
                headerData.setLoanType(loanData.getLoanType());
                headerData.setLoanRateCode(loanData.getLoanRateCode());
                headerData.setLoanDate(dateFormat.parse(current_date));
                headerData.setLoanInstallment(loanInstallment);
                headerData.setLoanInterest(loanInterest);
                headerData.setLoanMarketingOfficer(loanData.getMarketingOfficer());
                headerData.setLoanApproval1(loanData.getSalesPerson());
                headerData.setLoanApproval2(loanData.getRecoveryPerson());
                headerData.setLoanApproval3(loanData.getCed());
                headerData.setLoanApp1Comment(loanData.getComment1());
                headerData.setLoanApp2Comment(loanData.getComment2());
                headerData.setGroupId(groupID);

                String docSubmit = "from MSubLoanChecklist where MSubLoanType.subLoanId = " + loanData.getLoanType();
                List docList = sessionFactory.getCurrentSession().createQuery(docSubmit).list();
                if (!docList.isEmpty()) {
                    if (loanData.getBranchId() == 1 || loanData.getBranchId() == 2) {
                        headerData.setLoanDocumentSubmission(3);
                    } else {
                        headerData.setLoanDocumentSubmission(1);
                    }
                } else {
                    headerData.setLoanDocumentSubmission(3);
                }

                headerData.setDebtorHeaderDetails(debtor);
                headerData.setActionTime(actionTime);
                headerData.setUserId(userId);
                headerData.setBranchId(loanData.getBranchId());
                headerData.setDueDay(loanData.getDueDay());
                if (loanData.getStartDate() != null) {
                    headerData.setLoanStartDate(loanData.getStartDate());
                }
                if (loanData.getDueDate() != null) {
                    headerData.setLoanDueDate(loanData.getDueDate());
                }

                if (loanData.getBookNo() != null) {
                    headerData.setLoanBookNo(loanData.getBookNo());
                } else {
                    headerData.setLoanBookNo("");
                }

                session.saveOrUpdate(headerData);

            }

            loanId = headerData.getLoanId();
            //set loan Guarantors
            int order = 1;

            String sql5 = "from LoanGuaranteeDetails where loanId=" + loanId;
            List<LoanGuaranteeDetails> g_list = session.createQuery(sql5).list();
            if (g_list.size() > 0) {
                for (int i = 0; i < g_list.size(); i++) {
                    LoanGuaranteeDetails guarantor = (LoanGuaranteeDetails) g_list.get(i);
                    session.delete(guarantor);
                }
            }

            if (loanData.getGuarantorID() != null) {
                if (loanData.getGuarantorID().length > 0) {
                    for (int i = 0; i < loanData.getGuarantorID().length; i++) {
                        order = order + i;
                        LoanGuaranteeDetails guarantor = new LoanGuaranteeDetails();
                        guarantor.setLoanId(loanId);
                        guarantor.setDebtorId(loanData.getGuarantorID()[i]);
                        guarantor.setGurantorOrder(order);
                        guarantor.setActionTime(actionTime);
                        guarantor.setUserId(userId);
                        session.saveOrUpdate(guarantor);
                    }
                }
            }
            //set loan Other Charges

            String sql6 = "from LoanOtherCharges where loanId=" + loanId + " and isDownPaymentCharge=1";
            List<LoanOtherCharges> other_list = session.createQuery(sql6).list();
            if (other_list.size() > 0) {
                for (int i = 0; i < other_list.size(); i++) {
                    LoanOtherCharges charg = (LoanOtherCharges) other_list.get(i);
                    session.delete(charg);
                }
            }
            double chargSum = 0.00;
            if (loanData.getChragesId() != null) {
                if (loanData.getChragesId().length > 0) {
                    for (int i = 0; i < loanData.getChragesId().length; i++) {
                        int c_id = loanData.getChragesId()[i];
                        String sql4 = "from MSubTaxCharges where subTaxId=" + c_id;
                        MSubTaxCharges subTax = (MSubTaxCharges) sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();

                        LoanOtherCharges charges = new LoanOtherCharges();
                        charges.setLoanId(loanId);
                        charges.setChargesId(c_id);
                        charges.setActualRate(subTax.getSubTaxRate());
                        if (subTax.getIsPrecentage()) {
                            double actual_amount = (loanData.getLoanInvestment() * subTax.getSubTaxRate()) / 100;
                            charges.setActualAmount(actual_amount);
                        } else {
                            double actual_amount = subTax.getSubTaxValue();
                            charges.setActualAmount(actual_amount);
                        }
                        charges.setChargeRate(loanData.getChragesRate()[i]);
                        double chargAmnt = 0.00;
                        if (loanData.getChragesAmount()[i] != null) {
                            chargAmnt = loanData.getChragesAmount()[i];
                        }
                        chargSum = chargSum + chargAmnt;
                        charges.setChargeAmount(chargAmnt);
                        charges.setAccountNo(subTax.getSubTaxAccountNo());
                        charges.setIsDownPaymentCharge(1);
                        charges.setActionTime(actionTime);
                        charges.setUserId(userId);
                        session.save(charges);
                    }
                }
            }
            //set other property
            if (loanData.getProperyId() != null) {
                for (int i = 0; i < loanData.getProperyId().length; i++) {
                    int type = loanData.getProprtyType();
                    if (type == 1) {
                        String sql2 = "From LoanPropertyVehicleDetails where vehicleId=" + loanData.getProperyId()[i];
                        LoanPropertyVehicleDetails property = (LoanPropertyVehicleDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    } else if (type == 2) {
                        String sql2 = "From LoanPropertyArticlesDetails where articleId=" + loanData.getProperyId()[i];
                        LoanPropertyArticlesDetails property = (LoanPropertyArticlesDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    } else if (type == 3) {
                        String sql2 = "From LoanPropertyLandDetails where landId=" + loanData.getProperyId()[i];
                        LoanPropertyLandDetails property = (LoanPropertyLandDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    } else if (type == 4) {
                        String sql2 = "From LoanPropertyOtherDetails where otherId=" + loanData.getProperyId()[i];
                        LoanPropertyOtherDetails property = (LoanPropertyOtherDetails) session.createQuery(sql2).uniqueResult();
                        property.setLoanId(loanId);
                        session.update(property);
                    }
                }
            }

            // set capitalize types
            if (loanData.getCapitalizeAmount() != null && loanData.getCapitalizeId() != null) {
                String sql1 = "delete from LoanCapitalizeDetail where loanHeaderDetails.loanId = " + loanId;
                session.createQuery(sql1).executeUpdate();
                for (int i = 0; i < loanData.getCapitalizeAmount().length; i++) {
                    int capitalizeId = loanData.getCapitalizeId()[i];
                    double amount = loanData.getCapitalizeAmount()[i];
                    LoanCapitalizeDetail capitalizeDetail = new LoanCapitalizeDetail();
                    capitalizeDetail.setAmount(amount);
                    capitalizeDetail.setLoanHeaderDetails(new LoanHeaderDetails(loanId));
                    capitalizeDetail.setMCaptilizeType(new MCaptilizeType(capitalizeId));
                    session.saveOrUpdate(capitalizeDetail);
                }
            }
            message = "Saved Successfully";
            session.flush();
            tx.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanId;

    }

    @Override
    public List<DebtorHeaderDetails> findByMemNo(String memNo, Object bID) {
        List<DebtorHeaderDetails> debtors = new ArrayList<>();

        try {
            String sql = "from DebtorHeaderDetails where memberNumber like '%" + memNo + "%' and debtorIsActive=1 and branchId = " + bID + "";
            debtors = sessionFactory.getCurrentSession().createQuery(sql).list();
            return debtors;
        } catch (Exception e) {
            return null;
        }

    }

    private void setLoanCount(int loanId, String loanBookNo) {
        Session session = sessionFactory.openSession();

        try {
            int count = 0;
            int countInceriment = 0;

            String sql = "from LoanHeaderDetails where loanId = " + loanId;
            LoanHeaderDetails details = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            String sql1 = "SELECT COUNT(loanBookNo) from LoanHeaderDetails where loanBookNo='" + loanBookNo + "' AND loanIsIssue > 0";
            Object ob = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
            if (ob != null) {
                count = Integer.parseInt(ob.toString());
            }

            if (details != null) {
                countInceriment = count + 1;
                details.setLoanCount(countInceriment);
                sessionFactory.getCurrentSession().update(details);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<LoanHeaderDetails> findActiveLoansByDebtorIDAndLoanTypeId(int debtorID, int loanTypeId) {
        List<LoanHeaderDetails> lhd = new ArrayList<>();
        try {
            String sql = "from LoanHeaderDetails where debtorHeaderDetails.debtorId = " + debtorID + " and loanType =" + loanTypeId + " and loanSpec = 0 or loanSpec = 1 ";
            lhd = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lhd;
    }

    @Override
    public int findVoucherIdByVoucherNo(String voucherNo) {

        Session session = sessionFactory.openSession();

        String sql = "FROM SettlementVoucher WHERE voucherNo LIKE '" + voucherNo + "%'";
        SettlementVoucher voucherDetails = (SettlementVoucher) session.createQuery(sql).uniqueResult();
        int voucherId = 0;
        if (voucherDetails != null) {
            voucherId = voucherDetails.getId();
        }
        session.clear();
        session.close();
        return voucherId;
    }

    @Override
    public SettlementPaymentDetailCheque findSettlementPaymentDetailChequeByVoucherNo(String voucherNo, double amount, int loanId) {
        Session session = sessionFactory.openSession();
        SettlementPaymentDetailCheque detailCheque = null;

        String sql = "FROM SettlementPaymentDetailCheque WHERE transactionNo LIKE '" + voucherNo + "%'";
        detailCheque = (SettlementPaymentDetailCheque) session.createQuery(sql).uniqueResult();

        //        **************************************************************************************
// sahan ekanayake xxxxxxxxxxxxxxxxxxxxx
        //settlement
        double otherCharge = 0.00, downPaymnt = 0.00, amuontt = 0.00;
        String voucherCode = null, code = "";
        String voucher_no = "";
        String cash_ref1 = "";
        String cash_ref2 = "";
        String paybleAcc = "";
        String chequeInHandAcc = "";
        boolean update = false;

        //
        String sql4n = "from LoanHeaderDetails where loanId=" + loanId;
        LoanHeaderDetails loan_hn = (LoanHeaderDetails) session.createQuery(sql4n).uniqueResult();

        if (loan_hn != null) {
            loan_hn.setCreateVoucher(3);
            session.update(loan_hn);
            session.flush();//Ann
        }
        //

//        updatePrintVoucher(loanId);

        String sql4 = "from LoanHeaderDetails where loanId=" + loanId;
        LoanHeaderDetails loan_h = (LoanHeaderDetails) session.createQuery(sql4).uniqueResult();

        paybleAcc = findLoanAccountNo(loan_h.getLoanType(), PAYBLE_ACC_CODE);
        chequeInHandAcc = findLoanAccountNo(0, CHEQUE_IN_HAND_ACC_CODE);

        otherCharge = findOtherChargeSum(loan_h.getLoanId());

        if (loan_h.getLoanDownPayment() != null) {
            downPaymnt = loan_h.getLoanDownPayment();
        }

        paybleAcc = findLoanAccountNo(loan_h.getLoanType(), PAYBLE_ACC_CODE);
        chequeInHandAcc = findLoanAccountNo(0, CHEQUE_IN_HAND_ACC_CODE);
        String postingRefNo = generateReferencNo(loan_h.getLoanId(), 1);
        int paymentType = 2;

        //posting 6 --(1)
        double payingAmountt = amount;

        int cash_id = addToSettlement(loan_h, "LOAN", voucherCode, loan_h.getLoanAmount().doubleValue(), 1, 1);
        cash_ref1 = postingRefNo + "/" + cash_id;
        if (paymentType == 1) {
            if (!chequeInHandAcc.equals("")) {
                addToPosting(chequeInHandAcc, cash_ref1, new BigDecimal(payingAmountt), loan_h.getBranchId(), 1);
            } else {
                System.out.println("Can't Find Cheque In Hand Account");
            }
        } else {
            int bankId = detailCheque.getBank();
            String cheqNo = detailCheque.getChequeNo();
            cash_ref1 = cash_ref1 + "/" + cheqNo;
            String accNo = "";
            String sql_bnk = "from ConfigChartofaccountBank where bankId=" + bankId;
            ConfigChartofaccountBank bankData = (ConfigChartofaccountBank) session.createQuery(sql_bnk).uniqueResult();
            if (bankData != null) {
                accNo = bankData.getBankAccountNo();
            }
            addToPosting(accNo, cash_ref1, new BigDecimal(payingAmountt), loan_h.getBranchId(), 1);
        }
        //posting loan account(1)
        if (!update) {
            addToPosting(paybleAcc, cash_ref1, new BigDecimal(payingAmountt - downPaymnt), loan_h.getBranchId(), 2);
        }
        if (downPaymnt > 0 && !update) {
            if (loan_h.getLoanIsPaidDown() == 1) {
                //has suspend                    
                String refNo = "LOAN" + loan_h.getLoanId();
                int dwns_id = addToSettlement(loan_h, "DWNS", refNo, downPaymnt, 2, 1);
            }
            String downpymntAcc = findLoanAccountNo(loan_h.getLoanType(), DWN_ACC_CODE);
            if (!downpymntAcc.equals("")) {
                addToPosting(downpymntAcc, cash_ref1, new BigDecimal(downPaymnt), loan_h.getBranchId(), 2);
            } else {
                System.out.println("Can't Find DWN Account" + loan_h.getLoanId());
            }
        }

        //add excess amount
        double excessMoney = findExcessAmount(1);
        if (excessMoney > 0.00) {
            String trnscode = "EXSS" + loan_h.getLoanId();
            int excId = addToSettlement(loan_h, "LOAN", trnscode, excessMoney, 2, 1);
            String exc_ref = postingRefNo + "/" + excId;
            String exssMoneyAcc = findLoanAccountNo(0, EXSS_ACC_CODE);
            String debotorAcc = loan_h.getDebtorHeaderDetails().getDebtorAccountNo();
            if (!exssMoneyAcc.equals("") && !debotorAcc.equals("")) {
                addToPosting(exssMoneyAcc, exc_ref, new BigDecimal(excessMoney), loan_h.getBranchId(), 1);
                addToPosting(debotorAcc, exc_ref, new BigDecimal(excessMoney), loan_h.getBranchId(), 2);
            } else if (exssMoneyAcc.equals("")) {
                System.out.println("Can't Find ExssMoney Account");
            } else {
                System.out.println("Can't Find Debtor Account");
            }
        }

//        **************************************************************************************
        session.clear();
        session.close();
        return detailCheque;
    }

    @Override
    public void updatePaymentAmount(String voucherNo, double amt
    ) {
        Session session = sessionFactory.openSession();
        Transaction tx=session.beginTransaction();
        SettlementPaymentDetailCheque detailCheque = null;

        String sql = "FROM SettlementPaymentDetailCheque WHERE transactionNo LIKE '" + voucherNo + "%'";
        detailCheque = (SettlementPaymentDetailCheque) session.createQuery(sql).uniqueResult();

        if (detailCheque != null) {
            detailCheque.setAmount(amt);
            session.update(detailCheque);
        }
        session.flush();
        tx.commit();
        session.clear();
        session.close();
    }

//    @Transactional
    private void updatePrintVoucher(int loanId) {
        Session session = sessionFactory.openSession();
        String sql4 = "from LoanHeaderDetails where loanId=" + loanId;
        LoanHeaderDetails loan_h = (LoanHeaderDetails) session.createQuery(sql4).uniqueResult();
        if (loan_h != null) {
            loan_h.setCreateVoucher(3);
            session.update(loan_h);
            session.flush();
            session.close();
        }

    }

}

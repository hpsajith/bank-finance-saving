/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.ReturnCheckPaymentList;
import com.ites.bankfinance.dao.CheckReturnDao;
import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanOtherCharges;
import com.ites.bankfinance.model.MBankDetails;
import com.ites.bankfinance.model.MCompany;
import com.ites.bankfinance.model.MSubTaxCharges;
import com.ites.bankfinance.model.Posting;
import com.ites.bankfinance.model.SettlementMain;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementPayments;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ITESS
 */
@Repository
public class CheckReturnDaoImpl implements CheckReturnDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    SettlmentService settlmentService;

    @Autowired
    SettlementDaoImpl settlementDaoImpl;

    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    public final String CHEQUE_IN_HAND_ACC_CODE = "CHEQUE_IN_HAND";
    private final String refNoInstallmentCode = "INS";

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public List<ReturnCheckPaymentList> getCheckPayments(String fromDate, String toDate) {
        String sql = "from SettlementPayments where systemDate  between '" + fromDate + "' and '" + toDate + "' and paymentType='2' ";

        List<SettlementPayments> stlnmtPaymnt = (List<SettlementPayments>) sessionFactory.getCurrentSession().createQuery(sql).list();
        List<ReturnCheckPaymentList> returnList = new ArrayList();
        if (stlnmtPaymnt != null) {
            for (SettlementPayments obj : stlnmtPaymnt) {
                ReturnCheckPaymentList returnObj = new ReturnCheckPaymentList();
                returnObj.setActionDate(obj.getActionDate());
                returnObj.setReceiptNo(obj.getReceiptNo());
                String sql2 = "from SettlementPaymentDetailCheque where chequeId ='" + obj.getPaymentDetails() + "' ";
                SettlementPaymentDetailCheque checkDetlai = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql2).uniqueResult();
                if (checkDetlai != null) {
                    returnObj.setBank(checkDetlai.getBank());
                    returnObj.setChequeId(checkDetlai.getChequeId());
                    returnObj.setChequeNo(checkDetlai.getChequeNo());
                    returnObj.setAmount(checkDetlai.getAmount());
                    returnObj.setIsReturn(false);
                    if (checkDetlai.getIsReturn() == 1) {
                        returnObj.setIsReturn(true);
                    }
                    if (checkDetlai.getIsReturn() == 2) {
                        returnObj.setIsRealized(true);
                    }
                    returnObj.setTransactionDate(checkDetlai.getTransactionDate());
                    returnObj.setUserId(checkDetlai.getUserId());
                    returnObj.setRealizeDate(checkDetlai.getRealizeDate());
                    String sql4 = "from MBankDetails where id ='" + checkDetlai.getBank() + "' ";
                    MBankDetails bankD = (MBankDetails) sessionFactory.getCurrentSession().createQuery(sql4).uniqueResult();
                    returnObj.setBankName(bankD.getBankName());
                }
                String sql3 = "from LoanHeaderDetails where loanId ='" + obj.getLoanId() + "' ";
                LoanHeaderDetails loanH = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql3).uniqueResult();
                returnObj.setAgreementNo(loanH.getLoanAgreementNo());
                returnObj.setDebtorAccountNo(loanH.getDebtorHeaderDetails().getDebtorAccountNo());

                returnList.add(returnObj);
            }
        }
        return returnList;
    }

    public boolean updateChequePayment(int checkDetaiId, String remark, int userId, int stus, int bankId) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            int branchId = settlmentService.getUserLogedBranch(userId);
            String sql = "from SettlementPaymentDetailCheque where chequeId ='" + checkDetaiId + "' ";
            SettlementPaymentDetailCheque cheqD = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            cheqD.setIsReturn(stus);
            cheqD.setRemark(remark);
            cheqD.setChequeReturnBy(userId);

            session.update(cheqD);
            double cheqAmount = cheqD.getAmount();
            // Remove Cheque Realize Posting
//            if (stus == 2) {
//                String sql1 = "from SettlementPayments where transactionNo = '" + cheqD.getTransactionNo() + "'";
//                SettlementPayments stlmntPayment = (SettlementPayments) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();
//                String receiptNo = stlmntPayment.getReceiptNo();
//
//                String sql2 = "from SettlementMain where receiptNo = '" + receiptNo + "'";
//                List<SettlementMain> stlmntMainList = (List<SettlementMain>) sessionFactory.getCurrentSession().createQuery(sql2).list();
//                String settId = "";
//                int loanId = 0;
//                for (int i = 0; i < stlmntMainList.size(); i++) {
//                    SettlementMain settt = (SettlementMain) stlmntMainList.get(i);
//                    settId = settId + settt.getSettlementId() + "/";
//                    loanId = settt.getLoanId();
//
//                }
//
//                String refNo = generateReferencNo(loanId) + "/" + settId;
//                String chequeInHandAccNo = loanDaoImpl.findLoanAccountNo(0, CHEQUE_IN_HAND_ACC_CODE);
//                String bankAcc = "";
//                String sql3 = "select bankAccountNo from ConfigChartofaccountBank where bankId=" + bankId;
//                Object bb = session.createQuery(sql3).uniqueResult();
//                if (bb != null) {
//                    bankAcc = bb.toString();
//                }
//                if (!bankAcc.equals("") && !chequeInHandAccNo.equals("")) {
//                    addToPosting(chequeInHandAccNo, refNo, new BigDecimal(cheqAmount), branchId, 1);
//                    addToPosting(bankAcc, refNo, new BigDecimal(cheqAmount), branchId, 2);
//                } else {
//                    if (bankAcc.equals("")) {
//                        System.out.println(" Can't Find Bank  Account No" + loanId);
//                    } else {
//                        System.out.println(" Can't Find CHEQUE_IN_HAND_ACC Account No");
//                    }
//                    return false;
//
//                }
//            }
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            transaction.commit();
            session.flush();
            session.close();
        }
        return true;
    }

    public boolean addDebitEntriesforReturnChecks(int checkDetaiId, double chargeAmount) {
        boolean status = false;
        String refCodeOtherCharge = "OTHR";

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);

        String sql = "from SettlementPaymentDetailCheque where chequeId = '" + checkDetaiId + "'";
        SettlementPaymentDetailCheque cheqD = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

        String sql1 = "from SettlementPayments where transactionNo = '" + cheqD.getTransactionNo() + "'";
        SettlementPayments stlmntPayment = (SettlementPayments) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();

        String sql2 = "from SettlementMain where receiptNo = '" + stlmntPayment.getReceiptNo() + "'";
        List<SettlementMain> stlmntMainList = (List<SettlementMain>) sessionFactory.getCurrentSession().createQuery(sql2).list();

        int loanId = 0;
        String ac_debtor = "";
        if (stlmntMainList.size() > 0) {
            loanId = stlmntMainList.get(0).getLoanId();
        }
        String refNO = generateReferencNo(loanId);
        String sqld = "select loan.debtorHeaderDetails.debtorAccountNo from LoanHeaderDetails as loan where loanId=" + loanId;
        Object obb = session.createQuery(sqld).uniqueResult();
        if (obb != null) {
            ac_debtor = obb.toString();
        }

        for (SettlementMain obj : stlmntMainList) {
            SettlementMain smnt = new SettlementMain();
            smnt.setReceiptNo(obj.getReceiptNo());
            smnt.setBranchId(obj.getBranchId());
            smnt.setChargeId(obj.getChargeId());
            smnt.setDbtAmount(obj.getCrdtAmount());
            smnt.setCrdtAmount(obj.getDbtAmount());
            if (obj.getTransactionCodeCrdt().equals(refNoInstallmentCode)) {
                settlementDaoImpl.removeInstalmentPayStatus(obj.getLoanId(), obj.getCrdtAmount());
            }
            if (obj.getTransactionCodeCrdt() != "OVP") {
                smnt.setTransactionCodeDbt(obj.getTransactionCodeCrdt());
                smnt.setDescription(obj.getDescription() + "-ILU");
            } else {
                String sql5 = "from SettlementMain where transactionCodeCrdt='INS' and loanId ='" + obj.getLoanId() + "' and settlementId > '" + obj.getSettlementId() + "'";
                List<SettlementMain> stlmntMainList5 = (List<SettlementMain>) sessionFactory.getCurrentSession().createQuery(sql5).list();
                if (stlmntMainList5 != null) {
                    if (stlmntMainList5.size() > 0) {
                        smnt.setTransactionCodeDbt("INS");
                        smnt.setDescription("INS" + "-ILU");
                    }
                } else {
                    smnt.setTransactionCodeDbt(obj.getTransactionCodeCrdt());
                }
            }
            smnt.setTransactionCodeCrdt(obj.getTransactionCodeDbt());
            smnt.setTransactionNoDbt(obj.getTransactionNoCrdt());
            smnt.setSystemDate(dt);
            smnt.setLoanId(obj.getLoanId());
            smnt.setIsPosting(false);
            smnt.setDebtorId(obj.getDebtorId());
            smnt.setUserId(userId);

            session.save(smnt);

            String bankAcc = "";
            String sql3 = "select bankAccountNo from ConfigChartofaccountBank where bankId=" + cheqD.getBank();
            Object bb = session.createQuery(sql3).uniqueResult();
            if (bb != null) {
                bankAcc = bb.toString();
            }
            int setId = smnt.getSettlementId();
            refNO = refNO + "/" + setId;
            if (!bankAcc.equals("") && !ac_debtor.equals("")) {
                addToPosting(bankAcc, refNO, new BigDecimal(obj.getCrdtAmount()), userLogBranchId, 1);
                addToPosting(ac_debtor, refNO, new BigDecimal(obj.getCrdtAmount()), userLogBranchId, 2);
                status = true;
            } else {
                status = false;
                if (bankAcc.equals("")) {
                    System.out.println(" Can't Find Bank Account No" + cheqD.getBank());
                } else {
                    System.out.println(" Can't Find Debtor Account No" + loanId);

                }
            }
        }

//        SettlementMain smnt = new SettlementMain();
//            smnt.setReceiptNo(stlmntMainList.get(0).getReceiptNo());
//            smnt.setBranchId(stlmntMainList.get(0).getBranchId());
//            smnt.setChargeId(stlmntMainList.get(0).getChargeId());
//            smnt.setDbtAmount(chargeAmount);
//            smnt.setCrdtAmount(0.00);
//            smnt.setTransactionCodeDbt(refCodeOtherCharge);
//            smnt.setTransactionCodeCrdt("");
//            smnt.setTransactionNoDbt(stlmntMainList.get(0).getTransactionNoCrdt());
//            smnt.setSystemDate(dt);
//            smnt.setLoanId(stlmntMainList.get(0).getLoanId());
//            smnt.setIsPosting(false);
//            smnt.setDebtorId(stlmntMainList.get(0).getDebtorId());
//            smnt.setDescription(stlmntMainList.get(0).getDescription());
//            smnt.setUserId(userId);
//            
//            session.save(smnt);
        transaction.commit();
        session.close();

        return status;
    }

    @Override
    public SettlementPaymentDetailCheque getChequePaymentDetail(int checkDetaiId) {
        String sql = "from SettlementPaymentDetailCheque where chequeId = '" + checkDetaiId + "'";
        SettlementPaymentDetailCheque cheqD = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        return cheqD;
    }

    @Override
    public boolean addCheckReturnOtherCharge(int checkDetaiId, double chargeAmount, Integer chargeTypeId) {
        Session session = null;
        Transaction transaction = null;

        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Date systemDate = null;
            String sql_d = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + userLogBranchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql_d).uniqueResult();
            if (obj != null) {
                systemDate = obj;
            }

            String sql = "from SettlementPaymentDetailCheque where chequeId = '" + checkDetaiId + "'";
            SettlementPaymentDetailCheque cheqD = (SettlementPaymentDetailCheque) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();

            String sql1 = "from SettlementPayments where transactionNo = '" + cheqD.getTransactionNo() + "'";
            SettlementPayments stlmntPayment = (SettlementPayments) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();

            String sql8 = "from MSubTaxCharges where subTaxId=" + chargeTypeId;
            MSubTaxCharges tax = (MSubTaxCharges) sessionFactory.getCurrentSession().createQuery(sql8).uniqueResult();

            String accoNo = "";
            if (tax != null && !tax.getSubTaxAccountNo().equals("")) {
                accoNo = tax.getSubTaxAccountNo();
            } else {
                return false;
            }
            String debAccNo = "";
            String sql9 = "select loan.debtorHeaderDetails.debtorAccountNo from LoanHeaderDetails as loan where loan.loanId=" + stlmntPayment.getLoanId();
            Object ob9 = sessionFactory.getCurrentSession().createQuery(sql9).uniqueResult();
            if (ob9 != null) {
                debAccNo = ob9.toString();
            } else {
                return false;
            }
            LoanOtherCharges loanOtherCharges = new LoanOtherCharges();

            loanOtherCharges.setChargeAmount(chargeAmount);
            loanOtherCharges.setChargesId(chargeTypeId);
            loanOtherCharges.setLoanId(stlmntPayment.getLoanId());
            loanOtherCharges.setUserId(userId);
            loanOtherCharges.setIsDownPaymentCharge(0);
            loanOtherCharges.setIsPaid(0);
            if (tax.getSubTaxValue() != null) {
                loanOtherCharges.setActualAmount(tax.getSubTaxValue());
            }
            loanOtherCharges.setActionTime(Calendar.getInstance().getTime());
            loanOtherCharges.setAccountNo(accoNo);
            session.save(loanOtherCharges);

            SettlementMain main = new SettlementMain();
            main.setDebtorId(stlmntPayment.getDebtorId());
            main.setLoanId(stlmntPayment.getLoanId());
            main.setTransactionCodeDbt("OTHR");
            main.setTransactionNoDbt(stlmntPayment.getTransactionNo());
            main.setDescription("Cheque Return Charge");
            main.setDbtAmount(chargeAmount);
            main.setUserId(userId);
            main.setReceiptNo(stlmntPayment.getReceiptNo());
            main.setBranchId(stlmntPayment.getBranchId());
            main.setIsPosting(true);
            main.setSystemDate(systemDate);
            session.save(main);

            int setlId = main.getSettlementId();
            String postingRefNo = generateReferencNo(stlmntPayment.getLoanId());
            String referenceNo = postingRefNo + "/" + setlId;
            if (accoNo != null && !accoNo.equals("") && !debAccNo.equals("")) {
                if (chargeAmount > 0.00) {
                    addToPosting(accoNo, referenceNo, new BigDecimal(chargeAmount), userLogBranchId, 1);
                    addToPosting(debAccNo, referenceNo, new BigDecimal(chargeAmount), userLogBranchId, 2);
                }
            } else {
                return false;
            }
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return true;
    }

    @Override
    public List<MSubTaxCharges> getChargeTypes() {
        String sql1 = "from MSubTaxCharges where subTaxId = 19";
        List<MSubTaxCharges> chargeTypes = sessionFactory.getCurrentSession().createQuery(sql1).list();
        return chargeTypes;
    }

    //posting data
    public void addToPosting(String accountNo, String referenceNo, BigDecimal amount, int branchId, int cd) {
        int user_id = userDao.findByUserName();
        Calendar cal = Calendar.getInstance();
        Date action_time = cal.getTime();
        Date dayEndDate = null;
        try {
            MCompany company = userDao.findCompanyDetails();
            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                dayEndDate = obj;
            }

            if (!accountNo.equals("")) {
                Posting posting = new Posting();
                posting.setReferenceNo(referenceNo);
                posting.setAccountNo(accountNo);
                posting.setCreditDebit(cd);
                posting.setAmount(amount);
                if (cd == 1) {
                    posting.setCAmount(amount);
                    posting.setDAmount(new BigDecimal(0.00));
                } else {
                    posting.setDAmount(amount);
                    posting.setCAmount(new BigDecimal(0.00));
                }
                posting.setUserId(user_id);
                posting.setCurrDateTime(action_time);
                posting.setBranchId(branchId);
                posting.setAllocation(0);
                posting.setPostingFrom(1);
                posting.setPostingState(1);
                posting.setDate(dayEndDate);
                posting.setDescription("Posting from " + company.getComName() + " " + company.getComLname());
                sessionFactory.getCurrentSession().save(posting);
            } else {
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String generateReferencNo(int loanId) {
        try {
            Date action_time = Calendar.getInstance().getTime();
            String referenceNo = "";
            String datePart = "";
            String c_date = dateFormat.format(action_time);
            String splitDate[] = c_date.split("-");
            if (splitDate.length > 0) {
                for (int n = 0; n < splitDate.length; n++) {
                    datePart = datePart + splitDate[n];
                }
            }
            referenceNo = referenceNo + datePart + loanId;
            return referenceNo;
        } catch (Exception e) {
            return "";
        }

    }

}

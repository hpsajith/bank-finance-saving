package com.ites.bankfinance.daoImpl;

import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.encription.Encription;
import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.MCompany;
import com.ites.bankfinance.model.UmSpecialPassword;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserLog;
import com.ites.bankfinance.model.UmUserRole;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public UmUser findByUserName(String username) {
        List<UmUser> users = new ArrayList<UmUser>();
        users = sessionFactory.getCurrentSession().createQuery("from UmUser where userName=?")
                .setParameter(0, username).list();
        if (users.size() > 0) {
            return users.get(0);
        } else {
            return null;
        }

    }

    @Override
    public Set<UmUserRole> getUserRole(String username) {
        List<UmUserRole> userRoles = new ArrayList<UmUserRole>();

        userRoles = sessionFactory.getCurrentSession().createQuery("from UmUserRole where userName=?")
                .setParameter(0, username).list();
        if (userRoles.size() > 0) {
            Set<UmUserRole> user_roles = new HashSet<UmUserRole>(userRoles);
            return user_roles;
        } else {
            return null;
        }
    }

    @Override
    public int findByUserName() {
        int userId = 0;
        Session session = sessionFactory.openSession();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserDetails user = (UserDetails) authentication.getPrincipal();
            String username = user.getUsername();
            UmUser umUser;
            umUser = (UmUser) session.createCriteria(UmUser.class)
                    .add(Restrictions.eq("userName", username))
                    .add(Restrictions.eq("isActive", true)).uniqueResult();

//            umUser = (UmUser) sessionFactory.getCurrentSession().createCriteria(UmUser.class)
//                    .add(Restrictions.eq("userName", username))
//                    .add(Restrictions.eq("isActive", true)).uniqueResult();//Annr

            //String user_id = query.uniqueResult().toString();
            if (umUser != null) {
                userId = umUser.getUserId();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return userId;
    }

    @Override
    public String findWorkingUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails user = (UserDetails) authentication.getPrincipal();
        String username = user.getUsername();
        return username;
    }

    @Override
    public int findUserTypeByUserId(int userId) {
        int userType = 0;
        Session session = sessionFactory.openSession();
        try {
            String sql = "select userTypeId from UmUser where userId=" + userId;
            String u_type = session.createQuery(sql).uniqueResult().toString();
//            String u_type = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult().toString();//Annr
            userType = Integer.parseInt(u_type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return userType;
    }

    @Override
    public List findApproveLevelByUser(int userId) {
        Session session = sessionFactory.openSession();
        List appLevel = null;
        try {
            String sql = "from UmUserTypeApproval where userId=" + userId;
            appLevel = session.createQuery(sql).list();
//            appLevel = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return appLevel;

    }

    @Override
    public List findLoanTypesByUserId(int userId) {
        List appLevel = null;
        Session session = sessionFactory.openSession();
        try {
//            String sql = "from UmUserTypeLoan where userType=" + userId;
            String sql = "from UmUserTypeLoan " ;
            appLevel = session.createQuery(sql).list();
//            appLevel = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
        } catch (Exception e) {
             e.printStackTrace();
        }
        session.clear();
        session.close();
        return appLevel;

    }

    @Override
    public Employee findEmployeeById(int empId) {
        Employee emp = null;
        try {
            String sql = "from Employee where empNo=" + empId;
            emp = (Employee) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emp;

    }

    @Override
    public int findEmployeeByUserId(int userId) {
        UmUser usr = null;
        Session session = sessionFactory.openSession();
        int empNo = 0;
        try {
            String sql = "from UmUser where userId=" + userId;
            usr = (UmUser) session.createQuery(sql).uniqueResult();
//            usr = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();//Annr
            if (usr != null) {
                empNo = usr.getEmpId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return empNo;

    }

    @Override
    public String findApprovalsByPassword(String password) {
        String hasApprove = "";
        try {

            Encription encrypt = new Encription();
            String encodePassword = encrypt.encryptPassword(password);

            System.out.println("encodePassword" + encodePassword);

            String sql = "from UmSpecialPassword where userPassword='" + encodePassword + "'";
            List userList = sessionFactory.getCurrentSession().createQuery(sql).list();
            if (userList.size() > 0) {
                for (int i = 0; i < userList.size(); i++) {
                    UmSpecialPassword user = (UmSpecialPassword) userList.get(i);
                    int userId = user.getUserId();
                    String sql1 = "from UmUserTypeApproval where userId=" + userId;
                    List userAppr = sessionFactory.getCurrentSession().createQuery(sql1).list();
                    if (userAppr.size() > 0) {
                        hasApprove = "yes";
                        break;
                    } else {
                        hasApprove = "noApp";
                    }
                }
            } else {
                hasApprove = "no";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasApprove;
    }

    @Override
    public boolean findLoggedUserApproval() {
        boolean hasApprove = false;
        try {
            int userId = findByUserName();
            String sql1 = "from UmUserTypeApproval where userId=" + userId;
            List userAppr = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (userAppr.size() > 0) {
                hasApprove = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasApprove;
    }

    @Override
    public String findWorkingBranchName(int branchID) {
        String userName = null;
        try {
            String sql = "select b.branchName from MBranch as b where b.branchId = " + branchID;
            userName = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult().toString();
            return userName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public MCompany findCompanyDetails() {
        MCompany company = null;
        try {
            company = (MCompany) sessionFactory.getCurrentSession().createCriteria(MCompany.class)
                    .add(Restrictions.eq("comId", 1)).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return company;
    }

    @Override
    public int findUserWorkingBranch(int userId, int branchId) {
        // String sql1 = "from UmUserLog where userId='" + userId + "' and branchId = '" + branchId + "' order by pk desc ";
        try {
            UmUserLog obj;
            obj = (UmUserLog) sessionFactory.getCurrentSession().createCriteria(UmUserLog.class)
                    .add(Restrictions.eq("userId", userId))
                    .add(Restrictions.eq("branchId", branchId)).addOrder(Order.desc("pk")).setMaxResults(1).uniqueResult();
            if (obj != null) {
                branchId = obj.getBranchId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchId;
    }

    @Override
    public int findUserType(int userId) {
        int userType = 0;
        try {
            UmUser user = (UmUser) sessionFactory.getCurrentSession().createCriteria(UmUser.class).add(Restrictions.eq("userId", userId)).uniqueResult();
            userType = user.getUserTypeId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userType;
    }

}

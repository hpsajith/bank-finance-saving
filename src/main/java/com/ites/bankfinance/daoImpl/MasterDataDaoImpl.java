/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.daoImpl;

import com.bankfinance.form.JobDefine;
import com.bankfinance.form.OtherChargseModel;
import com.ites.bankfinance.dao.MasterDataDao;
import com.ites.bankfinance.dao.UserDao;
import com.ites.bankfinance.model.AMaintab;
import com.ites.bankfinance.model.ASubtab;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MPeriodTypes;
import com.ites.bankfinance.model.MRateType;
import com.ites.bankfinance.model.MSection;
import com.ites.bankfinance.model.MSubLoanOtherChargers;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.MSupplier;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserLog;
import com.ites.bankfinance.model.VehicleMake;
import com.ites.bankfinance.model.VehicleModel;
import com.ites.bankfinance.model.VehicleType;
import com.ites.bankfinance.savingsModel.MSavingsType;
import com.ites.bankfinance.savingsModel.MSubSavingsType;
import com.ites.bankfinance.service.UserService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class MasterDataDaoImpl implements MasterDataDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private SessionFactory savingsSessionFactory;


    @Autowired
    private UserDao userDao;

    @Autowired
    UserService userService;

    @Override
    public List findLoanTypes() {
        List loanTypes = null;
        try {
            String sql = "";
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    sql = "from MLoanType where loanTypeId<>1";
                } else {
                    sql = "from MLoanType where loanTypeId<>1 and isActive = 1";
                }
            }
            loanTypes = sessionFactory.getCurrentSession().createQuery(sql).list();
            return loanTypes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List findSubLoanTypes(int loanType) {
        List loanTypes = null;
        try {
            String sql = "";
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    sql = "from MSubLoanType where loanTypeId=" + loanType;
                } else {
                    sql = "from MSubLoanType where loanTypeId=" + loanType + " and isActive = 1";
                }
            }
            loanTypes = sessionFactory.getCurrentSession().createQuery(sql).list();
            return loanTypes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List findOthercharges() {
        List loanTypes = null;
        try {
            String sql = "from MSubTaxCharges";
            loanTypes = sessionFactory.getCurrentSession().createQuery(sql).list();
            return loanTypes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List findUsersByUserType(int userType) {

        List users = null;
        try {
            String sql = "select e.empNo, e.empFname, e.empLname, e.addres1, e.phone1 from UmUser as u, Employee as e where u.empId=e.empNo and u.userTypeId=" + userType + " and e.isActive=1 and e.isDelete=0";
            users = sessionFactory.getCurrentSession().createQuery(sql).list();
            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List findUsersByApproval(int appId, int subLoanId) {
        String sql1 = "";
        List users = null;
        try {

            if (subLoanId == 0) {
                sql1 = " SELECT emp.empFname, emp.empLname, usr.userId, emp.empNo FROM UmUserTypeApproval AS app, UmUser AS usr, Employee AS emp WHERE"
                        + " usr.empId=emp.empNo AND usr.userId=app.userId AND usr.userTypeId != 18 AND app.MApproveLevels.appId=" + appId;
            } else if (appId == 0) {
                sql1 = " SELECT emp.empFname, emp.empLname, usr.userId, emp.empNo FROM UmUser AS usr, UmUserType AS ut, Employee AS emp WHERE"
                        + " usr.empId=emp.empNo AND usr.userTypeId = ut.userTypeId AND ut.userTypeId = 8"
                        + " AND usr.userId IN( SELECT l.userId FROM UmUserTypeLoan as l"
                        + " WHERE l.MSubLoanType.subLoanId=" + subLoanId + ")";
            } else {
                sql1 = " SELECT emp.empFname, emp.empLname, usr.userId, emp.empNo FROM UmUserTypeApproval AS app, UmUser AS usr, Employee AS emp WHERE"
                        + " usr.empId=emp.empNo AND usr.userId=app.userId AND app.MApproveLevels.appId=" + appId + "AND usr.userTypeId != 18"
                        + " AND usr.userId IN( SELECT l.userId FROM UmUserTypeLoan as l "
                        + " WHERE l.MSubLoanType.subLoanId=" + subLoanId + ")";
            }
            users = sessionFactory.getCurrentSession().createQuery(sql1).list();
            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<MSupplier> findSuppliers() {
        List<MSupplier> returnList = new ArrayList();
//        List suppliers = null;
        try {
            String sql = "from MSupplier where isActive=1 and isDelete=0";
            returnList = sessionFactory.getCurrentSession().createQuery(sql).list();
//            for (int i = 0; i < suppliers.size(); i++) {
//                MSupplier msupllier = (MSupplier) suppliers.get(i);
//                String[] obj = new String[4];
//                obj[0] = String.valueOf(msupllier.getSupplierId());
//                obj[1] = msupllier.getSupplierName();
//                obj[2] = "";
//                obj[3] = msupllier.getSupplierAddress();
//                returnList.add(obj);
//            }
            return returnList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public List findCheckList(int loanType) {
        List checkList = new ArrayList();
        try {
//            String sql1 = "from MLoanType where loanTypeId=" + loanType;
//            MLoanType loanTypeObj = (MLoanType) sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult();

            String sql = "from MSubLoanChecklist as sl where sl.MSubLoanType.subLoanId=" + loanType;
            checkList = sessionFactory.getCurrentSession().createQuery(sql).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return checkList;
    }

    @Override
    public String findCurrencyType() {
        String currencyName = null;
        try {
            String sql = "select h.currencySign from MCurrencyD as d, MCurrencyH as h where d.curId=1 and d.currencyHId=h.currencyId";
            currencyName = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult().toString();
            return currencyName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override

    public double findCurrencyPrice() {
        String currencyPrice = null;
        double price = 0.00;
        try {
            String sql = "select d.currencyPrice from MCurrencyD as d, where d.curId=1";
            currencyPrice = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult().toString();
            price = Double.parseDouble(currencyPrice);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return price;
    }

    @Override
    public String findPrinterName() {
        String printerName = "";
        try {
            String sql = "select printerName from MPrinter where id=1";
            Object ob = sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (ob != null) {
                printerName = ob.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return printerName;
    }

    @Override
    public int findLoanTypeByLoanId(int loanId) {
        int loanTypeId = 0;
        try {
            String sql = "from LoanHeaderDetails where loanId=" + loanId;
            LoanHeaderDetails loan = (LoanHeaderDetails) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (loan != null) {
                int subLoanId = loan.getLoanType();
                String sql1 = "select loanTypeId from MSubLoanType where subLoanId=" + subLoanId;
                String type = sessionFactory.getCurrentSession().createQuery(sql1).uniqueResult().toString();
                loanTypeId = Integer.parseInt(type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanTypeId;
    }

    public List findPeriodTypes() {
        List period = null;
        try {
            String sql = "from MPeriodTypes";
            period = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return period;
    }

    @Override
    public List findApprovalTypes() {
        List approval = null;
        Session session = sessionFactory.openSession();
        try {
            String sql = "from MApproveLevels";
            approval = session.createQuery(sql).list();
//            approval = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return approval;
    }

    @Override
    public List findExtraChargesBySubLoanId(int subLoanId) {
        List extraCharges = null;
        List<OtherChargseModel> extraChargesList = new ArrayList();
        try {
            String sql = "from MSubLoanOtherChargers as mm where mm.MSubLoanType.subLoanId=" + subLoanId;
            extraCharges = sessionFactory.getCurrentSession().createQuery(sql).list();

            if (extraCharges.size() > 0) {
                for (int i = 0; i < extraCharges.size(); i++) {
                    MSubLoanOtherChargers mSubCharges = (MSubLoanOtherChargers) extraCharges.get(i);
                    OtherChargseModel subTax = new OtherChargseModel();
                    subTax.setId(mSubCharges.getMSubTaxCharges().getSubTaxId());
                    subTax.setDescription(mSubCharges.getMSubTaxCharges().getSubTaxDescription());
                    subTax.setIsPrecentage(mSubCharges.getMSubTaxCharges().getIsPrecentage());
                    subTax.setTaxRate(mSubCharges.getMSubTaxCharges().getSubTaxRate());
                    subTax.setAmount(mSubCharges.getMSubTaxCharges().getSubTaxValue());
                    subTax.setIsAdded(mSubCharges.isIsAdded());
                    extraChargesList.add(subTax);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return extraChargesList;
    }

    @Override
    public List findBankAccountsDetails() {

        List bankData = null;
        try {
            String sql = "from MBankDetails";
            bankData = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bankData;
    }

    @Override
    public List findBankAccountsDetails2() {
        Session session=sessionFactory.openSession();
        List bankData = null;
        try {
            String sql = "from ConfigChartofaccountBank";
            bankData = session.createQuery(sql).list();
//            bankData = sessionFactory.getCurrentSession().createQuery(sql).list();//Annr
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return bankData;
    }

    @Override
    public List findUserJobs() {

        List jobList = new ArrayList();

        try {
            int userId = userDao.findByUserName();
            int userType = userDao.findUserTypeByUserId(userId);

            if (userType > 0) {
                //predefined user
                if (userType == 18) { // super admin user
                    String sql1 = "from AMaintab";
                    List<AMaintab> mainJobs = sessionFactory.getCurrentSession().createQuery(sql1).list();
                    if (mainJobs != null) {
                        for (AMaintab mainJob : mainJobs) {
                            JobDefine job = new JobDefine();
                            int jobId = mainJob.getMainTabId();
                            String tabCode = mainJob.getMainTabCode();
                            String tabName = mainJob.getMainTabName();
                            String refPage = "";
                            if (mainJob != null) {
                                refPage = mainJob.getRefPage();
                            }
                            job.setMainTabId(jobId);
                            job.setMainCode(tabCode);
                            job.setMainTabName(tabName);
                            job.setRefPage(refPage);
                            job.setIsActive(mainJob.getIsActive());
                            String sql4 = "from ASubtab where mainTabId = " + mainJob.getMainTabId();
                            List<ASubtab> subTabs = sessionFactory.getCurrentSession().createQuery(sql4).list();
                            job.setSubList(subTabs);
                            jobList.add(job);
                        }
                    }
                } else { // normal user
                    String sql3 = "select m.mainTabId,m.mainTabCode,m.mainTabName,m.refPage from AJobDefine j, AMaintab m where j.refUserId=" + userType + " and m.mainTabId=j.mainTabId group by j.mainTabId order by m.orderId ASC";
                    List mainJobs = sessionFactory.getCurrentSession().createQuery(sql3).list();

                    if (mainJobs != null) {
                        for (int i = 0; i < mainJobs.size(); i++) {
                            Object[] obj = (Object[]) mainJobs.get(i);
                            JobDefine job = new JobDefine();
                            int jobId = Integer.parseInt(obj[0].toString());
                            String tabCode = obj[1].toString();
                            String tabName = obj[2].toString();
                            String refPage = "";
                            if (obj[3] != null) {
                                refPage = obj[3].toString();
                            }

                            job.setMainTabId(jobId);
                            job.setMainCode(tabCode);
                            job.setMainTabName(tabName);
                            job.setRefPage(refPage);
                            String sql4 = "select s.subTabNo, s.subTabName, s.subTabCode, s.refPage from AJobDefine aj, ASubtab s where aj.mainTabId=" + jobId + " and aj.refUserId=" + userType + " and aj.subTabId=s.subTabNo and s.isActive=1 and s.isDelete=0";
                            List subTabs = sessionFactory.getCurrentSession().createQuery(sql4).list();
                            List<ASubtab> subTabList = new ArrayList();
                            for (int j = 0; j < subTabs.size(); j++) {
                                Object[] obj2 = (Object[]) subTabs.get(j);
                                ASubtab sub = new ASubtab();
                                int subTabId = Integer.parseInt(obj2[0].toString());
                                String subTabName = obj2[1].toString();
                                String subTabCode = obj2[2].toString();
                                String subTabRefPage = "";
                                if (obj2[3] != null) {
                                    subTabRefPage = obj2[3].toString();
                                }
                                sub.setSubTabNo(subTabId);
                                sub.setSubTabName(subTabName);
                                sub.setSubTabCode(subTabCode);
                                sub.setRefPage(subTabRefPage);
                                subTabList.add(sub);
                            }

                            job.setSubList(subTabList);
                            jobList.add(job);
                        }
                    }
                }
            } else {
                //custom user
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jobList;
    }

    @Override
    public List<MSubLoanType> findPawningType() {
        List<MSubLoanType> pawningTypes = null;
        try {
            String sql = "from MSubLoanType where loanTypeId=" + 1;
            pawningTypes = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pawningTypes;
    }

    @Override
    public List findArticleTypes() {
        List articles = new ArrayList();
        try {
            String sql = "from MArticleTypes";
            articles = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return articles;
    }

    @Override
    public List findBranchesByUsername(String username, String password) {
        List<MBranch> branchList = new ArrayList();
        String sql1 = "";
        try {
            String sql = "from UmUser where userName='" + username + "'";
            UmUser user = (UmUser) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (user != null) {
                if (user.getUserTypeId() == 18) {
                    sql1 = "select b.branchId,b.branchName,b.branchLname from MBranch as b where um.isBlock=0 and b.isActive=1 and b.isDelete=0";
                } else {
                    sql1 = "select b.branchId,b.branchName,b.branchLname from UmUserBranch as um, MBranch as b where um.userId=" + user.getUserId() + " and um.isBlock=0 and b.branchId=um.branchId and b.isActive=1 and b.isDelete=0";
                }
                List branchIds = sessionFactory.getCurrentSession().createQuery(sql1).list();
                if (branchIds.size() > 0) {
                    for (int i = 0; i < branchIds.size(); i++) {
                        Object[] obj = (Object[]) branchIds.get(i);
                        MBranch mbranch = new MBranch();
                        mbranch.setBranchId(Integer.parseInt(obj[0].toString()));

                        if (obj[1] != null) {
                            mbranch.setBranchName(obj[1].toString());
                        } else {
                            mbranch.setBranchName("");
                        }

                        if (obj[2] != null) {
                            mbranch.setBranchLname(obj[2].toString());
                        } else {
                            mbranch.setBranchLname("");
                        }
                        branchList.add(mbranch);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchList;
    }

    @Override
    public void setUserLogin(int branchId) {
        int userId = userDao.findByUserName();
        Date dayEndDate = null;
        try {
            String sql = "SELECT MAX(dayEndDate) FROM DayEndDate WHERE branchId=" + branchId;
            Date obj = (Date) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
            if (obj != null) {
                dayEndDate = obj;
            }

            UmUserLog log = new UmUserLog();
            log.setBranchId(branchId);
            log.setUserId(userId);
            log.setUserActive(true);
            log.setLogDate(dayEndDate);
            sessionFactory.getCurrentSession().save(log);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void inactiveUser() {
        try {
            int userId = userDao.findByUserName();
            String sql = "FROM UmUserLog WHERE userId=" + userId + " ORDER BY pk DESC";
            UmUserLog userLog = (UmUserLog) sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();
            if (userLog != null) {
                userLog.setUserActive(false);
                sessionFactory.getCurrentSession().update(userLog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean checkUserIsBlock() {
        boolean isBlock = false;
        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
        try {
            int userId = userDao.findByUserName();
            String sql = "FROM UmUserLog WHERE userId=" + userId + " ORDER BY pk DESC";
            UmUserLog userLog = (UmUserLog)session.createQuery(sql).setMaxResults(1).uniqueResult();
//            UmUserLog userLog = (UmUserLog) sessionFactory.getCurrentSession().createQuery(sql).setMaxResults(1).uniqueResult();//Annr
            if (userLog != null) {
                isBlock = userLog.isUserBlock();
            }
            session.clear();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isBlock;
    }

    @Override
    public List findUsersByUserTypeAndBranch(int userType, int branchId) {
        List list = null;
        try {
            String sql = "SELECT DISTINCT e.empNo, e.empLname, e.phone2 FROM UmUser AS u, Employee AS e, UmUserBranch AS br , MBranch AS mbr WHERE "
                    + "u.empId=e.empNo AND u.userTypeId = " + userType + " AND e.isActive=1 AND br.branchId = " + branchId + " AND mbr.branchId = br.branchId";
            list = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Double findLoanRate(int loanType) {
        Double rate = 0.00;

        try {
            MSubLoanType mSubLoanType = (MSubLoanType) sessionFactory.getCurrentSession().createCriteria(MSubLoanType.class)
                    .add(Restrictions.eq("subLoanId", loanType)).uniqueResult();
            rate = mSubLoanType.getSubInterestRate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rate;
    }

    @Override
    public List<MBranch> findBranches() {
        List<MBranch> branchs = null;
        Session session = sessionFactory.openSession();
        try {
            branchs = session.createCriteria(MBranch.class
            ).add(Restrictions.eq("isActive", true)).list();

//            branchs = sessionFactory.getCurrentSession().createCriteria(MBranch.class
//            ).add(Restrictions.eq("isActive", true)).list();//Annr
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return branchs;
    }

    @Override
    public List<MRateType> findRateType() {
        List<MRateType> mRateTypes = null;

        try {
            mRateTypes = sessionFactory.getCurrentSession().createCriteria(MRateType.class
            ).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mRateTypes;

    }

    @Override
    public List<VehicleType> findVehicleType() {
        List<VehicleType> vehicleTypes = null;

        try {
            vehicleTypes = sessionFactory.getCurrentSession().createCriteria(VehicleType.class
            )
                    .add(Restrictions.eq("isActive", true)).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleTypes;
    }

    @Override
    public List<VehicleMake> findVehicleMake(int vehicleType) {
        List<VehicleMake> vehicleTypes = null;

        try {
            vehicleTypes = sessionFactory.getCurrentSession().createCriteria(VehicleMake.class)
                    .add(Restrictions.eq("isActive", true))
                    .add(Restrictions.eq("vehicleType.vehicleType", vehicleType)).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleTypes;
    }

    @Override
    public List<VehicleModel> findVehicleModel(int vehicleType, int vehicleMake) {
        List<VehicleModel> vehicleTypes = null;

        try {
            vehicleTypes = sessionFactory.getCurrentSession().createCriteria(VehicleModel.class)
                    .add(Restrictions.eq("isActive", true))
                    .add(Restrictions.eq("vehicleType.vehicleType", vehicleType))
                    .add(Restrictions.eq("vehicleMake.vehicleMake", vehicleMake)).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleTypes;
    }

    @Override
    public List<VehicleMake> findVehicleMake() {
        List<VehicleMake> vehicleMakes = null;
        try {
            String sql = "from VehicleMake";
            vehicleMakes = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicleMakes;
    }

    @Override
    public List<VehicleModel> findVehicleModel() {
        List<VehicleModel> vehicleModels = null;
        try {
            String sql = "from VehicleModel";
            vehicleModels = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicleModels;
    }

    @Override
    public VehicleMake findVehicleMakeById(int vehicleMakeId) {
        VehicleMake vehicleMake = null;
        try {
            String sql = "from VehicleMake where vehicleMake = " + vehicleMakeId;
            vehicleMake = (VehicleMake) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicleMake;
    }

    @Override
    public VehicleModel findVehicleModelById(int vehicleModelId) {
        VehicleModel vehicleModel = null;
        try {
            String sql = "from VehicleModel where vehicleModel = " + vehicleModelId;
            vehicleModel = (VehicleModel) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicleModel;
    }

    @Override
    public VehicleType findVehicleTypeById(int vehicleTypeId) {
        VehicleType vehicleType = null;
        try {
            String sql = "from VehicleType where vehicleType = " + vehicleTypeId;
            vehicleType = (VehicleType) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicleType;
    }

    @Override
    public MPeriodTypes findPeriodType(int periodTypeId) {
        MPeriodTypes mPeriodType = null;
        try {
            String sql = "from MPeriodTypes where typeId = " + periodTypeId;
            mPeriodType = (MPeriodTypes) sessionFactory.getCurrentSession().createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return mPeriodType;
    }

    @Override
    public MBranch findBranch(int branchId) {
        MBranch branch = null;
        try {
            String sql = "from MBranch where branchId = :branchId";
            branch = (MBranch) sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("branchId", branchId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return branch;
    }

    @Override
    public List<MSection> findSections() {
        List<MSection> sections = null;
        try {
            String sql = "from MSection";
            sections = sessionFactory.getCurrentSession().createQuery(sql).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return sections;
    }

    @Override
    public UmUser authenticate(String userName, String password) {
        UmUser user = null;
        try {
            String sql = "from UmUser where userName = :userName";
            user = (UmUser) sessionFactory.getCurrentSession().createQuery(sql)
                    .setParameter("userName", userName).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public List<MBranch> loadUserBranches(int userId, int sectionId) {
        List<MBranch> branchList = new ArrayList();
        String sql1 = "";
        try {
            int userType = userService.findUserType(userId);
            if (userType == 18) {
                sql1 = "select b.branchId,b.branchName,b.branchLname from MBranch as b where b.isActive=1 and b.isDelete=0 and b.MSection.id = " + sectionId;
            } else {
                sql1 = "select b.branchId,b.branchName,b.branchLname from UmUserBranch as um, MBranch as b where um.userId=" + userId + " and um.isBlock=0 and b.branchId=um.branchId and b.isActive=1 and b.isDelete=0 and b.MSection.id = " + sectionId;
            }
            List branchIds = sessionFactory.getCurrentSession().createQuery(sql1).list();
            if (branchIds.size() > 0) {
                for (Object branchId : branchIds) {
                    Object[] obj = (Object[]) branchId;
                    MBranch mbranch = new MBranch();
                    mbranch.setBranchId(Integer.parseInt(obj[0].toString()));
                    if (obj[1] != null) {
                        mbranch.setBranchName(obj[1].toString());
                    } else {
                        mbranch.setBranchName("");
                    }
                    if (obj[2] != null) {
                        mbranch.setBranchLname(obj[2].toString());
                    } else {
                        mbranch.setBranchLname("");
                    }
                    branchList.add(mbranch);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return branchList;
    }

    @Override
    public Chartofaccount findAccount(String accountNo) {
        Chartofaccount chartofaccount = null;
        try {
            String sql = "from Chartofaccount where accountNo = :accountNo";
            chartofaccount = (Chartofaccount) sessionFactory.getCurrentSession().createQuery(sql).
                    setParameter("accountNo", accountNo).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return chartofaccount;
    }

    public List<MSavingsType> findAllSavingsTypes(){
        List<MSavingsType> savingsType=null;
        try{
            String sql="from MSavingsType";
            savingsType= savingsSessionFactory.openSession().createQuery(sql).list();
//            savingsSessionFactory.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return savingsType;
    }

    @Override
    public MSavingsType findAllSavingsTypesById(int savingsId){
        MSavingsType savingsType=null;
        try{
            String sql="from MSavingsType where savingsTypeId='"+savingsId+"'";
            savingsType= (MSavingsType) savingsSessionFactory.openSession().createQuery(sql).uniqueResult();
        }catch(Exception e){
            e.printStackTrace();
        }
        return savingsType;
    }

    public List<MSubSavingsType> findAllSubSavingsTypes(){
        List<MSubSavingsType> subSavingsType=null;
        try{
            String sql="from MSubSavingsType";
            subSavingsType= savingsSessionFactory.openSession().createQuery(sql).list();
            System.out.println("subSavingsType="+subSavingsType);
//            savingsSessionFactory.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return subSavingsType;
    }

    @Override
    public MSubSavingsType findAllSubSavingsTypesById(int subSavingsId){
        MSubSavingsType subSavingsType=null;
        try{
            String sql="from MSubSavingsType where subSavingsId='"+subSavingsId+"'";
            subSavingsType= (MSubSavingsType)savingsSessionFactory.openSession().createQuery(sql).uniqueResult();
            savingsSessionFactory.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return subSavingsType;
    }

    @Override
    public List findSubSavingsTypes(int savingsType) {
        List<MSubSavingsType> savingsTypes = null;
        try {
            String sql = "";
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    sql = "from MSubSavingsType where subSavingsId=" + savingsType;
                } else {
                    sql = "from MSubSavingsType where subSavingsId=" + savingsType + " and isActive = 1";
                }
            }
            savingsTypes = (List<MSubSavingsType>)savingsSessionFactory.openSession().createQuery(sql).list();
            return savingsTypes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

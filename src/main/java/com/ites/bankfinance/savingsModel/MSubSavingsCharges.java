package com.ites.bankfinance.savingsModel;
// Generated May 21, 2018 10:02:07 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MSubSavingsCharges generated by hbm2java
 */
@Entity
@Table(name="m_sub_savings_charges"
    ,catalog="axa_bank_savings"
)
public class MSubSavingsCharges  implements java.io.Serializable {


     private Integer subSavingsChargesId;
     private String subTaxDescription;
     private String subTaxAccountNo;
     private Double subTaxRate;
     private Double subTaxValue;
     private Boolean isPrecentage;
     private String commisionAccountNo;
     private Double commision;
     private Integer userId;
     private Integer branchId;
     private Date actionTime;
     private Set MSubsavingsChargesMappings = new HashSet(0);

    public MSubSavingsCharges() {
    }

    public MSubSavingsCharges(String subTaxDescription, String subTaxAccountNo, Double subTaxRate, Double subTaxValue, Boolean isPrecentage, String commisionAccountNo, Double commision, Integer userId, Integer branchId, Date actionTime, Set MSubsavingsChargesMappings) {
       this.subTaxDescription = subTaxDescription;
       this.subTaxAccountNo = subTaxAccountNo;
       this.subTaxRate = subTaxRate;
       this.subTaxValue = subTaxValue;
       this.isPrecentage = isPrecentage;
       this.commisionAccountNo = commisionAccountNo;
       this.commision = commision;
       this.userId = userId;
       this.branchId = branchId;
       this.actionTime = actionTime;
       this.MSubsavingsChargesMappings = MSubsavingsChargesMappings;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="SubSavings_Charges_Id", unique=true, nullable=false)
    public Integer getSubSavingsChargesId() {
        return this.subSavingsChargesId;
    }
    
    public void setSubSavingsChargesId(Integer subSavingsChargesId) {
        this.subSavingsChargesId = subSavingsChargesId;
    }

    
    @Column(name="Sub_Tax_Description", length=100)
    public String getSubTaxDescription() {
        return this.subTaxDescription;
    }
    
    public void setSubTaxDescription(String subTaxDescription) {
        this.subTaxDescription = subTaxDescription;
    }

    
    @Column(name="Sub_Tax_AccountNo", length=15)
    public String getSubTaxAccountNo() {
        return this.subTaxAccountNo;
    }
    
    public void setSubTaxAccountNo(String subTaxAccountNo) {
        this.subTaxAccountNo = subTaxAccountNo;
    }

    
    @Column(name="Sub_Tax_Rate", precision=5)
    public Double getSubTaxRate() {
        return this.subTaxRate;
    }
    
    public void setSubTaxRate(Double subTaxRate) {
        this.subTaxRate = subTaxRate;
    }

    
    @Column(name="Sub_Tax_Value", precision=10)
    public Double getSubTaxValue() {
        return this.subTaxValue;
    }
    
    public void setSubTaxValue(Double subTaxValue) {
        this.subTaxValue = subTaxValue;
    }

    
    @Column(name="IsPrecentage")
    public Boolean getIsPrecentage() {
        return this.isPrecentage;
    }
    
    public void setIsPrecentage(Boolean isPrecentage) {
        this.isPrecentage = isPrecentage;
    }

    
    @Column(name="Commision_AccountNo", length=20)
    public String getCommisionAccountNo() {
        return this.commisionAccountNo;
    }
    
    public void setCommisionAccountNo(String commisionAccountNo) {
        this.commisionAccountNo = commisionAccountNo;
    }

    
    @Column(name="Commision", precision=10)
    public Double getCommision() {
        return this.commision;
    }
    
    public void setCommision(Double commision) {
        this.commision = commision;
    }

    
    @Column(name="User_Id")
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    
    @Column(name="Branch_Id")
    public Integer getBranchId() {
        return this.branchId;
    }
    
    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="Action_Time", length=19)
    public Date getActionTime() {
        return this.actionTime;
    }
    
    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

//@OneToMany(fetch=FetchType.LAZY, mappedBy="MSubSavingsCharges")
//    public Set getMSubsavingsChargesMappings() {
//        return this.MSubsavingsChargesMappings;
//    }
//
//    public void setMSubsavingsChargesMappings(Set MSubsavingsChargesMappings) {
//        this.MSubsavingsChargesMappings = MSubsavingsChargesMappings;
//    }




}



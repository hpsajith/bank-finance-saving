/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.savingsModel;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "m_saving_account_header")
@NamedQueries({
    @NamedQuery(name = "MSavingAccountHeader.findAll", query = "SELECT m FROM MSavingAccountHeader m")})
public class MSavingAccountHeader implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Saving_Acc_Id")
    private Integer savingAccId;
    @Column(name = "Debtor_Id")
    private Integer debtorId;
    @Column(name = "Saving_Type_Id")
    private Integer savingTypeId;
    @Column(name = "Saving_Sub_Type_Id")
    private Integer savingSubTypeId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Saving_Rate")
    private Double savingRate;
    @Column(name = "Saving_Amount")
    private Double savingAmount;
    @Column(name = "Branch_Id")
    private Integer branchId;
    @Size(max = 20)
    @Column(name = "Book_No")
    private String bookNo;
    @Size(max = 15)
    @Column(name = "System_Date")
    private String systemDate;
    @Size(max = 15)
    @Column(name = "Action_Time")
    private String actionTime;

    public MSavingAccountHeader() {
    }

    public MSavingAccountHeader(Integer savingAccId) {
        this.savingAccId = savingAccId;
    }

    public Integer getSavingAccId() {
        return savingAccId;
    }

    public void setSavingAccId(Integer savingAccId) {
        this.savingAccId = savingAccId;
    }

    public Integer getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(Integer debtorId) {
        this.debtorId = debtorId;
    }

    public Integer getSavingTypeId() {
        return savingTypeId;
    }

    public void setSavingTypeId(Integer savingTypeId) {
        this.savingTypeId = savingTypeId;
    }

    public Integer getSavingSubTypeId() {
        return savingSubTypeId;
    }

    public void setSavingSubTypeId(Integer savingSubTypeId) {
        this.savingSubTypeId = savingSubTypeId;
    }

    public Double getSavingRate() {
        return savingRate;
    }

    public void setSavingRate(Double savingRate) {
        this.savingRate = savingRate;
    }

    public Double getSavingAmount() {
        return savingAmount;
    }

    public void setSavingAmount(Double savingAmount) {
        this.savingAmount = savingAmount;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getBookNo() {
        return bookNo;
    }

    public void setBookNo(String bookNo) {
        this.bookNo = bookNo;
    }

    public String getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(String systemDate) {
        this.systemDate = systemDate;
    }

    public String getActionTime() {
        return actionTime;
    }

    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (savingAccId != null ? savingAccId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MSavingAccountHeader)) {
            return false;
        }
        MSavingAccountHeader other = (MSavingAccountHeader) object;
        if ((this.savingAccId == null && other.savingAccId != null) || (this.savingAccId != null && !this.savingAccId.equals(other.savingAccId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ites.bankfinance.model.MSavingAccountHeader[ savingAccId=" + savingAccId + " ]";
    }
    
}

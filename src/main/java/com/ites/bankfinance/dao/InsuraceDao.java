/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.dao;

import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.InsuranceCompany;
import com.ites.bankfinance.model.InsuranceOtherCharge;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MSubLoanType;
import java.util.Date;
import java.util.List;

/**
 *
 * @author MsD
 */
public interface InsuraceDao {

    public Boolean saveOrUpdateUserInsuraceCompany(InsuranceCompany formData);

    public List<InsuranceCompany> findInsuranceCompanyes();

    public InsuranceCompany findInsuranceCompanyID(int comId);

    public Boolean inActiveCompany(int comID);

    public Boolean activeCompany(int comID);

    public List findLoanHeaderDetails();

    public InsuranceOtherCharge findInsOtherCharge();

    public LoanHeaderDetails findLoanDetails(int loanId);

    public MSubLoanType findSubLoanType(int loanId);

    public Boolean processInsurace(InsuraceProcessMain insurance);

    public List findProcessLoan();

    public InsuraceProcessMain findInsuranceDetails(int insuId);

    public List findRenewInsurance();

    public List<MSubLoanType> findSubLoanType();

    public List<LoanHeaderDetails> findLoanDetails();

    public Boolean saveOrUpdateOtherCustomers(InsuranceOtherCustomers otherCustomers);

    public List<InsuranceOtherCustomers> findOtherCustomers();

    public InsuranceOtherCustomers findInsuranceOtherCustomer(int Id);

    public List findOtherInsurance();

    public InsuraceProcessMain findOtherInsurance(int insuId);

    public List<MBranch> findBranches();

    public Boolean registedInsurance(int insuId);

    public boolean generateInsuranceVoucher(Integer[] loans);

    public List findRegistedLoan();

    public Boolean saveDiliverType(int insuId, int diliverType);

    public InsuraceProcessMain findDiluverInsurance(int insuId);

    public List findInsuranceByAgreementNo(String agreementNo);

    public Date findDayEndDate();

    public InsuraceProcessMain findByLoanId(int loanId);

    public boolean addInsuraceCommission(InsuraceProcessMain insurance);

    public List findInsuranceByAgreementNo(String agreementNo, int status);

    public List findInsuranceByPolicyNo(String policyNo, int status);

    public List findInsuranceByVehicleNo(String vehicleNo, int status);

    public List findInsuranceByDebtorName(String debtorName, int status);

    public Boolean saveInsuranceDebitProcess(InsuraceProcessMain insurance);

    public Boolean saveOrUpdateInsuranceProcessDelete(InsuraceProcessMain insuraceProcessMain);

    public Boolean deleteInsuranceProcessMainByInsuId(int insuId, String insuCode, int loanId);

}

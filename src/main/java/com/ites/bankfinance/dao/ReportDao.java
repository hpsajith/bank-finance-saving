/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.dao;

import com.bankfinance.form.ActiveContracts;
import com.bankfinance.form.PortfolioDetail;
import com.bankfinance.form.RmvReceivable;
import com.bankfinance.form.ServiceChargeReceivable;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.SettlementCharges;
import java.util.List;

public interface ReportDao {

    public LoanHeaderDetails findByLoanDetails(String agreeNo);

    public List<SettlementCharges> findBySettlementChargees(int loanID);

    public Double findLoanBalance(int loanID);

    public String findByInsuCompanyName(Integer insuComId);

    public Double findTotalPayments(int loanID);

    public List<Chartofaccount> findChartofaccountList();

    public PortfolioDetail findPortfolioDetail(String agerementNo, String user_Name, String branch_Name);

    public double getNoOfRentalInArrears(int loanId);

    public Double[] getArrearsAmounts(int loanId);

    public Double getOdiArrearsAmount(int loanId);

    public Double getOtherChargersArrearsAmount(int loanId);

    public Double[] getLoanFutureDetails(int loanId);

    public PortfolioDetail findPortfolioDetail(int branchId);

    public PortfolioDetail findPortfolioDetailByOfficerId(int officerId);

    public PortfolioDetail findPortfolioDetailByDueDay(int dueDay);

    public PortfolioDetail findPortfolioDetailByInitiateDate(String initiateFromDate, String initiateToDate);

    public PortfolioDetail findPortfolioDetailByLoanAmount(String loanAmount, String lagValue);

    public PortfolioDetail findPortfolioDetailByGrossRental(String grossRental, String grValue);

    public List<ActiveContracts> getActiveContract(String date1, String date2);

    public List<ActiveContracts> getNotActiveContract(String date1, String date2);

    public List<RmvReceivable> getRmvReceivableRecords(String date1, String date2);

    public List<ServiceChargeReceivable> getServiceChargeReceivable(String date1, String date2);

    public PortfolioDetail findPortfolioDetailByLoanType(int loanType);
}

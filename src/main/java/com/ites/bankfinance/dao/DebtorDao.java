/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.dao;

import com.bankfinance.form.DebtorForm;
import com.bankfinance.form.GroupCustomerForm;
import com.ites.bankfinance.model.*;

import java.io.File;
import java.util.List;

public interface DebtorDao {

    public int saveDebtorDetails(DebtorForm loanform);

//  public int saveBasicLoanDetails(LoanBasicDetails lbd);
    public void saveLoanEmploymentDetails(DebtorEmploymentDetails led);

    public void saveLoanBusinessDetails(DebtorBusinessDetails led);

    public void saveLoanDependentDetails(DebtorDependentDetails ldd);

    public void saveLoanAssessBankDetails(DebtorAssessBankDetails labd);

    public void saveLoanAssessVehicleDetails(DebtorAssessVehicleDetails lavd);

    public void saveLoanAssessLandDetails(DebtorAssessRealestateDetails lard);

    public void saveLoanLiabilityDetails(DebtorLiabilityDetails lld);

    public DebtorForm findDebtorOtherDetails(int debtorId);

    public List<LoanHeaderDetails> findDebtorLoanAppStatus(int id);

    public DebtorDependentDetails findDependent(int depedentId);

    public DebtorAssessRealestateDetails findAssetLand(int id);

    public DebtorAssessVehicleDetails findAssetVehicle(int id);

    public DebtorAssessBankDetails findAssetBank(int id);

    public DebtorLiabilityDetails findLiability(int id);

    public DebtorBusinessDetails findBusiness(int id);

    public DebtorEmploymentDetails findEmployment(int id);

    public void updateDebtorDetails(DebtorHeaderDetails debHeadDetails);

    public boolean saveCustomerProfilePicture(int customerId, String filePath);

    public File findProfilePicById(int cusId);

    public int findMaxCustomerId();

    public List<DebtorHeaderDetails> findDebtorBuNic(String nicNo);

    public List<DebtorHeaderDetails> findDebtors(boolean isDebtor);

    public List<DebtorHeaderDetails> findDebtors(boolean isDebtor, boolean isGenerate);

    public DebtorHeaderDetails findDebtor(int debtorId);

    public List<String> findDebtorNicNo(String pattern);

    public List<DebtorHeaderDetails> findDebtorByName(String name);

    public List<DebtorHeaderDetails> findDebtorByAccNo(String accNo);

    public boolean saveGroupDebtor(GroupCustomerForm customerForm);
    
    public DebtorHeaderDetails findDebtorByMemberNo(String memberNo, Object bID);

}

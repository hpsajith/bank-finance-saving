package com.ites.bankfinance.dao;

import com.bankfinance.form.SavingNewAccForm;
import com.ites.bankfinance.savingsModel.*;

import java.util.List;

/**
 * Created by AXA-FINANCE on 5/21/2018.
 */
public interface SavingsDao {

    public Boolean saveOrUpdateSavingsType(MSavingsType formData);

    public Boolean saveOrUpdateSubSavingsType(MSubSavingsType formData);

    public List<MSavingsType> findSavingType();

    public List<MSubSavingsType>  findSubSavingType();

    public List<MSubSavingsCharges>  findSavingCharges();

    int saveNewSavingsData(SavingNewAccForm formData);
}

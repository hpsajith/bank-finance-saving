package com.ites.bankfinance.dao;

import com.ites.bankfinance.model.Announcement;
import com.ites.bankfinance.model.LoanMessage;
import com.ites.bankfinance.model.UserMessage;
import java.util.Date;
import java.util.List;

public interface UtilityDao {

    public boolean saveOrUpdateAnnouncement(Announcement announcement);

    public List<Announcement> findAnnouncements();

    public List<Announcement> findAnnouncements(int userTypeId);

    public List<Announcement> findAnnouncements(Date publishDate);

    public List<Announcement> findAnnouncements(int userTypeId, Date publishDate);

    public Announcement findAnnouncement(int announcementId);

    public boolean deleteAnnouncement(int announcementId);

    public List<Announcement> findPreviousAnnouncements(Date publishDate);

    public List<Announcement> findPreviousAnnouncements(int userTypeId, Date publishDate);

    public boolean saveOrUpdateUserMessage(UserMessage userMessage);

    public List<UserMessage> findUserMessages();

    public List<UserMessage> findUserMessages(int userId);

    public List<UserMessage> findUserMessages(Date publishDate);

    public List<UserMessage> findUserMessages(int userId, Date publishDate);

    public List<UserMessage> findUserMessages(Date sDate, Date eDate);

    public UserMessage findUserMessage(int userMessageId);

    public boolean readUserMessage(int userMessageId);

    public boolean deleteUserMessage(int userMessageId);

    public boolean saveOrUpdateLoanMessage(LoanMessage loanMessage);

    public List<LoanMessage> findLoanMessages();

    public List<LoanMessage> findLoanMessages(int loanId);

    public List<LoanMessage> findLoanMessages(Date publishDate);

    public List<LoanMessage> findLoanMessages(Date sDate, Date eDate);

    public LoanMessage findLoanMessage(int loanMessageId);

    public boolean deleteLoanMessage(int loanMessageId);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.dao;

import com.ites.bankfinance.model.DayEndDate;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Uththara
 */
public interface DayEndDao {
    
    public String dayEndProcess(int branchId);

    public List findConfigJobs();

    public String getLastDayEnd(int branchId);
    
    public String correctPosting(String sDate, String eDate);
    
    public String findLoanOverPayment(int branchId);
    
    public boolean postingCheque(String code, int id);

    public boolean calLapsDate();

    public boolean calNullInstallment();

    public Double findBalanceWithOverPay(int printLoanId);
    
    public boolean calculateBalances();

    public List<DayEndDate> dayEndProcessBranches();
}

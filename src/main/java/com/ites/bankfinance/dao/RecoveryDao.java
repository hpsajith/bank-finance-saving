/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.dao;

import com.bankfinance.form.ArriesLoan;
import com.bankfinance.form.DirectDebitCharge;
import com.bankfinance.form.Mail;
import com.bankfinance.form.RecoveryHistory;
import com.bankfinance.form.ReturnCheckPaymentList;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.LoanOtherCharges;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.RecoveryLetterDetail;
import com.ites.bankfinance.model.RecoveryLetters;
import com.ites.bankfinance.model.RecoveryShift;
import com.ites.bankfinance.model.RecoveryVisitsDetails;
import com.ites.bankfinance.model.SeizeOrder;
import com.ites.bankfinance.model.SeizeOrderApproval;
import com.ites.bankfinance.model.SeizeYardRegister;
import com.ites.bankfinance.model.SeizeYardRegisterCheklist;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ITESS
 */
public interface RecoveryDao {

    public List<viewLoan> getLoanListForDue(String name, String agreementNo, String nic, String due, int branch);

    public boolean addRecoveryShiftDate(RecoveryShift formData);

    public double getInstalmentBalance(int loanId);

    public List<viewLoan> getArrearsLoanListForDue(String name, String agreemntNo, String nic, String due, int branch);

    public RecoveryShift getRecoveryShift(Integer instmntId);

    public List getRecoveryShiftDates(int LoanId);

    public List<ReturnCheckPaymentList> getCheckPayments(String fromDate, String toDate);

    public boolean updateChequePayment(int checkDetaiId, String remark, int userId);

    public double getOpenArrears(int loanId);

    public List loadLetterPostingLoans();

    public DebtorHeaderDetails findDebtorByLoanId(int loanId);

    public boolean saveLetter(RecoveryLetterDetail letter);

    public List<RecoveryHistory> findRecoveryLetterDetails(int loanId);

    public String findInvoiceDetails(int loanId);

    public Date findPreviousDate(int letterId, int loanId);

    public int findLetterTypeByLoanType(int loanId);

    public int saveOrUpdateRecoveryOfficer(int officerId, int loanId, String dueDate, int visitType, String comment);

    public List searchRecoveryOfficer();

    public List<RecoveryLetters> findAllLetters();

    public List findVisitDetailsByuser(String dueDate);

    public Object[] findRemainigInstallment(int loanId);

    public List<viewLoan> findSearchLoanList(String agNo, String cusName, String nicNo, String dueDate, String lapsDate, String vehicleNo);

    public List loanTransactionDetails(int loanId);

    public boolean saveOtherRecoveryCharge(int loanId, int chargeId, int supplierId, double loanAmount, String dueDate);

    public RecoveryVisitsDetails findRecoveryVisitDetail(int recId, int loanId);

    public boolean saveRecoveryManagerComment(int recId, String comment);

    public RecoveryVisitsDetails findRecoveryVisitDetail(int loanId);

    public int saveOrUpdateRecoveryVisitDetails(RecoveryVisitsDetails recoveryVisitsDetails);

    public List<RecoveryVisitsDetails> getBySubmitDate(Date sDate, Date eDate);

    public List<ArriesLoan> getLoanArries(String date1, String date2);

    public List<SeizeOrder> getSeizeOrders();

    public boolean saveOrUpdateSeizeOrder(SeizeOrder seizeOrder);

    public boolean activeSeizeOrder(int orderId, int loanId);

    public boolean inActiveSeizeOrder(int orderId, int loanId);

    public boolean saveOrUpdateSeizeOrderApproval(SeizeOrderApproval orderApproval);

    public SeizeOrderApproval findSeizeOrderApproval(int seizeOrdeId, int type);

    public boolean keyIssue(int seizeOrderId);

    public List<SeizeYardRegister> getYardRegisterList();

    public String saveOrUpdateYardRegister(SeizeYardRegister yardRegister);

    public SeizeYardRegister findYardRegister(int seizeOrdeId);

    public boolean saveOrUpdateYardRegisterCheckList(SeizeYardRegisterCheklist yardRegisterCheklist);

    public List<SeizeYardRegisterCheklist> getYardRegisterCheklists(int seizeOrdeId);

    public SeizeYardRegisterCheklist findYardRegisterCheklist(int docId, int seizeOrdeId);

    public List<SeizeOrder> loadSeizeApproval();

    public List<SeizeOrder> loadSeizeOrdersByAgreementNo(String agreementNo);

    public SeizeYardRegisterCheklist getYardRegisterInspectionReport(int seizeOrdeId);

    public List<SeizeOrder> getRenewalSeizeOrders();

    public List<SeizeOrder> getSeizeOrderHistory(int loanId);

    public boolean deleteYardRegister(int yardRegId);

    public boolean reverseYardRegister(int seizeOrderId);

    public List<LoanPropertyVehicleDetails> findByLoan(String agreementNo);

    public List<LoanPropertyVehicleDetails> findByVehicleNo(String vehicleNo);

    public List<LoanPropertyVehicleDetails> findByVehicleEngineNo(String engineNo);

    public List<LoanPropertyVehicleDetails> findByVehicleChassisNo(String chassisNo);

    public List loadLetterPostingByLoan(String agreementNo);

    public List loadLetterPostingByVehicle(String vehicleNo);

    public List<DirectDebitCharge> findDirectDebitCharges(int loanId);

    public boolean deleteDirectDebitCharge(int chargeId, int settlementId);

    public List<Mail> findMails(int letterTypeId);

    public RecoveryLetters findLetter(int letterId);

    public List<RecoveryLetters> findLetters(boolean flag);
    
    public List<viewLoan> findSearchLoanListforReport(String memNo);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.dao;

import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MPeriodTypes;
import com.ites.bankfinance.model.MRateType;
import com.ites.bankfinance.model.MSection;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.MSupplier;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.VehicleMake;
import com.ites.bankfinance.model.VehicleModel;
import com.ites.bankfinance.model.VehicleType;
import com.ites.bankfinance.savingsModel.MSavingsType;
import com.ites.bankfinance.savingsModel.MSubSavingsType;

import java.util.List;

public interface MasterDataDao {

    public List findLoanTypes();

    public List findSubLoanTypes(int loanType);

    public List findOthercharges();

    public List findUsersByUserType(int userType);

    public List findUsersByApproval(int appId, int subLoanId);

    public List<MSupplier> findSuppliers();

    public List findCheckList(int loanType);

    public String findCurrencyType();

    public double findCurrencyPrice();

    public String findPrinterName();

    public int findLoanTypeByLoanId(int loanId);

    public List findPeriodTypes();

    public List findApprovalTypes();

    public List findExtraChargesBySubLoanId(int subLoanId);

    public List findBankAccountsDetails();

    public List findBankAccountsDetails2();

    public List findUserJobs();

    public List<MSubLoanType> findPawningType();

    public List findArticleTypes();

    public List findBranchesByUsername(String username, String password);

    public void setUserLogin(int branchId);

    public void inactiveUser();

    public boolean checkUserIsBlock();

    public List findUsersByUserTypeAndBranch(int userType, int branchId);

    public Double findLoanRate(int loanType);

    public List<MBranch> findBranches();

    public List<MRateType> findRateType();

    public List<VehicleType> findVehicleType();

    public List<VehicleMake> findVehicleMake(int vehicleType);

    public List<VehicleModel> findVehicleModel(int vehicleType, int vehicleMake);

    public List<VehicleMake> findVehicleMake();

    public List<VehicleModel> findVehicleModel();

    public VehicleMake findVehicleMakeById(int vehicleMakeId);

    public VehicleModel findVehicleModelById(int vehicleModelId);

    public VehicleType findVehicleTypeById(int vehicleTypeId);

    public MPeriodTypes findPeriodType(int periodTypeId);

    public MBranch findBranch(int branchId);

    public List<MSection> findSections();

    public UmUser authenticate(String userName, String password);

    public List<MBranch> loadUserBranches(int userId, int sectionId);

    public Chartofaccount findAccount(String accountNo);

    public List<MSavingsType> findAllSavingsTypes();

    public MSavingsType findAllSavingsTypesById(int savingsId);

    public List<MSubSavingsType> findAllSubSavingsTypes();

    public MSubSavingsType findAllSubSavingsTypesById(int subSavingsId);

    public List findSubSavingsTypes(int savingsType);

}

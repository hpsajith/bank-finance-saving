/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.dao;

import com.bankfinance.form.ApprovalDetailsModel;
import com.bankfinance.form.CustomerInfo;
import com.bankfinance.form.LoanForm;
import com.bankfinance.form.PaymentDetails;
import com.bankfinance.form.PaymentVoucher;
import com.bankfinance.form.RecoveryReportModel;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.LoanApprovedLevels;
import com.ites.bankfinance.model.LoanCapitalizeDetail;
import com.ites.bankfinance.model.LoanGuaranteeDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanInstalmentSchedule;
import com.ites.bankfinance.model.LoanPendingDates;
import com.ites.bankfinance.model.LoanPropertyArticlesDetails;
import com.ites.bankfinance.model.LoanPropertyLandDetails;
import com.ites.bankfinance.model.LoanPropertyOtherDetails;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.LoanRecoveryReport;
import com.ites.bankfinance.model.MCaptilizeType;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.SettlementChequeDetails;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface LoanDao {

    public int saveNewLoanData(LoanForm loanForm);

    public int saveLoanApproval(LoanApprovedLevels loanApprove);

    public int saveLoanCheckList(int listId, int loanId, String path, String description);

//    public List<LoanBasicDetails> findLoanDetails();
    public Map<String, List> findByLoanId(int loanId);

    public List findByNic(String nic, Object bID);

    public Boolean findByNicNo(String nic, Object bID);

    public List findByName(String name, Object bID);

    public List findByLoanNo(String loanNo, Object bID);

    public List findByVehicleNo(String vehicleNo);

    public List findLoansByDebtorId(int debtorId);

    public List findGuarantorByDebtorId(int debtorId);

    public List findLoanByUserType(int issuType, int branch, Date startDate, Date endDate);

    public DebtorHeaderDetails findById(int id);

    public viewLoan findLoanByLoanId(int loanId);

    public int saveLoanPropertyVehicleDetails(LoanPropertyVehicleDetails formData);

    public int saveLoanPropertyArticleDetails(LoanPropertyArticlesDetails formData);

    public int saveLoanPropertyLandDetails(LoanPropertyLandDetails formData);

    public int saveLoanPropertyOtherDetails(LoanPropertyOtherDetails formData);

    public viewLoan findLoanStatus(int loanId);

    public String createLoanAgreementNo(int loanId);

//    public List createInstallmentList(int loanId);
    public LoanHeaderDetails searchLoanByLoanId(int loanId);

    public List findOtherChargesByLoanId(int loanId);

    public List findGuarantorByLoanId(int loanId);

    public LoanHeaderDetails findLoanHeaderByLoanId(int loanId);

    public List findOtherChargesByLoand(int loanId);

    public PaymentDetails calculatePayments(LoanHeaderDetails loan);

    public int saveChekDetails(SettlementChequeDetails formData);

    public int saveVoucher(int paymentType, String amount, int loanId);

    public LoanPropertyVehicleDetails findVehicleById(int id);

    public LoanPropertyArticlesDetails findArticleById(int id);

    public LoanPropertyLandDetails findLandById(int id);

    public LoanPropertyOtherDetails findOtherById(int id);

    public List findLoanCheckListByLoanId(int loanId);

    public File findDocumentById(int list_id);

    public String findDocumentNameById(int list_id);

    public void updateDocumentSubmissionStaus(int loanId);

    public int saveRecoveryOfficerComment(LoanApprovedLevels app);

    public List findApproveLevelByUser();

    public LoanApprovedLevels findloanApproval(int loanId, int type);

    public LoanPendingDates findPendingDates(int loanId);

    public int saveOrUpdatePendingDates(LoanPendingDates formData, int typeId);

    public LoanRecoveryReport findRecoveryReportById(int loanId);

    public ApprovalDetailsModel findApprovalDetails(int loanId);

    public int saveOrUpdateFieldOfficer(int loanId, int officerId);

    public List<RecoveryReportModel> findrecoveryReport(int loanId);

    public List findFiledOfficers(int loanId);

    public List findLoanListByFiledOfficer();

    public int saveRecoveryReport(int loanId, String[] comment);

    public List findInvoiceDetailsByLoanId(int loanId);

    public void setLoanIsIssueStatus(int loanId);

    public LoanHeaderDetails findLoanBalance(int loanId);

    public List findArticleDetailsById(int loanId);

    public int findLoanTypeBySubType(int loanType);

    public void saveLoanInstallment(int loanId);

    public String findLoanPeriodType(Integer loanPeriodTypeId);

    public int[] createNewVoucher(PaymentVoucher formData);

    public boolean processForPayment(int loanId);

    public String updateLoanPaymentStatus(int loanId);

    public double findOtherChargeSum(int loanId);

    public int saveAdvancePayment(SettlementPaymentDetailCheque formData);

    public List<MSubLoanType> findMsubLoans();

    public int findLoanIdByAgreementNo(String agNo);

    public List removeLoans(int branchID);

    public List deletedLoans(int branchID);

    public int removeLoan(int loanId);

    public List findVouchersByDate(String vDate);

    public List findPaymentByVoucher(int vId);

    public List findInstallmentByLoanNo(String loanNo);

    public boolean updateRental(LoanInstallment instlment);

    public String reverseProcessLoan(int loanId, String comment);

    public String reverseApprovedLoan(int loanId, String comment);

    public InsuraceProcessMain findInsuranceByLoanId(int loanId);

    public boolean removeVoucher(int vId);

    public boolean updateCheqDetails(SettlementPaymentDetailCheque cheq);

    public boolean deletePayment(int transId, int type);

    public Double[] findLoanInitialCharges(int loanId);

    public int updateDueDate(String due, int loanId);

    public String findCheqRecievedName(int chekId, int cheqTo);

    public List createInstallmentList(LoanHeaderDetails loan);

    public DebtorHeaderDetails findDebtorHeader(int debtorId);

    public Boolean findInsuranceStatus(int loanId);

    public boolean deleteLoanPropertyArticleDetails(int articleId);

    public Double findLossAmount(int loanId);

    public List<LoanInstalmentSchedule> findInstalmentSchedule(int loanId);

    public String findTotalLoanAmountWithInterest(int loanId);

    public Boolean createSchedule(LoanInstalmentSchedule instalmentSchedule, int[] insMonths, Double[] insAmounts);

    public LoanHeaderDetails findLoanHeaderDetails(int loanId);

    public List<LoanInstallment> findScheduleList(int loanId);

    public List<MCaptilizeType> findCaptilizeTypes();

    public MCaptilizeType findCaptilizeType(int capTypId);

    public boolean saveOrUpdateLoanCapitalizeDetail(LoanCapitalizeDetail loanCapitalizeDetail);

    public boolean saveOrUpdateLoanCapitalizeDetail(List<LoanCapitalizeDetail> loanCapitalizeDetails);

    public List<LoanCapitalizeDetail> findLoanCapitalizeDetails(Integer loanId);

    public LoanPropertyVehicleDetails findVehicleByLoanId(int loanId);

    public int findLoanInstallmentCount(int loanId);

    public List<LoanGuaranteeDetails> findLoanGuranteeDetails(int loanId);

    public List<LoanHeaderDetails> findLoanHeaderDetailseByAggNo(String aggNo);

    public Boolean clearInstalmentSchedule(int loanId);

    public double getTotalInterestAmount(Integer loanId);

    public DebtorHeaderDetails findDebtorHeader(String debtorAccount);

    public List<LoanPropertyVehicleDetails> findVehicleByRegNo(String vehicleRegNo);

    public List<CustomerInfo> findLoanByIsIssuAndIsDelete();

    public List<LoanHeaderDetails> findLoanDetailsAndVehicleDetailsByAggNo(String aggNo);

    List<LoanHeaderDetails> findLoanDetailsAndVehicleDetailsByVehicleNo(String vehicleNo);

    boolean saveOrUpdateTerminateAgreementByLoanId(int loanId);

    public int saveNewLoanDataGroup(LoanForm loanData);

    public List<DebtorHeaderDetails> findByMemNo(String memNo, Object bID);

    public List<LoanHeaderDetails> findActiveLoansByDebtorIDAndLoanTypeId(int debtorID, int loanTypeId);

    public String createMyNewVoucher(int loanId, double totalAmount, String chekNo, int bankAccount, String chkDate, double payingAmount, String TxtChqAmt, int chbIsACPay);

    public int findVoucherIdByVoucherNo(String voucherNo);

    public SettlementPaymentDetailCheque findSettlementPaymentDetailChequeByVoucherNo(String voucherNo, double amount, int loanId);

    public void updatePaymentAmount(String voucherNo, double amt);
}

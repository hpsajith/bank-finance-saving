/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.dao;

import com.bankfinance.form.OpenLoanForm;

/**
 *
 * @author ites
 */
public interface OpenLoanDao {

    public void addOpenLoan(OpenLoanForm formData);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.dao;

import com.bankfinance.form.ChartDataSet;
import com.bankfinance.form.DayPlanTargetModel;
import com.bankfinance.form.RecOffCollection;
import com.ites.bankfinance.model.DayPlanAssigntoRecoOfficer;
import com.ites.bankfinance.model.DayPlanAssingtoRecoManager;
import com.ites.bankfinance.model.DayPlanMonthSchedule;
import com.ites.bankfinance.model.DayPlanMonthTargetAssingCed;
import com.ites.bankfinance.model.MLoanType;
import java.util.Date;
import java.util.List;


public interface DayPlanDao {
    
    public DayPlanTargetModel calculateTarget(String m, int loanType, int branchId);
    
    public int saveOrUpdateMonthTarget(DayPlanMonthTargetAssingCed dpm);
    
    public void saveOrUpdateAssignTarget(DayPlanAssingtoRecoManager dpa);
    
    public DayPlanMonthTargetAssingCed isExistDayPlan(int loanType, Date date, int branch);
    
    public DayPlanAssingtoRecoManager isExistAssign(int targetId);
    
    public double getTargetForRecMan(int recManId, int branchId, Date tgtMon, int loanType);
    
    public void saveOrUpdateAssignTargetToRecOff(DayPlanAssigntoRecoOfficer dparo);
    
    public List<DayPlanAssigntoRecoOfficer> getAssignedList(int recManId, int branchId, Date tgtMon, int loanType);
    
    public DayPlanAssigntoRecoOfficer isExistAssignRecOff(int recOffId, int branchId, Date tgtMon, int loanType);
    
    public List<MLoanType> getAssignedLoanTypes(int recManId, int branchId);
    
    public List<MLoanType> getAssignedLoanTypesToRecOff(int recOffId, int branchId, Date tgtDate);
    
    public List<MLoanType> getAssignedLoanTypesToRecOff(int recOffId, int branchId);
    
    public void saveOrUpdateDayPlanMonthSchedule(DayPlanMonthSchedule dpms);
    
    public DayPlanMonthSchedule isExistMonthSchedule(int recOffId, int loanType, int branchId, Date tgtDate);
    
    public double getTargetForRecOff(int recOffId, int branchId, Date sDate, Date eDate, int loanType);
    
    public List<DayPlanMonthSchedule> getMonthSchedule(int recOffId, int branchId, Date sDate, Date eDate, int loanType);
    
    public void activeDayPlan(int id);
    
    public void inActiveDayPlan(int id, String reason);
    
    public List<DayPlanMonthSchedule> getCollection(int loanType, int recOffId, int branchId, Date sDate, Date eDate);

    public List<ChartDataSet> getRecManChartDataSet(int loanType, int recManId, int branchId, int type);
    
    public List<ChartDataSet> getRecOffChartDataSet(int loanType, int recOffId, int branchId, int type);
    
    public List<RecOffCollection> getRecoveryOfficersWithCollection(int loanType, int branchId, String month);

    public List<DayPlanMonthTargetAssingCed> getTargetAssingCeds(Date tgtDate, int branchId);
    
    public List getRecoveryOfficers(Date tgtDate, int branchId, int loanType);
    
    public double getRecoveryOfficerCollection(int loanType, int branchId, Date sDate, Date eDate);
    
    public double[] getBalances(int loanType, int recOffId, int branchId, Date sDate, Date eDate);
}

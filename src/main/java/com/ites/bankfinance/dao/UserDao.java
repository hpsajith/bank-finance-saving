package com.ites.bankfinance.dao;

import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.MCompany;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserRole;
import java.util.List;
import java.util.Set;

public interface UserDao {

    UmUser findByUserName(String username);

    Set<UmUserRole> getUserRole(String username);
    
    public int findByUserName();
    
    public int findUserTypeByUserId(int userId);
    
    public List findApproveLevelByUser(int userType);

    public List findLoanTypesByUserId(int userId);
    
    public Employee findEmployeeById(int empId);

    public int findEmployeeByUserId(int userId);

    public String findApprovalsByPassword(String password);

    public boolean findLoggedUserApproval();

    public String findWorkingUserName();

    public String findWorkingBranchName(int branchID);

    public MCompany findCompanyDetails();

    public int findUserWorkingBranch(int userId, int branchId);

    public int findUserType(int userId);

    
}

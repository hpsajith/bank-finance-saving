/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.dao;

import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.SettlementPaymentOther;
import com.ites.bankfinance.model.SettlementVoucher;
import com.ites.bankfinance.model.SettlementVoucherCategory;
import java.util.List;

/**
 *
 * @author Amidu-pc
 */
public interface OtherPaymentDao {

    public List<Chartofaccount> findChtAccountsList();

    public String addSettlmentPaymentOther(SettlementPaymentOther spo);

    public String getOtherTransactionIdMax(String code);

    public int saveIssuePayment(SettlementVoucher vouch);
    
    public List<SettlementVoucherCategory> getVoucherCategorys();
    
    public List<Chartofaccount> findChtAccountListByName(String accountName);

    public List<InsuranceOtherCustomers> findChtAccountInsuranceOtherCustomer(String accountName);

    public InsuranceOtherCustomers findChtAccountDetails(String accName);
    
    
}

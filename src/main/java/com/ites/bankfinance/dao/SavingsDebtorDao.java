package com.ites.bankfinance.dao;

import com.ites.bankfinance.savingsModel.DebtorHeaderDetails;

import java.util.List;

/**
 * Created by AXA-FINANCE on 5/24/2018.
 */
public interface SavingsDebtorDao {
    public int saveDebtorDetails(DebtorHeaderDetails loanform);

    public List findByNic(String nic, Object bID);

    public List findByName(String name, Object bID);

    public DebtorHeaderDetails findDebtorById(int Id);

    public void updateDebtorDetails(DebtorHeaderDetails debHeadDetails);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.config.init;

/**
 *
 * @author Uththara
 */
import javax.servlet.http.HttpSessionEvent;
import org.springframework.security.web.session.HttpSessionEventPublisher;

public class SessionListner extends HttpSessionEventPublisher {

    private static int totalActiveSessions;

    public static int getTotalActiveSession() {
        return totalActiveSessions;
    }

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        totalActiveSessions++;
        System.out.println("totalActiveSessions create="+totalActiveSessions);
        System.out.println("==== Session is created ====");
//        event.getSession().setMaxInactiveInterval(10);
        event.getSession().setMaxInactiveInterval(60*30);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        totalActiveSessions--;
        System.out.println("totalActiveSessions destroyed="+totalActiveSessions);
        System.out.println("==== Session is destroyed ====");
        super.sessionDestroyed(event);
    }

}

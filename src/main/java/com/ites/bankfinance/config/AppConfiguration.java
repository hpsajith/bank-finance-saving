package com.ites.bankfinance.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import java.util.List;
import java.util.Properties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableWebMvc
@Configuration
@ComponentScan({"com.ites.bankfinance.*"})
@EnableTransactionManagement
@Import({SecurityConfig.class})

public class AppConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public SessionFactory sessionFactory() {
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
        builder.scanPackages("com.ites.bankfinance.model").addProperties(getHibernateProperties());
        return builder.buildSessionFactory();
    }
    @Bean
    public SessionFactory savingsSessionFactory() {
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(savingsDataSource());
        builder.scanPackages("com.ites.bankfinance.savingsModel").addProperties(getHibernateProperties());
        return builder.buildSessionFactory();
    }

    @Bean
    public SessionFactory financeSessionFactory() {
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(financeDataSource());
        builder.scanPackages("com.ites.finance.model").addProperties(getHibernateProperties());
        System.out.println("=== finance session factory created ===");
        return builder.buildSessionFactory();
    }

    @Bean
    public static PropertyPlaceholderConfigurer properties() {
        PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
        ClassPathResource[] resources = new ClassPathResource[]{new ClassPathResource("database.properties")};
        ppc.setLocations(resources);
        ppc.setIgnoreUnresolvablePlaceholders(true);
        return ppc;
    }

    @Value("${jdbc.url}")
    private String jdbcUrl;
    @Value("${jdbc.financeUrl}")
    private String financeUrl;
    @Value("${jdbc.savingsUrl}")
    private String savingsUrl;
    @Value("${jdbc.driverClassName}")
    private String driverClassName;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    private Properties getHibernateProperties() {
        Properties prop = new Properties();
//        prop.put("hibernate.format_sql", "true");
        prop.put("hibernate.show_sql", "true");
        prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        prop.put("hibernate.c3p0.max_size", "600");
        prop.put("hibernate.c3p0.min_size", "5");
        prop.put("hibernate.c3p0.timeout", "15000");

        prop.put("hibernate.c3p0.pool_size",50);
        prop.put("hibernate.c3p0.current_session_context_class","thread");
//        prop.put("hibernate.c3p0.autocommit",false);

//        prop.put("hibernate.c3p0.max_statements", "50");
//        prop.put("hibernate.c3p0.acquireRetryAttempts", "0");
//        prop.put("hibernate.c3p0.acquireRetryAttempts", "0");
//        prop.put("hibernate.c3p0.acquireRetryDelay", "3000");
//        prop.put("c3p0.breakAfterAcquireFailure", "false");


//        //        prop.put("hibernate.format_sql", "true");
//        prop.put("hibernate.show_sql", "true");
//        prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
//        prop.put("hibernate.c3p0.acquire_increment", "1");
//        prop.put("hibernate.c3p0.idle_test_period", "60");
//        prop.put("hibernate.c3p0.max_size", "60");
//        prop.put("hibernate.c3p0.min_size", "1");
//        prop.put("hibernate.c3p0.timeout", "0");
//        prop.put("hibernate.c3p0.max_statements", "0");
//        prop.put("hibernate.c3p0.acquireRetryAttempts", "1");
//        prop.put("hibernate.c3p0.acquireRetryDelay", "250");
////        prop.put("c3p0.breakAfterAcquireFailure", "false");

        return prop;
    }

    @Bean(name = "dataSource")
    public BasicDataSource dataSource() {

        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUrl(jdbcUrl);
        ds.setUsername(username);
        ds.setPassword(password);
        return ds;
    }

    @Bean(name = "savingsDataSource")
    public BasicDataSource savingsDataSource() {

        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUrl(savingsUrl);
        ds.setUsername(username);
        ds.setPassword(password);
        return ds;
    }

    @Bean(name = "financeDataSource")
    public BasicDataSource financeDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUrl(financeUrl);
        ds.setUsername(username);
        ds.setPassword(password);
        return ds;
    }

    @Bean
    public HibernateTransactionManager txManager() {
        return new HibernateTransactionManager(sessionFactory());
    }

    @Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(1024000000);
        return multipartResolver;
    }

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/pages/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean
    public MappingJackson2HttpMessageConverter jacksonMessageConverter() {
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();

        ObjectMapper mapper = new ObjectMapper();
        //Registering Hibernate4Module to support lazy objects
        mapper.registerModule(new Hibernate4Module());

        messageConverter.setObjectMapper(mapper);
        return messageConverter;

    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        //Here we add our custom-configured HttpMessageConverter
        converters.add(jacksonMessageConverter());
        converters.add(new ByteArrayHttpMessageConverter());
        super.configureMessageConverters(converters);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/WEB-INF/resources/*");
    }

}

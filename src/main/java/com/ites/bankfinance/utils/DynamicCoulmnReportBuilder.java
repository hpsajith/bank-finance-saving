package com.ites.bankfinance.utils;

import java.awt.Color;
import java.util.List;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRLineBox;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;

/**
 * Uses the Jasper Reports API to dynamically build Report
 */
public class DynamicCoulmnReportBuilder {

    //The prefix used in defining the field name that is later used by the JasperFillManager
    public static final String COL_EXPR_PREFIX = "col";

    // The prefix used in defining the column header name that is later used by the JasperFillManager
    public static final String COL_HEADER_EXPR_PREFIX = "header";

    // The page width for a page in portrait mode with 10 pixel margins
    private final static int TOTAL_PAGE_WIDTH = 8000;

    // The whitespace between columns in pixels
    private final static int SPACE_BETWEEN_COLS = 0;

    // The height in pixels of an element in a row and column
    private final static int COLUMN_HEIGHT = 12;

    private final static int COLUMN_WIDTH = 150;

    // The total height of the column header or detail band
    private final static int BAND_HEIGHT = 15;

    // The left and right margin in pixels
    private final static int MARGIN = 10;

    // The JasperDesign object is the internal representation of a report
    private final JasperDesign jasperDesign;

    // The number of columns that are to be displayed
    private final int numColumns;

    private final List<String> columnHeaders;

    public DynamicCoulmnReportBuilder(JasperDesign jasperDesign, int numColumns, List<String> columnHeaders) {
        this.jasperDesign = jasperDesign;
        this.numColumns = numColumns;
        this.columnHeaders = columnHeaders;
    }

    public JasperDesign buildReport() throws JRException {

        JRDesignStyle normalStyle = getNormalStyle();
        JRDesignStyle columnHeaderStyle = getColumnHeaderStyle();
        JRDesignStyle boldStyle = getBoldStyle();
        jasperDesign.addStyle(normalStyle);
        jasperDesign.addStyle(columnHeaderStyle);
        jasperDesign.addStyle(boldStyle);

        JRDesignBand columnHeaderBand = new JRDesignBand();
        columnHeaderBand.setHeight(BAND_HEIGHT);

        JRDesignBand detailBand = new JRDesignBand();
        detailBand.setHeight(BAND_HEIGHT);

        int xPos = MARGIN;
        int columnWidth = (TOTAL_PAGE_WIDTH - (SPACE_BETWEEN_COLS * (numColumns - 1))) / numColumns;

        for (int i = 0; i < numColumns; i++) {
            JRDesignStaticText coulmnHeader = new JRDesignStaticText();
            coulmnHeader.setX(xPos);
            coulmnHeader.setY(0);
            coulmnHeader.setWidth(columnWidth);
            coulmnHeader.setHeight(COLUMN_HEIGHT);
            coulmnHeader.setStyle(columnHeaderStyle);
            coulmnHeader.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
            coulmnHeader.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
            coulmnHeader.setText(columnHeaders.get(i).toUpperCase());
            JRLineBox lb = coulmnHeader.getLineBox();
            lb.getLeftPen().setLineWidth(.5f);
            lb.getRightPen().setLineWidth(.5f);
            lb.getBottomPen().setLineWidth(.5f);
            lb.getTopPen().setLineWidth(.5f);
            columnHeaderBand.addElement(coulmnHeader);

            // Create a Column Field
            JRDesignField field = new JRDesignField();
            field.setName(columnHeaders.get(i));
            field.setDescription(columnHeaders.get(i));
            field.setValueClass(java.lang.String.class);
            jasperDesign.addField(field);

            // Add text field to the detailBand
            JRDesignTextField textField = new JRDesignTextField();
            textField.setX(xPos);
            textField.setY(0);
            textField.setWidth(columnWidth);
            textField.setHeight(COLUMN_HEIGHT);
            textField.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
            textField.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
            textField.setStyle(normalStyle);
            JRLineBox lineBox = textField.getLineBox();
            lineBox.getLeftPen().setLineWidth(.5f);
            lineBox.getRightPen().setLineWidth(.5f);
            lineBox.getBottomPen().setLineWidth(.5f);
            lineBox.getTopPen().setLineWidth(.5f);
            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(java.lang.String.class);
            expression.setText("$F{" + columnHeaders.get(i) + "}");
            textField.setExpression(expression);
            detailBand.addElement(textField);

            xPos = xPos + columnWidth + SPACE_BETWEEN_COLS;
        }

        jasperDesign.setColumnHeader(columnHeaderBand);
        ((JRDesignSection) jasperDesign.getDetailSection()).addBand(detailBand);

        return jasperDesign;
    }

    private JRDesignStyle getNormalStyle() {
        JRDesignStyle normalStyle = new JRDesignStyle();
        normalStyle.setName("Sans_Normal");
        normalStyle.setDefault(true);
        normalStyle.setFontName("SansSerif");
        normalStyle.setFontSize(9);
        normalStyle.setPdfFontName("Helvetica");
        normalStyle.setPdfEncoding("Cp1252");
        normalStyle.setPdfEmbedded(false);
        return normalStyle;
    }

    private JRDesignStyle getBoldStyle() {
        JRDesignStyle boldStyle = new JRDesignStyle();
        boldStyle.setName("Sans_Bold");
        boldStyle.setFontName("SansSerif");
        boldStyle.setDefault(false);
        boldStyle.setBold(true);
        boldStyle.setFontSize(20);
        boldStyle.setPdfFontName("Helvetica");
        boldStyle.setPdfEncoding("Cp1252");
        boldStyle.setPdfEmbedded(false);
        return boldStyle;
    }

    private JRDesignStyle getColumnHeaderStyle() {
        JRDesignStyle columnHeaderStyle = new JRDesignStyle();
        columnHeaderStyle.setName("Sans_Header");
        columnHeaderStyle.setDefault(false);
        columnHeaderStyle.setFontName("SansSerif");
        columnHeaderStyle.setFontSize(9);
        columnHeaderStyle.setBold(true);
        columnHeaderStyle.setPdfFontName("Helvetica");
        columnHeaderStyle.setPdfEncoding("Cp1252");
        columnHeaderStyle.setPdfEmbedded(false);
        return columnHeaderStyle;
    }

}

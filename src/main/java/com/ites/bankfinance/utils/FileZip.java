package com.ites.bankfinance.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author Thushan
 *
 * This class contains fields and accessors related to file compress
 */
public class FileZip {

    private static List<String> fileList = null;
    private static String OUTPUT_ZIP_FILE = "";
    private static String SOURCE_FOLDER = "";

    /**
     * Zip it
     *
     * @param outputZipFile output ZIP file location
     * @param srcFile
     */
    public static void zipIt(String srcFile, String outputZipFile) {
        fileList = new ArrayList<>();
        OUTPUT_ZIP_FILE = outputZipFile;
        SOURCE_FOLDER = srcFile;
        generateFileList(new File(SOURCE_FOLDER));
        byte[] buffer = new byte[1024];
        try {
            FileOutputStream fos = new FileOutputStream(new File(OUTPUT_ZIP_FILE));
            ZipOutputStream zos = new ZipOutputStream(fos);
            //System.out.println("Output to Zip : " + outputZipFile);
            for (String file : fileList) {
                //System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(srcFile + File.separator + file);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            //remember to close it
            zos.close();
            //System.out.println("Done");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Traverse a directory and get all files, and add the file into fileList
     *
     * @param node file or directory
     */
    public static void generateFileList(File node) {

        //add file only
        if (node.isFile()) {
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }

    }

    /**
     * Format the file path for zip
     *
     * @param file file path
     * @return Formatted file path
     */
    private static String generateZipEntry(String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }

}

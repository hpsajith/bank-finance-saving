package com.ites.bankfinance.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * A sample service to run a Jasper Report and export it to a PDF file.
 */
public class DynamicColumnReportService {

    public JasperPrint runReport(List<String> columns, File xmlFile) throws JRException {
        JasperPrint jasperPrint = null;
        InputStream is = null;
        is = this.getClass().getClassLoader().getResourceAsStream("..//reports/portfolio/portfolioReport.jrxml");
        JasperDesign jasperReportDesign = JRXmlLoader.load(is);
        DynamicCoulmnReportBuilder reportBuilder = new DynamicCoulmnReportBuilder(jasperReportDesign, columns.size(), columns);
        jasperReportDesign = reportBuilder.buildReport();
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperReportDesign);
        JRXmlDataSource jRXmlDataSource = new JRXmlDataSource(xmlFile, "/PortfolioDetail/PortfolioDetails");
        jasperPrint = JasperFillManager.fillReport(jasperReport, null, jRXmlDataSource);
        try {
            if (is != null) {
                is.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return jasperPrint;
    }

}


package com.ites.bankfinance.model;
/**
 *
 * @author ITESS
 */
public class SettlementPreview {

    private String refNoDownPymntSuspCodeDscrpt;
    private double downPymntSuspCodeDscrptAmount;
    private String refArrearsCodeDscrpt;
    private double arrearsAmount;
    private String refNoOtherChargeCodeDscrpt;    
    private double otherChargeCodeDscrptAmount;
    private String refNoInsuaranceCodeDscrpt;
    private double insuaranceCodeDscrptAmount;
    private String refNoODICodeDscrpt;
    private double paidCurrentOdi;
    private String refNoInstallmentCodeDscrpt;
    private double installmentCodeDscrptAmount; 
    private String refNoOverPayCodeDescrpt;
    private double overPayAmount;
    private String refNoRebateDscrpt;
    private double rebateAmount;

    public String getRefNoDownPymntSuspCodeDscrpt() {
        return refNoDownPymntSuspCodeDscrpt;
    }

    public void setRefNoDownPymntSuspCodeDscrpt(String refNoDownPymntSuspCodeDscrpt) {
        this.refNoDownPymntSuspCodeDscrpt = refNoDownPymntSuspCodeDscrpt;
    }

    public double getDownPymntSuspCodeDscrptAmount() {
        return downPymntSuspCodeDscrptAmount;
    }

    public void setDownPymntSuspCodeDscrptAmount(double downPymntSuspCodeDscrptAmount) {        
        this.downPymntSuspCodeDscrptAmount = downPymntSuspCodeDscrptAmount;
//      this.downPymntSuspCodeDscrptAmount = Math.round(downPymntSuspCodeDscrptAmount*100)/100;
    }

    public String getRefArrearsCodeDscrpt() {
        return refArrearsCodeDscrpt;
    }

    public void setRefArrearsCodeDscrpt(String refArrearsCodeDscrpt) {
        this.refArrearsCodeDscrpt = refArrearsCodeDscrpt;
    }

    public double getArrearsAmount() {
        return arrearsAmount;
    }

    public void setArrearsAmount(double arrearsAmount) {
        this.arrearsAmount =  arrearsAmount;
//        this.arrearsAmount =  Math.round(arrearsAmount*100)/100;
    }

    public String getRefNoOtherChargeCodeDscrpt() {
        return refNoOtherChargeCodeDscrpt;
    }

    public void setRefNoOtherChargeCodeDscrpt(String refNoOtherChargeCodeDscrpt) {
        this.refNoOtherChargeCodeDscrpt = refNoOtherChargeCodeDscrpt;
    }

    public double getOtherChargeCodeDscrptAmount() {
        return otherChargeCodeDscrptAmount;
    }

    public void setOtherChargeCodeDscrptAmount(double otherChargeCodeDscrptAmount) {
        this.otherChargeCodeDscrptAmount =  otherChargeCodeDscrptAmount;
//        this.otherChargeCodeDscrptAmount =  Math.round(otherChargeCodeDscrptAmount*100)/100;
    }

    public String getRefNoInsuaranceCodeDscrpt() {
        return refNoInsuaranceCodeDscrpt;
    }

    public void setRefNoInsuaranceCodeDscrpt(String refNoInsuaranceCodeDscrpt) {
        this.refNoInsuaranceCodeDscrpt = refNoInsuaranceCodeDscrpt;
    }

    public double getInsuaranceCodeDscrptAmount() {
        return insuaranceCodeDscrptAmount;
    }

    public void setInsuaranceCodeDscrptAmount(double insuaranceCodeDscrptAmount) {
        this.insuaranceCodeDscrptAmount =  insuaranceCodeDscrptAmount;
//        this.insuaranceCodeDscrptAmount =  Math.round(insuaranceCodeDscrptAmount*100)/100;
    }

    public String getRefNoODICodeDscrpt() {
        return refNoODICodeDscrpt;
    }

    public void setRefNoODICodeDscrpt(String refNoODICodeDscrpt) {
        this.refNoODICodeDscrpt = refNoODICodeDscrpt;
    }

    public double getInstallmentCodeDscrptAmount() {
        return installmentCodeDscrptAmount;
    }

    public void setInstallmentCodeDscrptAmount(double installmentCodeDscrptAmount) {
        this.installmentCodeDscrptAmount =  installmentCodeDscrptAmount;
//        this.installmentCodeDscrptAmount =  Math.round(installmentCodeDscrptAmount*100)/100;
    }

    public String getRefNoInstallmentCodeDscrpt() {
        return refNoInstallmentCodeDscrpt;
    }

    public void setRefNoInstallmentCodeDscrpt(String refNoInstallmentCodeDscrpt) {
        this.refNoInstallmentCodeDscrpt = refNoInstallmentCodeDscrpt;
    }

    public double getPaidCurrentOdi() {
        return paidCurrentOdi;
    }

    public void setPaidCurrentOdi(double paidCurrentOdi) {
        this.paidCurrentOdi =  paidCurrentOdi;
//        this.paidCurrentOdi =  Math.round(paidCurrentOdi*100)/100;
    }

    public String getRefNoOverPayCodeDescrpt() {
        return refNoOverPayCodeDescrpt;
    }

    public void setRefNoOverPayCodeDescrpt(String refNoOverPayCodeDescrpt) {
        this.refNoOverPayCodeDescrpt = refNoOverPayCodeDescrpt;
    }

    public double getOverPayAmount() {
        return overPayAmount;
    }

    public void setOverPayAmount(double overPayAmount) {
        this.overPayAmount =  overPayAmount;
//        this.overPayAmount =  Math.round(overPayAmount*100)/100;
    }

    public double getRebateAmount() {
        return rebateAmount;
    }

    public void setRebateAmount(double RebateAmount) {
        this.rebateAmount = RebateAmount;
    }

    public String getRefNoRebateDscrpt() {
        return refNoRebateDscrpt;
    }

    public void setRefNoRebateDscrpt(String refNoRebateDscrpt) {
        this.refNoRebateDscrpt = refNoRebateDscrpt;
    }
    
    
}

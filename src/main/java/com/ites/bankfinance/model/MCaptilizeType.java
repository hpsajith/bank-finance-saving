package com.ites.bankfinance.model;
// Generated Apr 22, 2016 3:12:17 PM by Hibernate Tools 3.6.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * MCaptilizeType generated by hbm2java
 */
@Entity
@Table(name = "m_captilize_type", catalog = "axa_bank_finance"
)
public class MCaptilizeType implements java.io.Serializable {

    private Integer id;
    private String capitalizeType;
    private String description;
    private Set<LoanCapitalizeDetail> loanCapitalizeDetails = new HashSet<LoanCapitalizeDetail>(0);

    public MCaptilizeType() {
    }

    public MCaptilizeType(int id) {
        this.id = id;
    }

    public MCaptilizeType(String capitalizeType, String description, Set<LoanCapitalizeDetail> loanCapitalizeDetails) {
        this.capitalizeType = capitalizeType;
        this.description = description;
        this.loanCapitalizeDetails = loanCapitalizeDetails;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Capitalize_Type", length = 150)
    public String getCapitalizeType() {
        return this.capitalizeType;
    }

    public void setCapitalizeType(String capitalizeType) {
        this.capitalizeType = capitalizeType;
    }

    @Column(name = "Description", length = 250)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "MCaptilizeType")
    public Set<LoanCapitalizeDetail> getLoanCapitalizeDetails() {
        return this.loanCapitalizeDetails;
    }

    public void setLoanCapitalizeDetails(Set<LoanCapitalizeDetail> loanCapitalizeDetails) {
        this.loanCapitalizeDetails = loanCapitalizeDetails;
    }

}

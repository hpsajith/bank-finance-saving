/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Administrator
 */

@Entity
@Table(name="settlement_payment_type"
    ,catalog="axa_bank_finance"
)
public class SettlementPaymentTypes implements java.io.Serializable{
    private int paymentTypeId;
    private String paymentType;
    private String accNo;
    private int bank;

    
     @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="Paymant_Type_Id", unique=true, nullable=false)
    public int getpaymentTypeId() {
        return paymentTypeId;
    }

    public void setpaymentTypeId(int paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    @Column(name="Payment_Type")
    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @Column(name="Acc_No")
    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    @Column(name="Bank")
    public int getBank() {
        return bank;
    }

    public void setBank(int bank) {
        this.bank = bank;
    }
    
    
}

package com.ites.bankfinance.model;
// Generated Feb 26, 2015 2:21:31 PM by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * LoanPropertyLandDetails generated by hbm2java
 */
@Entity
@Table(name="loan_property_land_details"
    ,catalog="axa_bank_finance"
)
public class LoanPropertyLandDetails  implements java.io.Serializable {


     private Integer landId;
     private Integer loanId;
     private String landDeedNo;
     private String landPlace;
     private String landArea;
     private String landDescription;
     private Double landPrice = 0.00;
     private Boolean isActive=true;
     private Boolean isDelete=false;
     private Boolean isCancel=false;
     private Integer landStatus=0;
     private Date actionTime;
     private Integer userId;
     private Integer branchId;
     private Integer dealerId=1;

    public LoanPropertyLandDetails() {
    }

    public LoanPropertyLandDetails(Integer loanId, String landDeedNo, String landPlace, String landArea, String landDescription, Double landPrice, Boolean isActive, Boolean isDelete, Boolean isCancel, Integer landStatus, Date actionTime, Integer userId, Integer branchId, Integer dealerId) {
       this.loanId = loanId;
       this.landDeedNo = landDeedNo;
       this.landPlace = landPlace;
       this.landArea = landArea;
       this.landDescription = landDescription;
       this.landPrice = landPrice;
       this.isActive = isActive;
       this.isDelete = isDelete;
       this.isCancel = isCancel;
       this.landStatus = landStatus;
       this.actionTime = actionTime;
       this.userId = userId;
       this.branchId = branchId;
       this.dealerId = dealerId;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Land_Id", unique=true, nullable=false)
    public Integer getLandId() {
        return this.landId;
    }
    
    public void setLandId(Integer landId) {
        this.landId = landId;
    }

    
    @Column(name="Loan_Id")
    public Integer getLoanId() {
        return this.loanId;
    }
    
    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    
    @Column(name="Land_DeedNo", length=50)
    public String getLandDeedNo() {
        return this.landDeedNo;
    }
    
    public void setLandDeedNo(String landDeedNo) {
        this.landDeedNo = landDeedNo;
    }

    
    @Column(name="Land_Place", length=200)
    public String getLandPlace() {
        return this.landPlace;
    }
    
    public void setLandPlace(String landPlace) {
        this.landPlace = landPlace;
    }

    
    @Column(name="Land_Area", length=100)
    public String getLandArea() {
        return this.landArea;
    }
    
    public void setLandArea(String landArea) {
        this.landArea = landArea;
    }

    
    @Column(name="Land_Description", length=200)
    public String getLandDescription() {
        return this.landDescription;
    }
    
    public void setLandDescription(String landDescription) {
        this.landDescription = landDescription;
    }

    
    @Column(name="Land_Price", precision=20)
    public Double getLandPrice() {
        return this.landPrice;
    }
    
    public void setLandPrice(Double landPrice) {
        this.landPrice = landPrice;
    }

    
    @Column(name="IsActive")
    public Boolean getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    
    @Column(name="IsDelete")
    public Boolean getIsDelete() {
        return this.isDelete;
    }
    
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    
    @Column(name="IsCancel")
    public Boolean getIsCancel() {
        return this.isCancel;
    }
    
    public void setIsCancel(Boolean isCancel) {
        this.isCancel = isCancel;
    }

    
    @Column(name="Land_Status")
    public Integer getLandStatus() {
        return this.landStatus;
    }
    
    public void setLandStatus(Integer landStatus) {
        this.landStatus = landStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="Action_Time", length=19)
    public Date getActionTime() {
        return this.actionTime;
    }
    
    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    
    @Column(name="UserId")
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    
    @Column(name="BranchId")
    public Integer getBranchId() {
        return this.branchId;
    }
    
    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    
    @Column(name="Dealer_Id")
    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }


}



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Harshana
 */
@Entity
@Table(name = "debtor_group_details")
@NamedQueries({
    @NamedQuery(name = "DebtorGroupDetails.findAll", query = "SELECT d FROM DebtorGroupDetails d")})
public class DebtorGroupDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "m_branch_id")
    private Integer mBranchId;
    @Column(name = "m_center_id")
    private Integer mCenterId;
    @Column(name = "m_group_id")
    private Integer mGroupId;
    @Column(name = "debtor_id")
    private Integer debtorId;
    @Column(name = "loan_id")
    private Integer loanId;
    @Column(name = "loan_status")
    private Boolean loanStatus;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "action_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actionTime;

    public DebtorGroupDetails() {
    }

    public DebtorGroupDetails(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMBranchId() {
        return mBranchId;
    }

    public void setMBranchId(Integer mBranchId) {
        this.mBranchId = mBranchId;
    }

    public Integer getMCenterId() {
        return mCenterId;
    }

    public void setMCenterId(Integer mCenterId) {
        this.mCenterId = mCenterId;
    }

    public Integer getMGroupId() {
        return mGroupId;
    }

    public void setMGroupId(Integer mGroupId) {
        this.mGroupId = mGroupId;
    }

    public Integer getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(Integer debtorId) {
        this.debtorId = debtorId;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Boolean getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(Boolean loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DebtorGroupDetails)) {
            return false;
        }
        DebtorGroupDetails other = (DebtorGroupDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelclassgenerator.DebtorGroupDetails[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrator
 */

@Entity
@Table(name="settlement_payment_details_bank_deposite"
    ,catalog="axa_bank_finance"
)
public class SettlementPaymentDetailBankDeposite implements java.io.Serializable{
    /*settlement_payment_details_bank_deposite

Bank_Deposit_Id
Transaction_No
Transaction_Code
Loan_Id
Deposite_No
Bank_Name
Bank_Branch
Deposite_Account_No
Deposite_Date
User_Id
Action_Time*/
    
    private Integer bankDepositId;
    private String transactionNo;
    private int loanId;
    private int bankId;
    private String depositeAccountNo;
    private Date depositeDate;
    private int userId;
    private Date actionTime;
    private double amount;

    
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="Bank_Deposit_Id", unique=true, nullable=false)
    public Integer getBankDepositId() {
        return bankDepositId;
    }

    public void setBankDepositId(Integer BankDepositId) {
        this.bankDepositId = BankDepositId;
    }

    @Column(name="Transaction_No")
    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String TransactionNo) {
        this.transactionNo = TransactionNo;
    }

    @Column(name="Loan_Id")
    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int LoanId) {
        this.loanId = LoanId;
    }

    @Column(name="Bank_Id")
    public int getBankId() {
        return bankId;
    }

    public void setBankId(int BankId) {
        this.bankId = BankId;
    }


    @Column(name="Deposite_Account_No")
    public String getDepositeAccountNo() {
        return depositeAccountNo;
    }
    public void setDepositeAccountNo(String DepositeAccountNo) {
        this.depositeAccountNo = DepositeAccountNo;
    }

    @Column(name="Deposite_Date")
    public Date getDepositeDate() {
        return depositeDate;
    }

    public void setDepositeDate(Date DepositeDate) {
        this.depositeDate = DepositeDate;
    }

    @Column(name="User_Id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int UserId) {
        this.userId = UserId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="Action_Time")
    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date ActionTime) {
        this.actionTime = ActionTime;
    }
    
    @Column(name="Amount")
    public double getAmount() {
        return amount;
    }

    public void setAmount(double Amount) {
        this.amount = Amount;
    }
}

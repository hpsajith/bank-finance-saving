/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.model;

import java.util.Date;
import javax.persistence.Entity;

/**
 *
 * @author Administrator
 */
public class SettlementAddNewPayment implements java.io.Serializable{
    

    private int loanId;
    private int branchId;
    
    private double totalOtherCharges;
    private double otherChargesAmount;
    private double totalArrears;
    private double downPayment;
    private double insuranceBalance;
    private double overPayAmount;

  
    
    private boolean isArreas;
    private boolean isDownPayment;
    private boolean isRabatePayment;
    private boolean isInsurance;
    private boolean isInstalment;
    private boolean isOverPay;

    

    private String remark;
    
    private Integer installmentId;
    private double installmentAmount;
    
    private int paymentTypeId[];

    private double Amount[];
    private String amountText[];
    private Integer BankId[];
    private String AccNo[];
    private String TransDate[];
    
    private int pdChequeId = 0;
    
      public double getOverPayAmount() {
        return overPayAmount;
    }

    public void setOverPayAmount(double overPayAmount) {
        this.overPayAmount = overPayAmount;
    }
    
    public boolean getIsInstalment() {
        return isInstalment;
    }

    public void setIsInstalment(boolean isInstalment) {
        this.isInstalment = isInstalment;
    }

    public double getInsuranceBalance() {
        return insuranceBalance;
    }

    public void setInsuranceBalance(double insuranceBalance) {
        this.insuranceBalance = insuranceBalance;
    }
    
    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }
    
    public double getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }
    
    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public double getTotalArrears() {
        return totalArrears;
    }

    public void setTotalArrears(double totalArrears) {
        this.totalArrears = totalArrears;
    }
    
    public double getTotalOtherCharges() {
        return totalOtherCharges;
    }

    public void setTotalOtherCharges(double sumOtherCharges) {
        this.totalOtherCharges = sumOtherCharges;
    }

    public double getOtherChargesAmount() {
        return otherChargesAmount;
    }

    public void setOtherChargesAmount(double otherChargesAmount) {
        this.otherChargesAmount = otherChargesAmount;
    }

    public boolean getIsArreas() {
        return isArreas;
    }

    public void setIsArreas(boolean isArreas) {
        this.isArreas = isArreas;
    }
    
    public boolean getIsDownPayment() {
        return isDownPayment;
    }

    public void setIsDownPayment(boolean isDownPayment) {
        this.isDownPayment = isDownPayment;
    }

    public boolean getIsRabatePayment() {
        return isRabatePayment;
    }

    public void setIsRabatePayment(boolean isRabatePayment) {
        this.isRabatePayment = isRabatePayment;
    }

    public boolean getIsInsurance() {
        return isInsurance;
    }

    public void setIsInsurance(boolean isInsurance) {
        this.isInsurance = isInsurance;
    }
    
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getInstallmentId() {
        return installmentId;
    }

    public void setInstallmentId(Integer installmentId) {
        this.installmentId = installmentId;
    }

    public double getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(double installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public int[] getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(int[] paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public double[] getAmount() {
        return Amount;
    }

    public void setAmount(double[] Amount) {
        this.Amount = Amount;
    }

    public String[] getAmountText() {
        return amountText;
    }

    public void setAmountText(String[] amountText) {
        this.amountText = amountText;
    }

    public Integer[] getBankId() {
        return BankId;
    }

    public void setBankId(Integer[] BankId) {
        this.BankId = BankId;
    }

    public String[] getAccNo() {
        return AccNo;
    }

    public void setAccNo(String[] AccNo) {
        this.AccNo = AccNo;
    }

    public String[] getTransDate() {
        return TransDate;
    }

    public void setTransDate(String[] TransDate) {
        this.TransDate = TransDate;
    }
    
    public boolean getIsOverPay() {
        return isOverPay;
    }

    public void setIsOverPay(boolean isOverPay) {
        this.isOverPay = isOverPay;
    }

    public int getPdChequeId() {
        return pdChequeId;
    }

    public void setPdChequeId(int pdChequeId) {
        this.pdChequeId = pdChequeId;
    }
    
}

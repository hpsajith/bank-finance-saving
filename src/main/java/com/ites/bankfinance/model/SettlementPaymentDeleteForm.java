/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.model;

import java.io.Serializable;
import javax.persistence.Transient;

/**
 *
 * @author ITESS
 */
public class SettlementPaymentDeleteForm implements Serializable{
    

    int transactionId;
    String reason;

    @Transient
    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    @Transient
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


    
    
    
}

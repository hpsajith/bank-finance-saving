package com.ites.bankfinance.model;
// Generated Apr 8, 2015 12:16:30 PM by Hibernate Tools 3.6.0


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * UmSpecialPassword generated by hbm2java
 */
@Entity
@Table(name="um_special_password"
    ,catalog="axa_bank_finance"
)
public class UmSpecialPassword  implements java.io.Serializable {


     private Integer pk;
     private Integer userId;
     private String userPassword;

    public UmSpecialPassword() {
    }

    public UmSpecialPassword(Integer userId, String userPassword) {
       this.userId = userId;
       this.userPassword = userPassword;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Pk", unique=true, nullable=false)
    public Integer getPk() {
        return this.pk;
    }
    
    public void setPk(Integer pk) {
        this.pk = pk;
    }

    
    @Column(name="User_Id")
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    
    @Column(name="User_Password")
    public String getUserPassword() {
        return this.userPassword;
    }
    
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }




}



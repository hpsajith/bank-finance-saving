package com.ites.bankfinance.model;
// Generated Jul 18, 2016 10:32:38 AM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * LoanPropertyVehicleDetails generated by hbm2java
 */
@Entity
@Table(name = "loan_property_vehicle_details", catalog = "axa_bank_finance")
public class LoanPropertyVehicleDetails implements java.io.Serializable {

    private Integer vehicleId;
    private Integer loanId;
    private String facilityNo;
    private String applicationNo;
    private Integer lotNo;
    private Integer sequenceNo;
    private Boolean isDelivered = false;
    private String countryOfOrigin;
    private Date purchaseOrderDate;
    private String deliveryAddress;
    private Date firstRegDate;
    private String introducerCommission;
    private Double commissionAmount = 0.00;
    private String introducerCode;
    private Date insuranceStartDate;
    private Date insuranceExpiryDate;
    private Double insurancePremium = 0.00;
    private Double insuranceAmount = 0.00;
    private Boolean insuranceAssigned;
    private String insurancePolicyNo;
    private String insuranceInstitute;
    private Double marketPrice = 0.00;
    private String provincialCouncil;
    private Date vehicleRegDate;
    private String vehicleType;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleRegNo;
    private String statusWhenReg;
    private String taxationClass;
    private String vehicleDescription;
    private String vehicleEngineNo;
    private String vehiclChassisNo;
    private String vehicleBodyColor;
    private String typeOfBody;
    private String weight;
    private String wheelBase;
    private String manufacturedYear;
    private String vehicleRoundNo;
    private String vehicleCapacity;
    private String cylinderCapacity;
    private Double vehiclePrice = 0.00;
    private Boolean isActive = true;
    private Boolean isDelete = false;
    private Boolean isCancel = false;
    private Integer vehicleStatus = 0;
    private Date actionTime;
    private Integer userId;
    private Integer branchId;
    private Integer dealerId = 1;
    private LoanHeaderDetails loanHeaderDetails;
    private VehicleMake vMake;
    private VehicleModel vModel;
    private VehicleType vType;

    public LoanPropertyVehicleDetails() {
    }

    public LoanPropertyVehicleDetails(Integer loanId, String facilityNo, String applicationNo, Integer lotNo, Integer sequenceNo, Boolean isDelivered, String countryOfOrigin, Date purchaseOrderDate, String deliveryAddress, Date firstRegDate, String introducerCommission, Double commissionAmount, String introducerCode, Date insuranceStartDate, Date insuranceExpiryDate, Double insurancePremium, Double insuranceAmount, Boolean insuranceAssigned, String insurancePolicyNo, String insuranceInstitute, Double marketPrice, String provincialCouncil, Date vehicleRegDate, String vehicleType, String vehicleMake, String vehicleModel, String vehicleRegNo, String statusWhenReg, String taxationClass, String vehicleDescription, String vehicleEngineNo, String vehiclChassisNo, String vehicleBodyColor, String typeOfBody, String weight, String wheelBase, String manufacturedYear, String vehicleRoundNo, String vehicleCapacity, String cylinderCapacity, Double vehiclePrice, Boolean isActive, Boolean isDelete, Boolean isCancel, Integer vehicleStatus, Date actionTime, Integer userId, Integer branchId, Integer dealerId) {
        this.loanId = loanId;
        this.facilityNo = facilityNo;
        this.applicationNo = applicationNo;
        this.lotNo = lotNo;
        this.sequenceNo = sequenceNo;
        this.isDelivered = isDelivered;
        this.countryOfOrigin = countryOfOrigin;
        this.purchaseOrderDate = purchaseOrderDate;
        this.deliveryAddress = deliveryAddress;
        this.firstRegDate = firstRegDate;
        this.introducerCommission = introducerCommission;
        this.commissionAmount = commissionAmount;
        this.introducerCode = introducerCode;
        this.insuranceStartDate = insuranceStartDate;
        this.insuranceExpiryDate = insuranceExpiryDate;
        this.insurancePremium = insurancePremium;
        this.insuranceAmount = insuranceAmount;
        this.insuranceAssigned = insuranceAssigned;
        this.insurancePolicyNo = insurancePolicyNo;
        this.insuranceInstitute = insuranceInstitute;
        this.marketPrice = marketPrice;
        this.provincialCouncil = provincialCouncil;
        this.vehicleRegDate = vehicleRegDate;
        this.vehicleType = vehicleType;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.vehicleRegNo = vehicleRegNo;
        this.statusWhenReg = statusWhenReg;
        this.taxationClass = taxationClass;
        this.vehicleDescription = vehicleDescription;
        this.vehicleEngineNo = vehicleEngineNo;
        this.vehiclChassisNo = vehiclChassisNo;
        this.vehicleBodyColor = vehicleBodyColor;
        this.typeOfBody = typeOfBody;
        this.weight = weight;
        this.wheelBase = wheelBase;
        this.manufacturedYear = manufacturedYear;
        this.vehicleRoundNo = vehicleRoundNo;
        this.vehicleCapacity = vehicleCapacity;
        this.cylinderCapacity = cylinderCapacity;
        this.vehiclePrice = vehiclePrice;
        this.isActive = isActive;
        this.isDelete = isDelete;
        this.isCancel = isCancel;
        this.vehicleStatus = vehicleStatus;
        this.actionTime = actionTime;
        this.userId = userId;
        this.branchId = branchId;
        this.dealerId = dealerId;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "Vehicle_Id", unique = true, nullable = false)
    public Integer getVehicleId() {
        return this.vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    @Column(name = "Loan_Id")
    public Integer getLoanId() {
        return this.loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    @Column(name = "Facility_No", length = 50)
    public String getFacilityNo() {
        return this.facilityNo;
    }

    public void setFacilityNo(String facilityNo) {
        this.facilityNo = facilityNo;
    }

    @Column(name = "Application_No", length = 50)
    public String getApplicationNo() {
        return this.applicationNo;
    }

    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    @Column(name = "Lot_No")
    public Integer getLotNo() {
        return this.lotNo;
    }

    public void setLotNo(Integer lotNo) {
        this.lotNo = lotNo;
    }

    @Column(name = "Sequence_No")
    public Integer getSequenceNo() {
        return this.sequenceNo;
    }

    public void setSequenceNo(Integer sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    @Column(name = "Is_Delivered")
    public Boolean getIsDelivered() {
        return this.isDelivered;
    }

    public void setIsDelivered(Boolean isDelivered) {
        this.isDelivered = isDelivered;
    }

    @Column(name = "Country_Of_Origin", length = 25)
    public String getCountryOfOrigin() {
        return this.countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "Purchase_Order_Date", length = 10)
    public Date getPurchaseOrderDate() {
        return this.purchaseOrderDate;
    }

    public void setPurchaseOrderDate(Date purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    @Column(name = "Delivery_Address", length = 200)
    public String getDeliveryAddress() {
        return this.deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "First_Reg_Date", length = 10)
    public Date getFirstRegDate() {
        return this.firstRegDate;
    }

    public void setFirstRegDate(Date firstRegDate) {
        this.firstRegDate = firstRegDate;
    }

    @Column(name = "Introducer_Commission", length = 100)
    public String getIntroducerCommission() {
        return this.introducerCommission;
    }

    public void setIntroducerCommission(String introducerCommission) {
        this.introducerCommission = introducerCommission;
    }

    @Column(name = "Commission_Amount", precision = 20)
    public Double getCommissionAmount() {
        return this.commissionAmount;
    }

    public void setCommissionAmount(Double commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    @Column(name = "Introducer_Code", length = 50)
    public String getIntroducerCode() {
        return this.introducerCode;
    }

    public void setIntroducerCode(String introducerCode) {
        this.introducerCode = introducerCode;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "Insurance_Start_Date", length = 10)
    public Date getInsuranceStartDate() {
        return this.insuranceStartDate;
    }

    public void setInsuranceStartDate(Date insuranceStartDate) {
        this.insuranceStartDate = insuranceStartDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "Insurance_Expiry_Date", length = 10)
    public Date getInsuranceExpiryDate() {
        return this.insuranceExpiryDate;
    }

    public void setInsuranceExpiryDate(Date insuranceExpiryDate) {
        this.insuranceExpiryDate = insuranceExpiryDate;
    }

    @Column(name = "Insurance_Premium", precision = 20)
    public Double getInsurancePremium() {
        return this.insurancePremium;
    }

    public void setInsurancePremium(Double insurancePremium) {
        this.insurancePremium = insurancePremium;
    }

    @Column(name = "Insurance_Amount", precision = 20)
    public Double getInsuranceAmount() {
        return this.insuranceAmount;
    }

    public void setInsuranceAmount(Double insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    @Column(name = "Insurance_Assigned")
    public Boolean getInsuranceAssigned() {
        return this.insuranceAssigned;
    }

    public void setInsuranceAssigned(Boolean insuranceAssigned) {
        this.insuranceAssigned = insuranceAssigned;
    }

    @Column(name = "Insurance_Policy_No", length = 50)
    public String getInsurancePolicyNo() {
        return this.insurancePolicyNo;
    }

    public void setInsurancePolicyNo(String insurancePolicyNo) {
        this.insurancePolicyNo = insurancePolicyNo;
    }

    @Column(name = "Insurance_Institute", length = 150)
    public String getInsuranceInstitute() {
        return this.insuranceInstitute;
    }

    public void setInsuranceInstitute(String insuranceInstitute) {
        this.insuranceInstitute = insuranceInstitute;
    }

    @Column(name = "Market_Price", precision = 20)
    public Double getMarketPrice() {
        return this.marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    @Column(name = "Provincial_Council", length = 50)
    public String getProvincialCouncil() {
        return this.provincialCouncil;
    }

    public void setProvincialCouncil(String provincialCouncil) {
        this.provincialCouncil = provincialCouncil;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "Vehicle_RegDate", length = 10)
    public Date getVehicleRegDate() {
        return this.vehicleRegDate;
    }

    public void setVehicleRegDate(Date vehicleRegDate) {
        this.vehicleRegDate = vehicleRegDate;
    }

    @Column(name = "Vehicle_Type", length = 50)
    public String getVehicleType() {
        return this.vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Column(name = "Vehicle_Make", length = 5)
    public String getVehicleMake() {
        return this.vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    @Column(name = "Vehicle_Model", length = 100)
    public String getVehicleModel() {
        return this.vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    @Column(name = "Vehicle_RegNo", length = 50)
    public String getVehicleRegNo() {
        return this.vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    @Column(name = "Status_When_Reg", length = 50)
    public String getStatusWhenReg() {
        return this.statusWhenReg;
    }

    public void setStatusWhenReg(String statusWhenReg) {
        this.statusWhenReg = statusWhenReg;
    }

    @Column(name = "Taxation_Class", length = 50)
    public String getTaxationClass() {
        return this.taxationClass;
    }

    public void setTaxationClass(String taxationClass) {
        this.taxationClass = taxationClass;
    }

    @Column(name = "Vehicle_Description", length = 200)
    public String getVehicleDescription() {
        return this.vehicleDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    @Column(name = "Vehicle_EngineNo", length = 50)
    public String getVehicleEngineNo() {
        return this.vehicleEngineNo;
    }

    public void setVehicleEngineNo(String vehicleEngineNo) {
        this.vehicleEngineNo = vehicleEngineNo;
    }

    @Column(name = "Vehicl_ChassisNo", length = 50)
    public String getVehiclChassisNo() {
        return this.vehiclChassisNo;
    }

    public void setVehiclChassisNo(String vehiclChassisNo) {
        this.vehiclChassisNo = vehiclChassisNo;
    }

    @Column(name = "Vehicle_Body_Color", length = 100)
    public String getVehicleBodyColor() {
        return this.vehicleBodyColor;
    }

    public void setVehicleBodyColor(String vehicleBodyColor) {
        this.vehicleBodyColor = vehicleBodyColor;
    }

    @Column(name = "Type_Of_Body", length = 50)
    public String getTypeOfBody() {
        return this.typeOfBody;
    }

    public void setTypeOfBody(String typeOfBody) {
        this.typeOfBody = typeOfBody;
    }

    @Column(name = "Weight", length = 50)
    public String getWeight() {
        return this.weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Column(name = "Wheel_Base", length = 50)
    public String getWheelBase() {
        return this.wheelBase;
    }

    public void setWheelBase(String wheelBase) {
        this.wheelBase = wheelBase;
    }

    @Column(name = "Manufactured_Year", length = 10)
    public String getManufacturedYear() {
        return this.manufacturedYear;
    }

    public void setManufacturedYear(String manufacturedYear) {
        this.manufacturedYear = manufacturedYear;
    }

    @Column(name = "Vehicle_RoundNo", length = 50)
    public String getVehicleRoundNo() {
        return this.vehicleRoundNo;
    }

    public void setVehicleRoundNo(String vehicleRoundNo) {
        this.vehicleRoundNo = vehicleRoundNo;
    }

    @Column(name = "Vehicle_Capacity", length = 200)
    public String getVehicleCapacity() {
        return this.vehicleCapacity;
    }

    public void setVehicleCapacity(String vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    @Column(name = "Cylinder_Capacity", length = 200)
    public String getCylinderCapacity() {
        return this.cylinderCapacity;
    }

    public void setCylinderCapacity(String cylinderCapacity) {
        this.cylinderCapacity = cylinderCapacity;
    }

    @Column(name = "Vehicle_Price", precision = 20)
    public Double getVehiclePrice() {
        return this.vehiclePrice;
    }

    public void setVehiclePrice(Double vehiclePrice) {
        this.vehiclePrice = vehiclePrice;
    }

    @Column(name = "IsActive")
    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Column(name = "IsDelete")
    public Boolean getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    @Column(name = "IsCancel")
    public Boolean getIsCancel() {
        return this.isCancel;
    }

    public void setIsCancel(Boolean isCancel) {
        this.isCancel = isCancel;
    }

    @Column(name = "Vehicle_Status")
    public Integer getVehicleStatus() {
        return this.vehicleStatus;
    }

    public void setVehicleStatus(Integer vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Action_Time", length = 19)
    public Date getActionTime() {
        return this.actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    @Column(name = "UserId")
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "BranchId")
    public Integer getBranchId() {
        return this.branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Column(name = "Dealer_Id")
    public Integer getDealerId() {
        return this.dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    @Transient
    public LoanHeaderDetails getLoanHeaderDetails() {
        return loanHeaderDetails;
    }

    public void setLoanHeaderDetails(LoanHeaderDetails loanHeaderDetails) {
        this.loanHeaderDetails = loanHeaderDetails;
    }

    @Transient
    public VehicleMake getvMake() {
        return vMake;
    }

    public void setvMake(VehicleMake vMake) {
        this.vMake = vMake;
    }

    @Transient
    public VehicleModel getvModel() {
        return vModel;
    }

    public void setvModel(VehicleModel vModel) {
        this.vModel = vModel;
    }

    @Transient
    public VehicleType getvType() {
        return vType;
    }

    public void setvType(VehicleType vType) {
        this.vType = vType;
    }

}

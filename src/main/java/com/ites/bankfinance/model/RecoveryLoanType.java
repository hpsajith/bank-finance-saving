package com.ites.bankfinance.model;
// Generated Aug 11, 2015 3:48:14 PM by Hibernate Tools 3.6.0


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * RecoveryLoanType generated by hbm2java
 */
@Entity
@Table(name="recovery_loan_type"
    ,catalog="axa_bank_finance"
)
public class RecoveryLoanType  implements java.io.Serializable {


     private Integer id;
     private Integer type;
     private Integer loanTypeId;

    public RecoveryLoanType() {
    }

    public RecoveryLoanType(Integer type, Integer loanTypeId) {
       this.type = type;
       this.loanTypeId = loanTypeId;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    
    @Column(name="Type")
    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }

    
    @Column(name="Loan_Type_Id")
    public Integer getLoanTypeId() {
        return this.loanTypeId;
    }
    
    public void setLoanTypeId(Integer loanTypeId) {
        this.loanTypeId = loanTypeId;
    }




}



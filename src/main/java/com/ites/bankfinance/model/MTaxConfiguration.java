package com.ites.bankfinance.model;
// Generated Feb 18, 2015 12:53:34 PM by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MTaxConfiguration generated by hbm2java
 */
@Entity
@Table(name="m_tax_configuration"
    ,catalog="axa_bank_finance"
)
public class MTaxConfiguration  implements java.io.Serializable {


     private Integer taxId;
     private String taxDescription;
     private String taxAccountNo;
     private Double taxRate;
     private Integer userId;
     private Date actionTime;

    public MTaxConfiguration() {
    }

    public MTaxConfiguration(String taxDescription, String taxAccountNo, Double taxRate, Integer userId, Date actionTime) {
       this.taxDescription = taxDescription;
       this.taxAccountNo = taxAccountNo;
       this.taxRate = taxRate;
       this.userId = userId;
       this.actionTime = actionTime;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Tax_Id", unique=true, nullable=false)
    public Integer getTaxId() {
        return this.taxId;
    }
    
    public void setTaxId(Integer taxId) {
        this.taxId = taxId;
    }

    
    @Column(name="Tax_Description", length=100)
    public String getTaxDescription() {
        return this.taxDescription;
    }
    
    public void setTaxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
    }

    
    @Column(name="Tax_Account_No", length=15)
    public String getTaxAccountNo() {
        return this.taxAccountNo;
    }
    
    public void setTaxAccountNo(String taxAccountNo) {
        this.taxAccountNo = taxAccountNo;
    }

    
    @Column(name="Tax_Rate", precision=22, scale=0)
    public Double getTaxRate() {
        return this.taxRate;
    }
    
    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    
    @Column(name="User_Id")
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="Action_Time", length=19)
    public Date getActionTime() {
        return this.actionTime;
    }
    
    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }




}



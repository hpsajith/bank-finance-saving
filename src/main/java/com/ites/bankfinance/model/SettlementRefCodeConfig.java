package com.ites.bankfinance.model;
// Generated May 19, 2015 2:10:13 PM by Hibernate Tools 3.6.0


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * SettlementRefCodeConfig generated by hbm2java
 */
@Entity
@Table(name="settlement_refcode_config"
    ,catalog="axa_bank_finance"
)
public class SettlementRefCodeConfig  implements java.io.Serializable {


     private Integer typeNo;
     private String name;
     private String description;
     private String accNo;

    public SettlementRefCodeConfig() {
    }

    public SettlementRefCodeConfig(String name, String description, String accNo) {
       this.name = name;
       this.description = description;
       this.accNo = accNo;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Type_No", unique=true, nullable=false)
    public Integer getTypeNo() {
        return this.typeNo;
    }
    
    public void setTypeNo(Integer typeNo) {
        this.typeNo = typeNo;
    }

    
    @Column(name="Name", length=250)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="Description", length=250)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="AccNo", length=20)
    public String getAccNo() {
        return this.accNo;
    }
    
    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }




}



package com.ites.bankfinance.model;
// Generated Feb 27, 2015 3:25:06 PM by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SettlementChequeDetails generated by hbm2java
 */
@Entity
@Table(name="settlement_cheque_details"
    ,catalog="axa_bank_finance"
)
public class SettlementChequeDetails  implements java.io.Serializable {


     private Integer cheqId;
     private Integer loanId;
     private String cheqNo;
     private Integer bankId;
     private Double cheqAmount;
     private String cheqAmountText;
     private Date cheqRenewalDate;
     private Date cheqIssueDate;
     private Date actionTime;
     private Integer userId;
     private Integer branchId;

    public SettlementChequeDetails() {
    }

    public SettlementChequeDetails(Integer loanId, String cheqNo, Integer bankId, String bankBranch, Double cheqAmount,String cheqAmountText, Date cheqRenewalDate, Date cheqIssueDate, Date actionTime, Integer userId, Integer branchId) {
       this.loanId = loanId;
       this.cheqNo = cheqNo;
       this.bankId = bankId;
       this.cheqAmount = cheqAmount;
       this.cheqAmountText = cheqAmountText;
       this.cheqRenewalDate = cheqRenewalDate;
       this.cheqIssueDate = cheqIssueDate;
       this.actionTime = actionTime;
       this.userId = userId;
       this.branchId = branchId;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Cheq_Id", unique=true, nullable=false)
    public Integer getCheqId() {
        return this.cheqId;
    }
    
    public void setCheqId(Integer cheqId) {
        this.cheqId = cheqId;
    }

    
    @Column(name="Loan_Id")
    public Integer getLoanId() {
        return this.loanId;
    }
    
    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    
    @Column(name="Cheq_No", length=50)
    public String getCheqNo() {
        return this.cheqNo;
    }
    
    public void setCheqNo(String cheqNo) {
        this.cheqNo = cheqNo;
    }

    
    @Column(name="Bank_Id")
    public Integer getBankId() {
        return this.bankId;
    }
    
    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    
    @Column(name="Cheq_Amount", precision=10)
    public Double getCheqAmount() {
        return this.cheqAmount;
    }
    
    public void setCheqAmount(Double cheqAmount) {
        this.cheqAmount = cheqAmount;
    }
    
    @Column(name="Cheq_Amount_Text", length=200)
    public String getCheqAmountText() {
        return this.cheqAmountText;
    }
    
    public void setCheqAmountText(String cheqAmountText) {
        this.cheqAmountText = cheqAmountText;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="Cheq_Renewal_Date", length=10)
    public Date getCheqRenewalDate() {
        return this.cheqRenewalDate;
    }
    
    public void setCheqRenewalDate(Date cheqRenewalDate) {
        this.cheqRenewalDate = cheqRenewalDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="Cheq_Issue_Date", length=10)
    public Date getCheqIssueDate() {
        return this.cheqIssueDate;
    }
    
    public void setCheqIssueDate(Date cheqIssueDate) {
        this.cheqIssueDate = cheqIssueDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="Action_Time", length=19)
    public Date getActionTime() {
        return this.actionTime;
    }
    
    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    
    @Column(name="User_Id")
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    
    @Column(name="Branch_Id")
    public Integer getBranchId() {
        return this.branchId;
    }
    
    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }




}



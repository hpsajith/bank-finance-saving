package com.ites.bankfinance.model;
// Generated Jan 14, 2016 1:52:15 PM by Hibernate Tools 3.6.0


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SmsType generated by hbm2java
 */
@Entity
@Table(name="sms_type"
    ,catalog="axa_bank_finance"
)
public class SmsType  implements java.io.Serializable {


     private int smsTypeId;
     private String smsTypeName;
     private String smsDescription;
     private Set<SmsDetails> smsDetailses = new HashSet<SmsDetails>(0);

    public SmsType() {
    }

	
    public SmsType(int smsTypeId) {
        this.smsTypeId = smsTypeId;
    }
    public SmsType(int smsTypeId, String smsTypeName, String smsDescription, Set<SmsDetails> smsDetailses) {
       this.smsTypeId = smsTypeId;
       this.smsTypeName = smsTypeName;
       this.smsDescription = smsDescription;
       this.smsDetailses = smsDetailses;
    }
   
     @Id 

    
    @Column(name="SMS_TYPE_ID", unique=true, nullable=false)
    public int getSmsTypeId() {
        return this.smsTypeId;
    }
    
    public void setSmsTypeId(int smsTypeId) {
        this.smsTypeId = smsTypeId;
    }

    
    @Column(name="SMS_TYPE_NAME", length=100)
    public String getSmsTypeName() {
        return this.smsTypeName;
    }
    
    public void setSmsTypeName(String smsTypeName) {
        this.smsTypeName = smsTypeName;
    }

    
    @Column(name="SMS_DESCRIPTION")
    public String getSmsDescription() {
        return this.smsDescription;
    }
    
    public void setSmsDescription(String smsDescription) {
        this.smsDescription = smsDescription;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="smsType")
    public Set<SmsDetails> getSmsDetailses() {
        return this.smsDetailses;
    }
    
    public void setSmsDetailses(Set<SmsDetails> smsDetailses) {
        this.smsDetailses = smsDetailses;
    }




}



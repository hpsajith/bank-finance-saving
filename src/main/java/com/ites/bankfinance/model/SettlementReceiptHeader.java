package com.ites.bankfinance.model;
// Generated May 19, 2015 2:10:13 PM by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SettlementReceiptHeader generated by hbm2java
 */
@Entity
@Table(name="settlement_receipt_header"
    ,catalog="axa_bank_finance"
)
public class SettlementReceiptHeader  implements java.io.Serializable {


     private Integer refNo;
     private Integer debtorId;
     private Integer rcptNo;
     private Date rcptDate;
     private double paidAmount;
     private String uc;
     private Date actionDate;
     private Double amount;
     private Double chequeId;
     private Boolean isWrite;
     private double cash;
     private Double bankDepositId;

    public SettlementReceiptHeader() {
    }

	
    public SettlementReceiptHeader(double paidAmount, String uc, Date actionDate, double cash) {
        this.paidAmount = paidAmount;
        this.uc = uc;
        this.actionDate = actionDate;
        this.cash = cash;
    }
    public SettlementReceiptHeader(Integer debtorId, Integer rcptNo, Date rcptDate, double paidAmount, String uc, Date actionDate, Double amount, Double chequeId, Boolean isWrite, double cash, Double bankDepositId) {
       this.debtorId = debtorId;
       this.rcptNo = rcptNo;
       this.rcptDate = rcptDate;
       this.paidAmount = paidAmount;
       this.uc = uc;
       this.actionDate = actionDate;
       this.amount = amount;
       this.chequeId = chequeId;
       this.isWrite = isWrite;
       this.cash = cash;
       this.bankDepositId = bankDepositId;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Ref_No", unique=true, nullable=false)
    public Integer getRefNo() {
        return this.refNo;
    }
    
    public void setRefNo(Integer refNo) {
        this.refNo = refNo;
    }

    
    @Column(name="Debtor_Id")
    public Integer getDebtorId() {
        return this.debtorId;
    }
    
    public void setDebtorId(Integer debtorId) {
        this.debtorId = debtorId;
    }

    
    @Column(name="Rcpt_No")
    public Integer getRcptNo() {
        return this.rcptNo;
    }
    
    public void setRcptNo(Integer rcptNo) {
        this.rcptNo = rcptNo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="Rcpt_Date", length=10)
    public Date getRcptDate() {
        return this.rcptDate;
    }
    
    public void setRcptDate(Date rcptDate) {
        this.rcptDate = rcptDate;
    }

    
    @Column(name="Paid_Amount", nullable=false, precision=20)
    public double getPaidAmount() {
        return this.paidAmount;
    }
    
    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    
    @Column(name="UC", nullable=false, length=50)
    public String getUc() {
        return this.uc;
    }
    
    public void setUc(String uc) {
        this.uc = uc;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="ActionDate", nullable=false, length=19)
    public Date getActionDate() {
        return this.actionDate;
    }
    
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    
    @Column(name="Amount", precision=20)
    public Double getAmount() {
        return this.amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    
    @Column(name="Cheque_Id", precision=20)
    public Double getChequeId() {
        return this.chequeId;
    }
    
    public void setChequeId(Double chequeId) {
        this.chequeId = chequeId;
    }

    
    @Column(name="IsWrite")
    public Boolean getIsWrite() {
        return this.isWrite;
    }
    
    public void setIsWrite(Boolean isWrite) {
        this.isWrite = isWrite;
    }

    
    @Column(name="Cash", nullable=false, precision=20)
    public double getCash() {
        return this.cash;
    }
    
    public void setCash(double cash) {
        this.cash = cash;
    }

    
    @Column(name="Bank_Deposit_Id", precision=22, scale=0)
    public Double getBankDepositId() {
        return this.bankDepositId;
    }
    
    public void setBankDepositId(Double bankDepositId) {
        this.bankDepositId = bankDepositId;
    }




}



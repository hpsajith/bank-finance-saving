/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author itess-nb
 */
@Entity
@Table(name = "m_center")
@NamedQueries({
    @NamedQuery(name = "MCenter.findAll", query = "SELECT m FROM MCenter m")})
public class MCenter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "center_ID")
    private Integer centerID;
    @Column(name = "branch_ID")
    private Integer branchID;
    @Column(name = "center_code")
    private String centerCode;
    @Column(name = "center_name")
    private String centerName;
    @Lob
    @Column(name = "description")
    private String description;
    @Column(name = "hotline")
    private String hotline;
    @Column(name = "isActive")
    private Integer isActive;
    @Column(name = "userId")
    private Integer userId;
    @Column(name = "action_time")
    private String actionTime;
    
 

    public MCenter() {
    }

    public MCenter(Integer centerID) {
        this.centerID = centerID;
    }

    public Integer getCenterID() {
        return centerID;
    }

    public void setCenterID(Integer centerID) {
        this.centerID = centerID;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHotline() {
        return hotline;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getActionTime() {
        return actionTime;
    }

    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (centerID != null ? centerID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MCenter)) {
            return false;
        }
        MCenter other = (MCenter) object;
        if ((this.centerID == null && other.centerID != null) || (this.centerID != null && !this.centerID.equals(other.centerID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pa.MCenter[ centerID=" + centerID + " ]";
    }
    
}

package com.ites.bankfinance.model;
// Generated Apr 28, 2015 11:26:13 AM by Hibernate Tools 3.6.0


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ASubtab generated by hbm2java
 */
@Entity
@Table(name="a_subtab"
    ,catalog="axa_bank_finance"
)
public class ASubtab  implements java.io.Serializable {


     private Integer subTabNo;
     private int mainTabId;
     private String subTabName;
     private String subTabCode;
     private String refPage;
     private Boolean isActive;
     private Boolean isDelete;
     private Integer ordertab;

    public ASubtab() {
    }

	
    public ASubtab(int mainTabId, String subTabName) {
        this.mainTabId = mainTabId;
        this.subTabName = subTabName;
    }
    public ASubtab(int mainTabId, String subTabName, String subTabCode, String refPage, Boolean isActive, Boolean isDelete, Integer ordertab) {
       this.mainTabId = mainTabId;
       this.subTabName = subTabName;
       this.subTabCode = subTabCode;
       this.refPage = refPage;
       this.isActive = isActive;
       this.isDelete = isDelete;
       this.ordertab = ordertab;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Sub_Tab_No", unique=true, nullable=false)
    public Integer getSubTabNo() {
        return this.subTabNo;
    }
    
    public void setSubTabNo(Integer subTabNo) {
        this.subTabNo = subTabNo;
    }

    
    @Column(name="Main_Tab_ID", nullable=false)
    public int getMainTabId() {
        return this.mainTabId;
    }
    
    public void setMainTabId(int mainTabId) {
        this.mainTabId = mainTabId;
    }

    
    @Column(name="Sub_Tab_Name", nullable=false, length=100)
    public String getSubTabName() {
        return this.subTabName;
    }
    
    public void setSubTabName(String subTabName) {
        this.subTabName = subTabName;
    }

    
    @Column(name="Sub_Tab_Code", length=50)
    public String getSubTabCode() {
        return this.subTabCode;
    }
    
    public void setSubTabCode(String subTabCode) {
        this.subTabCode = subTabCode;
    }

    
    @Column(name="RefPage")
    public String getRefPage() {
        return this.refPage;
    }
    
    public void setRefPage(String refPage) {
        this.refPage = refPage;
    }

    
    @Column(name="isActive")
    public Boolean getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    
    @Column(name="isDelete")
    public Boolean getIsDelete() {
        return this.isDelete;
    }
    
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    
    @Column(name="Ordertab")
    public Integer getOrdertab() {
        return this.ordertab;
    }
    
    public void setOrdertab(Integer ordertab) {
        this.ordertab = ordertab;
    }




}



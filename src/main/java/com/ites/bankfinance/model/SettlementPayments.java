/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.DATE;

/**
 *
 * @author Administrator
 */


@Entity
@Table(name="settlement_payment"
    ,catalog="axa_bank_finance"
)
public class SettlementPayments implements java.io.Serializable{
    
    
    private int transactionId;
    private String transactionNo;
    private int debtorId;
    private int paymentType;
    private double paymentAmount;
    private int loanId;
    private int userId;
    private String accountNo;
    private int discountId;
    private int branchId;
    private String receiptNo;
    private int paymentDetails;
    private String remark;
    private int isDownPayment;
    private int isRebatePayment;
    private int chequeId;
    private int directDepositeId;
    private Date actionDate;
    private int isDelete;
    private Date paymentDate;
    private String amountText;
    private Date systemDate;
    
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="Transaction_Id", unique=true, nullable=false)
    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    @Column(name="Transaction_No")
    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    @Column(name="Debtor_Id")
    public int getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(int debtorId) {
        this.debtorId = debtorId;
    }

    @Column(name="Payment_Type")
    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    @Column(name="Payment_Amount")
    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    @Column(name="Loan_Id")
    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    @Column(name="User_Id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
         @Column(name="Payment_Date")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Column(name="Account_No")
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Column(name="Discount_Id")
    public int getDiscountId() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    @Column(name="Branch_Id")
    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    @Column(name="Receipt_No")
    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    @Column(name="Payment_Detail_Id")
    public int getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(int paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    @Column(name="Remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(name="Is_Down_Payment")
    public int getIsDownPayment() {
        return isDownPayment;
    }

    public void setIsDownPayment(int isDownPayment) {
        this.isDownPayment = isDownPayment;
    }

    @Column(name="Is_Rebate_Payment")
    public int getIsRebatePayment() {
        return isRebatePayment;
    }

    public void setIsRebatePayment(int isRebatePayment) {
        this.isRebatePayment = isRebatePayment;
    }

    @Column(name="Chaque_Id")
    public int getChequeId() {
        return chequeId;
    }

    public void setChequeId(int chequeId) {
        this.chequeId = chequeId;
    }

    @Column(name="Direct_Deposit_Id")
    public int getDirectDepositeId() {
        return directDepositeId;
    }

    public void setDirectDepositeId(int directDepositeId) {
        this.directDepositeId = directDepositeId;
    }

    @Column(name="Action_Date")
    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    @Column(name="Is_Delete")
    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }    
    
    @Column(name="Amount_Text")
    public String getAmountText() {
        return amountText;
    }

    public void setAmountText(String amountText) {
        this.amountText = amountText;
    }
    
       @Temporal(DATE)
    @Column(name="System_Date")
    public Date getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(Date systemDate) {
        this.systemDate = systemDate;
    }
}

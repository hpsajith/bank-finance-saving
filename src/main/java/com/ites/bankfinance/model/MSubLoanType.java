package com.ites.bankfinance.model;
// Generated Apr 6, 2016 9:35:36 AM by Hibernate Tools 3.6.0

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MSubLoanType generated by hbm2java
 */
@Entity
@Table(name = "m_sub_loan_type", catalog = "axa_bank_finance"
)
public class MSubLoanType implements java.io.Serializable {

    private Integer subLoanId;
    private String subLoanName;
    private String subLoanCode;
    private Integer loanTypeId;
    private Double subInterestRate;
    private Integer userId;
    private Integer maxCount = 0;
    private Boolean isInsurance = false;
    private Boolean isActive = false;
    private Integer branchId;
    private Date actionTime;

    public MSubLoanType() {
    }

    public MSubLoanType(String subLoanName, String subLoanCode, Integer loanTypeId, Double subInterestRate, Integer userId, Integer maxCount, Boolean isInsurance, Boolean isActive, Integer branchId, Date actionTime) {
        this.subLoanName = subLoanName;
        this.subLoanCode = subLoanCode;
        this.loanTypeId = loanTypeId;
        this.subInterestRate = subInterestRate;
        this.userId = userId;
        this.maxCount = maxCount;
        this.isInsurance = isInsurance;
        this.isActive = isActive;
        this.branchId = branchId;
        this.actionTime = actionTime;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "Sub_Loan_Id", unique = true, nullable = false)
    public Integer getSubLoanId() {
        return this.subLoanId;
    }

    public void setSubLoanId(Integer subLoanId) {
        this.subLoanId = subLoanId;
    }

    @Column(name = "Sub_Loan_Name", length = 30)
    public String getSubLoanName() {
        return this.subLoanName;
    }

    public void setSubLoanName(String subLoanName) {
        this.subLoanName = subLoanName;
    }

    @Column(name = "Sub_Loan_Code", length = 5)
    public String getSubLoanCode() {
        return this.subLoanCode;
    }

    public void setSubLoanCode(String subLoanCode) {
        this.subLoanCode = subLoanCode;
    }

    @Column(name = "Loan_Type_Id")
    public Integer getLoanTypeId() {
        return this.loanTypeId;
    }

    public void setLoanTypeId(Integer loanTypeId) {
        this.loanTypeId = loanTypeId;
    }

    @Column(name = "Sub_Interest_Rate", precision = 5)
    public Double getSubInterestRate() {
        return this.subInterestRate;
    }

    public void setSubInterestRate(Double subInterestRate) {
        this.subInterestRate = subInterestRate;
    }

    @Column(name = "User_Id")
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "Is_Insurance")
    public Boolean getIsInsurance() {
        return this.isInsurance;
    }

    public void setIsInsurance(Boolean isInsurance) {
        this.isInsurance = isInsurance;
    }

    @Column(name = "Max_Count")
    public Integer getMaxCount() {
        return this.maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    @Column(name = "Is_Active")
    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Column(name = "Branch_Id")
    public Integer getBranchId() {
        return this.branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Action_Time", length = 19)
    public Date getActionTime() {
        return this.actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

}

package com.ites.bankfinance.model;
// Generated Mar 31, 2015 4:56:54 PM by Hibernate Tools 3.6.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * UmUser generated by hbm2java
 */
@Entity
@Table(name = "um_user", catalog = "axa_bank_finance"
)
public class UmUser implements java.io.Serializable {

    private Integer userId;
    private int empId;
    private int userTypeId;
    private String userName;
    private String password;
    private Boolean isActive = true;
    private Boolean isBlock = false;
    private Boolean isDelete = false;
    private Date actionTime;
    private Integer UId;
    private Set<LoanRescheduleDetails> loanRescheduleDetailses = new HashSet<>(0);
    private Set<Announcement> announcements = new HashSet<>(0);
    private Set<UserMessage> userMessagesForUId = new HashSet<>(0);
    private Set<UserMessage> userMessagesForUserId = new HashSet<>(0);
    private Set<LoanMessage> loanMessages = new HashSet<>(0);

    public UmUser() {
    }

    public UmUser(int userId) {
        this.userId = userId;
    }

    public UmUser(int empId, int userTypeId) {
        this.empId = empId;
        this.userTypeId = userTypeId;
    }

    public UmUser(int empId, int userTypeId, String userName, String password, Boolean isActive, Boolean isBlock, Boolean isDelete, Date actionTime, Integer UId) {
        this.empId = empId;
        this.userTypeId = userTypeId;
        this.userName = userName;
        this.password = password;
        this.isActive = isActive;
        this.isBlock = isBlock;
        this.isDelete = isDelete;
        this.actionTime = actionTime;
        this.UId = UId;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "User_ID", unique = true, nullable = false)
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "Emp_ID", nullable = false)
    public int getEmpId() {
        return this.empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    @Column(name = "User_Type_ID", nullable = false)
    public int getUserTypeId() {
        return this.userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Column(name = "User_Name", length = 25)
    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "password")
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "isActive")
    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Column(name = "isBlock")
    public Boolean isIsBlock() {
        return isBlock;
    }

    public void setIsBlock(Boolean isBlock) {
        this.isBlock = isBlock;
    }

    @Column(name = "isDelete")
    public Boolean getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Action_Time", length = 19)
    public Date getActionTime() {
        return this.actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    @Column(name = "U_ID")
    public Integer getUId() {
        return this.UId;
    }

    public void setUId(Integer UId) {
        this.UId = UId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "umUser")
    public Set<LoanRescheduleDetails> getLoanRescheduleDetailses() {
        return this.loanRescheduleDetailses;
    }

    public void setLoanRescheduleDetailses(Set<LoanRescheduleDetails> loanRescheduleDetailses) {
        this.loanRescheduleDetailses = loanRescheduleDetailses;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "umUser")
    public Set<Announcement> getAnnouncements() {
        return this.announcements;
    }

    public void setAnnouncements(Set<Announcement> announcements) {
        this.announcements = announcements;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "umUserByUId")
    public Set<UserMessage> getUserMessagesForUId() {
        return this.userMessagesForUId;
    }

    public void setUserMessagesForUId(Set<UserMessage> userMessagesForUId) {
        this.userMessagesForUId = userMessagesForUId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "umUserByUserId")
    public Set<UserMessage> getUserMessagesForUserId() {
        return this.userMessagesForUserId;
    }

    public void setUserMessagesForUserId(Set<UserMessage> userMessagesForUserId) {
        this.userMessagesForUserId = userMessagesForUserId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "umUser")
    public Set<LoanMessage> getLoanMessages() {
        return this.loanMessages;
    }

    public void setLoanMessages(Set<LoanMessage> loanMessages) {
        this.loanMessages = loanMessages;
    }

}

package com.ites.bankfinance.model;
// Generated Jun 22, 2015 4:16:04 PM by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * DayEndConfig generated by hbm2java
 */
@Entity
@Table(name="day_end_config"
    ,catalog="axa_bank_finance"
)
public class DayEndConfig  implements java.io.Serializable {


     private Integer configId;
     private Integer processId;
     private Boolean processStatus;
     private Date dayEndDate;
     private Integer branchId;
     private Integer userId;
     private Date actionTime;

    public DayEndConfig() {
    }

	
    public DayEndConfig(Date actionTime) {
        this.actionTime = actionTime;
    }
    public DayEndConfig(Integer processId, Boolean processStatus, Date dayEndDate, Integer branchId, Integer userId, Date actionTime) {
       this.processId = processId;
       this.processStatus = processStatus;
       this.dayEndDate = dayEndDate;
       this.branchId = branchId;
       this.userId = userId;
       this.actionTime = actionTime;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="Config_Id", unique=true, nullable=false)
    public Integer getConfigId() {
        return this.configId;
    }
    
    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    
    @Column(name="Process_Id")
    public Integer getProcessId() {
        return this.processId;
    }
    
    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    
    @Column(name="Process_Status")
    public Boolean getProcessStatus() {
        return this.processStatus;
    }
    
    public void setProcessStatus(Boolean processStatus) {
        this.processStatus = processStatus;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="Day_End_Date", length=10)
    public Date getDayEndDate() {
        return this.dayEndDate;
    }
    
    public void setDayEndDate(Date dayEndDate) {
        this.dayEndDate = dayEndDate;
    }

    
    @Column(name="Branch_Id")
    public Integer getBranchId() {
        return this.branchId;
    }
    
    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    
    @Column(name="User_Id")
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="Action_Time", length=19)
    public Date getActionTime() {
        return this.actionTime;
    }
    
    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }




}



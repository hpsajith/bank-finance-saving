/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.model;

/**
 *
 * @author ITESS
 */
public class ReturnList {
    private int arreasTypeId;
    private double amount;
    private String arreasDescription;

    public ReturnList(int arreasTypeId, double amount, String arreasDescription) {
        this.arreasTypeId = arreasTypeId;
        this.amount = amount;
        this.arreasDescription = arreasDescription;
    }

    
    
    public int getArreasTypeId() {
        return arreasTypeId;
    }

    public void setArreasTypeId(int arreasTypeId) {
        this.arreasTypeId = arreasTypeId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getArreasDescription() {
        return arreasDescription;
    }

    public void setArreasDescription(String arreasDescription) {
        this.arreasDescription = arreasDescription;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author ITESS
 */
@Entity
@Table(name = "recovery_shift")
public class RecoveryShift implements java.io.Serializable {

    
    private Integer recoverId;    
    private Integer loanId;    
    private String dueDate;   
    private String shiftDate;    
    private String remark;    
    private Integer userId;    
    private Date actionTime;
    private Integer instmntId;

    
    public RecoveryShift() {
    }

    public RecoveryShift(Integer recoverId) {
        this.recoverId = recoverId;
    }

    public RecoveryShift(Integer recoverId, Date actionTime) {
        this.recoverId = recoverId;
        this.actionTime = actionTime;
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recover_Id")
    public Integer getRecoverId() {
        return recoverId;
    }

    public void setRecoverId(Integer recoverId) {
        this.recoverId = recoverId;
    }

    @Column(name = "loan_Id")
    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    @Column(name = "due_date")
    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    @Column(name = "shift_date")
    public String getShiftDate() {
        return shiftDate;
    }

    public void setShiftDate(String shiftDate) {
        this.shiftDate = shiftDate;
    }

    @Size(max = 150)
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(name = "user_Id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "Action_Time")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }
    
    @Column(name = "Instmnt_Id")
    public Integer getInstmntId() {
        return instmntId;
    }

    public void setInstmntId(Integer instmntId) {
        this.instmntId = instmntId;
    }

    
}

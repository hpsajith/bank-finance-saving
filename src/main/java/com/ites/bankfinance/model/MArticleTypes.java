package com.ites.bankfinance.model;
// Generated Aug 11, 2015 8:34:40 AM by Hibernate Tools 3.6.0


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * MArticleTypes generated by hbm2java
 */
@Entity
@Table(name="m_article_types"
    ,catalog="axa_bank_finance"
)
public class MArticleTypes  implements java.io.Serializable {


     private Integer AId;
     private String AType;
     private String ACode;

    public MArticleTypes() {
    }

    public MArticleTypes(String AType, String ACode) {
       this.AType = AType;
       this.ACode = ACode;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="A_Id", unique=true, nullable=false)
    public Integer getAId() {
        return this.AId;
    }
    
    public void setAId(Integer AId) {
        this.AId = AId;
    }

    
    @Column(name="A_Type")
    public String getAType() {
        return this.AType;
    }
    
    public void setAType(String AType) {
        this.AType = AType;
    }

    
    @Column(name="A_Code", length=50)
    public String getACode() {
        return this.ACode;
    }
    
    public void setACode(String ACode) {
        this.ACode = ACode;
    }




}



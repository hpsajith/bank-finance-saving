package com.ites.bankfinance.controller;

import com.bankfinance.form.JobDefine;
import com.ites.bankfinance.config.init.SessionListner;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MCompany;
import com.ites.bankfinance.model.MSection;
import com.ites.bankfinance.model.MSystemValidation;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserLog;
import com.ites.bankfinance.model.UserMessage;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import com.ites.bankfinance.service.UtilityService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class InitController {

    @Autowired
    private MasterDataService masterService;
    @Autowired
    private UserService userService;
    @Autowired
    private SettlmentService settlmentService;
    @Autowired
    private UtilityService utilityService;
    @Autowired
    private AdminService adminService;

    @RequestMapping(value = {"/", "/welcome**"}, method = RequestMethod.GET)
    public String welcomePage() {
        return "redirect:/admin";
    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        masterService.setUserLogin(branchId);
        List<JobDefine> jobList = masterService.findUserJobs();
        String branch_Name = userService.findWorkingBranchName(branchId);
        Date system_Date = settlmentService.getSystemDate(branchId);
        MCompany mCompany = userService.findCompanyDetails();
        String comFName = mCompany.getComName();
        String comLName = mCompany.getComLname();
        String company_Name = comFName + comLName;
        String com_TellNo = mCompany.getTelephone();
        String com_Email = mCompany.getEmailAdd();

        int userId = userService.findByUserName();
        List<UserMessage> userMessages = utilityService.findUserMessages(userId);
        int userType = userService.findUserTypeByUserId(userId);
        if (userType == 2) {
            boolean loanChequesExist = settlmentService.isLoanChequesExist(system_Date);
            model.addObject("loanChequesExist", loanChequesExist);
        }
        model.addObject("title", "Spring Security Custom Login Form");
        model.addObject("message", "This is protected page!");
        model.addObject("jobList", jobList);
        model.addObject("branch", branch_Name);
        model.addObject("systemDate", system_Date);
        model.addObject("companyName", company_Name);
        model.addObject("comTellNo", com_TellNo);
        model.addObject("comEmail", com_Email);
        model.addObject("userMessages", userMessages);
        model.setViewName("bankFinance/admin");

        return model;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }
        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("bankFinance/login");
        return model;

    }

    // for 403 access denied page
    @RequestMapping(value = "/invalideLogin", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {

        ModelAndView model = new ModelAndView();

        // check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();

            model.addObject("username", userDetail.getUsername());

        }

        model.setViewName("bankFinance/invalideLogin");
        return model;

    }

    @RequestMapping(value = "/loadUserBranches/{username}", method = RequestMethod.GET)
    @ResponseBody
    public List<MBranch> loadUserBranches(@PathVariable("username") String username) {

        String password = "";
        List<MBranch> branchList = masterService.findBranchesByUsername(username, password);
        return branchList;
    }

    @RequestMapping(value = "/setBranchId/{branchId}/{branchName}", method = RequestMethod.GET)
    @ResponseBody
    public int setBranchId(@PathVariable("branchId") int branchId, @PathVariable("branchName") String branchName, HttpServletRequest request) {
        request.getSession().setAttribute("branchId", branchId);
        request.getSession().setAttribute("branchName", branchName);
        int loginUserCount = 0;
        int maxUserCount = 0;
        MSystemValidation mUserCount = adminService.findSystemValidation();
        if (mUserCount != null) {
            maxUserCount = mUserCount.getMaxUserCount();
        }
        loginUserCount = SessionListner.getTotalActiveSession();
        if (loginUserCount > maxUserCount) {
            return 0;
        }
        return 1;
    }

    @RequestMapping(value = "/inactiveUserLog", method = RequestMethod.GET)
    @ResponseBody
    public int inactiveUserLog() {
        masterService.inactiveUser();
        return 1;
    }

    // This method is used to view company logo
    @RequestMapping(value = "/viewCompanyLogo", method = RequestMethod.GET, produces = "image/png")
    @ResponseBody
    public byte[] viewCompanyLogo(HttpServletRequest request, Model model) {
        byte[] imageInByte = null;
        try {
            MCompany mCompany = userService.findCompanyDetails();
            if (mCompany != null) {
                imageInByte = mCompany.getComLogo();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageInByte;
    }

    @RequestMapping(value = "/findBranch/{branchId}", method = RequestMethod.GET)
    @ResponseBody
    public MBranch findBranch(@PathVariable("branchId") int branchId, HttpServletRequest request) {
        MBranch branch = null;
        try {
            branch = masterService.findBranch(branchId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branch;
    }

    @RequestMapping(value = "/findSections", method = RequestMethod.GET)
    @ResponseBody
    public List<MSection> findSections(HttpServletRequest request) {
        List<MSection> sections = null;
        try {
            sections = masterService.findSections();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sections;
    }

    @RequestMapping(value = "/authenticate/{userName}/{password}", method = RequestMethod.GET)
    @ResponseBody
    public HashMap authenticate(@PathVariable("userName") String userName, @PathVariable("password") String password, HttpServletRequest request) {
        HashMap hashMap = null;
        try {
            UmUser authenicateUser = masterService.authenticate(userName, password);
            if (authenicateUser != null) {
                List<MSection> sections = masterService.findSections();
                hashMap = new HashMap();
                hashMap.put("exist", true);
                hashMap.put("userId", authenicateUser.getUserId());
                hashMap.put("sections", sections);
            } else {
                hashMap = new HashMap();
                hashMap.put("exist", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    @RequestMapping(value = "/loadUserBranches/{userId}/{sectionId}", method = RequestMethod.GET)
    @ResponseBody
    public List<MBranch> loadUserBranches(@PathVariable("userId") int userId, @PathVariable("sectionId") int sectionId, HttpServletRequest request) {
        List<MBranch> branchs = null;
        try {
            branchs = masterService.loadUserBranches(userId, sectionId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchs;
    }

}

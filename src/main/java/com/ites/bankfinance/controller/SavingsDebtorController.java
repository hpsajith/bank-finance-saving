package com.ites.bankfinance.controller;

import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.savingsModel.DebtorHeaderDetails;
import com.ites.bankfinance.savingsModel.MSavingsType;
import com.ites.bankfinance.savingsModel.MSubSavingsType;
import com.ites.bankfinance.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by AXA-FINANCE on 5/24/2018.
 */
@Controller
@RequestMapping(value = "/SavingsDebtorController")
public class SavingsDebtorController {
    @Autowired
    private AdminService adminService;

    @Autowired
    private SavingsDebtorService savingsDebtorService;

    @Autowired
    LoanService loanService;

//    @Autowired
//    private DebtorService debtorService;


    @RequestMapping(value = "/loadSavingsDebtorSearch", method = RequestMethod.GET)
    public ModelAndView loadSavingsDebtorSearch(Model model) {

        return new ModelAndView("savingsDebtor/savingsDebtor");
    }

    //    @Autowired
//    private SavingsService savingsService;
    @Autowired
    private MasterDataService masterService;

    @Autowired
    SettlmentService settlmentService;

    @RequestMapping(value = "/savingsDebtorSave", method = RequestMethod.POST)
    public ModelAndView savingsDebtorSave(@ModelAttribute("debtorForm") DebtorHeaderDetails formData, BindingResult result, Model model, HttpServletRequest request) {

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        formData.setDebtorIsDebtor(true);
        formData.setBranchId(branchId);

        MBranch currentBranch = null;
        currentBranch = adminService.findActiveBranch(branchId);
        String branchCode = currentBranch.getBranchCode();

        int debtorId = savingsDebtorService.saveDebtorDetails(formData);
        model.addAttribute("id", debtorId);
        model.addAttribute("branchCode", branchCode);
        model.addAttribute("edit", true);

        return new ModelAndView("savingsDebtor/savingsNewCustomer", "debtor", new DebtorHeaderDetails());
    }

    @RequestMapping(value = "/savingsNewCustomer", method = RequestMethod.GET)
    public ModelAndView savingsNewCustomer(Model model, HttpServletRequest request) {
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = (int) bID;
        MBranch currentBranch = null;
        currentBranch = adminService.findActiveBranch(branchId);
        String branchCode = currentBranch.getBranchCode();
        model.addAttribute("branchCode", branchCode);
        return new ModelAndView("savingsDebtor/savingsNewCustomer", "debtorForm", new DebtorHeaderDetails());
    }

    @RequestMapping(value = "/newSavingForm", method = RequestMethod.GET)
    public ModelAndView newSavingForm(Model model, HttpServletRequest request) {
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = (int) bID;
//        int branchId = 1;

        // loading branched
        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);

        List<MSavingsType> savingsType = masterService.findAllSavingsTypes();
        List<MSubSavingsType> subSubSavingsType = masterService.findAllSubSavingsTypes();
//        List<MSubSavingsCharges> subSavingsCharges = masterService.findSavingCharges();

        MBranch currentBranch = null;
        currentBranch = adminService.findActiveBranch(branchId);
        String branchCode = currentBranch.getBranchCode();

        Date systemDate = settlmentService.getSystemDate(branchId);

        model.addAttribute("savingTypeList", savingsType);
        model.addAttribute("savingSubTypesList", subSubSavingsType);
        model.addAttribute("branchCode", branchCode);
        model.addAttribute("edit", false);
//        model.addAttribute("subSavingsCharges", subSavingsCharges);
//        model.addAttribute("subSavingsCharges", subSavingsCharges);
        model.addAttribute("systemDate", systemDate);
        return new ModelAndView("savings/newSavingAccountForm");
    }

    @RequestMapping(value = "/searchByNic3/{nicNo}", method = RequestMethod.GET)
    public ModelAndView searchSavingsByNicNo(@PathVariable("nicNo") String nicNo, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = savingsDebtorService.findByNic(nicNo, bID);
        Boolean message = false;
        if (debtor.size() > 0) {
            message = true;
        }
        model.addAttribute("message", message);
        return new ModelAndView("savingsDebtor/viewSavingsDebtor", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchByName3/{name}", method = RequestMethod.GET)
    public ModelAndView searchSavingsByName(@PathVariable("name") String name, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = savingsDebtorService.findByName(name, bID);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("savingsDebtor/viewSavingsDebtor", "debtorList", debtor);
    }

    @RequestMapping(value = "/viewSavingsCustomers/{debtorId}", method = RequestMethod.GET)
    public ModelAndView viewSavingsCustomers(@PathVariable("debtorId") int debtorId, Model model) {
        DebtorHeaderDetails debtor = null;
        try {
            debtor = savingsDebtorService.findDebtorById(debtorId);
            model.addAttribute("id", debtorId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("savingsDebtor/editSavingsCustomer", "debtor", debtor);
    }

    @RequestMapping(value = "/updateSavingsDebtorForm", method = RequestMethod.POST)
    public @ResponseBody
    String updateSavingsDebtorForm(@ModelAttribute("debtorSavingsHeaderDetail") DebtorHeaderDetails debHeadDetails, BindingResult result, Model model) {
        String message;
        message = "Successfully Updated";
        try {
            savingsDebtorService.updateDebtorDetails(debHeadDetails);
        } catch (Exception e) {
            e.printStackTrace();
            message = "An Error Occurred";
        }
        return message;
    }
}

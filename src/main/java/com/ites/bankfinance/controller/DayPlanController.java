/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.ChartData;
import com.bankfinance.form.ChartDataSet;
import com.bankfinance.form.DayPlanTargetModel;
import com.bankfinance.form.MonthlyScheduleAccess;
import com.bankfinance.form.RecOffCollection;
import com.ites.bankfinance.model.DayPlanAssigntoRecoOfficer;
import com.ites.bankfinance.model.DayPlanAssingtoRecoManager;
import com.ites.bankfinance.model.DayPlanMonthSchedule;
import com.ites.bankfinance.model.DayPlanMonthTargetAssingCed;
import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MLoanType;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.DayPlanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.UserService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/DayPlanController")
public class DayPlanController {

    @Autowired
    DayPlanService dayPlanService;

    @Autowired
    UserService userService;

    @Autowired
    MasterDataService masterDataService;

    @Autowired
    AdminService adminService;

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = "/loadTargetMain", method = RequestMethod.GET)
    public ModelAndView loadTargetMain(Model model, HttpServletRequest request) {
        String tgtDate = request.getParameter("tgtDate");
        String branchId = request.getSession().getAttribute("branchId").toString();
        List<MBranch> branchs = null;
        List<MLoanType> mLoanTypes = null;
        List<DayPlanMonthTargetAssingCed> targetAssingCeds = null;
        try {
            branchs = adminService.findBranch();
            mLoanTypes = adminService.findLoanType();
            targetAssingCeds = dayPlanService.getTargetAssingCeds(dateFormat.parse(tgtDate), Integer.parseInt(branchId));
            model.addAttribute("loggedBranch", Integer.parseInt(branchId));
            model.addAttribute("branches", branchs);
            model.addAttribute("mLoanTypes", mLoanTypes);
            model.addAttribute("targetAssingCeds", targetAssingCeds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("dayPlan/targetMonthCEDForm");
    }

    @RequestMapping(value = "/getDayPlanTargetInfo", method = RequestMethod.GET)
    @ResponseBody
    public DayPlanTargetModel getDayPlanTargetInfo(Model model, HttpServletRequest request) {
        DayPlanTargetModel dptm = null;
        String month = request.getParameter("month");
        String loanType = request.getParameter("loanType");
        String branchId = request.getParameter("branchId");
        try {
            if (month != null && loanType != null && branchId != null) {
                dptm = dayPlanService.calculateTarget(month, Integer.parseInt(loanType), Integer.parseInt(branchId));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return dptm;
    }

    @RequestMapping(value = "/saveTargetInfo", method = RequestMethod.POST)
    public ModelAndView saveTargetInfo(Model model, HttpServletRequest request) {
        String message;
        String loanType = request.getParameter("loanType");
        String totDue = request.getParameter("totDue");
        String totArrs = request.getParameter("totArrs");
        String dueRate = request.getParameter("dueRate");
        String arrsRate = request.getParameter("arrsRate");
        String target = request.getParameter("target");
        String month_year = request.getParameter("month_year");
        String branch = request.getParameter("branch");
        String recMan = request.getParameter("recMan");
        try {
            DayPlanMonthTargetAssingCed existDayPlan = dayPlanService.isExistDayPlan(Integer.parseInt(loanType), dateFormat.parse(month_year), Integer.parseInt(branch));
            if (existDayPlan != null) {
                existDayPlan.setTargetArriesRate(Double.parseDouble(arrsRate));
                existDayPlan.setTargetDueRate(Double.parseDouble(dueRate));
                existDayPlan.setMonthTargetAmount(Double.parseDouble(target));
                dayPlanService.saveOrUpdateMonthTarget(existDayPlan);
                DayPlanAssingtoRecoManager existAssign = dayPlanService.isExistAssign(existDayPlan.getPk());
                if (existAssign != null) {
                    existAssign.setAssingAmount(Double.parseDouble(target));
                    existAssign.setRecoveryManagerId(Integer.parseInt(recMan));
                    dayPlanService.saveOrUpdateAssignTarget(existAssign);
                }
                message = "Successfully Save Target.";
            } else {
                // Create Month Target by CED
                DayPlanMonthTargetAssingCed dpm = new DayPlanMonthTargetAssingCed();
                dpm.setBranchId(Integer.parseInt(branch));
                dpm.setLoanType(Integer.parseInt(loanType));
                dpm.setMonthTargetAmount(Double.parseDouble(target));
                dpm.setTargetArriesRate(Double.parseDouble(arrsRate));
                dpm.setTargetDueRate(Double.parseDouble(dueRate));
                dpm.setTotalArriesAmount(Double.parseDouble(totArrs));
                dpm.setTotalDueAmount(Double.parseDouble(totDue));
                dpm.setTargetDate(dateFormat.parse(month_year));
                int dpmId = dayPlanService.saveOrUpdateMonthTarget(dpm);
                // Assign Month Target to Recovery Manager
                DayPlanAssingtoRecoManager dpa = new DayPlanAssingtoRecoManager();
                dpa.setTargetCedId(dpmId);
                dpa.setAssingAmount(Double.parseDouble(target));
                dpa.setHoldBalance(0.00);
                dpa.setRecoveryManagerId(Integer.parseInt(recMan));
                dayPlanService.saveOrUpdateAssignTarget(dpa);
                message = "Successfully Save Target.";
            }
        } catch (NumberFormatException | ParseException e) {
            message = "Error Save Target..";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/loadTargetAssignMain", method = RequestMethod.GET)
    public ModelAndView loadTargetAssignMain(Model model, HttpServletRequest request) {
        List<MLoanType> mLoanTypes;
        int userId = userService.findByUserName();
        int recManId = userService.findEmployeeByUserId(userId);
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            mLoanTypes = (List<MLoanType>) dayPlanService.getAssignedLoanTypes(recManId, Integer.parseInt(branchId));
            model.addAttribute("loanTypes", mLoanTypes);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return new ModelAndView("dayPlan/dayPlanAssignToRecOffcForm");
    }

    @RequestMapping(value = "/getTarget", method = RequestMethod.GET)
    @ResponseBody
    public double getTarget(Model model, HttpServletRequest request) {
        double targetAmount = 0.00;
        String loanType = request.getParameter("loanType");
        String targetDate = request.getParameter("targetDate");
        String branchId = request.getSession().getAttribute("branchId").toString();
        int userId = userService.findByUserName();
        int recManId = userService.findEmployeeByUserId(userId);
        try {
            targetAmount = dayPlanService.getTargetForRecMan(recManId, Integer.parseInt(branchId), dateFormat.parse(targetDate), Integer.parseInt(loanType));
        } catch (NumberFormatException | ParseException e) {
            e.printStackTrace();
        }
        return targetAmount;
    }

    @RequestMapping(value = "/saveAssignedTargetInfo", method = RequestMethod.POST)
    public ModelAndView saveAssignedTargetInfo(Model model, HttpServletRequest request) {
        String message;
        String recOffId = request.getParameter("recOffId");
        String tgtAmount = request.getParameter("tgtAmount");
        String tgtDate = request.getParameter("tgtDate");
        String loanType = request.getParameter("loanType");
        String branchId = request.getSession().getAttribute("branchId").toString();
        int userId = userService.findByUserName();
        int recManId = userService.findEmployeeByUserId(userId);
        try {
            //check if assign amount is less than target(rest) amount
            Double restAmount = dayPlanService.getTargetForRecMan(recManId, Integer.parseInt(branchId), dateFormat.parse(tgtDate), Integer.parseInt(loanType));
            if (restAmount <= Double.parseDouble(tgtAmount) && restAmount != 0.00) {
                message = "Assign amount should be less than Target amount";
                model.addAttribute("errorMessage", message);
                return new ModelAndView("messages/error");
            } else {
                //check if there is an already assigned target amount
                DayPlanAssigntoRecoOfficer existAssignRecOff = dayPlanService.isExistAssignRecOff(Integer.parseInt(recOffId), Integer.parseInt(branchId), dateFormat.parse(tgtDate), Integer.parseInt(loanType));
                if (existAssignRecOff != null) {
                    double updateAmount = Double.parseDouble(tgtAmount);
                    existAssignRecOff.setTargetAmount(updateAmount);
                    dayPlanService.saveOrUpdateAssignTargetToRecOff(existAssignRecOff);
                } else {
                    //assigned target info
                    DayPlanAssigntoRecoOfficer dparo = new DayPlanAssigntoRecoOfficer();
                    dparo.setBranchId(Integer.parseInt(branchId));
                    dparo.setLoanType(Integer.parseInt(loanType));
                    dparo.setRecoOffcId(Integer.parseInt(recOffId));
                    dparo.setTargetAmount(Double.parseDouble(tgtAmount));
                    dparo.setTargetDate(dateFormat.parse(tgtDate));
                    if (userId != 0) {
                        dparo.setUserId(userId);
                        dparo.setRecoManagerId(recManId);
                    }
                    dparo.setActionTime(Calendar.getInstance().getTime());
                    //save to db
                    dayPlanService.saveOrUpdateAssignTargetToRecOff(dparo);
                }
                message = "Assigned Target Amount.";
            }
        } catch (NumberFormatException | ParseException e) {
            message = "Error Save Target..";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/getAssignedAmountToRecOff", method = RequestMethod.GET)
    @ResponseBody
    public double getAssignedAmountToRecOff(Model model, HttpServletRequest request) {
        double assignedAmount = 0.00;
        String loanType = request.getParameter("loanType");
        String targetDate = request.getParameter("targetDate");
        String branchId = request.getSession().getAttribute("branchId").toString();
        String recOffId = request.getParameter("recOffId");
        try {
            DayPlanAssigntoRecoOfficer existAssignRecOff = dayPlanService.isExistAssignRecOff(Integer.parseInt(recOffId), Integer.parseInt(branchId), dateFormat.parse(targetDate), Integer.parseInt(loanType));
            if (existAssignRecOff != null) {
                assignedAmount = existAssignRecOff.getTargetAmount();
            }
        } catch (NumberFormatException | ParseException e) {
            e.printStackTrace();
        }
        return assignedAmount;
    }

    @RequestMapping(value = "/loadMonthlySchedule", method = RequestMethod.GET)
    public ModelAndView loadMonthlySchedule(Model model, HttpServletRequest request) {
        List<MLoanType> mLoanTypes;
        int userId = userService.findByUserName();
        int recOffId = userService.findEmployeeByUserId(userId);
        String branchId = request.getSession().getAttribute("branchId").toString();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        Date date = c.getTime();
        try {
            mLoanTypes = (List<MLoanType>) dayPlanService.getAssignedLoanTypesToRecOff(recOffId, Integer.parseInt(branchId), date);
            model.addAttribute("loanTypes", mLoanTypes);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return new ModelAndView("dayPlan/monthlyScheduleForm");
    }

    @RequestMapping(value = "/getMonthlySchduleAcc", method = RequestMethod.GET)
    @ResponseBody
    public MonthlyScheduleAccess getMonthlySchduleAcc(Model model, HttpServletRequest request) {
        MonthlyScheduleAccess msa = null;
        int userId = userService.findByUserName();
        if (userId != 0) {
            int userTypeId = userService.findUserTypeByUserId(userId);
            if (userTypeId != 0) {
                if (userTypeId == 1) {
                    msa = new MonthlyScheduleAccess();
                    msa.setBranchId(Integer.parseInt(request.getSession().getAttribute("branchId").toString()));
                    msa.setUserId(userId);
                    msa.setUserTypeId(userTypeId);
                    msa.setNextMonthAcc(true);
                    msa.setNextYearAcc(true);
                    msa.setPrvMonthAcc(true);
                    msa.setPrvYearAcc(true);
                } else if (userTypeId == 6) {
                    msa = new MonthlyScheduleAccess();
                    msa.setBranchId(Integer.parseInt(request.getSession().getAttribute("branchId").toString()));
                    msa.setUserId(userId);
                    msa.setUserTypeId(userTypeId);
                    msa.setNextMonthAcc(true);
                    msa.setNextYearAcc(false);
                    msa.setPrvMonthAcc(true);
                    msa.setPrvYearAcc(false);
                } else if (userTypeId == 4) {
                    msa = new MonthlyScheduleAccess();
                    msa.setBranchId(Integer.parseInt(request.getSession().getAttribute("branchId").toString()));
                    msa.setUserId(userId);
                    msa.setUserTypeId(userTypeId);
                    msa.setNextMonthAcc(false);
                    msa.setNextYearAcc(false);
                    msa.setPrvMonthAcc(false);
                    msa.setPrvYearAcc(false);
                }
            }
        }
        return msa;
    }

    @RequestMapping(value = "/loadMonthlyCollection", method = RequestMethod.GET)
    public ModelAndView loadMonthlyCollection(Model model, HttpServletRequest request) {
        List<MLoanType> mLoanTypes;
        int userId = userService.findByUserName();
        int recOffId = userService.findEmployeeByUserId(userId);
        String branchId = request.getSession().getAttribute("branchId").toString();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        Date date = c.getTime();
        try {
            mLoanTypes = (List<MLoanType>) dayPlanService.getAssignedLoanTypesToRecOff(recOffId, Integer.parseInt(branchId), date);
            model.addAttribute("loanTypes", mLoanTypes);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return new ModelAndView("dayPlan/monthlySchedule");
    }

    @RequestMapping(value = "/getTargetAmountByRecOff", method = RequestMethod.GET)
    @ResponseBody
    public double[] getTargetAmountByRecOff(Model model, HttpServletRequest request) {
        double[] amounts = new double[2];
        String loanType = request.getParameter("loanType");
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        Date sDate = c.getTime();
        c.setTime(sDate);
        c.add(Calendar.MONTH, 1);
        c.add(Calendar.DATE, -1);
        Date eDate = c.getTime();
        String branchId = request.getSession().getAttribute("branchId").toString();
        int userId = userService.findByUserName();
        int recOffId = userService.findEmployeeByUserId(userId);
        try {
            DayPlanAssigntoRecoOfficer existAssignRecOff = dayPlanService.isExistAssignRecOff(recOffId, Integer.parseInt(branchId), sDate, Integer.parseInt(loanType));
            //double tgtAmount = dayPlanService.getTargetForRecOff(recOffId, Integer.parseInt(branchId), sDate, eDate, Integer.parseInt(loanType));
            if (existAssignRecOff != null) {
                amounts[0] = existAssignRecOff.getTargetAmount();
            }
            double receivable = dayPlanService.getRecoveryOfficerCollection(Integer.parseInt(loanType), Integer.parseInt(branchId), sDate, eDate);
            amounts[1] = receivable;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return amounts;
    }

    @RequestMapping(value = "/saveSchedule", method = RequestMethod.POST)
    public ModelAndView saveSchedule(HttpServletRequest request, Model model) {
        String message = "";
        String loanType = request.getParameter("loanType");
        String branchId = request.getSession().getAttribute("branchId").toString();
        int userId = userService.findByUserName();
        int recOffId = userService.findEmployeeByUserId(userId);
        String[] tgtDates = request.getParameterValues("tgtDates[]");
        String[] tgtAmounts = request.getParameterValues("tgtAmounts[]");
        String[] achAmounts = request.getParameterValues("achAmounts[]");
        String[] diffAmounts = request.getParameterValues("diffAmounts[]");
        Date actionTime = Calendar.getInstance().getTime();
        try {
            if (tgtDates.length > 0 && tgtAmounts.length > 0) {
                for (int i = 0; i < tgtDates.length; i++) {
                    //check if there is an already created target
                    DayPlanMonthSchedule existMonthSchedule = dayPlanService.isExistMonthSchedule(recOffId, Integer.parseInt(loanType), Integer.parseInt(branchId), dateFormat.parse(tgtDates[i]));
                    if (existMonthSchedule != null) {
                        existMonthSchedule.setTarget(Double.parseDouble(tgtAmounts[i]));
                        existMonthSchedule.setCollection(Double.parseDouble(achAmounts[i]));
                        existMonthSchedule.setDifference(Double.parseDouble(diffAmounts[i]));
                        dayPlanService.saveOrUpdateDayPlanMonthSchedule(existMonthSchedule);
                        message = "Successfully Saved";
                    } else {
                        //create a new target
                        DayPlanMonthSchedule dpms = new DayPlanMonthSchedule();
                        dpms.setActionTime(actionTime);
                        dpms.setBranchId(Integer.parseInt(branchId));
                        dpms.setTarget(Double.parseDouble(tgtAmounts[i]));
                        dpms.setCollection(Double.parseDouble(achAmounts[i]));
                        dpms.setDifference(Double.parseDouble(diffAmounts[i]));
                        dpms.setLoanTypeId(Integer.parseInt(loanType));
                        dpms.setTargetDate(tgtDates[i]);
                        dpms.setIsactive(true);
                        dpms.setReason("");
                        if (recOffId != 0) {
                            dpms.setRecOffId(recOffId);
                            dpms.setUserId(recOffId);
                        }
                        dayPlanService.saveOrUpdateDayPlanMonthSchedule(dpms);
                        message = "Successfully Saved";
                    }
                }
            }
        } catch (NumberFormatException | ParseException e) {
            message = "Error";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/getMonthSchedule", method = RequestMethod.GET)
    public ModelAndView getMonthSchedule(HttpServletRequest request, Model model) {
        List<DayPlanMonthSchedule> schedule = null;
        List<String> dateList = new ArrayList();
        String sDate = request.getParameter("sDate");
        String eDate = request.getParameter("eDate");
        String loanType = request.getParameter("loanType");
        String branchId = request.getSession().getAttribute("branchId").toString();
        int userId = userService.findByUserName();
        int recOffId = userService.findEmployeeByUserId(userId);
        double[] balances = null;
        try {
            schedule = dayPlanService.getCollection(Integer.parseInt(loanType), recOffId, Integer.parseInt(branchId), dateFormat.parse(sDate), dateFormat.parse(eDate));
            balances = dayPlanService.getBalances(Integer.parseInt(loanType), recOffId, Integer.parseInt(branchId), dateFormat.parse(sDate), dateFormat.parse(eDate));
            Date s_Date = dateFormat.parse(sDate);
            Date e_Date = dateFormat.parse(eDate);
            Calendar c = Calendar.getInstance();
            c.setTime(s_Date);
            int startDay = c.get(Calendar.DAY_OF_MONTH);
            c.setTime(e_Date);
            String dts[] = sDate.split("-");
            String dt = dts[0].concat("-").concat(dts[1]);
            int endDay = c.get(Calendar.DAY_OF_MONTH);
            int diff = endDay - startDay;
            for (int i = 0; i <= diff; i++) {
                int d = startDay + i;
                java.sql.Date date = java.sql.Date.valueOf(dt.concat("-").concat(String.valueOf(d)));
                dateList.add(dateFormat.format(date));
            }
        } catch (NumberFormatException | ParseException e) {
            e.printStackTrace();
        }
        model.addAttribute("schedule", schedule);
        model.addAttribute("dateList", dateList);
        model.addAttribute("totTarget", balances[0]);
        model.addAttribute("totCollection", balances[1]);
        model.addAttribute("totDifference", balances[2]);
        return new ModelAndView("dayPlan/monthlyScheduleTable");
    }

    @RequestMapping(value = "/activeDayPlan", method = RequestMethod.GET)
    public ModelAndView activeDayPlan(HttpServletRequest request, Model model) {
        String message;
        String dayPlanId = request.getParameter("dayPlanId");
        try {
            dayPlanService.activeDayPlan(Integer.parseInt(dayPlanId));
            message = "Successfully Active";
        } catch (NumberFormatException e) {
            message = "Error Active";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/inActiveDayPlan", method = RequestMethod.GET)
    public ModelAndView inActiveDayPlan(HttpServletRequest request, Model model) {
        String message;
        String dayPlanId = request.getParameter("dayPlanId");
        String reason = request.getParameter("reason");
        try {
            dayPlanService.inActiveDayPlan(Integer.parseInt(dayPlanId), reason);
            message = "Successfully In-Active";
        } catch (NumberFormatException e) {
            message = "Error In-Active";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    // load Recovery Manager Collection Page
    @RequestMapping(value = "/loadRMCollection", method = RequestMethod.GET)
    public ModelAndView loadRMCollection(Model model, HttpServletRequest request) {
        List<Employee> managers = new ArrayList<>();
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            if (branchId != null) {
                List list = masterDataService.findUsersByUserTypeAndBranch(6, Integer.parseInt(branchId));
                if (list.size() > 0) {
                    for (Object ob : list) {
                        Object[] o = (Object[]) ob;
                        Employee emp = new Employee();
                        emp.setEmpNo(Integer.parseInt(o[0].toString()));
                        emp.setEmpLname(o[1].toString());
                        emp.setPhone2(o[2].toString());
                        managers.add(emp);
                    }
                }
            }
            model.addAttribute("managers", managers);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return new ModelAndView("dayPlan/recManCollection");
    }

    // create Recovery Manager Prograss Chart
    @RequestMapping(value = "/getRecManChartDataSet", method = RequestMethod.GET)
    @ResponseBody
    public ChartData getRecManChartDataSet(Model moel, HttpServletRequest request) {
        ChartData chartData = null;
        String managerId = request.getParameter("managerId");
        String loanType = request.getParameter("loanType");
        String type = request.getParameter("type");
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            List<ChartDataSet> chartDataSets = dayPlanService.getRecManChartDataSet(Integer.parseInt(loanType), Integer.parseInt(managerId), Integer.parseInt(branchId), Integer.parseInt(type));
            List<String> labels = new ArrayList<>();
            switch (type) {
                case "1":
                    for (Month month : Month.values()) {
                        labels.add(month.name().substring(0, 3));
                    }   
                    break;
                case "2":
                    Calendar c = Calendar.getInstance();
                    int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
                    for (int i = 1; i <= monthMaxDays; i++) {
                        labels.add(String.valueOf(i));
                    }   
                    break;
            }
            chartData = new ChartData();
            chartData.setDatasets(chartDataSets);
            chartData.setLabels(labels);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chartData;
    }

    // get Assigned Loan Types to recovery manager
    @RequestMapping(value = "/getAssignedLoanTypesToRecMan", method = RequestMethod.GET)
    @ResponseBody
    public List<MLoanType> getAssignedLoanTypesToRecMan(HttpServletRequest request, Model model) {
        List<MLoanType> loanTypes = null;
        String branchId = request.getSession().getAttribute("branchId").toString();
        String managerId = request.getParameter("managerId");
        try {
            loanTypes = dayPlanService.getAssignedLoanTypes(Integer.parseInt(managerId), Integer.parseInt(branchId));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return loanTypes;
    }

    // load Recovery officer Collection page
    @RequestMapping(value = "/loadROCollection", method = RequestMethod.GET)
    public ModelAndView loadROCollection(Model model, HttpServletRequest request) {
        List<Employee> officers = new ArrayList<>();
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            if (branchId != null) {
                List list = masterDataService.findUsersByUserTypeAndBranch(4, Integer.parseInt(branchId));
                if (list.size() > 0) {
                    for (Object ob : list) {
                        Object[] o = (Object[]) ob;
                        Employee emp = new Employee();
                        emp.setEmpNo(Integer.parseInt(o[0].toString()));
                        emp.setEmpLname(o[1].toString());
                        emp.setPhone2(o[2].toString());
                        officers.add(emp);
                    }
                }
            }
            model.addAttribute("officers", officers);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return new ModelAndView("dayPlan/recOffCollection");
    }

    // create Recovery Officer Prograss Chart
    @RequestMapping(value = "/getRecOffChartDataSet", method = RequestMethod.GET)
    @ResponseBody
    public ChartData getRecOffChartDataSet(Model moel, HttpServletRequest request) {
        ChartData chartData = null;
        String officerId = request.getParameter("officerId");
        String loanType = request.getParameter("loanType");
        String type = request.getParameter("type");
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            List<ChartDataSet> chartDataSets = dayPlanService.getRecOffChartDataSet(Integer.parseInt(loanType), Integer.parseInt(officerId), Integer.parseInt(branchId), Integer.parseInt(type));
            List<String> labels = new ArrayList<>();
            switch (type) {
                case "1":
                    for (Month month : Month.values()) {
                        labels.add(month.name().substring(0, 3));
                    }   
                    break;
                case "2":
                    Calendar c = Calendar.getInstance();
                    int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
                    for (int i = 1; i <= monthMaxDays; i++) {
                        labels.add(String.valueOf(i));
                    }   
                    break;
            }
            chartData = new ChartData();
            chartData.setDatasets(chartDataSets);
            chartData.setLabels(labels);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chartData;
    }

    // get Assigned Loan Types to recovery officer
    @RequestMapping(value = "/getAssignedLoanTypesToRecOff", method = RequestMethod.GET)
    @ResponseBody
    public List<MLoanType> getAssignedLoanTypesToRecOff(HttpServletRequest request, Model model) {
        List<MLoanType> loanTypes = null;
        String branchId = request.getSession().getAttribute("branchId").toString();
        String officerId = request.getParameter("officerId");
        try {
            loanTypes = dayPlanService.getAssignedLoanTypesToRecOff(Integer.parseInt(officerId), Integer.parseInt(branchId));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return loanTypes;
    }

    // get Recovery Officers with monthly receivable
    @RequestMapping(value = "/getRecoveryOfficersWithCollection", method = RequestMethod.GET)
    @ResponseBody
    public List<RecOffCollection> getRecoveryOfficersWithCollection(HttpServletRequest request, Model model) {
        List<RecOffCollection> recOffCollections = null;
        String loanType = request.getParameter("loanType");
        String targetDate = request.getParameter("targetDate");
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            recOffCollections = dayPlanService.getRecoveryOfficersWithCollection(Integer.parseInt(loanType), Integer.parseInt(branchId), targetDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recOffCollections;
    }

    // get Recovery Managers by usertype and branch
    @RequestMapping(value = "/getRecoveryManagers", method = RequestMethod.GET)
    public ModelAndView getRecoveryManagers(HttpServletRequest request, Model model) {
        List<Employee> allRecManagers = new ArrayList<>();
        List<Employee> assignedRecManagers = new ArrayList<>();
        String userType = request.getParameter("userType");
        String branchId = request.getParameter("branchId");
        String loanType = request.getParameter("loanType");
        String tgtDate = request.getParameter("tgtDate");
        try {
            if (userType != null && branchId != null) {
                List list1 = masterDataService.findUsersByUserTypeAndBranch(Integer.parseInt(userType), Integer.parseInt(branchId));
                if (list1.size() > 0) {
                    for (Object ob : list1) {
                        Object[] o = (Object[]) ob;
                        Employee emp = new Employee();
                        emp.setEmpNo(Integer.parseInt(o[0].toString()));
                        emp.setEmpLname(o[1].toString());
                        emp.setPhone2(o[2].toString());
                        allRecManagers.add(emp);
                    }
                    model.addAttribute("recManagers", allRecManagers);
                }
                List list2 = dayPlanService.getRecoveryManagers(dateFormat.parse(tgtDate), Integer.parseInt(branchId), Integer.parseInt(loanType));
                if (list2.size() > 0) {
                    for (Object ob : list2) {
                        Employee emp = new Employee();
                        emp.setEmpNo(Integer.parseInt(ob.toString()));
                        assignedRecManagers.add(emp);
                    }
                    model.addAttribute("assignedRecManagers", assignedRecManagers);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("dayPlan/recoveryManagers");
    }
}

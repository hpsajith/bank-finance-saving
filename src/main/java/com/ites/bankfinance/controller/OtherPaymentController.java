/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.OtherPaymentForm;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.MBankDetails;
import com.ites.bankfinance.model.SettlementPaymentDetailBankDeposite;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementPaymentOther;
import com.ites.bankfinance.model.SettlementReceiptDetail;
import com.ites.bankfinance.model.SettlementVoucher;
import com.ites.bankfinance.model.SettlementVoucherCategory;
import com.ites.bankfinance.reportManager.DBFacade;
import com.ites.bankfinance.service.OtherPaymentService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Amidu-pc
 */
@Controller
@RequestMapping(value = "/OtherPaymentController")
public class OtherPaymentController {

    private final String transactionOtherCode = "TRANO";
    private int userLogBranchId;
    private Date systemDate;

    @Autowired
    SettlmentService settlmentService;
    @Autowired
    OtherPaymentService otherPaymentService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/loadOtherPayment", method = RequestMethod.GET)
    public ModelAndView loadOtherPayment(Model model) {
        List<MBankDetails> mbankDetailList = settlmentService.findBankList();
        List<SettlementVoucherCategory> voucherCategorys = otherPaymentService.getVoucherCategorys();
        List<Chartofaccount> chAccountsList = otherPaymentService.findChtAccountsList();
        model.addAttribute("mBankDetailList", mbankDetailList);
        model.addAttribute("voucherCategorys", voucherCategorys);
        model.addAttribute("chaAccountList", chAccountsList);
        return new ModelAndView("settlement/addOtherPayment");
    }

    @RequestMapping(value = "/save_OtherPayment", method = RequestMethod.POST)
    public ModelAndView addOtherPayment(@ModelAttribute("otherPaymentForm") OtherPaymentForm opfData, Model model, HttpServletRequest request, BindingResult result) {
        String errorMsg = null;
        String successMsg = null;
        String rcptNo = "";
        try {
            String accountno = opfData.getAccount();
            int paytype = opfData.getPayType();
            String amountText = opfData.getAmountText();
            String cusName = opfData.getCusName();
            String cusAdd = opfData.getCusAddress();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            int userId = userService.findByUserName();
            userLogBranchId = settlmentService.getUserLogedBranch(userId);
            systemDate = settlmentService.getSystemDate(userLogBranchId);
            double amount;
            String chequeno;
            int Bank;
            Date RealizeDate;
            String res1 = "";
            boolean cashres = false;
            boolean chequeres = false;
            boolean bankres = false;
            String TransactionOtherNo = "";

            amount = opfData.getAmount();
            rcptNo = settlmentService.getReceiptNo("SETO");
            String serielNo = settlmentService.getMaxSerialNo();

            SettlementReceiptDetail srd = new SettlementReceiptDetail();
            srd.setRcptNo(rcptNo);
            srd.setInvDate(sdf.parse(sdf.format(Calendar.getInstance().getTime())));
            srd.setPayment(amount);
            srd.setDescription(opfData.getDescription());
            srd.setSerielNo(serielNo);
            srd.setSystemDate(systemDate);
            boolean res = settlmentService.addSettlementReceiptDetail(srd);

            if (res) {
                SettlementPaymentOther spo = new SettlementPaymentOther();
                TransactionOtherNo = otherPaymentService.getOtherTransactionIdMax(transactionOtherCode);
                spo.setTransactionNo(TransactionOtherNo);
                spo.setCusName(cusName);
                spo.setCusAddress(cusAdd);
                spo.setPaymentType(paytype);
                spo.setPaymentAmount(amount);
                spo.setAccountNo(accountno);
                spo.setBranchId(userLogBranchId);
                spo.setUserId(userId);
                spo.setSystemDate(systemDate);
                spo.setActionDate(sdf1.parse(sdf1.format(Calendar.getInstance().getTime())));
                spo.setRecieptNo(rcptNo);
                spo.setAmountText(amountText);
                spo.setIsPosting(0);
                res1 = otherPaymentService.addSettlmentPaymentOther(spo);
            }
            if (paytype == 1) {
//                SettlementPaymentDetailCash cash = new SettlementPaymentDetailCash();
                cashres = settlmentService.addCashPaymentDetails(userId, amount, TransactionOtherNo);
                if (!res1.equals("") && cashres) {
                    successMsg = "Successfully Saved..!";
                    model.addAttribute("successMessage", successMsg);
                    model.addAttribute("status", res1);
                    return new ModelAndView("messages/success");
                }

            } else if (paytype == 2) {
                chequeno = opfData.getChequeno();
                Bank = opfData.getChequeBank();
                RealizeDate = opfData.getRealizeDate();
                SettlementPaymentDetailCheque cheque = new SettlementPaymentDetailCheque();
                cheque.setTransactionNo(TransactionOtherNo);
                cheque.setChequeNo(chequeno);
                cheque.setBank(Bank);
                cheque.setTransactionDate(sdf.parse(sdf.format(Calendar.getInstance().getTime())));
                cheque.setRealizeDate(RealizeDate);
                cheque.setAmount(amount);
                cheque.setUserId(userId);
                cheque.setActionDate(sdf1.parse(sdf1.format(Calendar.getInstance().getTime())));
                chequeres = settlmentService.addChequePaymentDetails(cheque);
                if (!res1.equals("") && chequeres) {
                    successMsg = "Successfully Saved..!";
                    model.addAttribute("successMessage", successMsg);
                    model.addAttribute("status", res1);
                    return new ModelAndView("messages/success");
                }
            } else if (paytype == 3) {
                Bank = opfData.getBank();
                String depositaccountno = opfData.getBankaccountno();
                SettlementPaymentDetailBankDeposite bankDeposite = new SettlementPaymentDetailBankDeposite();
                bankDeposite.setTransactionNo(TransactionOtherNo);
                bankDeposite.setLoanId(0);
                bankDeposite.setBankId(Bank);
                bankDeposite.setDepositeAccountNo(depositaccountno);
                bankDeposite.setDepositeDate(sdf.parse(sdf.format(Calendar.getInstance().getTime())));
                bankDeposite.setUserId(userId);
                bankDeposite.setActionTime(sdf1.parse(sdf1.format(Calendar.getInstance().getTime())));
                bankDeposite.setAmount(amount);
                bankres = settlmentService.addBankDepositePaymentDetails(bankDeposite);
                if (!res1.equals("") && bankres) {
                    successMsg = "Successfully Saved..!";
                    model.addAttribute("successMessage", successMsg);
                    model.addAttribute("status", res1);
                    return new ModelAndView("messages/success");
                }
            }

        } catch (ParseException ex) {
            Logger.getLogger(OtherPaymentController.class.getName()).log(Level.SEVERE, null, ex);
            errorMsg = "Error Occured";
//            model.addAttribute("errorMsg", errorMsg);
//            return new ModelAndView("messages/error");
        }
        model.addAttribute("", errorMsg);
        return new ModelAndView("messages/error");
    }

    @RequestMapping(value = "/printOtherReceipt", method = RequestMethod.GET)
    public void printOtherReceipt(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        try {

            String rcptNo = request.getParameter("rNo");

            DBFacade db = new DBFacade();
            Connection c = db.connect();
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            String userName = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branchName = userService.findWorkingBranchName(branchID);
            Date systemDate = settlmentService.getSystemDate(branchID);

            params.put("receiptNo", rcptNo);
            params.put("userName", userName);
            params.put("branhName", branchName);
            params.put("systemDate", systemDate);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/receipt");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("../reports/receipt/wijitha_other_receipt.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/save_newVoucher", method = RequestMethod.POST)
    public ModelAndView saveNewVoucher(HttpServletRequest request, Model model) {
        int save = 0;
        try {
            double amount = Double.parseDouble(request.getParameter("amount"));
            int type = Integer.parseInt(request.getParameter("paymentType"));
            String desc = request.getParameter("description");
            String amountText = request.getParameter("word");

            int userId = userService.findByUserName();
            int userLogBranchId = settlmentService.getUserLogedBranch(userId);
            Date systemDate = settlmentService.getSystemDate(userLogBranchId);

            SettlementVoucher vouch = new SettlementVoucher();
            vouch.setAmount(amount);
            vouch.setVoucherCatogery(type);
            vouch.setDescription(desc);
            vouch.setUserId(userId);
            vouch.setVoucherDate(systemDate);
            vouch.setBranchId(userLogBranchId);

            save = otherPaymentService.saveIssuePayment(vouch);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (save > 0) {
            String successMsg = "Successfully Saved..!";
            model.addAttribute("successMessage", successMsg);
            return new ModelAndView("messages/success");
        } else {
            String errorMsg = "Error Occured";
            model.addAttribute("", errorMsg);
            return new ModelAndView("messages/error");
        }
    }

    @RequestMapping(value = "/findChtAccountsByName", method = RequestMethod.GET)
    @ResponseBody
    public List<Chartofaccount> findChtAccountsByName(HttpServletRequest request) {
        List<Chartofaccount> chartofaccounts = null;
        String accName = request.getParameter("accName");
        try {
            chartofaccounts = otherPaymentService.findChtAccountListByName(accName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chartofaccounts;
    }

    @RequestMapping(value = "/findChtAccountsForOtherInsurance", method = RequestMethod.GET)
    @ResponseBody
    public List<InsuranceOtherCustomers> findChtAccountsForOtherInsurance(HttpServletRequest request) {
        List<InsuranceOtherCustomers> insuranceOtherCustomerses = null;
        String accName = request.getParameter("accName");
        try {
            insuranceOtherCustomerses = otherPaymentService.findChtAccountInsuranceOtherCustomer(accName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return insuranceOtherCustomerses;
    }

    @RequestMapping(value = "/loadAccountDetails", method = RequestMethod.GET)
    @ResponseBody
    public InsuranceOtherCustomers loadAccountDetails(HttpServletRequest request) {
        InsuranceOtherCustomers insuranceOtherCustomerses = null;
        String accName = request.getParameter("accName");
        try {
            insuranceOtherCustomerses = otherPaymentService.findChtAccountDetails(accName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return insuranceOtherCustomerses;
    }

    @RequestMapping(value = "/loadChartOfAccounts", method = RequestMethod.GET)
    @ResponseBody
    public List<Chartofaccount> loadChartOfAccounts(HttpServletRequest request) {
        List<Chartofaccount> chAccountsList = null;
        try {
            chAccountsList = otherPaymentService.findChtAccountsList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chAccountsList;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

}

package com.ites.bankfinance.controller;

import com.bankfinance.form.AddJobsForm;
import com.bankfinance.form.ChartOfAccountWrapper;
import com.bankfinance.form.JobDefine;
import com.bankfinance.form.RecOffColDateForm;
import com.bankfinance.form.RecOffColDateFormWrapper;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.savingsModel.MSavingsType;
import com.ites.bankfinance.savingsModel.MSubSavingsType;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.UserService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Harshana
 */
@Controller
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private MasterDataService masterDataService;

    @Autowired
    UserService userService;

    @Autowired
    private MasterDataService masterService;

    //add addLoanType Form to MainPage
    @RequestMapping(value = "/loadLoanTypeFormMain", method = RequestMethod.GET)
    public ModelAndView adminAddLoanTypeAdd(Model model) {
        List<MLoanType> mLoanTypes = null;
        boolean isSuperAdmin = false;
        try {
            mLoanTypes = adminService.findLoanType();
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    isSuperAdmin = true;
                }
            }
            model.addAttribute("mLoanTypes", mLoanTypes);
            model.addAttribute("isSuperAdmin", isSuperAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("admin/addLoanTypeMain");
    }

    //<---------------------------Start New User- ----------------------------->  
    //load NewUserForm on NewUserMain
    @RequestMapping(value = "/loadAddUserForm", method = RequestMethod.GET)
    public ModelAndView loadNewUserForm(Model model) {
        List<UmUser> umUsers = null;
        try {
            umUsers = adminService.findUmUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("user", umUsers);
        return new ModelAndView("admin/addUserForm");
    }

    //add addUser Form to MainPage
    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public ModelAndView adminAddUser(Model model) {
        List<UmUser> umUsers = null;
        try {
            umUsers = adminService.findUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("users", umUsers);
        return new ModelAndView("admin/addUserMain");
    }

    //add addUser Form to MainPage
    @RequestMapping(value = "/editUser/{userId}", method = RequestMethod.GET)
    public ModelAndView adminEditUser(@PathVariable("userId") int userId, Model model) {
        UmUser umUser = null;
        List<UmUser> umUsers = null;
        try {
            umUser = adminService.findUserName(userId);
            umUsers = adminService.findUmUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("userName", umUser);
        model.addAttribute("user", umUsers);
        return new ModelAndView("admin/addUserForm");
    }

    @RequestMapping(value = "/loadUser", method = RequestMethod.GET)
    public @ResponseBody
    List loadUsers() {
        List users = adminService.findUser();
        return users;
    }

    //<---------------------------End New User- ------------------------------->  
    //<---------------------------Start Branches------------------------------->
    //load AddBranchForm to AddBranchMain page
    @RequestMapping(value = "/loadAddBranchForm", method = RequestMethod.GET)
    public ModelAndView loadAddBranchForm(Model model) {
        int maxBranchCount = 0, remainBranchCount = 0;

        int savedBranches = adminService.maxBranchCount();
        MSystemValidation sysValidation = adminService.findSystemValidation();
        if (sysValidation != null) {
            maxBranchCount = sysValidation.getMaxBranchCount();
        }
        remainBranchCount = maxBranchCount - savedBranches;
        model.addAttribute("remainBranchCount", remainBranchCount);
        return new ModelAndView("admin/addBranchForm");
    }

    //Load addBranchMain to MainPage
    @RequestMapping(value = "/loadAddBranchMain", method = RequestMethod.GET)
    public ModelAndView adminAddBranchMain(Model model) {
        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);
        return new ModelAndView("admin/addBranchMain");
    }

    //Load addNewGoldWeight
    @RequestMapping(value = "/loadAddNewGoldPrice", method = RequestMethod.GET)
    public ModelAndView adminAddNewGoldprice(Model model) {
        MWeightunits units = null;

        try {
            units = adminService.findWeightType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(units.toString());
        model.addAttribute("unitList",units);
        return new ModelAndView("admin/changeGoldValueForm");
    }

    @RequestMapping(value = "/saveOrUpdateGoldValue", method = RequestMethod.GET)
    public ModelAndView saveOrUpdateGoldValue(@ModelAttribute("addNewGoldForm") MWeightunits formData,Model model) {
        String successMsg = "";
        String errorMsg = "";
        int res=0;

        System.out.println("FORM DATA :"+formData.toString());
        try {
            if (formData.getWeightId() == null) {
                res = adminService.saveOrUpdateGoldValue(formData);
                if (res > 0) {
                    successMsg = "New center saved successfully";
                } else {
                    errorMsg = "Save Process Failed";
                }
            } else {
                res = adminService.saveOrUpdateGoldValue(formData);
                if (res > 0) {
                    successMsg = "selected center Updated Successfully";

                } else {
                    errorMsg = "Update Process Failed";
                }
            }
            formData.setWeightId(res);

        } catch (Exception e) {
            errorMsg = "Save Or Update Process Failed" + e;
        }
        model.addAttribute("successMsg", successMsg);
        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("admin/addCenterForm", "center", formData);

    }

//    @RequestMapping(value = "/saveOrUpdateGoldValue/{weightId/{unitName}}", method = RequestMethod.POST)
//    public ModelAndView saveOrUpdateGoldValue(@PathVariable("weightId") int weightId,@PathVariable("unitName") String unitName,@ModelAttribute("addNewGoldForm") MWeightunits formData,Model model) {
//        String successMsg = "";
//        String errorMsg = "";
//        int res=0;
//
//        System.out.println("FORM DATA :"+formData.toString());
////        try {
////            if (formData.getWeightId() == null) {
//                res = adminService.saveOrUpdateGoldValue(formData);
////                if (res > 0) {
////                    successMsg = "New center saved successfully";
////                } else {
////                    errorMsg = "Save Process Failed";
////                }
////            } else {
////                res = adminService.saveOrUpdateGoldValue(formData);
////                if (res > 0) {
////                    successMsg = "selected center Updated Successfully";
////
////                } else {
////                    errorMsg = "Update Process Failed";
////                }
////            }
////            formData.setWeightId(weightId);
////
////        } catch (Exception e) {
////            errorMsg = "Save Or Update Process Failed" + e;
////        }
////        model.addAttribute("successMsg", successMsg);
////        model.addAttribute("errorMsg", errorMsg);
//        return new ModelAndView("admin/addCenterForm", "center", formData);
//
//    }


    //save or update New Branch
    @RequestMapping(value = "/saveOrUpdateBranch", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateNewBranch(@ModelAttribute("newBranchForm") MBranch formData, Model model, BindingResult result) {
        String successMsg = "";
        String errorMsg = "";
        int branchId = 0;
        int maxBranchCount = 0, remainBranchCount = 0;

        try {
            if (formData.getBranchId() == null) {
                branchId = adminService.saveNewBranch(formData);
                if (branchId > 0) {
                    successMsg = "Save Process Successfully";
                } else {
                    errorMsg = "Save Process Failed";
                }
            } else {
                branchId = adminService.saveNewBranch(formData);
                if (branchId > 0) {
                    successMsg = "Update Process Successfully";

                } else {
                    errorMsg = "Update Process Failed";
                }
            }
            formData.setBranchId(branchId);
            int savedBranches = adminService.maxBranchCount();
            MSystemValidation sysValidation = adminService.findSystemValidation();
            if (sysValidation != null) {
                maxBranchCount = sysValidation.getMaxBranchCount();
            }
            remainBranchCount = maxBranchCount - savedBranches;

        } catch (Exception e) {
            errorMsg = "Save Or Update Process Failed" + e;

        }
        model.addAttribute("remainBranchCount", remainBranchCount);
        model.addAttribute("successMsg", successMsg);
        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("admin/addBranchForm", "branch", formData);
    }

    //Edit Branch
    @RequestMapping(value = "/editBranch/{branchId}", method = RequestMethod.GET)
    public ModelAndView clickToeditBranch(@PathVariable("branchId") int branchId, Model model) {
        MBranch mBranch = null;
        int maxBranchCount = 0, remainBranchCount = 0;
        try {
            mBranch = adminService.findEditBranch(branchId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int savedBranches = adminService.maxBranchCount();
        MSystemValidation sysValidation = adminService.findSystemValidation();
        if (sysValidation != null) {
            maxBranchCount = sysValidation.getMaxBranchCount();
        }
        remainBranchCount = maxBranchCount - savedBranches;

        model.addAttribute("remainBranchCount", remainBranchCount);
        model.addAttribute("branch", mBranch);
        return new ModelAndView("admin/addBranchForm");
    }

    //inActive Branch
    @RequestMapping(value = "/inActiveBranch/{branchID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean inActiveBranch(@PathVariable("branchID") int branchID, Model model) {
        Boolean inActive = false;
        try {
            inActive = adminService.inActiveBranch(branchID);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return inActive;
    }

    //Active Branch
    @RequestMapping(value = "/activeBranch/{branchID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean activeBranch(@PathVariable("branchID") int branchID, Model model) {
        Boolean active = false;
        try {
            active = adminService.activeBranch(branchID);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return active;
    }

    //<---------------------------End Branches- -------------------------------> 
    //<---------------------------Start Center- -------------------------------> 
    //load AddCenterForm to AddCenterMain page
    @RequestMapping(value = "/loadAddCenterForm", method = RequestMethod.GET)
    public ModelAndView loadAddCenterForm(Model model) {

        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);
        System.out.println("branch list : " + branchs);
        return new ModelAndView("admin/addCenterForm");
    }

    //Load addCenterMain to MainPage
    @RequestMapping(value = "/loadAddCenterMain", method = RequestMethod.GET)
    public ModelAndView adminAddCenterMain(Model model) {

        List<MCenter> center = null;
        List<MBranch> branchs = null;
        try {
            center = adminService.findCenter();
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("centerList", center);
        model.addAttribute("branchList", branchs);


        return new ModelAndView("admin/addCenterMain");
    }

    //save or update New center
    @RequestMapping(value = "/saveOrUpdateCenter", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateCenter(@ModelAttribute("newCenterForm") MCenter formData, Model model, BindingResult result) {
        String successMsg = "";
        String errorMsg = "";
        int centerId = 0;

        try {
            if (formData.getCenterID() == null) {
                centerId = adminService.saveNewCenter(formData);
                if (centerId > 0) {
                    successMsg = "New center saved successfully";
                } else {
                    errorMsg = "Save Process Failed";
                }
            } else {
                centerId = adminService.saveNewCenter(formData);
                if (centerId > 0) {
                    successMsg = "selected center Updated Successfully";

                } else {
                    errorMsg = "Update Process Failed";
                }
            }
            formData.setCenterID(centerId);

        } catch (Exception e) {
            errorMsg = "Save Or Update Process Failed" + e;
        }
        model.addAttribute("successMsg", successMsg);
        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("admin/addCenterForm", "center", formData);
    }

    //Edit Center
    @RequestMapping(value = "/editCenter/{centerId}", method = RequestMethod.GET)
    public ModelAndView clickToeditCenter(@PathVariable("centerId") int centerId, Model model) {

        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);

        MCenter mCenter = null;

        try {
            mCenter = adminService.editCenter(centerId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("center", mCenter);
        return new ModelAndView("admin/addCenterForm");
    }

    //inActive center
    @RequestMapping(value = "/inActiveCenter/{centerID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean inActiveCenter(@PathVariable("centerID") int centerID, Model model) {
        Boolean inActive = false;
        try {
            inActive = adminService.inActiveCenter(centerID);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return inActive;
    }

    //Active center
    @RequestMapping(value = "/activeCenter/{centerID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean activeCenter(@PathVariable("centerID") int centerID, Model model) {
        Boolean active = false;
        try {
            active = adminService.activeCenter(centerID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return active;
    }

    //Load center for id
    @RequestMapping(value = "/loadCenter/{branchId}", method = RequestMethod.GET)
    public ModelAndView loadCenter(@PathVariable("branchId") int branchId, Model model) {
        List<MCenter> center = null;
        try {
            center = adminService.findCenter(branchId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("centerList", center);

        return new ModelAndView("loan/centerDropdownForm");
    }

    //<---------------------------End Center- ------------------------------->
    //--------------------- get group users---------------
    @RequestMapping(value = "/loadGroupUsers/{groupID}", method = RequestMethod.GET)
    public ModelAndView loadGroupUsers(@PathVariable("groupID") int groupID, Model model) {
        List<DebtorHeaderDetails> dhd = null;
        try {

            dhd = adminService.findGroupUsers(groupID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("groupUserList", dhd);

        return new ModelAndView("loan/groupUserTable");
    }

    //--------------------- end get group users---------------
    //<---------------------------Start Group- -------------------------------> 
    //load AddGroupForm to AddGroupMain page
    @RequestMapping(value = "/loadAddGroupForm", method = RequestMethod.GET)
    public ModelAndView loadAddGroupForm(Model model) {

        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<MCenter> center = null;
        try {
            center = adminService.findCenter();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("====>> Center ID : " + center);

        model.addAttribute("branchList", branchs);
        model.addAttribute("centerList", center);

        return new ModelAndView("admin/addGroupForm");
    }

    //Load addGroupMain to MainPage
    @RequestMapping(value = "/loadAddGroupMain", method = RequestMethod.GET)
    public ModelAndView loadAddGroupMain(Model model) {

        List<MGroup> group = null;
        List<MCenter> center = null;
        List<MBranch> branch = null;
        try {
            group = adminService.findGroup();
            center = adminService.findCenter();
            branch = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("groupList", group);
        model.addAttribute("centerList", center);
        model.addAttribute("branchList", branch);

        return new ModelAndView("admin/addGroupMain");
    }

    //save or update New group
    @RequestMapping(value = "/saveOrUpdateGroup", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateGroup(@ModelAttribute("newSeizerForm") MGroup formData, Model model, BindingResult result) {
        String successMsg = "";
        String errorMsg = "";
        int groupId = 0;
        System.out.println("-------===>>> group ID : " + formData.getGroupID());
        try {
            if (formData.getGroupID() == null) {
                groupId = adminService.saveNewGroup(formData);
                if (groupId > 0) {
                    successMsg = "New group saved successfully";
                } else {
                    errorMsg = "Save Process Failed";
                }
            } else {
                groupId = adminService.saveNewGroup(formData);
                if (groupId > 0) {
                    successMsg = "selected group Updated Successfully";

                } else {
                    errorMsg = "Update Process Failed";
                }
            }
            formData.setGroupID(groupId);

        } catch (Exception e) {
            errorMsg = "Save Or Update Process Failed" + e;
        }
        System.out.println("--------------------------- " + successMsg);
        model.addAttribute("successMsg", successMsg);
        model.addAttribute("errorMsg", errorMsg);
//        return new ModelAndView("admin/addGroupForm", "group", formData);
        return new ModelAndView("admin/addGroupForm", "group", formData);
    }

    //Edit group
    @RequestMapping(value = "/editGroup/{groupID}", method = RequestMethod.GET)
    public ModelAndView clickToeditGroup(@PathVariable("groupID") int groupID, Model model) {

        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);

        List<MCenter> center = null;
        try {
            center = adminService.findCenter();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("centerList", center);

        MGroup group = null;

        try {
            group = adminService.editGroup(groupID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("group", group);
        return new ModelAndView("admin/addGroupForm");
    }

    //inActive group
    @RequestMapping(value = "/inActiveGroup/{groupID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean inActiveGroup(@PathVariable("groupID") int groupID, Model model) {
        Boolean inActive = false;
        try {
            inActive = adminService.inActiveGroup(groupID);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return inActive;
    }

    //Active group
    @RequestMapping(value = "/activeGroup/{groupID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean activeGroup(@PathVariable("groupID") int groupID, Model model) {
        Boolean active = false;
        System.out.println(">>>>>> group id : " + groupID);
        try {
            active = adminService.activeGroup(groupID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return active;
    }

    //Load group for id
    @RequestMapping(value = "/loadGroup/{centerID}", method = RequestMethod.GET)
    public ModelAndView loadGroup(@PathVariable("centerID") int centerID, Model model) {
        System.out.println(">>>>>>>>-----center ID --->>>>>>> " + centerID);

        List<MGroup> group = null;
        try {
            group = adminService.findGroup(centerID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("groupList", group);

        return new ModelAndView("loan/groupDropdownForm");

    }

    //<---------------------------End group- ------------------------------->
    //<---------------------------Start Department- ---------------------------> 
    @RequestMapping(value = "/loadDepartmentform/{branchID}", method = RequestMethod.GET)
    public ModelAndView loadDepartment(@PathVariable("branchID") int branchId, Model model) {
        MBranch branch = null;
        try {
            branch = adminService.findActiveBranch(branchId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("branch", branch);
        model.addAttribute("branchID", branchId);
        return new ModelAndView("admin/addDepartmentForm");
    }

    //saveOrUpdate Department 
    @RequestMapping(value = "/saveNewDepartment", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateUserDepartment(@ModelAttribute("newDepartmentForm") MDepartment formData, BindingResult result) {
        String message = "";
        boolean res = false;
        try {
            res = adminService.saveDepartment(formData);
            if (res) {
                message = "Save Process Successfully";
            } else {
                message = "Save Process Failed";
            }
        } catch (Exception e) {
            message = "Save Process Failed" + e;
        }

//        model.addAttribute("message", message);
        return message;
    }

    //<---------------------------Start Employee- ----------------------------->
    //add New Employee Form to MainPage
    @RequestMapping(value = "/newEmployee", method = RequestMethod.GET)
    public ModelAndView adminNewEmployeeForm(Model model) {
        List<Employee> employeesList = null;
        try {
            employeesList = adminService.findEmployeeList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("employeeList", employeesList);
        return new ModelAndView("admin/addEmployeeMain");
    }

    //add viewEmployeeForm Form to addEmployeeMain
    @RequestMapping(value = "/viewEmployeeForm", method = RequestMethod.GET)
    public ModelAndView adminviewEmployeeForm(Model model) {
        List<UmUserType> umUserTypes = null;
        try {
            umUserTypes = adminService.findUserType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("userTypeList", umUserTypes);
        return new ModelAndView("admin/addEmployeeForm");
    }

    //save Or Update New Employee
    @RequestMapping(value = "/saveOrUpdateNewEmployee", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateNewEmployee(@ModelAttribute("newEmployeeForm") Employee formData, BindingResult result, Model model) {
        String successMsg = "";
        String errorMsg = "";
        int empId = 0;
        UmUser umUser = null;
        List<UmUserType> umUserTypes = null;
        int remainUserCount = 0;
        int saveUserCount = 0;
        int maxUserCount = 0;
        try {
            if (formData.getEmpNo() == null) {
                empId = adminService.saveNewEmployee(formData);
                umUser = adminService.findEmpUser(empId);
                umUserTypes = adminService.findUserType();
                if (empId > 0) {
                    successMsg = "Successfully Saved..!";
                }
            } else {
                empId = adminService.saveNewEmployee(formData);
                umUser = adminService.findEmpUser(empId);
                umUserTypes = adminService.findUserType();
                if (empId > 0) {
                    successMsg = "Successfully Updated..!";
                }
            }
            formData.setEmpNo(empId);
            List<UmUser> activeUsers = adminService.findAllUser();
            saveUserCount = activeUsers.size();
            MSystemValidation mUserCount = adminService.findSystemValidation();
            if (mUserCount != null) {
                maxUserCount = mUserCount.getMaxUserCount();
            }
            remainUserCount = maxUserCount - saveUserCount;
        } catch (Exception e) {
            errorMsg = "Save Process Fail";
        }
        model.addAttribute("successMsg", successMsg);
        model.addAttribute("errorMsg", errorMsg);
        model.addAttribute("empUser", umUser);
        model.addAttribute("userTypeList", umUserTypes);
        model.addAttribute("remainUserCount", remainUserCount);
        model.addAttribute("maxUserCount", maxUserCount);

        return new ModelAndView("admin/addEmployeeForm", "employee", formData);
    }

    //view edit to Employee
    @RequestMapping(value = "/viewEditEmployee/{empId}", method = RequestMethod.GET)
    public ModelAndView viewEditEmployee(@PathVariable("empId") int employeeId, Model model) {
        Employee employee = null;
        UmUser umUser = null;
        List<UmUserType> umUserTypes = null;
        List<UmUserBranch> branchs = null;
        int numBranches = 0;
        int remainUserCount = 0;
        int saveUserCount = 0;
        int maxUserCount = 0;
        try {
            if (employeeId > 0) {
                employee = adminService.findEmployee(employeeId);
                umUser = adminService.findEmpUser(employeeId);
                umUserTypes = adminService.findUserType();
                branchs = adminService.findUserBranches(umUser.getUserId());
                numBranches = branchs.size();
            } else {
                employee = new Employee();
                umUser = new UmUser();
                umUserTypes = new ArrayList<UmUserType>();
            }
            List<UmUser> activeUsers = adminService.findAllUser();
            saveUserCount = activeUsers.size();
            MSystemValidation mUserCount = adminService.findSystemValidation();
            if (mUserCount != null) {
                maxUserCount = mUserCount.getMaxUserCount();
            }
            remainUserCount = maxUserCount - saveUserCount;
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("empUser", umUser);
        model.addAttribute("userTypeList", umUserTypes);
        model.addAttribute("numOfBranch", numBranches);
        model.addAttribute("remainUserCount", remainUserCount);
        model.addAttribute("maxUserCount", maxUserCount);
        return new ModelAndView("admin/addEmployeeForm", "employee", employee);
    }

    //load Add Branches
    @RequestMapping(value = "/loadBranch/{empId}", method = RequestMethod.GET)
    public ModelAndView loadBranches(@PathVariable("empId") int empID, Model model) {
        List<MBranch> branchs = null;
        int numOfBranch = 0;
        UmUser umUser = null;
        List<UmUserBranch> umUserBranchs = null;
        try {
            branchs = adminService.findBranch();
            umUser = adminService.findUserForEmpID(empID);
            umUserBranchs = adminService.findUserBranch(empID);

            if (umUser.getUserTypeId() == 0) {
                numOfBranch = 0;
            } else {
                numOfBranch = adminService.findNumOfBranch(umUser.getUserTypeId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);
        model.addAttribute("empId", empID);
        model.addAttribute("numOfBranch", numOfBranch);
        model.addAttribute("userBranche", umUserBranchs);
        return new ModelAndView("admin/branchForm");
    }

    //load Add Department
    @RequestMapping(value = "/loadUserDepartment/{empId}", method = RequestMethod.GET)
    public ModelAndView loadDepartments(@PathVariable("empId") int empID, Model model) {
        List<MDepartment> departments = null;
        List<UmUserBranch> userBranchs = null;
        List<MBranch> branchs = null;
        try {
            departments = adminService.findDepartment();
            userBranchs = adminService.findUserBranch(empID);
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("departmentList", departments);
        model.addAttribute("emp_ID", empID);
        model.addAttribute("userBranch", userBranchs);
        model.addAttribute("umBranch", branchs);

        return new ModelAndView("admin/departmentForm");
    }

    //saveOrUpdate User Department 
    @RequestMapping(value = "/saveUserDepartment", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateUserDepartment(@RequestParam("branchId") int branchId, @RequestParam("empId") int empID, Model model, @RequestParam("DepID") int[] userDepartment) {
        String message = "";
        String branch = "";
        boolean res = false;
        try {
            res = adminService.saveUserDepartment(branchId, empID, userDepartment);
            branch = adminService.findBranchName(branchId);
            if (res) {
                message = "Save Process Successfully";
            } else {
                message = "Save Process Failed";
            }
        } catch (Exception e) {
            message = "Save Process Failed" + e;
        }
        userDepartment = null;
        return branch + " : " + message;
    }

    //save User Branch
    @RequestMapping(value = "saveBranchUserType", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateUserTypeBranch(@ModelAttribute("userTypeBranchForm") UmUserBranch formData, Model model, BindingResult result, @RequestParam("branchID") int[] userTypeBranch) {
        String message = "";
        boolean res = false;

        try {
            res = adminService.saveUserTypeBranch(formData, userTypeBranch);
            if (res) {
                message = "Save Process Successfully";
                model.addAttribute("successMessage", message);
                return new ModelAndView("messages/success");
            } else {
                message = "Save Process Failed";
                model.addAttribute("errorMessage", message);
                return new ModelAndView("messages/error");
            }

        } catch (Exception e) {
            message = "Save Process Failed" + e;
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }

    }

    @RequestMapping(value = "/inActiveEmployee/{empNo}", method = RequestMethod.GET)
    public ModelAndView inActiveEmployee(@PathVariable("empNo") int empNo, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        boolean sucess = false;
        try {
            sucess = adminService.inActiveEmployee(empNo);
            if (sucess) {
                message = "Successfully In-Active";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in In-Active";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return modelAndView;
    }

    @RequestMapping(value = "/activeEmployee/{empNo}", method = RequestMethod.GET)
    public ModelAndView activeEmployee(@PathVariable("empNo") int empNo, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        boolean sucess = false;
        int remainUserCount = 0;
        int saveUserCount = 0;
        int maxUserCount = 0;
        try {
            List<UmUser> activeUsers = adminService.findAllUser();
            saveUserCount = activeUsers.size();
            MSystemValidation mUserCount = adminService.findSystemValidation();
            if (mUserCount != null) {
                maxUserCount = mUserCount.getMaxUserCount();
            }
            remainUserCount = maxUserCount - saveUserCount;
            if (remainUserCount == 0) {
                message = "Max user count is exceeded.";
                model.addAttribute("warningMessage", message);
                modelAndView = new ModelAndView("messages/warning");
            } else {
                sucess = adminService.activeEmployee(empNo);
                if (sucess) {
                    message = "Successfully Active";
                    model.addAttribute("successMessage", message);
                    modelAndView = new ModelAndView("messages/success");
                } else {
                    message = "Error in Active";
                    model.addAttribute("errorMessage", message);
                    modelAndView = new ModelAndView("messages/error");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return modelAndView;
    }

    //<---------------------------End Employee -------------------------------->
    //<---------------------------Start User Type ----------------------------->
    //Load addUserTypeMain Page to Main Page
    @RequestMapping(value = "/loadAddUserTypeMain", method = RequestMethod.GET)
    public ModelAndView adminLoadUserTypeMain(Model model) {
        List<UmUserType> umUserTypes = null;
        try {
            umUserTypes = adminService.findUserTypeList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("userTypeList", umUserTypes);
        return new ModelAndView("admin/addUserTypeMain");
    }

    //Load addUserTypeForm to addUserTypeMain
    @RequestMapping(value = "/loadUserTypeForm", method = RequestMethod.GET)
    public ModelAndView LoadUserTypeForm(Model model) {
        boolean isSuperAdmin = false;
        try {
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    isSuperAdmin = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("isSuperAdmin", isSuperAdmin);
        return new ModelAndView("admin/addUserTypeForm");
    }

    //save Or Update User Type
    @RequestMapping(value = "/saveOrUpdateUserType", method = RequestMethod.POST)
    public ModelAndView saveUserType(@ModelAttribute("userTypeForm") UmUserType formData, BindingResult result, Model model) {
        String successMsg = "";
        String errorMsg = "";
        int typeId = 0;
        try {
            if (formData.getUserTypeId() == null) {
                typeId = adminService.saveUserType(formData);
                successMsg = "Successfully Saved..!";
            } else {
                typeId = adminService.saveUserType(formData);
                successMsg = "Successfully Updated..!";
            }
            formData.setUserTypeId(typeId);

        } catch (Exception e) {
            errorMsg = "Save Process Fail..!";
        }
        model.addAttribute("successMsg", successMsg);
        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("admin/addUserTypeForm", "userType", formData);
    }

    // viewEdit User Type
    @RequestMapping(value = "/editUserType/{userTypeId}", method = RequestMethod.GET)
    public ModelAndView viewEditUserType(@PathVariable("userTypeId") int userTypeId, Model model) {
        boolean isSuperAdmin = false;
        boolean isAdmin = false;
        UmUserType umUserType = null;
        try {
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    isSuperAdmin = true;
                }
                if (loggedUserType == 18 || loggedUserType == 1) {
                    isAdmin = true;
                }
            }
            if (userTypeId > 0) {
                umUserType = adminService.findEditUserType(userTypeId);
            } else {
                umUserType = new UmUserType();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("isSuperAdmin", isSuperAdmin);
        model.addAttribute("isAdmin", isAdmin);
        return new ModelAndView("admin/addUserTypeForm", "userType", umUserType);
    }

    //job defind user Type
    @RequestMapping(value = "/loadJobs/{userTypeId}", method = RequestMethod.GET)
    public ModelAndView loadJobs(@PathVariable("userTypeId") int userTypeId, Model model) {
        List<AddJobsForm> addJobsList = null;
        List<AddJobsForm> defineJobs = null;
        Boolean hasPage = true;
        try {
            addJobsList = adminService.findJobList();
            defineJobs = adminService.findDefineJobs(userTypeId);
            if (defineJobs.isEmpty()) {
                hasPage = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("userTpe", userTypeId);
        model.addAttribute("jobs", addJobsList);
        model.addAttribute("defineJobs", defineJobs);
        model.addAttribute("hasPage", hasPage);
        model.addAttribute("customUser", 0);

        return new ModelAndView("admin/addPages");
    }

    //job defind user Type (for custom users)
    @RequestMapping(value = "/loadJobsForCustomUser/{empId}", method = RequestMethod.GET)
    public ModelAndView loadJobsForCustomUser(@PathVariable("empId") int empId, Model model) {
        List<AddJobsForm> addJobsList = null;
        List<AddJobsForm> defineJobs = null;
        int userId = 0;
        Boolean hasPage = true;
        try {
            addJobsList = adminService.findJobList();
            UmUser customUser = adminService.findUserForEmpID(empId);
            if (customUser != null) {
                defineJobs = adminService.findDefineJobs(customUser.getUserId());
                userId = customUser.getUserId();
            }
            if (defineJobs.isEmpty()) {
                hasPage = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("userTpe", userId);
        model.addAttribute("jobs", addJobsList);
        model.addAttribute("defineJobs", defineJobs);
        model.addAttribute("hasPage", hasPage);
        model.addAttribute("customUser", 1);

        return new ModelAndView("admin/addPages");
    }

    //save add pages
    @RequestMapping(value = "/saveAddPages", method = RequestMethod.POST)
    public ModelAndView saveAddPages(@ModelAttribute("jobsform") AddJobsForm formData, HttpServletRequest request, BindingResult result, Model model) {
        String message = "";
        boolean res = false;

        String[] tabId = request.getParameterValues("tabId");
        String[] isMainId = request.getParameterValues("isMainId");

        try {
            res = adminService.saveAddPages(formData, tabId, isMainId);
            if (res) {
                message = "Save Process Successfully";
                model.addAttribute("successMessage", message);
                return new ModelAndView("messages/success");
            } else {
                message = "Save Process Failed";
                model.addAttribute("errorMessage", message);
                return new ModelAndView("messages/error");
            }

        } catch (Exception e) {
            message = "Save Process Failed" + e;
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }

    }

    //<---------------------------End User Type ------------------------------->
    //<---------------------------Start Field Officer--------------------------->
    //Load Field Officer Form On FieldOfficerMain
    @RequestMapping(value = "/loadFieldOfficeForm", method = RequestMethod.GET)
    public ModelAndView loadFieldOfficerForm(Model model) {
        List<UmUser> recoManagers = null;
        List<UmUser> recoveryOfficer = null;
        try {
            recoManagers = adminService.findRManagers();
            recoveryOfficer = adminService.findOfficer();
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("Rmanager", recoManagers);
        model.addAttribute("ROfficer", recoveryOfficer);
        return new ModelAndView("admin/addFieldOfficerForm");
    }

    //Load FieldOfficerMain On MainPage
    @RequestMapping(value = "/loadFieldOfficerMain", method = RequestMethod.GET)
    public ModelAndView adminFieldOfficerMain(Model model) {
        List<UmFieldOfficers> manageres = null;
        List<UmUser> umUser = null;
        try {
            manageres = adminService.findRecoveryManageres();
            umUser = adminService.findAllUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("managers", manageres);
        model.addAttribute("UmUser", umUser);
        return new ModelAndView("admin/addFieldOfficeMain");
    }

    @RequestMapping(value = "/loadBranchForManagerID/{managerUserId}", method = RequestMethod.GET)
    public @ResponseBody
    List loadManagerBranch(@PathVariable("managerUserId") int managerUserId) {
        List mangerBranch = adminService.findManagerBranches(managerUserId);
        return mangerBranch;
    }

    //save Or Update Field Officer 
    @RequestMapping(value = "saveOrUpdateOfficer", method = RequestMethod.POST)
    public ModelAndView saveOrUpdatOfficer(@ModelAttribute("addFieldOfficerForm") UmFieldOfficers formData, Model model, BindingResult result, @RequestParam("officerID") int[] officerID) {
        String message = "";
        boolean res = false;

        try {
            res = adminService.saveFieldOfficer(formData, officerID);
            if (res) {
                message = "Save Process Successfully";
                model.addAttribute("successMessage", message);
                return new ModelAndView("messages/success");
            } else {
                message = "Save Process Failed";
                model.addAttribute("errorMessage", message);
                return new ModelAndView("messages/error");
            }

        } catch (Exception e) {
            message = "Save Process Failed" + e;
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }

    }

    //<---------------------------End Field Officer----------------------------->
    //add loadUserTypeLoanMain to Main Page
    @RequestMapping(value = "/loadUserTypeLoanMain", method = RequestMethod.GET)
    public ModelAndView adminLoadUserTypeLoan(Model model) {
        List<UmUser> umUsers = null;
        List typeLoans = null;
        try {
            umUsers = adminService.findUserWithOutSuperAdmin();
            typeLoans = adminService.findUserTypeSubLoans();
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("userList", umUsers);
        model.addAttribute("userTypeLoans", typeLoans);

        return new ModelAndView("admin/addUserTypeLoanMain");
    }

    //add viewUserTypeForm on addUserTypeLoanMain
    @RequestMapping(value = "/viewUserTypeForm", method = RequestMethod.GET)
    public ModelAndView viewUserTypeForm(Model model) {
        List<UmUser> umUsers = null;
        List<MSubLoanType> mSubLoanTypes = null;
        try {
            umUsers = adminService.findUserWithOutSuperAdmin();
            mSubLoanTypes = adminService.findSubLoanType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("userList", umUsers);
        model.addAttribute("subLoanType", mSubLoanTypes);

        return new ModelAndView("admin/addUserTypeLoanForm");
    }

    //<---------------------------Start SubLoan Type- ------------------------->
    //add subLoanType Form to MainPage , view Other Chargers, view Document Check List
    @RequestMapping(value = "/subLoanType", method = RequestMethod.GET)
    public ModelAndView adminSubLoanType(Model model) {
        List<MSubLoanType> mSubLoanTypes = null;
        boolean isSuperAdmin = false;
        try {
            mSubLoanTypes = adminService.findSubLoanType();
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    isSuperAdmin = true;
                }
            }
            model.addAttribute("mSubLoanTypes", mSubLoanTypes);
            model.addAttribute("isSuperAdmin", isSuperAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("admin/addSubLoanTypeMain");
    }

    //add addSubloanTypeForm Form to MainPage
    @RequestMapping(value = "/addSubloanTypeForm", method = RequestMethod.GET)
    public ModelAndView adminAddSubLoanTypeEdit(Model model) {
        List<MSubTaxCharges> chargeses = null;
        List<MCheckList> checkLists = null;
        List<MLoanType> mloanTypes = null;
        try {
            chargeses = adminService.findOtherCharges();
            checkLists = adminService.findCheckList();
            mloanTypes = adminService.findLoanType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("otherChargesList", chargeses);
        model.addAttribute("documentCheckList", checkLists);
        model.addAttribute("mloanTypes", mloanTypes);

        return new ModelAndView("admin/addSubLoanTypeForm");
    }

    //save Or Update Sub Loan Type
    @RequestMapping(value = "/saveSubLoanType", method = RequestMethod.POST)
    public ModelAndView saveSubLoanType(@ModelAttribute("SubLoanTypeForm") MSubLoanType formData, BindingResult result, @RequestParam("documentCheckListId") int[] documentCheckListId, @RequestParam("documentIsCompulsory") int[] documentIsCompulseryId, @RequestParam("otherChargeID") int[] otherChargesId, @RequestParam("chartOfAccount") int[] chartOfAccounts, @RequestParam("chartOfAccountName") String[] chartOfAccountName, Model model) {
        String message = "";
        try {
            if (formData.getSubLoanId() == null) {
                adminService.saveSubLoanType(formData, documentCheckListId, documentIsCompulseryId, otherChargesId, chartOfAccounts, chartOfAccountName);
                message = "Successfully Saved";
            } else {
                adminService.saveSubLoanType(formData, documentCheckListId, documentIsCompulseryId, otherChargesId, chartOfAccounts, chartOfAccountName);
                message = "Successfully Updated";
            }

        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    //<---------------------------End Sub Loan Type- -------------------------->
    //<---------------------------Start Loan Type- ------------------------->
    //add addLoanTypeForm Form to MainPage
    @RequestMapping(value = "/addLoanTypeForm", method = RequestMethod.GET)
    public ModelAndView adminAddLoanTypeForm() {
        return new ModelAndView("admin/addLoanTypeForm");
    }

    //save Or Update Loan type
    @RequestMapping(value = "/saveLoanType", method = RequestMethod.POST)
    public ModelAndView saveLoanType(@ModelAttribute("addLoanTypeForm") MLoanType formData, BindingResult result, Model model) {
        String massage = "";
        try {
            if (formData.getLoanTypeId() == null) {
                massage = "Successfully Saved";
                adminService.saveAddLoanType(formData);
            } else {
                massage = "Successfully Updated";
                adminService.saveAddLoanType(formData);
            }

        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", massage);
        return new ModelAndView("messages/success");
    }

    //<---------------------------End Loan Type- ------------------------------>
    //<---------------------------Start Other Charges- ------------------------>
    //add addOtherChargeMain to MainPage
    @RequestMapping(value = "/loadOtherChargesMain", method = RequestMethod.GET)
    public ModelAndView adminAddOtherCharges(Model model) {
        List<MSubTaxCharges> chargeses = null;
        try {
            chargeses = adminService.findOtherCharges();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("otherCharges", chargeses);
        return new ModelAndView("admin/addOtherChargeMain");
    }

    //add addOtherForm on addOtherChargeMain
    @RequestMapping(value = "/viewOtherChargesForm", method = RequestMethod.GET)
    public ModelAndView adminAddOtherChargesForm() {

        return new ModelAndView("admin/addOtherChargeForm");
    }

    //Save Or Update OtherCharges
    @RequestMapping(value = "/saveOrUpdateOtherCharges", method = RequestMethod.POST)
    public ModelAndView saveOtherCharges(@ModelAttribute("otherChargeForm") MSubTaxCharges formData, BindingResult result, Model model) {
        String massage = "";
        try {
            if (formData.getSubTaxId() == null) {
                adminService.saveOtherCharges(formData);
                massage = "Successfully Saved";
            } else {
                adminService.saveOtherCharges(formData);
                massage = "Successfully Updated";
            }

        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", massage);
        return new ModelAndView("messages/success");
    }

    //view Edit Other Charges
    @RequestMapping(value = "/viewEditOtherCharges/{otherId}", method = RequestMethod.GET)
    public ModelAndView viewEditOtherChaeges(@PathVariable("otherId") int otherChargeId, Model model) {
        MSubTaxCharges chargeses = null;
        try {
            chargeses = adminService.findOtherChargesList(otherChargeId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("otherCharge", chargeses);

        return new ModelAndView("admin/addOtherChargeForm");
    }

    //<---------------------------End Other Charges- -------------------------->
    //<---------------------------Start Approval Level------------------------->
    //add addApprovalMain Form to MainPage
    @RequestMapping(value = "/addApprovalMain", method = RequestMethod.GET)
    public ModelAndView adminAddApproveLevel(Model model) throws Exception {
        List<UmUserTypeApproval> userApprovals = null;
        List<UmUser> umUsers = null;
        //List<MDepartment> departments = null;
        try {
            userApprovals = adminService.findUserApproveLevels();
            umUsers = adminService.findAllUser();
            //departments = adminService.findDepartmentList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("appLevelList", userApprovals);
        model.addAttribute("userList", umUsers);
        // model.addAttribute("depList", departments);

        return new ModelAndView("admin/addApprovalMain");
    }

    //view viewApprovalForm form on addApprovalMain page
    @RequestMapping(value = "/viewApprovalForm", method = RequestMethod.GET)
    public ModelAndView adminViewApproveLevelForm(Model model) throws Exception {
        List<UmUserType> userTypes = null;
        List<MApproveLevels> approveLevelses = null;
        List users = null;

        try {
            userTypes = adminService.findUserType();
            approveLevelses = adminService.findAppLevel();
            users = adminService.findUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("userType", userTypes);
        model.addAttribute("appLevels", approveLevelses);
        model.addAttribute("user", users);
        return new ModelAndView("admin/addApprovalForm");
    }

    //save Or Update Approve Levels
    @RequestMapping(value = "/saveOrUpdateAppLevels", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateAppLevels(@ModelAttribute("adminApproveLevelForm") UmUserTypeApproval formData, BindingResult result, @RequestParam("approveId") int[] appLevels, Model model) {
        String message = "";
        try {
            if (formData.getPk() == null) {
                adminService.saveOrUpdateAppLevels(formData, appLevels);
                message = "Successfully Saved";
            } else {
                adminService.saveOrUpdateAppLevels(formData, appLevels);
                message = "Successfully Updated";
            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    //<---------------------------End Approval Level--------------------------->
    //<---------------------------Start Document Checklist--------------------->
    //add addDocumentListMain Form to MainPage
    @RequestMapping(value = "/loadDocumentListMain", method = RequestMethod.GET)
    public ModelAndView adminDocumentListMain(Model model) throws Exception {
        List<MCheckList> checkLists = null;
        try {
            checkLists = adminService.findCheckList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("documentList", checkLists);

        return new ModelAndView("admin/addDocumentListMain");
    }

    @RequestMapping(value = "/loadDocumentListForm", method = RequestMethod.GET)
    public ModelAndView viewDocumentListForm(Model model) throws Exception {

        return new ModelAndView("admin/addDocumentListForm");
    }

    //save Or Update Document List
    @RequestMapping(value = "/saveOrUpdateDocumentlist", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateDocumentList(@ModelAttribute("documentListForm") MCheckList formData, BindingResult result, Model model) {
        String massage = "";
        try {
            if (formData.getId() == null) {
                massage = "Successfully Saved";
                adminService.saveDocumentList(formData);
            } else {
                massage = "Successfully Updated";
                adminService.saveDocumentList(formData);
            }

        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", massage);
        return new ModelAndView("messages/success");
    }

    //find Document
    @RequestMapping(value = "/findDocument/{docId}", method = RequestMethod.GET)
    public ModelAndView findDocument(@PathVariable("docId") int docId, Model model) {
        MCheckList mcl = null;
        try {
            mcl = adminService.findDocument(docId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("mcheckList", mcl);
        return new ModelAndView("admin/addDocumentListForm");
    }

    //<---------------------------End Document CheckList----------------------->
    //save to DB
    //save Or Update User Type Sub Loan
    @RequestMapping(value = "/saveOrUpdateUserTypeSubLoan", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateUserTypeSubLoan(@ModelAttribute("adminUserTypeSubLoanForm") UmUserTypeLoan formData, BindingResult result, @RequestParam("subLoanId") int[] subLoanIdList, Model model) {
        String message = "";
        try {
            if (formData.getPk() == null) {
                adminService.saveOrUpdateUserSubLoan(formData, subLoanIdList);
                message = "Successfully Saved";
            } else {
                adminService.saveOrUpdateUserSubLoan(formData, subLoanIdList);
                message = "Successfully Updated";
            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    //find usertype loan list
    @RequestMapping(value = "/findUserTypeLoanList/{userId}", method = RequestMethod.GET)
    public ModelAndView findUserTypeLoanList(HttpServletRequest request, @PathVariable("userId") int userId, Model model) {
        List<MSubLoanType> mSubLoanTypes = null;
        List<UmUserTypeLoan> userTypeLoans = null;
        UmUser user = null;
        try {
            userTypeLoans = adminService.findUserTypeLoanList(userId);
            user = adminService.findUserName(userId);
            mSubLoanTypes = adminService.findSubLoanType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("user", user);
        model.addAttribute("userTypeLoans", userTypeLoans);
        model.addAttribute("subLoanType", mSubLoanTypes);

        return new ModelAndView("admin/addUserTypeLoanForm");
    }

    //save Or Update User
    @RequestMapping(value = "/saveOrUpdateUser", method = RequestMethod.POST)
    public ModelAndView saveUser(@ModelAttribute("addUserForm") UmUser formData, Model model) {
        String message = "Save Process Fail";
        boolean result = false;
        try {
            if (formData.getUserId() == null) {
                result = adminService.saveUser(formData);
                if (result) {
                    message = "SuccessFully Saved User";
                }
            } else {
                result = adminService.saveUser(formData);
                if (result) {
                    message = "SuccessFully Updated User";
                }
            }

        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    //view Update SubLoan Type
    @RequestMapping(value = "/viewUpdateSubLoanType/{subLoanId}", method = RequestMethod.GET)
    public ModelAndView UpdateSubLoanType(@PathVariable("subLoanId") int subLoanId, Model model) {
        MSubLoanType mSubLoanType = null;
        List<MLoanType> mloanTypes = null;
        List<MSubLoanChecklist> checklists = null;
        List<MSubLoanOtherChargers> otherChargers = null;
        List<MSubTaxCharges> chargeses = null;
        List<MCheckList> checkLists = null;
        List<ConfigChartofaccountLoan> chartOfAccounts = null;
        try {
            if (subLoanId > 0) {
                checklists = adminService.findCheckList(subLoanId);
                otherChargers = adminService.findOtherCharges(subLoanId);
                mSubLoanType = adminService.findMSubLoanType(subLoanId);
                chartOfAccounts = adminService.findConfigChartOfAccounts(subLoanId);

                mloanTypes = adminService.findLoanType();
                chargeses = adminService.findOtherCharges();
                checkLists = adminService.findCheckList();

            } else {
                mSubLoanType = new MSubLoanType();
                mloanTypes = new ArrayList<MLoanType>();
                checklists = new ArrayList<MSubLoanChecklist>();
                otherChargers = new ArrayList<MSubLoanOtherChargers>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("checkList", checklists);
        model.addAttribute("otherCharge", otherChargers);
        model.addAttribute("subLoanType", mSubLoanType);
        model.addAttribute("chartOfAccount", chartOfAccounts);

        model.addAttribute("mloanTypes", mloanTypes);
        model.addAttribute("otherChargesList", chargeses);
        model.addAttribute("documentCheckList", checkLists);

        return new ModelAndView("admin/addSubLoanTypeForm");
    }

    //view edit to Loan Type
    @RequestMapping(value = "/viewLoanType/{loanTypeId}", method = RequestMethod.GET)
    public ModelAndView viewLoanType(@PathVariable("loanTypeId") int loanTypeId, Model model) {
        MLoanType loanType = null;
        try {
            if (loanTypeId > 0) {
                loanType = adminService.findMLoanType(loanTypeId);
            } else {
                loanType = new MLoanType();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("admin/addLoanTypeForm", "loanType", loanType);
    }

    //view edit to Approve Levels 
    @RequestMapping(value = "/viewEditAppLevels/{userId}", method = RequestMethod.GET)
    public ModelAndView EditAppLevels(@PathVariable("userId") int userId, Model model) {
        List<UmUserTypeApproval> approvals = null;
        List users = null;
        List<MApproveLevels> approveLevelses = null;
        try {
            if (userId > 0) {
                approvals = adminService.findApproLevels(userId);
                users = adminService.findUser();
                approveLevelses = adminService.findAppLevel();
            } else {
                approvals = new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("e_approvals", approvals);
        model.addAttribute("userId", userId);
        model.addAttribute("user", users);
        model.addAttribute("appLevels", approveLevelses);
        return new ModelAndView("admin/addApprovalForm");
    }

    //check UserName alredy exist
    @RequestMapping(value = "/userNameCheck/{userName}", method = RequestMethod.GET)
    public @ResponseBody
    int checkUserName(@PathVariable("userName") String userName) {
        List<UmUser> users = adminService.findUserName(userName);
        if (users.size() > 0) {
            return 1;
        }
        return 0;
    }

    //view List
    @RequestMapping(value = "/loadLoanType", method = RequestMethod.GET)
    public @ResponseBody
    List<MLoanType> loadAddLoanType() {
        List<MLoanType> mLoanTypes = adminService.findLoanType();
        return mLoanTypes;
    }

    //load department by Branch 
    @RequestMapping(value = "/loadDepartment/{branchId}", method = RequestMethod.GET)
    public @ResponseBody
    List<MDepartment> loadDepartment(@PathVariable("branchId") int branchId) {
        List<MDepartment> departments = null;
        try {
            departments = adminService.findDepartment(branchId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return departments;
    }

    //load User by departmentId
    @RequestMapping(value = "/loadUser/{depId}", method = RequestMethod.GET)
    public @ResponseBody
    List<UmUser> loadUser(@PathVariable("depId") int depId) {
        List<UmUser> users = null;
        try {
            users = adminService.findAllUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    //load loanType for check List
    @RequestMapping(value = "/loadLoanTypeCheck", method = RequestMethod.GET)
    public @ResponseBody
    List<MLoanType> loadAddLoanTypeCheck() {
        List<MLoanType> mLoanTypes = adminService.findLoanType();
        return mLoanTypes;
    }

    @RequestMapping(value = "/loadSubLoanType", method = RequestMethod.GET)
    public @ResponseBody
    List<MSubLoanType> loadSubLoanType() {
        List<MSubLoanType> mSubLoanTypes = adminService.findSubLoanType();
        return mSubLoanTypes;
    }

    @RequestMapping(value = "/loadUserType", method = RequestMethod.GET)
    public @ResponseBody
    List<UmUserType> loadUserType() {
        List<UmUserType> userTypes = adminService.findUserType();
        return userTypes;
    }

    @RequestMapping(value = "/approveLevel", method = RequestMethod.GET)
    public @ResponseBody
    List<MApproveLevels> loadAppLevel() {
        List<MApproveLevels> approveLevels = adminService.findAppLevel();
        return approveLevels;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    //check SubLoanType alredy exist
    @RequestMapping(value = "/subLoanTypeCheck/{subLoanType}", method = RequestMethod.GET)
    public @ResponseBody
    int checkSubLoanType(@PathVariable("subLoanType") String subLoanType) {
        Boolean isExist = adminService.checkSubLoanType(subLoanType);
        if (isExist) {
            return 1;
        }
        return 0;
    }

    //save or update Supplier
    @RequestMapping(value = "/saveOrUpdateSupplier", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateSupplier(@ModelAttribute("newSupplierForm") MSupplier formData, Model model, BindingResult bindingResult) {
        String successMsg = "";
        String errorMsg = "";
        int supplierId = 0;

        try {
            if (formData.getSupplierId() == null) {
                supplierId = adminService.saveNewSupplier(formData);
                if (supplierId > 0) {
                    successMsg = "Save Process Successfully";
                    model.addAttribute("successMessage", successMsg);
                    return new ModelAndView("messages/success");
                } else {
                    errorMsg = "Save Process Failed";
                    model.addAttribute("errorMessage", errorMsg);
                    return new ModelAndView("messages/error");
                }
            } else {
                supplierId = adminService.saveNewSupplier(formData);
                if (supplierId > 0) {
                    successMsg = "Update Process Successfully";
                    model.addAttribute("successMessage", successMsg);
                    return new ModelAndView("messages/success");
                } else {
                    errorMsg = "Update Process Failed";
                    model.addAttribute("errorMessage", errorMsg);
                    return new ModelAndView("messages/error");
                }
            }

        } catch (Exception e) {
            errorMsg = "Save Or Update Process Failed" + e;
            model.addAttribute("errorMessage", errorMsg);
            return new ModelAndView("messages/error");
        }
    }

    //find Supplier
    @RequestMapping(value = "/findSupplier/{supplierId}", method = RequestMethod.GET)
    public ModelAndView findSupplier(@PathVariable("supplierId") int supplierId, Model model) {
        MSupplier mSupplier = null;
        try {
            mSupplier = adminService.findSupplier(supplierId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("supplier", mSupplier);
        return new ModelAndView("admin/addSupplierForm");
    }

    //load Suppliers
    @RequestMapping(value = "/loadSupplier", method = RequestMethod.GET)
    public @ResponseBody
    List<MSupplier> loadSuppliers() {
        List<MSupplier> mSuppliers = adminService.loadSuppliers();
        return mSuppliers;
    }

    //set addSupplier Form to Supplier MainPage
    @RequestMapping(value = "/addSupplierForm", method = RequestMethod.GET)
    public ModelAndView adminAddSupplierForm() {
        return new ModelAndView("admin/addSupplierForm");
    }

    //load Supplier MainPage
    @RequestMapping(value = "/loadAddSupplierMain", method = RequestMethod.GET)
    public ModelAndView adminAddSupplier(Model model) {
        return new ModelAndView("admin/addSupplierMain");
    }

    @RequestMapping(value = "/loadCollection", method = RequestMethod.GET)
    public ModelAndView loadCollection(HttpServletRequest request, Model model) {
        List<Employee> recOffs = new ArrayList<>();
        List<RecOffColDateForm> rocdfs = new ArrayList<>();
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            List list = masterDataService.findUsersByUserTypeAndBranch(4, Integer.parseInt(branchId));
            if (list.size() > 0) {
                for (Object ob : list) {
                    Object[] o = (Object[]) ob;
                    Employee emp = new Employee();
                    emp.setEmpNo(Integer.parseInt(o[0].toString()));
                    emp.setEmpLname(o[1].toString());
                    recOffs.add(emp);
                }
            }
            List<RecOffCollDates> clcDates = adminService.getCollectionDates(Integer.parseInt(branchId));
            for (RecOffCollDates ob : clcDates) {
                String[] dates = ob.getCollectionDates().split(",");
                for (String d : dates) {
                    if (!d.equals("")) {
                        RecOffColDateForm rocdf = new RecOffColDateForm();
                        rocdf.setRecOffId(ob.getEmpId());
                        rocdf.setDate(Integer.parseInt(d));
                        rocdfs.add(rocdf);
                    }
                }
            }
            model.addAttribute("recOffs", recOffs);
            model.addAttribute("rocdfs", rocdfs);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return new ModelAndView("admin/collectionForm");
    }

    @RequestMapping(value = "/saveOrUpadteCollectionDates", method = RequestMethod.POST, consumes = "application/json")
    public ModelAndView saveOrUpadteCollectionDates(@RequestBody RecOffColDateFormWrapper rocdfs, HttpServletRequest request, Model model) {
        String message = null;
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            adminService.saveOrUpdateCollectionDates(rocdfs.getRocdfs(), Integer.parseInt(branchId));
            message = "Collection Added";
            model.addAttribute("successMessage", message);
        } catch (NumberFormatException e) {
            message = "Error";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/getCollectionDates", method = RequestMethod.GET)
    @ResponseBody
    public List<RecOffColDateForm> getCollectionDates(HttpServletRequest request, Model model) {
        List<RecOffColDateForm> rocdfs = new ArrayList<>();
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            List<RecOffCollDates> clcDates = adminService.getCollectionDates(Integer.parseInt(branchId));
            for (RecOffCollDates ob : clcDates) {
                String[] dates = ob.getCollectionDates().split(",");
                for (String d : dates) {
                    if (!d.equals("")) {
                        RecOffColDateForm rocdf = new RecOffColDateForm();
                        rocdf.setRecOffId(ob.getEmpId());
                        rocdf.setDate(Integer.parseInt(d));
                        rocdfs.add(rocdf);
                    }
                }
            }
            System.out.println(rocdfs);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return rocdfs;
    }

    @RequestMapping(value = "/loadChartOfAccount", method = RequestMethod.GET)
    public ModelAndView ChartOfAccount(Model model) {
        List<Chartofaccount> chartofaccounts = null;
        try {
            chartofaccounts = adminService.getChartOfAccounts();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("charOfList", chartofaccounts);
        return new ModelAndView("admin/addChartOfAccount");
    }

    @RequestMapping(value = "/saveChartOfAccounts", method = RequestMethod.POST, consumes = "application/json")
    public ModelAndView saveChartOfAccounts(@RequestBody ChartOfAccountWrapper accounts, HttpServletRequest request, Model model) {
        String message = "";
        try {
            adminService.saveChartOfAccounts(accounts.getAccounts());
            message = "Successfully Saved";
            model.addAttribute("successMessage", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("messages/success");
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Seizer Details~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //Load addSeizerMain to MainPage
    @RequestMapping(value = "/loadNewSeizerMain", method = RequestMethod.GET)
    public ModelAndView loadNewSeizerMain(Model model) {
        List<MSeizerDetails> seizerDetails = null;
        try {
            seizerDetails = adminService.findSeizerDetailsList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("seizerList", seizerDetails);
        return new ModelAndView("admin/addSeizerMain");
    }

    @RequestMapping(value = "/loadAddSeizerForm", method = RequestMethod.GET)
    public ModelAndView loadAddSeizerForm(Model model) {

        return new ModelAndView("admin/addSeizerForm");
    }

    @RequestMapping(value = "/inActiveSeizer/{seizerId}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean inActiveSeizer(@PathVariable("seizerId") int seizerId, Model model) {
        Boolean inActive = false;
        try {
            inActive = adminService.inActiveSeizer(seizerId);
        } catch (Exception e) {
            e.printStackTrace();

        }

        return inActive;
    }

    @RequestMapping(value = "/activeSeizer/{seizerId}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean activeSeizer(@PathVariable("seizerId") int seizerId, Model model) {
        Boolean active = false;
        try {
            active = adminService.activeSeizer(seizerId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return active;
    }

    @RequestMapping(value = "/saveOrUpdateNewSeizer", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateSeizerDetail(@ModelAttribute("newSeizerForm") MSeizerDetails formData, Model model) {
        String message = "";
        try {
            Boolean isSave = false;
            if (formData.getSeizerId() != null) {
                isSave = adminService.saveOrUpdateSeizerDetails(formData);
                if (isSave) {
                    message = "Update Process Successfully";
                    model.addAttribute("successMessage", message);
                    return new ModelAndView("messages/success");
                } else {
                    message = "Update Process Failed";
                    model.addAttribute("errorMessage", message);
                    return new ModelAndView("messages/error");
                }
            } else {
                isSave = adminService.saveOrUpdateSeizerDetails(formData);
                if (isSave) {
                    message = "Save Process Successfully";
                    model.addAttribute("successMessage", message);
                    return new ModelAndView("messages/success");
                } else {
                    message = "Save Process Failed";
                    model.addAttribute("errorMessage", message);
                    return new ModelAndView("messages/error");
                }
            }
        } catch (Exception e) {
            message = "Save Process Failed";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }

    }

    @RequestMapping(value = "/editSeizerDetails/{seizerId}", method = RequestMethod.GET)
    public ModelAndView editSeizerDetails(@PathVariable("seizerId") int seizerID, Model model) {
        MSeizerDetails details = null;
        try {
            details = adminService.findSeizerDetailsList(seizerID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("seizer", details);
        return new ModelAndView("admin/addSeizerForm");
    }

    @RequestMapping(value = "/loadModules", method = RequestMethod.GET)
    public ModelAndView loadModules(Model model) {
        List<JobDefine> jobDefines = null;
        try {
            jobDefines = masterDataService.findUserJobs();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("jobDefines", jobDefines);
        return new ModelAndView("admin/addModules");
    }

    @RequestMapping(value = "/addOrRemoveModules", method = RequestMethod.POST)
    public ModelAndView addOrRemoveModules(Model model, HttpServletRequest request) {
        ModelAndView modelAndView = null;
        String message = "";
        String status = request.getParameter("status");
        String[] modules = request.getParameterValues("modules");
        try {
            boolean success = adminService.addOrRemoveModules(modules, Integer.parseInt(status));
            if (success) {
                message = "Save Process Successfully";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Save Process Failed";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/changeLoanType", method = RequestMethod.GET)
    public ModelAndView changeLoanType(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String loanTypeId = request.getParameter("loanTypeId");
        String status = request.getParameter("status");
        try {
            boolean success = adminService.changeLoanType(Integer.parseInt(loanTypeId), Integer.parseInt(status));
            if (success) {
                if (status.equals("1")) {
                    message = "Successfully Active Loan Type";
                } else {
                    message = "Successfully In-Active Loan Type";
                }
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Process Failed";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/changeSubLoanType", method = RequestMethod.GET)
    public ModelAndView changeSubLoanType(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String subLoanTypeId = request.getParameter("subLoanTypeId");
        String status = request.getParameter("status");
        try {
            boolean success = adminService.changeSubLoanType(Integer.parseInt(subLoanTypeId), Integer.parseInt(status));
            if (success) {
                if (status.equals("1")) {
                    message = "Successfully Active SubLoan Type";
                } else {
                    message = "Successfully In-Active SubLoan Type";
                }
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Process Failed";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadAddYardMainPage", method = RequestMethod.GET)
    public ModelAndView loadAddYardMainPage(Model model) {
        List<MYardDetails> yardDetails = null;
        try {
            yardDetails = adminService.findYardDetailsList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("yardList", yardDetails);
        return new ModelAndView("admin/addYardMain");
    }

    @RequestMapping(value = "/loadAddYardForm", method = RequestMethod.GET)
    public ModelAndView loadAddYardForm(Model model) {
        return new ModelAndView("admin/addYardForm");
    }

    @RequestMapping(value = "/saveOrUpdateNewYard", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateYardDetail(@ModelAttribute("newYardForm") MYardDetails formData, Model model) {
        String message = "";
        try {
            Boolean isSave = false;
            if (formData.getYardId() != null) {
                isSave = adminService.saveOrUpdateYardDetails(formData);
                if (isSave) {
                    message = "Update Process Successfully";
                    model.addAttribute("successMessage", message);
                    return new ModelAndView("messages/success");
                } else {
                    message = "Update Process Failed";
                    model.addAttribute("errorMessage", message);
                    return new ModelAndView("messages/error");
                }
            } else {
                isSave = adminService.saveOrUpdateYardDetails(formData);
                if (isSave) {
                    message = "Save Process Successfully";
                    model.addAttribute("successMessage", message);
                    return new ModelAndView("messages/success");
                } else {
                    message = "Save Process Failed";
                    model.addAttribute("errorMessage", message);
                    return new ModelAndView("messages/error");
                }
            }
        } catch (Exception e) {
            message = "Save Process Failed";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
    }

    @RequestMapping(value = "/editYardDetails/{yardId}", method = RequestMethod.GET)
    public ModelAndView editYardDetails(@PathVariable("yardId") int yardID, Model model) {
        MYardDetails details = null;
        try {
            details = adminService.findYardDetailsList(yardID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("yard", details);
        return new ModelAndView("admin/addYardForm");
    }

    @RequestMapping(value = "/inActiveYard/{yardId}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean inActiveYard(@PathVariable("yardId") int yardId, Model model) {
        Boolean inActive = false;
        try {
            inActive = adminService.inActiveYard(yardId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inActive;
    }

    @RequestMapping(value = "/activeYard/{yardId}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean activeYard(@PathVariable("yardId") int yardId, Model model) {
        Boolean active = false;
        try {
            active = adminService.activeYard(yardId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return active;
    }

    //    ===================== Vehicle =====================
    @RequestMapping(value = "/loadAddVehicleMainPage", method = RequestMethod.GET)
    public ModelAndView loadAddVehicleMainPage(Model model) {
        List<VehicleType> vehicleTypes = null;
        try {
            vehicleTypes = adminService.findVehicleTypesList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("vehicleTypeList", vehicleTypes);
        return new ModelAndView("admin/addVehicleMain");
    }

    @RequestMapping(value = "/loadAddVehicleForm", method = RequestMethod.GET)
    public ModelAndView loadAddVehiclForm(Model model) {

        return new ModelAndView("admin/addVehicleForm");
    }

    @RequestMapping(value = "/saveOrUpdateVehicleType", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateVehicleTypeDetail(@ModelAttribute("vehicleTypeForm") VehicleType formData, Model model) {
        String message = "";
        try {
            Boolean isSave = false;
            if (formData.getVehicleType() != null) {
                isSave = adminService.saveOrUpdateVehicleDetails(formData);
                if (isSave) {
                    message = "Update Process Successfully";
                    model.addAttribute("successMessage", message);
                    return new ModelAndView("messages/success");
                } else {
                    message = "Update Process Failed";
                    model.addAttribute("errorMessage", message);
                    return new ModelAndView("messages/error");
                }
            } else {
                isSave = adminService.saveOrUpdateVehicleDetails(formData);
                if (isSave) {
                    message = "Save Process Successfully";
                    model.addAttribute("successMessage", message);
                    return new ModelAndView("messages/success");
                } else {
                    message = "Save Process Failed";
                    model.addAttribute("errorMessage", message);
                    return new ModelAndView("messages/error");
                }
            }
        } catch (Exception e) {
            message = "Save Process Failed";
            model.addAttribute("errorMessage", message);
            return new ModelAndView("messages/error");
        }
    }

    @RequestMapping(value = "/editeVehicleType/{vehicleType}", method = RequestMethod.GET)
    public ModelAndView editVehicleType(@PathVariable("vehicleType") int vehicleTypeId, Model model) {
        VehicleType details = null;
        try {
            details = adminService.findVehicleTypeList(vehicleTypeId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("vehicle", details);
        return new ModelAndView("admin/addVehicleForm");
    }

    @RequestMapping(value = "/loadAddMakeForm/{vType}", method = RequestMethod.GET)
    public ModelAndView loadAddmakeForm(@PathVariable("vType") int vehicleType, Model model) {
        List<VehicleMake> vehicleMakesList = null;

        try {
            vehicleMakesList = adminService.findVehicleMakeList(vehicleType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("vehicle_Type", vehicleType);
        model.addAttribute("vehicleMakesList", vehicleMakesList);
        return new ModelAndView("admin/addVehicleMakeForm");
    }

    @RequestMapping(value = "/saveOrUpdateMakeType", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateVehicleMakeDetail(@ModelAttribute("vehicleMakeForm") VehicleMake formData) {
        String message = "";
        try {
            Boolean isSave = false;
            if (formData.getVehicleType() != null) {
                isSave = adminService.saveOrUpdateVehicleMakeDetails(formData);
                if (isSave) {
                    message = "Update Process Successfully";
                } else {
                    message = "Update Process Failed";
                }
            } else {
                isSave = adminService.saveOrUpdateVehicleMakeDetails(formData);
                if (isSave) {
                    message = "Save Process Successfully";

                } else {
                    message = "Save Process Failed";
                }
            }
        } catch (Exception e) {
            message = "Save Process Failed";

        }
        return message;
    }

    @RequestMapping(value = "/loadAddModelForm", method = RequestMethod.GET)
    public ModelAndView loadAddModelForm(HttpServletRequest request, Model model) {
        //  int makeId = Integer.parseInt(request.getParameter("vehicleMake"));
        int vehicleTypeId = Integer.parseInt(request.getParameter("vehicleType"));

        List<VehicleMake> makes = null;
        List<VehicleModel> models = null;
        try {
            makes = adminService.findVehicleMakeList(vehicleTypeId);
            models = adminService.findVehicleModelList(vehicleTypeId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("makeList", makes);
        model.addAttribute("modelList", models);
        model.addAttribute("vehicleTypeId", vehicleTypeId);
        return new ModelAndView("admin/addVehicleModelForm");
    }

    @RequestMapping(value = "/saveOrUpdateModel", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateVehicleModelDetail(@ModelAttribute("vehicleModelForm") VehicleModel formData) {
        String message = "";
        try {
            Boolean isSave = false;
            isSave = adminService.saveOrUpdateVehicleModelDetail(formData);
            if (isSave) {
                message = "Update Process Successfully";
            } else {
                message = "Update Process Failed";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    @RequestMapping(value = "/inActiveVehicle/{vehicleType}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean inActiveVehicle(@PathVariable("vehicleType") int vehiledId, Model model) {
        Boolean inActive = false;
        try {
            inActive = adminService.inActiveVehicle(vehiledId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inActive;
    }

    @RequestMapping(value = "/activeVehicle/{vehicleType}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean activeVehicle(@PathVariable("vehicleType") int vehiledId, Model model) {
        Boolean active = false;
        try {
            active = adminService.activeVehicle(vehiledId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return active;
    }

    //<---------------------------Start Saving Accounts Type------------------------->

    @RequestMapping(value = "/loadAddAccountTypeForm", method = RequestMethod.GET)
    public ModelAndView loadAddAccountTypeForm(Model model) {
        List<MSavingsType> savingsTypes = masterService.findAllSavingsTypes();
        model.addAttribute("mAccountTypes",savingsTypes);
        return new ModelAndView("admin/savings/addAccountTypeMain");
    }

    @RequestMapping(value = "/changeAccountType", method = RequestMethod.GET)
    public ModelAndView changeAccountType(HttpServletRequest request, Model model) {
//        ModelAndView modelAndView = null;
//        String message = "";
//        String accountTypeId = request.getParameter("accountTypeId");
//        String status = request.getParameter("status");
//        try {
//            boolean success = adminService.changeAccountType(Integer.parseInt(accountTypeId), Integer.parseInt(status));
//            if (success) {
//                if (status.equals("1")) {
//                    message = "Successfully Active Account Type";
//                } else {
//                    message = "Successfully In-Active Account Type";
//                }
//                model.addAttribute("successMessage", message);
//                modelAndView = new ModelAndView("messages/success");
//            } else {
//                message = "Process Failed";
//                model.addAttribute("errorMessage", message);
//                modelAndView = new ModelAndView("messages/error");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return modelAndView;
        return new ModelAndView("admin/savings/addAccountTypeMain");
    }

    @RequestMapping(value = "/addAccountTypeForm", method = RequestMethod.GET)
    public ModelAndView addAccountTypeForm() {

        return new ModelAndView("admin/savings/addAccountTypeForm");
    }

    //<---------------------------End Saving Accounts Type------------------------->
    //<---------------------------Start Sub Accounts Type- ------------------------->

    @RequestMapping(value = "/subAccountTypes", method = RequestMethod.GET)
    public ModelAndView subAccountType(Model model) {
        boolean isSuperAdmin = false;
        try {
            List<MSubSavingsType> subSavingsTypes = masterService.findAllSubSavingsTypes();
            int loggedUser = userService.findByUserName();
            if (loggedUser > 0) {
                int loggedUserType = userService.findUserTypeByUserId(loggedUser);
                if (loggedUserType == 18) {
                    isSuperAdmin = true;
                }
            }
            model.addAttribute("mSubAccountTypes", subSavingsTypes);
            model.addAttribute("isSuperAdmin", isSuperAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("admin/savings/addSubAccountTypeMain");
    }

    @RequestMapping(value = "/addSubAccountTypeForm", method = RequestMethod.GET)
    public ModelAndView adminAddSubAccountTypeEdit(Model model) {
        model.addAttribute("addSubAccountType", new MSubSavingsType());
        model.addAttribute("maccountTypes",masterService.findAllSavingsTypes());
        return new ModelAndView("admin/savings/addSubAccountTypeForm");
    }

    @RequestMapping(value = "/subAccountTypeCheck/{subAccountType}", method = RequestMethod.GET)
    public @ResponseBody
    int checkSubAccountType(@PathVariable("subAccountType") String subAccountType) {
//        Boolean isExist = adminService.checkSubAccountType(subAccountType);
////        if (isExist) {
////            return 1;
////        }
        return 0;
    }

    @RequestMapping(value = "/changeSubAccountType", method = RequestMethod.GET)
    public ModelAndView changeSubAccountType(HttpServletRequest request, Model model) {
//        ModelAndView modelAndView = null;
//        String message = "";
//        String subAccountTypeId = request.getParameter("subAccountTypeId");
//        String status = request.getParameter("status");
//        try {
//            boolean success = adminService.changeSubAccountType(Integer.parseInt(subAccountTypeId), Integer.parseInt(status));
//            if (success) {
//                if (status.equals("1")) {
//                    message = "Successfully Active SubAccount Type";
//                } else {
//                    message = "Successfully In-Active SubAccount Type";
//                }
//                model.addAttribute("successMessage", message);
//                modelAndView = new ModelAndView("messages/success");
//            } else {
//                message = "Process Failed";
//                model.addAttribute("errorMessage", message);
//                modelAndView = new ModelAndView("messages/error");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////        return modelAndView;
        return new ModelAndView("admin/savings/addSubAccountTypeMain");
    }

//    @RequestMapping(value = "/saveSubAccountType", method = RequestMethod.POST)
//    public ModelAndView saveSubAccountType(@ModelAttribute("SubAccountTypeForm") MSubAccountType formData,
//                                           BindingResult result,
//                                           @RequestParam("documentCheckListId") int[] documentCheckListId,
//                                           @RequestParam("documentIsCompulsory") int[] documentIsCompulseryId,
//                                           @RequestParam("otherChargeID") int[] otherChargesId,
//                                           @RequestParam("chartOfAccount") int[] chartOfAccounts,
//                                           @RequestParam("chartOfAccountName") String[] chartOfAccountName, Model model) {
//        String message = "";
//        try {
//            if (formData.getSubAccountId() == null) {
//                adminService.saveSubAccountType(formData, documentCheckListId, documentIsCompulseryId, otherChargesId, chartOfAccounts, chartOfAccountName);
//                message = "Successfully Saved";
//            } else {
//                adminService.saveSubAccountType(formData, documentCheckListId, documentIsCompulseryId, otherChargesId, chartOfAccounts, chartOfAccountName);
//                message = "Successfully Updated";
//            }

//        } catch (Exception e) {
//            e.printStackTrace();
//            model.addAttribute("errorMessage", "Save Process Fail");
//            return new ModelAndView("messages/error");
//        }
//        model.addAttribute("successMessage", message);
//        return new ModelAndView("messages/success");
//    }

    //<---------------------------End Sub Accounts Type- -------------------------->
}

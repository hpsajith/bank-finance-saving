/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.DebtorForm;
import com.bankfinance.form.GroupCustomerForm;
import com.ites.bankfinance.model.DebtorAssessBankDetails;
import com.ites.bankfinance.model.DebtorAssessRealestateDetails;
import com.ites.bankfinance.model.DebtorAssessVehicleDetails;
import com.ites.bankfinance.model.DebtorBusinessDetails;
import com.ites.bankfinance.model.DebtorDependentDetails;
import com.ites.bankfinance.model.DebtorEmploymentDetails;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.DebtorLiabilityDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.DebtorService;
import com.ites.bankfinance.service.LoanService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.ites.bankfinance.service.SavingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DebtorController {

    @Autowired
    private DebtorService debtorService;

    @Autowired
    private AdminService adminService;

    @Autowired
    LoanService loanService;

//    @Autowired
//    SavingsService savingsService;

    @RequestMapping(value = "/saveDebtorFrom", method = RequestMethod.POST)
    public ModelAndView saveDebtorForm(@ModelAttribute("debtorForm") DebtorForm formData, BindingResult result, Model model, HttpServletRequest request) {

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        formData.setBranchId(branchId);

        int debtorId = debtorService.saveDebtorDetails(formData);
        DebtorForm debtor = debtorService.findDebtorOtherDetails(debtorId);

        int dependentId = 0;
        List<DebtorDependentDetails> dependent = debtor.getDependent();
        if (dependent.size() > 0) {
            dependentId = dependent.get(0).getDependentId();
        }

        model.addAttribute("id", debtorId);
        model.addAttribute("dependentId", dependentId);
        model.addAttribute("dependent", debtor.getDependent());
        model.addAttribute("business", debtor.getBusiness());
        model.addAttribute("employee", debtor.getEmployement());
        model.addAttribute("liability", debtor.getLiability());
        model.addAttribute("bank", debtor.getBank());
        model.addAttribute("vehicle", debtor.getVehicle());
        model.addAttribute("land", debtor.getLand());
        model.addAttribute("edit", true);

        return new ModelAndView("debtor/newCustomer", "debtor", debtor);
    }

    @RequestMapping(value = "/sepSaveDebtorFrom", method = RequestMethod.POST)
    public ModelAndView sepSaveDebtorForm(@ModelAttribute("debtorForm") DebtorForm formData, BindingResult result, Model model, HttpServletRequest request) {

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        formData.setBranchId(branchId);

        MBranch currentBranch = null;
        currentBranch = adminService.findActiveBranch(branchId);
        String branchCode = currentBranch.getBranchCode();

        int debtorId = debtorService.saveDebtorDetails(formData);
        DebtorForm debtor = debtorService.findDebtorOtherDetails(debtorId);

        int dependentId = 0;
        List<DebtorDependentDetails> dependent = debtor.getDependent();
        if (dependent.size() > 0) {
            dependentId = dependent.get(0).getDependentId();
        }

        model.addAttribute("id", debtorId);
        model.addAttribute("dependentId", dependentId);
        model.addAttribute("dependent", debtor.getDependent());
        model.addAttribute("business", debtor.getBusiness());
        model.addAttribute("employee", debtor.getEmployement());
        model.addAttribute("liability", debtor.getLiability());
        model.addAttribute("bank", debtor.getBank());
        model.addAttribute("vehicle", debtor.getVehicle());
        model.addAttribute("land", debtor.getLand());
        model.addAttribute("branchCode", branchCode);
        model.addAttribute("edit", true);

        return new ModelAndView("debtor/sepNewCustomer", "debtor", debtor);
    }

    @RequestMapping(value = "/viewcustomor/{debtorId}", method = RequestMethod.GET)
    public ModelAndView viewcustomor(@PathVariable("debtorId") int debtorId, Model model) {

        DebtorForm debtor = debtorService.findDebtorOtherDetails(debtorId);
        model.addAttribute("id", debtorId);
        model.addAttribute("dependent", debtor.getDependent());
        model.addAttribute("business", debtor.getBusiness());
        model.addAttribute("employee", debtor.getEmployement());
        model.addAttribute("liability", debtor.getLiability());
        model.addAttribute("bank", debtor.getBank());
        model.addAttribute("vehicle", debtor.getVehicle());
        model.addAttribute("land", debtor.getLand());
        return new ModelAndView("debtor/editCustomer", "debtor", debtor);
    }

    @RequestMapping(value = "/viewDependentForm/{debtorId}/{depedentId}", method = RequestMethod.GET)
    public ModelAndView viewDependentForm(@PathVariable("debtorId") int debtorId, @PathVariable("depedentId") int depedentId, Model model) {
        if (depedentId > 0) {
            DebtorDependentDetails dependent = debtorService.findDependent(depedentId);
            return new ModelAndView("debtor/dependentForm", "dep", dependent);
        } else {
            DebtorDependentDetails newDependent = new DebtorDependentDetails();
            newDependent.setDebtorId(debtorId);
            return new ModelAndView("debtor/dependentForm", "dep", newDependent);
        }
    }

    @RequestMapping(value = "/saveOrUpdateDependent", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateDependent(@ModelAttribute("dependentLoanForm") DebtorDependentDetails formData, BindingResult result, Model model) {
        String massge = "Successfully Saved";
        debtorService.saveLoanDependentDetails(formData);
        return massge;
    }

    @RequestMapping(value = "/viewEmplyementForm/{debtorId}/{id}", method = RequestMethod.GET)
    public ModelAndView viewEmplyementForm(@PathVariable("debtorId") int debtorId, @PathVariable("id") int id, Model model) {
        if (id > 0) {
            DebtorEmploymentDetails employemnt = debtorService.findEmployment(id);
            return new ModelAndView("debtor/employmentForm", "emp", employemnt);
        } else {
            DebtorEmploymentDetails newEmployement = new DebtorEmploymentDetails();
            newEmployement.setDebtorId(debtorId);
            return new ModelAndView("debtor/employmentForm", "emp", newEmployement);
        }
    }

    @RequestMapping(value = "/saveOrUpdateEmployement", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateEmployement(@ModelAttribute("employementLoanForm") DebtorEmploymentDetails formData, BindingResult result, Model model) {
        String massge = "Successfully Saved";
        debtorService.saveLoanEmploymentDetails(formData);
        return massge;
    }

    @RequestMapping(value = "/viewBusinessForm/{debtorId}/{id}", method = RequestMethod.GET)
    public ModelAndView viewBusinessForm(@PathVariable("debtorId") int debtorId, @PathVariable("id") int id, Model model) {
        if (id > 0) {
            DebtorBusinessDetails business = debtorService.findBusiness(id);
            return new ModelAndView("debtor/businessForm", "bus", business);
        } else {
            DebtorBusinessDetails newBusiness = new DebtorBusinessDetails();
            newBusiness.setDebtorId(debtorId);
            return new ModelAndView("debtor/businessForm", "bus", newBusiness);
        }
    }

    @RequestMapping(value = "/saveOrUpdateBusiness", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateBusiness(@ModelAttribute("businessLoanForm") DebtorBusinessDetails formData, BindingResult result, Model model) {
        String massge = "Successfully Saved";
        debtorService.saveLoanBusinessDetails(formData);
        return massge;
    }

    @RequestMapping(value = "/viewLiabilityForm/{debtorId}/{id}", method = RequestMethod.GET)
    public ModelAndView viewLiabilityForm(@PathVariable("debtorId") int debtorId, @PathVariable("id") int id, Model model) {
        if (id > 0) {
            DebtorLiabilityDetails liability = debtorService.findLiability(id);
            return new ModelAndView("debtor/liabilityForm", "lia", liability);
        } else {
            DebtorLiabilityDetails newLiability = new DebtorLiabilityDetails();
            newLiability.setDebtorId(debtorId);
            return new ModelAndView("debtor/liabilityForm", "lia", newLiability);
        }
    }

    @RequestMapping(value = "/saveOrUpdateLiability", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateLiability(@ModelAttribute("liabilityLoanForm") DebtorLiabilityDetails formData, BindingResult result, Model model) {
        String massge = "Successfully Saved";
        debtorService.saveLoanLiabilityDetails(formData);
        return massge;
    }

    @RequestMapping(value = "/viewAssetForms", method = RequestMethod.GET)
    public ModelAndView viewAssetForms() {
        return new ModelAndView("debtor/assetForm");
    }

    @RequestMapping(value = "/viewAssetBankForm/{debtorId}/{id}", method = RequestMethod.GET)
    public ModelAndView viewAssetBankForm(@PathVariable("debtorId") int debtorId, @PathVariable("id") int id, Model model) {
        if (id > 0) {
            DebtorAssessBankDetails bank = debtorService.findAssetBank(id);
            return new ModelAndView("debtor/assessBankForm", "bank", bank);
        } else {
            DebtorAssessBankDetails newBank = new DebtorAssessBankDetails();
            newBank.setDebtorId(debtorId);
            return new ModelAndView("debtor/assessBankForm", "bank", newBank);
        }
    }

    @RequestMapping(value = "/saveOrUpdateAssetBank", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateAssetBank(@ModelAttribute("assetBankDetails") DebtorAssessBankDetails formData, BindingResult result, Model model) {
        String massge = "Successfully Saved";
        debtorService.saveLoanAssessBankDetails(formData);
        return massge;
    }

    @RequestMapping(value = "/viewAssetVehicleForm/{debtorId}/{id}", method = RequestMethod.GET)
    public ModelAndView viewAssetVehicleForm(@PathVariable("debtorId") int debtorId, @PathVariable("id") int id, Model model) {
        if (id > 0) {
            DebtorAssessVehicleDetails vehicle = debtorService.findAssetVehicle(id);
            return new ModelAndView("debtor/assessVehicleForm", "vehicle", vehicle);
        } else {
            DebtorAssessVehicleDetails newVehicle = new DebtorAssessVehicleDetails();
            newVehicle.setDebtorId(debtorId);
            return new ModelAndView("debtor/assessVehicleForm", "vehicle", newVehicle);
        }
    }

    @RequestMapping(value = "/saveOrUpdateAssetVehicle", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateAssetVehicle(@ModelAttribute("assetVehicleDetails") DebtorAssessVehicleDetails formData, BindingResult result, Model model) {
        String massge = "Successfully Saved";
        debtorService.saveLoanAssessVehicleDetails(formData);
        return massge;
    }

    @RequestMapping(value = "/viewAssetLandForm/{debtorId}/{id}", method = RequestMethod.GET)
    public ModelAndView viewAssetLandForm(@PathVariable("debtorId") int debtorId, @PathVariable("id") int id, Model model) {
        if (id > 0) {
            DebtorAssessRealestateDetails land = debtorService.findAssetLand(id);
            return new ModelAndView("debtor/assessLandForm", "vehicle", land);
        } else {
            DebtorAssessRealestateDetails newLand = new DebtorAssessRealestateDetails();
            newLand.setDebtorId(debtorId);
            return new ModelAndView("debtor/assessLandForm", "vehicle", newLand);
        }
    }

    @RequestMapping(value = "/saveOrUpdateAssetLand", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdateAssetLand(@ModelAttribute("assetLandDetails") DebtorAssessRealestateDetails formData, BindingResult result, Model model) {
        String massge = "Successfully Saved";
        debtorService.saveLoanAssessLandDetails(formData);
        return massge;
    }

    @RequestMapping(value = "/editDebtorFrom/{debtorId}", method = RequestMethod.GET)
    public ModelAndView editDebtorFrom(@PathVariable("debtorId") int debtorId, Model model) {
        DebtorForm debtor = debtorService.findDebtorOtherDetails(debtorId);
        int dependentId = 0;
        List<DebtorDependentDetails> dependent = debtor.getDependent();
        if (dependent.size() > 0) {
            DebtorDependentDetails dep = (DebtorDependentDetails) dependent.get(0);
            dependentId = dep.getDependentId();
            debtor.setDependentId(dep.getDependentId());
            debtor.setDependentName(dep.getDependentName());
            debtor.setDependentNic(dep.getDependentNic());
            debtor.setDependentJob(dep.getDependentJob());
            debtor.setDependentOccupation(dep.getDependentOccupation());
            debtor.setDependentOfficeName(dep.getDependentOfficeName());
            debtor.setDependentOfficeAddress(dep.getDependentOfficeAddress());
            debtor.setDependentOfficeTelephone(dep.getDependentOfficeTelephone());
            debtor.setDependentNetSalary(dep.getDependentNetSalary());
            debtor.setDependentTelephone(dep.getDependentTelephone());
        }

        model.addAttribute("id", debtorId);
        model.addAttribute("dependentId", dependentId);
        model.addAttribute("dependent", debtor.getDependent());
        model.addAttribute("business", debtor.getBusiness());
        model.addAttribute("employee", debtor.getEmployement());
        model.addAttribute("liability", debtor.getLiability());
        model.addAttribute("bank", debtor.getBank());
        model.addAttribute("vehicle", debtor.getVehicle());
        model.addAttribute("land", debtor.getLand());
        model.addAttribute("edit", true);
        return new ModelAndView("debtor/newCustomer", "debtor", debtor);
    }

//    @RequestMapping(value = "/updateDebtorForm", method = RequestMethod.POST)
//    public @ResponseBody
//    String updateDebtorForm(@ModelAttribute("debtorHeaderDetail") DebtorHeaderDetails debHeadDetails, BindingResult result, Model model) {
//        String message = "Successfully Updated";
//        debtorService.updateDebtorDetails(debHeadDetails);
//        return message;
//    }

    @RequestMapping(value = "/updateDebtorForm", method = RequestMethod.POST)
    public @ResponseBody
    String updateDebtorForm(@ModelAttribute("debtorHeaderDetail") DebtorHeaderDetails debHeadDetails, BindingResult result, Model model) {
        String message = null;
        try {
            List<LoanHeaderDetails> state = debtorService.findDebtorLoanApprovalStatus(debHeadDetails.getDebtorId());
            if (state.stream().anyMatch(ti -> ti.getLoanApprovalStatus() > 3)) {
                message = "Sorry! An Approved Loan Exists";
            } else {
                debtorService.updateDebtorDetails(debHeadDetails);
                message = "Successfully Updated";
            }
        }catch (Exception ex){
            message="An Error Occurred";
            ex.printStackTrace();
        }
        return message;
    }

    @RequestMapping(value = "/loadDebtorSearch", method = RequestMethod.GET)
    public ModelAndView loadDebtorSearch(Model model) {

        return new ModelAndView("debtor/mainDebtor");
    }

    @RequestMapping(value = "/checkDebtorNic/{nic}", method = RequestMethod.GET)
    public @ResponseBody
    int checkDebtorForNic(@PathVariable("nic") String nicNo) {
        List<DebtorHeaderDetails> detailses = debtorService.findDebtorBuNic(nicNo);
        if (detailses.size() > 0) {
            return 1;
        }
        return 0;

    }

    @RequestMapping(value = "/searchByNic2/{nicNo}", method = RequestMethod.GET)
    public ModelAndView searchByNicNo(@PathVariable("nicNo") String nicNo, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByNic(nicNo, bID);
        Boolean message = false;
        if (debtor.size() > 0) {
            message = true;
        }
        model.addAttribute("message", message);
        return new ModelAndView("debtor/viewDebtor", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchByName2/{name}", method = RequestMethod.GET)
    public ModelAndView searchByName(@PathVariable("name") String name, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByName(name, bID);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("debtor/viewDebtor", "debtorList", debtor);
    }

    @RequestMapping(value = "/findDebtorNicNo/{pattern}", method = RequestMethod.GET)
    @ResponseBody
    public List<String> findDebtorNicNo(@PathVariable("pattern") String pattern) {
        return debtorService.findDebtorNicNo(pattern);
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = "/saveNewGroupCustomer", method = RequestMethod.POST)
    public ModelAndView saveNewGroupCustomer(@ModelAttribute("loanForm") GroupCustomerForm groupCustomerForm, BindingResult result, Model model, HttpServletRequest request) {

        boolean saved = debtorService.saveGroupDebtor(groupCustomerForm);
        if (saved == true) {
            model.addAttribute("saveUpdate", "Customers added to selected group successfully!");
        } else {
            model.addAttribute("saveUpdate", "Adding customers failed");
        }

        // loading branched
        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);
        return new ModelAndView("debtor/newLoanGroupCustomerForm");
    }

    @RequestMapping(value = "/loanValidation/{loanTypeId}/{debtorID}", method = RequestMethod.GET)
    public @ResponseBody
    int loanValidation(@PathVariable("loanTypeId") int loanTypeId, @PathVariable("debtorID") int debtorID) {
        List<LoanHeaderDetails> lhdList = loanService.findActiveLoansByDebtorIDAndLoanTypeId(debtorID, loanTypeId);
        return lhdList.size();
    }

    @RequestMapping(value = "/debtorAdd/{loanBookNo}", method = RequestMethod.GET)
    public @ResponseBody
    DebtorHeaderDetails debtorAdd(@PathVariable("loanBookNo") String loanBookNo, HttpServletRequest request) {
        DebtorHeaderDetails debtorByMemberNo = null;
        Object bID = request.getSession().getAttribute("branchId");
        String memberNo = loanBookNo.replaceAll("-", "/");
        try {
            debtorByMemberNo = debtorService.findDebtorByMemberNo(memberNo, bID);
            if (debtorByMemberNo != null) {
                return debtorByMemberNo;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @RequestMapping(value = "/viewSavingsCustomers/{debtorId}", method = RequestMethod.GET)
    public ModelAndView viewSavingsCustomers(@PathVariable("debtorId") int debtorId, Model model) {

//        DebtorForm debtor = debtorService.findDebtorOtherDetails(debtorId);
//        model.addAttribute("id", debtorId);
//        model.addAttribute("dependent", debtor.getDependent());
//        model.addAttribute("business", debtor.getBusiness());
//        model.addAttribute("employee", debtor.getEmployement());
//        model.addAttribute("liability", debtor.getLiability());
//        model.addAttribute("bank", debtor.getBank());
//        model.addAttribute("vehicle", debtor.getVehicle());
//        model.addAttribute("land", debtor.getLand());

//        return new ModelAndView("debtor/editCustomer", "debtor", debtor);
        return new ModelAndView("debtor/editCustomer");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.*;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.reportManager.DBFacade;
import com.ites.bankfinance.service.*;
import com.ites.bankfinance.utils.DynamicColumnReportService;
import com.ites.bankfinance.utils.NumberToWords;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/ReportController")
public class ReportController {

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    UserService userService;

    @Autowired
    LoanService loanService;

    @Autowired
    ReportService reportService;

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private AdminService adminService;

    @Autowired
    SettlmentService settlmentService;

    @Autowired
    RecoveryService recService;

    @Autowired
    MasterDataService masterDataService;

    @RequestMapping(value = "/loadCashierReport", method = RequestMethod.GET)
    public ModelAndView insuLoadInsuranceMain(Model model) {
        List<MSubLoanType> subLoanList = loanService.findMSubLoanList();
        List<Chartofaccount> chartofaccounts = reportService.findChartofaccountList();

        model.addAttribute("subLoanList", subLoanList);
        model.addAttribute("paymentType", chartofaccounts);
        return new ModelAndView("reports/cashierReport");
    }

    @RequestMapping(value = "/loadLedgerReport", method = RequestMethod.GET)
    public ModelAndView loadLedgerReport(Model model) {
        List<MSubLoanType> subLoanList = loanService.findMSubLoanList();
        model.addAttribute("subLoanList", subLoanList);
        return new ModelAndView("reports/ledgerReport");
    }

    @RequestMapping(value = "/loadAllLoanReport", method = RequestMethod.GET)
    public ModelAndView loadAllLoanReport(Model model) {
        List<MSubLoanType> subLoanList = loanService.findMSubLoanList();
        model.addAttribute("branchList", adminService.findBranch());
        model.addAttribute("subLoanList", subLoanList);
        return new ModelAndView("reports/allLoanReport");
    }

    @RequestMapping(value = "/printCashierReport")
    public void generateCashierReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String date_1 = request.getParameter("date1");
        String date_2 = request.getParameter("date2");
        String repType = request.getParameter("report");
        String loantype = request.getParameter("loantype");
        String payments = request.getParameter("otherPayment");

        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);

            System.out.println(user_Name + "B Name" + branch_Name);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("date1", date_1);
            params.put("date2", date_2);
            params.put("userName", user_Name);
            params.put("branchName", branch_Name);
            params.put("branchId", branchID);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/cashierReport");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            if (repType.equals("N")) {
                params.put("subLoanId", Integer.parseInt(loantype));
                InputStream nreportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/cashierReport/cashier_report_cash.jasper");
                JasperRunManager.runReportToPdfStream(nreportstream, outStream, params, c);
            } else if (repType.equals("A")) {
                InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/cashierReport/cashier_report_cash_all.jasper");
                JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);
            } else if (repType.equals("P")) {
                params.put("accountNo", payments);
                InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/cashierReport/cashier_report_other_payments.jasper");
                JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);
            } else {
            }

            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @RequestMapping(value = "/printActiveCheques")
    public void printActiveCheques(HttpServletRequest request, HttpServletResponse response) {
//        System.out.println("printActiveCheques in controller"+request.getParameter("Loan_id"));

        response.setContentType("application/pdf");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            SettlementVoucher sv=null;
            sv=settlmentService.getSettlementVoucherDetailsbyLoanId(Integer.parseInt(request.getParameter("Loan_id")));
            SettlementPaymentDetailCheque chequePaymentDetails=settlmentService.getSettlementPaymentDetailChequeDetails(sv.getVoucherNo());

            DBFacade db = new DBFacade();
            Connection c = db.connect();
            String TxtCrName = "";
            String voucherNo = chequePaymentDetails.getTransactionNo();
            int cheqTo = 0;//nt
            int loanId = Integer.parseInt(request.getParameter("Loan_id"));
            int cheqType = 1;
            double amt = chequePaymentDetails.getAmount();
            SettlementPaymentDetailCheque spdc = loanService.findSettlementPaymentDetailChequeByVoucherNo(voucherNo, amt, loanId);
            if (cheqTo != 2) {
                if (cheqType == 1) {
                    TxtCrName = loanService.findCheqRecievedName(spdc.getChequeId(), cheqTo);
                } else {
                    TxtCrName = "Cash";
                }
            } else {
                TxtCrName = "Cash";
            }
            String TxtChqAmt = chequePaymentDetails.getAmount().toString().trim();
            String TxtChqAmtINWord = spdc.getAmountInTxt();
            String ChkDate = spdc.getRealizeDate().toString();

            boolean IsFullDate = false;
            boolean ChbIsACPay = false;
//            if (request.getParameter("IsFullDate") != null && request.getParameter("IsFullDate").toString().trim().equalsIgnoreCase("on")) {
//                IsFullDate = true;
//            }
            if (spdc.getChbIsACPay() != 0) {
                ChbIsACPay = true;
            }
            String DateArr[] = ChkDate.split("-");

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            URL url = this.getClass().getClassLoader().getResource("../reports/ChequeReport/");
            String path = url.toURI().toString();

            params.put("REPORT_CONNECTION", c);
            params.put("SUBREPORT_DIR", path);
            params.put("aD1", DateArr[2].substring(0, 1));
            params.put("aD2", DateArr[2].substring(1));
            params.put("bD1", DateArr[1].substring(0, 1));
            params.put("bD2", DateArr[1].substring(1));
            if (IsFullDate) {
                params.put("cD1", DateArr[0].substring(0, 1));
                params.put("cD2", DateArr[0].substring(1, 2));
            } else {
                params.put("cD1", "");
                params.put("cD2", "");
            }
            params.put("cD3", DateArr[0].substring(2, 3));
            params.put("cD4", DateArr[0].substring(3));
            if (ChbIsACPay) {
                params.put("AcPay", "A/C PAYEE ONLY");
            } else {
                params.put("AcPay", "");
            }
            double amount = Double.parseDouble(TxtChqAmt);
            DecimalFormat formatter = new DecimalFormat("#,###.00");

            String chV = formatter.format(amount);
            params.put("CrName", TxtCrName);
            params.put("ChqAmt", chV);
            params.put("ChqAmtINWord", TxtChqAmtINWord);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/ChequeReport/classic.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    //Document view
    @RequestMapping(value = "/generateDocumentReport")
    public void generateDocumentReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String docId = request.getParameter("docId");
        String docName = request.getParameter("docName");

        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            params.put("doId", docId);
            params.put("doName", docName);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/documentsView");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream nreportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/documentsView/documentView.jasper");
            JasperRunManager.runReportToPdfStream(nreportstream, outStream, params, c);

            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    ////////////////////////Remove Loan Report////////////////////////
    @RequestMapping(value = "/loadRemoveLoanList", method = RequestMethod.GET)
    public ModelAndView loadloadRemoveLoanList(Model model) {
        List<MSubLoanType> subLoanList = loanService.findMSubLoanList();
        model.addAttribute("subLoanList", subLoanList);
        return new ModelAndView("reports/removeLoansReport");
    }

    @RequestMapping(value = "/printRemoveLoansReport")
    public void generateRemoveLoanReports(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String repType = request.getParameter("report");
        String loantype = request.getParameter("loantype");

        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            params.put("userName", user_Name);
            params.put("branchName", branch_Name);
            params.put("branchId", branchID);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/removeLoanReport");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            if (repType.equals("N")) {
                params.put("subLoanId", Integer.parseInt(loantype));
                InputStream nreportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/removeLoanReport/removeLoans.jasper");
                JasperRunManager.runReportToPdfStream(nreportstream, outStream, params, c);
            } else if (repType.equals("A")) {
                InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/removeLoanReport/removeLoansAll.jasper");
                JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);
            }

            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/printLedgerReport")
    public void generateLedgerReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String date_1 = request.getParameter("date1");
        String date_2 = request.getParameter("date2");
        String repType = request.getParameter("report");
        String loantype = request.getParameter("loantype");

        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();
            String user_Name = userService.findWorkingUserName();
            int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(loginBranchID);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            params.put("date1", date_1);
            params.put("date2", date_2);
            params.put("userName", user_Name);
            params.put("branchId", loginBranchID);
            params.put("branchName", branch_Name);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/ledgerReport");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            if (repType.equals("N")) {
                params.put("subLoanId", Integer.parseInt(loantype));
                InputStream nreportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/ledgerReport/ledger_report_normal.jasper");
                JasperRunManager.runReportToPdfStream(nreportstream, outStream, params, c);
            } else if (repType.equals("A")) {
                InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/ledgerReport/ledger_report_all.jasper");
                JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);
            }
            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/printVoucherReport")
    public void generateVoucherReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String date_1 = request.getParameter("datev1");
        String date_2 = request.getParameter("datev2");

        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();
            String user_Name = userService.findWorkingUserName();
            int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(loginBranchID);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            params.put("fromDate", date_1);
            params.put("toDate", date_2);
            params.put("userName", user_Name);
            params.put("branchId", loginBranchID);
            params.put("branchName", branch_Name);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/ledgerReport");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/ledgerReport/viewVoucher.jasper");
            JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);

            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/printAllLoanByBranch")
    public void generateAllLoanByBranch(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        int branchId = 0;
        if (request.getParameter("branchId") != null) {
            branchId = Integer.parseInt(request.getParameter("branchId"));
        }
        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();
            String user_Name = userService.findWorkingUserName();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            String branch_Name = userService.findWorkingBranchName(branchId);

            params.put("branchName", branch_Name);
            params.put("branchId", branchId);
            params.put("userName", user_Name);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/allLoans");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/allLoans/customerLoan.jasper");
            JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);
            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Msd
    @RequestMapping(value = "/loadBasicLoanDetails", method = RequestMethod.GET)
    public ModelAndView viewBasicLoanDetailsForm(Model model) {

        return new ModelAndView("reports/basicLoanDetails");
    }

    @RequestMapping(value = "/PrintBasicLoanDetailsReport")
    public void genarateBasicLoanDEtailsReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String agreeNo = request.getParameter("agreementNo");
        int loanID = 0;
        String branch_Name;
        int periodOfUser;
        LoanHeaderDetails loanHeaderDetails = null;
        List<SettlementCharges> chargeses = null;
        try {

            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            loanHeaderDetails = reportService.findByLoanDetails(agreeNo);
            branch_Name = userService.findWorkingBranchName(branchID);
            loanID = loanHeaderDetails.getLoanId();
            chargeses = reportService.findBySettlementChargees(loanID);
            periodOfUser = chargeses.size();

            Double total_Payment = reportService.findTotalPayments(loanID);

            Double finalBalance = reportService.findLoanBalanace(loanID);

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("userName", user_Name);
            params.put("branchName", branch_Name);
            params.put("branchId", branchID);
            params.put("useMonth", periodOfUser);
            params.put("loanId", loanID);
            params.put("finalBalance", finalBalance);
            params.put("totalPayments", total_Payment);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/loanDetails");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanDetails/loan_details.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/loadInsuranceList", method = RequestMethod.GET)
    public ModelAndView viewInsuranceList(Model model) {
        List<InsuranceCompany> companys = null;
        List<MBranch> branchs = null;
        try {
            companys = insuranceService.findInsuranceCompanyes();
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("companyList", companys);
        model.addAttribute("branchList", branchs);
        return new ModelAndView("reports/insuranceList");
    }

    @RequestMapping(value = "/PrintInsuranceListReport")
    public void genaratePrintInsuranceListReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        try {
            String user_Name = userService.findWorkingUserName();
            int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(loginBranchID);

            String date_1 = request.getParameter("date1");
            String date_2 = request.getParameter("date2");
            Integer branch_Id = Integer.parseInt(request.getParameter("branchId"));
            Integer insuComId = Integer.parseInt(request.getParameter("insuCompanyId"));
            String insuComName = reportService.findByInsuCompanyName(insuComId);

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("userName", user_Name);
            params.put("branchName", branch_Name);
            params.put("loginBranchId", loginBranchID);

            params.put("date1", date_1);
            params.put("date2", date_2);
            params.put("branchId", branch_Id);
            params.put("insuCompanyName", insuComName);
            params.put("insuComId", insuComId);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/insuranceReport");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/insuranceReport/insuranceList.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/loadBalanceSheet", method = RequestMethod.GET)
    public ModelAndView viewloadBalanceSheet(Model model, HttpServletRequest request) {
        try {
            int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
            Date systemDate = settlmentService.getSystemDate(loginBranchID);
            model.addAttribute("systemDate", systemDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("reports/balanceSheet");
    }

    @RequestMapping(value = "/PrintDayEndBalanceSheet", method = RequestMethod.GET)
    public void genaratePrintDayEndBalanceSheet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String sdate = request.getParameter("sysDate");
        try {

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            String user_Name = userService.findWorkingUserName();
            int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(loginBranchID);

            Date systemDate = settlmentService.getSystemDate(loginBranchID);
//            sdate = sdf.format(systemDate);
            java.sql.Date date_a = null;
            date_a = new java.sql.Date(sdf.parse(sdate).getTime());
            double[] totalAmounts = settlmentService.getTotalBalancesForDayEnd(date_a, loginBranchID);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("userName", user_Name);
            params.put("branchName", branch_Name);
            params.put("branchId", loginBranchID);
            params.put("systemDate", sdate);

            params.put("payments", totalAmounts[0]);
            params.put("otherPayments", totalAmounts[1]);
            params.put("issuePayments", totalAmounts[2]);
            params.put("chqPayments", totalAmounts[3]);
            params.put("cashPayments", totalAmounts[7]);
            params.put("opening", totalAmounts[4]);
            params.put("openingCash", totalAmounts[5]);
            params.put("openingCheque", totalAmounts[6]);
            params.put("dirctFundCash", totalAmounts[8]);
            params.put("dirctFundCheq", totalAmounts[9]);
            params.put("totBankMemo", totalAmounts[10]);
            params.put("mdFundCash", totalAmounts[11]);
            params.put("mdFundCheq", totalAmounts[12]);

            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/balanceSheet");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/balanceSheet/balanceSheet.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/PrintCustomerLedgerReport")
    public void generateCustomerLedgerReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String sdate = request.getParameter("sDate");
        String edate = request.getParameter("eDate");
        String agNo = request.getParameter("agNoNew");
        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            String user_Name = userService.findWorkingUserName();
            int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(loginBranchID);

            int loanId = loanService.findLoanIdByAgreementNo(agNo);
            int loanPeriod = 0;
            LoanHeaderDetails loanHeaderDetails = loanService.searchLoanByLoanId(loanId);
            if (loanHeaderDetails != null) {
                Integer loanPeriodType = loanHeaderDetails.getLoanPeriodType();
                if (loanPeriodType == 1) {
                    loanPeriod = loanHeaderDetails.getLoanPeriod();
                } else if (loanPeriodType == 2) {
                    loanPeriod = loanHeaderDetails.getLoanPeriod() * 12;
                } else if (loanPeriodType == 3) {
                    loanPeriod = loanHeaderDetails.getLoanPeriod() / 30;
                }
            }
            Object[] instl = recService.findRemainigInstallment(loanId);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("userName", user_Name);
            params.put("branchName", branch_Name);
            params.put("branchId", loginBranchID);
            params.put("startDate", sdate);
            params.put("endDate", edate);
            params.put("loanId", loanId);
            params.put("noOfIns", instl[0].toString());
            params.put("loanPeriod", loanPeriod);
            if (instl[1] == null) {
                params.put("dueDate", "");
            } else {
                params.put("dueDate", instl[1].toString());
            }

            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/customerLeger");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("../reports/customerLeger/customer_wice_LoanDetails.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/PrintRescheduleLoansReport")
    public void generateRescheduleLoansReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        try {
            DBFacade db = new DBFacade();
            Connection con = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
//            String branch_Name = userService.findWorkingBranchName(branchId);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();

            params.put("userName", user_Name);

            params.put("REPORT_CONNECTION", con);

//             URL url = this.getClass().getClassLoader().getResource("../reports/rescheduleLoans");
//            String path = url.toURI().toString();//getPath() + "/";
//            params.put("SUBREPORT_DIR", path);
            InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/rescheduleLoans/rescheduleLoansReport.jasper");
            JasperRunManager.runReportToPdfStream(areportstream, outStream, params, con);
            con.close();
            con = null;
            params = null;
            outStream.flush();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/viewRecoverReport", method = RequestMethod.POST)
    public void viewRecoverReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        int branchId = 0;
        if (request.getParameter("branchId") != null) {
            branchId = Integer.parseInt(request.getParameter("branchId"));
        }
        String aa = request.getParameter("loan_id_r");
        int loanId = Integer.parseInt(aa);

        try {
            DBFacade db = new DBFacade();
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(branchId);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();

            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("loan_id", loanId);

            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/reoveryReport");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/reoveryReport/Recovery_Report.jasper");
            JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);
            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }

    }

    //view Visit Report
    @RequestMapping(value = "/viewVisitReport", method = RequestMethod.POST)
    public void viewVisitReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        int branchId = 0;
        if (request.getParameter("branchId") != null) {
            branchId = Integer.parseInt(request.getParameter("branchId"));
        }
        String aa = request.getParameter("visit_Id");
        int recoveryId = Integer.parseInt(aa);

        try {
            DBFacade db = new DBFacade();
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(branchId);

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();

            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("loan_id", recoveryId);

            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/reoveryReport");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream areportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/reoveryReport/Recovery_Report.jasper");
            JasperRunManager.runReportToPdfStream(areportstream, outStream, params, c);
            c.close();
            c = null;
            params = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/searchByNameReport/{name}", method = RequestMethod.GET)
    public ModelAndView searchByName(@PathVariable("name") String name, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByName(name, bID);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("reports/viewSearchDebtor", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchByNicReport/{nicNo}", method = RequestMethod.GET)
    public ModelAndView searchByNicNo(@PathVariable("nicNo") String nicNo, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByNic(nicNo, bID);
        Boolean message = false;
        if (debtor.size() > 0) {
            message = true;
        }
        model.addAttribute("message", message);
        return new ModelAndView("reports/viewSearchDebtor", "debtorList", debtor);
    }

    @RequestMapping(value = "/PrintCustomerTotalLedgerReport")
    public void generateCustomerTotalLedgerReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fromDate = request.getParameter("cusFromDate");
        String toDate = request.getParameter("cusToDate");
        int debtorId = Integer.parseInt(request.getParameter("cusId"));
        try {

            DBFacade db = new DBFacade();
            Connection c = DBFacade.connect();

            String user_Name = userService.findWorkingUserName();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("userName", user_Name);
            params.put("fromDate", fromDate);
            params.put("toDate", toDate);
            params.put("debtorId", debtorId);

            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/debtorAccount");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/debtorAccount/debtorAcc.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/PrintMonthlyBusiness")
    public void PrintMonthlyBusiness(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fromDate = request.getParameter("mfDate");
        String toDate = request.getParameter("mtDate");
//        int debtorId = Integer.parseInt(request.getParameter("cusId"));
        try {

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            String user_Name = userService.findWorkingUserName();
            int branchId = 0;
            if (request.getParameter("branchId") != null) {
                branchId = Integer.parseInt(request.getParameter("branchId"));
            }
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
//            params.put("userName", user_Name);
            params.put("date1", fromDate);
            params.put("date2", toDate);
            params.put("branchId", branchId);
//            params.put("debtorId", debtorId);

            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/mb");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/mb/monthly_business.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | NumberFormatException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method print an arrears report(PDF) from a list get from db and
     * store list with xml without creating tempory tables. Used JAXB (Java
     * Architecture for XML Binding)
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/printArriesReport")
    public void getArries(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String sDate = request.getParameter("date1");
        String eDate = request.getParameter("date2");
        int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
        File dir = new File("C:\\tmpFile\\arriesLoans");
        if (!dir.exists()) {    // check directory exists or not
            dir.mkdir();
        }
        String xmlFile = "C:\\tmpFile\\arriesLoans\\arriesLoans.xml";
        String expression = "/ArriesLoans/ArriesLoan";
        Map<String, Object> params = new HashMap<>();
        try {
            List<ArriesLoan> arriesLoans = recService.getLoanArries(sDate, eDate); // get list from db
            if (arriesLoans.size() > 0) {
                ArriesLoans al = new ArriesLoans();
                al.setArriesLoans(arriesLoans);
                JAXBContext jAXBContext = JAXBContext.newInstance(ArriesLoans.class); // store list in xml
                Marshaller marshaller = jAXBContext.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.marshal(al, new File(xmlFile));
                Connection c = DBFacade.connect(); // get connection for sub report
                String user_Name = userService.findWorkingUserName();
                String branch_Name = userService.findWorkingBranchName(loginBranchID);
                params.put("sDate", sDate); // set parameters
                params.put("eDate", eDate);
                params.put("userName", user_Name);
                params.put("branchName", branch_Name);
                params.put("REPORT_CONNECTION", c);
                URL url = this.getClass().getClassLoader().getResource("../reports/arriesLoan");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);
                InputStream reportFile = this.getClass().getClassLoader().getResourceAsStream("..//reports/arriesLoan/arriesReport.jasper");
                ServletOutputStream outStream = response.getOutputStream();
                JRXmlDataSource xmlDataSource = new JRXmlDataSource(xmlFile, expression);
                JasperRunManager.runReportToPdfStream(reportFile, outStream, params, xmlDataSource); // print report as PDF
            }
        } catch (IOException | URISyntaxException | JAXBException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/loadPortfolioPage", method = RequestMethod.GET)
    public ModelAndView loadPortfolioPage(HttpServletRequest request, Model model) {
        List<PortFolioFields> folioFieldses = new ArrayList<>();

        Field[] fields = PortfolioDetails.class.getDeclaredFields();
        String agreeNo = request.getParameter("agreNo");
        String branchId = request.getParameter("branchId");
        String status = request.getParameter("status");
        String officerId = request.getParameter("officerId");
        String dueDay = request.getParameter("dueDay");
        String initiateFromDate = request.getParameter("initiateFromDate");
        String initiateToDate = request.getParameter("initiateToDate");
        String loanAmountGranted = request.getParameter("loanAmountGranted");
        String lagValue = request.getParameter("lagValue");
        String grossRental = request.getParameter("grossRental");
        String grValue = request.getParameter("grValue");
        String loanType = request.getParameter("loanType");

        for (int i = 0; i < fields.length; i++) {
            PortFolioFields pff = new PortFolioFields();
            pff.setIndex(i);
            pff.setName(fields[i].getName());
            folioFieldses.add(pff);
            pff = null;
        }

        model.addAttribute("fields", folioFieldses);
        model.addAttribute("agreementNo", agreeNo);
        model.addAttribute("branchId", branchId);
        model.addAttribute("status", status);
        model.addAttribute("officerId", officerId);
        model.addAttribute("dueDay", dueDay);
        model.addAttribute("initiateFromDate", initiateFromDate);
        model.addAttribute("initiateToDate", initiateToDate);
        model.addAttribute("loanAmountGranted", loanAmountGranted);
        model.addAttribute("lagValue", lagValue);
        model.addAttribute("grossRental", grossRental);
        model.addAttribute("grValue", grValue);
        model.addAttribute("loanType", loanType);
        return new ModelAndView("reports/portFolioPage");
    }

    @RequestMapping(value = "/genaratePortfolioReport")
    public void genaratePortfolioReport(HttpServletRequest request, HttpServletResponse response, @RequestParam("fields") String[] fields) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename = Portfolio_Report.xls");
        String agerementNo = request.getParameter("agreementNo");
        String br = request.getParameter("branchId");
        String status = request.getParameter("status");
        int officerId = Integer.parseInt(request.getParameter("officerId"));
        int dueDay = Integer.parseInt(request.getParameter("dueDay"));
        String initiateFromDate = request.getParameter("initiateFromDate");
        String initiateToDate = request.getParameter("initiateToDate");
        String loanAmount = request.getParameter("loanAmountGranted");
        String grossRental = request.getParameter("grossRental");
        String grValue = request.getParameter("grValue");
        String lagValue = request.getParameter("lagValue");
        int loanType = Integer.parseInt(request.getParameter("loanType"));

        int branchId = 0;
        if (br != null) {
            branchId = Integer.parseInt(br);
        }
        int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
        try {
            ServletOutputStream outStream = response.getOutputStream();
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(loginBranchID);
            File dir = new File("C:\\tmpFile\\portfolioReports");
            if (!dir.exists()) {    // check directory exists or not
                dir.mkdir();
            }
//            File xmlFile = new File("C://tmpFile//portfolioReports//portfolioReport.xml");
            File xmlFile = new File("C:\\tmpFile\\portfolioReports\\portfolioReport.xml");

            PortfolioDetail portfolioDetail = null;
            if (status.equals("1")) {
                portfolioDetail = reportService.findPortfolioDetail(agerementNo, user_Name, branch_Name);
            } else if (status.equals("2")) {
                portfolioDetail = reportService.findPortfolioDetail(branchId);
            } else if (status.equals("3")) {
                portfolioDetail = reportService.findPortfolioDetailByOfficerId(officerId);
            } else if (status.equals("4")) {
                portfolioDetail = reportService.findPortfolioDetailByDueDay(dueDay);
            } else if (status.equals("5")) {
                portfolioDetail = reportService.findPortfolioDetailByInitiateDate(initiateFromDate, initiateToDate);
            } else if (status.equals("6")) {
                portfolioDetail = reportService.findPortfolioDetailByLoanAmount(loanAmount, lagValue);
            } else if (status.equals("7")) {
                portfolioDetail = reportService.findPortfolioDetailByGrossRental(grossRental, grValue);
            } else if (status.equals("8")) {
                portfolioDetail = reportService.findPortfolioDetailByLoanType(loanType);
            }
            if (portfolioDetail != null) {
                JAXBContext jAXBContext = JAXBContext.newInstance(PortfolioDetail.class); // store list in xml
                Marshaller marshaller = jAXBContext.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                if (!xmlFile.exists()) {
                    marshaller.marshal(portfolioDetail, xmlFile);
                } else {
                    xmlFile.delete();
                    marshaller.marshal(portfolioDetail, xmlFile);
                }
                DynamicColumnReportService service = new DynamicColumnReportService(); // start dynamic column report service
                JasperPrint print = service.runReport(Arrays.asList(fields), xmlFile);
                JRXlsExporter exporter = new JRXlsExporter();
                if (print != null) {
                    print.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                    exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                    exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                    exporter.exportReport();
                }
                if (outStream != null) {
                    outStream.flush();
                    outStream.close();
                }
            }
//            if (portfolioDetail != null) {
//                JasperPrint print = JasperManager.fillReport(reportstream2, params, c);
//                ByteArrayOutputStream output = new ByteArrayOutputStream();
//
//                OutputStream outputfile = new FileOutputStream(new File(System.getProperty("user.dir") + File.separatorChar + "/stock.xls"));
//
//                JRXlsExporter exporterXLS = new JRXlsExporter();
//                exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
//                exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, output);
//                exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
//                exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
//                exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
//                exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
//                exporterXLS.exportReport();
//                outputfile.write(output.toByteArray());
//
//                response.setContentType("application/vnd.ms-excel");
//                ServletOutputStream outputStream = response.getOutputStream();
//                outputStream.write(output.toByteArray());
//                outputStream.flush();
//                outputStream.close();
//            }
        } catch (IOException | JAXBException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/loadPrintChequeRecvNote", method = RequestMethod.GET)
    public ModelAndView loadPrintChequeRecvNote(Model model) {
        return new ModelAndView("reports/chequeRecvNote");
    }

    @RequestMapping(value = "/PrintChequeRecvNote")
    public void PrintChequeRecvNote(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String aggreementNo = request.getParameter("aggNo");
        int branchId = (Integer) request.getSession().getAttribute("branchId");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(branchId);
            int loanId = loanService.findLoanIdByAgreementNo(aggreementNo);
            LoanHeaderDetails loanHeaderDetails = loanService.searchLoanByLoanId(loanId);
            ServletOutputStream outStream = response.getOutputStream();
            if (loanHeaderDetails != null) {
                Map<String, Object> params = new HashMap<>();
                params.put("branchName", branch_Name);
                params.put("userName", user_Name);
                params.put("loanId", loanHeaderDetails.getLoanId());
                params.put("aggNo", loanHeaderDetails.getLoanAgreementNo());
                params.put("custName", loanHeaderDetails.getDebtorHeaderDetails().getDebtorName());
                Double totAmount = settlmentService.getLoanChequeTotAmount(loanHeaderDetails.getLoanId());
                String totAmountInWords = NumberToWords.convert(totAmount.longValue());
                params.put("sumInText", totAmountInWords.toUpperCase());
                MSubLoanType loanType = adminService.findMSubLoanType(loanHeaderDetails.getLoanType());
                params.put("loanType", loanType.getSubLoanName());
                params.put("REPORT_CONNECTION", c);
                URL url = this.getClass().getClassLoader().getResource("../reports/loanCheques");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);
                InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanCheques/LoanCheques.jasper");
                JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
                c.close();
                c = null;
                outStream.flush();
                outStream.close();
            }
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/PrintPdChequeList")
    public void PrintPDChequeList(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String date_1 = request.getParameter("date1");
        String date_2 = request.getParameter("date2");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);

            ServletOutputStream outStream = response.getOutputStream();

            Map<String, Object> params = new HashMap<>();
            params.put("date1", date_1);
            params.put("date2", date_2);
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("branchId", branchID);

            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/PdChequeList");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/PdChequeList/PD_ChequeList.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();

        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/loadCustomerDetail", method = RequestMethod.GET)
    public ModelAndView loadCustomerDetail(Model model, HttpServletRequest request) {
        try {
            List<MSubLoanType> subLoanTypes = adminService.findSubLoanType();
            List<MBranch> branches = adminService.findBranch();
            List<MSubLoanType> subLoanTypeList = adminService.findSubLoanType();
            int type = 0;
            int loanType = 4;
            List nameList = masterDataService.findUsersByApproval(type, loanType);
            model.addAttribute("subLoanTypes", subLoanTypes);
            model.addAttribute("branches", branches);
            model.addAttribute("nameList", nameList);
            model.addAttribute("subLoanTypeList", subLoanTypeList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("reports/customerDetail");
    }

    @RequestMapping(value = "/printCustomerBaseByLoanTypeReport")
    public void printCustomerBaseByLoanTypeReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String loanTypeId = request.getParameter("loanTypeId");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);
            MSubLoanType subLoanType = adminService.findMSubLoanType(Integer.parseInt(loanTypeId));
            ServletOutputStream outStream = response.getOutputStream();

            Map<String, Object> params = new HashMap<>();
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("loanTypeId", Integer.parseInt(loanTypeId));
            if (subLoanType != null) {
                params.put("loanType", subLoanType.getSubLoanName());
            }

            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/customer");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/customer/customer_base_by_loan_wise.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();

        } catch (IOException | NumberFormatException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printRebateandFullyPaidList")
    public void printRebateandFullyPaidList(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);
            ServletOutputStream outStream = response.getOutputStream();

            Map<String, Object> params = new HashMap<>();
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);

            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/customer");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/customer/loan_fully_paid.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();

        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printSMSReport")
    public void printSMSReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        Date fdate = null;
        Date tdate = null;
        try {
            fdate = new SimpleDateFormat("yyyy-MM-dd").parse(fromDate);
            tdate = new SimpleDateFormat("yyyy-MM-dd").parse(toDate);
        } catch (ParseException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);
            ServletOutputStream outStream = response.getOutputStream();

            Map<String, Object> params = new HashMap<>();
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("fromDate", fdate);
            params.put("toDate", tdate);

            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/customer");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/customer/sms_details.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();

        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printAcceptanceReceipt")
    public void printAcceptanceReceipt(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String loanId = request.getParameter("loanId");
        String aggreementNo = "", loanStartDate = "", description = "", sellerAddress = "", debtorAddress = "", termsCount = "";
        try {
            LoanHeaderDetails loanHeaderDetails = loanService.searchLoanByLoanId(Integer.parseInt(loanId));
            if (loanHeaderDetails != null) {
                aggreementNo = loanHeaderDetails.getLoanAgreementNo();
                loanStartDate = dateFormat.format(loanHeaderDetails.getLoanStartDate());
                DebtorHeaderDetails debtorHeaderDetails = loanHeaderDetails.getDebtorHeaderDetails();
                if (debtorHeaderDetails != null) {
                    debtorAddress = debtorHeaderDetails.getNameWithInitial() + "\n" + debtorHeaderDetails.getDebtorPersonalAddress();
                }
                LoanPropertyVehicleDetails vehicleDetails = loanService.findVehicleByLoanId(loanHeaderDetails.getLoanId());
                if (vehicleDetails != null) {
                    description = vehicleDetails.getVehiclChassisNo() + "\n" + vehicleDetails.getVehicleEngineNo() + "\n";
                    VehicleMake vehicleMake = masterDataService.findVehicleMakeById(Integer.parseInt(vehicleDetails.getVehicleMake()));
                    if (vehicleMake != null) {
                        description = description + vehicleMake.getVehicleMakeName() + " / ";
                    }
                    VehicleModel vehicleModel = masterDataService.findVehicleModelById(Integer.parseInt(vehicleDetails.getVehicleModel()));
                    if (vehicleModel != null) {
                        description = description + vehicleModel.getVehicleModelName() + " ";
                    }
                    VehicleType vehicleType = masterDataService.findVehicleTypeById(Integer.parseInt(vehicleDetails.getVehicleType()));
                    if (vehicleType != null) {
                        description = description + vehicleType.getVehicleTypeName();
                    }
                    MSupplier supplier = adminService.findSupplier(vehicleDetails.getDealerId());
                    if (supplier != null) {
                        sellerAddress = supplier.getSupplierName() + "\n" + supplier.getSupplierAddress();
                    }
                }
                Integer installments = loanService.findLoanInstallmentCount(loanHeaderDetails.getLoanId());
                String numberToWords = NumberToWords.convert(installments.longValue());
                termsCount = numberToWords + "(" + installments + ")";
            }
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("aggreementNo", aggreementNo);
            params.put("startDate", loanStartDate);
            params.put("decription", description);
            params.put("seller", sellerAddress);
            params.put("debtor", debtorAddress);
            params.put("terms", termsCount);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/receipt/acceptance_receipt.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(retInputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
            outStream.flush();
            outStream.close();
        } catch (IOException | NumberFormatException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/generateLoanVoucher")
    public void printLoanVoucher(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        double totalAmount = Double.parseDouble(request.getParameter("totalAmount"));
        String capitalizeAmount = request.getParameter("capitalizeAmount");
        String downPayment = request.getParameter("downPayment");
        String OtherCharge = request.getParameter("OtherCharge");
        String chekNo = request.getParameter("chekNo");
        int bankAccount = Integer.parseInt(request.getParameter("bankAccount"));
        String cheqToName = request.getParameter("cheqToName");
        String cheqType = request.getParameter("cheqType");
        String chkDate = request.getParameter("expireDate");
        int chbIsACPay = Integer.parseInt(request.getParameter("ChbIsACPay"));
//        double payingAmount = Double.parseDouble(request.getParameter("payingAmount"));
        double payingAmount = 0.00;
//        amountInTxt

        String voucherNo = loanService.createMyNewVoucher(loanId, totalAmount, chekNo, bankAccount, chkDate, payingAmount, cheqToName, chbIsACPay);

        int voucherId = loanService.findVoucherIdByVoucherNo(voucherNo);

        response.setContentType("application/pdf");
        int paymentType = 1;
        System.out.println("Controller");
        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("voucher_no", voucherId);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/voucher/");
//            String path = url.getPath() + "/";
            String path = url.toURI().toString();
            System.out.println("path**" + path);
            params.put("SUBREPORT_DIR", path);

//            if (paymentType == 1) {
            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/voucher/cheque_voucher.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/printLoanPostingReport")
    public void printLoanPostingReport(HttpServletRequest request, HttpServletResponse response) {
        String aggreementNo = request.getParameter("aggNo");
        String type = request.getParameter("type");
        int branchId = (Integer) request.getSession().getAttribute("branchId");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(branchId);
            int loanId = loanService.findLoanIdByAgreementNo(aggreementNo);
            LoanPropertyVehicleDetails vehicleDetails = loanService.findVehicleByLoanId(loanId);
            List<LoanPosting> loanPostingList = settlmentService.findLoanPostings(loanId);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("agreementNo", aggreementNo);
            params.put("vRegNo", vehicleDetails != null ? vehicleDetails.getVehicleRegNo() : "N/A");
            params.put("vChasNo", vehicleDetails != null ? vehicleDetails.getVehiclChassisNo() : "N/A");
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/loanPosting");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanPosting/loan_posting.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(retInputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(loanPostingList));

            switch (type) {
                case "A": {
                    response.setContentType("application/pdf");
                    //response.setHeader("Content-disposition", "attachment; filename = GL_Entries.pdf");
                    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
                    break;
                }
                case "B": {
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-disposition", "attachment; filename = GL_Entries.xls");
                    JRXlsExporter xlsExporter = new JRXlsExporter();
                    jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                    xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                    xlsExporter.exportReport();
                    break;
                }
            }
            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printLoanTransactionsReport")
    public void printLoanTransactionsReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String aggreementNo = request.getParameter("aggNo");
        int branchId = (Integer) request.getSession().getAttribute("branchId");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(branchId);
            int loanId = loanService.findLoanIdByAgreementNo(aggreementNo);
            LoanPropertyVehicleDetails vehicleDetails = loanService.findVehicleByLoanId(loanId);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("aggNo", aggreementNo);
            params.put("loanId", loanId);
            if (vehicleDetails != null) {
                params.put("vehNo", vehicleDetails.getVehicleRegNo() != null ? vehicleDetails.getVehicleRegNo() : "N/A");
            }
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/loanTransactions");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanTransactions/loan_transactions.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printLoanChequesToDateReport")
    public void printLoanChequesToDateReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        int branchId = (Integer) request.getSession().getAttribute("branchId");
        try {
            Connection c = DBFacade.connect();
            Date system_Date = settlmentService.getSystemDate(branchId);
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(branchId);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("systemDate", dateFormat.format(system_Date));
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/loanCheques");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanCheques/LoanChequesToDate.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/PrintInsuranceSummaryReport")
    public void PrintInsuranceSummaryReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename = Insurance_Summary.xls");
        String sDate = request.getParameter("sDate");
        String eDate = request.getParameter("eDate");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("sDate", sDate);
            params.put("eDate", eDate);
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/insuranceReport");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/insuranceReport/insuranceSummary.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(retInputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, c);
            if (jasperPrint != null) {
                JRXlsExporter xlsExporter = new JRXlsExporter();
                jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                xlsExporter.exportReport();
            }

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printInsuranceVoucher")
    public void printInsuranceVoucher(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String param = request.getParameter("hiddenArray");
        if (param != null) {
            String[] split = param.split(",");
            int[] loanIds = new int[split.length];
            for (int i = 0; i < split.length; i++) {
                int loanId = Integer.parseInt(split[i]);
                loanIds[i] = loanId;
            }
            if (loanIds.length > 0) {
                int branchId = (Integer) request.getSession().getAttribute("branchId");
                try {
                    Connection c = DBFacade.connect();
                    String user_Name = userService.findWorkingUserName();
                    String branch_Name = userService.findWorkingBranchName(branchId);
                    ServletOutputStream outStream = response.getOutputStream();
                    Map<String, Object> params = new HashMap<>();
                    params.put("branchName", branch_Name);
                    params.put("userName", user_Name);
                    params.put("loanIds", loanIds);
                    params.put("REPORT_CONNECTION", c);
                    URL url = this.getClass().getClassLoader().getResource("../reports/insuranceReport");
                    String path = url.toURI().toString();
                    params.put("SUBREPORT_DIR", path);
                    InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/insuranceReport/insuranceVoucher.jasper");
                    JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
                    c.close();
                    c = null;
                    outStream.flush();
                    outStream.close();
                } catch (IOException | URISyntaxException | JRException | SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequestMapping(value = "/PrintInsuranceCommissionReport")
    public void PrintInsuranceCommissionReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename = Insurance_Commission.xls");
        String sDate = request.getParameter("sDate");
        String eDate = request.getParameter("eDate");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("sDate", sDate);
            params.put("eDate", eDate);
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/insuranceReport");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/insuranceReport/insuranceCommission.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(retInputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, c);
            if (jasperPrint != null) {
                JRXlsExporter xlsExporter = new JRXlsExporter();
                jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                xlsExporter.exportReport();
            }

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/PrintInsuranceRenewalReport")
    public void PrintInsuranceRenewalReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename = Insurance_Renewal.xls");
        String sDate = request.getParameter("sDate");
        String eDate = request.getParameter("eDate");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("sDate", sDate);
            params.put("eDate", eDate);
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/insuranceReport");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/insuranceReport/insuranceRenew.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(retInputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, c);
            if (jasperPrint != null) {
                JRXlsExporter xlsExporter = new JRXlsExporter();
                jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                xlsExporter.exportReport();
            }

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/PrintInsuranceOutstandingReport")
    public void PrintInsuranceOutstandingReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename = Insurance_Outstanding.xls");
        String sDate = request.getParameter("sDate");
        String eDate = request.getParameter("eDate");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            String branch_Name = userService.findWorkingBranchName(branchID);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("sDate", sDate);
            params.put("eDate", eDate);
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/insuranceReport");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/insuranceReport/insuranceOutstading.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(retInputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, c);
            if (jasperPrint != null) {
                JRXlsExporter xlsExporter = new JRXlsExporter();
                jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                xlsExporter.exportReport();
            }

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | SQLException | JRException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printRebateQuotation")
    public void printRebateQuotation(HttpServletRequest request, HttpServletResponse response) {
        int loanId = 0;
        String s = request.getParameter("loanId");
        if (s != null) {
            loanId = Integer.parseInt(s);
        }
        if (loanId > 0) {
            response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            response.setHeader("Content-disposition", "attachment; filename = Rebate_Quotation.docx");
            try {
                Connection c = DBFacade.connect();
                String user_Name = userService.findWorkingUserName();
                int branchID = (Integer) request.getSession().getAttribute("branchId");
                String branch_Name = userService.findWorkingBranchName(branchID);
                RebateQuotation rebateQuotation = settlmentService.getRebateQuotation(loanId);
                ServletOutputStream outStream = response.getOutputStream();
                Map<String, Object> params = new HashMap<>();
                params.put("branchName", branch_Name);
                params.put("userName", user_Name);
                params.put("loanId", loanId);
                if (rebateQuotation != null) {
                    params.put("debtorName", rebateQuotation.getDebtorName());
                    params.put("epfNo", rebateQuotation.getEpfNo());
                    params.put("facilityNo", rebateQuotation.getFacilityNo());
                    params.put("vehicleNo", rebateQuotation.getVehicleNo());
                    params.put("loanBalance", rebateQuotation.getLoanBalance());
                    params.put("dateBalance", rebateQuotation.getDateBalance());
                    params.put("futureCapital", rebateQuotation.getFutureCapital());
                    params.put("futureInterest", rebateQuotation.getFutureInterest());
                    params.put("rebateRate", rebateQuotation.getRebateRate());
                    params.put("discount", rebateQuotation.getDiscount());
                    params.put("rebateAmount", rebateQuotation.getRebateAmount());
                    params.put("rebateAmountWithTax", rebateQuotation.getRebateAmountWithTax());
                    params.put("rentalOutstanding", rebateQuotation.getRentalOutstanding());
                    params.put("interestCharge", rebateQuotation.getInterestCharge());
                    params.put("rebateInterestCharge", rebateQuotation.getRebateInterestCharge());
                    params.put("odiWithTax", rebateQuotation.getOdiWithTax());
                    params.put("transferFee", rebateQuotation.getTransferFee());
                    params.put("transferFeeWithTax", rebateQuotation.getTransferFeeWithTax());
                    params.put("insuranceCharges", rebateQuotation.getInsuranceCharges());
                    params.put("seizingCharges", rebateQuotation.getSeizingCharges());
                    params.put("parkingCharges", rebateQuotation.getParkingCharges());
                    params.put("valuationCharges", rebateQuotation.getValuationCharges());
                    params.put("legalCharges", rebateQuotation.getLegalCharges());
                    params.put("otherCharges", rebateQuotation.getOtherCharges());
                    params.put("repaymentBalance", rebateQuotation.getRepaymentBalance());
                    params.put("ppWithTaxt", rebateQuotation.getPpWithTaxt());
                    params.put("ovpBalance", rebateQuotation.getOvpBalance());
                    params.put("secDeposit", rebateQuotation.getSecDeposit());
                    params.put("foregoneDeposit", rebateQuotation.getForegoneDeposit());
                    params.put("totRecoverable", rebateQuotation.getTotRecoverable());
                    params.put("rebatValidDate", rebateQuotation.getRebateValidDate());
                }
                params.put("REPORT_CONNECTION", c);
                URL url = this.getClass().getClassLoader().getResource("../reports/loanTransactions");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);

                InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanTransactions/rebateQuotation.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(retInputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, c);
                if (jasperPrint != null) {
                    jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    JRDocxExporter docxExporter = new JRDocxExporter();
                    docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outStream);
                    docxExporter.exportReport();
                }

                c.close();
                c = null;
                outStream.flush();
                outStream.close();
            } catch (IOException | URISyntaxException | SQLException | JRException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(value = "/printLoanTransactionsReceiptReport")
    public void printLoanTransactionsReceiptReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        String aggreementNo = request.getParameter("aggNo");
        int branchId = (Integer) request.getSession().getAttribute("branchId");
        try {
            Connection c = DBFacade.connect();
            String user_Name = userService.findWorkingUserName();
            String branch_Name = userService.findWorkingBranchName(branchId);
            int loanId = loanService.findLoanIdByAgreementNo(aggreementNo);
            LoanPropertyVehicleDetails vehicleDetails = loanService.findVehicleByLoanId(loanId);
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("branchName", branch_Name);
            params.put("userName", user_Name);
            params.put("aggNo", aggreementNo);
            params.put("loanId", loanId);
            if (vehicleDetails != null) {
                params.put("vehNo", vehicleDetails.getVehicleRegNo() != null ? vehicleDetails.getVehicleRegNo() : "N/A");
            }
            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/loanTransactions");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanTransactions/loan_receipt_transaction.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printCustomerInfoDetails")
    public void printCustomerInfoDetails(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename = Customer_Details.xls");
        try {
            Connection c = DBFacade.connect();
            List<CustomerInfo> customerInfoList = loanService.findLoanByIsIssuAndIsDelete();
            if (customerInfoList != null) {
                ServletOutputStream outStream = response.getOutputStream();
                Map<String, Object> params = new HashMap<>();
                params.put("REPORT_CONNECTION", c);
                URL url = this.getClass().getClassLoader().getResource("../reports/customer");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);

                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/customer/Customers.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(customerInfoList));
                if (jasperPrint != null) {
                    JRXlsExporter xlsExporter = new JRXlsExporter();
                    jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                    xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                    xlsExporter.exportReport();
                }

                c.close();
                c = null;
                outStream.flush();
                outStream.close();

            }
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printEnvelope")
    public void printEnvelope(HttpServletRequest request, HttpServletResponse response) {
        int debtorId = 0;
        String deb = request.getParameter("debtorId");
        String size = request.getParameter("size");
        String postType = request.getParameter("postType");
        String post = null;
        if (deb != null) {
            debtorId = Integer.parseInt(deb);
        }

        if (postType.equals("1")) {
            post = "REGISTERED POST";
        } else {
            post = "";
        }
        if (debtorId > 0) {
            try {
                response.setContentType("application/pdf");
                Connection c = DBFacade.connect();
                ServletOutputStream outStream = response.getOutputStream();
                Map<String, Object> params = new HashMap<>();
                params.put("debtorId", debtorId);
                params.put("postType", post);
                params.put("REPORT_CONNECTION", c);
                InputStream inputStream = null;
                switch (size) {
                    case "1":
                        inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/envelope/envelope_size_l.jasper");
                        break;
                    case "2":
                        inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/envelope/envelope_size_m.jasper");
                        break;
                    case "3":
                        inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/envelope/envelope_size_s.jasper");
                        break;
                }
                if (inputStream != null) {
                    JasperRunManager.runReportToPdfStream(inputStream, outStream, params);
                    c.close();
                    c = null;
                    outStream.flush();
                    outStream.close();
                }
            } catch (IOException | JRException | SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(value = "/loadMailListPage", method = RequestMethod.GET)
    public ModelAndView loadMailListPage(HttpServletRequest request, Model model) {
        List<RecoveryLetters> rls = recService.findLetters(true);
        model.addAttribute("letters", rls);
        return new ModelAndView("reports/mailListPage");
    }

    @RequestMapping(value = "/printMailList")
    public void printMailList(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename = Mail_List.xls");
        int branchId = (Integer) request.getSession().getAttribute("branchId");
        String let = request.getParameter("letterId");
        int letterType = 0;
        if (let != null) {
            letterType = Integer.parseInt(let);
        }
        if (letterType > 0) {
            try {
                List<Mail> mailList = recService.findMails(letterType);
                if (mailList != null) {
                    Connection c = DBFacade.connect();
                    String user_Name = userService.findWorkingUserName();
                    String branch_Name = userService.findWorkingBranchName(branchId);
                    RecoveryLetters letter = recService.findLetter(letterType);
                    ServletOutputStream outStream = response.getOutputStream();
                    Map<String, Object> params = new HashMap<>();
                    params.put("branchName", branch_Name);
                    params.put("userName", user_Name);
                    if (letter != null) {
                        params.put("letterName", letter.getDescription());
                    }
                    params.put("REPORT_CONNECTION", c);
                    URL url = this.getClass().getClassLoader().getResource("../reports/letters/mailingList");
                    String path = url.toURI().toString();
                    params.put("SUBREPORT_DIR", path);

                    InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/letters/mailingList/mailingList.jrxml");
                    JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                    JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(mailList));
                    if (jasperPrint != null) {
                        JRXlsExporter xlsExporter = new JRXlsExporter();
                        jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                        xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                        xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outStream);
                        xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                        xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                        xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                        xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                        xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                        xlsExporter.exportReport();
                    }
                    c.close();
                    c = null;
                    outStream.flush();
                    outStream.close();
                }
            } catch (IOException | URISyntaxException | JRException | SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(value = "/showContractReport")
    public ModelAndView printContractReport() {
        ModelAndView model = new ModelAndView("reports/contractReport");
        return model;
    }

    @RequestMapping(value = "/showReceivableReport")
    public ModelAndView printReceivableReport() {
        ModelAndView model = new ModelAndView("reports/receivableReport");
        return model;
    }

    @RequestMapping(value = "/pirntActiveContractReport")
    public void pirntActiveContractReport(HttpServletRequest request, HttpServletResponse response) {
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String user = userService.findWorkingUserName();
        String printed = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis());
        try {
            List<ActiveContracts> activeContract = reportService.getActiveContract(date1, date2);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename = Active_Contracts.xls");
            Connection con = DBFacade.connect();
            if (activeContract != null) {
                ServletOutputStream outputStream = response.getOutputStream();
                Map<String, Object> params = new HashMap<>();
                params.put("REPORT_CONNECTION", con);
                URL url = this.getClass().getClassLoader().getResource("../reports/activeContracts");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);
                params.put("date1", date1);
                params.put("date2", date2);
                params.put("user", user);
                params.put("printed", printed);

                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/activeContracts/ActiveContract.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(activeContract));
                if (jasperPrint != null) {
                    JRXlsExporter xlsExporter = new JRXlsExporter();
                    jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                    xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                    xlsExporter.exportReport();
                }
                con.close();
                con = null;
                outputStream.flush();
                outputStream.close();
            }
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/pirntNotActiveContractReport")
    public void pirntNotActiveContractReport(HttpServletRequest request, HttpServletResponse response) {
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String user = userService.findWorkingUserName();
        String printed = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis());
        try {
            List<ActiveContracts> notActiveContract = reportService.getNotActiveContract(date1, date2);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename = Initiated_but_not_activated_report.xls");
            Connection con = DBFacade.connect();
            if (notActiveContract != null) {
                ServletOutputStream outputStream = response.getOutputStream();
                Map<String, Object> params = new HashMap<>();
                params.put("REPORT_CONNECTION", con);
                URL url = this.getClass().getClassLoader().getResource("../reports/notActiveContracts");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);
                params.put("date1", date1);
                params.put("date2", date2);
                params.put("user", user);
                params.put("printed", printed);

                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/notActiveContracts/NotActiveContract.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(notActiveContract));
                if (jasperPrint != null) {
                    JRXlsExporter xlsExporter = new JRXlsExporter();
                    jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                    xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                    xlsExporter.exportReport();
                }
                con.close();
                con = null;
                outputStream.flush();
                outputStream.close();
            }
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/pirntRmvReceivableReport")
    public void pirntRmvReceivableReport(HttpServletRequest request, HttpServletResponse response) {
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String user = userService.findWorkingUserName();
        String printed = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis());
        try {
            List<RmvReceivable> rrs = reportService.getRmvReceivableRecords(date1, date2);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename = RMV_receivable_report.xls");
            Connection con = DBFacade.connect();
            if (rrs != null) {
                ServletOutputStream outputStream = response.getOutputStream();
                Map<String, Object> params = new HashMap<>();
                params.put("REPORT_CONNECTION", con);
                URL url = this.getClass().getClassLoader().getResource("../reports/rmvReceivableReport");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);
                params.put("date1", date1);
                params.put("date2", date2);
                params.put("user", user);
                params.put("printed", printed);

                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/rmvReceivableReport/rmvReceivableReport.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(rrs));
                if (jasperPrint != null) {
                    JRXlsExporter xlsExporter = new JRXlsExporter();
                    jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                    xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                    xlsExporter.exportReport();
                }
                con.close();
                con = null;
                outputStream.flush();
                outputStream.close();
            }
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/pirntServiceChargeReceivableReport")
    public void pirntServiceChargeReceivableReport(HttpServletRequest request, HttpServletResponse response) {
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String user = userService.findWorkingUserName();
        String printed = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis());
        try {
            List<RmvReceivable> rrs = reportService.getRmvReceivableRecords(date1, date2);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename = Service_Charge_Receivable_Report.xls");
            Connection con = DBFacade.connect();
            if (rrs != null) {
                ServletOutputStream outputStream = response.getOutputStream();
                Map<String, Object> params = new HashMap<>();
                params.put("REPORT_CONNECTION", con);
                URL url = this.getClass().getClassLoader().getResource("../reports/serviceChargeReceivable");
                String path = url.toURI().toString();
                params.put("SUBREPORT_DIR", path);
                params.put("date1", date1);
                params.put("date2", date2);
                params.put("user", user);
                params.put("printed", printed);

                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/serviceChargeReceivable/serviceChargeReceivableReport.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(rrs));
                if (jasperPrint != null) {
                    JRXlsExporter xlsExporter = new JRXlsExporter();
                    jasperPrint.setProperty("net.sf.jasperreports.export.xls.ignore.graphics", "true");
                    xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                    xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
                    xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, true);
                    xlsExporter.exportReport();
                }
                con.close();
                con = null;
                outputStream.flush();
                outputStream.close();
            }
        } catch (IOException | URISyntaxException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/loadoprefundrportform", method = RequestMethod.GET)
    public ModelAndView loadOPRefundReportForm() {
        return new ModelAndView("reports/OpRefundReportform");
    }

    @RequestMapping(value = "/generateOPRefundReport", method = RequestMethod.GET)
    public void generateOPRefundReport(HttpServletRequest request, HttpServletResponse response) {
        try {
            Connection c = DBFacade.connect();
            response.setContentType("application/pdf");
            ServletOutputStream outStream = response.getOutputStream();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println(request.getParameter("sdate"));
            Date sdate = format.parse(request.getParameter("sdate"));
            Date edate = format.parse(request.getParameter("edate"));
            Map<String, Object> params = new HashMap<>();
            params.put("sdate", sdate);
            params.put("edate", edate);

            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/OPRefundReport");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);
            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/OPRefundReport/payment_refund_daterange.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);
            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/rebateEarlyTermination", method = RequestMethod.GET)
    public void rebateEarlyTerminationReport(HttpServletRequest request, HttpServletResponse response) {
        try {
            Connection c = DBFacade.connect();
            response.setContentType("application/pdf");
            ServletOutputStream outStream = response.getOutputStream();

            int loanId = Integer.parseInt(request.getParameter("loanId"));
            LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(loanId);
            double totalLoanBalance = Double.parseDouble(request.getParameter("totalBalance"));
            double capitalUptoDate = Double.parseDouble(request.getParameter("remainingBalance"));
            int paidInstallmentCount = settlmentService.findPaidInstallmentCount(loanId);
            List<LoanInstallment> insList = loanService.findInstallmentByLoanNo(loan.getLoanAgreementNo());

            int totalInsCount = insList.size();
            int reInstallmentCount = totalInsCount - paidInstallmentCount;

            double interestUptoDate = Double.parseDouble(request.getParameter("remainingInterest"));
            double discountRate = Double.parseDouble(request.getParameter("discountRate"));
            double discountAmount = Double.parseDouble(request.getParameter("discountAmount"));
            double rebateAmount = Double.parseDouble(request.getParameter("rebateAmount"));
            double rebateCharge = Double.parseDouble(request.getParameter("charge"));
            double totalReceivable = Double.parseDouble(request.getParameter("totalReceivable"));
            double totalPaid = Double.parseDouble(request.getParameter("totalPaid"));

            double totRec = 0.0;
            if (totalReceivable == totRec) {
                totalReceivable = rebateAmount;
            }
            String user = userService.findWorkingUserName();

            Map<String, Object> params = new HashMap<>();
            params.put("totalLoanBalance", totalLoanBalance);
            params.put("capitalUptoDate", capitalUptoDate);
            params.put("totalInsCount", totalInsCount);
            params.put("paidInstallmentCount", paidInstallmentCount);
            params.put("reInstallmentCount", reInstallmentCount);
            params.put("interestUptoDate", interestUptoDate);
            params.put("discountRate", discountRate);
            params.put("discountAmount", discountAmount);
            params.put("rebateAmount", rebateAmount);
            params.put("rebateCharge", rebateCharge);
            params.put("totalReceivable", totalReceivable);
            params.put("totalPaid", totalPaid);
            params.put("agreementNo", loan.getLoanAgreementNo());
            params.put("user", user);

            params.put("REPORT_CONNECTION", c);
            URL url = this.getClass().getClassLoader().getResource("../reports/rebateLoanEarlyTermination");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/rebateLoanEarlyTermination/rebateLoanEarlyTermination.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // get loan details installment
    @RequestMapping(value = "/loadLoanInstallments", method = RequestMethod.GET)
    public ModelAndView loadLoanInstallments(Model model) {
        return new ModelAndView("reports/loanInstallments");
    }

    @RequestMapping(value = "/PrintLoanIntallmentReport")
    public void PrintLoanIntallmentReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        LoanHeaderDetails loanHeaderDetails = settlmentService.findLoanHeaderDetails(loanId);
//        String agreeNo = findLoanHeaderDetails.getLoanAgreementNo();
//        int loanID = 0;
        String branch_Name;
        int periodOfUser;
//        LoanHeaderDetails loanHeaderDetails = null;
        List<SettlementCharges> chargeses = null;
        try {

            String user_Name = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
//            loanHeaderDetails = reportService.findByLoanDetails(agreeNo);
            branch_Name = userService.findWorkingBranchName(branchID);
//            loanID = loanHeaderDetails.getLoanId();
            chargeses = reportService.findBySettlementChargees(loanId);
            periodOfUser = chargeses.size();

            Double total_Payment = reportService.findTotalPayments(loanId);

            Double finalBalance = reportService.findLoanBalanace(loanId);

            String loanBookNo = loanHeaderDetails.getLoanBookNo();

            String debtorName = loanHeaderDetails.getDebtorHeaderDetails().getDebtorName();
            String debtorNic = loanHeaderDetails.getDebtorHeaderDetails().getDebtorNic();
            String debtorPersonalAddress = loanHeaderDetails.getDebtorHeaderDetails().getDebtorPersonalAddress();
            String debtorTelephoneHome = loanHeaderDetails.getDebtorHeaderDetails().getDebtorTelephoneHome();
            String debtorTelephoneMobile = loanHeaderDetails.getDebtorHeaderDetails().getDebtorTelephoneMobile();

            MSubLoanType mSubLoanType = adminService.findMSubLoanType(loanHeaderDetails.getLoanType());
            double overPayAmount = settlmentService.getOverPayAmount(loanId);

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("userName", user_Name);
//            params.put("agreeNo", agreeNo);
            params.put("branchName", branch_Name);
            params.put("branchId", branchID);
            params.put("useMonth", periodOfUser);
            params.put("loanId", loanId);
            params.put("finalBalance", finalBalance);
            params.put("totalPayments", total_Payment);
            params.put("loanBookNo", loanBookNo);
            params.put("debtorName", debtorName);
            params.put("debtorNic", debtorNic);
            params.put("debtorPersonalAddress", debtorPersonalAddress);
            params.put("debtorTelephoneHome", debtorTelephoneHome);
            params.put("debtorTelephoneMobile", debtorTelephoneMobile);
            params.put("subLoanName", mSubLoanType.getSubLoanName());
            params.put("overPayAmount", overPayAmount);
            params.put("dueDate", loanHeaderDetails.getLoanDueDate());
            params.put("jobInfo", loanHeaderDetails.getDebtorHeaderDetails().getJobInformation());
            params.put("loanCount", loanHeaderDetails.getLoanCount());
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/loanDetails");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream retInputStream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanDetails/loan_installments.jasper");
            JasperRunManager.runReportToPdfStream(retInputStream, outStream, params);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

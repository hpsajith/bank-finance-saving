/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.ApprovalDetailsModel;
import com.bankfinance.form.DebtorForm;
import com.bankfinance.form.GuranterDetailsModel;
import com.bankfinance.form.InvoiseDetails;
import com.bankfinance.form.LoanForm;
import com.bankfinance.form.OtherChargseModel;
import com.bankfinance.form.PaymentDetails;
import com.bankfinance.form.PaymentVoucher;
import com.bankfinance.form.VoucherDetails;
import com.bankfinance.form.viewLoan;
import static com.ites.bankfinance.daoImpl.LoanDaoImpl.df2;
import com.ites.bankfinance.model.ConfigChartofaccountBank;
import com.ites.bankfinance.model.ConfigChartofaccountLoan;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.LoanApprovedLevels;
import com.ites.bankfinance.model.LoanCapitalizeDetail;
import com.ites.bankfinance.model.LoanCheckList;
import com.ites.bankfinance.model.LoanGuaranteeDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanInstalmentSchedule;
import com.ites.bankfinance.model.LoanPendingDates;
import com.ites.bankfinance.model.LoanPropertyArticlesDetails;
import com.ites.bankfinance.model.LoanPropertyLandDetails;
import com.ites.bankfinance.model.LoanPropertyOtherDetails;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.LoanRescheduleDetails;
import com.ites.bankfinance.model.MApproveLevels;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MCaptilizeType;
import com.ites.bankfinance.model.MCompany;
import com.ites.bankfinance.model.MLoanType;
import com.ites.bankfinance.model.MPeriodTypes;
import com.ites.bankfinance.model.MRateType;
import com.ites.bankfinance.model.MSupplier;
import com.ites.bankfinance.model.SettlementChequeDetails;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementVoucher;
import com.ites.bankfinance.model.UmUserTypeApproval;
import com.ites.bankfinance.model.VehicleMake;
import com.ites.bankfinance.model.VehicleModel;
import com.ites.bankfinance.model.VehicleType;
import com.ites.bankfinance.reportManager.DBFacade;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import com.ites.bankfinance.utils.NumberToWords;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoanController {

    @Autowired
    private LoanService loanService;

    @Autowired
    private AdminService adminService;

    @Autowired
    private MasterDataService masterService;

    @Autowired
    FileUploadController fileuploader;

    @Autowired
    SettlmentService settlmentService;

    @Autowired
    UserService userService;

    //--------LoanHeaderDetails loanSpec-------//
//    @RequestMapping(value = "/submitLoanFrom", method = RequestMethod.POST)
//    public ModelAndView submitLoanForm(@ModelAttribute("loanForm") LoanBasicDetails formData, BindingResult result, Model model) {
//        int loanId = loanService.saveBasicLoanDetails(formData);
////        List<LoanBasicDetails> loanList = loanService.findLoanDetails();
//        Map loanDetails = loanService.findByLoanId(loanId);
//        List<LoanBasicDetails> basicData = (List<LoanBasicDetails>)loanDetails.get("basic");
//        List<LoanEmploymentDetails> employmentData = (List<LoanEmploymentDetails>)loanDetails.get("employment");
//        
//        model.addAttribute("basic", basicData);
//        model.addAttribute("employment", employmentData);
//        
//        return new ModelAndView("viewLoanDetails");
//    }
    @RequestMapping(value = "/saveVehicleProperty", method = RequestMethod.POST)
    public @ResponseBody
    int saveVehiclePropertyFrom(@ModelAttribute("formVehicle") LoanPropertyVehicleDetails formData, Model model) {
        int saveId = 0;
        saveId = loanService.saveLoanPropertyVehicleDetails(formData);
        return saveId;
    }

    @RequestMapping(value = "/saveArticleProperty", method = RequestMethod.POST)
    public @ResponseBody
    int saveArticlePropertyFrom(@ModelAttribute("formArticle") LoanPropertyArticlesDetails formData, Model model) {
        int saveId = 0;
        saveId = loanService.saveLoanPropertyArticleDetails(formData);
        return saveId;
    }

    @RequestMapping(value = "/saveLandProperty", method = RequestMethod.POST)
    public @ResponseBody
    int saveLandPropertyFrom(@ModelAttribute("formLand") LoanPropertyLandDetails formData, Model model) {
        int saveId = 0;
        saveId = loanService.saveLoanPropertyLandDetails(formData);
        return saveId;
    }

    @RequestMapping(value = "/saveOtherProperty", method = RequestMethod.POST)
    public @ResponseBody
    int saveOtherPropertyFrom(@ModelAttribute("formArticle") LoanPropertyOtherDetails formData, Model model) {
        int saveId = 0;
        saveId = loanService.saveLoanPropertyOtherDetails(formData);
        return saveId;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = "/newLoanForm", method = RequestMethod.GET)
    public ModelAndView newLoanForm(Model model, HttpServletRequest request) {

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = (int) bID;
//        int branchId = 1;

        // loading branched
        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);

        // end loading branches
        List<MLoanType> loantypes = masterService.findLoanTypes();
        List<MRateType> loanRate = masterService.findRateType();
        MBranch currentBranch = null;
        currentBranch = adminService.findActiveBranch(branchId);
        String branchCode = currentBranch.getBranchCode();
        Date systemDate = settlmentService.getSystemDate(branchId);

        model.addAttribute("loanTypes", loantypes);
        model.addAttribute("rateTypeList", loanRate);
        model.addAttribute("branchCode", branchCode);
        model.addAttribute("systemDate", systemDate);
        return new ModelAndView("loan/newLoanForm");
    }

    @RequestMapping(value = "/deletedLoanForm", method = RequestMethod.GET)
    public ModelAndView deletedLoanForm(Model model, HttpServletRequest request) {
        List removeLoans = null;
        int branchID = (Integer) request.getSession().getAttribute("branchId");
        try {
            removeLoans = loanService.deletedLoans(branchID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("loanList", removeLoans);
        return new ModelAndView("loan/deletedLoansForm");
    }

    @RequestMapping(value = "/newLoanGroupCustomerForm", method = RequestMethod.GET)
    public ModelAndView newLoanGroupCustomerForm(Model model) {

        // loading branched
        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);

        // end loading branches
        List<MLoanType> loantypes = masterService.findLoanTypes();
        List<MRateType> loanRate = masterService.findRateType();
        model.addAttribute("loanTypes", loantypes);
        model.addAttribute("rateTypeList", loanRate);
        return new ModelAndView("debtor/newLoanGroupCustomerForm");
    }

    @RequestMapping(value = "/newCustomer", method = RequestMethod.GET)
    public ModelAndView newCustomer() {
        return new ModelAndView("debtor/newCustomer", "debtorForm", new DebtorForm());
    }

    @RequestMapping(value = "/sepNewCustomer", method = RequestMethod.GET)
    public ModelAndView sepNewCustomer(Model model, HttpServletRequest request) {
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = (int) bID;
        MBranch currentBranch = null;
        currentBranch = adminService.findActiveBranch(branchId);
        String branchCode = currentBranch.getBranchCode();
        model.addAttribute("branchCode", branchCode);
        return new ModelAndView("debtor/sepNewCustomer", "debtorForm", new DebtorForm());
    }

    @RequestMapping(value = "/searchCustomer", method = RequestMethod.GET)
    public ModelAndView searchCustomer() {
        return new ModelAndView("debtor/searchCustomer");
    }

    @RequestMapping(value = "/saveNewLoanForm", method = RequestMethod.POST)
    public ModelAndView saveNewLoanForm(@ModelAttribute("loanForm") LoanForm formData, BindingResult result, Model model, HttpServletRequest request) {

        String saveUpdate = "";
        Boolean isSave = false;
        Object bID = request.getSession().getAttribute("branchId");
        String bookNumber = request.getParameter("loanBookNo");
        String duedate = request.getParameter("loanDueDate");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate;
        try {
            startDate = df.parse(duedate);
            String newDateString = df.format(startDate);
            System.out.println("startDate ----->> " + startDate);
            formData.setDueDate(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        if (formData.getLoanId() == null) {
            isSave = true;
        }
        formData.setBranchId(branchId);
        formData.setBookNo(bookNumber);

        int loanId = 0;
        DebtorHeaderDetails debtor = null;

        if (formData.getGroupLoanID() != 0) {
            loanId = loanService.saveNewLoanDataGroup(formData);
        } else {
            loanId = loanService.saveNewLoanData(formData);
            debtor = loanService.findById(formData.getDebtorId());
        }

        if (formData.getLoanRescheduleId() != null) {
            settlmentService.changeLoanRescheduleDetailStatus(formData.getLoanRescheduleId(), loanId);
        }
        if (loanId > 0 && isSave) {
            saveUpdate = "Save Process Success..!";
        } else if (loanId > 0 && !isSave) {
            saveUpdate = "Update Process Success..!";
        } else {
            saveUpdate = "Process Fail..!";
        }

        List<GuranterDetailsModel> gurantorsList = loanService.findGuarantorByLoanId(loanId);
        List<OtherChargseModel> otherChargers = loanService.findOtherChargesByLoanId(loanId);
        List<InvoiseDetails> invoiceData = loanService.findInvoiceDetailsByLoanId(loanId);
        List<MRateType> rateTypes = masterService.findRateType();

        //udates
        //find total of other charges
        double total = 0.00;
        if (otherChargers != null) {
            for (int i = 0; i < otherChargers.size(); i++) {
                OtherChargseModel other = otherChargers.get(i);
                total = total + other.getAmount();
            }
        }

        //loan types
        List<MLoanType> loantypes = masterService.findLoanTypes();

        //find total interest
        double totalInterest = formData.getLoanInterest() * formData.getLoanPeriod();

        //loan Approvals
        ApprovalDetailsModel approve = loanService.findApprovalDetails(loanId);

        //findMarketing Officer
        String marketingOfficer = "";
        if (formData.getMarketingOfficer() != null) {
            marketingOfficer = adminService.findEmoloyeeLastName(formData.getMarketingOfficer());
        }

        double totalAmount = totalInterest + formData.getLoanInvestment();

        // loan capitalize tot amount
        List<LoanCapitalizeDetail> capitalizeDetails = loanService.findLoanCapitalizeDetails(loanId);
        double capitalizeAmount = 0.00;
        for (LoanCapitalizeDetail capitalizeDetail : capitalizeDetails) {
            capitalizeAmount = capitalizeAmount + capitalizeDetail.getAmount();
        }

        model.addAttribute("loanTypes", loantypes);
        model.addAttribute("rateTypeList", rateTypes);
        model.addAttribute("loanId", loanId);
        model.addAttribute("debtor", debtor);
        model.addAttribute("gurantors", gurantorsList);
        model.addAttribute("otherChargers", otherChargers);
        model.addAttribute("approve", approve);
        model.addAttribute("total", total);
        model.addAttribute("totalInterest", totalInterest);
        model.addAttribute("totalAmount", totalAmount);
        model.addAttribute("capitalizeAmount", capitalizeAmount);
        model.addAttribute("invoise", invoiceData);
        model.addAttribute("edit", true);
        model.addAttribute("markOfficer", marketingOfficer);
        model.addAttribute("loanBookNo", bookNumber);
        model.addAttribute("saveUpdate", saveUpdate);

        return new ModelAndView("loan/newLoanForm", "loan", formData);
    }

    @RequestMapping(value = "/searchLoanDeatilsById/{loanId}", method = RequestMethod.GET)
    public ModelAndView searchLoanDeatilsById(@PathVariable("loanId") int loanId, Model model, HttpServletRequest request) {
        LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(loanId);
        String message = "";
        LoanForm formData = new LoanForm();

        if (loan == null) {
            message = "Enter valid loan number";
        } else if (loan.getLoanIsIssue() >= 1) {
            message = "You have not authorization to edit this loan";
        } else if (loan.getLoanStatus() >= 2) {
            message = "You have not authorization to edit this loan";
        } else if (loan.getLoanApprovalStatus() > 1) {
            String approval_level = "";
            int appId = loan.getLoanApprovalStatus();
            if (appId == 2) {
                approval_level = "Marketing Manager";
            } else if (appId == 3) {
                approval_level = "Recovery Manager";
            } else if (appId == 4) {
                approval_level = "CED";
            }
            message = "This loan is already approved by " + approval_level;
        } else {
            int mainType = loanService.findLoanTypeBySubType(loan.getLoanType());

            formData.setLoanId(loanId);
            formData.setLoanAmount(loan.getLoanAmount());
            formData.setLoanDownPayment(loan.getLoanDownPayment());
            formData.setLoanInvestment(loan.getLoanInvestment());
            formData.setLoanPeriod(loan.getLoanPeriod());
            formData.setPeriodType(loan.getLoanPeriodType());
            formData.setLoanRate(loan.getLoanInterestRate());
            formData.setLoanRateCode(loan.getLoanRateCode());
            formData.setLoanInstallment(loan.getLoanInstallment());
            formData.setLoanInterest(loan.getLoanInterest());
            formData.setLoanType(loan.getLoanType());
            formData.setLoanMainType(mainType);
            formData.setDueDay(loan.getDueDay());
            formData.setMarketingOfficer(loan.getLoanMarketingOfficer());
            formData.setBookNo(loan.getLoanBookNo());

            //findMarketing Officer
            String marketingOfficer = "";
            if (formData.getMarketingOfficer() != null) {
                marketingOfficer = adminService.findEmoloyeeLastName(formData.getMarketingOfficer());
            }

            List<GuranterDetailsModel> gurantorsList = loanService.findGuarantorByLoanId(loanId);
            List<OtherChargseModel> otherChargers = loanService.findOtherChargesByLoanId(loanId);
            List<InvoiseDetails> invoiceData = loanService.findInvoiceDetailsByLoanId(loanId);
            DebtorHeaderDetails debtor = loanService.findById(loan.getDebtorHeaderDetails().getDebtorId());
            List<MLoanType> loantypes = masterService.findLoanTypes();
            List<MRateType> rateTypes = masterService.findRateType();

            double total = 0.00;
            if (otherChargers != null) {
                for (int i = 0; i < otherChargers.size(); i++) {
                    OtherChargseModel other = otherChargers.get(i);
                    total = total + other.getAmount();
                }
            }

            double totalInterest = formData.getLoanInterest() * formData.getLoanPeriod();

            ApprovalDetailsModel approve = loanService.findApprovalDetails(loanId);
            double totalAmount = totalInterest + formData.getLoanInvestment();

            List<LoanCapitalizeDetail> capitalizeDetails = loanService.findLoanCapitalizeDetails(loanId);
            double capitalizeAmount = 0.00;
            for (LoanCapitalizeDetail capitalizeDetail : capitalizeDetails) {
                capitalizeAmount = capitalizeAmount + capitalizeDetail.getAmount();
            }
            MBranch currentBranch = null;
            currentBranch = adminService.findActiveBranch(loan.getBranchId());
            String branchCode = currentBranch.getBranchCode();

            model.addAttribute("loanId", loanId);
            model.addAttribute("rateTypeList", rateTypes);
            model.addAttribute("debtor", debtor);
            model.addAttribute("gurantors", gurantorsList);
            model.addAttribute("otherChargers", otherChargers);
            model.addAttribute("approve", approve);
            model.addAttribute("total", total);
            model.addAttribute("capitalizeAmount", capitalizeAmount);
            model.addAttribute("totalInterest", totalInterest);
            model.addAttribute("edit", true);
            model.addAttribute("totalAmount", totalAmount);
            model.addAttribute("invoise", invoiceData);
            model.addAttribute("loanTypes", loantypes);
            model.addAttribute("markOfficer", marketingOfficer);
            model.addAttribute("loanBookNo", loan.getLoanBookNo());
            model.addAttribute("branchCode", branchCode);
        }
        model.addAttribute("message", message);
        return new ModelAndView("loan/newLoanForm", "loan", formData);
    }

    @RequestMapping(value = "/loadPropertForm/{type}", method = RequestMethod.GET)
    public ModelAndView loadPropertForm(@PathVariable("type") int type, HttpServletRequest request, Model model) {
        List<MSupplier> suppList = masterService.findSuppliers();
        List<VehicleType> vehicleType = masterService.findVehicleType();
        model.addAttribute("dealerList", suppList);
        model.addAttribute("vehicleTypeList", vehicleType);
//        return new ModelAndView("loan/loanVehicleDetails");
        if (type == 1) {
            model.addAttribute("vehicleTypeList", vehicleType);
            return new ModelAndView("loan/loanVehicleDetails");
        } else if (type == 2) {
            return new ModelAndView("loan/loanPawningArticle");
        } else if (type == 3) {
            return new ModelAndView("loan/loanLandDetails");
        } else {
            return new ModelAndView("loan/loanOtherDetails");
        }
    }

    @RequestMapping(value = "/loadVehicleMake/{vType}", method = RequestMethod.GET)
    @ResponseBody()
    public List loadVehicleModel(@PathVariable("vType") int vehicleType) {
        List<VehicleMake> vehicleMakes = null;
        try {
            vehicleMakes = masterService.findVehicleMake(vehicleType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleMakes;
    }

    @RequestMapping(value = "/loadVehicleModel", method = RequestMethod.GET)
    @ResponseBody()
    public List loadVehicleModel(HttpServletRequest request) {
        List<VehicleModel> vehicleMakes = null;
        int vehicleType = Integer.parseInt(request.getParameter("vehicleType"));
        int vehicleMake = Integer.parseInt(request.getParameter("vehicleMake"));
        try {
            vehicleMakes = masterService.findVehicleModel(vehicleType, vehicleMake);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleMakes;
    }

    @RequestMapping(value = "/viewAllLoans/{type}", method = RequestMethod.GET)
    public ModelAndView viewAllLoans(@PathVariable("type") int type, Model model, HttpServletRequest request) {

        // loading branched
        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);

        String sDate = "", eDate = "";
        if (request.getParameter("start") != null) {
            sDate = request.getParameter("start");
        }
        if (request.getParameter("end") != null) {
            eDate = request.getParameter("end");
        }

        Date startDate = null, endDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (sDate != null && !sDate.equals("")) {
                startDate = sdf.parse(sDate);
            }
            if (eDate != null && !eDate.equals("")) {
                endDate = sdf.parse(eDate);
            }
        } catch (Exception e) {
            System.out.println("Date parseble exception");
        }

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        List<MBranch> branchList = masterService.findBranches();

        List<viewLoan> loanList = loanService.findLoanByUserType(type, branchId, startDate, endDate);
        List<MApproveLevels> approval = masterService.findApprovalTypes();
        List<UmUserTypeApproval> user_approval = loanService.findApproveLevelByUser();
        model.addAttribute("type", type);
        model.addAttribute("approval", approval);
        model.addAttribute("userApproval", user_approval);
        model.addAttribute("sdate", sDate);
        model.addAttribute("edate", eDate);
        model.addAttribute("branch", branchList);
        return new ModelAndView("views/viewAllLoans", "loanList", loanList);
    }

    @RequestMapping(value = "/editPropertForm/{type}/{id}", method = RequestMethod.GET)
    public ModelAndView editPropertForm(@PathVariable("type") int type, @PathVariable("id") int id, HttpServletRequest request, Model model) {
        List<MSupplier> suppList = masterService.findSuppliers();
        model.addAttribute("dealerList", suppList);
        if (type == 1) {
            LoanPropertyVehicleDetails vehicle = (LoanPropertyVehicleDetails) loanService.findVehicleById(id);
            return new ModelAndView("loan/loanVehicleDetails", "vehicle", vehicle);
        } else if (type == 2) {
            LoanPropertyArticlesDetails article = (LoanPropertyArticlesDetails) loanService.findArticleById(id);
            return new ModelAndView("loan/loanPawningArticle", "article", article);
        } else if (type == 3) {
            LoanPropertyLandDetails land = (LoanPropertyLandDetails) loanService.findLandById(id);
            return new ModelAndView("loan/loanLandDetails", "land", land);
        } else {
            LoanPropertyOtherDetails other = (LoanPropertyOtherDetails) loanService.findOtherById(id);
            return new ModelAndView("loan/loanOtherDetails", "other", other);
        }
    }

    //Start

    @RequestMapping(value = "/viewActiveLoans", method = RequestMethod.GET)
    public ModelAndView viewActiveLoans(Model model, HttpServletRequest request) {

        // loading branches
        List<MBranch> branchs = null;
        try {
            branchs = adminService.findBranch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("branchList", branchs);

        String sDate = "", eDate = "";
        if (request.getParameter("start") != null) {
            sDate = request.getParameter("start");
        }
        if (request.getParameter("end") != null) {
            eDate = request.getParameter("end");
        }

        Date startDate = null, endDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (sDate != null && !sDate.equals("")) {
                startDate = sdf.parse(sDate);
            }
            if (eDate != null && !eDate.equals("")) {
                endDate = sdf.parse(eDate);
            }
        } catch (Exception e) {
            System.out.println("Date parseble exception");
        }

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        List<MBranch> branchList = masterService.findBranches();

        List<viewLoan> loanList = loanService.findLoanByUserType(3, branchId, startDate, endDate);
        List<MApproveLevels> approval = masterService.findApprovalTypes();
        List<UmUserTypeApproval> user_approval = loanService.findApproveLevelByUser();
        model.addAttribute("type", 3);
        model.addAttribute("approval", approval);
        model.addAttribute("userApproval", user_approval);
        model.addAttribute("sdate", sDate);
        model.addAttribute("edate", eDate);
        model.addAttribute("branch", branchList);
        return new ModelAndView("reports/viewActiveLoans", "loanList", loanList);
    }
    //End


    //click process button in new loan Form
    @RequestMapping(value = "/loanProcessStatus/{loanId}", method = RequestMethod.GET)
    public ModelAndView loanProcessStatus(@PathVariable("loanId") int loanId, Model model) {
        String message = "";

        viewLoan loanList = loanService.findLoanStatus(loanId);
        int loanStatus = loanList.getLoanStatus();
        int approvalLevelId = loanList.getAppLevelId();
        int approvalStatus = loanList.getLoanApprovalStatus();
        int documentStatus = loanList.getLoanDocumentSubmission();

        if (approvalLevelId > 0) {
            if (loanStatus == 2) {
                //Loan is reject
                message = "Loan has been rejected!!!";
                return new ModelAndView("messages/error", "errorMessage", message);
            } else if (approvalStatus < 4) {
                //Loan has no appravals
                message = "Loan is not approved!!!";
                return new ModelAndView("messages/warning", "warningMessage", message);
            } else if (documentStatus == 1) {
                model.addAttribute("type", "1");
                message = "Do you want to proceed loan without completing documents";
                return new ModelAndView("messages/dialog", "message", message);
            } else if (documentStatus == 3) {
                message = "This loan is already proceeded";
                return new ModelAndView("messages/warning", "warningMessage", message);
            } else {
                message = "Loan has been proceeded.\nDo you want to make payments now";
                model.addAttribute("type", "2");
                return new ModelAndView("messages/dialog", "message", message);
            }
        } else {
            //no authorization to process
            message = "You have no Authorization to proceed!!!";
            return new ModelAndView("messages/warning", "warningMessage", message);
        }
    }

    //click process button for generate aggrement no
    @RequestMapping(value = "/loanProcessForPayments/{loanId}/{due}", method = RequestMethod.GET)
    @ResponseBody
    public int loanProcessForPayments(@PathVariable("loanId") int loanId, @PathVariable("due") String due, Model model) {
        String message = "";
        Boolean isInsurance = false;

        viewLoan loanList = loanService.findLoanStatus(loanId);
        int loanStatus = loanList.getLoanStatus();
        int approvalLevelId = loanList.getAppLevelId();
        int approvalStatus = loanList.getLoanApprovalStatus();
        int documentStatus = loanList.getLoanDocumentSubmission();
        int loanIsIssue = loanList.getLoanIsIssue();
        Date dueDate = loanList.getDueDate();
        isInsurance = loanService.findInsuranceStatus(loanId);

        int loanType = 0;
        if (loanList.getLoanType() != null) {
            loanType = Integer.parseInt(loanList.getLoanType());
        }

        if (approvalLevelId > 0) {
            if (loanStatus == 2) {
                //Loan is reject                
                return 0;
            } else if (approvalStatus < 4) {
                //Loan has no appravals                    
                return 2;
            } else if (loanIsIssue == 0) {
//                if (dueDate == null && (loanType == 12 || loanType == 11 || loanType == 19)) {
                if (dueDate == null && (loanType == 0)) {
                    //set due date
                    return 6;
                } else if (isInsurance) {
                    boolean isProcess = loanService.processForPayment(loanId);
                    if (isProcess) {
                        //proceed success
                        return 3;
                    } else {
                        //no payments complete 
                        return 5;
                    }
                    //
                } else {
                    return 7;
                }
            } else {
                return 4;
            }
        } else {
            //no authorization to process           
            return 1;
        }
    }

    //add due Date
    @RequestMapping(value = "/addDueDate/{loanId}/{due}", method = RequestMethod.GET)
    @ResponseBody
    public int addDueDate(@PathVariable("loanId") int loanId, @PathVariable("due") String due, Model model) {
        int ret = 0;
        if (due != null && !due.equals("NO")) {
            ret = loanService.updateDueDate(due, loanId);
        }
        return ret;
    }

//    check document submission status
    //click process button
    @RequestMapping(value = "/loanDocumentSubmissionStatus/{loanId}", method = RequestMethod.GET)
    @ResponseBody
    public int loanDocumentSubmissionStatus(@PathVariable("loanId") int loanId, Model model, HttpServletRequest request) {
        String message = "";
        viewLoan loanList = loanService.findLoanStatus(loanId);
        int documentStatus = loanList.getLoanDocumentSubmission();
        int branchID = (Integer) request.getSession().getAttribute("branchId");

        if (documentStatus == 1) {
            if (branchID == 1) {
                return 2;
            } else {
                return 1;
            }
        } else if (documentStatus == 3) {
            return 2;
        } else {
            return 3;
        }
    }

    @RequestMapping(value = "/beforeProcessLoan/{loanId}", method = RequestMethod.GET)
    @ResponseBody
    public String beforeProcessLoan(@PathVariable("loanId") int loanId, Model model) {
        String isPost = loanService.updateLoanPaymentStatus(loanId);
        return isPost;
    }

    @RequestMapping(value = "/processLoan/{loanId}", method = RequestMethod.GET)
    public ModelAndView processLoan(@PathVariable("loanId") int loanId, Model model) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        int loanStatus = 0;
        Double insuranceCharge = 0.00;

        Double lossAmount = 0.00;
        LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(loanId);
        //List<PaymentDetails> installList = loanService.createInstallmentList(loan);

        List<LoanInstallment> installments = loanService.findInstallmentByLoanNo(loan.getLoanAgreementNo());

        lossAmount = loanService.findLossAmount(loanId);

        PaymentDetails payment = loanService.calculatePayments(loan);

        InsuraceProcessMain processMain = loanService.findInsuranceByLoanId(loanId);
        if (processMain != null) {
            if (processMain.getInsuTotalPremium() != null) {
                insuranceCharge = processMain.getInsuTotalPremium();
            }
        }

        Double[] initialCharges = loanService.findLoanInitialCharges(loanId);
        double loanPaidBalane = loan.getPaidAmount();
        double downPayment = initialCharges[2];
        double otherCharg = initialCharges[1];
        double paidAmount = initialCharges[4];

        model.addAttribute("agreementNo", loan.getLoanAgreementNo());
        model.addAttribute("memberNo", loan.getLoanBookNo());
        model.addAttribute("date", dateFormat.format(date));
        model.addAttribute("loanEndDate", loan.getLoanLapsDate());

        String loanDueDate = "";
//        String loanEndDate = this.calculateLoanEndDate(loan);
        if (loan.getLoanDueDate() != null) {
            loanDueDate = dateFormat.format(loan.getLoanDueDate());
        }
        double with_OtherCharges = loan.getLoanInvestment() - payment.getO_charges();
        String message = "";
        double issuAmount = 0.00;
        double advancedPayment = (downPayment + otherCharg + insuranceCharge) - paidAmount;
        double finalBalance = 0.00;
        if (advancedPayment > 0) {
            if (loan.getLoanIsPaidDown() == 0) {
                //not paid advance payment
                issuAmount = loan.getLoanAmount().doubleValue() - (advancedPayment);
                model.addAttribute("isPaid", false);
                message = "Loan is not Activated";
            } else if (loan.getLoanIsPaidDown() == 1) {
                //paid advanced payment for suspend
                issuAmount = loan.getLoanAmount().doubleValue();
                finalBalance = issuAmount - loanPaidBalane - advancedPayment;
                if (finalBalance > 0) {
                    message = "Remaining to pay " + finalBalance + " amount";
                } else {
                    message = "Loan is Activated";
                    loanStatus = 1;
                }
                model.addAttribute("isPaid", true);
            } else {
                // paid advanced payment for chek     
                issuAmount = loan.getLoanAmount().doubleValue() - (advancedPayment);
                finalBalance = loan.getLoanAmount().doubleValue() - loanPaidBalane;
                if (finalBalance > 0) {
                    message = "Remaining to pay " + finalBalance + " amount";
                } else {
                    message = "Loan is Activated";
                    loanStatus = 1;
                }
                model.addAttribute("isPaid", true);
            }
        } else {
            issuAmount = loan.getLoanAmount().doubleValue();
            finalBalance = issuAmount - loanPaidBalane;
            if (finalBalance > 0) {
                message = "Remaining to pay " + finalBalance + " amount";
            } else {
                message = "Loan is Activated";
                loanStatus = 1;
            }
        }

        model.addAttribute("loan", loan);
        model.addAttribute("insList", installments);
        model.addAttribute("loss", lossAmount);
        model.addAttribute("insuCharge", insuranceCharge);
//        model.addAttribute("loanEndDate", loanEndDate);
        model.addAttribute("loanDueDate", loanDueDate);
        model.addAttribute("pay", payment);
        model.addAttribute("other", with_OtherCharges);
        model.addAttribute("issuAmount", String.format("%.2f", issuAmount));
        model.addAttribute("voucher", loan.getVoucherNo());
        model.addAttribute("voucherId", loan.getVoucherId());
        model.addAttribute("advance", advancedPayment);
        model.addAttribute("message", message);
        model.addAttribute("loanStatus", loanStatus);
        return new ModelAndView("views/viewLoanProcess");
    }

    //generate agreement no
    @RequestMapping(value = "/generateAgreementNo/{loanId}", method = RequestMethod.GET)
    public ModelAndView generateAgreementNo(@PathVariable("loanId") int loanId, Model model) {
        String aggrementNo = "";
        aggrementNo = loanService.createLoanAgreementNo(loanId);
        if (!aggrementNo.equals("")) {
            return new ModelAndView("messages/success");
        } else {
            return new ModelAndView("messages/error");
        }
    }

    @RequestMapping(value = "/loadPendingDate/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadPendingDate(@PathVariable("loanId") int loanId, Model model) {
        LoanPendingDates pend = loanService.findPendingDates(loanId);
        List<MPeriodTypes> periods = masterService.findPeriodTypes();
        model.addAttribute("periodType", periods);
        if (pend != null) {
            return new ModelAndView("loan/pendingDocument", "pend", pend);
        } else {
            pend = new LoanPendingDates();
            pend.setLoanId(loanId);
            return new ModelAndView("loan/pendingDocument", "pend", pend);
        }
    }

    @RequestMapping(value = "/savePendingDates/{typeId}", method = RequestMethod.POST)
    public ModelAndView loadPendingDate(@ModelAttribute("pendingDates") LoanPendingDates formData, @PathVariable("typeId") int typeId, Model model) {
        int retVal = 0;
        retVal = loanService.saveOrUpdatePendingDates(formData, typeId);
        String message = "";
        if (retVal > 0) {
            model.addAttribute("status", "1");
            message = "Successfully Saved";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            model.addAttribute("status", "0");
            message = "Save Process Faild";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    @RequestMapping(value = "/viewLoanDetails/{loanId}", method = RequestMethod.GET)
    public ModelAndView findLoanDetails(@PathVariable("loanId") int loanId, Model model) {
//       viewLoan loan = loanService.findLoanByLoanId(loanId);
        LoanHeaderDetails loan = loanService.searchLoanByLoanId(loanId);
        List otherCharges = loanService.findOtherChargesByLoanId(loanId);
        List<GuranterDetailsModel> gurantors = loanService.findGuarantorByLoanId(loanId);
        DebtorHeaderDetails debtorHeaderDetailses = loanService.findDebtorHeader(loan.getDebtorHeaderDetails().getDebtorId());

        //calculate Loan End Date
        String loanEndDate = this.calculateLoanEndDate(loan);

        model.addAttribute("charges", otherCharges);
        model.addAttribute("guarantors", gurantors);
        model.addAttribute("loanEndDate", loanEndDate);
        model.addAttribute("debtorDetails", debtorHeaderDetailses);
        model.addAttribute("Total", loan.getLoanInterest() + loan.getLoanInstallment());

        return new ModelAndView("views/viewLoan", "loan", loan);
    }

    @RequestMapping(value = "/viewInstalmentSchedule/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadInstalmentSchedule(@PathVariable("loanId") int loanId, Model model) {
        List<LoanInstalmentSchedule> schedule = loanService.findInstalmentSchedule(loanId);
        String totalAmount = loanService.findTotalLoanAmountWithInterest(loanId);
        LoanHeaderDetails details = loanService.findLoanHeaderDetails(loanId);
        model.addAttribute("insScheduleList", schedule);
        model.addAttribute("totalAmount", totalAmount);
        model.addAttribute("loanID", loanId);
        model.addAttribute("loanStatus", details.getLoanStatus());
        model.addAttribute("listSize", schedule.size());
        return new ModelAndView("views/instalmentSchedule");
    }

    @RequestMapping(value = "/viewInsScheduleList/{loanId}", method = RequestMethod.GET)
    public ModelAndView viewInstalmentSchedule(@PathVariable("loanId") int loanId, Model model) {
        List<LoanInstallment> installments = null;
        Double tot = 0.00;
        Double ins = 0.00;
        Double instr = 0.00;
        try {
            installments = loanService.findScheduleList(loanId);
            for (int i = 0; i < installments.size(); i++) {
                LoanInstallment li = (LoanInstallment) installments.get(i);
                tot = tot + li.getInsPrinciple();
                ins = ins + li.getInsRoundAmount();
                instr = instr + li.getInsInterest();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("scheduleView", installments);
        model.addAttribute("tot", Double.parseDouble(df2.format(tot)));
        model.addAttribute("ins", Double.parseDouble(df2.format(ins)));
        model.addAttribute("instr", Double.parseDouble(df2.format(instr)));
        return new ModelAndView("views/instalmentScheduleView");
    }

    @RequestMapping(value = "/createSchedule", method = RequestMethod.POST)

    public ModelAndView updateInstalmentSchedule(@ModelAttribute("formInstalmentSchedule") LoanInstalmentSchedule instalmentSchedule, @RequestParam("instalmentAmount") Double[] insAmounts, @RequestParam("instalmentMonth") int[] insMonths, Model model) {
        String message = "";
        Boolean isUpdate = loanService.createSchedule(instalmentSchedule, insMonths, insAmounts);
        if (isUpdate) {
            message = "Successfully Updated";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Update Process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    @RequestMapping(value = "/clearInstallmentSchedule/{loanId}", method = RequestMethod.GET)
    public ModelAndView clearInstalmentSchedule(@PathVariable("loanId") int loanId, Model model) {
        String message = "";
        Boolean isUpdate = loanService.clearInstalmentSchedule(loanId);
        if (isUpdate) {
            message = "Successfully Clear Instalment Schedule";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Update Process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    @RequestMapping(value = "/findOtherCharges/{loanId}", method = RequestMethod.GET)
    public @ResponseBody
    List findOtherCharges(@PathVariable("loanId") int loanId) {
        List charges = loanService.findOtherChargesByLoand(loanId);
        return charges;
    }

    @RequestMapping(value = "/loadDocumentChecking", method = RequestMethod.GET)
    public ModelAndView loadDocumentChecking(@RequestParam("loan_id") int loanId, Model model) {
        //int loanType = masterService.findLoanTypeByLoanId(loanId);
        LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(loanId);
        LoanPendingDates pending = null;
        model.addAttribute("loanId", loanId);

        List<LoanCheckList> checkList = masterService.findCheckList(loan.getLoanType());
        List<LoanCheckList> loanChekList = loanService.findLoanCheckListByLoanId(loanId);
        pending = loanService.findPendingDates(loanId);

        String pendingMsg = "";
        if (pending != null) {
            pendingMsg = "Should be completed all documents before " + pending.getEndDate();
        }
        model.addAttribute("pendingMsg", pendingMsg);
        model.addAttribute("loanChekList", loanChekList);
        return new ModelAndView("loan/documentCheckList", "checklist", checkList);

    }

    @RequestMapping(value = "/saveCheqDetails", method = RequestMethod.POST)
    public ModelAndView saveCheqDetails(@ModelAttribute("checkPayment") SettlementVoucher formData, BindingResult result, Model model) {
        int checkId = 0;
        String message = "";

        SettlementChequeDetails cheque = new SettlementChequeDetails();
        cheque.setCheqNo(formData.getCheqNo());
        cheque.setCheqAmount(formData.getAmount());
        cheque.setCheqAmountText(formData.getAmountInText());
        cheque.setCheqRenewalDate(formData.getRenewalDate());
        cheque.setCheqIssueDate(formData.getIssueDate());
        checkId = loanService.saveChekDetails(cheque);
        if (checkId > 0) {
            model.addAttribute("status", checkId);
            message = "Saved Successfully";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Save process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    @RequestMapping(value = "/updateDocumentStatus/{loanId}", method = RequestMethod.GET)
    public @ResponseBody
    void updateDocumentStatus(@PathVariable("loanId") int loanId) {
        loanService.updateDocumentSubmissionStaus(loanId);
    }

    @RequestMapping(value = "/saveVoucherDetails", method = RequestMethod.POST)
    public @ResponseBody
    int[] saveVoucherDetails(@ModelAttribute("checkPayment") PaymentVoucher formData, BindingResult result, Model model, HttpServletRequest request) {
        int voucheId = 0;
//        String amount = String.valueOf(formData.getAmount());
//        voucheId = loanService.saveVoucher(formData.getVoucherType(), amount, formData.getLoanId());
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        formData.setBranchId(branchId);
        int[] saveIds = loanService.createNewVoucher(formData);

        return saveIds;
//        String message = "";
//        if(voucheId>0){
//            model.addAttribute("status",voucheId);
//            message = "Saved Successfully";
//            return new ModelAndView("messages/success", "successMessage", message);
//        }else{
//            message = "Save process Failed";
//            return new ModelAndView("messages/error", "errorMessage", message);
//        }   
    }

    @RequestMapping(value = "/generateMandateReport")
    public void generateMandateReport(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");

        String sLoanId = request.getParameter("mandateLoanId");
        int loanId = 0;
        try {

            if (sLoanId != null) {
                loanId = Integer.parseInt(sLoanId);
            }

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(Integer.parseInt(sLoanId));
            double totalInterest = 0.00;
            double investment = 0.00, totalLoanAmount = 0.00;
            double insAmount = 0.00;

            int insStatus = 0;
            if (loan != null) {
                totalInterest = loanService.getTotalInterestAmount(loan.getLoanId());
                investment = loan.getLoanInvestment();
                totalLoanAmount = investment + totalInterest;
                insStatus = loan.getInsuranceStatus();

                if (insStatus == 1) {
                    InsuraceProcessMain insurance = loanService.findInsuranceByLoanId(Integer.parseInt(sLoanId));
                    if (insurance != null) {
                        if (insurance.getInsuTotalPremium() != null) {
                            insAmount = insurance.getInsuTotalPremium();
                        }
                    }
                }
            }

            params.put("loan_id", loanId);
            params.put("totalLoanAmount", totalLoanAmount);
            params.put("totalInterest", totalInterest);
            params.put("insAmount", insAmount);
            params.put("insStatus", insStatus);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/mandateReport");
//            String path = url.getPath() + "/";
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/mandateReport/New_Loan_Details.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/generatePDF")
    public void generatePdf(HttpServletRequest request, HttpServletResponse response
    ) {
        response.setContentType("application/pdf");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            String TxtCrName = "";
//            String ChkDate = request.getParameter("ChkDate").toString().trim();
            String voucherNo = request.getParameter("voucherNo");
//            int chekId = Integer.parseInt(request.getParameter("hidCheqId").toString().trim());
            int cheqTo = Integer.parseInt(request.getParameter("cheqToName"));
            int loanId = Integer.parseInt(request.getParameter("loanId"));
            int cheqType = Integer.parseInt(request.getParameter("cheqType").toString().trim());

            double amt = Double.parseDouble(request.getParameter("TxtChqAmt"));

            loanService.updatePaymentAmount(voucherNo, amt);

            SettlementPaymentDetailCheque spdc = loanService.findSettlementPaymentDetailChequeByVoucherNo(voucherNo, amt, loanId);

            if (cheqTo != 2) {
                if (cheqType == 1) {
                    TxtCrName = loanService.findCheqRecievedName(spdc.getChequeId(), cheqTo);
                } else {
                    TxtCrName = "Cash";
                }
            } else {
                TxtCrName = "Cash";
            }

            String TxtChqAmt = request.getParameter("TxtChqAmt").toString().trim();
            String TxtChqAmtINWord = spdc.getAmountInTxt();
            String ChkDate = spdc.getRealizeDate().toString();

            boolean IsFullDate = false;
            boolean ChbIsACPay = false;
            if (request.getParameter("IsFullDate") != null && request.getParameter("IsFullDate").toString().trim().equalsIgnoreCase("on")) {
                IsFullDate = true;
            }
//            if (request.getParameter("ChbIsACPay") != null && request.getParameter("ChbIsACPay").toString().trim().equalsIgnoreCase("on")) {
//                ChbIsACPay = true;
//            }
            if (spdc.getChbIsACPay() != 0) {
                ChbIsACPay = true;
            }
            String DateArr[] = ChkDate.split("-");

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            URL url = this.getClass().getClassLoader().getResource("../reports/ChequeReport/");
            String path = url.toURI().toString();

            params.put("REPORT_CONNECTION", c);
            params.put("SUBREPORT_DIR", path);
            params.put("aD1", DateArr[2].substring(0, 1));
            params.put("aD2", DateArr[2].substring(1));
            params.put("bD1", DateArr[1].substring(0, 1));
            params.put("bD2", DateArr[1].substring(1));
            if (IsFullDate) {
                params.put("cD1", DateArr[0].substring(0, 1));
                params.put("cD2", DateArr[0].substring(1, 2));
            } else {
                params.put("cD1", "");
                params.put("cD2", "");
            }
            params.put("cD3", DateArr[0].substring(2, 3));
            params.put("cD4", DateArr[0].substring(3));
            if (ChbIsACPay) {
                params.put("AcPay", "A/C PAYEE ONLY");
            } else {
                params.put("AcPay", "");
            }
            double amount = Double.parseDouble(TxtChqAmt);
            DecimalFormat formatter = new DecimalFormat("#,###.00");

            String chV = formatter.format(amount);
            params.put("CrName", TxtCrName);
            params.put("ChqAmt", chV);
            params.put("ChqAmtINWord", TxtChqAmtINWord);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/ChequeReport/classic.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/generatePDFVoucher")
    public void generatePDFVoucher(@RequestParam("hidVoucherId") int voucherId, @RequestParam("hidVoucherType") int type, HttpServletRequest request, HttpServletResponse response
    ) {
        response.setContentType("application/pdf");
        int paymentType = type;
        System.out.println("Controller");
        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("voucher_no", voucherId);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/voucher/");
//            String path = url.getPath() + "/";
            String path = url.toURI().toString();
            System.out.println("path**" + path);
            params.put("SUBREPORT_DIR", path);

//            if (paymentType == 1) {
            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/voucher/cheque_voucher.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String calculateLoanEndDate(LoanHeaderDetails loan) {
        String endDate = "";
        try {
            Date loanDate = loan.getLoanStartDate();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("MM");
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd");

            c.setTime(loanDate);
            String due = "";

            if (loan.getLoanPeriodType() == 1) {
                c.add(Calendar.MONTH, loan.getLoanPeriod());
            }
            if (loan.getLoanPeriodType() == 2) {
                c.add(Calendar.YEAR, loan.getLoanPeriod());
            }
            if (loan.getLoanPeriodType() == 3) {
                c.add(Calendar.DATE, loan.getLoanPeriod());
            }

            if (loan.getDueDay() != null) {
                due = String.valueOf(loan.getDueDay());
            } else {
                due = sdf2.format(c.getTime());
            }

            String formattedYear = sdf.format(c.getTime());
            String formattedMonth = sdf1.format(c.getTime());

            endDate = formattedYear + "-" + formattedMonth + "-" + due;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return endDate;
    }

    private String calculateLoanDueDate(LoanHeaderDetails loan) {
        String dueDate = "";
        Date loanDate = loan.getLoanStartDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd");
        String due = "";
        Calendar c2 = Calendar.getInstance();
        c2.setTime(loanDate);
        if (loan.getLoanPeriodType() == 1 || loan.getLoanPeriodType() == 2) {
            c2.add(Calendar.MONTH, 1);
        } else if (loan.getLoanPeriodType() == 3) {
            c2.add(Calendar.DATE, 7);
        }
        try {
            if (loan.getDueDay() != null) {
                due = String.valueOf(loan.getDueDay());
            } else {
                due = sdf2.format(c2.getTime());
            }

            String formattedYear = sdf.format(c2.getTime());
            String formattedMonth = sdf1.format(c2.getTime());

            dueDate = formattedYear + "-" + formattedMonth + "-" + due;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dueDate;
    }

    @RequestMapping(value = "/findApprovalMessage", method = RequestMethod.POST)
    public ModelAndView loadApprovalPages(HttpServletRequest request, Model model) throws Exception {
        String loanId = request.getParameter("loanId");
//        String loanId = request.getParameter("loanId");
//        String loanId = request.getParameter("loanId");
        return null;
    }

    // load aproval level forms
    @RequestMapping(value = "/recoveryReportEdit", method = RequestMethod.POST)
    public ModelAndView recoveryReportEdit(HttpServletRequest request, Model model) throws Exception {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        int appLevelId = Integer.parseInt(request.getParameter("appLevelId"));
        int type = Integer.parseInt(request.getParameter("type"));

        LoanApprovedLevels approv = loanService.findloanApproval(loanId, appLevelId);
        if (approv == null) {
            approv = new LoanApprovedLevels();
            approv.setLoanId(loanId);
            approv.setApprovedLevel(appLevelId);
        }
        String title = "";
        if (appLevelId == 1) {
            title = "Marketing Manager Approval";
            model.addAttribute("title", title);
            model.addAttribute("type", type);
            model.addAttribute("appID", appLevelId);
            return new ModelAndView("loan/approveLoan", "approve", approv);
        } else if (appLevelId == 2) {
            title = "Recovery Manager Approval";
//            LoanRecoveryReport recovery = loanService.findRecoveryReportById(loanId);
            model.addAttribute("title", title);
            model.addAttribute("type", type);
            model.addAttribute("appID", appLevelId);
            return new ModelAndView("loan/approveLoan", "approve", approv);
         /*   if (recovery == null) {
                //not assign field officer
                List<ApprovalDetailsModel> officerList = loanService.findFiledOfficers(loanId);
                model.addAttribute("loanId", loanId);
                model.addAttribute("type", type);
                model.addAttribute("fieldOfficer", officerList);
                return new ModelAndView("loan/assignFieldOfficer");
            } else {
                //assign field officer
                String officerName = "";
                Employee emp = userService.findEmployeeById(recovery.getEmpNo());
                if (emp != null) {
                    officerName = emp.getEmpLname();
                }
                model.addAttribute("officerName", officerName);
                model.addAttribute("recovery", recovery);
                model.addAttribute("loanId", loanId);
                model.addAttribute("type", type);
                model.addAttribute("appID", appLevelId);
                return new ModelAndView("loan/recoveryManagerReport", "approve", approv);
            }*/
        } else {
            title = "CED/Accountant Approval";
            model.addAttribute("title", title);
            model.addAttribute("type", type);
            model.addAttribute("appID", appLevelId);
            return new ModelAndView("loan/approveLoan", "approve", approv);
        }

//        if (appLevelId > 1) {
//            if (appLevelId == 2) {
//                title = "Recovery Manager Approval";
//            } else {
//                title = "CED Approval";
//            }
//            model.addAttribute("title", title);
//            model.addAttribute("type", "1");
//            model.addAttribute("appID", appLevelId);
//            return new ModelAndView("loan/approveLoan", "approve", approv);
//        } else {
//            model.addAttribute("type", "1");
//            model.addAttribute("appID", appLevelId);
//            return new ModelAndView("loan/recoveryReport", "approve", approv);
//        }
    }

    // load aproval level forms
    @RequestMapping(value = "/recoveryReportView", method = RequestMethod.POST)
    public ModelAndView recoveryReportView(HttpServletRequest request, Model model) throws Exception {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        int appLevelId = Integer.parseInt(request.getParameter("appLevelId"));
        String message = "";

        LoanApprovedLevels approv = loanService.findloanApproval(loanId, appLevelId);
        if (approv == null) {
            message = "Approval is Pending";
            return new ModelAndView("messages/warning", "warningMessage", message);
        } else if (appLevelId > 1) {
            String title = "";
            if (appLevelId == 2) {
                title = "Recovery Manager Approval";
            } else {
                title = "CED Approval";
            }
            model.addAttribute("title", title);
            model.addAttribute("type", "0");
            model.addAttribute("appID", appLevelId);
            return new ModelAndView("loan/approveLoan", "approve", approv);
        } else {
            model.addAttribute("appID", appLevelId);
            model.addAttribute("type", "0");
            return new ModelAndView("loan/recoveryManagerReport", "approve", approv);
        }
    }

    //sane approve level 1 2 and 3        
    @RequestMapping(value = "/saveApproveLevel", method = RequestMethod.POST)
    public ModelAndView saveApproveLevel(@ModelAttribute("recoveryOfficerForm") LoanApprovedLevels formData, BindingResult result, Model model) {
        int approve = 0;
        String message = "";
        String status = "Save";
        if (formData.getPk() != null) {
            status = "Update";
        }

        approve = loanService.saveLoanApproval(formData);
        if (approve > 0) {
            message = "Successfully " + status + "d";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = status + " process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    //save field officer      
    @RequestMapping(value = "/saveFieldOfficer", method = RequestMethod.POST)
    public ModelAndView saveFieldOfficer(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("loan_id"));
        int officerId = Integer.parseInt(request.getParameter("officer_id"));
        String message = "";
        int save = 0;
        save = loanService.saveOrUpdateFieldOfficer(loanId, officerId);
        if (save > 0) {
            message = "Saved Successfully";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Save Process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    //assign fieldOfficer      
    @RequestMapping(value = "/changeFieldOfficer/{loanId}", method = RequestMethod.GET)
    public ModelAndView changeFieldOfficer(@PathVariable("loanId") int loanId, Model model) {
        List<ApprovalDetailsModel> officerList = loanService.findFiledOfficers(loanId);
        model.addAttribute("loanId", loanId);
        model.addAttribute("fieldOfficer", officerList);
        return new ModelAndView("loan/assignFieldOfficer");
    }

    //load edit rate   
    @RequestMapping(value = "/loadEditRate/{row}/{type}", method = RequestMethod.GET)
    public ModelAndView loadEditRate(@PathVariable("row") int row, @PathVariable("type") int type, Model model) {
        boolean hasApprove = userService.findLoggedUserApproval();
        model.addAttribute("hasApproval", hasApprove);
        model.addAttribute("row", row);
        model.addAttribute("type", type);
        String title = "Edit Rate";
        if (!hasApprove) {
            title = "You have no autherization to edit rate.\n Enter Password";
        }
        model.addAttribute("title", title);
        return new ModelAndView("loan/editRate");
    }

    //check password   
    @RequestMapping(value = "/checkUserPassword/{password}/{row}/{type}", method = RequestMethod.GET)
    public ModelAndView checkUserPassword(@PathVariable("password") String password, @PathVariable("row") int row, @PathVariable("type") int type, Model model) {
        boolean hasApprove = false;
        String state = userService.findApprovalsByPassword(password);
        String message = "";
        String title = "Edit Rate";
        if (state.equals("no")) {
            message = "Password not matched";
        } else if (state.equals("noApp")) {
            message = "User have no privilages";
        } else {
            hasApprove = true;
            message = "Password matched";
        }

        model.addAttribute("hasApproval", hasApprove);
        model.addAttribute("message", message);
        model.addAttribute("title", title);
        model.addAttribute("row", row);
        model.addAttribute("type", type);
        return new ModelAndView("loan/editRate");
    }

    //Make Payments   
    @RequestMapping(value = "/loadPaymentsPage/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadPaymentsPage(@PathVariable("loanId") int loanId, Model model) {

        List<ConfigChartofaccountLoan> bankDetails = null;
        LoanHeaderDetails loan = null;

        double totalOther_ini = 0.00;

        double paidOther = 0.00;
        double downpayment = 0.00;
        double totalPay = 0.00;
        double paidBalance = 0.00;
        double adjustment = 0.00;

        double investment = 0.00;
        double paidAmount = 0.00;
        double downPayment_ini = 0.00;
        double balance = 0.00;
        double advancedPayment = 0.00;
        double insuranceCharge = 0.00;
        BigDecimal totalAmount = new BigDecimal(0.00);
        double capitalizeAmount = 0.00;
        List<LoanCapitalizeDetail> capitalizeDetails = null;
        try {
            bankDetails = masterService.findBankAccountsDetails2();
            loan = loanService.findLoanBalance(loanId);
            InsuraceProcessMain processMain = loanService.findInsuranceByLoanId(loanId);
            if (processMain != null) {
                if (processMain.getInsuTotalPremium() != null) {
                    insuranceCharge = processMain.getInsuTotalPremium();
                }
            }
//          double totalOther = loanService.findOtherChargeSum(loanId);
            totalOther_ini = loanService.findOtherChargeSum(loanId);
            Double[] initialCharges = loanService.findLoanInitialCharges(loanId);
            totalAmount = loan.getLoanAmount();
            paidOther = initialCharges[4];
            downpayment = initialCharges[2];
            paidBalance = initialCharges[0];
            adjustment = initialCharges[3];
            totalPay = initialCharges[5];

            investment = loan.getLoanInvestment();
            paidAmount = loan.getPaidAmount();
            downPayment_ini = loan.getLoanDownPayment();
            balance = totalAmount.doubleValue() - paidAmount;
            advancedPayment = (downPayment_ini + totalOther_ini + adjustment + insuranceCharge) - totalPay;

            capitalizeDetails = loanService.findLoanCapitalizeDetails(loanId);
            for (LoanCapitalizeDetail capitalizeDetail : capitalizeDetails) {
                capitalizeAmount = capitalizeAmount + capitalizeDetail.getAmount();
            }

            if (loan.getLoanIsPaidDown() == 1) {
                model.addAttribute("isPaid", true);
            } else if (loan.getLoanIsPaidDown() == 2) {
                model.addAttribute("isPaid", true);
//            balance = balance - advancedPayment;
            } else {
                model.addAttribute("isPaid", false);
                balance = balance - paidBalance;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("loanId", loanId);
        model.addAttribute("adjustment", adjustment);
        model.addAttribute("insuCharge", insuranceCharge);
        model.addAttribute("total", totalAmount);
        model.addAttribute("paidAmount", paidAmount);
        model.addAttribute("balance", new BigDecimal(balance).setScale(2, RoundingMode.HALF_UP).doubleValue() - paidBalance);
        model.addAttribute("downpayment", downPayment_ini);
        model.addAttribute("investment", investment);
        model.addAttribute("banks", bankDetails);
        model.addAttribute("other", totalOther_ini);
        model.addAttribute("voucherNo", loan.getVoucherNo());
        model.addAttribute("adPayment", advancedPayment);
        model.addAttribute("capitalizeAmount", capitalizeAmount);
        model.addAttribute("capitalizeDetails", capitalizeDetails);
        model.addAttribute("bankBranch", loan.getDebtorHeaderDetails().getBankBranch());
        model.addAttribute("accountName", loan.getDebtorHeaderDetails().getAccountName());
        model.addAttribute("accountNo", loan.getDebtorHeaderDetails().getAccountNo());

        return new ModelAndView("loan/makePayments");
    }

    //load warning pop up   
    @RequestMapping(value = "/loadWarningBox/{message}", method = RequestMethod.GET)
    public ModelAndView loadWarningBox(@PathVariable("message") String message, Model model) {
        return new ModelAndView("messages/warning", "warningMessage", message);
    }

    //load warning pop up   
    @RequestMapping(value = "/loadSuccessBox/{message}", method = RequestMethod.GET)
    public ModelAndView loadSuccessBox(@PathVariable("message") String message, Model model) {
        return new ModelAndView("messages/success", "successMessage", message);
    }

    //load dialog popup   
    @RequestMapping(value = "/loadDialogBox/{message}/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadDialogBox(@PathVariable("message") String message, @PathVariable("loanId") int loanId, Model model) {
        model.addAttribute("loanId", loanId);
        model.addAttribute("type", 1);
        return new ModelAndView("messages/dialog", "message", message);
    }

    //load Error popup   
    @RequestMapping(value = "/loadErrorBox/{message}", method = RequestMethod.GET)
    public ModelAndView loadDialogBox(@PathVariable("message") String message, Model model) {
        return new ModelAndView("messages/error", "errorMessage", message);
    }

    //open loan form
    @RequestMapping(value = "/openLoanForm", method = RequestMethod.GET)
    public ModelAndView openLoanForm(Model model) {
        List<MLoanType> loantypes = masterService.findLoanTypes();
        model.addAttribute("loanTypes", loantypes);
        return new ModelAndView("loan/openLoanForm");
    }

    //advanced paymnet
    @RequestMapping(value = "/loadAdvancedPayment", method = RequestMethod.GET)
    public ModelAndView loadPaymentsPage(Model model, HttpServletRequest request) {

        List<ConfigChartofaccountBank> bankDetails = masterService.findBankAccountsDetails2();
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        double amount = Double.valueOf(request.getParameter("amount"));
        String voucher = request.getParameter("voucher");
        DecimalFormat df = new DecimalFormat("##.00");

//        System.out.println("mm"+df.format(amount));
        model.addAttribute("loanId", loanId);
        model.addAttribute("amount", df.format(amount));
        model.addAttribute("voucher", voucher);
        model.addAttribute("banks", bankDetails);

        return new ModelAndView("loan/advancePayment");
    }

    //save advanced paymnet
    @RequestMapping(value = "/saveAdvancedPayment", method = RequestMethod.POST)
    public ModelAndView saveAdvancedPayment(@ModelAttribute("checkPayment") SettlementPaymentDetailCheque formData, Model model, HttpServletRequest request, BindingResult result) {
        int chqId = loanService.saveAdvancePayment(formData);
        if (chqId > 0) {
            model.addAttribute("successMessage", "Advance Payment Print Successfully ");
            model.addAttribute("status", chqId);
            return new ModelAndView("messages/success");
        } else {
            model.addAttribute("errorMessage", "Print Process Failed");
            model.addAttribute("status", 0);
            return new ModelAndView("messages/error");
        }

    }

    //Remove Laon loan main page
    @RequestMapping(value = "/removeCurrentLoan", method = RequestMethod.GET)
    public ModelAndView searchLaon(Model model, HttpServletRequest request) {
        List removeLoans = null;
        int branchID = (Integer) request.getSession().getAttribute("branchId");
        try {
            removeLoans = loanService.removeLoans(branchID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("loanList", removeLoans);
        return new ModelAndView("views/removeLoan");
    }

    //remove loan
    @RequestMapping(value = "/removeLoan/{loanId}", method = RequestMethod.GET)
    public ModelAndView removeLoan(@PathVariable("loanId") int loanId, Model model) {
        model.addAttribute("loanID", loanId);
        model.addAttribute("methodType", 1);
        model.addAttribute("message", "Are you sure to remove this Loan...!");
        return new ModelAndView("messages/conformDialog");

    }

    //remove Success loan
    @RequestMapping(value = "/removeLoanSuccess/{loanId}", method = RequestMethod.GET)
    public ModelAndView removeLoanSuccess(@PathVariable("loanId") int loanId, Model model) {
        int isRemove = 0;
        try {
            isRemove = loanService.removeLoan(loanId);
            if (isRemove == 1) {
                model.addAttribute("successMessage", "Successfully Remove Loan");
                return new ModelAndView("messages/success");
            }
            if (isRemove == 2) {
                model.addAttribute("errorMessage", "Can't Remove This Loan, Because The Loan Is Issued");
                return new ModelAndView("messages/error");
            }
            if (isRemove == 0) {
                model.addAttribute("errorMessage", "Remove Process Failed");
                return new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            return new ModelAndView("messages/error");
        }
        return new ModelAndView("messages/success");
    }

    //load voucher
    @RequestMapping(value = "/loadVoucherByDate", method = RequestMethod.GET)
    public ModelAndView loadVoucherByDate(Model model, HttpServletRequest request) {
        String vDate = request.getParameter("vDate");
        List<SettlementVoucher> voucherList = loanService.findVouchersByDate(vDate);
        if (vDate == null) {
            model.addAttribute("vdate", "");
        } else {
            model.addAttribute("vdate", vDate);
        }
        model.addAttribute("voucherList", voucherList);
        return new ModelAndView("views/removeVoucher");
    }

    //edit voucher
    @RequestMapping(value = "/editVoucher/{voucherId}", method = RequestMethod.GET)
    public ModelAndView editVoucher(@PathVariable("voucherId") int vId, Model model) {

        List<VoucherDetails> voucherList = loanService.findPaymentByVoucher(vId);
        List<ConfigChartofaccountBank> bankDetails = masterService.findBankAccountsDetails2();
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("banks", bankDetails);
        model.addAttribute("vocherId", vId);
        return new ModelAndView("views/voucherDetails");
    }

    //edit Payments
    @RequestMapping(value = "/editPaymentType", method = RequestMethod.POST)
    public ModelAndView editPaymentType(HttpServletRequest request, Model model) {
        int transId = 0, bankId = 0, type = 0;
        String rDate = null, cheqNo = "", comment = "";
        if (request.getParameter("trnsId") != null && !request.getParameter("trnsId").equals("")) {
            transId = Integer.parseInt(request.getParameter("trnsId"));
        }
        if (request.getParameter("bank") != null && !request.getParameter("bank").equals("")) {
            bankId = Integer.parseInt(request.getParameter("bank"));
        }
        if (request.getParameter("type") != null && !request.getParameter("type").equals("")) {
            type = Integer.parseInt(request.getParameter("type"));
        }
        if (request.getParameter("rDate") != null && !request.getParameter("rDate").equals("")) {
            rDate = request.getParameter("rDate");
        }
        if (request.getParameter("cheqNo") != null && !request.getParameter("cheqNo").equals("")) {
            cheqNo = request.getParameter("cheqNo");
        }
        if (request.getParameter("comment") != null && !request.getParameter("comment").equals("")) {
            comment = request.getParameter("comment");
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date realizeDate = null;
        try {
            realizeDate = sdf.parse(rDate);
        } catch (Exception e) {
            System.out.println("Date Format Exception");
        }
        boolean msg = true;
        if (type == 2) {
            SettlementPaymentDetailCheque cheq = new SettlementPaymentDetailCheque();
            cheq.setBank(bankId);
            cheq.setChequeNo(cheqNo);
            cheq.setRealizeDate(realizeDate);
            cheq.setChequeId(transId);
            cheq.setRemark(comment);
            msg = loanService.updateCheqDetails(cheq);
        }

        String message = "";
        if (msg) {
            message = "Successfully Removed.";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Failed to remove voucher";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    //delete payment
    @RequestMapping(value = "/deletePayment", method = RequestMethod.POST)
    public ModelAndView deletePayment(HttpServletRequest request, Model model) {
        int transId = 0, type = 0;
        if (request.getParameter("trnsId") != null && !request.getParameter("trnsId").equals("")) {
            transId = Integer.parseInt(request.getParameter("trnsId"));
        }
        if (request.getParameter("type") != null && !request.getParameter("type").equals("")) {
            type = Integer.parseInt(request.getParameter("type"));
        }
        boolean msg = loanService.deletePayment(transId, type);
        String message = "";
        if (msg) {
            message = "Successfully Removed.";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Failed to remove voucher";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    //remove voucher
    @RequestMapping(value = "/removeVoucher/{voucherId}", method = RequestMethod.GET)
    public ModelAndView removeVoucher(@PathVariable("voucherId") int vId, Model model) {

        boolean voucher = loanService.removeVoucher(vId);
        String message = "";
        if (voucher) {
            message = "Successfully Removed.";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Failed to remove voucher";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    @RequestMapping(value = "/loadLoanInstallment", method = RequestMethod.GET)
    public ModelAndView loadLoanInstallment(HttpServletRequest request, Model model) {
        List<LoanInstallment> insList = new ArrayList();
        return new ModelAndView("loan/editInstallment", "insList", insList);
    }

    @RequestMapping(value = "/findInstallemntByAggNo", method = RequestMethod.GET)
    public ModelAndView findInstallemntByAggNo(HttpServletRequest request, Model model) {

        String loanNo = request.getParameter("loanNo");
        List<LoanInstallment> insList = loanService.findInstallmentByLoanNo(loanNo);
        model.addAttribute("aggNo", loanNo);
        return new ModelAndView("loan/editInstallment", "insList", insList);
    }

    @RequestMapping(value = "/updateLoanRental", method = RequestMethod.GET)
    public ModelAndView updateLoanRental(HttpServletRequest request, Model model) {

        double principle = 0.00, interest = 0.00, total = 0.00;
        int loanId = 0, insId = 0;

        if (request.getParameter("principle") != null) {
            principle = Double.parseDouble(request.getParameter("principle"));
        }
        if (request.getParameter("interest") != null) {
            interest = Double.parseDouble(request.getParameter("interest"));
        }
        if (request.getParameter("total") != null) {
            total = Double.parseDouble(request.getParameter("total"));
        }
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        if (request.getParameter("insId") != null) {
            insId = Integer.parseInt(request.getParameter("insId"));
        }
        String due = request.getParameter("dueDate");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dueDate = null;
        try {
            dueDate = sdf.parse(due);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Double roundedValue = (Double) (Math.ceil(total / 5d) * 5);
        LoanInstallment instlment = new LoanInstallment();
        instlment.setDueDate(dueDate);
        instlment.setInsPrinciple(principle);
        instlment.setInsInterest(interest);
        instlment.setInsAmount(total);
        instlment.setInsRoundAmount(roundedValue);
        instlment.setInsId(insId);
        instlment.setLoanId(loanId);

        boolean msg = loanService.updateRental(instlment);
        String message;
        if (msg) {
            message = "Updated Rental Successfully";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Update Rental Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }

    }

    @RequestMapping(value = "/deleteLoanPropertyArticleDetails", method = RequestMethod.GET)
    @ResponseBody
    public boolean deleteLoanPropertyArticleDetails(HttpServletRequest request) {
        boolean isDelete = false;
        String articleId = request.getParameter("articleId");
        try {
            isDelete = loanService.deleteLoanPropertyArticleDetails(Integer.parseInt(articleId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDelete;
    }

    @RequestMapping(value = "/loadLoanCapitalizeForm", method = RequestMethod.GET)
    public ModelAndView loadLoanCapitalizeForm(HttpServletRequest request, Model model) {
        try {
            List<MCaptilizeType> captilizeTypes = loanService.findCaptilizeTypes();
            model.addAttribute("captilizeTypes", captilizeTypes);
            model.addAttribute("capitalizeDetailsSize", 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("loan/loanCapitalizeForm");
    }

    @RequestMapping(value = "/editLoanCapitalizeForm/{loanId}", method = RequestMethod.GET)
    public ModelAndView editLoanCapitalizeForm(@PathVariable("loanId") int loanId, HttpServletRequest request, Model model) {
        try {
            List<MCaptilizeType> captilizeTypes = loanService.findCaptilizeTypes();
            List<LoanCapitalizeDetail> capitalizeDetails = loanService.findLoanCapitalizeDetails(loanId);
            model.addAttribute("captilizeTypes", captilizeTypes);
            model.addAttribute("capitalizeDetails", capitalizeDetails);
            model.addAttribute("capitalizeDetailsSize", capitalizeDetails.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("loan/loanCapitalizeForm");
    }

    @RequestMapping(value = "/generateLoanAgreement")
    public void generateLoanAgreement(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        int loanId = Integer.parseInt(request.getParameter("agreementLoanId"));
        String debtor_gurantor = "", sumLoanAmount = "", companyAddress = "", companyName = "",
                vehBodyType = "", vehRegNo = "", vehChasisNo = "", installmentCount = "",
                vehDescription = "", vehEquipments = "", vehEngineNo = "", vehExtras = "",
                vehInsuCompany = "", loanODIRate = "", loanAmountWithInt = "";
        List<String> grNo = Arrays.asList(new String[]{"SECOND PART", "THIRD PART", "FOURTH PART", "FIFTH PART"});
        try {
            LoanHeaderDetails loanDetail = loanService.findLoanHeaderDetails(loanId);
            if (loanDetail != null) {
                DebtorHeaderDetails debtorDetail = loanService.findDebtorHeader(loanDetail.getDebtorHeaderDetails().getDebtorId());
                List<LoanGuaranteeDetails> gurauntorDetails = loanService.findLoanGuranteeDetails(loanDetail.getLoanId());
                if (debtorDetail != null) {
                    debtor_gurantor = "FIRST PART and " + debtorDetail.getDebtorName() + " (" + debtorDetail.getDebtorNic() + ") of "
                            + debtorDetail.getDebtorPersonalAddress() + " (hereinafter called 'the Hirer'), of the";
                }
                for (int i = 0; i <= gurauntorDetails.size() - 1; i++) {
                    DebtorHeaderDetails guarantor = loanService.findDebtorHeader(gurauntorDetails.get(i).getDebtorId());
                    int num = i + 1;
                    if (i != gurauntorDetails.size() - 1) {
                        debtor_gurantor = debtor_gurantor + " " + grNo.get(i) + " and (" + num + "). " + guarantor.getDebtorName() + " (" + guarantor.getDebtorNic() + ") of "
                                + guarantor.getDebtorPersonalAddress() + " of the";
                    } else {
                        debtor_gurantor = debtor_gurantor + " " + grNo.get(i) + " and (" + num + "). " + guarantor.getDebtorName() + " (" + guarantor.getDebtorNic() + ") "
                                + guarantor.getDebtorPersonalAddress() + " (hereinafter called 'the Guarantors').";
                    }
                }
                BigDecimal loanAmount = loanDetail.getLoanAmount();
                String amountToWords = NumberToWords.convert(loanAmount.longValue());
                sumLoanAmount = amountToWords + " (Rs. " + loanAmount + ")";
                Integer installments = loanService.findLoanInstallmentCount(loanDetail.getLoanId());
                MPeriodTypes periodType = masterService.findPeriodType(loanDetail.getLoanPeriodType());
                if (periodType != null) {
                    installmentCount = installments + " " + periodType.getTypeDescription();
                }
                LoanPropertyVehicleDetails vehicleDetails = loanService.findVehicleByLoanId(loanDetail.getLoanId());
                if (vehicleDetails != null) {
                    VehicleMake vehicleMake = masterService.findVehicleMakeById(Integer.parseInt(vehicleDetails.getVehicleMake()));
                    if (vehicleMake != null) {
                        vehDescription = vehicleMake.getVehicleMakeName() + " / ";
                    }
                    VehicleModel vehicleModel = masterService.findVehicleModelById(Integer.parseInt(vehicleDetails.getVehicleModel()));
                    if (vehicleModel != null) {
                        vehDescription = vehDescription + vehicleModel.getVehicleModelName() + " ";
                    }
                    VehicleType vehicleType = masterService.findVehicleTypeById(Integer.parseInt(vehicleDetails.getVehicleType()));
                    if (vehicleType != null) {
                        vehDescription = vehDescription + vehicleType.getVehicleTypeName();
                    }
                    vehEngineNo = vehicleDetails.getVehicleEngineNo();
                    vehChasisNo = vehicleDetails.getVehiclChassisNo();
                    vehRegNo = vehicleDetails.getVehicleRegNo();
                }
                loanAmountWithInt = loanService.findTotalLoanAmountWithInterest(loanDetail.getLoanId());
            }
            MCompany company = userService.findCompanyDetails();
            if (company != null) {
                companyName = company.getComName();
                companyAddress = company.getAddress();
            }
            DBFacade db = new DBFacade();
            Connection c = DBFacade.connect();
            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<>();
            params.put("debtor_gurantors", debtor_gurantor);
            params.put("sumLoanAmount", sumLoanAmount);
            params.put("companyAddress", companyAddress);
            params.put("companyName", companyName);
            params.put("loanId", loanId);
            params.put("vehBodyType", vehBodyType);
            params.put("loanAmountWithInt", loanAmountWithInt);
            params.put("vehRegNo", vehRegNo);
            params.put("vehChasisNo", vehChasisNo);
            params.put("installmentCount", installmentCount);
            params.put("vehDescription", vehDescription);
            params.put("vehEquipments", vehEquipments);
            params.put("vehEngineNo", vehEngineNo);
            params.put("vehExtras", vehExtras);
            params.put("vehInsuCompany", vehInsuCompany);
            params.put("loanODIRate", loanODIRate);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/loanDetails/");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/loanDetails/loan_agrement.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/findRescheduleLoanDeatilsById/{rescheduleId}", method = RequestMethod.GET)
    public ModelAndView findRescheduleLoanDeatilsById(@PathVariable("rescheduleId") int rescheduleId, Model model, HttpServletRequest request) {
        LoanRescheduleDetails loanRescheduleDetails = settlmentService.findLoanRescheduleDetail(rescheduleId);
        LoanHeaderDetails loan = loanRescheduleDetails.getLoanHeaderDetailsByParentLoan();
        LoanForm formData = new LoanForm();
        int mainType = loanService.findLoanTypeBySubType(loan.getLoanType());
        formData.setLoanAmount(BigDecimal.valueOf(loanRescheduleDetails.getRescheduleBalance()));
        formData.setLoanDownPayment(0.00);
        formData.setLoanInvestment(0.00);
        formData.setLoanRateCode(loan.getLoanRateCode());
        formData.setLoanRate(loan.getLoanInterestRate());
        formData.setLoanType(loan.getLoanType());
        formData.setLoanMainType(mainType);
        formData.setLoanId(null);
        formData.setLoanRescheduleId(rescheduleId);

        List<OtherChargseModel> loanOtherChargers = loanService.findOtherChargesByLoanId(loan.getLoanId());
        List<OtherChargseModel> otherChargers = new ArrayList<>();
        for (OtherChargseModel charge : loanOtherChargers) {
            charge.setAmount(0.00);
            otherChargers.add(charge);
        }
        DebtorHeaderDetails debtor = loanService.findById(loan.getDebtorHeaderDetails().getDebtorId());
        List<MLoanType> loantypes = masterService.findLoanTypes();
        List<MRateType> rateTypes = masterService.findRateType();

        model.addAttribute("otherChargers", otherChargers);
        model.addAttribute("rateTypeList", rateTypes);
        model.addAttribute("debtor", debtor);
        model.addAttribute("edit", false);
        model.addAttribute("loanTypes", loantypes);

        return new ModelAndView("loan/newLoanForm", "loan", formData);
    }

    @RequestMapping(value = "/findLoanHeaderDetailseByAggNo", method = RequestMethod.GET)
    @ResponseBody
    public List<LoanHeaderDetails> findLoanHeaderDetailseByAggNo(HttpServletRequest request, Model model) {
        List<LoanHeaderDetails> loanHeaderDetailse = null;
        String aggNo = request.getParameter("aggNo");
        try {
            loanHeaderDetailse = loanService.findLoanHeaderDetailseByAggNo(aggNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loanHeaderDetailse;
    }

    @RequestMapping(value = "/findVehicleByRegNo", method = RequestMethod.GET)
    @ResponseBody
    public List<LoanPropertyVehicleDetails> findVehicleByRegNo(HttpServletRequest request, Model model) {
        List<LoanPropertyVehicleDetails> lpvds = null;
        String vehicleRegNo = request.getParameter("vehicleRegNo");
        try {
            lpvds = loanService.findVehicleByRegNo(vehicleRegNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lpvds;
    }

//    @RequestMapping(value = "/loanValidation/{loanTypeId}/{debtorID}", method = RequestMethod.GET)
//    public @ResponseBody
//    int loanValidation(@PathVariable("loanTypeId") int loanTypeId, @PathVariable("debtorID") int debtorID) {
//        System.out.println("loanTypeId -->> " + loanTypeId);
//        System.out.println("debtorID ---->> " + debtorID);
//        return 0;
//    }

    /*   @RequestMapping(value = "/checkDebtorNic/{nic}", method = RequestMethod.GET)
    public @ResponseBody
    int checkDebtorForNic(@PathVariable("nic") String nicNo) {
        List<DebtorHeaderDetails> detailses = debtorService.findDebtorBuNic(nicNo);
        if (detailses.size() > 0) {
            return 1;
        }
        return 0;

    }*/
}

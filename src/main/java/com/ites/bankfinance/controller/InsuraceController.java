/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.InsuranceCompany;
import com.ites.bankfinance.model.InsuranceOtherCharge;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.service.InsuranceService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/InsuraceController")
public class InsuraceController {

    @Autowired
    private InsuranceService insuranceService;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    //<---------------------------Start Insurace Part--------------------->
    //Load InsuranceCompanyMain to main Page
    @RequestMapping(value = "/loadInsuranceComMain", method = RequestMethod.GET)
    public ModelAndView insuLoadInsuranceMain(Model model) {
        List<InsuranceCompany> companys = null;
        try {
            companys = insuranceService.findInsuranceCompanyes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("companyList", companys);
        return new ModelAndView("insurance/insuranceCompanyMain");
    }

    //Laod InsuranceCompanyForm to insuranceCompanyForm page
    @RequestMapping(value = "/loadInsuranceForm", method = RequestMethod.GET)
    public ModelAndView adminLoadInsurancefORM(Model model) {
        return new ModelAndView("insurance/insuranceCompanyForm");
    }

    //saveOrUpdate InsuranceForm
    @RequestMapping(value = "/saveOrUpdateInsuranceCom", method = RequestMethod.POST)
    public ModelAndView insuSaveOrUpdateInsuranceCompany(@ModelAttribute("insuraceCompanyForm") InsuranceCompany formData, BindingResult result, Model model) {
        String message = "";
        Boolean is_Saved = false;
        try {
            if (formData.getComId() == null) {
                is_Saved = insuranceService.saveOrUpdateUserInsuraceCompany(formData);
                if (is_Saved) {
                    message = "Successfully Saved";
                }
            } else {
                is_Saved = insuranceService.saveOrUpdateUserInsuraceCompany(formData);
                if (is_Saved) {
                    message = "Successfully Updated";
                }
            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    //Edit Insurance Form
    @RequestMapping(value = "/editInsuraceCompany/{comId}", method = RequestMethod.GET)
    public ModelAndView editInsuranceCompanyForm(@PathVariable("comId") int comId, Model model) {
        InsuranceCompany company = null;
        try {
            company = insuranceService.findInsuranceCompanyID(comId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("company", company);
        return new ModelAndView("insurance/insuranceCompanyForm");
    }

    //Inactive Company
    @RequestMapping(value = "/inActiveInsuraceCompany/{comID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean inActiveCompany(@PathVariable("comID") int comID, Model model) {
        Boolean inActive = false;
        try {
            inActive = insuranceService.inActiveCompany(comID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inActive;
    }

    //active Company
    @RequestMapping(value = "/activeInsuraceCompany/{comID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean activeCompany(@PathVariable("comID") int comID, Model model) {
        Boolean active = false;
        try {
            active = insuranceService.activeCompany(comID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return active;
    }

    //load LS and HP leasing Type Grid
    @RequestMapping(value = "/loadAddInsuranceMain", method = RequestMethod.GET)
    public ModelAndView insuLoadInsuranceMian(Model model) {
        List loanList = null;
        try {
            loanList = insuranceService.findLoanHeaderDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("viewLoan", loanList);
        return new ModelAndView("insurance/viewInsuranceMain");
    }

    //laod LS and HP loan Using Agrement No:
    @RequestMapping(value = "/searchInsuranceByAgreementNo", method = RequestMethod.GET)
    public ModelAndView insuLoadByAgreementNO(Model model, HttpServletRequest request) {
        List loanList = null;
        String agreementNo = "";
        if (request.getParameter("agreeNo") != null) {
            agreementNo = (request.getParameter("agreeNo"));
        }
        try {
            loanList = insuranceService.findInsuranceByAgreementNo(agreementNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("viewLoan", loanList);
        return new ModelAndView("insurance/viewInsuranceMain");

    }

    //view Process Loan
    @RequestMapping(value = "/insuranceProcess", method = RequestMethod.GET)
    public ModelAndView viewInsuranceProcess(Model model, HttpServletRequest request) {
        LoanHeaderDetails details = null;
        List<InsuranceCompany> companys = null;
        InsuranceOtherCharge charge = null;
        InsuraceProcessMain insuranceDetails = null;
        MSubLoanType loanType = null;
        List<MBranch> branchs = null;
        int loanId = 0, insuId = 0, renewInsu = 0;
        double insuValue = 0.00;
        Date dayEnd_Date = null;
        Date insuDate = null;
        int Is_DayEnd = 0;

        if (request.getParameter("insuID") != null) {
            insuId = Integer.parseInt(request.getParameter("insuID"));
        }
        if (request.getParameter("renewInsu") != null) {
            renewInsu = Integer.parseInt(request.getParameter("renewInsu"));
        }

        if (request.getParameter("loanID") != null && request.getParameter("loanValue") != null) {
            loanId = Integer.parseInt(request.getParameter("loanID"));
            insuValue = Double.parseDouble(request.getParameter("loanValue"));
        }
        try {
            if (insuId != 0) {
                insuranceDetails = insuranceService.findInsuranceDetails(insuId);
            }
            companys = insuranceService.findInsuranceCompanyes();
            details = insuranceService.findLoanDetails(loanId);
            charge = insuranceService.findInsOtherCharge();
            loanType = insuranceService.findSubLoanType(loanId);
            branchs = insuranceService.findBranches();
            dayEnd_Date = insuranceService.findDayEndDate();
            if (insuId != 0) {
                String date = dateFormat.format(insuranceDetails.getActionTime());
                insuDate = dateFormat.parse(date);
                if (dayEnd_Date.compareTo(insuDate) == 0) {
                    Is_DayEnd = 1;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("companyList", companys);
        model.addAttribute("dayEndDate", dayEnd_Date);
        model.addAttribute("loanDetails", details);
        model.addAttribute("insOtherCharge", charge);
        model.addAttribute("loanType", loanType);
        model.addAttribute("loanValue", insuValue);
        model.addAttribute("renew", renewInsu);
        model.addAttribute("insuDetails", insuranceDetails);
        model.addAttribute("branches", branchs);
        model.addAttribute("IsDayEnd", Is_DayEnd);
        return new ModelAndView("insurance/processInsurance");
    }

    //view Process Loan
    @RequestMapping(value = "/insuranceDebitProcess", method = RequestMethod.GET)
    public ModelAndView insuranceDebitProcess(Model model, HttpServletRequest request) {
        LoanHeaderDetails details = null;
        List<InsuranceCompany> companys = null;
        InsuranceOtherCharge charge = null;
        InsuraceProcessMain insuranceDetails = null;
        MSubLoanType loanType = null;
        List<MBranch> branchs = null;
        int loanId = 0, insuId = 0, renewInsu = 0;
        double insuValue = 0.00;
        Date dayEnd_Date = null;
        Date insuDate = null;
        int Is_DayEnd = 0;

        if (request.getParameter("insuID") != null) {
            insuId = Integer.parseInt(request.getParameter("insuID"));
        }
        if (request.getParameter("renewInsu") != null) {
            renewInsu = Integer.parseInt(request.getParameter("renewInsu"));
        }

        if (request.getParameter("loanID") != null && request.getParameter("loanValue") != null) {
            loanId = Integer.parseInt(request.getParameter("loanID"));
            insuValue = Double.parseDouble(request.getParameter("loanValue"));
        }
        try {
            if (insuId != 0) {
                insuranceDetails = insuranceService.findInsuranceDetails(insuId);
            }
          
            
            
            companys = insuranceService.findInsuranceCompanyes();
            details = insuranceService.findLoanDetails(loanId);
            charge = insuranceService.findInsOtherCharge();
            loanType = insuranceService.findSubLoanType(loanId);
            branchs = insuranceService.findBranches();
            dayEnd_Date = insuranceService.findDayEndDate();
            if (insuId != 0) {
                String date = dateFormat.format(insuranceDetails.getActionTime());
                insuDate = dateFormat.parse(date);
                if (dayEnd_Date.compareTo(insuDate) == 0) {
                    Is_DayEnd = 1;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("companyList", companys);
        model.addAttribute("dayEndDate", dayEnd_Date);
        model.addAttribute("loanDetails", details);
        model.addAttribute("insOtherCharge", charge);
        model.addAttribute("loanType", loanType);
        model.addAttribute("loanValue", insuValue);
        model.addAttribute("renew", renewInsu);
        model.addAttribute("insuDetails", insuranceDetails);
        model.addAttribute("branches", branchs);
        model.addAttribute("IsDayEnd", Is_DayEnd);
        return new ModelAndView("insurance/insuranceDebitProcess");
    }

    @RequestMapping(value = "/registedInsurance/{insuID}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean registedInsurance(@PathVariable("insuID") int insuId, Model model) {
        Boolean isRegisted = false;
        try {
            isRegisted = insuranceService.registedInsurance(insuId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isRegisted;
    }

    @RequestMapping(value = "/generateInsuranceVoucher/{loans}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean generateInsuranceVoucher(@PathVariable("loans") String loans, Model model) {
        Boolean isGenerate = false;
        try {
            if (loans != null) {
                String[] split = loans.split(",");
                Integer[] loanIds = new Integer[split.length];
                for (int i = 0; i < split.length; i++) {
                    Integer loanId = Integer.parseInt(split[i]);
                    loanIds[i] = loanId;
                }
                if (loanIds.length > 0) {
                    isGenerate = insuranceService.generateInsuranceVoucher(loanIds);
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return isGenerate;
    }

    @RequestMapping(value = "/saveDiliverType", method = RequestMethod.GET)
    public @ResponseBody
    Boolean saveDiliver(Model model, HttpServletRequest request) {
        Boolean isDiliver = false;
        int insuId = 0, diliverType = 0;

        if (request.getParameter("insu_ID") != null) {
            insuId = Integer.parseInt(request.getParameter("insu_ID"));
        }
        if (request.getParameter("diliver") != null) {
            diliverType = Integer.parseInt(request.getParameter("diliver"));
        }
        try {
            isDiliver = insuranceService.saveDiliverType(insuId, diliverType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDiliver;
    }

    @RequestMapping(value = "/insuranceCardIsDeliver", method = RequestMethod.GET)
    public ModelAndView LoanInsuranceCardIsDeliver(Model model, HttpServletRequest request) {
        int insuId = 0;
        InsuraceProcessMain processMain = null;
        int isDiliver = 0;
        if (request.getParameter("insuID") != null) {
            insuId = Integer.parseInt(request.getParameter("insuID"));
        }
        try {
            processMain = insuranceService.findDiluverInsurance(insuId);
            isDiliver = processMain.getIsDiliver();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("diliverType", isDiliver);
        model.addAttribute("insu_ID", insuId);
        return new ModelAndView("insurance/insuranceCardDiliverType");
    }

    @RequestMapping(value = "/loanInsuranceRegistry", method = RequestMethod.GET)
    public ModelAndView loanInsuranceRegistry(Model model) {
        List registedList = null;
        try {
            registedList = insuranceService.findRegistedLoan();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("registedLoans", registedList);
        return new ModelAndView("insurance/insuranceRegistry");
    }

    //process Insurance
    @RequestMapping(value = "/saveInsurance/{otherInsu}", method = RequestMethod.POST)
    public ModelAndView insuranceProcess(@ModelAttribute("processInsuranceForm") InsuraceProcessMain insurance, BindingResult result, Model model, HttpServletRequest request) {
        String message = "";
        Boolean is_Saved = false;
        int other = 0;

        if (request.getParameter("otherInsu") != null) {
            other = Integer.parseInt(request.getParameter("otherInsu"));
        }
        insurance.setOtherInsu(other);
        try {
            is_Saved = insuranceService.processInsurace(insurance);
            if (is_Saved) {
                message = "Process Successfully ";
            }

        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errorMessage", "Insurance Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    //Load viewProcessInsurance to main Page
    @RequestMapping(value = "/loadViewProcessInsurance", method = RequestMethod.GET)
    public ModelAndView insuLoadViewProcessInsu(Model model) {
        List processList = null;
        try {
            processList = insuranceService.findProcessLoan();

        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("processLoan", processList);
        return new ModelAndView("insurance/viewProcessInsurance");
    }

    //Load loadRenewInsuranceMain to main Page
    @RequestMapping(value = "/loadRenewInsuranceMain", method = RequestMethod.GET)
    public ModelAndView insuLoadRenewInsurance(Model model) {
        List renewList = null;
        List<MSubLoanType> subLoanType = null;
        List<LoanHeaderDetails> loanHeaderDetailses = null;
        try {
            renewList = insuranceService.findRenewInsurance();
            subLoanType = insuranceService.findSubLoanType();
            loanHeaderDetailses = insuranceService.findLoanDetails();
        } catch (Exception e) {
            System.out.println("");
            e.printStackTrace();
        }

        model.addAttribute("renewInsu", renewList);
        model.addAttribute("subLoanType", subLoanType);
        model.addAttribute("loanDetails", loanHeaderDetailses);
        return new ModelAndView("insurance/renewInsuranceMain");
    }

    //<---------------------------End Insurace Part--------------------->
    //<---------------------------Start Insurace Other Customer--------------------->
    //Load InsuCustomerMain to main Page
    @RequestMapping(value = "/loadInsuCustomerMain", method = RequestMethod.GET)
    public ModelAndView insuLoadCustomerMain(Model model) {
        List<InsuranceOtherCustomers> customerses = null;
        try {
            customerses = insuranceService.findOtherCustomers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("customerList", customerses);
        return new ModelAndView("insurance/addCustomerMain");
    }

    //Load InsuCustomerForm to InsuCustomerMain
    @RequestMapping(value = "/loadInsuCustomerForm", method = RequestMethod.GET)
    public ModelAndView insuLoadCustomerForm(Model model) {

        return new ModelAndView("insurance/addCustomerForm");
    }

    //saveOrUpdate InsuranceForm
    @RequestMapping(value = "/saveOrUpdateCustomer", method = RequestMethod.POST)
    public ModelAndView insuSaveOrUpdateOtherCustomer(@ModelAttribute("addInsuCustomerForm") InsuranceOtherCustomers formData, BindingResult result, Model model) {
        String message = "";
        Boolean is_Saved = false;
        try {
            if (formData.getInsuCustomerId() == null) {
                is_Saved = insuranceService.saveOrUpdateOtherCustomers(formData);
                if (is_Saved) {
                    message = "Successfully Saved";
                }
            } else {
                is_Saved = insuranceService.saveOrUpdateOtherCustomers(formData);
                if (is_Saved) {
                    message = "Successfully Updated";
                }

            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    //edit Other Customer
    @RequestMapping(value = "/viewEditOtherCustomer/{Id}", method = RequestMethod.GET)
    public ModelAndView editInsuranceOtherCustomer(@PathVariable("Id") int Id, Model model) {
        InsuranceOtherCustomers otherCustomers = null;
        try {
            otherCustomers = insuranceService.findInsuranceOtherCustomer(Id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("otherCustomer", otherCustomers);
        return new ModelAndView("insurance/addCustomerForm");
    }

    //popUp Other Insurance
    @RequestMapping(value = "/popUpOtherInsurance", method = RequestMethod.GET)
    public ModelAndView insuOtherInsurance(Model model) {
        List<InsuranceOtherCustomers> customerses = null;
        List<InsuranceCompany> companys = null;
        List<MBranch> branchs = null;
        try {
            customerses = insuranceService.findOtherCustomers();
            companys = insuranceService.findInsuranceCompanyes();
            branchs = insuranceService.findBranches();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("companyList", companys);
        model.addAttribute("customerList", customerses);
        model.addAttribute("branches", branchs);
        return new ModelAndView("insurance/otherInsuranceForm");
    }

    //view Other Insurance
    @RequestMapping(value = "/loadOtherInsuranceMain", method = RequestMethod.GET)
    public ModelAndView insuViewOtherInsurance(Model model) {
        List otherInsu = null;
        List<InsuranceOtherCustomers> customerses = null;
        try {
            otherInsu = insuranceService.findOtherInsurance();
            customerses = insuranceService.findOtherCustomers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("otherInsu", otherInsu);
        model.addAttribute("customerList", customerses);
        return new ModelAndView("insurance/viewOtherInsuranceMain");
    }

    //edit OtherInsurance
    @RequestMapping(value = "/editOtherInsurace/{otherID}", method = RequestMethod.GET)
    public ModelAndView editOtherInsurance(@PathVariable("otherID") int insuId, Model model) {
        InsuraceProcessMain processMain = null;
        List<InsuranceOtherCustomers> customerses = null;
        List<MBranch> branchs = null;
        try {
            processMain = insuranceService.findOtherInsurance(insuId);
            customerses = insuranceService.findOtherCustomers();
            branchs = insuranceService.findBranches();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("otherInsurance", processMain);
        model.addAttribute("otherCustomer", customerses);
        model.addAttribute("branches", branchs);
        return new ModelAndView("insurance/otherInsuranceForm");
    }

    @RequestMapping(value = "/findByLoanId/{loanId}", method = RequestMethod.GET)
    public ModelAndView findByLoanId(@PathVariable("loanId") int loanId, Model model) {
        LoanHeaderDetails details = null;
        List<InsuranceCompany> companys = null;
        InsuranceOtherCharge charge = null;
        InsuraceProcessMain insuranceDetails = null;
        MSubLoanType loanType = null;
        List<MBranch> branchs = null;
        double insuValue = 0.00;
        Date dayEnd_Date = null;
        int Is_DayEnd = 0;
        boolean byLoanSearch = true;
        try {
            insuranceDetails = insuranceService.findByLoanId(loanId);
            companys = insuranceService.findInsuranceCompanyes();
            details = insuranceService.findLoanDetails(loanId);
            charge = insuranceService.findInsOtherCharge();
            loanType = insuranceService.findSubLoanType(loanId);
            branchs = insuranceService.findBranches();
            dayEnd_Date = insuranceService.findDayEndDate();
        } catch (Exception e) {
            e.printStackTrace();

        }
        model.addAttribute("companyList", companys);
        model.addAttribute("dayEndDate", dayEnd_Date);
        model.addAttribute("loanDetails", details);
        model.addAttribute("insOtherCharge", charge);
        model.addAttribute("loanType", loanType);
        model.addAttribute("loanValue", insuValue);
        model.addAttribute("insuDetails", insuranceDetails);
        model.addAttribute("branches", branchs);
        model.addAttribute("IsDayEnd", Is_DayEnd);
        model.addAttribute("byLoanSearch", byLoanSearch);
        return new ModelAndView("insurance/processInsurance");
    }

    @RequestMapping(value = "/findInsuranceCompany/{companyId}", method = RequestMethod.GET)
    @ResponseBody
    public InsuranceCompany findInsuranceCompany(@PathVariable("companyId") int companyId) {
        return insuranceService.findInsuranceCompanyID(companyId);
    }

    @RequestMapping(value = "/processInsuranceCommission/{insuId}", method = RequestMethod.GET)
    public ModelAndView processInsuranceCommission(@PathVariable("insuId") int insuId, Model model) {
        model.addAttribute("insuDetails", insuranceService.findInsuranceDetails(insuId));
        return new ModelAndView("insurance/processInsuranceCommission");
    }

    //process Insurance
    @RequestMapping(value = "/saveInsuCommission/{otherInsu}", method = RequestMethod.POST)
    public ModelAndView saveInsuCommission(@ModelAttribute("processInsuranceForm") InsuraceProcessMain insurance, BindingResult result, Model model, HttpServletRequest request) {
        String message = "";
        Boolean is_Saved = false;
        int other = 0;
        if (request.getParameter("otherInsu") != null) {
            other = Integer.parseInt(request.getParameter("otherInsu"));
        }
        insurance.setOtherInsu(other);
        try {
            is_Saved = insuranceService.addInsuraceCommission(insurance);
            if (is_Saved) {
                message = "Process Successfully ";
            }

        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errorMessage", "Insurance Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/findInsuranceByAgreementNo", method = RequestMethod.GET)
    public ModelAndView findInsuranceByAgreementNo(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String agreementNo = request.getParameter("agreementNo");
        String s = request.getParameter("status");
        int status = Integer.parseInt(s);
        List list = insuranceService.findInsuranceByAgreementNo(agreementNo, status);
        System.out.println(list.size());
        switch (status) {
            case 1:
                modelAndView = new ModelAndView("insurance/viewProcessInsuranceSearch");
                model.addAttribute("processLoan", list);
                break;
            case 2:
                modelAndView = new ModelAndView("insurance/insuranceDebitChargeSearch");
                model.addAttribute("debitCharges", list);
                break;
            default:
                modelAndView = new ModelAndView("insurance/insuranceRegistrySearch");
                model.addAttribute("registedLoans", list);
                break;
        }
        return modelAndView;
    }

    @RequestMapping(value = "/findInsuranceByPolicyNo", method = RequestMethod.GET)
    public ModelAndView findInsuranceByPolicyNo(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String policyNo = request.getParameter("policyNo");
        String s = request.getParameter("status");
        int status = Integer.parseInt(s);
        List list = insuranceService.findInsuranceByPolicyNo(policyNo, status);
        switch (status) {
            case 1:
                modelAndView = new ModelAndView("insurance/viewProcessInsuranceSearch");
                model.addAttribute("processLoan", list);
                break;
            case 2:
                modelAndView = new ModelAndView("insurance/insuranceDebitChargeSearch");
                model.addAttribute("debitCharges", list);
                break;
            default:
                modelAndView = new ModelAndView("insurance/insuranceRegistrySearch");
                model.addAttribute("registedLoans", list);
                break;
        }
        return modelAndView;
    }

    @RequestMapping(value = "/findInsuranceByVehicleNo", method = RequestMethod.GET)
    public ModelAndView findInsuranceByVehicleNo(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String vehicleNo = request.getParameter("vehicleNo");
        String s = request.getParameter("status");
        int status = Integer.parseInt(s);
        List list = insuranceService.findInsuranceByVehicleNo(vehicleNo, status);
        switch (status) {
            case 1:
                modelAndView = new ModelAndView("insurance/viewProcessInsuranceSearch");
                model.addAttribute("processLoan", list);
                break;
            case 2:
                modelAndView = new ModelAndView("insurance/insuranceDebitChargeSearch");
                model.addAttribute("debitCharges", list);
                break;
            default:
                modelAndView = new ModelAndView("insurance/insuranceRegistrySearch");
                model.addAttribute("registedLoans", list);
                break;
        }
        return modelAndView;
    }

    @RequestMapping(value = "/findInsuranceByDebtorName", method = RequestMethod.GET)
    public ModelAndView findInsuranceByDebtorName(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String debtorName = request.getParameter("debtorName");
        String s = request.getParameter("status");
        int status = Integer.parseInt(s);
        List list = insuranceService.findInsuranceByDebtorName(debtorName, status);
        switch (status) {
            case 1:
                modelAndView = new ModelAndView("insurance/viewProcessInsuranceSearch");
                model.addAttribute("processLoan", list);
                break;
            case 2:
                modelAndView = new ModelAndView("insurance/insuranceDebitChargeSearch");
                model.addAttribute("debitCharges", list);
                break;
            default:
                modelAndView = new ModelAndView("insurance/insuranceRegistrySearch");
                model.addAttribute("registedLoans", list);
                break;
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadViewInsuranceDebitCharge", method = RequestMethod.GET)
    public ModelAndView loadViewInsuranceDebitCharge(Model model, HttpServletRequest request) {
        return new ModelAndView("insurance/insuranceDebitCharge");
    }

    //Insurance Debit Process
    @RequestMapping(value = "/saveInsuranceDebitProcess/{otherInsu}", method = RequestMethod.POST)
    public ModelAndView insuranceDebitProcess(@ModelAttribute("processInsuranceForm") InsuraceProcessMain insurance, BindingResult result, Model model, HttpServletRequest request) {
        String message = "";
        Boolean is_Saved = false;
        int other = 0;

        if (request.getParameter("otherInsu") != null) {
            other = Integer.parseInt(request.getParameter("otherInsu"));
        }
        insurance.setOtherInsu(other);
        try {
            is_Saved = insuranceService.saveInsuranceDebitProcess(insurance);
            if (is_Saved) {
                message = "Process Successfully ";
            }

        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errorMessage", "Insurance Process Fail");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/deleteInsuranceMessage", method = RequestMethod.GET)
    public ModelAndView deleteInsuranceMessage(HttpServletRequest request, Model model) {
        String loanId = request.getParameter("loanID");
        String insuId = request.getParameter("insuID");
        String insuCode = request.getParameter("insuCode");

        model.addAttribute("loanId", loanId);
        model.addAttribute("insuId", insuId);
        model.addAttribute("insuCode", insuCode);
        model.addAttribute("methodType", 1);
        model.addAttribute("message", "Are you sure to Delete This Insurance ?");
        return new ModelAndView("messages/conformDialog2");
    }

    @RequestMapping(value = "/deleteInsurance", method = RequestMethod.GET)
    public ModelAndView deleteInsurance(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        Boolean is_Saved = false;
        Boolean is_delete = false;

        int loanId = Integer.parseInt(request.getParameter("loanID"));
        int insuId = Integer.parseInt(request.getParameter("insuID"));
        String insuCode = request.getParameter("insuCode");
        LoanHeaderDetails loanHeaderDetails = null;
        InsuraceProcessMain insuraceProcessMain = null;
        loanHeaderDetails = insuranceService.findLoanDetails(loanId);
        if (loanHeaderDetails.getLoanId() != null || loanHeaderDetails.getLoanSpec() != 0 || loanHeaderDetails.getLoanIsIssue() != 0) {
            insuraceProcessMain = insuranceService.findInsuranceDetails(insuId);
            if (insuraceProcessMain.getInsuId() != null) {
                is_Saved = insuranceService.saveOrUpdateInsuranceProcessDelete(insuraceProcessMain);
                if (is_Saved == true) {
                    is_delete = insuranceService.deleteInsuranceProcessMainByInsuId(insuId, insuCode, loanId);

                    if (is_delete == true) {
                        message = "Insurance Successfully Deleted ";
                        model.addAttribute("successMessage", message);
                        modelAndView = new ModelAndView("messages/success");
                    } else {
                        message = "This Insurance cannot be Delete. This is On Going Loan.";
                        model.addAttribute("warningMessage", message);
                        modelAndView = new ModelAndView("messages/warning");
                    }
                } else {
                    message = "This Insurance cannot be Delete. This is On Going Loan.";
                    model.addAttribute("warningMessage", message);
                    modelAndView = new ModelAndView("messages/warning");
                }
            }
        } else {
            message = "This Insurance cannot be Delete. This is On Going Loan.";
            model.addAttribute("warningMessage", message);
            modelAndView = new ModelAndView("messages/warning");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadDeletedInsuranceMain", method = RequestMethod.GET)
    public ModelAndView loadDeletedInsuranceMain(Model model, HttpServletRequest request) {
        return new ModelAndView("insurance/viewDeletedInsuranceMain");
    }

    //<---------------------------End Insurace Other Customer--------------------->
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

}

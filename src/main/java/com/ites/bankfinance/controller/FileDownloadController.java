package com.ites.bankfinance.controller;

import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/FileDownloadController")
public class FileDownloadController {

    private static final int BUFFER_SIZE = 1024;
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private SettlmentService settlmentService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/loadFileDownloadPage", method = RequestMethod.GET)
    public ModelAndView loadFileDownloadPage(HttpServletRequest request, Model model) {
        try {
            int userId = userService.findByUserName();
            int userLogBranchId = settlmentService.getUserLogedBranch(userId);
            Date systemDate = settlmentService.getSystemDate(userLogBranchId);
            Calendar cal = Calendar.getInstance();
            cal.setTime(systemDate);
            cal.add(Calendar.DATE, -1);
            systemDate = cal.getTime();
            String sDate = dateFormat.format(systemDate);
            model.addAttribute("systemDate", sDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("fileDownload/fileDownloadPage");
    }

    @RequestMapping(value = "/downloadPostingReports", method = RequestMethod.GET)
    public void downloadPostingReports(HttpServletRequest request, HttpServletResponse response) {
        String sDate = request.getParameter("sDate");
        String eDate = request.getParameter("eDate");
        if (sDate != null && eDate != null) {
            try {
                Date startDate = dateFormat.parse(sDate);
                Date endDate = dateFormat.parse(eDate);
                File zipFile = settlmentService.batchPostingToZip(startDate, endDate);
                if (zipFile != null) {
                    if (zipFile.canRead()) {
                        FileInputStream inputStream = new FileInputStream(zipFile);
                        response.setContentType("application/octet-stream");
                        response.setContentLength((int) zipFile.length());
                        response.setHeader("Content-Disposition", "attachment; filename = " + zipFile.getName());
                        ServletOutputStream outputStream = response.getOutputStream();
                        byte[] buffer = new byte[BUFFER_SIZE];
                        int bytesRead = -1;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                        inputStream.close();
                        outputStream.close();
                    }
                }
            } catch (ParseException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(value = "/downloadODIPostingReport", method = RequestMethod.GET)
    public void downloadODIPostingReport(HttpServletRequest request, HttpServletResponse response) {
        String stDate = request.getParameter("startDate");
        String enDate = request.getParameter("endDate");
        int vType = Integer.parseInt(request.getParameter("vType"));

        if (stDate != null && enDate != null) {
            try {
                Date startDate = dateFormat.parse(stDate);
                Date endDate = dateFormat.parse(enDate);

                File zipFile = settlmentService.batchPostingODIToZip(startDate, endDate, vType);

                if (zipFile != null) {
                    if (zipFile.canRead()) {
                        FileInputStream inputStream = new FileInputStream(zipFile);
                        response.setContentType("application/octet-stream");
                        response.setContentLength((int) zipFile.length());
                        response.setHeader("Content-Disposition", "attachment; filename = " + zipFile.getName());
                        ServletOutputStream outputStream = response.getOutputStream();
                        byte[] buffer = new byte[BUFFER_SIZE];
                        int bytesRead = -1;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                        inputStream.close();
                        outputStream.close();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.DayEndStatus;
import com.ites.bankfinance.model.DayEndDate;
import com.ites.bankfinance.model.DayEndProcess;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.service.DayEndService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ites
 */
@Controller
@RequestMapping(value = "/DayEndController")
public class DayEndController {

    @Autowired
    DayEndService dayEndService;

    @Autowired
    UserService userService;

    @Autowired
    SettlmentService settlmentService;
    
    @Autowired
    MasterDataService masterDataService;

    @RequestMapping(value = "/loadDayEndProcess", method = RequestMethod.GET)
    public ModelAndView loadDayEndProcess(Model model, HttpServletRequest request) {
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }

        List<DayEndProcess> jobs = dayEndService.findConfigJobs();
        String lastDay = dayEndService.getLastDayEnd(branchId);
        String user_Name = userService.findWorkingUserName();
        model.addAttribute("lastDay", lastDay);
        model.addAttribute("userName", user_Name);
        return new ModelAndView("dayEnd/dayEndPage", "jobs", jobs);
    }

    @RequestMapping(value = "/dayEndProcess", method = RequestMethod.GET)
    public ModelAndView dayEndProcess(Model model, HttpServletRequest request) {
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        String msg = dayEndService.dayEndProcess(branchId);
        String nextMsg = dayEndService.findLoanOverPayment(branchId);

        if (msg.equals("OK")) {
            if (nextMsg.equals("OK")) {
                return new ModelAndView("messages/success", "successMessage", "Day End Process is Completed");
            } else {
                return new ModelAndView("messages/warning", "warningMessage", nextMsg);
            }
        } else {
            return new ModelAndView("messages/warning", "warningMessage", msg);
        }
    }

    @RequestMapping(value = "/dayEndProcessCorrection", method = RequestMethod.GET)
    public ModelAndView dayEndProcessCorrection(Model model, HttpServletRequest request) {

        String sdate = request.getParameter("start");
        String edate = request.getParameter("end");

        String msg = dayEndService.correctPosting(sdate, edate);
        if (msg.equals("OK")) {
            return new ModelAndView("messages/success", "successMessage", "Day End Process is Completed");
        } else {
            return new ModelAndView("messages/warning", "warningMessage", msg);
        }
    }

    @RequestMapping(value = "/confirmDayEnd", method = RequestMethod.GET)
    public ModelAndView confirmDayEnd(Model model, HttpServletRequest request) {
        int loginBranchID = (Integer) request.getSession().getAttribute("branchId");
        String branch_Name = userService.findWorkingBranchName(loginBranchID);
        Date systemDate = settlmentService.getSystemDate(loginBranchID);
        model.addAttribute("dayEndDate", systemDate);
        model.addAttribute("branch", branch_Name);
        return new ModelAndView("dayEnd/confirmDayEnd");

    }

    @RequestMapping(value = "/confirmCommonDayEnd", method = RequestMethod.POST)
    public ModelAndView commonDayEndConfirmation(@RequestParam("branches") int branchList[], Model model, HttpServletRequest request) {
        Date systemDate = settlmentService.getSystemDate(1);
        List<MBranch> branchs = masterDataService.findBranches();
        List<MBranch> dayEndBranch = new ArrayList<>();
        String branchNames = "";
        for (int i = 0; i < branchList.length; i++) {
            MBranch branch = new MBranch();
            branch.setBranchId(i + 1);
            if (branchs.get(i).getBranchId() == branch.getBranchId()) {
                branch.setBranchName(branchs.get(i).getBranchName());
                if (branchNames.equals("")) {
                    branchNames = branch.getBranchName();
                } else {
                    branchNames = branchNames + "," + branch.getBranchName();
                }
            }
            dayEndBranch.add(branch);
            branch = null;
        }
        model.addAttribute("dayEndDate", systemDate);
        model.addAttribute("dayEndBranches", dayEndBranch);
        model.addAttribute("mBranchs", branchs);
        model.addAttribute("branchName", branchNames);
        model.addAttribute("dayEndBranchId", branchList);
        return new ModelAndView("dayEnd/confirmCommonDayEnd");
    }

    @RequestMapping(value = "/commonDayEndProcess", method = RequestMethod.GET)
    public ModelAndView commonDayEndProcess(Model model, HttpServletRequest request) {
        String successMsg = "", errorMsg = "", msg = "", nextMsg = "";
        String lastDay = "";
        String branch[] = request.getParameterValues("branches");
        String branches[] = branch[0].split(",");
        List<DayEndStatus> dayEndStatus = new ArrayList<>();
        List<DayEndProcess> jobs = dayEndService.findConfigJobs();
        List<MBranch> branchs = masterDataService.findBranches();
        List<DayEndDate> dayEndDates = dayEndService.dayEndProcessBranches();
        String user_Name = userService.findWorkingUserName();
        int count = 0;
        for (int i = 0; i < branches.length; i++) {
            int branchId = Integer.parseInt(branches[i]);
            msg = dayEndService.dayEndProcess(branchId);
            nextMsg = dayEndService.findLoanOverPayment(branchId);
            DayEndStatus status = new DayEndStatus();
            if (msg.equals("OK")) {
                if (nextMsg.equals("OK")) {
                    status.setBranchId(branchId);
                    status.setStatus(1);
                    count++;
                    dayEndStatus.add(status);
                    status = null;
                } else {
                    status.setBranchId(branchId);
                    status.setStatus(2);
                    dayEndStatus.add(status);
                    break;
                }
            } else {
                status.setBranchId(branchId);
                status.setStatus(3);
                dayEndStatus.add(status);
                break;
            }
            status = null;
        }

        List<DayEndStatus> stopDayendList = new ArrayList<>();
        List<DayEndStatus> dayendList = new ArrayList<>();
        int status = branches.length - dayEndStatus.size();
        if (status > 0) {
            DayEndStatus lastStatus = dayEndStatus.get(dayEndStatus.size() - 1);
            int lastBranch = lastStatus.getBranchId();
            for (int i = 1; i <= status; i++) {
                DayEndStatus endStatus = new DayEndStatus();
                endStatus.setBranchId(lastBranch + i);
                endStatus.setStatus(0);
                stopDayendList.add(endStatus);
                endStatus = null;
            }
            dayEndStatus.addAll(stopDayendList);

        }

        for (int i = 0; i < dayEndStatus.size(); i++) {
            DayEndStatus des = (DayEndStatus) dayEndStatus.get(i);
            if (des.getStatus() == 2) {
                errorMsg = nextMsg;
            } else if (des.getStatus() == 3) {
                errorMsg = msg;
            }
        }
        model.addAttribute("userName", user_Name);

        model.addAttribute("branchList", dayEndDates);
        model.addAttribute("allBranchs", branchs);
        model.addAttribute("dayEndStatus", dayEndStatus);
        model.addAttribute("warningMessage", errorMsg);
        if (count == branches.length) {
            model.addAttribute("successMessage", "Day End Process is Completed");
            lastDay = dayEndService.getLastDayEnd(1);
        }
        model.addAttribute("lastDay", "New Date : " + lastDay);
        return new ModelAndView("dayEnd/commonDayEndPage", "jobs", jobs);
    }

    @RequestMapping(value = "/loadCommonDayEnd", method = RequestMethod.GET)
    public ModelAndView commonDayEndPage(Model model, HttpServletRequest request) {
        List<DayEndProcess> jobs = null;
        String lastDay = "";
        String user_Name = "";
        List<MBranch> branchs = null;
        List<DayEndStatus> statuses = new ArrayList<>();
        List<DayEndDate> dayEndDates = null;
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        try {
            jobs = dayEndService.findConfigJobs();
            lastDay = dayEndService.getLastDayEnd(branchId);
            user_Name = userService.findWorkingUserName();
            dayEndDates = dayEndService.dayEndProcessBranches();
            branchs = masterDataService.findBranches();

            for (int i = 1; i <= dayEndDates.size(); i++) {
                DayEndStatus status = new DayEndStatus();
                status.setBranchId(i);
                status.setStatus(0);
                statuses.add(status);
                status = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("dayEndStatus", statuses);
        model.addAttribute("lastDay", lastDay);
        model.addAttribute("userName", user_Name);
        model.addAttribute("branchList", dayEndDates);
        model.addAttribute("allBranchs", branchs);
        return new ModelAndView("dayEnd/commonDayEndPage", "jobs", jobs);
    }

    @RequestMapping(value = "/postingCheq", method = RequestMethod.GET)
    public ModelAndView postingCheq(Model model, HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        String code = request.getParameter("code");
        boolean msg = dayEndService.postingCheque(code, id);
        if (msg) {
            return new ModelAndView("messages/success", "successMessage", "Posting Success");
        } else {
            return new ModelAndView("messages/warning", "warningMessage", "Posting Error");
        }
    }

    @RequestMapping(value = "/calLapsDate", method = RequestMethod.GET)
    public ModelAndView calculateLaspDate(Model model, HttpServletRequest request) {

        boolean msg = dayEndService.calLapsDate();
        try {
            if (msg) {
                return new ModelAndView("messages/success", "successMessage", "Succesfully Calculate Laps Date");
            }
            return new ModelAndView("messages/warning", "warningMessage", "Error Calculation");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("messages/success", "successMessage", "Succesfully Calculate Laps Date");

    }

    @RequestMapping(value = "/calNullInstallment", method = RequestMethod.GET)
    public ModelAndView calculateinstallment(Model model, HttpServletRequest request) {

        boolean msg = dayEndService.calNullInstallment();
        try {
            if (msg) {
                return new ModelAndView("messages/success", "successMessage", "Succesfully Calculate Laps Date");
            }
            return new ModelAndView("messages/warning", "warningMessage", "Error Calculation");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("messages/success", "successMessage", "Succesfully Calculate Laps Date");

    }

    @RequestMapping(value = "/calculateBalances", method = RequestMethod.GET)
    @ResponseBody
    public boolean calculateBalances(HttpServletRequest request) {
        boolean success = false;
        try {
            success = dayEndService.calculateBalances();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }
}

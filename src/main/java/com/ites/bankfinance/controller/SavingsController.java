package com.ites.bankfinance.controller;

import com.bankfinance.form.*;
import com.ites.bankfinance.savingsModel.DebtorHeaderDetails;
import com.ites.bankfinance.savingsModel.MSavingAccountHeader;
import com.ites.bankfinance.savingsModel.MSavingsType;
import com.ites.bankfinance.savingsModel.MSubSavingsType;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.SavingsDebtorService;
import com.ites.bankfinance.service.SavingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by AXA-FINANCE on 5/21/2018.
 */

@Controller
@RequestMapping(value = "/SavingsController")
//@Configuration
//@ComponentScan("com.ites.bankfinance.service")
public class SavingsController {


    @Autowired
    private SavingsService savingsService;

    @Autowired
    private MasterDataService masterService;

    @Autowired
    private SavingsDebtorService savingsDebtorService;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = "/saveOrUpdateSavingsType", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateSavingsType(@ModelAttribute("addAccountTypeForm") MSavingsType formData, BindingResult result, Model model) {
        String message = "";
        Boolean is_Saved = false;
        try {
            if (formData.getSavingsTypeId() == null) {
                is_Saved = savingsService.saveOrUpdateSavingsType(formData);
                if (is_Saved) {
                    message = "Successfully Saved";
                }
            } else {
                is_Saved = savingsService.saveOrUpdateSavingsType(formData);
                if (is_Saved) {
                    message = "Successfully Updated";
                }
            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Failed");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/saveOrUpdateSubSavingsType", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateSubSavingsType(@ModelAttribute("SubAccountTypeForm") MSubSavingsType formData, BindingResult result, Model model) {
        String message = "";
        Boolean is_Saved = false;
        try {
            if (formData.getSubSavingsId() == null) {
                is_Saved = savingsService.saveOrUpdateSubSavingsType(formData);
                if (is_Saved) {
                    message = "Successfully Saved";
                }
            } else {
                is_Saved = savingsService.saveOrUpdateSubSavingsType(formData);
                if (is_Saved) {
                    message = "Successfully Updated";
                }
            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Save Process Failed");
            return new ModelAndView("messages/error");
        }
        model.addAttribute("successMessage", message);
        return new ModelAndView("messages/success");
    }

    @RequestMapping(value = "/saveNewSavingsForm", method = RequestMethod.POST)
    public ModelAndView saveNewSavingsForm(@ModelAttribute("savingForm") SavingNewAccForm formData, Model model, HttpServletRequest request) {

        String saveUpdate = "";
        Boolean isSave = false;

        try {
            savingsService.saveNewSavingsData(formData);

            List<MSavingsType> savingsTypes = masterService.findAllSavingsTypes();
            List<MSubSavingsType> subSavingsTypes = masterService.findAllSubSavingsTypes();

            model.addAttribute("savingTypeList", savingsTypes);
            model.addAttribute("savingSubTypesList", subSavingsTypes);
            model.addAttribute("branchCode", formData.getBranchCodeId());
            model.addAttribute("edit", true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("savings/newSavingAccountForm");
    }


}

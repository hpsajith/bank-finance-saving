/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.OtherChargseModel;
import com.bankfinance.form.StlmntReturnList;
import com.ites.bankfinance.model.DebtorDeposit;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.InstallmentReturnList;
import com.ites.bankfinance.model.LoanCheques;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanRescheduleDetails;
import com.ites.bankfinance.model.MBankDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.RebateApprovals;
import com.ites.bankfinance.model.RebateDetail;
import com.ites.bankfinance.model.ReturnList;
import com.ites.bankfinance.model.SettlementAddNewPayment;
import com.ites.bankfinance.model.SettlementCharges;
import com.ites.bankfinance.model.SettlementMain;
import com.ites.bankfinance.model.SettlementPaymentDeleteForm;
import com.ites.bankfinance.model.SettlementPaymentDetailBankDeposite;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementPayments;
import com.ites.bankfinance.model.SettlementPreview;
import com.ites.bankfinance.model.SettlementReceiptDetail;
import com.ites.bankfinance.model.SettlementRefund;
import com.ites.bankfinance.model.SettlementTransfer;
import com.ites.bankfinance.model.SmsDetails;
import com.ites.bankfinance.model.SmsType;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.reportManager.DBFacade;
import com.ites.bankfinance.service.DayEndService;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.ReportService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Sajith
 */
@Controller
@RequestMapping(value = "/SettlementController")
public class SettlementController {

    private String newReceiptNo;
    private int printLoanId;

    private final String refNoReceptCode = "SETT";
    private final String refNoDownPymntSuspCode = "DWNS";
    private final String refNoDownPymntSuspCodeDscrpt = "Down Payment/Other Charge";
    private final String refArrearsCode = "ARRS";
    private final String refArrearsCodeDscrpt = "Arrears Payment";
    private final String refNoInsuaranceCode = "INSU";
    private final String refNoInsuaranceCodeDscrpt = "Insurance Payment";
    private final String transactionCode = "TRANS";
    private final String refNoInstallmentCodeDscrpt = "Installment";
    private final String refNoInstallmentCode = "INS";
    private final String refNoOverPayCode = "OVP";
    private final String refNoOverPayCodeDescrpt = "Over Pay";
    private final String refNoRentalInterCode = "RNTI";
    private final String refNoRentalDiffCode = "EXSS";
    private final String refNoRentalCode = "RNT";
    private final String refNoDownPaymentCode = "DWN";
    private final String refNoDownPaymentCodeDcsrpt = "Down Payment";
    private final String refNoOtherChargeCode = "OTHR";
    private final String refNoOtherChargeCodeDscrpt = "Other Charges";
    private final String refNoBalanceWithoutODICode = "WODI";
    //    private  final String refNoBalanceWithoutODICodeDscrpt = "Arrears Without ODI";
    private final String refNoBalanceWithoutODICodeDscrpt = "Arrears ";
    private final String refNoODICode = "ODI";
    private final String refNoODICodeDscrpt = "Over Due Charge";
    private final String refNoAdjustment = "ADJ";
    private final String rebatePaymentCode = "RBT";
    private final String rebatePaymentCodeDscept = "Rebate Payment";
    private final String loanInitial = "Initial Loan";

    private String description = "";
    private Date systemDate;

    @Autowired
    LoanService loanService;
    @Autowired
    MasterDataService masterDataService;
    @Autowired
    SettlmentService settlmentService;
    @Autowired
    UserService userService;
    @Autowired
    ReportService reportService;
    @Autowired
    DayEndService dayEndService;

    @RequestMapping(value = "/getBankDetailList", method = RequestMethod.GET)
    public @ResponseBody
    List<MBankDetails> getBankDetailList() {
        List<MBankDetails> mbankDetailList = settlmentService.findBankList();
        return mbankDetailList;
    }

    @RequestMapping(value = "/newSettlementForm", method = RequestMethod.GET)
    public ModelAndView newSettlementForm() {
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        systemDate = settlmentService.getSystemDate(userLogBranchId);
        return new ModelAndView("settlement/settlementCustomerDetail");
    }

    @RequestMapping(value = "/searchCustomer", method = RequestMethod.GET)
    public ModelAndView searchCustomer() {
        return new ModelAndView("settlement/searchCustomer");
    }

    @RequestMapping(value = "/searchByName/{name}", method = RequestMethod.GET)
    public ModelAndView searchByName(@PathVariable("name") String name, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByName(name, bID);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("settlement/viewSearchCustomer", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchByNic/{nicNo}", method = RequestMethod.GET)
    public ModelAndView searchByNicNo(@PathVariable("nicNo") String nicNo, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByNic(nicNo, bID);
        Boolean message = false;
        if (debtor.size() > 0) {
            message = true;
        }
        model.addAttribute("message", message);
        return new ModelAndView("settlement/viewSearchCustomer", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchByLoanNo", method = RequestMethod.GET)
    public ModelAndView searchByLoanNo(HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        String loanNo = request.getParameter("loanNo");
        List<DebtorHeaderDetails> debtor = loanService.findByLoanNo(loanNo, bID);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("settlement/viewSearchCustomer", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchByVehicleNo", method = RequestMethod.GET)
    public ModelAndView searchByVehicleNo(HttpServletRequest request, Model model) {

        String vehicleNo = request.getParameter("vehicleNo");
        List<DebtorHeaderDetails> debtor = loanService.findByVehicleNo(vehicleNo);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("settlement/viewSearchCustomer", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchLoanById/{id}", method = RequestMethod.GET)
    public ModelAndView searchLoanById(@PathVariable("id") String id, HttpServletRequest request, Model model) {
        int debtorId = Integer.parseInt(id);
        List<LoanHeaderDetails> loan = loanService.findLoansByDebtorId(debtorId);
        DebtorHeaderDetails debtor = loanService.findById(debtorId);
        List<MBranch> branchs = masterDataService.findBranches();

        boolean message = true;
        if (loan.isEmpty()) {
            message = false;
        }
        model.addAttribute("name", debtor.getDebtorName());
        model.addAttribute("message", message);
        model.addAttribute("branchList", branchs);
        return new ModelAndView("settlement/viewSearchLoans", "loanList", loan);
    }

    @RequestMapping(value = "/viewLoanDetails/{loanId}", method = RequestMethod.GET)
    public ModelAndView findLoanDetails(@PathVariable("loanId") int loanId, Model model) {

        LoanHeaderDetails loan = loanService.searchLoanByLoanId(loanId);
        List otherCharges = loanService.findOtherChargesByLoanId(loanId);
        String loanEndDate = this.calculateLoanEndDate(loan);
        String loanPeriodTypeId = loanService.findLoanPeriodType(loan.getLoanPeriodType());

        model.addAttribute("charges", otherCharges);
        model.addAttribute("LoanPeriodType", loanPeriodTypeId);
        model.addAttribute("loanEndDate", loanEndDate);
        model.addAttribute("Total", loan.getLoanInterest() + loan.getLoanInstallment());

        return new ModelAndView("settlement/viewLoan", "loan", loan);
    }

    private String calculateLoanEndDate(LoanHeaderDetails loan) {
        String endDate = "";
        try {
            Date loanDate = loan.getLoanDate();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            c.setTime(loanDate);

            if (loan.getLoanPeriodType() == 1) {
                c.add(Calendar.MONTH, loan.getLoanPeriod());
            }
            if (loan.getLoanPeriodType() == 2) {
                c.add(Calendar.YEAR, loan.getLoanPeriod());
            }
            if (loan.getLoanPeriodType() == 3) {
                c.add(Calendar.DATE, loan.getLoanPeriod());
            }
            endDate = sdf.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return endDate;
    }

    @RequestMapping(value = "/addSettlement/{loanId}", method = RequestMethod.GET)
    public ModelAndView addSettlement(@PathVariable("loanId") int loanId, Model model) {

        List otherChargesList = loanService.findOtherChargesByLoanId(loanId);//from loan other charges
        List<InstallmentReturnList> installmentsList = settlmentService.getDebitCreditInstallmentList(loanId);
        List<ReturnList> openningBalanceList = settlmentService.findOpenningBalance(loanId);
        SettlementAddNewPayment formData = new SettlementAddNewPayment();
        List<InstallmentReturnList> adjustmentList = settlmentService.findAdjustmentList(loanId, refNoAdjustment);

        double totalOtherCharges = settlmentService.getSumOfOtherCherges(otherChargesList);
        double totalOpenningD = 0.00;
        if (openningBalanceList != null) {
            totalOpenningD = settlmentService.getSumOfOpenningBalance(openningBalanceList);
        }

        double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(loanId);

        double paidArrears = settlmentService.getPaidArrears(loanId); //done
//        Disable Other Charges Change 
        double paidOtherCharges = 0.00;//done
//        paidOtherCharges = settlmentService.getPaidOtherCharges(loanId);//done//Annr
        double paidDownPayment = settlmentService.getPaidDownPayment(loanId);//done
        double paidInsurance = settlmentService.getPaidInsuarance(loanId);//done
        double overPayAmount = settlmentService.getOverPayAmount(loanId);

        double totalDownPayment = settlmentService.getTotalDownPayment(loanId);//from loan header detail
        double totalInsurance = settlmentService.getTotalInsuarance(loanId);
        double installmentBalance = settlmentService.getInstalmentBalance(loanId);
        double odiBalance = settlmentService.getOdiBalance(loanId);
        double adjustmntBalance = settlmentService.getAdjustmntBalance(loanId, refNoAdjustment);
        double rebateBalance = settlmentService.getRebateBalance(loanId);

        List payTypes = settlmentService.findPayTypes();
        List mBankDetailList = settlmentService.findBankList();
//        double totalCharges = settlmentService.findTotalCharges(loanId);
//        double totalPayment = settlmentService.findTotalPayment(loanId);
//        double totalBalance =totalCharges - totalPayment;

//        double OtherChargesBalance = totalOtherCharges -paidOtherCharges;
        double OtherChargesBalance = -paidOtherCharges;
//        double intialDownPayment = OtherChargesBalance + totalOtherCharges + totalDownPayment;
        double intialDownPayment = OtherChargesBalance + totalDownPayment;
        double downPaymentBalance = OtherChargesBalance + totalDownPayment - paidDownPayment;
        double totalOpeningBalance = (totalOpenningD + openningBalanceWithoutODI) - paidArrears;
        double insuranceBalance = -paidInsurance;
        paidInsurance = totalInsurance - insuranceBalance;
        int instalmentCount = settlmentService.findPaidInstallmentCount(loanId);

        boolean msgOther = false;
        boolean msgOpenBal = false;
        boolean msgInstallment = false;
        boolean msgDownPayment = false;
        boolean msgInsurance = false;
        boolean msgOverPay = false;
        boolean msgOdiBalance = false;
        boolean msgRebate = false;

        if (0 < downPaymentBalance) {
            msgDownPayment = true;
        }
        if (0 < OtherChargesBalance || 0 < downPaymentBalance) {
            msgOther = true;
        }
        if (0 < totalOpeningBalance) {
            msgOpenBal = true;
        }
        if (0 < insuranceBalance) {
            msgInsurance = true;
        }
        if (!installmentsList.isEmpty()) {
            msgInstallment = true;
        }
        if (0 < overPayAmount) {
            msgOverPay = true;
        }
        if (0 < odiBalance) {
            msgOdiBalance = true;
        }
        if (0 < rebateBalance) {
            msgRebate = true;
        }

        double totalOpenning = totalOpenningD + openningBalanceWithoutODI;

        model.addAttribute("msgOther", msgOther);
        model.addAttribute("msgOpenBal", msgOpenBal);
        model.addAttribute("msgDownPayment", msgDownPayment);
        model.addAttribute("msgInstallment", msgInstallment);
        model.addAttribute("msgInsurance", msgInsurance);
        model.addAttribute("msgOverPay", msgOverPay);
        model.addAttribute("msgOdiBalance", msgOdiBalance);
        model.addAttribute("msgRebate", msgRebate);

//        model.addAttribute("totalCharges",totalCharges);
//        model.addAttribute("totalPayment",totalPayment);       
//        model.addAttribute("totalBalance",totalBalance);
        model.addAttribute("otherChargesList", otherChargesList);
        model.addAttribute("openningBalanceList", openningBalanceList);
        model.addAttribute("installmentsList", installmentsList);
        model.addAttribute("adjustmentList", adjustmentList);
        model.addAttribute("intialDownPayment", new BigDecimal(intialDownPayment).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("totalDownPayment", new BigDecimal(totalDownPayment).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("totalOtherCharges", new BigDecimal(totalOtherCharges).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("totalOpenning", new BigDecimal(totalOpenning).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("totalInsurance", new BigDecimal(totalInsurance).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("openningBalanceWithoputODI", new BigDecimal(openningBalanceWithoutODI).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("overPayAmount", new BigDecimal(overPayAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("paidDownPayment", new BigDecimal(paidDownPayment).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("paidOtherCharges", new BigDecimal(paidOtherCharges).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("paidArrears", new BigDecimal(paidArrears).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("paidInsurance", new BigDecimal(paidInsurance).setScale(2, RoundingMode.HALF_UP).doubleValue());

        model.addAttribute("OtherChargesBalance", new BigDecimal(OtherChargesBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("totalOpeningBalance", new BigDecimal(totalOpeningBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("downPaymentBalance", new BigDecimal(downPaymentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("insuranceBalance", new BigDecimal(insuranceBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());

        double balance = totalOpeningBalance + downPaymentBalance + insuranceBalance + installmentBalance + adjustmntBalance;

        model.addAttribute("balance", new BigDecimal(balance).setScale(2, RoundingMode.HALF_UP).doubleValue());
        rebateBalance = rebateBalance + balance;
        model.addAttribute("rebateBalance", new BigDecimal(rebateBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
        model.addAttribute("instalmentCount", instalmentCount);
        model.addAttribute("loanId", loanId);
        model.addAttribute("payTypes", payTypes);
        model.addAttribute("mBankDetailList", mBankDetailList);
        model.addAttribute("formData", formData);

        return new ModelAndView("settlement/addSettlement");
    }

    @RequestMapping(value = "/viewPreview", method = RequestMethod.GET)
    public ModelAndView viewPreview(@ModelAttribute("newPaymentForm") SettlementAddNewPayment formData, Model model, HttpServletRequest request) {
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        systemDate = settlmentService.getSystemDate(userLogBranchId);

        String newTransactionNo = settlmentService.getTransactionIdMax(transactionCode);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date dt = new java.util.Date();

        int paymentTypeId[] = formData.getPaymentTypeId();
        double Amount[] = formData.getAmount();
        int loanId = formData.getLoanId();
        Integer BankId[] = formData.getBankId();
        String amountText[] = formData.getAmountText();
        String AccountNo[] = formData.getAccNo();
        String TransDate[] = formData.getTransDate();

        DebtorHeaderDetails debtorHeaderDetail = settlmentService.findDebtorByLoanId(loanId);

        String accNo = null;
        String invoiceNo = null;
        String receiptNo = null;

        double totalPayAmount = 0;

        receiptNo = settlmentService.getReceiptNo(refNoReceptCode);
        String serielNo = settlmentService.getMaxSerialNo();

        // **********************#########************************
        SettlementPreview settlementPreview = addSettlementMainPreview(formData, loanId, newTransactionNo, debtorHeaderDetail, userId, receiptNo);
        //***********************#########************************

        for (int i = 0; i < paymentTypeId.length; i++) {
            if (paymentTypeId[i] == 1) {
                totalPayAmount = totalPayAmount + Amount[i];
            } else if (paymentTypeId[i] == 2) {
                totalPayAmount = totalPayAmount + Amount[i];
            } else if (paymentTypeId[i] == 3) {
                totalPayAmount = totalPayAmount + Amount[i];
            }
        }

//        System.out.println("***********************#########************************");
//        System.out.println("receiptNo: " + receiptNo);
//        System.out.println(refNoDownPymntSuspCodeDscrpt + ":" + settlementPreview.getDownPymntSuspCodeDscrptAmount());
//        System.out.println(refArrearsCodeDscrpt + ":" + settlementPreview.getArrearsAmount());
//        System.out.println(refNoOtherChargeCodeDscrpt + ":" + settlementPreview.getOtherChargeCodeDscrptAmount());
//        System.out.println(refNoInsuaranceCodeDscrpt + ":" + settlementPreview.getInsuaranceCodeDscrptAmount());
//        System.out.println(refNoODICodeDscrpt + ":" + settlementPreview.getPaidCurrentOdi());
//        System.out.println(refNoInstallmentCodeDscrpt + ":" + settlementPreview.getInstallmentCodeDscrptAmount());
        boolean receiptadded = false;

        newReceiptNo = receiptNo;
        printLoanId = loanId;

        double balance = getBalance(loanId) - totalPayAmount;
        if (0.00 <= balance) {
            model.addAttribute("balance", balance);
        } else {
            model.addAttribute("balance", 0.00);
        }
        model.addAttribute("receiptNo", receiptNo);
        model.addAttribute("loanId", loanId);
        model.addAttribute("settlementPreview", settlementPreview);
        return new ModelAndView("settlement/settlementPreview");
    }

    @RequestMapping(value = "/saveAddPayment", method = RequestMethod.GET)
    public ModelAndView addPayment(@ModelAttribute("newPaymentForm") SettlementAddNewPayment formData, Model model, HttpServletRequest request) {

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }

        int userId = userService.findByUserName();
        //int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        systemDate = settlmentService.getSystemDate(branchId);

        //AccNo for both ChequeNo & DepositeAccountNo
        //TransDate for both RealizeDate & DepositeDate
//        Object branchIdObj = request.getSession().getAttribute("branchId");
//        int branchId = Integer.parseInt(branchIdObj.toString());
//        formData.setBranchId(branchId);
//        System.out.println("Branch Id: "+branchId.toString());
        String newTransactionNo = settlmentService.getTransactionIdMax(transactionCode);

        //dont remove
//        boolean isAddedPaymentDetail = settlmentService.addSettlmentPaymentDetails(formData,newTransactionNo,userId);    
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date dt = new java.util.Date();

        int paymentTypeId[] = formData.getPaymentTypeId();
        double Amount[] = formData.getAmount();
        int loanId = formData.getLoanId();
        Integer BankId[] = formData.getBankId();
        String amountText[] = formData.getAmountText();
        String AccountNo[] = formData.getAccNo();
        String TransDate[] = formData.getTransDate();
        int paymentType = 0;
//        Integer installmentId[] = formData.getInstallmentId();
//        double installmentAmount[] = formData.getInstallmentAmount();
        SettlementPaymentDetailCheque settlementPaymentDetailCheque = new SettlementPaymentDetailCheque();
        SettlementPaymentDetailBankDeposite settlementPaymentDetailBankDeposite = new SettlementPaymentDetailBankDeposite();
        SettlementPayments settlementPayments = new SettlementPayments();
        SettlementReceiptDetail settlementReceiptDetail = new SettlementReceiptDetail();
        LoanHeaderDetails loanDetail = loanService.findLoanHeaderByLoanId(loanId);
        DebtorHeaderDetails debtorHeaderDetail = settlmentService.findDebtorByLoanId(loanId);

        String accNo = null;
        String invoiceNo = null;
        String receiptNo = null;

        double totalPayAmount = 0;

        receiptNo = settlmentService.getReceiptNo(refNoReceptCode);
        String serielNo = settlmentService.getMaxSerialNo();
        formData.setBranchId(branchId);

        // **********************#########************************
        boolean settlementStatus = addSettlementMain(formData, loanId, newTransactionNo, debtorHeaderDetail, userId, receiptNo);
        //***********************#########************************

        for (int i = 0; i < paymentTypeId.length; i++) {
            int detailId1 = 0;
            int detailId2 = 0;
            int detailId3 = 0;
            if (paymentTypeId[i] == 1) {
                paymentType = 1;
                boolean status = settlmentService.addCashPaymentDetails(userId, Amount[i], newTransactionNo);
                if (status) {
                    detailId1 = settlmentService.getCashPaymentDetailsId();
                    accNo = settlmentService.getAccountNo(paymentTypeId[i]);
                    settlementPayments.setAccountNo(accNo);
                    settlementPayments.setPaymentAmount(Amount[i]);
                    settlementPayments.setAmountText(amountText[i]);
                    settlementPayments.setBranchId(formData.getBranchId());
                    settlementPayments.setDebtorId(debtorHeaderDetail.getDebtorId());
                    settlementPayments.setLoanId(loanId);
                    settlementPayments.setPaymentDetails(detailId1);
                    settlementPayments.setPaymentType(paymentTypeId[i]);
                    settlementPayments.setReceiptNo(receiptNo);
                    settlementPayments.setTransactionNo(newTransactionNo);
                    settlementPayments.setUserId(userId);
                    settlementPayments.setRemark(formData.getRemark());
                    settlementPayments.setPaymentDate(dt);
                    settlementPayments.setSystemDate(systemDate);
                    settlementPayments.setIsDownPayment(formData.getIsDownPayment() ? 1 : 0);
                    settlmentService.addSettlmentPayment(settlementPayments);

                }
                totalPayAmount = totalPayAmount + Amount[i];
            } else if (paymentTypeId[i] == 2) {
                settlementPaymentDetailCheque.setAmount(Amount[i]);
                settlementPaymentDetailCheque.setBank(BankId[i]);
                settlementPaymentDetailCheque.setChequeNo(AccountNo[i]);
                try {
                    settlementPaymentDetailCheque.setRealizeDate(sdf.parse(TransDate[i]));
                } catch (ParseException ex) {
                    Logger.getLogger(SettlementController.class.getName()).log(Level.SEVERE, null, ex);
                }
                settlementPaymentDetailCheque.setTransactionDate(dt);
                settlementPaymentDetailCheque.setUserId(userId);
                settlementPaymentDetailCheque.setIsReturn(0);
                settlementPaymentDetailCheque.setTransactionDate(systemDate);
                settlementPaymentDetailCheque.setTransactionNo(newTransactionNo);
                //update loan cheque
                if (formData.getPdChequeId() != 0) {
                    settlmentService.setPaidLoanCheque(formData.getPdChequeId(), loanId);
                }
                boolean status2 = settlmentService.addChequePaymentDetails(settlementPaymentDetailCheque);
                if (status2) {
                    detailId2 = settlmentService.getChequePaymentDetailsId();
                    accNo = settlmentService.getAccountNo(paymentTypeId[i]);

                    settlementPayments.setAccountNo(accNo);
                    settlementPayments.setPaymentAmount(Amount[i]);
                    settlementPayments.setAmountText(amountText[i]);
                    settlementPayments.setBranchId(formData.getBranchId());
                    settlementPayments.setDebtorId(debtorHeaderDetail.getDebtorId());
                    settlementPayments.setLoanId(loanId);
                    settlementPayments.setPaymentDetails(detailId2);
                    settlementPayments.setPaymentType(paymentTypeId[i]);
                    settlementPayments.setReceiptNo(receiptNo);
                    settlementPayments.setTransactionNo(newTransactionNo);
                    settlementPayments.setUserId(userId);
                    settlementPayments.setPaymentDate(dt);
                    settlementPayments.setRemark(formData.getRemark());
                    settlementPayments.setSystemDate(systemDate);
                    settlementPayments.setIsDownPayment(formData.getIsDownPayment() ? 1 : 0);
                    settlmentService.addSettlmentPayment(settlementPayments);
                }
                totalPayAmount = totalPayAmount + Amount[i];
            } else if (paymentTypeId[i] == 3) {
                paymentType = 3;
                settlementPaymentDetailBankDeposite.setAmount(Amount[i]);
                settlementPaymentDetailBankDeposite.setBankId(BankId[i]);
                settlementPaymentDetailBankDeposite.setDepositeAccountNo(AccountNo[i]);
                try {
                    settlementPaymentDetailBankDeposite.setDepositeDate(sdf.parse(TransDate[i]));
                } catch (ParseException ex) {
                    Logger.getLogger(SettlementController.class.getName()).log(Level.SEVERE, null, ex);
                }
                settlementPaymentDetailBankDeposite.setTransactionNo(newTransactionNo);
                settlementPaymentDetailBankDeposite.setLoanId(loanId);
                settlementPaymentDetailBankDeposite.setUserId(userId);

                boolean status = settlmentService.addBankDepositePaymentDetails(settlementPaymentDetailBankDeposite);
                if (status) {
                    detailId3 = settlmentService.getBankDepositePaymentDetailsId();

                    accNo = settlmentService.getAccountNo(paymentTypeId[i]);

                    settlementPayments.setAccountNo(accNo);
                    settlementPayments.setPaymentAmount(Amount[i]);
                    settlementPayments.setAmountText(amountText[i]);
                    settlementPayments.setBranchId(formData.getBranchId());
                    settlementPayments.setDebtorId(debtorHeaderDetail.getDebtorId());
                    settlementPayments.setLoanId(loanId);
                    settlementPayments.setPaymentDetails(detailId3);
                    settlementPayments.setPaymentType(paymentTypeId[i]);
                    settlementPayments.setReceiptNo(receiptNo);
                    settlementPayments.setTransactionNo(newTransactionNo);
                    settlementPayments.setUserId(userId);
                    settlementPayments.setPaymentDate(dt);
                    settlementPayments.setRemark(formData.getRemark());
                    settlementPayments.setSystemDate(systemDate);
                    settlementPayments.setIsDownPayment(formData.getIsDownPayment() ? 1 : 0);
                    settlmentService.addSettlmentPayment(settlementPayments);
                }
                totalPayAmount = totalPayAmount + Amount[i];
            }
        }
        settlementReceiptDetail.setAmount(0.00);
        settlementReceiptDetail.setBalance(0.00);
        settlementReceiptDetail.setInvNo(invoiceNo);
        settlementReceiptDetail.setInvDate(dt);
        settlementReceiptDetail.setPayment(totalPayAmount);
        settlementReceiptDetail.setRcptNo(receiptNo);
        settlementReceiptDetail.setSerielNo(serielNo);
        settlementReceiptDetail.setSystemDate(systemDate);
        settlementReceiptDetail.setDescription(description);

        boolean receiptadded = false;

        newReceiptNo = receiptNo;
        printLoanId = loanId;

        receiptadded = settlmentService.addSettlementReceiptDetail(settlementReceiptDetail);

        description = "";

        // update debtor deposit as paid
        if (debtorHeaderDetail != null) {
            settlmentService.updateDebtorDeposit(debtorHeaderDetail.getDebtorId(), debtorHeaderDetail.getDebtorAccountNo());
        }

        //save SMS option
        if (formData.getIsInstalment()) {
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");//dd/MM/yyyy
            Date now = new Date();
            String date = sdfDate.format(now);

            String message = "";
            if (loanDetail.getDebtorHeaderDetails().getDebtorTelephoneMobile() != null && !"".equals(loanDetail.getDebtorHeaderDetails().getDebtorTelephoneMobile())) {
                if (loanDetail.getLoanAgreementNo() != null) {
                    message = "DEAR VALUED CUSTOMER,THANKS FOR PAYMENT RS." + totalPayAmount + " TO- " + loanDetail.getLoanAgreementNo() + ", RECIPT NO- " + receiptNo + " AT " + date + ".FOR DETAILS-0114323208.CRM WIJITHA FINANCE LIMITED.";
                } else {
                    message = "DEAR VALUED CUSTOMER,THANKS FOR PAYMENT RS." + totalPayAmount + ", RECIPT NO- " + receiptNo + " AT " + date + ".FOR DETAILS-0114323208.CRM WIJITHA FINANCE LIMITED.";
                }
                SmsDetails smsDetails = new SmsDetails();
                smsDetails.setActionTime(dt);
                smsDetails.setBranchId(formData.getBranchId());
                smsDetails.setIsDelete(false);
                smsDetails.setIsSendTo(false);
                smsDetails.setSmsDebtorName(loanDetail.getDebtorHeaderDetails().getDebtorName());
                smsDetails.setSmsDebtorNumber(Integer.parseInt(loanDetail.getDebtorHeaderDetails().getDebtorTelephoneMobile()));
                smsDetails.setSmsLoanId(loanId);
                smsDetails.setSmsReceiptNo(receiptNo);
                smsDetails.setSmsType(new SmsType(0));
                smsDetails.setSmsDescription(message);
                settlmentService.saveSMS(smsDetails);

            }
        }
        ModelAndView mdl = addSettlement(loanId, model);
//      return new ModelAndView("settlement/settlementCustomerDetail");
        return mdl;
    }

    private boolean addSettlementMain(SettlementAddNewPayment formData, int loanId, String newTransactionNo, DebtorHeaderDetails debtor, int userid, String receiptNo) {

        double totalPayment = 0;
//    List<SettlementRefCodeConfig>  refCodeD = settlmentService.getRefCode();
        double payAmounts[] = formData.getAmount();
        int payTypeId[] = formData.getPaymentTypeId();
        boolean status = false;
        double updateOverPayAmount = 0;
        double overPayAmount = formData.getOverPayAmount();

        for (int i = 0; i < formData.getPaymentTypeId().length; i++) {
            totalPayment = totalPayment + payAmounts[i];
        }

        //***************Arrears payment*************
        if (formData.getIsArreas()) {
            List<ReturnList> openningBalance = settlmentService.findOpenningBalance(loanId);
            double sumOpenningBalance = 0.00;
            if (openningBalance != null) {
                sumOpenningBalance = settlmentService.getSumOfOpenningBalance(openningBalance);
            }

            double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(loanId);
            double paidArrears = settlmentService.getPaidArrears(loanId);

            double totalOpening = (sumOpenningBalance + openningBalanceWithoutODI) - paidArrears;
            if (0 < totalOpening) {

                //            String code =refArrearsCode;
                //            postedAmount = configPosting(formData,loanId,totalPayment,postedAmount,false,true,code);
                double payAmount = 0;
                if (totalOpening <= totalPayment) {
                    payAmount = totalOpening;
                } else if (totalPayment < totalOpening && 0 < totalPayment) {
                    payAmount = totalPayment;
                }

                SettlementMain settlementMain = new SettlementMain();
                settlementMain.setTransactionCodeCrdt(refArrearsCode);
                settlementMain.setDescription(refArrearsCodeDscrpt);
                settlementMain.setTransactionNoCrdt(newTransactionNo);
                settlementMain.setCrdtAmount(payAmount);
                settlementMain.setLoanId(loanId);
                settlementMain.setDebtorId(debtor.getDebtorId());
                settlementMain.setUserId(userid);
                settlementMain.setReceiptNo(receiptNo);
                settlementMain.setIsPosting(false);
                settlementMain.setBranchId(formData.getBranchId());
                settlementMain.setSystemDate(systemDate);

                status = settlmentService.addSettlementMain(settlementMain);

                double postChargeAmount = payAmount;

                for (int j = 0; 0 < postChargeAmount; j++) {
                    SettlementCharges settlementCharges = new SettlementCharges();

                    settlementCharges.setBillNo(receiptNo);
                    settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                    settlementCharges.setDebtorId(debtor.getDebtorId());
                    settlementCharges.setLoanId(loanId);
                    settlementCharges.setReffNo(newTransactionNo);
                    settlementCharges.setUserId(userid);
                    settlementCharges.setSystemDate(systemDate);
                    settlementCharges.setBranchId(formData.getBranchId());

                    if (sumOpenningBalance + openningBalanceWithoutODI <= postChargeAmount) {

                        settlementCharges.setCharge(openningBalanceWithoutODI);
                        settlementCharges.setChargeType(refNoBalanceWithoutODICode);
                        description = description + refNoBalanceWithoutODICodeDscrpt;

                        status = settlmentService.addChargeDetails(settlementCharges);
                        postChargeAmount = postChargeAmount - openningBalanceWithoutODI;
                        double paidOtherChar = sumOpenningBalance;
                        for (int i = 0; 0 < paidOtherChar; i++) {
                            settlementCharges.setCharge(openningBalance.get(i).getAmount());
                            settlementCharges.setChargeType(refArrearsCode + "-" + openningBalance.get(i).getArreasTypeId());
                            description = description + "," + openningBalance.get(i).getArreasDescription();

                            postChargeAmount = postChargeAmount - openningBalance.get(i).getAmount();
                            paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();

                            status = settlmentService.addChargeDetails(settlementCharges);
                        }
                    } else if (sumOpenningBalance < postChargeAmount && postChargeAmount < (openningBalanceWithoutODI + sumOpenningBalance)) {

                        if (sumOpenningBalance < postChargeAmount) {
                            double paidOtherChar = sumOpenningBalance;
                            for (int i = 0; 0 < paidOtherChar; i++) {
                                if (openningBalance.get(i).getAmount() <= paidOtherChar) {
                                    settlementCharges.setCharge(openningBalance.get(i).getAmount());
                                    paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();
                                } else {
                                    settlementCharges.setCharge(paidOtherChar);
                                    paidOtherChar = 0;
                                }

                                settlementCharges.setChargeType(refArrearsCode + "-" + openningBalance.get(i).getArreasTypeId());
                                description = description + "," + openningBalance.get(i).getArreasDescription();

                                status = settlmentService.addChargeDetails(settlementCharges);
                            }
                        }

                        settlementCharges.setCharge(postChargeAmount - sumOpenningBalance);
                        settlementCharges.setChargeType(refNoBalanceWithoutODICode);
                        description = description + "," + refNoBalanceWithoutODICodeDscrpt;

                        status = settlmentService.addChargeDetails(settlementCharges);
                        postChargeAmount = 0;

                    } else if (sumOpenningBalance < postChargeAmount && postChargeAmount < openningBalanceWithoutODI) {

                        if (sumOpenningBalance < postChargeAmount) {
                            double paidOtherChar = sumOpenningBalance;
                            for (int i = 0; 0 < paidOtherChar; i++) {
                                if (openningBalance.get(i).getAmount() <= paidOtherChar) {
                                    settlementCharges.setCharge(openningBalance.get(i).getAmount());
                                    paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();
                                } else {
                                    settlementCharges.setCharge(paidOtherChar);
                                    paidOtherChar = 0;
                                }

                                settlementCharges.setChargeType(refArrearsCode + "-" + openningBalance.get(i).getArreasTypeId());
                                description = description + "," + openningBalance.get(i).getArreasDescription();

                                status = settlmentService.addChargeDetails(settlementCharges);
                            }
                        }

                        settlementCharges.setCharge(postChargeAmount - sumOpenningBalance);
                        settlementCharges.setChargeType(refNoBalanceWithoutODICode);
                        description = description + "," + refNoBalanceWithoutODICodeDscrpt;

                        status = settlmentService.addChargeDetails(settlementCharges);
                        postChargeAmount = 0;

                    } else {
                        double paidOtherChar = postChargeAmount;
                        for (int i = 0; 0 < paidOtherChar; i++) {
                            if (openningBalance.get(i).getAmount() <= paidOtherChar) {
                                settlementCharges.setCharge(openningBalance.get(i).getAmount());
                                paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();
                            } else {
                                settlementCharges.setCharge(paidOtherChar);
                                paidOtherChar = 0;
                            }

                            settlementCharges.setChargeType(refArrearsCode + "-" + openningBalance.get(i).getArreasTypeId());
                            description = description + "," + openningBalance.get(i).getArreasDescription();

                            postChargeAmount = postChargeAmount - openningBalance.get(i).getAmount();

                            status = settlmentService.addChargeDetails(settlementCharges);
                        }
                    }
                }

//                updateOverPayAmount = overPayAmount + totalPayment - payAmount;
                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;

            }
        }

        //******************Down payments***************
        if (formData.getIsDownPayment()) {
            //        double downPaymentBalance = settlmentService.getDownPaymentBalance(loanId);
//            List<OtherChargseModel> otherChargesList = loanService.findOtherChargesByLoanId(loanId);
            List<OtherChargseModel> otherChargesList = settlmentService.findOtherChargesByLoanId(loanId);
            double totalOtherCharges = settlmentService.getSumOfOtherCherges(otherChargesList);
            double paidOtherCharges = settlmentService.getPaidOtherCharges(loanId);
//            double OtherChargesBalance = totalOtherCharges - paidOtherCharges;
            double OtherChargesBalance = -paidOtherCharges;
            double totalDownPayment = settlmentService.getTotalDownPayment(loanId);
            double paidDownPayment = settlmentService.getPaidDownPayment(loanId);
//            double downPaymentBalance = OtherChargesBalance + totalDownPayment - paidDownPayment;
            double downPaymentBalance = totalDownPayment - paidDownPayment;

            double initialDownPayment = totalOtherCharges + totalDownPayment;
            if (0 < downPaymentBalance) {

                double payAmount = 0;
                if (downPaymentBalance <= totalPayment) {
                    payAmount = downPaymentBalance;
                } else if (totalPayment < downPaymentBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                }

                SettlementMain settlementMain = new SettlementMain();
                settlementMain.setTransactionCodeCrdt(refNoDownPymntSuspCode);
                settlementMain.setDescription(refNoDownPymntSuspCodeDscrpt);
                settlementMain.setTransactionNoCrdt(newTransactionNo);
                settlementMain.setCrdtAmount(payAmount);
                settlementMain.setLoanId(loanId);
                settlementMain.setDebtorId(debtor.getDebtorId());
                settlementMain.setUserId(userid);
                settlementMain.setReceiptNo(receiptNo);
                settlementMain.setIsPosting(false);
                settlementMain.setSystemDate(systemDate);
                settlementMain.setBranchId(formData.getBranchId());

                status = settlmentService.addSettlementMain(settlementMain);

                double postChargeAmount = payAmount;

                SettlementCharges settlementCharges = new SettlementCharges();

                settlementCharges.setBillNo(receiptNo);
                settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                settlementCharges.setDebtorId(debtor.getDebtorId());
                settlementCharges.setLoanId(loanId);
                settlementCharges.setReffNo(newTransactionNo);
                settlementCharges.setUserId(userid);
                settlementCharges.setSystemDate(systemDate);
                settlementCharges.setBranchId(formData.getBranchId());

                settlementCharges.setCharge(postChargeAmount);
                settlementCharges.setChargeType(refNoDownPaymentCode);
                description = description + "," + refNoDownPaymentCodeDcsrpt;

                postChargeAmount = postChargeAmount - postChargeAmount;
                status = settlmentService.addChargeDetails(settlementCharges);

                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;

                status = settlmentService.updateLoanHeaderIsDownPaymntPaid(loanId, initialDownPayment);
            }

            if (OtherChargesBalance > 0) {
                double payAmount = 0;
                if (OtherChargesBalance <= totalPayment) {
                    payAmount = OtherChargesBalance;
                } else if (totalPayment < OtherChargesBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                }
                boolean isInitialPaymentDone = settlmentService.isInitialPaymentDone(loanId);

                SettlementMain settlementMain = new SettlementMain();
                settlementMain.setTransactionCodeCrdt(refNoOtherChargeCode);
                if (isInitialPaymentDone) {
                    settlementMain.setDescription(refNoOtherChargeCodeDscrpt);
                } else {
                    settlementMain.setDescription(loanInitial);
                }
                settlementMain.setTransactionNoCrdt(newTransactionNo);
                settlementMain.setCrdtAmount(payAmount);
                settlementMain.setLoanId(loanId);
                settlementMain.setDebtorId(debtor.getDebtorId());
                settlementMain.setUserId(userid);
                settlementMain.setReceiptNo(receiptNo);
                settlementMain.setIsPosting(false);
                settlementMain.setSystemDate(systemDate);
                settlementMain.setBranchId(formData.getBranchId());

                status = settlmentService.addSettlementMain(settlementMain);

                double postChargeAmount = payAmount;

                double initialOtherCharge = 0;
                if (otherChargesList != null) {
                    for (int i = 0; i < otherChargesList.size(); i++) {
                        if (otherChargesList.get(i).getIsDownPaymentCharge() == 1) {
                            initialOtherCharge = initialOtherCharge + otherChargesList.get(i).getAmount();
                        }
                    }
                    if (initialOtherCharge <= postChargeAmount) {
                        status = settlmentService.updateLoanHeaderIsDownPaymntPaid(loanId, initialOtherCharge, "OTHR");
                    }
                }

                double paidOtherChar = postChargeAmount;
                for (int i = 0; 0 < paidOtherChar && i < otherChargesList.size(); i++) {
                    if (otherChargesList.get(i).getIsPaid() == 0) {
                        SettlementCharges settlementCharges = new SettlementCharges();
                        settlementCharges.setBillNo(receiptNo);
                        settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                        settlementCharges.setDebtorId(debtor.getDebtorId());
                        settlementCharges.setLoanId(loanId);
                        settlementCharges.setReffNo(newTransactionNo);
                        settlementCharges.setUserId(userid);
                        settlementCharges.setSystemDate(systemDate);
                        settlementCharges.setBranchId(formData.getBranchId());
                        if (otherChargesList.get(i).getAmount() <= paidOtherChar) {
                            settlementCharges.setCharge(otherChargesList.get(i).getAmount());
                            postChargeAmount = postChargeAmount - otherChargesList.get(i).getAmount();
                            paidOtherChar = paidOtherChar - otherChargesList.get(i).getAmount();
                        } else {
                            settlementCharges.setCharge(paidOtherChar);
                            postChargeAmount = postChargeAmount - paidOtherChar;
                            paidOtherChar = 0;
                        }
                        settlementCharges.setChargeType(refNoOtherChargeCode + "-" + otherChargesList.get(i).getId());
                        description = description + "," + otherChargesList.get(i).getDescription();

                        status = settlmentService.updatePaidStatus(otherChargesList.get(i).getChargeID());

                        status = settlmentService.addChargeDetails(settlementCharges);
                    }
                }

                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;
            }
        }
        //******************Insurance payments***************

        if (formData.getIsInsurance()) {
            double paidInsurance = settlmentService.getPaidInsuarance(loanId);
            double totalInsurance = settlmentService.getTotalInsuarance(loanId);
//            double insuranceBalance = totalInsurance - paidInsurance;
            double insuranceBalance = -paidInsurance;
            if (0 < insuranceBalance) {
                double payAmount = 0;
                int fullPaid = 3;
                int halfPaid = 2;

                if (insuranceBalance <= totalPayment) {
                    payAmount = insuranceBalance;
                    status = settlmentService.updateInsuaranceStatus(loanId, fullPaid);
                    status = settlmentService.updateInsuaranceProcessIsPay(loanId);
                } else if (totalPayment < insuranceBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                    status = settlmentService.updateInsuaranceStatus(loanId, halfPaid);
                    status = settlmentService.updateRemainingInsuraceProcessMainList(loanId, insuranceBalance);
                }

                SettlementMain settlementMain = new SettlementMain();
                settlementMain.setTransactionCodeCrdt(refNoInsuaranceCode);
                settlementMain.setDescription(refNoInsuaranceCodeDscrpt);
                settlementMain.setTransactionNoCrdt(newTransactionNo);
                settlementMain.setCrdtAmount(payAmount);
                settlementMain.setLoanId(loanId);
                settlementMain.setDebtorId(debtor.getDebtorId());
                settlementMain.setUserId(userid);
                settlementMain.setReceiptNo(receiptNo);
                settlementMain.setIsPosting(false);
                settlementMain.setSystemDate(systemDate);
                settlementMain.setBranchId(formData.getBranchId());

                status = settlmentService.addSettlementMain(settlementMain);

                SettlementCharges settlementCharges = new SettlementCharges();

                settlementCharges.setBillNo(receiptNo);
                settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                settlementCharges.setDebtorId(debtor.getDebtorId());
                settlementCharges.setLoanId(loanId);
                settlementCharges.setReffNo(newTransactionNo);
                settlementCharges.setUserId(userid);
                settlementCharges.setCharge(payAmount);
                settlementCharges.setChargeType(refNoInsuaranceCode);
                settlementCharges.setSystemDate(systemDate);
                settlementCharges.setBranchId(formData.getBranchId());

                description = description + "," + refNoInsuaranceCodeDscrpt;
                status = settlmentService.addChargeDetails(settlementCharges);

                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;

            }
        }

        //***************Rebate Payment ***********
        if (formData.getIsRabatePayment()) {
            double rebateBalance = settlmentService.getRebateBalance(loanId);
            if (0 < rebateBalance) {
                double payAmount = 0;

                if (rebateBalance <= totalPayment) {
                    payAmount = rebateBalance;
                    status = settlmentService.updateLoanHeaderStatus(loanId, 5);
                    status = settlmentService.updateRebateDetail(loanId);
                }

                SettlementMain settlementMain = new SettlementMain();
                settlementMain.setTransactionCodeCrdt(rebatePaymentCode);
                settlementMain.setDescription(rebatePaymentCodeDscept);
                settlementMain.setTransactionNoCrdt(newTransactionNo);
                settlementMain.setCrdtAmount(payAmount);
                settlementMain.setLoanId(loanId);
                settlementMain.setDebtorId(debtor.getDebtorId());
                settlementMain.setUserId(userid);
                settlementMain.setReceiptNo(receiptNo);
                settlementMain.setIsPosting(false);
                settlementMain.setSystemDate(systemDate);
                settlementMain.setBranchId(formData.getBranchId());

                status = settlmentService.addSettlementMain(settlementMain);

                description = description + "," + rebatePaymentCodeDscept;

                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;

            }
        }

        //********************Installment*************************
        if (formData.getIsInstalment()) {

            List<SettlementMain> paidInstalmentList = settlmentService.getPaidLoanInstallmnetList(loanId);
            List<SettlementMain> totalInstallmentList = settlmentService.getDebitInstallmnetList(loanId);

            List<SettlementMain> installmentListWithOdi = settlmentService.getInstalmntWithODI(loanId);

            LoanInstallment loanInstallment = settlmentService.findLoanInstallment(loanId);
            List<LoanInstallment> loanInstallmentList = settlmentService.findPaybleInstalmentsList(loanId, systemDate);

            double interest = 0.00;
            double rental = 0.00;
            double diff = 0.00;
            double roundAmount = 0.00;

//            if (loanInstallment != null) {
//                interest = new BigDecimal(loanInstallment.getInsInterest()).setScale(2, RoundingMode.HALF_UP).doubleValue();
//                rental = new BigDecimal(loanInstallment.getInsPrinciple()).setScale(2, RoundingMode.HALF_UP).doubleValue();
//                double differ = loanInstallment.getInsRoundAmount() - (rental + interest);
//                diff = new BigDecimal(differ).setScale(2, RoundingMode.HALF_UP).doubleValue();
//            }
//            if (diff < 0.00) {
//                diff = 0.00;
//            }
            double totalRental = interest + rental + diff;

            double paidInstalment = 0;
            double totalInstallment = 0;
            double paidOdi = 0;
            double totalOdi = 0;

            if (!installmentListWithOdi.isEmpty()) {
                for (int i = 0; i < installmentListWithOdi.size(); i++) {
                    String debtCode = null;
                    String credtCode = null;
                    debtCode = installmentListWithOdi.get(i).getTransactionCodeDbt();
                    if (debtCode != null) {
                        if (debtCode.equals(refNoInstallmentCode)) {
                            totalInstallment = totalInstallment + installmentListWithOdi.get(i).getDbtAmount();
                        } else if (debtCode.equals(refNoODICode)) {
                            totalOdi = totalOdi + installmentListWithOdi.get(i).getDbtAmount();
                        }
                    }
                    credtCode = installmentListWithOdi.get(i).getTransactionCodeCrdt();
                    if (credtCode != null) {
                        if (credtCode.equals(refNoInstallmentCode)) {
                            paidInstalment = paidInstalment + installmentListWithOdi.get(i).getCrdtAmount();
                        } else if (credtCode.equals(refNoODICode)) {
                            paidOdi = paidOdi + installmentListWithOdi.get(i).getCrdtAmount();
                        }
                    }
                }
            }
            double intallmentBalance = 0.00;
            double intallmentBalanceroun = (totalInstallment + totalOdi) - (paidInstalment + paidOdi);
            intallmentBalance = new BigDecimal(intallmentBalanceroun).setScale(2, RoundingMode.HALF_UP).doubleValue();

            //************************ Add Over Pay        
            double payAmount = 0;
            if (formData.getIsOverPay()) {
                totalPayment = totalPayment + overPayAmount;        //************Add Over Pay                
                if (intallmentBalance <= totalPayment) {
                    payAmount = intallmentBalance;
                    updateOverPayAmount = totalPayment - payAmount;
                    if (overPayAmount > 0) {
                        settlmentService.addOverPayDebit(loanId, overPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, formData.getBranchId(), systemDate);
                        overPayAmount = 0;
                    }
                    if (0 <= updateOverPayAmount) {
                        settlmentService.addOverPayAmount(loanId, updateOverPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, formData.getBranchId(), systemDate);
                        updateOverPayAmount = 0;
                    }
                } else if (totalPayment < intallmentBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                    if (overPayAmount > 0) {
                        settlmentService.addOverPayDebit(loanId, overPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, formData.getBranchId(), systemDate);
                        overPayAmount = 0;
                    }
                }

            } else {
                if (intallmentBalance <= totalPayment) {
                    payAmount = intallmentBalance;
                } else if (totalPayment < intallmentBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                }
                updateOverPayAmount = totalPayment - payAmount;
                if (0 <= updateOverPayAmount) {
                    settlmentService.addOverPayAmount(loanId, updateOverPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, formData.getBranchId(), systemDate);
                    updateOverPayAmount = 0;
                }
            }

            if (0.00 < intallmentBalance) {
                double paidCurrentOdi = 0;
                double remainOdi = new BigDecimal(totalOdi - paidOdi).setScale(2, RoundingMode.HALF_UP).doubleValue();
                if (0.00 < remainOdi) {
                    SettlementMain settlementMain = new SettlementMain();
                    settlementMain.setTransactionCodeCrdt(refNoODICode);
                    settlementMain.setDescription(refNoODICodeDscrpt);
                    settlementMain.setTransactionNoCrdt(newTransactionNo);
                    if (totalOdi - paidOdi < payAmount) {
                        paidCurrentOdi = totalOdi - paidOdi;
                        settlementMain.setCrdtAmount(paidCurrentOdi);
                    } else {
                        paidCurrentOdi = payAmount;
                        settlementMain.setCrdtAmount(paidCurrentOdi);
                    }
                    settlementMain.setLoanId(loanId);
                    settlementMain.setDebtorId(debtor.getDebtorId());
                    settlementMain.setUserId(userid);
                    settlementMain.setReceiptNo(receiptNo);
                    settlementMain.setIsPosting(false);
                    settlementMain.setSystemDate(systemDate);
                    settlementMain.setBranchId(formData.getBranchId());
                    status = settlmentService.addSettlementMain(settlementMain);

                    SettlementCharges settlementCharges = new SettlementCharges();

                    settlementCharges.setBillNo(receiptNo);
                    settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                    settlementCharges.setDebtorId(debtor.getDebtorId());
                    settlementCharges.setLoanId(loanId);
                    settlementCharges.setReffNo(newTransactionNo);
                    settlementCharges.setUserId(userid);
                    settlementCharges.setSystemDate(systemDate);
                    settlementCharges.setBranchId(formData.getBranchId());

                    if (totalOdi - paidOdi < payAmount) {
                        paidCurrentOdi = totalOdi - paidOdi;
                        settlementCharges.setCharge(paidCurrentOdi);
                    } else {
                        paidCurrentOdi = payAmount;
                        settlementCharges.setCharge(paidCurrentOdi);
                    }

                    settlementCharges.setChargeType(refNoODICode);

                    status = settlmentService.addChargeDetails(settlementCharges);
                }

                SettlementMain settlementMain = new SettlementMain();
                settlementMain.setTransactionCodeCrdt(refNoInstallmentCode);
                settlementMain.setDescription(refNoInstallmentCodeDscrpt);
                settlementMain.setTransactionNoCrdt(newTransactionNo);
                settlementMain.setCrdtAmount(payAmount - (paidCurrentOdi));
                settlementMain.setLoanId(loanId);
                settlementMain.setDebtorId(debtor.getDebtorId());
                settlementMain.setUserId(userid);
                settlementMain.setReceiptNo(receiptNo);
                settlementMain.setIsPosting(false);
                settlementMain.setSystemDate(systemDate);
                settlementMain.setBranchId(formData.getBranchId());

                status = settlmentService.addSettlementMain(settlementMain);

                //update Loaninstalment isPay Status
                double remainingInstallmentBalance = settlementMain.getCrdtAmount();
                settlmentService.updateLoanInstallment(loanId, remainingInstallmentBalance, systemDate, settlementMain.getTransactionCodeCrdt(), settlementMain.getCrdtAmount(), receiptNo, totalPayment);
                double postChargeAmount = new BigDecimal(payAmount - (paidCurrentOdi)).setScale(2, RoundingMode.HALF_UP).doubleValue();

                int k = loanInstallmentList.size();
                for (int j = 0; 0.00 < postChargeAmount && j < 100; j++) {

                    interest = new BigDecimal(loanInstallmentList.get(0).getInsInterest()).setScale(2, RoundingMode.HALF_UP).doubleValue();
                    rental = new BigDecimal(loanInstallmentList.get(0).getInsPrinciple()).setScale(2, RoundingMode.HALF_UP).doubleValue();
                    roundAmount = new BigDecimal(loanInstallmentList.get(0).getInsRoundAmount()).setScale(2, RoundingMode.HALF_UP).doubleValue();

                    double differ = roundAmount - (rental + interest);
                    diff = new BigDecimal(differ).setScale(2, RoundingMode.HALF_UP).doubleValue();

                    if (k > 0) {
                        k--;
                    }
                    if (k < loanInstallmentList.size() && roundAmount <= postChargeAmount) {
                        interest = new BigDecimal(loanInstallmentList.get(k).getInsInterest()).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        rental = new BigDecimal(loanInstallmentList.get(k).getInsPrinciple()).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        roundAmount = new BigDecimal(loanInstallmentList.get(k).getInsRoundAmount()).setScale(2, RoundingMode.HALF_UP).doubleValue();

                        differ = roundAmount - (rental + interest);
                        diff = new BigDecimal(differ).setScale(2, RoundingMode.HALF_UP).doubleValue();

                    }

                    totalRental = interest + rental + diff;
                    SettlementCharges settlementCharges = new SettlementCharges();

                    settlementCharges.setBillNo(receiptNo);
                    settlementCharges.setDebtorAccNo(debtor.getDebtorAccountNo());
                    settlementCharges.setDebtorId(debtor.getDebtorId());
                    settlementCharges.setLoanId(loanId);
                    settlementCharges.setReffNo(newTransactionNo);
                    settlementCharges.setUserId(userid);
                    settlementCharges.setSystemDate(systemDate);
                    settlementCharges.setBranchId(formData.getBranchId());

                    if (totalRental <= 0.00) {
                        settlementCharges.setCharge(postChargeAmount);
                        settlementCharges.setChargeType(refNoRentalCode);
                        postChargeAmount = 0.00;
                        status = settlmentService.addChargeDetails(settlementCharges);

                    } else if (interest + rental + diff <= postChargeAmount) {
                        settlementCharges.setCharge(rental);
                        settlementCharges.setChargeType(refNoRentalCode);
                        postChargeAmount = postChargeAmount - rental;
                        status = settlmentService.addChargeDetails(settlementCharges);

                        settlementCharges.setCharge(interest);
                        settlementCharges.setChargeType(refNoRentalInterCode);
                        postChargeAmount = postChargeAmount - interest;
                        status = settlmentService.addChargeDetails(settlementCharges);

                        settlementCharges.setCharge(diff);
                        settlementCharges.setChargeType(refNoRentalDiffCode);
                        postChargeAmount = postChargeAmount - diff;
                        status = settlmentService.addChargeDetails(settlementCharges);

                    } else if (interest + rental <= postChargeAmount && postChargeAmount < interest + rental + diff) {
                        settlementCharges.setCharge(rental);
                        settlementCharges.setChargeType(refNoRentalCode);
                        postChargeAmount = postChargeAmount - rental;
                        status = settlmentService.addChargeDetails(settlementCharges);

                        settlementCharges.setCharge(interest);
                        settlementCharges.setChargeType(refNoRentalInterCode);
                        postChargeAmount = postChargeAmount - interest;
                        status = settlmentService.addChargeDetails(settlementCharges);

                        settlementCharges.setCharge(postChargeAmount);
                        settlementCharges.setChargeType(refNoRentalDiffCode);
                        postChargeAmount = 0;
                        status = settlmentService.addChargeDetails(settlementCharges);

                    } else if (interest < postChargeAmount && postChargeAmount < rental) {
                        settlementCharges.setCharge(interest);
                        settlementCharges.setChargeType(refNoRentalInterCode);
                        postChargeAmount = postChargeAmount - interest;
                        status = settlmentService.addChargeDetails(settlementCharges);

                        settlementCharges.setCharge(postChargeAmount);
                        settlementCharges.setChargeType(refNoRentalCode);
                        postChargeAmount = 0;
                        status = settlmentService.addChargeDetails(settlementCharges);
                    } else {
                        if (interest < postChargeAmount) {
                            settlementCharges.setCharge(interest);
                            postChargeAmount = postChargeAmount - interest;
                        } else {
                            settlementCharges.setCharge(postChargeAmount);
                            postChargeAmount = 0;
                        }
                        settlementCharges.setChargeType(refNoRentalInterCode);
                        status = settlmentService.addChargeDetails(settlementCharges);
                    }
                    settlementCharges = null;
                }

                description = description + "," + refNoInstallmentCodeDscrpt;
            }
        }

        return status;
    }

    @RequestMapping(value = "/printReceipt", method = RequestMethod.GET)
    public void printReceipt(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        try {
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            URL url1 = this.getClass().getClassLoader().getResource("../resources/img/wijithaFinanceLogo.png");
//            String logopath = url1.getPath() + "/";
            String logopath = url1.toURI().toString();

            String userName = "", branchName = "";
            Double loanBalance = 0.00;
            int copy = 0;
            userName = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            branchName = userService.findWorkingBranchName(branchID);
            Date systemDate = settlmentService.getSystemDate(branchID);
            Calendar c1 = Calendar.getInstance();
            c1.setTime(systemDate);
            c1.add(Calendar.DATE, 1);
            Date checkDate = c1.getTime();
            LoanHeaderDetails headerDetails = settlmentService.findLoanHeaderDetails(printLoanId);
            Date lapsDate = headerDetails.getLoanLapsDate();
            if (lapsDate.before(checkDate)) {
                loanBalance = dayEndService.findBalanceWithOverPay(printLoanId);
                DecimalFormat df = new DecimalFormat("0.00");
                String formate = df.format(loanBalance);
                double finalValue = Double.parseDouble(formate);
                if (finalValue >= -5 && finalValue <= 0.00) {
                    String fullyPaid = "**FULLY PAID**";
                    params.put("isFullyPaid", fullyPaid);
                    settlmentService.updateLoanHeaderStatus(printLoanId, 4);
                }
            }
            RebateDetail detail = settlmentService.findRebateDetailsAfterRebate(printLoanId);
            if (detail != null) {
                if (detail.getIsFullyPaid()) {
                    String fullyPaid = "**FULLY PAID**";
                    params.put("isFullyPaid", fullyPaid);
                } else {
                    String fullyPaid = "**FULLY PAID WITH REBATE**";
                    params.put("isFullyPaid", fullyPaid);
                }
            }
            LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(printLoanId);
            double totalInterest = settlmentService.getTotalInterest(printLoanId, "INTRS");
            double totalInsurance = settlmentService.getTotalInsuarance(printLoanId);
            double totalDownPayment = settlmentService.getTotalDownPayment(printLoanId);//from loan header detail
            double odiBalance = settlmentService.getOdiBalance(printLoanId);
            double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(printLoanId);
            List<ReturnList> openningBalanceList = settlmentService.findOpenningBalance(printLoanId);
            double totalOpenningD = 0.00;
            if (openningBalanceList != null) {
                totalOpenningD = settlmentService.getSumOfOpenningBalance(openningBalanceList);
            }
            double totalOpening = totalOpenningD + openningBalanceWithoutODI;

            double paidArrears = settlmentService.getPaidArrears(printLoanId); //done
            double paidOtherCharges = settlmentService.getLoanPaidOtherCharges(printLoanId);//done
            double paidDownPayment = settlmentService.getPaidDownPayment(printLoanId);//done
            double paidInsurance = settlmentService.getLoanPaidInsurenceCharges(printLoanId);//done
            double overPayAmount = settlmentService.getOverPayAmount(printLoanId);
            double paidInstallments = settlmentService.getLoanPaidInstallments(printLoanId);
            double paidOdi = settlmentService.getPaidOdiBalance(printLoanId);

            double totalReceivble = loan.getLoanInvestment() + totalInterest + totalInsurance + totalDownPayment + odiBalance + totalOpening;
            double totalPaid = paidArrears + paidOtherCharges + paidDownPayment + paidInsurance + overPayAmount + paidInstallments + paidOdi;

            double remainingBalance = totalReceivble - totalPaid;

            params.put("loanId", printLoanId);
            params.put("logoPath", logopath);
            params.put("receiptNo", newReceiptNo);
            params.put("branchName", branchName);
            params.put("userName", userName);
            params.put("systemDate", systemDate);
            params.put("copy", 0);
            params.put("remainingBalance", remainingBalance);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/receipt");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("../reports/receipt/wijitha_receipt.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //printCorrectionReceipt
    @RequestMapping(value = "/printCorrectionReceipt", method = RequestMethod.GET)
    public void printCorrectionReceipt(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        try {
            String receiptNo = null;
            if (request.getParameter("receiptNo") != null) {
                receiptNo = request.getParameter("receiptNo");
            }
            int loanId = settlmentService.getLoanIdByReceiptNo(receiptNo);

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();

            URL url1 = this.getClass().getClassLoader().getResource("../resources/img/wijithaFinanceLogo.png");
//            String logopath = url1.getPath() + "/";
            String logopath = url1.toURI().toString();
            String userName = "", branchName = "";
            int copy = 0;
            userName = userService.findWorkingUserName();
            int branchID = (Integer) request.getSession().getAttribute("branchId");
            branchName = userService.findWorkingBranchName(branchID);
            Date systemDate = settlmentService.getSystemDate(branchID);

            params.put("branchName", branchName);
            params.put("userName", userName);
            params.put("loanId", loanId);
            params.put("logoPath", logopath);
            params.put("receiptNo", receiptNo);
            params.put("copy", 1);
            params.put("systemDate", systemDate);
            params.put("REPORT_CONNECTION", c);

            URL url = this.getClass().getClassLoader().getResource("../reports/receipt");
            String path = url.toURI().toString();//getPath() + "/";
            params.put("SUBREPORT_DIR", path);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("../reports/receipt/wijitha_receipt.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/loadCorrectPayment", method = RequestMethod.GET)
    public ModelAndView loadCorrectPayment(Model model, HttpServletRequest request) {
        boolean bySysDate = true;
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        systemDate = settlmentService.getSystemDate(userWorkingBanchId);
        model.addAttribute("systemDate", systemDate);
        model.addAttribute("bySysDate", bySysDate);
        return new ModelAndView("settlement/paymentCorrection");
    }

    @RequestMapping(value = "/loadCorrectPaymentTable", method = RequestMethod.GET)
    public ModelAndView loadCorrectPaymentTable(Model model, HttpServletRequest request) {
        boolean message = false;
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        systemDate = settlmentService.getSystemDate(userWorkingBanchId);
        List<StlmntReturnList> paymentList = settlmentService.getPaymentList(systemDate, userWorkingBanchId);
        if (paymentList != null) {
            message = true;
        }
        model.addAttribute("message", message);
        model.addAttribute("paymentList", paymentList);
        return new ModelAndView("settlement/paymentCorrectionTable");
    }

    @RequestMapping(value = "/loadPaymentCorrectionForm/{transactionId}", method = RequestMethod.GET)
    public ModelAndView loadPaymentCorrectionForm(@PathVariable(value = "transactionId") int transactionId, Model model) {

        int userId = userService.findByUserName();
        int userType = userService.findUserTypeByUserId(userId);
        boolean validation = false;
        boolean beforeDate = false;

        SettlementPayments paymentDetail = settlmentService.getPaymentDetail(transactionId);

        if (paymentDetail != null) {
            if (paymentDetail.getUserId() == userId || userType == 1 || userType == 18) {
                validation = true;
            }
            if (paymentDetail.getSystemDate().before(systemDate)) {
                beforeDate = true;
            }
        }

        model.addAttribute("validation", validation);
        model.addAttribute("paymentDetail", paymentDetail);
        model.addAttribute("beforeDate", beforeDate);

        return new ModelAndView("settlement/paymentCorrectionForm");
    }

    //savePaymentRemove
    @RequestMapping(value = "/savePaymentRemove", method = RequestMethod.GET)
    public ModelAndView savePaymentRemove(@ModelAttribute("newPaymentCorrectionForm") SettlementPaymentDeleteForm sttlmntDeleteDeleteForm, Model model, HttpServletRequest request) {
        SettlementPayments settlementPayments;
        boolean status = false;
        String successMsg = "";
        String errorMsg = "";
        if (sttlmntDeleteDeleteForm != null) {
            settlementPayments = settlmentService.getPaymentDetail(sttlmntDeleteDeleteForm.getTransactionId());
            status = settlmentService.addSettlementPaymentDelete(settlementPayments, sttlmntDeleteDeleteForm);
            if (status) {
                status = settlmentService.removeStlMainChargePayment(settlementPayments.getReceiptNo());
                //remove SMS
                settlmentService.removeSMS(settlementPayments.getLoanId(), settlementPayments.getReceiptNo());
            }
        }
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }

        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        systemDate = settlmentService.getSystemDate(userWorkingBanchId);

        boolean message = false;
        List<StlmntReturnList> paymentList = settlmentService.getPaymentList(systemDate, userWorkingBanchId);
        if (paymentList != null) {
            message = true;
        }

        if (status) {
            successMsg = "Successfully Saved..!";
            model.addAttribute("successMessage", successMsg);
            return new ModelAndView("messages/success");
        }
        errorMsg = "Payment Not Removed..!";

        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("messages/error");
    }

    @RequestMapping(value = "/getSystemDate", method = RequestMethod.GET)
    public @ResponseBody
    String getSystemDate() {
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        systemDate = settlmentService.getSystemDate(userLogBranchId);

        return systemDate.toString();
    }

    @RequestMapping(value = "/saveAdjustment", method = RequestMethod.GET)
    public @ResponseBody
    boolean saveAdjustment(HttpServletRequest request, Model model) {

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }

        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        systemDate = settlmentService.getSystemDate(userWorkingBanchId);
        boolean status = false;

        String successMsg;
        String errorMsg;
        int isDebt = 1;
        SettlementMain settlementMain = new SettlementMain();

        int loanId = 0;
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        double amount = 0, newAmount = 0.00;
        if (request.getParameter("amount") != null) {
            amount = Double.parseDouble(request.getParameter("amount"));
        }

        String remark = null;
        if (request.getParameter("remark") != null) {
            remark = request.getParameter("remark");
        }

        String accId = "";
        if (request.getParameter("accId") != null) {
            accId = request.getParameter("accId");
        }
        String code = "";
        String[] acc = accId.split("-");
        if (acc[1].equals("O")) {
            code = "OTHR";
        } else {
            int id = Integer.parseInt(acc[0]);
            if (id == 1) {
                code = "DWN";
            } else if (id == 2) {
                code = "INS";
            } else if (id == 3) {
                code = "INS";
            } else {
                code = "OVP";
            }
        }
        if (code.equals("OVP")) {
            if (amount < 0) {
                isDebt = 1;
            } else {
                isDebt = 0;
            }
        } else if (amount < 0) {
            isDebt = 0;
        } else {
            isDebt = 1;
        }

        newAmount = Math.abs(amount);

        DebtorHeaderDetails dbt = settlmentService.findDebtorByLoanId(loanId);
        String newTransNo = settlmentService.getAdjustTransCode(refNoAdjustment);

        int userType = userService.findUserTypeByUserId(userId);
        if (userType == 7 || userType == 1) {
            settlementMain.setUserId(userId);
            settlementMain.setBranchId(userWorkingBanchId);
            settlementMain.setSystemDate(systemDate);
            settlementMain.setLoanId(loanId);
            if (isDebt == 0) {
                //credit
                settlementMain.setCrdtAmount(newAmount);
                settlementMain.setTransactionCodeCrdt(code);
                settlementMain.setTransactionNoCrdt(newTransNo);
            } else {
                //debit
                settlementMain.setDbtAmount(newAmount);
                settlementMain.setTransactionCodeDbt(code);
                settlementMain.setTransactionNoDbt(newTransNo);
            }
            settlementMain.setDescription(refNoAdjustment + "-" + remark);
            settlementMain.setDebtorId(dbt.getDebtorId());
            settlementMain.setIsPosting(false);

            status = settlmentService.addSettlementMain2(settlementMain, accId);
        }

//        if (status) {
//            successMsg = "Successfully Saved..!";
//            model.addAttribute("successMessage", successMsg);
//            return new ModelAndView("messages/success");
//        }
//        errorMsg = "Session has expired..!";
//
//        model.addAttribute("errorMsg", errorMsg);
//        return new ModelAndView("messages/error");
        return status;
    }

    @RequestMapping(value = "/saveOdi", method = RequestMethod.GET)
    public @ResponseBody
    boolean saveOdi(HttpServletRequest request, Model model) {

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }

        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        systemDate = settlmentService.getSystemDate(userWorkingBanchId);

        String successMsg;
        String errorMsg;

        boolean status = false;

        int loanId = 0;
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        double odi = 0;
        if (request.getParameter("odi") != null) {
            odi = Double.parseDouble(request.getParameter("odi"));
        }

        DebtorHeaderDetails dbt = settlmentService.findDebtorByLoanId(loanId);
        String newTransNo = settlmentService.getAdjustTransCode(refNoODICode);

        int userType = userService.findUserTypeByUserId(userId);
        if (userType == 7 || userType == 1) {
            SettlementMain settlementMain = new SettlementMain();
            settlementMain.setUserId(userId);
            settlementMain.setBranchId(userWorkingBanchId);
            settlementMain.setSystemDate(systemDate);
            settlementMain.setLoanId(loanId);
            settlementMain.setDbtAmount(odi);// 
            settlementMain.setTransactionCodeDbt(refNoODICode);
            settlementMain.setTransactionNoDbt(newTransNo);
            settlementMain.setDescription("ODI Adjustment");
            settlementMain.setDebtorId(dbt.getDebtorId());
            settlementMain.setIsPosting(false);

            status = settlmentService.addSettlementMain(settlementMain);

//            if (status) {
////                successMsg = "Successfully Saved..!";
////                model.addAttribute("successMessage", successMsg);
////                return new ModelAndView("messages/success");
//                ModelAndView mdl = addSettlement(loanId, model);
//                return mdl;
//            }
        }
//        return new ModelAndView("settlement/checkPassWord");
        return status;
    }

    @RequestMapping(value = "/saveOdi2", method = RequestMethod.GET)
    public @ResponseBody
    boolean saveOdi2(HttpServletRequest request, Model model) {

        String successMsg;
        String errorMsg;

        boolean status = false;

        int loanId = 0;
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        double odi = 0;
        if (request.getParameter("odi") != null) {
            odi = Double.parseDouble(request.getParameter("odi"));
        }
        String uname = null;
        if (request.getParameter("uname") != null) {
            uname = request.getParameter("uname");
        }

        UmUser user = settlmentService.checkUserPrivileges(uname, "");

        DebtorHeaderDetails dbt = settlmentService.findDebtorByLoanId(loanId);
        String newTransNo = settlmentService.getAdjustTransCode(refNoODICode);

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);

//        int userType = userService.findUserTypeByUserId(user.getUserId());
//        if (user.getUserTypeId() == 7) {
        SettlementMain settlementMain = new SettlementMain();
        settlementMain.setUserId(user.getUserId());
        settlementMain.setBranchId(userWorkingBanchId);
        settlementMain.setSystemDate(systemDate);
        settlementMain.setLoanId(loanId);
        settlementMain.setDbtAmount(odi);// 
        settlementMain.setTransactionCodeDbt(refNoODICode);
        settlementMain.setTransactionNoDbt(newTransNo);
        settlementMain.setDescription("ODI Adjustment");
        settlementMain.setDebtorId(dbt.getDebtorId());
        settlementMain.setIsPosting(false);

        status = settlmentService.addSettlementMain(settlementMain);
        return status;
    }

    @RequestMapping(value = "/checkPriviledges", method = RequestMethod.GET)
    public @ResponseBody
    boolean checkPriviledges(HttpServletRequest request, Model model) {

        boolean status = false;
        String uname = null;
        if (request.getParameter("uname") != null) {
            uname = request.getParameter("uname");
        }
        String pword = null;
        if (request.getParameter("pword") != null) {
            pword = request.getParameter("pword");
        }
        UmUser user = settlmentService.checkUserPrivileges(uname, pword);

        if (user != null) {
            if (user.getUserTypeId() == 7 || user.getUserTypeId() == 1) {
                status = true;
            }
        }
        return true;
    }

    @RequestMapping(value = "/loadMainPage", method = RequestMethod.GET)
    public ModelAndView loadMainPage(HttpServletRequest request, Model model) {
        int loanId = 0;
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        ModelAndView mdl = addSettlement(loanId, model);
        return mdl;
    }

    @RequestMapping(value = "/loadCheckPassWord", method = RequestMethod.GET)
    public ModelAndView loadCheckPassWord(HttpServletRequest request, Model model) {
        int loanId = 0;
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        double odi = 0;
        if (request.getParameter("odi") != null) {
            odi = Double.parseDouble(request.getParameter("odi"));
        }

        model.addAttribute("odi", odi);
        model.addAttribute("loanId", loanId);
        return new ModelAndView("settlement/checkPassWord");
    }

    //    loadAdjCheckPassWord
    @RequestMapping(value = "/loadAdjCheckPassWord", method = RequestMethod.GET)
    public ModelAndView loadAdjCheckPassWord(HttpServletRequest request, Model model) {
        int loanId = 0;
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        double amount = 0;
        if (request.getParameter("amount") != null) {
            amount = Double.parseDouble(request.getParameter("amount"));
        }
        String remark = null;
        if (request.getParameter("remark") != null) {
            remark = request.getParameter("remark");
        }

        model.addAttribute("remark", remark);
        model.addAttribute("amount", amount);
        model.addAttribute("loanId", loanId);
        return new ModelAndView("settlement/checkAdjPassWord");
    }

    @RequestMapping(value = "/saveAdj2", method = RequestMethod.GET)
    public @ResponseBody
    boolean saveAdj2(HttpServletRequest request, Model model) {

        String successMsg;
        String errorMsg;
        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        int isDebt = 1;
        boolean status = false;
        SettlementMain settlementMain = new SettlementMain();
        int loanId = 0;
        if (request.getParameter("loanId") != null) {
            loanId = Integer.parseInt(request.getParameter("loanId"));
        }
        double amount = 0, newAmount = 0.00;
        if (request.getParameter("amount") != null) {
            amount = Double.parseDouble(request.getParameter("amount"));
        }

        String remark = null;
        if (request.getParameter("remark") != null) {
            remark = request.getParameter("remark");
        }

        String accId = "4-I";
        if (request.getParameter("accId") != null) {
            accId = request.getParameter("accId");
        }
        String code = "";
        String[] acc = accId.split("-");
        if (acc[1].equals("O")) {
            code = "OTHR";
        } else {
            int id = Integer.parseInt(acc[0]);
            if (id == 1) {
                code = "DWN";
            } else if (id == 2) {
                code = "INS";
            } else if (id == 3) {
                code = "INS";
            } else {
                code = "OVP";
            }
        }
        if (code.equals("OVP")) {
            if (amount < 0) {
                isDebt = 1;
            } else {
                isDebt = 0;
            }
        } else if (amount < 0) {
            isDebt = 0;
        } else {
            isDebt = 1;
        }

        newAmount = Math.abs(amount);

        DebtorHeaderDetails dbt = settlmentService.findDebtorByLoanId(loanId);
        String newTransNo = settlmentService.getAdjustTransCode(refNoAdjustment);

        settlementMain.setUserId(userId);
        settlementMain.setBranchId(userWorkingBanchId);
        settlementMain.setSystemDate(systemDate);
        settlementMain.setLoanId(loanId);
        if (isDebt == 0) {
            //credit
            settlementMain.setCrdtAmount(newAmount);
            settlementMain.setTransactionCodeCrdt(code);
            settlementMain.setTransactionNoCrdt(newTransNo);
        } else {
            //debit
            settlementMain.setDbtAmount(newAmount);
            settlementMain.setTransactionCodeDbt(code);
            settlementMain.setTransactionNoDbt(newTransNo);
        }
        settlementMain.setDescription(refNoAdjustment + "-" + remark);
        settlementMain.setDebtorId(dbt.getDebtorId());
        settlementMain.setIsPosting(false);

        status = settlmentService.addSettlementMain2(settlementMain, accId);
        return status;
    }

    @RequestMapping(value = "/loadOtherChargeAccounts/{loanId}", method = RequestMethod.GET)
    public @ResponseBody
    List<OtherChargseModel> loadSubLoanTypes(@PathVariable("loanId") int loanId) {
        List<OtherChargseModel> otherChargers = loanService.findOtherChargesByLoanId(loanId);
        return otherChargers;
    }

    private SettlementPreview addSettlementMainPreview(SettlementAddNewPayment formData, int loanId, String newTransactionNo, DebtorHeaderDetails debtor, int userid, String receiptNo) {

        SettlementPreview settlementPreview = new SettlementPreview();
        settlementPreview.setRefArrearsCodeDscrpt(refArrearsCodeDscrpt);
        settlementPreview.setRefNoDownPymntSuspCodeDscrpt(refNoDownPymntSuspCodeDscrpt);
        settlementPreview.setRefNoOtherChargeCodeDscrpt(refNoOtherChargeCodeDscrpt);
        settlementPreview.setRefNoInsuaranceCodeDscrpt(refNoInsuaranceCodeDscrpt);
        settlementPreview.setRefNoODICodeDscrpt(refNoODICodeDscrpt);
        settlementPreview.setRefNoInstallmentCodeDscrpt(refNoInstallmentCodeDscrpt);
        settlementPreview.setRefNoOverPayCodeDescrpt(refNoOverPayCodeDescrpt);
        settlementPreview.setRefNoRebateDscrpt(rebatePaymentCodeDscept);

        double totalPayment = 0;
        double payAmounts[] = formData.getAmount();
        int payTypeId[] = formData.getPaymentTypeId();
        boolean status = false;
        double updateOverPayAmount = 0;
        double overPayAmount = formData.getOverPayAmount();

        for (int i = 0; i < formData.getPaymentTypeId().length; i++) {
            totalPayment = totalPayment + payAmounts[i];
        }

        //***************Arrears payment*************
        if (formData.getIsArreas()) {
            List<ReturnList> openningBalance = settlmentService.findOpenningBalance(loanId);
            double sumOpenningBalance = 0.00;
            if (openningBalance != null) {
                sumOpenningBalance = settlmentService.getSumOfOpenningBalance(openningBalance);
            }

            double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(loanId);
            double paidArrears = settlmentService.getPaidArrears(loanId);

            double totalOpening = (sumOpenningBalance + openningBalanceWithoutODI) - paidArrears;
            if (0 < totalOpening) {

                double payAmount = 0;
                if (totalOpening <= totalPayment) {
                    payAmount = totalOpening;
                } else if (totalPayment < totalOpening && 0 < totalPayment) {
                    payAmount = totalPayment;
                }

                settlementPreview.setArrearsAmount(new BigDecimal(payAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                double postChargeAmount = payAmount;

                for (int j = 0; 0 < postChargeAmount; j++) {
                    if (sumOpenningBalance + openningBalanceWithoutODI <= postChargeAmount) {
//                        description = description + refNoBalanceWithoutODICodeDscrpt;
                        postChargeAmount = postChargeAmount - openningBalanceWithoutODI;
                        double paidOtherChar = sumOpenningBalance;
                        for (int i = 0; 0 < paidOtherChar; i++) {
//                            description = description + "," + openningBalance.get(i).getArreasDescription();

                            postChargeAmount = postChargeAmount - openningBalance.get(i).getAmount();
                            paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();
                        }
                    } else if (sumOpenningBalance < postChargeAmount && postChargeAmount < (openningBalanceWithoutODI + sumOpenningBalance)) {

                        if (sumOpenningBalance < postChargeAmount) {
                            double paidOtherChar = sumOpenningBalance;
                            for (int i = 0; 0 < paidOtherChar; i++) {
                                if (openningBalance.get(i).getAmount() <= paidOtherChar) {
                                    paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();
                                } else {
                                    paidOtherChar = 0;
                                }
//                                description = description + "," + openningBalance.get(i).getArreasDescription();
                            }
                        }
//                        description = description + "," + refNoBalanceWithoutODICodeDscrpt;
                        postChargeAmount = 0;

                    } else if (sumOpenningBalance < postChargeAmount && postChargeAmount < openningBalanceWithoutODI) {

                        if (sumOpenningBalance < postChargeAmount) {
                            double paidOtherChar = sumOpenningBalance;
                            for (int i = 0; 0 < paidOtherChar; i++) {
                                if (openningBalance.get(i).getAmount() <= paidOtherChar) {
                                    paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();
                                } else {
                                    paidOtherChar = 0;
                                }
//                                description = description + "," + openningBalance.get(i).getArreasDescription();
                            }
                        }
//                        description = description + "," + refNoBalanceWithoutODICodeDscrpt;
                        postChargeAmount = 0;

                    } else {
                        double paidOtherChar = postChargeAmount;
                        for (int i = 0; 0 < paidOtherChar; i++) {
                            if (openningBalance.get(i).getAmount() <= paidOtherChar) {
                                paidOtherChar = paidOtherChar - openningBalance.get(i).getAmount();
                            } else {
                                paidOtherChar = 0;
                            }
//                            description = description + "," + openningBalance.get(i).getArreasDescription();

                            postChargeAmount = postChargeAmount - openningBalance.get(i).getAmount();
                        }
                    }
                }
                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;
            }
        }

        //******************Down payments***************
        if (formData.getIsDownPayment()) {
            List<OtherChargseModel> otherChargesList = settlmentService.findOtherChargesByLoanId(loanId);
            double totalOtherCharges = settlmentService.getSumOfOtherCherges(otherChargesList);
            double paidOtherCharges = settlmentService.getPaidOtherCharges(loanId);
            double OtherChargesBalance = -paidOtherCharges;
            double totalDownPayment = settlmentService.getTotalDownPayment(loanId);
            double paidDownPayment = settlmentService.getPaidDownPayment(loanId);
            double downPaymentBalance = totalDownPayment - paidDownPayment;

            double initialDownPayment = totalOtherCharges + totalDownPayment;
            if (0 < downPaymentBalance) {

                double payAmount = 0;
                if (downPaymentBalance <= totalPayment) {
                    payAmount = downPaymentBalance;
                } else if (totalPayment < downPaymentBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                }

                settlementPreview.setDownPymntSuspCodeDscrptAmount(new BigDecimal(payAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());

                double postChargeAmount = payAmount;
//                description = description + "," + refNoDownPaymentCodeDcsrpt;

                postChargeAmount = postChargeAmount - postChargeAmount;
                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;
            }

            if (OtherChargesBalance > 0) {
                double payAmount = 0;
                if (OtherChargesBalance <= totalPayment) {
                    payAmount = OtherChargesBalance;
                } else if (totalPayment < OtherChargesBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                }

                settlementPreview.setOtherChargeCodeDscrptAmount(new BigDecimal(payAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());

                double postChargeAmount = payAmount;

                double initialOtherCharge = 0;
                if (otherChargesList != null) {
                    for (int i = 0; i < otherChargesList.size(); i++) {
                        if (otherChargesList.get(i).getIsDownPaymentCharge() == 1) {
                            initialOtherCharge = initialOtherCharge + otherChargesList.get(i).getAmount();
                        }
                    }
                    if (initialOtherCharge <= postChargeAmount) {
                    }
                }

                double paidOtherChar = postChargeAmount;
                for (int i = 0; 0 < paidOtherChar && i < otherChargesList.size(); i++) {
                    if (otherChargesList.get(i).getIsPaid() == 0) {
                        if (otherChargesList.get(i).getAmount() <= paidOtherChar) {
                            postChargeAmount = postChargeAmount - otherChargesList.get(i).getAmount();
                            paidOtherChar = paidOtherChar - otherChargesList.get(i).getAmount();
                        } else {
                            postChargeAmount = postChargeAmount - paidOtherChar;
                            paidOtherChar = 0;
                        }
//                        description = description + "," + otherChargesList.get(i).getDescription();

//                        status = settlmentService.updatePaidStatus(otherChargesList.get(i).getChargeID());
                    }
                }

                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;
            }
        }
        //******************Insurance payments***************

        if (formData.getIsInsurance()) {
            double paidInsurance = settlmentService.getPaidInsuarance(loanId);
            double totalInsurance = settlmentService.getTotalInsuarance(loanId);
            double insuranceBalance = -paidInsurance;
            paidInsurance = totalInsurance - insuranceBalance;
            if (0 < insuranceBalance) {
                double payAmount = 0;
                int fullPaid = 3;
                int halfPaid = 2;

                if (insuranceBalance <= totalPayment) {
                    payAmount = insuranceBalance;
                } else if (totalPayment < insuranceBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                }

                settlementPreview.setInsuaranceCodeDscrptAmount(new BigDecimal(payAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());

//                description = description + "," + refNoInsuaranceCodeDscrpt;
                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;

            }
        }

        //***************Rebate Payment ***********
        if (formData.getIsRabatePayment()) {
            double rebateBalance = settlmentService.getRebateBalance(loanId);
            if (0 < rebateBalance) {
                double payAmount = 0;

                if (rebateBalance <= totalPayment) {
                    payAmount = rebateBalance;
                }
                settlementPreview.setRebateAmount(payAmount);

                updateOverPayAmount = totalPayment - payAmount;
                totalPayment = totalPayment - payAmount;

            }
        }

        //********************Installment*************************
        if (formData.getIsInstalment()) {

            List<SettlementMain> paidInstalmentList = settlmentService.getPaidLoanInstallmnetList(loanId);
            List<SettlementMain> totalInstallmentList = settlmentService.getDebitInstallmnetList(loanId);

            List<SettlementMain> installmentListWithOdi = settlmentService.getInstalmntWithODI(loanId);

            LoanInstallment loanInstallment = settlmentService.findLoanInstallment(loanId);

            double interest = 0;
            double rental = 0;

            if (loanInstallment != null) {
                interest = loanInstallment.getInsInterest();
                rental = loanInstallment.getInsPrinciple();
            }

            double paidInstalment = 0;
            double totalInstallment = 0;
            double paidOdi = 0;
            double totalOdi = 0;
            if (!installmentListWithOdi.isEmpty()) {
                for (int i = 0; i < installmentListWithOdi.size(); i++) {
                    String debtCode = null;
                    String credtCode = null;
                    debtCode = installmentListWithOdi.get(i).getTransactionCodeDbt();
                    if (debtCode != null) {
                        if (debtCode.equals(refNoInstallmentCode)) {
                            totalInstallment = totalInstallment + installmentListWithOdi.get(i).getDbtAmount();
                        } else if (debtCode.equals(refNoODICode)) {
                            totalOdi = totalOdi + installmentListWithOdi.get(i).getDbtAmount();
                        }
                    }
                    credtCode = installmentListWithOdi.get(i).getTransactionCodeCrdt();
                    if (credtCode != null) {
                        if (credtCode.equals(refNoInstallmentCode)) {
                            paidInstalment = paidInstalment + installmentListWithOdi.get(i).getCrdtAmount();
                        } else if (credtCode.equals(refNoODICode)) {
                            paidOdi = paidOdi + installmentListWithOdi.get(i).getCrdtAmount();
                        }
                    }
                }
            }

            double intallmentBalance = (totalInstallment + totalOdi) - (paidInstalment + paidOdi);

            //************************ Add Over Pay        
            double payAmount = 0;
            if (formData.getIsOverPay()) {
                totalPayment = totalPayment + overPayAmount;        //************Add Over Pay                
                if (intallmentBalance <= totalPayment) {
                    payAmount = intallmentBalance;
                    updateOverPayAmount = totalPayment - payAmount;
                    if (overPayAmount > 0) {
                        overPayAmount = 0;
                    }
                    if (0 <= updateOverPayAmount) {
                        settlementPreview.setOverPayAmount(new BigDecimal(updateOverPayAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                        updateOverPayAmount = 0;
                    }
                } else if (totalPayment < intallmentBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                    if (overPayAmount > 0) {
                        overPayAmount = 0;
                    }
                }

            } else {
                if (intallmentBalance <= totalPayment) {
                    payAmount = intallmentBalance;
                } else if (totalPayment < intallmentBalance && 0 < totalPayment) {
                    payAmount = totalPayment;
                }
                updateOverPayAmount = totalPayment - payAmount;
                if (0 <= updateOverPayAmount) {
                    settlementPreview.setOverPayAmount(new BigDecimal(updateOverPayAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    updateOverPayAmount = 0;
                }
            }

            if (0 < intallmentBalance) {
                double paidCurrentOdi = 0;
                if (0 < totalOdi - paidOdi) {
                    if (totalOdi - paidOdi < payAmount) {
                        paidCurrentOdi = totalOdi - paidOdi;
                        settlementPreview.setPaidCurrentOdi(new BigDecimal(paidCurrentOdi).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    } else {
                        paidCurrentOdi = payAmount;
                        settlementPreview.setPaidCurrentOdi(new BigDecimal(paidCurrentOdi).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    }
//                    if (totalOdi - paidOdi < payAmount) {
//                        paidCurrentOdi = totalOdi - paidOdi;
//                    } else {
//                        paidCurrentOdi = payAmount;
//                    }
                }
                settlementPreview.setInstallmentCodeDscrptAmount(new BigDecimal(payAmount - (paidCurrentOdi)).setScale(2, RoundingMode.HALF_UP).doubleValue());

                double postChargeAmount = payAmount - (paidCurrentOdi);

                for (int j = 0; 0 < postChargeAmount; j++) {
                    if (interest + rental <= postChargeAmount) {
                        postChargeAmount = postChargeAmount - rental;
                        postChargeAmount = postChargeAmount - interest;

                    } else if (interest < postChargeAmount && postChargeAmount < rental) {
                        postChargeAmount = postChargeAmount - interest;
                        postChargeAmount = 0;
                    } else if (interest < postChargeAmount) {
                        postChargeAmount = postChargeAmount - interest;
                    } else {
                        postChargeAmount = 0;
                    }
                }
//                description = description + "," + refNoInstallmentCodeDscrpt;
            }
        }

        if (0 < updateOverPayAmount) {
            settlementPreview.setOverPayAmount(new BigDecimal(updateOverPayAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
//            settlmentService.addOverPayAmount(loanId, updateOverPayAmount, userid, debtor.getDebtorId(), newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, userLogBranchId, systemDate);
        }
        return settlementPreview;
    }

    private double getBalance(int loanId) {

        List<ReturnList> openningBalanceList = settlmentService.findOpenningBalance(loanId);
        double totalOpenningD = 0.00;
        if (openningBalanceList != null) {
            totalOpenningD = settlmentService.getSumOfOpenningBalance(openningBalanceList);
        }

        double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(loanId);
        double paidInsurance = settlmentService.getPaidInsuarance(loanId);
//        double totalInsurance = settlmentService.getTotalInsuarance(loanId);
        double totalDownPayment = settlmentService.getTotalDownPayment(loanId);
        double paidDownPayment = settlmentService.getPaidDownPayment(loanId);
        double paidArrears = settlmentService.getPaidArrears(loanId);

        double adjustmntBalance = settlmentService.getAdjustmntBalance(loanId, refNoAdjustment);
        double installmentBalance = settlmentService.getInstalmentBalance(loanId);
        double insuranceBalance = -paidInsurance;
        double downPaymentBalance = totalDownPayment - paidDownPayment;
        double totalOpeningBalance = (totalOpenningD + openningBalanceWithoutODI) - paidArrears;

        double balance = totalOpeningBalance + downPaymentBalance + insuranceBalance + installmentBalance + adjustmntBalance;
        return Math.round(balance * 100) / 100;
    }

    //////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MsD~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //Loan Rebate Module
    @RequestMapping(value = "/loadRebateLoan", method = RequestMethod.GET)
    public ModelAndView loadRebateLoanMain(HttpServletRequest request, Model model) {
        return new ModelAndView("settlement/rebateLoanMain");
    }

    @RequestMapping(value = "/addRebateLoanForm/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadRebateLoanForm(@PathVariable("loanId") int loanId, HttpServletRequest request, Model model) {
        Double futureInterest = 0.00;
        Double futureCapital = 0.00;
        Double totalBalance = 0.00;
        Double totalPaid = 0.00;
        String totalPay = null;
        int reInstallmentCount = 0;
        int paidInstallmentCount = 0;
        String msg = "";
        try {
            LoanHeaderDetails headerDetails = settlmentService.findLoanHeaderDetails(loanId);
            if (headerDetails.getLoanSpec() == 4 || headerDetails.getLoanSpec() == 5) {
                msg = "This Loan Is Already Rebate";
            } else {
                LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(loanId);
                String refNoInterest = "INTRS";
                //payment history//
                List otherChargesList = loanService.findOtherChargesByLoanId(loanId);//from loan other charges
                List<ReturnList> openningBalanceList = settlmentService.findOpenningBalance(loanId);

                double totalOtherCharges = settlmentService.getSumOfOtherCherges(otherChargesList);
                double totalOpenningD = 0.00;
                if (openningBalanceList != null) {
                    totalOpenningD = settlmentService.getSumOfOpenningBalance(openningBalanceList);
                }

                double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(loanId);

                double paidArrears = settlmentService.getPaidArrears(loanId); //done
                double paidOtherCharges = settlmentService.getLoanPaidOtherCharges(loanId);//done
                double paidDownPayment = settlmentService.getPaidDownPayment(loanId);//done
                double paidInsurance = settlmentService.getLoanPaidInsurenceCharges(loanId);//done
                double overPayAmount = settlmentService.getOverPayAmount(loanId);
                double paidInstallments = settlmentService.getLoanPaidInstallments(loanId);
                double paidOdi = settlmentService.getPaidOdiBalance(loanId);

                double totalDownPayment = settlmentService.getTotalDownPayment(loanId);//from loan header detail
                double totalInsurance = settlmentService.getTotalInsuarance(loanId);
                double odiBalance = settlmentService.getOdiBalance(loanId);
                double adjustmntBalance = settlmentService.getAdjustmntBalance(loanId, refNoAdjustment);
                double totalInterest = settlmentService.getTotalInterest(loanId, refNoInterest);

                totalPaid = paidArrears + paidOtherCharges + paidDownPayment + paidInsurance + overPayAmount + paidInstallments + paidOdi;
                totalPay = String.format("%.2f", totalPaid);

                double totalOpening = totalOpenningD + openningBalanceWithoutODI;
                double totalReceivble = loan.getLoanInvestment() + totalInterest + totalOtherCharges + totalInsurance + totalDownPayment + odiBalance + totalOpening;
                totalBalance = totalReceivble - totalPaid + adjustmntBalance;
                totalBalance = (double) Math.round(totalBalance * 100) / 100;

                // get loan future capital, interest
                Double[] loanFutureDetails = reportService.getLoanFutureDetails(loanId);
                futureInterest = loanFutureDetails[0];
                futureInterest = (double) Math.round(futureInterest * 100) / 100;
                futureCapital = loanFutureDetails[1];
                futureCapital = (double) Math.round(futureCapital * 100) / 100;

                List<LoanInstallment> insList = loanService.findInstallmentByLoanNo(loan.getLoanAgreementNo());
                paidInstallmentCount = settlmentService.findPaidInstallmentCount(loanId);
                int totalInsCount = insList.size();
                reInstallmentCount = totalInsCount - paidInstallmentCount;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("loanId", loanId);
        model.addAttribute("rebateMsg", msg);
        model.addAttribute("loan_Balance", totalBalance);
        model.addAttribute("interest", futureInterest);
        model.addAttribute("reInsCount", reInstallmentCount);
        model.addAttribute("paidIns", paidInstallmentCount);
        model.addAttribute("remInsBalance", futureCapital);
        model.addAttribute("totalPaid", totalPay);
        return new ModelAndView("settlement/rebateLoanForm");
    }

    @RequestMapping(value = "/saveRebateLoan", method = RequestMethod.POST)
    public ModelAndView saveRebateLoan(@ModelAttribute("rebateForm") RebateDetail formData, Model model, BindingResult result) {
        String successMsg = "";
        String errorMsg = "";
        Boolean is_save = false;
        try {
            is_save = settlmentService.saveRebateLoan(formData);
            if (is_save) {
                successMsg = "Save Process Successfully";
                model.addAttribute("successMessage", successMsg);
                return new ModelAndView("messages/success");
            } else {
                errorMsg = "Save Process Failed";
                model.addAttribute("errorMessage", errorMsg);
                return new ModelAndView("messages/error");
            }

        } catch (Exception e) {
            e.printStackTrace();
            errorMsg = "Save Process Failed";
            model.addAttribute("errorMessage", errorMsg);
            return new ModelAndView("messages/error");
        }

    }

    @RequestMapping(value = "/loadRebateLoanList", method = RequestMethod.GET)
    public ModelAndView loadRebateLoanList(HttpServletRequest request, Model model) {
        List<RebateDetail> rebateDetails = null;
        try {
            rebateDetails = settlmentService.getRebateDetails();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        model.addAttribute("rebateDetails", rebateDetails);
        return new ModelAndView("settlement/rebateLoanViewMain");
    }

    @RequestMapping(value = "/loadRebateApprovalList", method = RequestMethod.GET)
    public ModelAndView loadRebateLoans(Model model) {
        List<RebateDetail> rebateDetails = null;
        try {
            rebateDetails = settlmentService.loadRebateApprovalList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("rebateDetails", rebateDetails);
        return new ModelAndView("settlement/rebateApprovalMain");
    }

    @RequestMapping(value = "/addRebateApproval", method = RequestMethod.POST)
    public ModelAndView addRebateApprovalForm(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        int type = Integer.parseInt(request.getParameter("type"));
        RebateApprovals approvals = null;
        String title = "";
        int usType = 0;
        int status = 0;
        try {
            approvals = settlmentService.findRebateApprovals(loanId, type);
            int userId = userService.findByUserName();
            int userType = userService.findUserTypeByUserId(userId);

            if (type == 0) {
                //user type (Account,admin,CED) approval
                if (userType == 10 || userType == 1 || userType == 7 || userType == 18) {
                    usType = 1;
                }
                title = "Accountant Approval";
            } else {
                //user type (admin,CED) approval
                if (userType == 1 || userType == 7 || userType == 18) {
                    usType = 1;
                }
                title = "CED Approval";
                status = 1;
            }
            //isApproved == 1 (approved)
            //isApproved == 0 (reject)
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("title", title);
        model.addAttribute("loanID", loanId);
        model.addAttribute("type", type);
        model.addAttribute("apprStatus", status);
        model.addAttribute("userType", usType);
        model.addAttribute("approval", approvals);
        return new ModelAndView("settlement/rebateApprovalForm");
    }

    @RequestMapping(value = "/saveRebateApproveLevels", method = RequestMethod.POST)
    public ModelAndView saveRebateApproval(@ModelAttribute("rebateApprovalForm") RebateApprovals formData, Model model) {
        String msg = "";
        Boolean isSave = false;
        try {
            isSave = settlmentService.saveRebateApproval(formData);
            if (isSave) {
                msg = "Approval Process Successfully";
            }
            model.addAttribute("successMessage", msg);
            return new ModelAndView("messages/success");
        } catch (Exception e) {
            e.printStackTrace();
            msg = "Approval Process Fail";
            model.addAttribute("errorMessage", msg);
            return new ModelAndView("messages/error");
        }
    }

    @RequestMapping(value = "/deleteRebateLoan/{loanId}", method = RequestMethod.GET)
    public ModelAndView deleteRebateLoan(@PathVariable("loanId") int loanId, HttpServletRequest request, Model model) {
        String message = "";
        ModelAndView modelAndView = null;
        int userId = userService.findByUserName();
        int userLogBranchId = settlmentService.getUserLogedBranch(userId);
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        systemDate = settlmentService.getSystemDate(userLogBranchId);
        boolean isDelete = false;
        try {
            RebateDetail detail = settlmentService.findRebateDetails(loanId);
            if (detail.getSystemDate().equals(systemDate)) {
                isDelete = settlmentService.deleteRebateLoan(loanId);
            } else {
                message = "Can't Remove this Rebate..";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
            if (isDelete) {
                message = "Successfully Removed";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Delete";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/deleteConform/{loanId}", method = RequestMethod.GET)
    public ModelAndView deleteConform(@PathVariable("loanId") int loanId, Model model) {
        model.addAttribute("loanID", loanId);
        model.addAttribute("message", "Are you sure to remove this rebate loan.!");
        model.addAttribute("methodType", 3);
        return new ModelAndView("messages/conformDialog");
    }

    @RequestMapping(value = "/loadAddLoanChequeForm", method = RequestMethod.GET)
    public ModelAndView loadAddLoanChequeForm(HttpServletRequest request, Model model) {
        try {
            List mBankDetailList = settlmentService.findBankList();
            LoanCheques addLoanChequeForm = new LoanCheques();
            model.addAttribute("bankList", mBankDetailList);
            model.addAttribute("addLoanChequeForm", addLoanChequeForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/addLoanChequeForm");
    }

    @RequestMapping(value = "/saveOrUpdateLoanCheque", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateLoanCheque(@ModelAttribute("addLoanChequeForm") LoanCheques formData, BindingResult result, HttpServletRequest request, Model model) {
        String successMsg = "";
        String errorMsg = "";
        try {
            formData.setBranchId(Integer.parseInt(request.getSession().getAttribute("branchId").toString()));
            boolean success = settlmentService.saveOrUpdateLoanCheque(formData);
            if (success) {
                successMsg = "Save Process Successfully";
            } else {
                errorMsg = "Save Process Failed";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("successMsg", successMsg);
        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("settlement/addLoanChequeForm");
    }

    @RequestMapping(value = "/findLoanChequeCount/{loanId}", method = RequestMethod.GET)
    @ResponseBody
    public int findLoanChequeCount(@PathVariable("loanId") int loanId, HttpServletRequest request, Model model) {
        int chequeCount = 0;
        try {
            List<LoanCheques> loanChequeses = settlmentService.getLoanChequeses(loanId);
            if (loanChequeses.size() > 0) {
                chequeCount = loanChequeses.size();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chequeCount;
    }

    @RequestMapping(value = "/getLoanCheques/{loanId}", method = RequestMethod.GET)
    public ModelAndView getLoanCheques(@PathVariable("loanId") int loanId, HttpServletRequest request, Model model) {
        List<MBankDetails> bankDetailses = null;
        try {
            List<LoanCheques> loanChequeses = settlmentService.getLoanChequeses(loanId);
            bankDetailses = settlmentService.findBankList();
            model.addAttribute("loanChequesList", loanChequeses);
            model.addAttribute("bankList", bankDetailses);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/viewLoanChequeList");
    }

    @RequestMapping(value = "/loadAddDepositMain", method = RequestMethod.GET)
    public ModelAndView loadAddDepositMain(HttpServletRequest request, Model model) {
        List<DebtorDeposit> debtorDeposits = null;
        try {
            debtorDeposits = settlmentService.findDebtorDeposits(Integer.parseInt(request.getSession().getAttribute("branchId").toString()));
            model.addAttribute("deposits", debtorDeposits);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/addDepositMain");
    }

    @RequestMapping(value = "/loadAddDepositForm", method = RequestMethod.GET)
    public ModelAndView loadAddDepositForm(HttpServletRequest request, Model model) {
        return new ModelAndView("settlement/addDepositForm");
    }

    @RequestMapping(value = "/saveOrUpdateDebtorDeposit", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateDebtorDeposit(@ModelAttribute("depositForm") DebtorDeposit debtorDeposit, BindingResult result, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        try {
            debtorDeposit.setBranchId(Integer.parseInt(request.getSession().getAttribute("branchId").toString()));
            boolean success = settlmentService.saveOrUpdateDebtorDeposit(debtorDeposit);
            if (success) {
                message = "Save Process Successfully";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Save Process Failed";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/findDeposit/{depositId}", method = RequestMethod.GET)
    public ModelAndView loadAddDepositForm(@PathVariable("depositId") int depositId, HttpServletRequest request, Model model) {
        try {
            DebtorDeposit debtorDeposit = settlmentService.findDebtorDeposit(depositId);
            model.addAttribute("debtorDeposit", debtorDeposit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/addDepositForm");
    }

    @RequestMapping(value = "/findDebtorDepositCount/{debtorId}/{accNo}", method = RequestMethod.GET)
    @ResponseBody
    public int findDebtorDepositCount(@PathVariable("debtorId") int debtorId, @PathVariable("accNo") String accNo, HttpServletRequest request, Model model) {
        int depositCount = 0;
        try {
            List<DebtorDeposit> debtorDeposits = settlmentService.findDebtorDeposits(debtorId, accNo);
            if (debtorDeposits.size() > 0) {
                depositCount = debtorDeposits.size();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return depositCount;
    }

    @RequestMapping(value = "/findDebtorDeposits/{debtorId}/{accNo}", method = RequestMethod.GET)
    public ModelAndView findDebtorDeposits(@PathVariable("debtorId") int debtorId, @PathVariable("accNo") String accNo, HttpServletRequest request, Model model) {
        try {
            List<DebtorDeposit> debtorDeposits = settlmentService.findDebtorDeposits(debtorId, accNo);
            model.addAttribute("debtorDeposits", debtorDeposits);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/viewDebtorDepositList");
    }

    @RequestMapping(value = "/saveOrUpdateLoanRescheduleDetails", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateLoanRescheduleDetails(@ModelAttribute("loanRescheduleForm") LoanRescheduleDetails lrd, HttpServletRequest request, BindingResult result, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            lrd.setBranchId(Integer.parseInt(branchId));
            boolean success = settlmentService.saveOrUpdateLoanRescheduleDetails(lrd);
            if (success) {
                message = "Reschedule Process Success.";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Reschedule Process Failed.";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadLoanRescheduleApprovalMain", method = RequestMethod.GET)
    public ModelAndView loadLoanRescheduleApprovalMain(HttpServletRequest request, Model model) {
        boolean isAdmin = false;
        try {
            int userId = userService.findByUserName();
            int userType = userService.findUserTypeByUserId(userId);
            if (userType == 1 || userType == 18) {
                isAdmin = true;
            }
            List<LoanRescheduleDetails> loanRescheduleDetails = settlmentService.findLoanRescheduleDetails();
            model.addAttribute("loanRescheduleDetails", loanRescheduleDetails);
            model.addAttribute("isAdmin", isAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/rescheduleApprovalMain");
    }

    @RequestMapping(value = "/checkLoanRescheduleApproval/{rescheduleId}", method = RequestMethod.GET)
    public ModelAndView checkLoanRescheduleApproval(@PathVariable("rescheduleId") int rescheduleId, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        try {
            int userId = userService.findByUserName();
            int userType = userService.findUserTypeByUserId(userId);
            if (userType == 1 || userType == 18 || userType == 7) {
                LoanRescheduleDetails loanRescheduleDetail = settlmentService.findLoanRescheduleDetail(rescheduleId);
                model.addAttribute("loanRescheduleDetail", loanRescheduleDetail);
                modelAndView = new ModelAndView("settlement/rescheduleApprovalForm");
            } else {
                model.addAttribute("rescheduleId", rescheduleId);
                modelAndView = new ModelAndView("settlement/rescheduleApprovalPswdForm");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadLoanRescheduleApprovalForm", method = RequestMethod.GET)
    public ModelAndView loadLoanRescheduleApprovalForm(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String rescheduleId = request.getParameter("rescheduleId");
        String pswd = request.getParameter("pswd");
        String approval = userService.findApprovalsByPassword(pswd);
        switch (approval) {
            case "yes": {
                LoanRescheduleDetails loanRescheduleDetail = settlmentService.findLoanRescheduleDetail(Integer.parseInt(rescheduleId));
                model.addAttribute("loanRescheduleDetail", loanRescheduleDetail);
                modelAndView = new ModelAndView("settlement/rescheduleApprovalForm");
                break;
            }
            case "noApp": {
                message = "You don't have approvals to delete";
                model.addAttribute("warningMessage", message);
                modelAndView = new ModelAndView("messages/warning");
                break;
            }
            default: {
                message = "Authentication Failed";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
                break;
            }
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadCorrectPaymentForDate", method = RequestMethod.GET)
    public ModelAndView loadCorrectPaymentForDate(Model model, HttpServletRequest request) {
        boolean message = false;
        boolean bySysDate = false;
        Object bID = request.getSession().getAttribute("branchId");
        String sDate = request.getParameter("sDate");
        String eDate = request.getParameter("eDate");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        List<StlmntReturnList> paymentList = settlmentService.getPaymentList(sDate, eDate, userWorkingBanchId);
        if (paymentList != null) {
            message = true;
        }
        model.addAttribute("sDate", sDate);
        model.addAttribute("eDate", eDate);
        model.addAttribute("bySysDate", bySysDate);
        model.addAttribute("message", message);
        model.addAttribute("paymentList", paymentList);
        return new ModelAndView("settlement/paymentCorrectionTable");
    }

    @RequestMapping(value = "/loadCorrectPaymentByAgreementNo", method = RequestMethod.GET)
    public ModelAndView loadCorrectPaymentByAgreementNo(Model model, HttpServletRequest request) {
        boolean message = false;
        boolean bySysDate = false;
        Object bID = request.getSession().getAttribute("branchId");
        String agreementNo = request.getParameter("agreementNo");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        List<StlmntReturnList> paymentList = settlmentService.getPaymentListForAgNo(agreementNo, userWorkingBanchId);
        if (paymentList != null) {
            message = true;
        }
        model.addAttribute("bySysDate", bySysDate);
        model.addAttribute("message", message);
        model.addAttribute("paymentList", paymentList);
        return new ModelAndView("settlement/paymentCorrectionTable");
    }

    @RequestMapping(value = "/loadCorrectPaymentByVehicleNo", method = RequestMethod.GET)
    public ModelAndView loadCorrectPaymentByVehicleNo(Model model, HttpServletRequest request) {
        boolean message = false;
        boolean bySysDate = false;
        Object bID = request.getSession().getAttribute("branchId");
        String vehicleNo = request.getParameter("vehicleNo");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        List<StlmntReturnList> paymentList = settlmentService.getPaymentListForVehNo(vehicleNo, userWorkingBanchId);
        if (paymentList != null) {
            message = true;
        }
        model.addAttribute("vehicleNo", vehicleNo);
        model.addAttribute("bySysDate", bySysDate);
        model.addAttribute("message", message);
        model.addAttribute("paymentList", paymentList);
        return new ModelAndView("settlement/paymentCorrectionTable");
    }

    @RequestMapping(value = "/loadCorrectPaymentByPayMode", method = RequestMethod.GET)
    public ModelAndView loadCorrectPaymentByPayMode(Model model, HttpServletRequest request) {
        boolean message = false;
        boolean bySysDate = false;
        Object bID = request.getSession().getAttribute("branchId");
        Integer payMode = Integer.parseInt(request.getParameter("payMode"));
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        int userId = userService.findByUserName();
        int userWorkingBanchId = userService.findUserWorkingBranch(userId, branchId);
        List<StlmntReturnList> paymentList = settlmentService.getPaymentListForPayMode(payMode, userWorkingBanchId);

        if (paymentList != null) {
            message = true;
        }
        model.addAttribute("vehicleNo", payMode);
        model.addAttribute("bySysDate", bySysDate);
        model.addAttribute("message", message);
        model.addAttribute("paymentList", paymentList);
        return new ModelAndView("settlement/paymentCorrectionTable");
    }

    @RequestMapping(value = "/loadSettlementTransferForm/{transferLoanId}", method = RequestMethod.GET)
    public ModelAndView loadSettlementTransferForm(@PathVariable("transferLoanId") int transferLoanId, Model model) {
        double overPayment = settlmentService.getOverPayAmount(transferLoanId);
        model.addAttribute("transferId", transferLoanId);
        model.addAttribute("overPayment", new BigDecimal(overPayment).setScale(2, RoundingMode.HALF_UP).doubleValue());
        return new ModelAndView("settlement/addSettlementTransfer");
    }

    @RequestMapping(value = "/addSettlementTransfer", method = RequestMethod.POST)
    public ModelAndView addSettlementTransfer(@ModelAttribute("settlementTransferForm") SettlementTransfer st, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        try {
            boolean flag = settlmentService.addSettlementTransfer(st);
            if (flag) {
                message = "Transfer Successfull!";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Transfer Failed!";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadOverpayrefund", method = RequestMethod.GET)
    public ModelAndView loadRefund(Model model) {
        return new ModelAndView("settlement/OverPaymentRefund");
    }

    @RequestMapping(value = "/loadRefundTable", method = RequestMethod.GET)
    public ModelAndView loadRefundTable(Model model, HttpServletRequest request) {
        try {
            String sDate = request.getParameter("sDate");
            String eDate = request.getParameter("eDate");
            int userId = userService.findByUserName();
            int userLogBranchId = settlmentService.getUserLogedBranch(userId);
            List<SettlementMain> paymentList = settlmentService.loadRefundTable(sDate, eDate, userLogBranchId);
            model.addAttribute("paymentList", paymentList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/Overpaidloantable");
    }

    @RequestMapping(value = "/loadRefundTablebyagno", method = RequestMethod.GET)
    public ModelAndView loadRefundTableByAgreeNo(Model model, HttpServletRequest request) {
        try {
            String agreementNo = request.getParameter("agreementNo");
            if (!agreementNo.isEmpty()) {
                int userId = userService.findByUserName();
                int userLogBranchId = settlmentService.getUserLogedBranch(userId);
                List<SettlementMain> paymentList = settlmentService.loadRefundTablebyAgNo(agreementNo, userLogBranchId);
                model.addAttribute("paymentList", paymentList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/Overpaidloantable");
    }

    @RequestMapping(value = "/loadRefundTableByVehNo", method = RequestMethod.GET)
    public ModelAndView loadRefundTableByVehNo(Model model, HttpServletRequest request) {
        try {
            String vehicleNo = request.getParameter("vehicleNo");
            if (!vehicleNo.isEmpty()) {
                int userId = userService.findByUserName();
                int userLogBranchId = settlmentService.getUserLogedBranch(userId);
                List<SettlementMain> paymentList = settlmentService.loadRefundTablebyVehNo(vehicleNo, userLogBranchId);
                model.addAttribute("paymentList", paymentList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/Overpaidloantable");
    }

    @RequestMapping(value = "/loadRefundTableByCustomer", method = RequestMethod.GET)
    public ModelAndView loadRefundTableByCustomer(Model model, HttpServletRequest request) {
        try {
            String customerName = request.getParameter("customerName");
            if (!customerName.isEmpty()) {
                int userId = userService.findByUserName();
                int userLogBranchId = settlmentService.getUserLogedBranch(userId);
                List<SettlementMain> paymentList = settlmentService.loadRefundTablebyCustomer(customerName, userLogBranchId);
                model.addAttribute("paymentList", paymentList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/Overpaidloantable");
    }

    @RequestMapping(value = "/loadoprefundform/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadOPRefundForm(@PathVariable("loanId") int loanId, Model model) {
        try {
            SettlementMain sm = settlmentService.getSettlementLoanheaderDebtorDetailsByLoanID(loanId);
            model.addAttribute("settlementmain", sm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("settlement/OverPaymentRefundForm");
    }

    @RequestMapping(value = "/saveRefund", method = RequestMethod.GET)
    public ModelAndView saveRefund(@ModelAttribute("PaymentRefundForm") SettlementRefund formdata, Model model) {
        String successMsg = "";
        String errorMsg = "";
        try {
            int userId = userService.findByUserName();
            int userLogBranchId = settlmentService.getUserLogedBranch(userId);
            Date sysDate = settlmentService.getSystemDate(userLogBranchId);
            boolean result1 = settlmentService.saveRefund(formdata);
            boolean result2 = settlmentService.updateSettlementMainonRefund(formdata.getLoanId(), formdata.getRefundAmount(), formdata.getReceiptNo(), sysDate);
            if (result1 && result2) {
                successMsg = "Rs " + formdata.getRefundAmount() + " Refunded ";
                model.addAttribute("successMessage", successMsg);
                return new ModelAndView("messages/success");
            } else {
                errorMsg = "Payment Not Removed11..!";
                model.addAttribute("errorMessage", errorMsg);
                return new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorMsg = "Payment Not Removed..!";

            model.addAttribute("errorMessage", errorMsg);
            return new ModelAndView("messages/error");
        }
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = "/searchByMemNo", method = RequestMethod.GET)
    public ModelAndView searchByMemNo(HttpServletRequest request, Model model) {
        String memNo = request.getParameter("memNo");
        Object bID = request.getSession().getAttribute("branchId");
        System.out.println("==== member num == " + memNo);
        List<DebtorHeaderDetails> debtor = loanService.findByMemNo(memNo, bID);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("settlement/viewSearchCustomer", "debtorList", debtor);
    }

}

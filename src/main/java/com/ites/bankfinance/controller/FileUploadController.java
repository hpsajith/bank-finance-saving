/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.viewLoan;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ites.bankfinance.model.LoanApprovedLevels;
import com.ites.bankfinance.model.LoanCheckList;
import com.ites.bankfinance.model.RecoveryVisitsDetails;
import com.ites.bankfinance.model.SeizeYardRegisterCheklist;
import com.ites.bankfinance.service.DebtorService;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.RecoveryService;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.hibernate.annotations.common.util.impl.LoggerFactory.logger;
import org.omg.PortableServer.REQUEST_PROCESSING_POLICY_ID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FileUploadController {

    @Autowired
    LoanService loanService;

    @Autowired
    DebtorService debtorService;

    @Autowired
    RecoveryService recoveryService;

    @RequestMapping(value = "/uploadDocument", method = RequestMethod.POST)
    public ModelAndView handleFileUpload(MultipartHttpServletRequest request, Model model)
            throws Exception {
        Iterator<String> itrator = request.getFileNames();
        MultipartFile multiFile = request.getFile(itrator.next());
        String name = request.getParameter("name");
        String loanId = request.getParameter("loanId");
        String listId = request.getParameter("listId");
        int retValue;
        String message = "";
        if (!multiFile.isEmpty()) {
            retValue = 0;
            try {
                String fileName = multiFile.getOriginalFilename();
                String extension = getFileExtension(fileName);
                String path = ("C:/tmpFile/uploads");

                //making directories for our required path.
                byte[] bytes = multiFile.getBytes();
                File directory = new File(path + "/" + loanId);
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // saving the file
                String filePath = directory.getAbsolutePath() + System.getProperty("file.separator") + name + "." + extension;
                File file = new File(filePath);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(file));
                stream.write(bytes);
                stream.close();

                //save listIds to database
                retValue = loanService.saveLoanCheckList(Integer.parseInt(listId), Integer.parseInt(loanId), filePath, name);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                throw new Exception("Error while loading the file");
            }
        } else {
            retValue = -1;
        }
        if (retValue > 0) {
            model.addAttribute("status", "1");
            message = "File Uploaded Successfully";
            return new ModelAndView("messages/success", "successMessage", message);
        } else if (retValue == 0) {
            model.addAttribute("status", "0");
            message = "File Uploaded Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        } else {
            model.addAttribute("status", "2");
            message = "Please Select a valid File";
            return new ModelAndView("messages/warning", "warningMessage", message);
        }
    }

    public String toJson(Object data) {
        ObjectMapper mapper = new ObjectMapper();
        StringBuilder builder = new StringBuilder();
        try {
            builder.append(mapper.writeValueAsString(data));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return builder.toString();
    }

    public String getFileExtension(String file) {
        String fileName = file;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    @RequestMapping(value = "/viewDocument/{listId}", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] viewDocument(@PathVariable("listId") int list_id, HttpServletResponse response, Model model) {

        BufferedImage img;
        byte[] imageInByte = null;

        try {
            File file = loanService.findDocumentById(list_id);
            img = ImageIO.read(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            imageInByte = baos.toByteArray();
            baos.flush();
            baos.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageInByte;
    }

    @RequestMapping(value = "/viewRecoveryReport/{reportId}", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] viewRecoveryReport(@PathVariable("reportId") int report_id, Model model) {

        BufferedImage img;
        byte[] imageInByte = null;

        try {
//            File file = loanService.findRecoveryReportById(report_id);
            File file = new File("");
            img = ImageIO.read(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            imageInByte = baos.toByteArray();
            baos.flush();
            baos.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageInByte;
    }

    @RequestMapping(value = "/image", method = RequestMethod.GET, produces = "image/jpg")
    public @ResponseBody
    byte[] getFile() {
        try {
            // Retrieve image from the classpath.
            InputStream is = this.getClass().getClassLoader().getResourceAsStream("..//Image/aaa.jpg");

            // Prepare buffered image.
            BufferedImage img = ImageIO.read(is);

            // Create a byte array output stream.
            ByteArrayOutputStream bao = new ByteArrayOutputStream();

            // Write to output stream
            ImageIO.write(img, "jpg", bao);

            return bao.toByteArray();
        } catch (IOException e) {
//        logger.error(e);
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/recoveryOfficerReport", method = RequestMethod.POST)
    public ModelAndView saverecoveryOfficerReport(MultipartHttpServletRequest request) throws Exception {
        String message = "";

        Iterator<String> itrator = request.getFileNames();
        MultipartFile multiFile = request.getFile(itrator.next());
        String comment = request.getParameter("comment");
        String loanId = request.getParameter("loanId");
        String pk = request.getParameter("pk");
        String app_level = request.getParameter("appLevel");
        String name = "refer_report";
        int retValue;
        if (!multiFile.isEmpty()) {
            retValue = 0;
            try {
                String fileName = multiFile.getOriginalFilename();
                String extension = getFileExtension(fileName);
                String path = ("C:/tmpFile/uploads/" + loanId);

                //making directories for our required path.
                byte[] bytes = multiFile.getBytes();
                File directory = new File(path + "/Report");
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                //saving the file
                String filePath = directory.getAbsolutePath() + System.getProperty("file.separator") + name + "." + extension;
                File file = new File(filePath);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(file));
                stream.write(bytes);
                stream.close();

                //save data to database
                message = "Successfully Saved";
                LoanApprovedLevels app = new LoanApprovedLevels();
                app.setApprovedComment(comment);
                app.setApprovedLevel(Integer.parseInt(app_level));
                app.setLoanId(Integer.parseInt(loanId));
//              app.setReferenceReportPath(filePath);
                if (pk != null && !pk.equals("")) {
                    app.setPk(Integer.parseInt(pk));
                    message = "Successfully Updated";
                }
                retValue = loanService.saveLoanApproval(app);

                return new ModelAndView("messages/success", "successMessage", message);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error while loading the file");
            }
        } else {
            message = "Please select a File";
            return new ModelAndView("messages/warning", "warningMessage", message);
        }
    }

    @RequestMapping(value = "/uploadProfilePicture", method = RequestMethod.POST)
    @ResponseBody
    public boolean uploadProfilePicture(MultipartHttpServletRequest request)
            throws Exception {

        int customerId = Integer.parseInt(request.getParameter("customerId"));
        boolean save = false;
        Iterator<String> itrator = request.getFileNames();

        if (itrator.hasNext()) {
            MultipartFile multiFile = request.getFile(itrator.next());

            if (!multiFile.isEmpty()) {
                try {
                    String fileName = multiFile.getOriginalFilename();
                    String extension = getFileExtension(fileName);
                    String path = ("C:/tmpFile/uploads/profilePictures");

                    //making directories for our required path.
                    byte[] bytes = multiFile.getBytes();
                    File directory = new File(path);
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }

                    if (customerId == 0) {
                        customerId = debtorService.findMaxCustomerId();
                    }

                    // saving the file
                    String filePath = directory.getAbsolutePath() + System.getProperty("file.separator") + customerId + "." + extension;
                    File file = new File(filePath);
                    BufferedOutputStream stream = new BufferedOutputStream(
                            new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();

                    //save listIds to database
                    save = debtorService.saveCustomerProfilePicture(customerId, filePath);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    throw new Exception("Error while loading the file");
                }
            } else {
                save = false;
            }
            return save;
        } else {
            save = debtorService.saveCustomerProfilePicture(customerId, "");
            return save;
        }

    }

    //view profile image    
    @RequestMapping(value = "/viewProfilePicture/{cusId}", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] viewProfilePicture(@PathVariable("cusId") int cusId, Model model) {

        BufferedImage img;
        byte[] imageInByte = null;

        try {
//            File file = loanService.findRecoveryReportById(report_id);
            File file = debtorService.findProfilePicById(cusId);
            img = ImageIO.read(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            imageInByte = baos.toByteArray();
            baos.flush();
            baos.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageInByte;
    }

    //thushan
    // Save Recovery Visit Report
    @RequestMapping(value = "/recoveryVisitReport", method = RequestMethod.POST)
    public ModelAndView saveRecoveryVisitReport(MultipartHttpServletRequest request) throws Exception {
        String message = "";

        Iterator<String> itrator = request.getFileNames();
        MultipartFile multiFile = request.getFile(itrator.next());
        String loanId = request.getParameter("loanId");
        String recId = request.getParameter("recId");
        String comment = request.getParameter("comment");

        if (!multiFile.isEmpty()) {
            try {
                String fileName = multiFile.getOriginalFilename();
                String extension = getFileExtension(fileName);
                String path = ("C:/tmpFile/uploads/Report");

                //make directory for required path.
                byte[] bytes = multiFile.getBytes();
                File directory = new File(path + "/" + loanId);
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                //save the file
                String filePath = directory.getAbsolutePath() + System.getProperty("file.separator") + fileName + "." + extension;
                File file = new File(filePath);
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                stream.write(bytes);
                stream.close();

                //get Recovery Visit Details to given Loan Id and Recovery Id
                RecoveryVisitsDetails rvd = recoveryService.findRecoveryVisitDetail(Integer.parseInt(recId), Integer.parseInt(loanId));

                //save Recovery Visit Details to Database
                if (rvd != null) {
                    rvd.setVisitComment(comment);
                    rvd.setFinalComment(comment);
                    rvd.setVisitStatus(3);
                    rvd.setReportPath(filePath);
                    rvd.setSubmitDate(Calendar.getInstance().getTime());
                    rvd.setActionTime(Calendar.getInstance().getTime());
                    recoveryService.saveOrUpdateRecoveryVisitDetails(rvd);
                    message = "Successfully Saved";
                }
                return new ModelAndView("messages/success", "successMessage", message);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Error while loading the file");
            }
        } else {
            message = "Please select a File";
            return new ModelAndView("messages/warning", "warningMessage", message);
        }
    }

    // view recovery visit report
    @RequestMapping(value = "/viewRecoveryVisitReport", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] viewRecoveryVReport(HttpServletRequest request, Model model) {
        String loanId = request.getParameter("loanId");
        String recId = request.getParameter("recId");
        BufferedImage img;
        byte[] imageInByte = null;

        try {
            File file = null;
            RecoveryVisitsDetails rvd = recoveryService.findRecoveryVisitDetail(Integer.parseInt(recId), Integer.parseInt(loanId));
            if (rvd != null) {
                String rPath = rvd.getReportPath();
                if (rPath != null) {
                    file = new File(rPath);
                }
            }
            if (file != null) {
                img = ImageIO.read(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(img, "jpg", baos);
                imageInByte = baos.toByteArray();
                baos.flush();
                baos.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageInByte;
    }
    
    // This method is used to upload article images for Seize Orders
    @RequestMapping(value = "/uploadArticleImage", method = RequestMethod.POST)
    @ResponseBody
    public boolean uploadArticleImage(MultipartHttpServletRequest request, Model model) throws Exception {
        boolean upload = false;
        Iterator<String> itrator = request.getFileNames();
        MultipartFile multiFile = request.getFile(itrator.next());
        String docId = request.getParameter("docId");
        String seizeOrderId = request.getParameter("seizeOrderId");
        String yardRegId = request.getParameter("yardRegId");
        String name = request.getParameter("description");
        String branchId = request.getSession().getAttribute("branchId").toString();

        try {
            if (!multiFile.isEmpty()) {
                String fileName = multiFile.getOriginalFilename();
                String filePath = "C:/tmpFile/uploads/Yard_Register/Seize_Order-" + seizeOrderId + "/" + "Article_Images";
                // make directory for required path.
                File directory = new File(filePath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                // save the file
                filePath = filePath + "/" + fileName;
                multiFile.transferTo(new File(filePath));

                if (seizeOrderId != null) {
                    if (!docId.equals("0")) {
                        SeizeYardRegisterCheklist yardRegisterCheklist = recoveryService.findYardRegisterCheklist(Integer.parseInt(docId), Integer.parseInt(seizeOrderId));
                        if (yardRegisterCheklist != null) {
                            yardRegisterCheklist.setDescription(name);
                            yardRegisterCheklist.setFilePath(filePath);
                            upload = recoveryService.saveOrUpdateYardRegisterCheckList(yardRegisterCheklist);
                        }
                    } else {
                        SeizeYardRegisterCheklist yardRegisterCheklist = new SeizeYardRegisterCheklist();
                        yardRegisterCheklist.setBranchId(Integer.parseInt(branchId));
                        yardRegisterCheklist.setDescription(name);
                        yardRegisterCheklist.setDocTypeId(1); // Article Image
                        yardRegisterCheklist.setFilePath(filePath);
                        yardRegisterCheklist.setSeizeOrderId(Integer.parseInt(seizeOrderId));
                        yardRegisterCheklist.setYardRegId(Integer.parseInt(yardRegId));
                        upload = recoveryService.saveOrUpdateYardRegisterCheckList(yardRegisterCheklist);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return upload;
    }

    // This method is used to upload inspection report for Seize Orders
    @RequestMapping(value = "/uploadInspectionReport", method = RequestMethod.POST)
    @ResponseBody
    public boolean uploadInspectionReport(MultipartHttpServletRequest request, Model model) throws Exception {
        boolean upload = false;
        Iterator<String> itrator = request.getFileNames();
        MultipartFile multiFile = request.getFile(itrator.next());
        String docId = request.getParameter("docId");
        String seizeOrderId = request.getParameter("seizeOrderId");
        String yardRegId = request.getParameter("yardRegId");
        String name = request.getParameter("description");
        String branchId = request.getSession().getAttribute("branchId").toString();

        try {
            if (!multiFile.isEmpty()) {
                String fileName = multiFile.getOriginalFilename();
                String filePath = "C:/tmpFile/uploads/Yard_Register/Seize_Order-" + seizeOrderId + "/" + "Inspection_Report";
                // make directory for required path.
                File directory = new File(filePath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                // save the file
                filePath = filePath + "/" + fileName;
                multiFile.transferTo(new File(filePath));

                if (seizeOrderId != null) {
                    if (!docId.equals("0")) {
                        SeizeYardRegisterCheklist yardRegisterCheklist = recoveryService.findYardRegisterCheklist(Integer.parseInt(docId), Integer.parseInt(seizeOrderId));
                        if (yardRegisterCheklist != null) {
                            yardRegisterCheklist.setDescription(name);
                            yardRegisterCheklist.setFilePath(filePath);
                            upload = recoveryService.saveOrUpdateYardRegisterCheckList(yardRegisterCheklist);
                        }
                    } else {
                        SeizeYardRegisterCheklist yardRegisterCheklist = new SeizeYardRegisterCheklist();
                        yardRegisterCheklist.setBranchId(Integer.parseInt(branchId));
                        yardRegisterCheklist.setDescription(name);
                        yardRegisterCheklist.setDocTypeId(2); // Inspection Report
                        yardRegisterCheklist.setFilePath(filePath);
                        yardRegisterCheklist.setSeizeOrderId(Integer.parseInt(seizeOrderId));
                        yardRegisterCheklist.setYardRegId(Integer.parseInt(yardRegId));
                        upload = recoveryService.saveOrUpdateYardRegisterCheckList(yardRegisterCheklist);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return upload;
    }

    // This method is used to view article images 0f Seize Orders
    @RequestMapping(value = "/viewArticleImage", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] viewArticleImage(HttpServletRequest request, Model model) {
        String docId = request.getParameter("docId");
        String seizeOrderId = request.getParameter("seizeOrderId");
        BufferedImage img;
        byte[] imageInByte = null;

        try {
            File file = null;
            SeizeYardRegisterCheklist document = recoveryService.findYardRegisterCheklist(Integer.parseInt(docId), Integer.parseInt(seizeOrderId));
            if (document != null) {
                String fPath = document.getFilePath();
                if (fPath != null) {
                    file = new File(fPath);
                }
            }
            if (file != null) {
                img = ImageIO.read(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(img, "jpg", baos);
                imageInByte = baos.toByteArray();
                baos.flush();
                baos.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageInByte;
    }
    
    // This method is used to view inspection report 0f Seize Orders
    @RequestMapping(value = "/viewInspectionReport", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] viewInspectionReport(HttpServletRequest request, Model model) {
        String docId = request.getParameter("docId");
        String seizeOrderId = request.getParameter("seizeOrderId");
        BufferedImage img;
        byte[] imageInByte = null;

        try {
            File file = null;
            SeizeYardRegisterCheklist document = recoveryService.findYardRegisterCheklist(Integer.parseInt(docId), Integer.parseInt(seizeOrderId));
            if (document != null) {
                String fPath = document.getFilePath();
                if (fPath != null) {
                    file = new File(fPath);
                }
            }
            if (file != null) {
                img = ImageIO.read(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(img, "jpg", baos);
                imageInByte = baos.toByteArray();
                baos.flush();
                baos.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageInByte;
    }    

    

}

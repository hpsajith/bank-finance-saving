/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.ApprovalDetailsModel;
import com.bankfinance.form.DirectDebitCharge;
import com.bankfinance.form.GuranterDetailsModel;
import com.bankfinance.form.OtherChargseModel;
import com.bankfinance.form.PaymentDetails;
import com.bankfinance.form.RecoveryHistory;
import com.bankfinance.form.RecoveryLoanList;
import com.bankfinance.form.ReturnCheckPaymentList;
import com.bankfinance.form.TransactionList;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.InstallmentReturnList;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.MSeizerDetails;
import com.ites.bankfinance.model.MSupplier;
import com.ites.bankfinance.model.RecoveryLetterDetail;
import com.ites.bankfinance.model.RecoveryLetters;
import com.ites.bankfinance.model.RecoveryShift;
import com.ites.bankfinance.model.RecoveryVisitsDetails;
import com.ites.bankfinance.model.ReturnList;
import com.ites.bankfinance.model.SeizeOrder;
import com.ites.bankfinance.model.SeizeOrderApproval;
import com.ites.bankfinance.model.SeizeYardRegister;
import com.ites.bankfinance.model.SeizeYardRegisterCheklist;
import com.ites.bankfinance.model.SettlementAddNewPayment;
import com.ites.bankfinance.reportManager.DBFacade;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.DebtorService;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.RecoveryService;
import com.ites.bankfinance.service.ReportService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ITESS
 */
@Controller
@RequestMapping(value = "/RecoveryController")
public class RecoveryController {

    @Autowired
    RecoveryService recoveryService;

    @Autowired
    UserService userService;

    @Autowired
    LoanService loanService;

    @Autowired
    AdminService adminService;

    @Autowired
    SettlmentService settlmentService;

    @Autowired
    MasterDataService masterService;

    @Autowired
    ReportService reportService;

    @Autowired
    DebtorService debtorService;

    private int userLogBranchId;

    DecimalFormat formatter = new DecimalFormat("#,###.00");
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = "/loadFiledOfficerPage", method = RequestMethod.GET)
    public ModelAndView loadFiledOfficerPage(Model model) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int userId = userService.findByUserName();
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.add(Calendar.DATE, +3);
        String searchByDue = sdf.format(calendar.getTime());

        List<viewLoan> loanList = recoveryService.getLoanListForDue(null, null, null, null, userLogBranchId);

        model.addAttribute("searchByDue", searchByDue);
        model.addAttribute("loanList", loanList);
        model.addAttribute("systemDate", dt);

        return new ModelAndView("recoveryField/fieldOfficerPage");
    }

    @RequestMapping(value = "/loadRecoveryArrearsLoans", method = RequestMethod.GET)
    public ModelAndView loadRecoveryArrearsLoans(Model model) {
        List<RecoveryLoanList> loans = recoveryService.loadLetterPostingLoans();
        List<RecoveryLetters> letters = recoveryService.findAllLetters();
        model.addAttribute("letters", letters);
        return new ModelAndView("recoveryField/postingLetters", "loanList", loans);
    }

    //    loadRecoveryLetterPostedLoans
    @RequestMapping(value = "/loadRecoveryLetterPostedLoans", method = RequestMethod.GET)
    public ModelAndView loadRecoveryLetterPostedLoans(Model model) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int userId = userService.findByUserName();
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.add(Calendar.DATE, +3);
        String searchByDue = sdf.format(calendar.getTime());

        List<viewLoan> loanList = recoveryService.getArrearsLoanListForDue(null, null, null, searchByDue, userLogBranchId);

        model.addAttribute("searchByDue", searchByDue);
        model.addAttribute("loanList", loanList);
        model.addAttribute("systemDate", dt);
        model.addAttribute("arrearsLoans", "true");

        return new ModelAndView("recoveryField/fieldOfficerPage");
    }

    @RequestMapping(value = "/searchByName", method = RequestMethod.GET)
    public ModelAndView searchByName(HttpServletRequest request, Model model) {
        String name = null, agreementNo = null, nic = null, due = null, loanArrears = null;
        if (request.getParameter("name") != null) {
            name = request.getParameter("name");
        }
        if (request.getParameter("agry") != null) {
            agreementNo = request.getParameter("agry");
        }
        if (request.getParameter("nic") != null) {
            nic = request.getParameter("nic");
        }
        if (request.getParameter("searchByDue") != null) {
            due = request.getParameter("searchByDue");
        }
        if (request.getParameter("arrearsLoans") != null) {
            loanArrears = request.getParameter("arrearsLoans");
        }

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }

        List<viewLoan> loanList;
        if (loanArrears.equals("true")) {
            loanList = recoveryService.getArrearsLoanListForDue(name, agreementNo, nic, due, branchId);
        } else {
            loanList = recoveryService.getLoanListForDue(name, agreementNo, nic, due, branchId);
        }
        model.addAttribute("loanList", loanList);
        model.addAttribute("name", name);
        model.addAttribute("agreementNo", agreementNo);
        model.addAttribute("nic", nic);
        return new ModelAndView("recoveryField/fieldOfficerSearchPage");
    }

    @RequestMapping(value = "/recoveryProcess", method = RequestMethod.GET)
    public ModelAndView loadRecoveryProcess(HttpServletRequest request, Model model) {

        String searchByDue = null;
        if (request.getParameter("searchByDue") != null) {
            searchByDue = request.getParameter("searchByDue");
        }
        int loanId = 0;
        if (request.getParameter("loanID") != null) {
            loanId = Integer.parseInt(request.getParameter("loanID"));
        }
        Integer instmntId = null;
        RecoveryShift recoveryShift = new RecoveryShift();
        if (request.getParameter("instmntId") != null) {
            instmntId = Integer.parseInt(request.getParameter("instmntId"));
            recoveryShift = recoveryService.getRecoveryShift(instmntId);
        }
        List<RecoveryShift> recoveryShiftList = recoveryService.getRecoveryShiftDates(loanId);

        boolean msgDateShifted = false;
        if (request.getParameter("msgDateShifted") != null) {
            if (Integer.parseInt(request.getParameter("msgDateShifted")) == 1) {
                msgDateShifted = true;
            }
        }

        List otherChargesList = loanService.findOtherChargesByLoanId(loanId);//from loan other charges
        List<InstallmentReturnList> installmentsList = settlmentService.getDebitCreditInstallmentList(loanId);
        List<ReturnList> openningBalanceList = settlmentService.findOpenningBalance(loanId);
        SettlementAddNewPayment formData = new SettlementAddNewPayment();

        double totalOtherCharges = settlmentService.getSumOfOtherCherges(otherChargesList);
        double totalOpenningD = settlmentService.getSumOfOpenningBalance(openningBalanceList);
        double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(loanId);

        double paidArrears = settlmentService.getPaidArrears(loanId);
        double paidOtherCharges = settlmentService.getPaidOtherCharges(loanId);
        double paidDownPayment = settlmentService.getPaidDownPayment(loanId);
        double paidInsurance = settlmentService.getPaidInsuarance(loanId);
        double overPayAmount = settlmentService.getOverPayAmount(loanId);

        double totalDownPayment = settlmentService.getTotalDownPayment(loanId);//from loan header detail
        double totalInsurance = settlmentService.getTotalInsuarance(loanId);
        double installmentBalance = recoveryService.getInstalmentBalance(loanId);

        int userId = userService.findByUserName();
        LoanHeaderDetails loan = loanService.searchLoanByLoanId(loanId);
        String loanEndDate = this.calculateLoanEndDate(loan);
        String loanPeriodTypeId = loanService.findLoanPeriodType(loan.getLoanPeriodType());

        double balance = 0;

        double OtherChargesBalance = totalOtherCharges - paidOtherCharges;
        double downPaymentBalance = OtherChargesBalance + totalDownPayment - paidDownPayment;// Down payment with Other charges
        double totalOpeningBalance = (totalOpenningD + openningBalanceWithoutODI) - paidArrears;
        double insuranceBalance = totalInsurance - paidInsurance;

        double totalArreas = totalOpeningBalance;
        balance = OtherChargesBalance + totalOpeningBalance + downPaymentBalance + insuranceBalance + installmentBalance;

        boolean msgArrears = false;
        if (totalArreas > 0) {
            msgArrears = true;
        }

        List<OtherChargseModel> extraCharge = masterService.findExtraChargesBySubLoanId(loan.getLoanType());

        model.addAttribute("LoanPeriodType", loanPeriodTypeId);
        model.addAttribute("loan", loan);
        model.addAttribute("searchByDue", searchByDue);
        model.addAttribute("loanEndDate", loanEndDate);
        model.addAttribute("userId", userId);
        model.addAttribute("instmntId", instmntId);
        model.addAttribute("totalArreas", totalArreas);
        model.addAttribute("msgArrears", msgArrears);
        model.addAttribute("msgDateShifted", msgDateShifted);
        model.addAttribute("recoveryShiftList", recoveryShiftList);
        model.addAttribute("extraCharge", extraCharge);
        if (recoveryShift != null) {
            model.addAttribute("shiftedDate", recoveryShift.getShiftDate());
            model.addAttribute("remark", recoveryShift.getRemark());
        }
        model.addAttribute("balance", balance);
        return new ModelAndView("recoveryField/recoveryProcess");

    }

    private String calculateLoanEndDate(LoanHeaderDetails loan) {
        String endDate = "";
        try {
            Date loanDate = loan.getLoanDate();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            c.setTime(loanDate);

            if (loan.getLoanPeriodType() == 1) {
                c.add(Calendar.MONTH, loan.getLoanPeriod());
            }
            if (loan.getLoanPeriodType() == 2) {
                c.add(Calendar.YEAR, loan.getLoanPeriod());
            }
            if (loan.getLoanPeriodType() == 3) {
                c.add(Calendar.DATE, loan.getLoanPeriod());
            }
            endDate = sdf.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return endDate;
    }

//    /saveRecoveryShiftForm
    @RequestMapping(value = "/saveRecoveryShiftForm", method = RequestMethod.GET)
    public ModelAndView saveRecoveryShiftForm(@ModelAttribute("RecoveryShiftForm") RecoveryShift formData, Model model, HttpServletRequest request) {

        boolean status = recoveryService.addRecoveryShiftDate(formData);

//        ModelAndView mdv = popupSuccess(status,model);
        String searchByDue = formData.getDueDate();
        int loanID = formData.getLoanId();

        int userId = userService.findByUserName();
        LoanHeaderDetails loan = loanService.searchLoanByLoanId(loanID);
        String loanEndDate = this.calculateLoanEndDate(loan);
        String loanPeriodTypeId = loanService.findLoanPeriodType(loan.getLoanPeriodType());

        model.addAttribute("LoanPeriodType", loanPeriodTypeId);
        model.addAttribute("loan", loan);
        model.addAttribute("searchByDue", searchByDue);
        model.addAttribute("loanEndDate", loanEndDate);
        model.addAttribute("userId", userId);
        model.addAttribute("remark", formData.getRemark());
        model.addAttribute("shiftDate", formData.getShiftDate());

        String msg = "";
        if (status) {
            msg = "Saved Successfully";
            return new ModelAndView("messages/success", "successMessage", msg);
        } else {
            msg = "Failed Save Process";
            return new ModelAndView("messages/warning", "warningMessage", msg);
        }
    }

//    savePrintLetterInfo
    @RequestMapping(value = "/savePrintLetterInfo", method = RequestMethod.GET)
    public void savePrintLetterInfo(@ModelAttribute("RecoveryShiftForm") RecoveryShift forData, HttpServletRequest request, HttpServletResponse response) {

        if (forData != null) {
//        int loanId = recoveryService.savePrintLetterInfo(formData);
        }
    }

    @RequestMapping(value = "/generateLetter", method = RequestMethod.GET)
    public void generateLetter(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        int recoveryId = Integer.parseInt(request.getParameter("rletterId"));
        int loanId = Integer.parseInt(request.getParameter("loanId_l"));
        int letterId = Integer.parseInt(request.getParameter("letterId_l"));
        try {
            int loanReportType = recoveryService.findLetterTypeByLoanType(loanId);
            String invoiceDetails = recoveryService.findInvoiceDetails(loanId);
            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("loanId", loanId);
            params.put("REPORT_CONNECTION", c);
            String type = "";

            if (loanReportType == 1) {
                params.put("invoice", invoiceDetails);
                type = "typeB";
                if (letterId == 2) {
                    Date prevDate = recoveryService.findPreviousDate(1, loanId);
                    params.put("prevDate", prevDate);
                }
            } else {
                type = "typeA";
            }

            URL url = this.getClass().getClassLoader().getResource("../reports/letters/" + type + "/letter_" + letterId);
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/letters/" + type + "/letter_" + letterId + "/reminder" + letterId + ".jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    loadCheckPayment

    @RequestMapping(value = "/loadCheckPayment", method = RequestMethod.GET)
    public ModelAndView loadCheckPayment(HttpServletRequest request, Model model) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int userId = userService.findByUserName();
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);

        String fromDate = sdf.format(dt);
        String toDate = sdf.format(dt);

        List<ReturnCheckPaymentList> checkPaymentList = recoveryService.getCheckPayments(fromDate, toDate);
        model.addAttribute("systemDate", dt);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("checkPaymentList", checkPaymentList);
        return new ModelAndView("recoveryField/loadCheckPayments");
    }

    @RequestMapping(value = "/searchCheqPayments", method = RequestMethod.GET)
    public ModelAndView searchCheqPayments(HttpServletRequest request, Model model) {

        int userId = userService.findByUserName();
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);

        String fromDate = null;
        if (request.getParameter("fromDate") != null) {
            fromDate = request.getParameter("fromDate");
        }
        String toDate = null;
        if (request.getParameter("toDate") != null) {
            toDate = request.getParameter("toDate");
        }
        List<ReturnCheckPaymentList> checkPaymentList = recoveryService.getCheckPayments(fromDate, toDate);

        model.addAttribute("systemDate", dt);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("checkPaymentList", checkPaymentList);
        return new ModelAndView("recoveryField/loadSearchCheckPayments");
    }

//    checkReturnProcess
    @RequestMapping(value = "/checkReturnProcess", method = RequestMethod.GET)
    public ModelAndView checkReturnProcess(HttpServletRequest request, Model model) {

        int checkDetaiId = 0;
        boolean priviledge = false;
        if (request.getParameter("checkDetaiId") != null) {
            checkDetaiId = Integer.parseInt(request.getParameter("checkDetaiId"));
        }

        int userId = userService.findByUserName();
        int userType = userService.findUserTypeByUserId(userId);

        if (userType == 1) {
            priviledge = true;
        }

        model.addAttribute("priviledge", priviledge);
        model.addAttribute("checkDetaiId", checkDetaiId);
        return new ModelAndView("recoveryField/checkReturnProcess");
    }

//    saveCheckReturn
    @RequestMapping(value = "/saveCheckReturn", method = RequestMethod.GET)
    public ModelAndView saveCheckReturn(HttpServletRequest request, Model model) {

        boolean status = false;
        int userId = userService.findByUserName();
        int checkDetaiId = 0;
        String successMsg = null;
        String errorMsg = null;
        if (request.getParameter("checkDetaiId") != null) {
            checkDetaiId = Integer.parseInt(request.getParameter("checkDetaiId"));
        }
        String remark = null;
        if (request.getParameter("remark") != null) {
            remark = request.getParameter("remark");
        }

        status = recoveryService.updateChequePayment(checkDetaiId, remark, userId);

        if (status) {
            successMsg = "Successfully Saved..!";
            model.addAttribute("successMsg", successMsg);
            return new ModelAndView("messages/success");
        }
        errorMsg = "Payment Not Removed..!";

        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("messages/error");
    }

    //load letter
    @RequestMapping(value = "/loadLetterHistory", method = RequestMethod.GET)
    public ModelAndView loadLetterHistory(HttpServletRequest request, Model model) {

        int loanId = Integer.parseInt(request.getParameter("loanId"));
        List<RecoveryHistory> detail = recoveryService.findRecoveryLetterDetails(loanId);
        if (detail.size() > 0) {
            model.addAttribute("hasHistory", true);
        } else {
            model.addAttribute("hasHistory", false);
        }
        return new ModelAndView("recoveryField/letter", "letterList", detail);
    }

    //load letter
    @RequestMapping(value = "/saveLetter", method = RequestMethod.POST)
    public ModelAndView saveLetter(@ModelAttribute("letterPs") RecoveryLetterDetail letter, Model model, BindingResult result) {

        boolean save = recoveryService.saveLetter(letter);
        String msg = "";
        if (save) {
            msg = "Saved Successfully";
            return new ModelAndView("messages/success", "successMessage", msg);
        } else {
            msg = "Failed Save Process";
            return new ModelAndView("messages/warning", "warningMessage", msg);
        }

    }

    //Save recover officer
    @RequestMapping(value = "/saveRecoverOfficer", method = RequestMethod.POST)
    public ModelAndView saveRecoverOfficer(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("loan_id"));
        int officerId = Integer.parseInt(request.getParameter("officer_id"));
        String due_d = request.getParameter("dueDate");
        int visitType = Integer.parseInt(request.getParameter("visitType"));
        String comment = request.getParameter("comment");
        String message = "";
        int save = 0;
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date dueDate = null;
//        try {
//            dueDate = sdf.parse(due_d);
//        } catch (Exception e) {
//        }

        save = recoveryService.saveOrUpdateRecoveryOfficer(officerId, loanId, due_d, visitType, comment);
        if (save > 0) {
            message = "Saved Successfully";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Save Process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    //assign recover officer
    @RequestMapping(value = "/assignRecoveryOfficer", method = RequestMethod.GET)
    public ModelAndView assignRecoverOfficer(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        String dueDate = request.getParameter("dueDate");
        int visitType = Integer.parseInt(request.getParameter("visitType"));
        String comment = request.getParameter("comment");

        List<ApprovalDetailsModel> officerList = recoveryService.searchRecoveryOfficer();
        model.addAttribute("fieldOfficer", officerList);
        model.addAttribute("loanId", loanId);
        model.addAttribute("dueDate", dueDate);
        model.addAttribute("visitType", visitType);
        model.addAttribute("comment", comment);
        return new ModelAndView("recoveryField/recoverFieldOfficer");

    }

    //--------search loan module----------//
    @RequestMapping(value = "/loadSearchLoan", method = RequestMethod.GET)
    public ModelAndView loadSearchLoan(HttpServletRequest request, Model model) {
        return new ModelAndView("recoveryField/searchLoan");

    }

    @RequestMapping(value = "/searchLoanByRecovery", method = RequestMethod.POST)
    public ModelAndView loanSearchLoan(HttpServletRequest request, Model model) {
        String agNo = request.getParameter("agNo");
        String nicNo = request.getParameter("nic");
        String cusName = request.getParameter("name");
        String dueDate = request.getParameter("dueDate");
        String lapsDate = request.getParameter("lapDate");
        String vehicleNo = request.getParameter("vehicleNo");
        List<viewLoan> loanList = recoveryService.findSearchLoanList(agNo, cusName, nicNo, dueDate, lapsDate, vehicleNo);
        return new ModelAndView("recoveryField/viewrRecoverySearchLoan", "loanList", loanList);

    }

    //select one row
    @RequestMapping(value = "/viewLoanDetails", method = RequestMethod.GET)
    public ModelAndView viewLoanDetails(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(loanId);
        List<PaymentDetails> installList = loanService.createInstallmentList(loan);
//        String lossAmount = installList.get(installList.size() - 1).getLossAmount();
        List<GuranterDetailsModel> gurantors = loanService.findGuarantorByLoanId(loanId);

        String refNoAdjustment = "ADJ";
//        String refNoInstallmentCode = "INS";
        String refNoInterest = "INTRS";
        //payment history//
        List otherChargesList = loanService.findOtherChargesByLoanId(loanId);//from loan other charges
//        List<InstallmentReturnList> installmentsList = settlmentService.getDebitCreditInstallmentList(loanId);
        List<ReturnList> openningBalanceList = settlmentService.findOpenningBalance(loanId);
//        SettlementAddNewPayment formData = new SettlementAddNewPayment();
//        List<InstallmentReturnList> adjustmentList = settlmentService.findAdjustmentList(loanId, refNoAdjustment);
        List<LoanInstallment> insList = loanService.findInstallmentByLoanNo(loan.getLoanAgreementNo());

        double totalOtherCharges = settlmentService.getSumOfOtherCherges(otherChargesList);
        double totalOpenningD = 0.00;
        if (openningBalanceList != null) {
            totalOpenningD = settlmentService.getSumOfOpenningBalance(openningBalanceList);
        }

        double openningBalanceWithoutODI = settlmentService.getOpenningBalanceWithoputODI(loanId);

        double paidArrears = settlmentService.getPaidArrears(loanId); //done
        double paidOtherCharges = settlmentService.getLoanPaidOtherCharges(loanId);//done
        double paidDownPayment = settlmentService.getPaidDownPayment(loanId);//done
        double paidInsurance = settlmentService.getLoanPaidInsurenceCharges(loanId);//done
        double overPayAmount = settlmentService.getOverPayAmount(loanId);
        double paidInstallments = settlmentService.getLoanPaidInstallments(loanId);
        double paidOdi = settlmentService.getPaidOdiBalance(loanId);

        double totalDownPayment = settlmentService.getTotalDownPayment(loanId);//from loan header detail
        double totalInsurance = settlmentService.getTotalInsuarance(loanId);
//        double installmentBalance = settlmentService.getInstalmentBalance(loanId);
        double odiBalance = settlmentService.getOdiBalance(loanId);
        double adjustmntBalance = settlmentService.getAdjustmntBalance(loanId, refNoAdjustment);
        double totalInterest = settlmentService.getTotalInterest(loanId, refNoInterest);

        double totalPaid = paidArrears + paidOtherCharges + paidDownPayment + paidInsurance + overPayAmount + paidInstallments + paidOdi;
//        List payTypes = settlmentService.findPayTypes();
//        List mBankDetailList = settlmentService.findBankList();
        //double OtherChargesBalance = paidOtherCharges;
        //double intialDownPayment = OtherChargesBalance + totalDownPayment;
//        double downPaymentBalance = totalDownPayment - paidDownPayment;
//        double totalOpeningBalance = (totalOpenningD + openningBalanceWithoutODI) - paidArrears;
//        double insuranceBalance = totalInsurance - paidInsurance;
        int instalmentCount = settlmentService.findPaidInstallmentCount(loanId);
//        double balance = totalOpeningBalance + downPaymentBalance + insuranceBalance + installmentBalance + adjustmntBalance;
        double balance = reportService.findLoanBalanace(loanId);
        int remainIns = 0;
        if (insList == null) {
            int totalInsCount = 0;
            remainIns = 0;
        } else {
            int totalInsCount = insList.size();
            remainIns = totalInsCount - instalmentCount;
        }

        //------//
        Date nextDue = null;
//        int nextInstll = instalmentCount;
        if (insList != null) {
            if ((insList.size()) > instalmentCount) {
                nextDue = insList.get(instalmentCount).getDueDate();
            }
        }else{
            nextDue = null;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String loanEndDate = this.calculateLoanEndDate(loan);
        String loanDueDate = "";
        if (loan.getLoanDueDate() != null) {
            loanDueDate = dateFormat.format(loan.getLoanDueDate());
        }

        PaymentDetails payment = loanService.calculatePayments(loan);
        List<TransactionList> trnsList = recoveryService.loanTransactionDetails(loanId);

        String cusName = loan.getDebtorHeaderDetails().getDebtorName();
        String nic = loan.getDebtorHeaderDetails().getDebtorNic();
        String address = loan.getDebtorHeaderDetails().getDebtorPersonalAddress();
        String tp = String.valueOf(loan.getDebtorHeaderDetails().getDebtorTelephoneMobile());

        double totalOpening = totalOpenningD + openningBalanceWithoutODI;
        double totalReceivble = loan.getLoanInvestment() + totalInterest + totalInsurance + totalDownPayment + odiBalance + totalOpening;
        double totalBalance = totalReceivble - totalPaid + adjustmntBalance;

//        int spec = loan.getLoanSpec();
//        int isIssue = loan.getLoanIsIssue();
//        int paid = loan.getLoanIsPaidDown();
        // get arrears installment count
        // get arrears installment amount, odi amount, other charge amount
        Double[] installmentArrears = reportService.getArrearsAmounts(loanId);
//        Double odiArrears = reportService.getOdiArrearsAmount(loanId);
//        Double otherChargersArrears = reportService.getOtherChargersArrearsAmount(loanId);
        Double totalArrears = installmentArrears[2];
        double noOfInstallmentInArrears = (totalArrears / loan.getLoanInstallment());
        // get loan future capital, interest
        Double[] loanFutureDetails = reportService.getLoanFutureDetails(loanId);
        Double futureInterest = loanFutureDetails[0];
        Double futureCapital = loanFutureDetails[1];

        model.addAttribute("guarantors", gurantors);
        model.addAttribute("loan", loan);
        model.addAttribute("insList", installList);
        model.addAttribute("loanEndDate", loan.getLoanLapsDate());
        model.addAttribute("loanDueDate", loanDueDate);
        model.addAttribute("pay", payment);
        model.addAttribute("cusName", cusName);
        model.addAttribute("cusNic", nic);
        model.addAttribute("cusAddress", address);
        model.addAttribute("cusTelephone", tp);
        model.addAttribute("paidIns", instalmentCount);
        model.addAttribute("remainIns", remainIns);
        model.addAttribute("nextDue", nextDue);
        model.addAttribute("transactions", trnsList);
        model.addAttribute("agNo", loan.getLoanAgreementNo());

        model.addAttribute("tOther", formatter.format(totalOtherCharges));
        model.addAttribute("tInsurance", formatter.format(totalInsurance));
        model.addAttribute("tDown", formatter.format(totalDownPayment));
        model.addAttribute("tReceivble", formatter.format(totalReceivble));
        model.addAttribute("totalPaid", formatter.format(totalPaid));
        model.addAttribute("totalOver", formatter.format(overPayAmount));
        model.addAttribute("dateBalance", formatter.format(balance));
        model.addAttribute("odiBalance", formatter.format(odiBalance - paidOdi));
        model.addAttribute("tOpening", formatter.format(totalOpening));
        model.addAttribute("totalBalance", formatter.format(totalBalance));
        model.addAttribute("noOfInstallmentInArrears", formatter.format(noOfInstallmentInArrears));
        model.addAttribute("totalArrears", totalArrears);
        model.addAttribute("futureInterest", formatter.format(futureInterest));
        model.addAttribute("futureCapital", formatter.format(futureCapital));

        return new ModelAndView("recoveryField/viewRecoveryLoanDetils");

    }
    //-----------------------------//

    @RequestMapping(value = "/saveRecoveryCharge", method = RequestMethod.POST)
    public ModelAndView saveRecoveryCharge(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("recoveryChargeLoanId"));
        int chargeId = Integer.parseInt(request.getParameter("recoveryChargeType"));
        int supplierId = Integer.parseInt(request.getParameter("supplierId"));
        double charge = Double.parseDouble(request.getParameter("recoveryChargeAmount"));
        String due = "";

        boolean save = recoveryService.saveOtherRecoveryCharge(loanId, chargeId, supplierId, charge, due);
        String message = "";
        if (save) {
            message = "Saved Successfully";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Save Process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    //generate recovery history report
    @RequestMapping(value = "/generateHistoryPDF", method = RequestMethod.GET)
    public void generateHistoryPDF(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/pdf");
        int loanId = Integer.parseInt(request.getParameter("recLoanID"));
        try {

            DBFacade db = new DBFacade();
            Connection c = db.connect();

            ServletOutputStream outStream = response.getOutputStream();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("loanId", loanId);
            params.put("REPORT_CONNECTION", c);
            String type = "";

            URL url = this.getClass().getClassLoader().getResource("../reports/recoveryHistory/");
            String path = url.toURI().toString();
            params.put("SUBREPORT_DIR", path);

            InputStream reportstream = this.getClass().getClassLoader().getResourceAsStream("..//reports/recoveryHistory/recoverHistory.jasper");
            JasperRunManager.runReportToPdfStream(reportstream, outStream, params, c);

            c.close();
            c = null;
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //open other charge div
    @RequestMapping(value = "/loadRecoveryCharge", method = RequestMethod.GET)
    public ModelAndView loadRecoveryCharge(HttpServletRequest request, Model model) {

        int loanId = Integer.parseInt(request.getParameter("loanId"));
        LoanHeaderDetails loan = loanService.searchLoanByLoanId(loanId);
        List<OtherChargseModel> extraCharge = masterService.findExtraChargesBySubLoanId(loan.getLoanType());
        List<MSupplier> suppliers = masterService.findSuppliers();
        model.addAttribute("loan", loan);
        model.addAttribute("extraCharge", extraCharge);
        model.addAttribute("suppliers", suppliers);
        return new ModelAndView("recoveryField/recoveryCharge");
    }

    //set manager commment
    @RequestMapping(value = "/loadManagerComment", method = RequestMethod.GET)
    public ModelAndView loadManagerComment(Model model, HttpServletRequest request) {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        int recId = Integer.parseInt(request.getParameter("recId"));

        RecoveryVisitsDetails recovery = recoveryService.findRecoveryVisitDetail(recId, loanId);

        String officerName = "";
        Employee emp = userService.findEmployeeById(recovery.getAssignedOfficer());
        if (emp != null) {
            officerName = emp.getEmpLname();
        }
        int type = 0, appLevelId = 1;
        model.addAttribute("officerName", officerName);
        model.addAttribute("recovery", recovery);
        model.addAttribute("loanId", loanId);
        model.addAttribute("type", type);
        model.addAttribute("appID", appLevelId);

        return new ModelAndView("recoveryField/recoveryVisitReport");
    }

    //save Manager Comment
    @RequestMapping(value = "/saveMangerComment", method = RequestMethod.POST)
    public ModelAndView saveMangerComment(Model model, HttpServletRequest request) {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        int recId = Integer.parseInt(request.getParameter("recId"));
        String comment = request.getParameter("approvedComment");
        boolean suc = recoveryService.saveRecoveryManagerComment(recId, comment);
        String message = "";
        if (suc) {
            message = "Saved Successfully";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Save Process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }

    }

    @RequestMapping(value = "/getByDate", method = RequestMethod.GET)
    @ResponseBody
    public List<RecoveryVisitsDetails> getBySubmitDate(HttpServletRequest request, Model model) {
        List<RecoveryVisitsDetails> rvds = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date sDate = sdf.parse(request.getParameter("sDate"));
            Date eDate = sdf.parse(request.getParameter("eDate"));
            rvds = recoveryService.getBySubmitDate(sDate, eDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rvds;
    }

    //Seize Module
    @RequestMapping(value = "/loadSeizeOrderPage", method = RequestMethod.GET)
    public ModelAndView loadSeizeOrderPage(HttpServletRequest request, Model model) {
        List<SeizeOrder> seizeOrders = null;
        List<MSeizerDetails> seizers = null;
        try {
            seizeOrders = recoveryService.getSeizeOrders();
            seizers = adminService.findSeizerDetailsList();
            model.addAttribute("seizeOrders", seizeOrders);
            model.addAttribute("seizers", seizers);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeOrderPage");
    }

    @RequestMapping(value = "/saveOrUpdateSeizeOrder", method = RequestMethod.POST, consumes = "application/json")
    public ModelAndView saveOrUpdateSeizeOrder(@RequestBody SeizeOrder seizeOrder, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String branchId = request.getSession().getAttribute("branchId").toString();
        seizeOrder.setBranchId(Integer.parseInt(branchId));
        try {
            boolean success = recoveryService.saveOrUpdateSeizeOrder(seizeOrder);
            if (success) {
                message = "Successfully Saved Seize Order";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Seize Order";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/activeSeizeOrder", method = RequestMethod.GET)
    public ModelAndView activeSeizeOrder(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String orderId = request.getParameter("orderId");
        String loanId = request.getParameter("loanId");
        try {
            boolean success = recoveryService.activeSeizeOrder(Integer.parseInt(orderId), Integer.parseInt(loanId));
            if (success) {
                message = "Successfully Active Seize Order";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Active Seize Order";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/inActiveSeizeOrder", method = RequestMethod.GET)
    public ModelAndView inActiveSeizeOrder(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String orderId = request.getParameter("orderId");
        String loanId = request.getParameter("loanId");
        try {
            boolean success = recoveryService.inActiveSeizeOrder(Integer.parseInt(orderId), Integer.parseInt(loanId));
            if (success) {
                message = "Successfully In-Active Seize Order";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in In-Active Seize Order";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadSeizeOrderApprovals", method = RequestMethod.GET)
    public ModelAndView loadSeizeOrderIssuePage(HttpServletRequest request, Model model) {
        List<SeizeOrder> seizeApprovals = null;
        try {
            seizeApprovals = recoveryService.loadSeizeApproval();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("seizeApproveList", seizeApprovals);
        return new ModelAndView("recoveryField/seizeOrderApprovalMain");
    }

    @RequestMapping(value = "/loadSeizeOrderApprovalForm", method = RequestMethod.GET)
    public ModelAndView loadSeizeOrderIssueForm(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        int userId = userService.findByUserName();
        int userTypeId = userService.findUserTypeByUserId(userId);
        int usType = 0;
        String title = "";
        String seizeOrderId = request.getParameter("seizeOrderId");
        int type = Integer.parseInt(request.getParameter("type"));
        SeizeOrderApproval orderApproval = null;
        try {
            orderApproval = recoveryService.findSeizeOrderIssue(Integer.parseInt(seizeOrderId), type);
            if (type == 0) {
                //user type (admin, Recovery Manager, CED) approval
                if (userTypeId == 1 || userTypeId == 6 || userTypeId == 7 || userTypeId == 18) {
                    usType = 1;
                }
                title = "Recovery Manager Approval";
            } else {
                //user type (admin,CED) approval
                if (userTypeId == 1 || userTypeId == 7 || userTypeId == 18) {
                    usType = 1;
                }
                title = "CED Approval";
            }
            model.addAttribute("title", title);
            model.addAttribute("type", type);
            model.addAttribute("seizeOrderID", seizeOrderId);
            model.addAttribute("userType", usType);
            model.addAttribute("orderApproval", orderApproval);
            modelAndView = new ModelAndView("recoveryField/seizeOrderApprovalForm");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/saveOrUpdateSeizeOrderApproval", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateSeizeOrderIssue(@ModelAttribute("seizeOrderApprovalForm") SeizeOrderApproval orderApproval, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        try {
            boolean success = recoveryService.saveOrUpdateSeizeOrderApproval(orderApproval);
            if (success) {
                if (orderApproval.getIsReject() == 1) {
                    message = "Approved Seize Order";
                    model.addAttribute("successMessage", message);
                    modelAndView = new ModelAndView("messages/success");
                } else {
                    message = "Rejected Seize Order";
                    model.addAttribute("successMessage", message);
                    modelAndView = new ModelAndView("messages/success");
                }
            } else {
                message = "Error in Seize Order Issue";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/keyIssue", method = RequestMethod.GET)
    public ModelAndView keyIssue(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String seizeOrderId = request.getParameter("seizeOrderId");
        try {
            boolean success = recoveryService.keyIssue(Integer.parseInt(seizeOrderId));
            if (success) {
                message = "Successfully Key Issued";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Key Issue";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadSeizeYardRegisterPage", method = RequestMethod.GET)
    public ModelAndView loadSeizeYardRegisterPage(HttpServletRequest request, Model model) {
        List<SeizeYardRegister> yardRegisters = null;
        try {
            yardRegisters = recoveryService.getYardRegisterList();
            model.addAttribute("yardRegisters", yardRegisters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeYardRegisterPage");
    }

    @RequestMapping(value = "/loadSeizeYardRegisterForm", method = RequestMethod.GET)
    public ModelAndView loadSeizeYardRegisterForm(HttpServletRequest request, Model model) {
        String seizeOrderId = request.getParameter("seizeOrderId");
        SeizeYardRegister yardRegister = null;
        try {
            yardRegister = recoveryService.findYardRegister(Integer.parseInt(seizeOrderId));
            if (yardRegister != null) {
                model.addAttribute("yardRegister", yardRegister);
            } else {
                yardRegister = new SeizeYardRegister();
                model.addAttribute("yardRegister", yardRegister);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeYardRegisterForm");
    }

    @RequestMapping(value = "/saveOrUpdateYardRegister", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateYardRegister(@ModelAttribute("yardRegisterForm") SeizeYardRegister yardRegister, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String branchId = request.getSession().getAttribute("branchId").toString();
        yardRegister.setBranchId(Integer.parseInt(branchId));
        try {
            String success = recoveryService.saveOrUpdateYardRegister(yardRegister);
            if (success.equals("OK")) {
                message = "Successfully Add to Yard";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                model.addAttribute("errorMessage", success);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadSeizeArticleImagesPage", method = RequestMethod.GET)
    public ModelAndView loadSeizeArticleImagesPage(HttpServletRequest request, Model model) {
        List<SeizeYardRegisterCheklist> yardRegisterCheklists = null;
        String seizeOrderId = request.getParameter("seizeOrderId");
        try {
            yardRegisterCheklists = recoveryService.getYardRegisterCheklists(Integer.parseInt(seizeOrderId));
            model.addAttribute("checkList", yardRegisterCheklists);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeArticleImagesPage");
    }

    @RequestMapping(value = "/loadSeizeInspectionReportPage", method = RequestMethod.GET)
    public ModelAndView loadSeizeInspectionReportPage(HttpServletRequest request, Model model) {
        SeizeYardRegisterCheklist yardRegisterReport = null;
        String seizeOrderId = request.getParameter("seizeOrderId");
        try {
            yardRegisterReport = recoveryService.getYardRegisterInspectionReport(Integer.parseInt(seizeOrderId));
            model.addAttribute("yardRegisterReport", yardRegisterReport);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeReportPage");
    }
    //

    //Seize Module
    @RequestMapping(value = "/createSeizeOrderByAgreementNo", method = RequestMethod.GET)
    public ModelAndView loadSeizeOrderByAgreementNo(HttpServletRequest request, Model model) {
        List<SeizeOrder> seizeOrders = null;
        List<MSeizerDetails> seizers = null;
        String agreementNo = "";
        if (request.getParameter("agreeNo") != null) {
            agreementNo = (request.getParameter("agreeNo"));
        }
        try {
            seizeOrders = recoveryService.loadSeizeOrdersByAgreementNo(agreementNo);
            seizers = adminService.findSeizerDetailsList();
            model.addAttribute("seizeOrders", seizeOrders);
            model.addAttribute("seizers", seizers);
            model.addAttribute("isDefault", 1);
            model.addAttribute("message", seizeOrders.isEmpty() == true ? "There is not valid arrears amount" : "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeOrderPage");
    }

    //Issue Key
    @RequestMapping(value = "/keyIssueConfirmation/{orderId}", method = RequestMethod.GET)
    public ModelAndView removeLoan(@PathVariable("orderId") int orderId, Model model) {
        model.addAttribute("orderID", orderId);
        model.addAttribute("methodType", 2);
        model.addAttribute("message", "Are you sure to Issue vehicle Key..!");
        return new ModelAndView("messages/conformDialog");

    }

    @RequestMapping(value = "/loadSeizeOrderRenewPage", method = RequestMethod.GET)
    public ModelAndView loadSeizeOrderRenewPage(HttpServletRequest request, Model model) {
        List<SeizeOrder> seizeOrderRenewalList = null;
        try {
            seizeOrderRenewalList = recoveryService.getRenewalSeizeOrders();
            model.addAttribute("seizeOrderRenewalList", seizeOrderRenewalList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeOrderRenewPage");
    }

    @RequestMapping(value = "/loadSeizeOrderRenewForm", method = RequestMethod.GET)
    public ModelAndView loadSeizeOrderRenewForm(HttpServletRequest request, Model model) {
        model.addAttribute("renewalSeizeOrderForm", new SeizeOrder());
        return new ModelAndView("recoveryField/seizeOrderRenewForm");
    }

    @RequestMapping(value = "/saveRenewalSeizeOrder", method = RequestMethod.POST)
    public ModelAndView saveRenewalSeizeOrder(@ModelAttribute("renewalSeizeOrderForm") SeizeOrder renewalSeizeOrder, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String branchId = request.getSession().getAttribute("branchId").toString();
        renewalSeizeOrder.setBranchId(Integer.parseInt(branchId));
        try {
            boolean success = recoveryService.saveOrUpdateSeizeOrder(renewalSeizeOrder);
            if (success) {
                message = "Successfully Re-New Seize Order";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Re-New Seize Order";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadSeizeOrderHistoryPage", method = RequestMethod.GET)
    public ModelAndView loadSeizeOrderHistoryPage(HttpServletRequest request, Model model) {
        String loanId = request.getParameter("loanId");
        List<SeizeOrder> seizeOrderList = null;
        try {
            seizeOrderList = recoveryService.getSeizeOrderHistory(Integer.parseInt(loanId));
            model.addAttribute("seizeOrderList", seizeOrderList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("recoveryField/seizeOrderHistoryPage");
    }

    @RequestMapping(value = "/loadDeleteYardRegPswdForm", method = RequestMethod.GET)
    public ModelAndView loadDeleteYardRegPswdForm(HttpServletRequest request, Model model) {
        return new ModelAndView("recoveryField/seizeYardRegisterPswdForm");
    }

    @RequestMapping(value = "/deleteYardRegister", method = RequestMethod.GET)
    public ModelAndView deleteYardRegister(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String yardRegId = request.getParameter("yardRegId");
        String pswd = request.getParameter("pswd");
        String approval = userService.findApprovalsByPassword(pswd);
        if (approval.equals("yes")) {
            boolean delete = recoveryService.deleteYardRegister(Integer.parseInt(yardRegId));
            if (delete) {
                message = "Successfully Delete Yard Register";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Delete Yard Register";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } else if (approval.equals("noApp")) {
            message = "You don't have approvals to delete";
            model.addAttribute("warningMessage", message);
            modelAndView = new ModelAndView("messages/warning");
        } else {
            message = "Authentication Failed";
            model.addAttribute("errorMessage", message);
            modelAndView = new ModelAndView("messages/error");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/reverseYardRegister", method = RequestMethod.GET)
    public ModelAndView reverseYardRegister(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        String seizeOrderId = request.getParameter("seizeOrderId");
        try {
            boolean reverse = recoveryService.reverseYardRegister(Integer.parseInt(seizeOrderId));
            if (reverse) {
                message = "Successfully Reverse";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Reverse";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadSearchFacilityPage", method = RequestMethod.GET)
    public ModelAndView loadSearchFacilityPage(HttpServletRequest request, Model model) {
        return new ModelAndView("recoveryField/searchFacility");
    }

    @RequestMapping(value = "/loadSearchFacilityPageByAggNo", method = RequestMethod.GET)
    public ModelAndView loadSearchFacilityPageByAggNo(HttpServletRequest request, Model model) {
        String aggNo = request.getParameter("aggNo");
        List<LoanPropertyVehicleDetails> vehicleDetails = recoveryService.findByLoan(aggNo);
        model.addAttribute("vehicleDetails", vehicleDetails);
        model.addAttribute("agreementNo", aggNo);
        return new ModelAndView("recoveryField/searchFacility");
    }

    @RequestMapping(value = "/loadSearchFacilityPageByVehNo", method = RequestMethod.GET)
    public ModelAndView loadSearchFacilityPageByVehNo(HttpServletRequest request, Model model) {
        String vehNo = request.getParameter("vehNo");
        List<LoanPropertyVehicleDetails> vehicleDetails = recoveryService.findByVehicleNo(vehNo);
        model.addAttribute("vehicleDetails", vehicleDetails);
        model.addAttribute("vehicleNo", vehNo);
        return new ModelAndView("recoveryField/searchFacility");
    }

    @RequestMapping(value = "/loadRecoveryArrearsLoansByAgNo", method = RequestMethod.GET)
    public ModelAndView loadRecoveryArrearsLoansByAgNo(HttpServletRequest request, Model model) {
        String agNo = request.getParameter("agNo");
        boolean byDate = false;
        List<RecoveryLoanList> loans = recoveryService.loadLetterPostingByLoan(agNo);
        List<RecoveryLetters> letters = recoveryService.findAllLetters();
        model.addAttribute("byDate", byDate);
        model.addAttribute("letters", letters);
        return new ModelAndView("recoveryField/postingLettersSearch", "loanList", loans);
    }

    @RequestMapping(value = "/loadRecoveryArrearsLoansByVehNo", method = RequestMethod.GET)
    public ModelAndView loadRecoveryArrearsLoansByVehNo(HttpServletRequest request, Model model) {
        String vehNo = request.getParameter("vehNo");
        boolean byDate = false;
        List<RecoveryLoanList> loans = recoveryService.loadLetterPostingByVehicle(vehNo);
        List<RecoveryLetters> letters = recoveryService.findAllLetters();
        model.addAttribute("byDate", byDate);
        model.addAttribute("letters", letters);
        return new ModelAndView("recoveryField/postingLettersSearch", "loanList", loans);
    }

    @RequestMapping(value = "/loadDebitChargesPage/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadDebitChargesPage(@PathVariable("loanId") int loanId, Model model) {
        List<DirectDebitCharge> debitCharges = recoveryService.findDirectDebitCharges(loanId);
        model.addAttribute("debitCharges", debitCharges);
        model.addAttribute("loanId", loanId);
        return new ModelAndView("recoveryField/recoveryChargeMain");
    }

    @RequestMapping(value = "/deleteDebitCharge/{chargeId}/{settlementId}", method = RequestMethod.GET)
    public ModelAndView deleteDebitCharge(@PathVariable("chargeId") int chargeId, @PathVariable("settlementId") int settlementId, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        boolean flag = recoveryService.deleteDirectDebitCharge(chargeId, settlementId);
        if (flag) {
            message = "Successfully Delete !";
            model.addAttribute("successMessage", message);
            modelAndView = new ModelAndView("messages/success");
        } else {
            message = "Error in Delete !";
            model.addAttribute("errorMessage", message);
            modelAndView = new ModelAndView("messages/error");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadEnvelopePrint", method = RequestMethod.GET)
    public ModelAndView loadEnvelopePrint(HttpServletRequest request) {
        return new ModelAndView("recoveryField/envelopePrint");
    }

    @RequestMapping(value = "/loadEnvelopePrintTableByName", method = RequestMethod.GET)
    public ModelAndView loadEnvelopePrintTableByName(HttpServletRequest request, Model model) {
        String debName = request.getParameter("debName");
        List<DebtorHeaderDetails> dhds = debtorService.findDebtorByName(debName);
        model.addAttribute("debtors", dhds);
        return new ModelAndView("recoveryField/envelopePrintTable");
    }

    @RequestMapping(value = "/loadEnvelopePrintTableByAccNo", method = RequestMethod.GET)
    public ModelAndView loadEnvelopePrintTableByAccNo(HttpServletRequest request, Model model) {
        String debAccNo = request.getParameter("debAccNo");
        List<DebtorHeaderDetails> dhds = debtorService.findDebtorByAccNo(debAccNo);
        model.addAttribute("debtors", dhds);
        return new ModelAndView("recoveryField/envelopePrintTable");
    }

    @RequestMapping(value = "/loadAgreementTerminationMain", method = RequestMethod.GET)
    public ModelAndView loadAgreementTermination(HttpServletRequest request) {
        return new ModelAndView("recovery/agreementTerminationMain");
    }

    @RequestMapping(value = "/loadAgreementTerminationTable", method = RequestMethod.GET)
    public ModelAndView loadCorrectPaymentTable(Model model, HttpServletRequest request) {
        return new ModelAndView("recovery/agreementTerminationTable");
    }

    @RequestMapping(value = "/loadAgreementTerminateByAgreementNo", method = RequestMethod.GET)
    public ModelAndView loadAgreementTerminateByAgreementNo(Model model, HttpServletRequest request) {
        boolean message = false;
        boolean bySysDate = false;
        String agreementNo = request.getParameter("agreementNo");

        List<LoanHeaderDetails> terminateList = loanService.findLoanDetailsAndVehicleDetailsByAggNo(agreementNo);
        if (terminateList != null) {
            message = true;
        }

        model.addAttribute("bySysDate", bySysDate);
        model.addAttribute("message", message);
        model.addAttribute("terminateList", terminateList);
        return new ModelAndView("recovery/agreementTerminationTable");
    }

    @RequestMapping(value = "/loadAgreementTerminateByVehicleNo", method = RequestMethod.GET)
    public ModelAndView loadAgreementTerminateByVehicleNo(Model model, HttpServletRequest request) {
        boolean message = false;
        boolean bySysDate = false;
        String vehicleNo = request.getParameter("vehicleNo");

        List<LoanHeaderDetails> terminateList = loanService.findLoanDetailsAndVehicleDetailsByVehicleNo(vehicleNo);
        if (terminateList != null) {
            message = true;
        }

        model.addAttribute("bySysDate", bySysDate);
        model.addAttribute("message", message);
        model.addAttribute("terminateList", terminateList);
        return new ModelAndView("recovery/agreementTerminationTable");
    }

    @RequestMapping(value = "/agreementTerminateByLoanId", method = RequestMethod.GET)
    public ModelAndView agreementTerminateByLoanId(Model model, HttpServletRequest request) {
        String message = "";
        boolean is_delete = false;
        ModelAndView modelAndView = null;
        int loanId = Integer.parseInt(request.getParameter("loanNo"));

        if (loanId != 0) {
            is_delete = loanService.saveOrUpdateTerminateAgreementByLoanId(loanId);
        }

        if (is_delete == true) {
            message = "Agreement Successfully Terminated ";
            model.addAttribute("successMessage", message);
            modelAndView = new ModelAndView("messages/success");
        } else {
            message = "This Agreement cannot be Terminated.";
            model.addAttribute("warningMessage", message);
            modelAndView = new ModelAndView("messages/warning");
        }

        return new ModelAndView("recovery/agreementTerminationTable");
//        return modelAndView;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
    
     @RequestMapping(value = "/loanSearchforReport", method = RequestMethod.POST)
    public ModelAndView loanSearchforReport(HttpServletRequest request, Model model) {
        String memNo = request.getParameter("memNo");       
        List<viewLoan> loanList = recoveryService.findSearchLoanListforReport(memNo);
        return new ModelAndView("reports/searchLoanView", "loanList", loanList);

    }

}

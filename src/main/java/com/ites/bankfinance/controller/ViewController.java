/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.RecoveryReportModel;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.LoanGuaranteeDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MRateType;
import com.ites.bankfinance.model.RecoveryVisitsDetails;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.RecoveryService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {

    @Autowired
    LoanService loanService;

    @Autowired
    MasterDataService masterData;

    @Autowired
    RecoveryService recoveryServis;

    @RequestMapping(value = "/searchByNic/{nicNo}", method = RequestMethod.GET)
    public ModelAndView searchByNicNo(@PathVariable("nicNo") String nicNo, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByNic(nicNo, bID);
        Boolean message = false;
        if (debtor.size() > 0) {
            message = true;
        }
        model.addAttribute("message", message);
        return new ModelAndView("views/viewSearchCustomer", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchByName/{name}", method = RequestMethod.GET)
    public ModelAndView searchByName(@PathVariable("name") String name, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByName(name, bID);
        Boolean message = true;
        model.addAttribute("message", message);
        return new ModelAndView("views/viewSearchCustomer", "debtorList", debtor);
    }

    @RequestMapping(value = "/searchLoanById/{id}", method = RequestMethod.GET)
    public ModelAndView searchLoanById(@PathVariable("id") String id, HttpServletRequest request, Model model) {
        int debtorId = Integer.parseInt(id);

        List<LoanHeaderDetails> loan = loanService.findLoansByDebtorId(debtorId);
        DebtorHeaderDetails debtor = loanService.findById(debtorId);

        List<viewLoan> guarantor = loanService.findGuarantorByDebtorId(debtorId);
        List<MBranch> branchs = masterData.findBranches();

        boolean message = true;
        if (loan.isEmpty()) {
            message = false;
        }
        boolean hasGuarntors = false;
        if (guarantor.size() > 0) {
            hasGuarntors = true;
        }

        model.addAttribute("name", debtor.getDebtorName());
        model.addAttribute("gurantor", guarantor);
        model.addAttribute("message", message);
        model.addAttribute("MBranch", branchs);
        model.addAttribute("hasGuarntors", hasGuarntors);
        return new ModelAndView("views/viewSearchLoans", "loanList", loan);
    }

    @RequestMapping(value = "/SearchOfficers/{type}/{loanTypeId}", method = RequestMethod.GET)
    public @ResponseBody
    List SearchOfficers(@PathVariable("type") String type, @PathVariable("loanTypeId") int loanType, HttpServletRequest request, Model model) {
        List nameList = null;
        int t = Integer.parseInt(type);
        nameList = masterData.findUsersByApproval(t, loanType);

        List<UmUser> userList = new ArrayList();
        for (int i = 0; i < nameList.size(); i++) {
            Object[] obj = (Object[]) nameList.get(i);
            UmUser approval = new UmUser();
            String name = "" + obj[1];
            approval.setUserName(name);
            approval.setUserId(Integer.parseInt(obj[2].toString()));
            approval.setEmpId(Integer.parseInt(obj[3].toString()));
            userList.add(approval);
        }
        return userList;
    }

    //search by nic
    @RequestMapping(value = "/searchByNic_new/{nicNo}", method = RequestMethod.GET)
    @ResponseBody
    public DebtorHeaderDetails searchByNic_new(@PathVariable("nicNo") String nicNo, HttpServletRequest request, Model model) {
        Object bID = request.getSession().getAttribute("branchId");
        List<DebtorHeaderDetails> debtor = loanService.findByNic(nicNo, bID);
        DebtorHeaderDetails deb = new DebtorHeaderDetails();
        if (debtor.size() > 0) {
            for (int i = 0; i < debtor.size(); i++) {
                deb = debtor.get(i);
            }
        } else {
            deb.setDebtorId(0);
        }
        return deb;
    }

//    recovery report
    @RequestMapping(value = "/viewRecoveryReport/{loanId}", method = RequestMethod.GET)
    public ModelAndView viewRecoveryReport(@PathVariable("loanId") int loanId, Model model) {

        List<RecoveryReportModel> customerList = loanService.findrecoveryReport(loanId);
        model.addAttribute("customer", customerList);
        model.addAttribute("loanId", loanId);
        return new ModelAndView("recovery/recoveryReport");
    }

    @RequestMapping(value = "/loadFiledOfficerPage2", method = RequestMethod.GET)
    public ModelAndView loadFiledOfficerPage2(Model model, HttpServletRequest request) {
        List<viewLoan> loanList = loanService.findLoanListByFiledOfficer();
        String dueDate = request.getParameter("dueDate");
        List<RecoveryVisitsDetails> visitDetails = recoveryServis.findVisitDetailsByuser(dueDate);
        model.addAttribute("visitList", visitDetails);
        return new ModelAndView("recovery/recoverNewLoanPage", "loanList", loanList);
    }

    @RequestMapping(value = "/saveRecoveryReport", method = RequestMethod.POST)
    public ModelAndView saveRecoveryReport(HttpServletRequest request, Model model) {
        String[] comment = request.getParameterValues("comment");
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        int save = 0;
        String message = "";
        save = loanService.saveRecoveryReport(loanId, comment);
        if (save > 0) {
            message = "Successfully Saved";
            return new ModelAndView("messages/success", "successMessage", message);
        } else {
            message = "Save process Failed";
            return new ModelAndView("messages/error", "errorMessage", message);
        }
    }

    @RequestMapping(value = "/loadReversConfirm", method = RequestMethod.GET)
    public ModelAndView loadReversConfirm(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("id"));
        int type = Integer.parseInt(request.getParameter("type"));
        model.addAttribute("loanId", loanId);
        model.addAttribute("type", type);
        return new ModelAndView("views/reverseConfirm");
    }

    @RequestMapping(value = "/reversLoan", method = RequestMethod.GET)
    public ModelAndView reversLoan(HttpServletRequest request, Model model) {
        int loanId = Integer.parseInt(request.getParameter("loanId"));
        int type = Integer.parseInt(request.getParameter("type"));
        String comment = request.getParameter("comment");
        String msg = "";
        if (type == 1) {
            //revers processing loan
            msg = loanService.reverseProcessLoan(loanId, comment);
        } else {
            //revers approved loan
            msg = loanService.reverseApprovedLoan(loanId, comment);
        }

        if (msg.equals("OK")) {
            return new ModelAndView("messages/success", "successMessage", "Successfully Reversed");
        } else {
            return new ModelAndView("messages/warning", "warningMessage", msg);
        }
    }

    // get Recovery Officers by usertype and branch
    @RequestMapping(value = "/SearchRecoveryOfficer", method = RequestMethod.GET)
    public @ResponseBody
    List SearchRecoveryOfficer(HttpServletRequest request, Model model) {
        List<Employee> officers = new ArrayList<>();
        String userType = request.getParameter("userType");
        String branchId = request.getParameter("branchId");
        try {
            if (userType != null && branchId != null) {
                List list = masterData.findUsersByUserTypeAndBranch(Integer.parseInt(userType), Integer.parseInt(branchId));
                if (list.size() > 0) {
                    for (Object ob : list) {
                        Object[] o = (Object[]) ob;
                        Employee emp = new Employee();
                        emp.setEmpNo(Integer.parseInt(o[0].toString()));
                        emp.setEmpLname(o[1].toString());
                        emp.setPhone2(o[2].toString());
                        officers.add(emp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officers;
    }

    // get Recovery Officers by usertype
    @RequestMapping(value = "/SearchRecoveryOfficerByType", method = RequestMethod.GET)
    public @ResponseBody
    List SearchRecoveryOfficerByType(HttpServletRequest request, Model model) {
        List<Employee> officers = new ArrayList<>();
        String userType = request.getParameter("userType");
        String branchId = request.getSession().getAttribute("branchId").toString();
        try {
            if (userType != null && branchId != null) {
                List list = masterData.findUsersByUserTypeAndBranch(Integer.parseInt(userType), Integer.parseInt(branchId));
                if (list.size() > 0) {
                    for (Object ob : list) {
                        Object[] o = (Object[]) ob;
                        Employee emp = new Employee();
                        emp.setEmpNo(Integer.parseInt(o[0].toString()));
                        emp.setEmpLname(o[1].toString());
                        emp.setPhone2(o[2].toString());
                        officers.add(emp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officers;
    }

    @RequestMapping(value = "/loadLoanCalView", method = RequestMethod.GET)
    public ModelAndView loadLoanCalView(HttpServletRequest request, Model model) {
        List<MRateType> loanRate = masterData.findRateType();
        model.addAttribute("rateTypeList", loanRate);
        return new ModelAndView("loan/loanCalView");
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.ApprovalDetailsModel;
import com.bankfinance.form.DebtorForm;
import com.bankfinance.form.GuranterDetailsModel;
import com.bankfinance.form.InvoiseDetails;
import com.bankfinance.form.LoanForm;
import com.bankfinance.form.OtherChargseModel;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PawningController {

    @Autowired
    private MasterDataService masterService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/loadNewPawning", method = RequestMethod.GET)
    public ModelAndView loadNewPawning(Model model) {
        MWeightunits units = null;

        units = adminService.findWeightType();
        List<MSubLoanType> pawningType = masterService.findPawningType();
        model.addAttribute("units", units);
        model.addAttribute("types", pawningType);
        return new ModelAndView("pawning/newPawnForm");
    }

    @RequestMapping(value = "/findLoanRate/{loanType}", method = RequestMethod.GET)
    public @ResponseBody
    Double findLoanRate(@PathVariable("loanType") int loanType, Model model) {
        Double rate = masterService.findLoanRate(loanType);
        return rate;
    }

    @RequestMapping(value = "/saveNewPawningForm", method = RequestMethod.POST)
    public ModelAndView saveNewPawningForm(@ModelAttribute("loanForm") LoanForm formData, BindingResult result, Model model, HttpServletRequest request) {
        String saveUpdate = "";
        Boolean isSave = false;

        Object bID = request.getSession().getAttribute("branchId");
        int branchId = 0;
        if (bID != null) {
            branchId = Integer.parseInt(bID.toString());
        }
        formData.setBranchId(branchId);
        if (formData.getLoanId() == null) {
            isSave = true;
        }
        int loanId = loanService.saveNewLoanData(formData);

        if (loanId > 0 && isSave) {
            saveUpdate = "Save Process Success..!";
        } else if (loanId > 0 && !isSave) {
            saveUpdate = "Update Process Success..!";
        } else {
            saveUpdate = "Process Fail..!";
        }
        List<OtherChargseModel> otherChargers = loanService.findOtherChargesByLoanId(loanId);
        List<LoanPropertyArticlesDetails> articleData = loanService.findArticleDetailsById(loanId);

        DebtorHeaderDetails debtor = loanService.findById(formData.getDebtorId());
        //udates
        //find total of other charges
        double total = 0.00;
        if (otherChargers != null) {
            for (int i = 0; i < otherChargers.size(); i++) {
                OtherChargseModel other = otherChargers.get(i);
                total = total + other.getAmount();
            }
        }

        //sub loan types
        List<MSubLoanType> pawningType = masterService.findPawningType();

        //find total interest
        double totalInterest = formData.getLoanInterest() * formData.getLoanPeriod();

        //loan Approvals
        ApprovalDetailsModel approve = loanService.findApprovalDetails(loanId);

        double totalAmount = totalInterest + formData.getLoanInvestment();

        double t_weight = 0.00;
        double n_weight = 0.00;
        double carror_rate = 0.00;
        double total_value = 0.00;

        for (LoanPropertyArticlesDetails articleDetail : articleData) {
            t_weight = t_weight + Double.parseDouble(articleDetail.getArticleWeight1());
            n_weight = n_weight + Double.parseDouble(articleDetail.getArticleWeight2());
            carror_rate = carror_rate + Double.parseDouble(articleDetail.getArticleCarrateRate());
            total_value = total_value + articleDetail.getArticlePrice();
        }

        model.addAttribute("loanId", loanId);
        model.addAttribute("types", pawningType);
        model.addAttribute("debtor", debtor);
        model.addAttribute("otherChargers", otherChargers);
        model.addAttribute("approve", approve);
        model.addAttribute("total", total);
        model.addAttribute("totalInterest", totalInterest);
        model.addAttribute("totalAmount", totalAmount);
        model.addAttribute("invoise", articleData);
        model.addAttribute("edit", true);
        model.addAttribute("saveUpdate", saveUpdate);
        model.addAttribute("t_weight", t_weight);
        model.addAttribute("n_weight", n_weight);
        model.addAttribute("carror_rate", carror_rate);
        model.addAttribute("total_value", total_value);

        return new ModelAndView("pawning/newPawnForm", "loan", formData);
    }

    @RequestMapping(value = "/searchPawningDeatilsById/{loanId}", method = RequestMethod.GET)
    public ModelAndView searchLoanDeatilsById(@PathVariable("loanId") int loanId, Model model, HttpServletRequest request) {
        LoanHeaderDetails loan = loanService.findLoanHeaderByLoanId(loanId);
        String message = "";
        LoanForm formData = new LoanForm();
        if (loan.getLoanType() == 17 || loan.getLoanType() == 18) {
            if (loan == null) {
                message = "Enter valid loan number";
            } else if (loan.getLoanIsIssue() >= 1) {
                message = "You have not authorization to edit this loan";
            } else if (loan.getLoanStatus() >= 2) {
                message = "You have not authorization to edit this loan";
            } else if (loan.getLoanApprovalStatus() > 1) {
                String approval_level = "";
                int appId = loan.getLoanApprovalStatus();
                if (appId == 2) {
                    approval_level = "Marketing Manager";
                } else if (appId == 3) {
                    approval_level = "Recovery Manager";
                } else if (appId == 4) {
                    approval_level = "CED";
                }
                message = "This loan is already approved by " + approval_level;
            } else {
                int mainType = loanService.findLoanTypeBySubType(loan.getLoanType());

                formData.setLoanId(loanId);
                formData.setLoanAmount(loan.getLoanAmount());
                formData.setLoanDownPayment(loan.getLoanDownPayment());
                formData.setLoanInvestment(loan.getLoanInvestment());
                formData.setLoanPeriod(loan.getLoanPeriod());
                formData.setPeriodType(loan.getLoanPeriodType());
                formData.setLoanRate(loan.getLoanInterestRate());
                formData.setLoanInstallment(loan.getLoanInstallment());
                formData.setLoanInterest(loan.getLoanInterest());
                formData.setLoanType(loan.getLoanType());
                formData.setLoanMainType(mainType);
                formData.setDueDay(loan.getDueDay());

                List<GuranterDetailsModel> gurantorsList = loanService.findGuarantorByLoanId(loanId);
                List<OtherChargseModel> otherChargers = loanService.findOtherChargesByLoanId(loanId);
                List<LoanPropertyArticlesDetails> artcileDetails = loanService.findArticleDetailsById(loanId);
                DebtorHeaderDetails debtor = loanService.findById(loan.getDebtorHeaderDetails().getDebtorId());
                List<MLoanType> loantypes = masterService.findLoanTypes();

                double total = 0.00;
                if (otherChargers != null) {
                    for (int i = 0; i < otherChargers.size(); i++) {
                        OtherChargseModel other = otherChargers.get(i);
                        total = total + other.getAmount();
                    }
                }

                double totalInterest = formData.getLoanInterest() * formData.getLoanPeriod();

                ApprovalDetailsModel approve = loanService.findApprovalDetails(loanId);
                double totalAmount = totalInterest + formData.getLoanInvestment();

                List<MSubLoanType> pawningType = masterService.findPawningType();

                double t_weight = 0.00;
                double n_weight = 0.00;
                double carror_rate = 0.00;
                double total_value = 0.00;

                for (LoanPropertyArticlesDetails articleDetail : artcileDetails) {
                    t_weight = t_weight + Double.parseDouble(articleDetail.getArticleWeight1());
                    n_weight = n_weight + Double.parseDouble(articleDetail.getArticleWeight2());
                    carror_rate = carror_rate + Double.parseDouble(articleDetail.getArticleCarrateRate());
                    total_value = total_value + articleDetail.getArticlePrice();
                }

                model.addAttribute("loanId", loanId);
                model.addAttribute("debtor", debtor);
                model.addAttribute("gurantors", gurantorsList);
                model.addAttribute("otherChargers", otherChargers);
                model.addAttribute("approve", approve);
                model.addAttribute("total", total);
                model.addAttribute("totalInterest", totalInterest);
                model.addAttribute("edit", true);
                model.addAttribute("totalAmount", totalAmount);
                model.addAttribute("invoise", artcileDetails);
                model.addAttribute("loanTypes", loantypes);
                model.addAttribute("types", pawningType);
                model.addAttribute("t_weight", t_weight);
                model.addAttribute("n_weight", n_weight);
                model.addAttribute("carror_rate", carror_rate);
                model.addAttribute("total_value", total_value);
            }
        } else {
            message = "You can Search Only Pawning Loan here";
        }
        model.addAttribute("message", message);
        return new ModelAndView("pawning/newPawnForm", "loan", formData);
    }

}

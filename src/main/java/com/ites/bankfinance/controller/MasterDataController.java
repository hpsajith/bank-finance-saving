  /*
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */
  package com.ites.bankfinance.controller;

  import com.bankfinance.form.OtherChargseModel;
  import com.ites.bankfinance.model.MLoanType;
  import com.ites.bankfinance.model.MSubLoanType;
  import com.ites.bankfinance.model.MSubTaxCharges;
  import com.ites.bankfinance.savingsModel.MSavingsType;
  import com.ites.bankfinance.savingsModel.MSubSavingsType;
  import com.ites.bankfinance.service.MasterDataService;

  import java.util.List;

  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.security.core.Authentication;
  import org.springframework.security.core.context.SecurityContextHolder;
  import org.springframework.security.core.userdetails.UserDetails;
  import org.springframework.stereotype.Controller;
  import org.springframework.web.bind.annotation.PathVariable;
  import org.springframework.web.bind.annotation.RequestMapping;
  import org.springframework.web.bind.annotation.RequestMethod;
  import org.springframework.web.bind.annotation.ResponseBody;
  import org.springframework.web.servlet.ModelAndView;
  import org.springframework.ui.Model;

  import javax.servlet.http.HttpServletRequest;

  @Controller
  public class MasterDataController {

      @Autowired
      private MasterDataService masterService;

      @RequestMapping(value = "/loanTypes", method = RequestMethod.GET)
      public @ResponseBody
      List<MLoanType> loadLoanTypes() {
          List<MLoanType> loantypes = masterService.findLoanTypes();
          return loantypes;
      }

      @RequestMapping(value = "/subLoanTypes/{loanType}", method = RequestMethod.GET)
      public @ResponseBody
      List<MSubLoanType> loadSubLoanTypes(@PathVariable("loanType") int loanType) {
          List<MSubLoanType> loantypes = masterService.findSubLoanTypes(loanType);
          return loantypes;
      }

      @RequestMapping(value = "/otherCharges", method = RequestMethod.GET)
      public @ResponseBody
      List<MSubTaxCharges> loadOtherCharges() {
          List<MSubTaxCharges> otherCharges = masterService.findOtherCharges();
          return otherCharges;
      }

      @RequestMapping(value = "/otherChargesByLoanId/{subLoanId}", method = RequestMethod.GET)
      public @ResponseBody
      List<OtherChargseModel> otherChargesByLoanId(@PathVariable("subLoanId") int subLoanId) {
          List<OtherChargseModel> otherCharges = masterService.findExtraChargesBySubLoanId(subLoanId);
          return otherCharges;
      }

      @RequestMapping(value = "/checkIsBlock", method = RequestMethod.GET)
      public @ResponseBody
      boolean checkIsBlock() {
          boolean isBlock = masterService.checkUserIsBlock();
          return isBlock;
      }

      @RequestMapping(value = "/checkPageAuthentication", method = RequestMethod.GET)
      public @ResponseBody
      boolean checkPageAuthentication() {
          boolean isLogged = true;
          Authentication auth = SecurityContextHolder.getContext().getAuthentication();
          try {
              UserDetails userDetail = (UserDetails) auth.getPrincipal();
          } catch (Exception e) {
              isLogged = false;
          }
          return isLogged;
      }

      @RequestMapping(value = "/findAllSavingsTypes", method = RequestMethod.GET)
      public @ResponseBody
      List<MSavingsType> findAllSavingsTypes() {
          System.out.println("in savings in controller...");

          List<MSavingsType> savingsTypes = masterService.findAllSavingsTypes();
          MSavingsType savingsType = masterService.findAllSavingsTypesById(2);
          return savingsTypes;
      }

      @RequestMapping(value = "/viewAccountTypeById/{ID}", method = RequestMethod.GET)
      public ModelAndView viewAccountTypeById(@PathVariable("ID") int subLoanId) {
          MSavingsType savingsType = masterService.findAllSavingsTypesById(subLoanId);
          return new ModelAndView("admin/savings/addAccountTypeForm", "addAccountType", savingsType);
      }


      @RequestMapping(value = "/findAllSubSavingsTypes", method = RequestMethod.GET)
      public @ResponseBody
      List<MSubSavingsType> findAllSubSavingsTypes() {
          System.out.println("in sub savings in controller...");

          List<MSubSavingsType> subSavingsTypes = masterService.findAllSubSavingsTypes();
          MSubSavingsType subSavingsTypeObj = masterService.findAllSubSavingsTypesById(2);
          return subSavingsTypes;
      }

      @RequestMapping(value = "/viewSubAccountTypeById/{ID}", method = RequestMethod.GET)
      public ModelAndView viewSubAccountTypeById(@PathVariable("ID") int subLoanId, HttpServletRequest request, Model model) {
          MSubSavingsType subSavingsType = masterService.findAllSubSavingsTypesById(subLoanId);
          List<MSavingsType> savingsTypes = masterService.findAllSavingsTypes();
          model.addAttribute("maccountTypes", savingsTypes);
          return new ModelAndView("admin/savings/addSubAccountTypeForm", "subAccountType", subSavingsType);
      }

      @RequestMapping(value = "/accountTypes/{accountType}", method = RequestMethod.GET)
      public @ResponseBody
      List<MSubSavingsType> loadAccountTypes(@PathVariable("accountType") int accountType) {
          List<MSubSavingsType> accountTypes = masterService.findAllSubSavingsTypes();
          return accountTypes;
      }

      @RequestMapping(value = "/subAccountTypes/{accountType}", method = RequestMethod.GET)
      public @ResponseBody
      List<MSubSavingsType> loadSubAccountTypes(@PathVariable("accountType") int accountType) {
          List<MSubSavingsType> accountTypes = masterService.findSubSavingsTypes(accountType);
          return accountTypes;
      }
  }

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.OpenLoanForm;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.OpenLoanService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OpenLoanController {
    
    @Autowired
    private OpenLoanService openLoan;
    
    
    @Autowired
    LoanService loanService;
    

    @RequestMapping(value = "/saveOpenLoan", method = RequestMethod.POST)
    public ModelAndView saveDebtorForm(@ModelAttribute("newOpenLoan_form") OpenLoanForm formData, BindingResult result, Model model, HttpServletRequest request) {
        int saveId = 0;
        openLoan.addOpenLoan(formData);
        String msg = "Saved Successfully";
        return new ModelAndView("messages/success", "successMessage", msg);
    }

    @RequestMapping(value = "/loadOpenLoanForm", method = RequestMethod.GET)
    public ModelAndView openLoanView(Model model) {
        List<MSubLoanType> subLoanList = loanService.findMSubLoanList();
        model.addAttribute("subLoanList", subLoanList);
        return new ModelAndView("loan/allLoanForm");
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}

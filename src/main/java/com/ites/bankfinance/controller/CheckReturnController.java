/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.controller;

import com.bankfinance.form.ReturnCheckPaymentList;
import com.ites.bankfinance.model.ConfigChartofaccountLoan;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.MSubTaxCharges;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.model.SettlementPayments;
import com.ites.bankfinance.model.SmsDetails;
import com.ites.bankfinance.model.SmsType;
import com.ites.bankfinance.service.CheckReturnService;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.MasterDataService;
import com.ites.bankfinance.service.SettlmentService;
import com.ites.bankfinance.service.UserService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ITESS
 */
@Controller
@RequestMapping(value = "/CheckReturnController")
public class CheckReturnController {

    @Autowired
    LoanService loanService;
    @Autowired
    SettlmentService settlmentService;
    @Autowired
    UserService userService;
    @Autowired
    CheckReturnService checkReturnService;
    @Autowired
    MasterDataService masterService;

    private int userLogBranchId;

    //    loadCheckPayment
    @RequestMapping(value = "/loadCheckPayment", method = RequestMethod.GET)
    public ModelAndView loadCheckPayment(HttpServletRequest request, Model model) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int userId = userService.findByUserName();
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);

        String fromDate = sdf.format(dt);
        String toDate = sdf.format(dt);

        List<ReturnCheckPaymentList> checkPaymentList = checkReturnService.getCheckPayments(fromDate, toDate);
        model.addAttribute("systemDate", dt);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("checkPaymentList", checkPaymentList);
        return new ModelAndView("checkReturn/loadCheckPayments");
    }

    @RequestMapping(value = "/searchCheqPayments", method = RequestMethod.GET)
    public ModelAndView searchCheqPayments(HttpServletRequest request, Model model) {

        int userId = userService.findByUserName();
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);

        String fromDate = null;
        if (request.getParameter("fromDate") != null) {
            fromDate = request.getParameter("fromDate");
        }
        String toDate = null;
        if (request.getParameter("toDate") != null) {
            toDate = request.getParameter("toDate");
        }
        List<ReturnCheckPaymentList> checkPaymentList = checkReturnService.getCheckPayments(fromDate, toDate);

        model.addAttribute("systemDate", dt);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("checkPaymentList", checkPaymentList);
        return new ModelAndView("checkReturn/loadSearchCheckPayments");
    }

//    checkReturnProcess
    @RequestMapping(value = "/checkReturnProcess", method = RequestMethod.GET)
    public ModelAndView checkReturnProcess(HttpServletRequest request, Model model) {
        int userId = userService.findByUserName();
        userLogBranchId = settlmentService.getUserLogedBranch(userId);
        Date dt = settlmentService.getSystemDate(userLogBranchId);
        int userType = userService.findUserTypeByUserId(userId);
        String loanAggNo = request.getParameter("loanAggNo");
        List<ConfigChartofaccountLoan> bankDetails = masterService.findBankAccountsDetails2();

        int checkDetaiId = 0;
        boolean priviledge = false;
        if (request.getParameter("checkDetaiId") != null) {
            checkDetaiId = Integer.parseInt(request.getParameter("checkDetaiId"));
        }
        SettlementPaymentDetailCheque detail = checkReturnService.getChequePaymentDetail(checkDetaiId);
        List<MSubTaxCharges> chargeTypeList = checkReturnService.getChargeTypes();
        if (userType == 1 || userType == 10) {
            priviledge = true;
        }

        model.addAttribute("loanAggNo", loanAggNo);
        model.addAttribute("priviledge", priviledge);
        model.addAttribute("checkDetaiId", checkDetaiId);
        model.addAttribute("detail", detail);
        model.addAttribute("systemDate", dt);
        model.addAttribute("chargeTypeList", chargeTypeList);
        model.addAttribute("banks", bankDetails);
        return new ModelAndView("checkReturn/checkReturnProcess");
    }

//    saveCheckReturn
    @RequestMapping(value = "/saveCheckReturn", method = RequestMethod.GET)
    public ModelAndView saveCheckReturn(HttpServletRequest request, Model model) {

        boolean status = false;
        int userId = userService.findByUserName();
        int checkDetaiId = 0;
        Integer chargeTypeId = 0;
        String successMsg = null;
        String errorMsg = null;
        double chargeAmount = 0;
        if (request.getParameter("checkDetaiId") != null) {
            checkDetaiId = Integer.parseInt(request.getParameter("checkDetaiId"));
        }
        if (request.getParameter("chargeAmount") != null || request.getParameter("chargeAmount") != "") {
            chargeAmount = Double.parseDouble(request.getParameter("chargeAmount"));
        }
        String remark = null;
        if (request.getParameter("remark") != null) {
            remark = request.getParameter("remark");
        }
        int stut = 0;
        if (request.getParameter("status") != null) {
            stut = Integer.parseInt(request.getParameter("status"));
        }
        if (request.getParameter("chargeType") != null) {
            chargeTypeId = Integer.parseInt(request.getParameter("chargeType"));
        }
        int bankId = 0;
        if (request.getParameter("bank") != null) {
            bankId = Integer.parseInt(request.getParameter("bank"));
        }
        String aggNo = "";
        if (request.getParameter("aggNo") != null) {
            aggNo = request.getParameter("aggNo");
        }

        status = checkReturnService.updateChequePayment(checkDetaiId, remark, userId, stut, bankId);

        //add debit entries for return cheques
        if (stut == 1) {
            status = checkReturnService.addDebitEntriesforReturnChecks(checkDetaiId, chargeAmount);
        }
        if (status && stut == 1) {
            status = checkReturnService.addCheckReturnOtherCharge(checkDetaiId, chargeAmount, chargeTypeId);
//            if (status) {
//                // save SMS
//                if (!aggNo.equals("")) {
//                    int loanId = loanService.findLoanIdByAgreementNo(aggNo);
//                    LoanHeaderDetails loanDetails = loanService.findLoanHeaderByLoanId(loanId);
//                    String debtorName = loanDetails.getDebtorHeaderDetails().getNameWithInitial();
//                    int mobileNo = 0;
//                    if (!loanDetails.getDebtorHeaderDetails().getDebtorTelephoneMobile().equals("")) {
//                        mobileNo = Integer.parseInt(loanDetails.getDebtorHeaderDetails().getDebtorTelephoneMobile());
//                    }
//                    String reciptNo = "";
//                    SettlementPaymentDetailCheque cheqDetail = checkReturnService.getChequePaymentDetail(checkDetaiId);
//                    if (cheqDetail.getTransactionNo() != null) {
//                        SettlementPayments paymentDetail = settlmentService.getPaymentDetail(cheqDetail.getTransactionNo());
//                        reciptNo = paymentDetail.getReceiptNo();
//                    }
//                    SmsDetails sms = new SmsDetails();
//                    sms.setSmsDebtorName(debtorName);
//                    sms.setSmsDebtorNumber(mobileNo);
//                    sms.setSmsDescription("DEAR VALUED CUSTOMER, CHEQUE WAS RETURNED FOR-" + reciptNo + ". TO LOAN-" + aggNo);
//                    sms.setSmsLoanId(loanId);
//                    sms.setSmsReceiptNo(reciptNo);
//                    sms.setSmsType(new SmsType(4)); // sms type - cheque return
//                    sms.setIsDelete(false);
//                    sms.setIsSendTo(false);
//                    settlmentService.saveSMS(sms);
//                }
//            }
        }

        if (status) {
            successMsg = "Successfully ..!";
            model.addAttribute("successMessage", successMsg);
            return new ModelAndView("messages/success");
        }
        errorMsg = "Cheque Not Returned.. Create Account NO!";

        model.addAttribute("errorMsg", errorMsg);
        return new ModelAndView("messages/error");
    }

}

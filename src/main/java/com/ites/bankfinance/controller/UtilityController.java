package com.ites.bankfinance.controller;

import com.ites.bankfinance.model.Announcement;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanMessage;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.UmUserType;
import com.ites.bankfinance.model.UserMessage;
import com.ites.bankfinance.service.AdminService;
import com.ites.bankfinance.service.LoanService;
import com.ites.bankfinance.service.UserService;
import com.ites.bankfinance.service.UtilityService;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/UtilityController")
public class UtilityController {

    @Autowired
    private UtilityService utilityService;

    @Autowired
    private UserService userService;

    @Autowired
    private AdminService adminService;
    
    @Autowired
    private LoanService loanService;

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = "/loadAnnouncementMain", method = RequestMethod.GET)
    public ModelAndView loadAnnouncementMain(HttpServletRequest request, Model model) {
        boolean isAdmin = false;
        Date publishDate = Calendar.getInstance().getTime();
        List<Announcement> announcements = null;
        try {
            int userId = userService.findByUserName();
            int userType = userService.findUserTypeByUserId(userId);
            if (userType == 1 || userType == 18 || userType == 7) {
                announcements = utilityService.findAnnouncements(publishDate);
                isAdmin = true;
            } else {
                announcements = utilityService.findAnnouncements(userType, publishDate);
                isAdmin = false;
            }
            model.addAttribute("announcements", announcements);
            model.addAttribute("isAdmin", isAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addAnnouncementMain");
    }

    @RequestMapping(value = "/loadAnnouncementForm", method = RequestMethod.GET)
    public ModelAndView loadAnnouncementForm(HttpServletRequest request, Model model) {
        try {
            List<UmUserType> userTypes = adminService.findUserType();
            model.addAttribute("userTypes", userTypes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addAnnouncementForm");
    }

    @RequestMapping(value = "/saveOrUpdateAnnouncement", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateAnnouncement(@ModelAttribute("announcementForm") Announcement announcement, HttpServletRequest request, BindingResult bindingResult, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        int branchId = Integer.parseInt(request.getSession().getAttribute("branchId").toString());
        try {
            announcement.setBranchId(branchId);
            boolean success = utilityService.saveOrUpdateAnnouncement(announcement);
            if (success) {
                message = "Successfully Save Announcement";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Save Announcement";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadAnnouncementForm/{announcementId}", method = RequestMethod.GET)
    public ModelAndView loadAnnouncementForm(@PathVariable("announcementId") int announcementId, HttpServletRequest request, Model model) {
        try {
            List<UmUserType> userTypes = adminService.findUserType();
            Announcement announcement = utilityService.findAnnouncement(announcementId);
            model.addAttribute("userTypes", userTypes);
            model.addAttribute("announcement", announcement);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addAnnouncementForm");
    }

    @RequestMapping(value = "/deleteAnnouncement/{announcementId}", method = RequestMethod.GET)
    public ModelAndView deleteAnnouncement(@PathVariable("announcementId") int announcementId, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        try {
            boolean success = utilityService.deleteAnnouncement(announcementId);
            if (success) {
                message = "Successfully Delete Announcement";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Delete Announcement";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadPreviousAnnouncementMain", method = RequestMethod.GET)
    public ModelAndView loadPreviousAnnouncementMain(HttpServletRequest request, Model model) {
        boolean isAdmin = false;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date publishDate = cal.getTime();
        List<Announcement> announcements = null;
        try {
            int userId = userService.findByUserName();
            int userType = userService.findUserTypeByUserId(userId);
            if (userType == 1 || userType == 18 || userType == 7) {
                announcements = utilityService.findAnnouncements(publishDate);
                isAdmin = true;
            } else {
                announcements = utilityService.findAnnouncements(userType, publishDate);
                isAdmin = false;
            }
            model.addAttribute("announcements", announcements);
            model.addAttribute("isAdmin", isAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/previousAnnoucementMain");
    }

    @RequestMapping(value = "/loadMessageMain", method = RequestMethod.GET)
    public ModelAndView loadMessageMain(HttpServletRequest request, Model model) {
        boolean def = true;
        Date publishDate = Calendar.getInstance().getTime();
        List<UserMessage> userMessages = null;
        List<LoanMessage> loanMessages = null;
        try {
            int userId = userService.findByUserName();
            userMessages = utilityService.findUserMessages(publishDate);
            loanMessages = utilityService.findLoanMessages(publishDate);
            model.addAttribute("userMessages", userMessages);
            model.addAttribute("loanMessages", loanMessages);
            model.addAttribute("def", def);
            model.addAttribute("userId", userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addMessageMain");
    }

    @RequestMapping(value = "/loadMessageMain/{startDate}/{endDate}", method = RequestMethod.GET)
    public ModelAndView loadMessageMain(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, HttpServletRequest request, Model model) {
        boolean def = true;
        List<UserMessage> userMessages = null;
        List<LoanMessage> loanMessages = null;
        try {
            Date sDate = dateFormat.parse(startDate);
            Date eDate = dateFormat.parse(endDate);
            int userId = userService.findByUserName();
            userMessages = utilityService.findUserMessages(sDate, eDate);
            loanMessages = utilityService.findLoanMessages(sDate, eDate);
            model.addAttribute("userMessages", userMessages);
            model.addAttribute("loanMessages", loanMessages);
            model.addAttribute("isAdmin", def);
            model.addAttribute("userId", userId);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addMessageMain");
    }

    @RequestMapping(value = "/loadUserMessageForm", method = RequestMethod.GET)
    public ModelAndView loadUserMessageForm(HttpServletRequest request, Model model) {
        try {
            List<UmUser> users = adminService.findUmUser();
            model.addAttribute("users", users);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addUserMessageForm");
    }

    @RequestMapping(value = "/saveOrUpdateUserMessage", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateUserMessage(@ModelAttribute("userMessageForm") UserMessage userMessage, HttpServletRequest request, BindingResult bindingResult, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        int branchId = Integer.parseInt(request.getSession().getAttribute("branchId").toString());
        try {
            userMessage.setBranchId(branchId);
            boolean success = utilityService.saveOrUpdateUserMessage(userMessage);
            if (success) {
                message = "Successfully Save Message";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Save Message";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadUserMessageForm/{userMessageId}", method = RequestMethod.GET)
    public ModelAndView loadUserMessageForm(@PathVariable("userMessageId") int userMessageId, HttpServletRequest request, Model model) {
        try {
            List<UmUser> users = adminService.findUmUser();
            UserMessage userMessage = utilityService.findUserMessage(userMessageId);
            model.addAttribute("users", users);
            model.addAttribute("userMessage", userMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addUserMessageForm");
    }

    @RequestMapping(value = "/deleteUserMessage/{userMessageId}", method = RequestMethod.GET)
    public ModelAndView deleteUserMessage(@PathVariable("userMessageId") int userMessageId, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        try {
            boolean success = utilityService.deleteUserMessage(userMessageId);
            if (success) {
                message = "Successfully Delete Message";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Delete Message";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/readUserMessage/{userMessageId}", method = RequestMethod.GET)
    @ResponseBody
    public UserMessage readUserMessage(@PathVariable("userMessageId") int userMessageId, HttpServletRequest request, Model model) {
        UserMessage userMessage = null;
        try {
            utilityService.readUserMessage(userMessageId);
            userMessage = utilityService.findUserMessage(userMessageId);
            model.addAttribute("userMessage", userMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userMessage;
    }

    @RequestMapping(value = "/loadLoanMessageForm", method = RequestMethod.GET)
    public ModelAndView loadLoanMessageForm(HttpServletRequest request, Model model) {
        return new ModelAndView("utility/addLoanMessageForm");
    }

    @RequestMapping(value = "/saveOrUpdateLoanMessage", method = RequestMethod.POST)
    public ModelAndView saveOrUpdateLoanMessage(@ModelAttribute("loanMessageForm") LoanMessage loanMessage, HttpServletRequest request, BindingResult bindingResult, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        int branchId = Integer.parseInt(request.getSession().getAttribute("branchId").toString());
        try {
            loanMessage.setBranchId(branchId);
            boolean success = utilityService.saveOrUpdateLoanMessage(loanMessage);
            if (success) {
                message = "Successfully Save Message";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Save Message";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadLoanMessageForm/{loanMessageId}", method = RequestMethod.GET)
    public ModelAndView loadLoanMessageForm(@PathVariable("loanMessageId") int loanMessageId, HttpServletRequest request, Model model) {
        try {
            LoanMessage loanMessage = utilityService.findLoanMessage(loanMessageId);
            model.addAttribute("loanMessage", loanMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addLoanMessageForm");
    }

    @RequestMapping(value = "/deleteLoanMessage/{loanMessageId}", method = RequestMethod.GET)
    public ModelAndView deleteLoanMessage(@PathVariable("loanMessageId") int loanMessageId, HttpServletRequest request, Model model) {
        ModelAndView modelAndView = null;
        String message = "";
        try {
            boolean success = utilityService.deleteLoanMessage(loanMessageId);
            if (success) {
                message = "Successfully Delete Message";
                model.addAttribute("successMessage", message);
                modelAndView = new ModelAndView("messages/success");
            } else {
                message = "Error in Delete Message";
                model.addAttribute("errorMessage", message);
                modelAndView = new ModelAndView("messages/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/loadLoanMessageMain/{loanId}", method = RequestMethod.GET)
    public ModelAndView loadLoanMessageMain(@PathVariable("loanId") int loanId, Model model) {
        boolean def = false;
        List<LoanMessage> loanMessages = null;
        try {
            LoanHeaderDetails loanDetails = loanService.findLoanHeaderDetails(loanId);
            loanMessages = utilityService.findLoanMessages(loanId);
            model.addAttribute("loanMessages", loanMessages);
            model.addAttribute("loanDetails", loanDetails);
            model.addAttribute("def", def);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("utility/addLoanMessageMain");
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.reportManager;

import com.ites.bankfinance.service.MasterDataService;
import java.io.File;
import java.sql.Connection;
import java.util.Map;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterName;
import javax.print.attribute.standard.Sides;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;

public class ReportManager {

    MasterDataService masterData;
    
    public boolean generateReport(File reportJasper, Map<String, Object> params, String title, Connection c) throws JRException {
        JasperReport jasperReport3 = (JasperReport) JRLoader.loadObject(reportJasper.getPath());
        JasperPrint jasperPrint3 = JasperFillManager.fillReport(jasperReport3, params, c);
        //jasperPrint3.setOrientation(JasperReport.ORIENTATION_PORTRAIT);
       
        JasperExportManager.exportReportToPdfFile(jasperPrint3, "C:\\report\\check.pdf");
        JRSaver.saveObject(jasperPrint3, "C:\\report\\check.jrprint");
        return Print();
    }

    private boolean Print() {
       
//        String printerName = masterData.findPrinterName(); 
        String printerName = "pos1";
        System.out.println("printerName--"+printerName);
        boolean ret=false;
        /* Create an array of PrintServices */ 
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);         
                
        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();
      
        PrintServiceAttributeSet set = new HashPrintServiceAttributeSet();
        set.add(new PrinterName(printerName, null));       

        
        services = PrintServiceLookup.lookupPrintServices(null, set); 
        System.out.println("===="+services.length);       
           
        
        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        exporter.setParameter(JRExporterParameter.INPUT_FILE_NAME, "C://report/check.jrprint");
        
//        if(services.length>0)
//            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, services[selectedService]); 
        boolean duplex=false;
        for (int j=0; j<services.length;j++) {
            duplex = false;
            if (services[j].isAttributeValueSupported(Sides.DUPLEX, null, null)) {
                duplex = true;
            }
        }

        attributeSet.add(MediaSizeName.ISO_A4);        
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET, attributeSet);        
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, set);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
        try {
            exporter.exportReport();
            ret = true;
        } catch (JRException ex) {
            ex.printStackTrace();
//            System.out.println("Printer is not found");
            ret = false;
        }
        return ret;
    
    }
}

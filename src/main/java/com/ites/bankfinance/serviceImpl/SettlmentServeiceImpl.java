/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.LoanPosting;
import com.bankfinance.form.OtherChargseModel;
import com.bankfinance.form.RebateForm;
import com.bankfinance.form.RebateQuotation;
import com.bankfinance.form.StlmntReturnList;
import com.ites.bankfinance.dao.SettlementDao;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.service.SettlmentService;
import java.io.File;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MsD
 */
@Service
public class SettlmentServeiceImpl implements SettlmentService {

    @Autowired
    SettlementDao settlementDao;

    @Transactional
    public String getTransactionIdMax(String code) {
        return settlementDao.getTransactionIdMax(code);
    }

    @Transactional
    public String getReceiptNo(String refNo) {
        return settlementDao.getReceiptNo(refNo);
    }

    @Transactional
    public boolean addSettlementReceiptDetail(SettlementReceiptDetail settlementReceiptDetail) {
        return settlementDao.addSettlementReceiptDetail(settlementReceiptDetail);
    }

    @Transactional
    public boolean addCashPaymentDetails(int userId, double payAmount, String transactioId) {
        boolean status = settlementDao.addCashPaymentDetails(userId, payAmount, transactioId);
        return status;
    }

    @Transactional
    public boolean addChequePaymentDetails(SettlementPaymentDetailCheque formData) {
        boolean status = settlementDao.addChequePaymentDetails(formData);
        return status;
    }


     @Transactional
     public SettlementPaymentDetailCheque getSettlementPaymentDetailChequeDetails(String voucherNo){
        return settlementDao.getSettlementPaymentDetailChequeDetails(voucherNo);
    }

    @Transactional
    public SettlementVoucher getSettlementVoucherDetailsbyLoanId(int loanId){
        return settlementDao.getSettlementVoucherDetailsbyLoanId(loanId);
    }

    @Transactional
    public boolean addBankDepositePaymentDetails(SettlementPaymentDetailBankDeposite formData) {
        boolean status = settlementDao.addBankDepositePaymentDetails(formData);
        return status;
    }

    @Transactional
    public int getCashPaymentDetailsId() {
        int transactionId = 0;
        transactionId = settlementDao.getCashPaymentDetailsId();
        return transactionId;
    }

    @Transactional
    public int getChequePaymentDetailsId() {
        int transactionId = 0;
        transactionId = settlementDao.getChequePaymentDetailsId();
        return transactionId;
    }

    @Transactional
    public int getBankDepositePaymentDetailsId() {
        int transactionId = 0;
        transactionId = settlementDao.getBankDepositePaymentDetailsId();
        return transactionId;
    }

    @Transactional
    public int getUserLogedBranch(int userId) {
        int branchid = 0;
        branchid = settlementDao.getUserLogedBranch(userId);
        return branchid;
    }

    @Transactional
    public boolean addSettlmentPayment(SettlementPayments settlementPayments) {
        boolean settlementFrontPayment = settlementDao.addSettlmentPayment(settlementPayments);
        return settlementFrontPayment;
    }

    public double getSumOfOtherCherges(List otherCharges) {
        double SumOfOtherCherges = 0.00;
        List<OtherChargseModel> otherChargesList = (List<OtherChargseModel>) otherCharges;
        for (int i = 0; i < otherChargesList.size(); i++) {
            SumOfOtherCherges = SumOfOtherCherges + otherChargesList.get(i).getAmount();
        }
        return SumOfOtherCherges;
    }

//    @Transactional
//    public List findAllCharges(int loanId) {
//    return settlementDao.findAllCharges(loanId);
//    }
//     public double getSumOfOAllCherges(List allCharges) {
//        double SumOfAllCherges=0.00;
//        List<SettlementCharges> allChargesList =(List<SettlementCharges>)allCharges;
//       for(int i=0;i<allChargesList.size();i++){
//           SumOfAllCherges =SumOfAllCherges +allChargesList.get(i).getCharge();
//       }        
//      return SumOfAllCherges;
//    }
    @Transactional
    public MBankDetails getBankDetails(int bankId) {
        return settlementDao.getBankDetails(bankId);
    }

    @Transactional
    public List findPayTypes() {
        return settlementDao.findPayTypes();
    }

    @Transactional
    public List findBankList() {
        return settlementDao.findBankList();
    }

    @Transactional
    public List getPaidLoanInstallmnetList(int loanId) {
        return settlementDao.getPaidLoanInstallmnetList(loanId);
    }

    @Transactional
    public boolean addSettlmentPaymentDetails(SettlementAddNewPayment formData, int maxTransactionId, int userId) {

//         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        java.util.Date dt = new java.util.Date();
//        
//        int paymentTypeId[] =  formData.getPaymentTypeId();
//        double Amount[]= formData.getAmount();
//        int loanId = formData.getLoanId();
//        int BankId[] = formData.getBankId();         
//        String amountText[] = formData.getAmountText();
//        String AccountNo[] = formData.getAccNo();
//        String TransDate[] = formData.getTransDate();
//        String installmentId[] = formData.getInstallmentId();
//        String installmentAmount[] = formData.getInstallmentAmount();
//
//        SettlementPaymentDetailCheque settlementPaymentDetailCheque = new SettlementPaymentDetailCheque(); 
//        SettlementPaymentDetailBankDeposite settlementPaymentDetailBankDeposite = new SettlementPaymentDetailBankDeposite();
//        SettlementPayments settlementPayments = new SettlementPayments();
//        
//        DebtorHeaderDetails debtorHeaderDetail = findDebtorByLoanId(loanId);
//        
//        String accNo =null;
//        String receiptNo =null;
//        String transactionNo = null;
//       
//        
//        try {
//        
//                for(int i = 0; i< paymentTypeId.length ; i++){
//                    if(paymentTypeId[i]==1){
//                            int detailId = addCashPaymentDetails(Amount[i],maxTransactionId);                      
//                            accNo = getAccountNo(paymentTypeId[i]);
//
//                            settlementPayments.setAccountNo(accNo);
//                            settlementPayments.setPaymentAmount(Amount[i]);
//                            settlementPayments.setBranchId(1);
//                            settlementPayments.setDebtorId(debtorHeaderDetail.getDebtorId());
//                            settlementPayments.setLoanId(loanId);
//                            settlementPayments.setPaymentDetails(detailId);
//                            settlementPayments.setPaymentType(paymentTypeId[i]);
//                            settlementPayments.setReceiptNo(receiptNo);
//                            settlementPayments.setTransactionNo(transactionNo);
//                            settlementPayments.setUserId(userId);
//                            settlementPayments.setRemark(formData.getRemark());
//                            addSettlmentPayment(settlementPayments);
//
//                    }else if(paymentTypeId[i]==2){
//                            settlementPaymentDetailCheque.setAmount(Amount[i]);
//                            settlementPaymentDetailCheque.setBank(getBankDetails(BankId[i]).getBankName());
//                            settlementPaymentDetailCheque.setChequeNo(AccountNo[i]);
//                            settlementPaymentDetailCheque.setRealizeDate(sdf.parse(TransDate[i]));                   
//                            settlementPaymentDetailCheque.setTransactionDate(dt);
//                            settlementPaymentDetailCheque.setUserId(userId);
//                            settlementPaymentDetailCheque.setTransactionId(maxTransactionId);                    
//
//                            int detailId2 = addChequePaymentDetails(settlementPaymentDetailCheque);
//                            accNo = getAccountNo(paymentTypeId[i]);
//
//                            settlementPayments.setAccountNo(accNo);
//                            settlementPayments.setPaymentAmount(Amount[i]);
//                            settlementPayments.setBranchId(1);
//                            settlementPayments.setDebtorId(debtorHeaderDetail.getDebtorId());
//                            settlementPayments.setLoanId(loanId);
//                            settlementPayments.setPaymentDetails(detailId2);
//                            settlementPayments.setPaymentType(paymentTypeId[i]);
//                            settlementPayments.setReceiptNo(receiptNo);
//                            settlementPayments.setTransactionNo(transactionNo);
//                            settlementPayments.setUserId(userId);
//                            settlementPayments.setRemark(formData.getRemark());
//                            addSettlmentPayment(settlementPayments);
//
//                         }else if(paymentTypeId[i]==3){
//                            settlementPaymentDetailBankDeposite.setAmount(Amount[i]);
//                            settlementPaymentDetailBankDeposite.setBankName(getBankDetails(BankId[i]).getBankName());
//                            settlementPaymentDetailBankDeposite.setDepositeAccountNo(AccountNo[i]);
//                            settlementPaymentDetailBankDeposite.setDepositeDate(sdf.parse(TransDate[i]));
//                            settlementPaymentDetailBankDeposite.setTransactionId(maxTransactionId);
//                            settlementPaymentDetailBankDeposite.setLoanId(loanId);
//                            settlementPaymentDetailBankDeposite.setBankBranch(getBankDetails(BankId[i]).getBankBranch());
//                            settlementPaymentDetailBankDeposite.setUserId(userId);
//
//                            int detailId3 = addBankDepositePaymentDetails(settlementPaymentDetailBankDeposite);
//                            accNo = getAccountNo(paymentTypeId[i]);
//
//                            settlementPayments.setAccountNo(accNo);
//                            settlementPayments.setPaymentAmount(Amount[i]);
//                            settlementPayments.setBranchId(1);
//                            settlementPayments.setDebtorId(debtorHeaderDetail.getDebtorId());
//                            settlementPayments.setLoanId(loanId);
//                            settlementPayments.setPaymentDetails(detailId3);
//                            settlementPayments.setPaymentType(paymentTypeId[i]);
//                            settlementPayments.setReceiptNo(receiptNo);
//                            settlementPayments.setTransactionNo(transactionNo);
//                            settlementPayments.setUserId(userId);
//                            settlementPayments.setRemark(formData.getRemark());
//                            addSettlmentPayment(settlementPayments);
//
//                    }        
//                }
//        } catch (ParseException ex) {
//            Logger.getLogger(SettlmentServeiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
        return true;
    }

    @Transactional
    public String getAccountNo(int paytype) {
        return settlementDao.getAccountNo(paytype);
    }

    @Transactional
    public DebtorHeaderDetails findDebtorByLoanId(int loanId) {
        return settlementDao.findDebtorByLoanId(loanId);
    }

    @Transactional
    public String getMaxSerialNo() {
        return settlementDao.getMaxSerialNo();
    }

    @Transactional
    public List<SettlementRefCodeConfig> getRefCode() {
        return settlementDao.getRefCode();
    }

    @Transactional
    public boolean addSettlementMain(SettlementMain settlementMain) {
        return settlementDao.addSettlementMain(settlementMain);
    }

    @Transactional
    public boolean addSettlementMain2(SettlementMain settlementMain, String account) {
        return settlementDao.addSettlementMain2(settlementMain, account);
    }

    @Transactional
    public List<ReturnList> findOpenningBalance(int loanId) {
        return settlementDao.findOpenningBalance(loanId);
    }

    public double getSumOfOpenningBalance(List<ReturnList> openningBalance) {
        double total = 0;
        if (openningBalance != null) {
            for (int i = 0; i < openningBalance.size(); i++) {
                total = total + openningBalance.get(i).getAmount();
            }
        }
        return total;
    }

    @Transactional
    public double getOpenningBalanceWithoputODI(int loanId) {
        return settlementDao.getOpenningBalanceWithoputODI(loanId);
    }

    @Transactional
    public double findTotalPayment(int loanId) {
        return settlementDao.findTotalPayment(loanId);
    }

    @Transactional
    public double findTotalCharges(int loanId) {
        return settlementDao.findTotalCharges(loanId);
    }

    @Transactional
    public double getTotalDownPayment(int loanId) {
        return settlementDao.getTotalDownPayment(loanId);
    }

    @Transactional
    public double getDownPaymentBalance(int loanId) {
        return settlementDao.getDownPaymentBalance(loanId);
    }

    @Transactional
    public double getPaidArrears(int loanId) {
        return settlementDao.getPaidArrears(loanId);
    }

    @Transactional
    public double getPaidOtherCharges(int loanId) {
        return settlementDao.getPaidOtherCharges(loanId);
    }

    @Transactional
    public double getPaidDownPayment(int loanId) {
        return settlementDao.getPaidDownPayment(loanId);
    }

    @Transactional
    public double getPaidInsuarance(int loanId) {
        return settlementDao.getPaidInsuarance(loanId);
    }

    @Transactional
    public double getTotalInsuarance(int loanId) {
        return settlementDao.getTotalInsuarance(loanId);
    }

    @Transactional
    public List getDebitInstallmnetList(int loanId) {
        return settlementDao.getDebitInstallmnetList(loanId);
    }

    @Transactional
    public List<InstallmentReturnList> getDebitCreditInstallmentList(int loanId) {
        return settlementDao.getDebitCreditInstallmentList(loanId);
    }

    @Transactional
    public double getOverPayAmount(int loanId) {
        return settlementDao.getOverPayAmount(loanId);
    }

    @Transactional
    public void addOverPayAmount(int loanId, double totalPayment, int userid, int debtorId, String newTransactionNo, String code, String receiptNo, String codeDescrpt, int branchId, Date systemDate) {
        settlementDao.addOverPayAmount(loanId, totalPayment, userid, debtorId, newTransactionNo, code, receiptNo, codeDescrpt, branchId, systemDate);
    }

    @Transactional
    public boolean updateLoanHeaderIsDownPaymntPaid(int loanId, double initialDownPayment) {
        return settlementDao.updateLoanHeaderIsDownPaymntPaid(loanId, initialDownPayment);
    }

    @Transactional
    public LoanInstallment findLoanInstallment(int loanId) {
        return settlementDao.findLoanInstallment(loanId);
    }

    @Transactional
    public boolean addChargeDetails(SettlementCharges settlementCharges) {
        return settlementDao.addChargeDetails(settlementCharges);
    }

    @Transactional
    public boolean updateInsuaranceStatus(int loanId, int status) {
        return settlementDao.updateInsuaranceStatus(loanId, status);
    }

    @Transactional
    public List<SettlementMain> getInstalmntWithODI(int loanId) {
        return settlementDao.getInstalmntWithODI(loanId);
    }

    @Transactional
    public Date getSystemDate(int branchId) {
        return settlementDao.getSystemDate(branchId);
    }

    @Transactional
    public List<StlmntReturnList> getPaymentList(Date systemDate, int userBranchId) {
        return settlementDao.getPaymentList(systemDate, userBranchId);
    }

    @Transactional
    public SettlementPayments getPaymentDetail(int transactionId) {
        return settlementDao.getPaymentDetail(transactionId);
    }

    @Transactional
    public boolean addSettlementPaymentDelete(SettlementPayments settlementPayments, SettlementPaymentDeleteForm sttlmntDeleteDeleteForm) {
        return settlementDao.addSettlementPaymentDelete(settlementPayments, sttlmntDeleteDeleteForm);
    }

    @Transactional
    public boolean removeStlMainChargePayment(String receiptNo) {
        return settlementDao.removeStlMainChargePayment(receiptNo);
    }

    @Transactional
    public double getInstalmentBalance(int loanId) {
        double balance = settlementDao.getInstalmentBalance(loanId);
        return balance;
    }

    @Transactional
    public void addOverPayDebit(int loanId, double overPayAmount, int userid, Integer debtorId, String newTransactionNo, String refNoOverPayCode, String receiptNo, String refNoOverPayCodeDescrpt, int userLogBranchId, Date systemDate) {
        settlementDao.addOverPayDebit(loanId, overPayAmount, userid, debtorId, newTransactionNo, refNoOverPayCode, receiptNo, refNoOverPayCodeDescrpt, userLogBranchId, systemDate);
    }

    @Transactional
    public List findOtherChargesByLoanId(int loanId) {
        return settlementDao.findOtherChargesByLoanId(loanId);
    }

    @Transactional
    public boolean updatePaidStatus(Integer chargeID) {
        return settlementDao.updatePaidStatus(chargeID);
    }

    @Transactional
    public String getAdjustTransCode(String refNoAdjustment) {
        return settlementDao.getAdjustTransCode(refNoAdjustment);
    }

    @Transactional
    public double getOdiBalance(int loanId) {
        return settlementDao.getOdiBalance(loanId);
    }

    @Transactional
    public UmUser checkUserPrivileges(String uname, String pword) {
        return settlementDao.checkUserPrivileges(uname, pword);
    }

    @Transactional
    public double[] getTotalBalancesForDayEnd(Date systemDate, int loginBranchID) {
        return settlementDao.getTotalBalancesForDayEnd(systemDate, loginBranchID);
    }

    @Transactional
    public boolean updateLoanHeaderIsDownPaymntPaid(int loanId, double initialOtherCharge, String othr) {
        return settlementDao.updateLoanHeaderIsDownPaymntPaid(loanId, initialOtherCharge, othr);
    }

    @Transactional
    public List<InstallmentReturnList> findAdjustmentList(int loanId, String refNo) {
        return settlementDao.findAdjustmentList(loanId, refNo);
    }

    @Transactional
    public double getAdjustmntBalance(int loanId, String refNoAdjustment) {
        return settlementDao.getAdjustmntBalance(loanId, refNoAdjustment);
    }

    @Transactional
    public double getTotalInterest(int loanId, String refNoAdjustment) {
        return settlementDao.getTotalInterest(loanId, refNoAdjustment);
    }

    @Transactional
    public int getInstalmentCount(int loanId, String refNoInstallmentCode) {
        return settlementDao.getInstalmentCount(loanId, refNoInstallmentCode);
    }

    @Transactional
    public int getLoanIdByReceiptNo(String receiptNo) {
        return settlementDao.getLoanIdByReceiptNo(receiptNo);
    }

    @Transactional
    public RebateForm findRebateCustomerList(String agreementNo) {
        return settlementDao.findRebateCustomerList(agreementNo);
    }

    @Transactional
    public Double findRemainingInterestAmount(int loanID) {
        return settlementDao.findRemainingInterestAmount(loanID);
    }

    @Transactional
    public Boolean saveRebateLoan(RebateDetail rebateDetails) {
        return settlementDao.saveRebateLoan(rebateDetails);
    }

    @Transactional
    public int getInstalmentCount(int loanID) {
        return settlementDao.getInstalmentCount(loanID);
    }

    @Transactional
    public boolean saveSMS(SmsDetails smsDetails) {
        return settlementDao.saveSMS(smsDetails);
    }

    @Transactional
    public boolean removeSMS(int loanId, String recptNo) {
        return settlementDao.removeSMS(loanId, recptNo);
    }

    @Transactional
    public List<RebateDetail> getRebateDetails() {
        return settlementDao.getRebateDetails();
    }

    @Transactional
    public boolean deleteRebateLoan(int loanId) {
        return settlementDao.deleteRebateLoan(loanId);
    }

    @Transactional
    public boolean saveOrUpdateLoanCheque(LoanCheques loanCheques) {
        return settlementDao.saveOrUpdateLoanCheque(loanCheques);
    }

    @Transactional
    public List<LoanCheques> getLoanChequeses() {
        return settlementDao.getLoanChequeses();
    }

    @Transactional
    public List<LoanCheques> getLoanChequeses(int loanId) {
        return settlementDao.getLoanChequeses(loanId);
    }

    @Transactional
    public boolean deleteLoanCheque(int chequeId, int loanId) {
        return settlementDao.deleteLoanCheque(chequeId, loanId);
    }

    @Transactional
    public boolean setPaidLoanCheque(int chequeId, int loanId) {
        return settlementDao.setPaidLoanCheque(chequeId, loanId);
    }

    @Transactional
    public double getRebateBalance(int loanId) {
        return settlementDao.getRebateBalance(loanId);
    }

    @Transactional
    public boolean updateLoanHeaderStatus(int loanId, int type) {
        return settlementDao.updateLoanHeaderStatus(loanId, type);
    }

    @Transactional
    public boolean updateRebateDetail(int loanId) {
        return settlementDao.updateRebateDetail(loanId);
    }

    @Transactional
    public Double getLoanChequeTotAmount(int loanId) {
        return settlementDao.getLoanChequeTotAmount(loanId);
    }

    @Transactional
    public List<RebateDetail> loadRebateApprovalList() {
        return settlementDao.loadRebateApprovalList();
    }

    @Transactional
    public int findPaidInstallmentCount(int loanID) {
        return settlementDao.findPaidInstallmentCount(loanID);
    }

    @Transactional
    public RebateApprovals findRebateApprovals(int loanId, int type) {
        return settlementDao.findRebateApprovals(loanId, type);
    }

    @Transactional
    public Boolean saveRebateApproval(RebateApprovals approvals) {
        return settlementDao.saveRebateApproval(approvals);
    }

    @Transactional
    public RebateDetail findRebateDetails(int loanId) {
        return settlementDao.findRebateDetails(loanId);
    }

    @Transactional
    public boolean isInitialPaymentDone(int loanId) {
        return settlementDao.isInitialPaymentDone(loanId);
    }

    @Transactional
    public LoanHeaderDetails findLoanHeaderDetails(int printLoanId) {
        return settlementDao.findLoanHeaderDetails(printLoanId);
    }

    @Transactional
    public RebateDetail findRebateDetailsAfterRebate(int loanId) {
        return settlementDao.findRebateDetailsAfterRebate(loanId);
    }

    @Transactional
    public SettlementPayments getPaymentDetail(String transactionNo) {
        return settlementDao.getPaymentDetail(transactionNo);
    }

    @Transactional
    public List<LoanInstallment> findPaybleInstalmentsList(int loanId, Date systemDate) {
        return settlementDao.findPaybleInstalmentsList(loanId, systemDate);
    }

    @Transactional
    public boolean saveOrUpdateDebtorDeposit(DebtorDeposit debtorDeposit) {
        return settlementDao.saveOrUpdateDebtorDeposit(debtorDeposit);
    }

    @Transactional
    public List<DebtorDeposit> findDebtorDeposits(int branchId) {
        return settlementDao.findDebtorDeposits(branchId);
    }

    @Transactional
    public DebtorDeposit findDebtorDeposit(int depositId) {
        return settlementDao.findDebtorDeposit(depositId);
    }

    @Transactional
    public List<DebtorDeposit> findDebtorDeposits(int debtorId, String accNo) {
        return settlementDao.findDebtorDeposits(debtorId, accNo);
    }

    @Transactional
    public boolean updateDebtorDeposit(int debtorId, String accNo) {
        return settlementDao.updateDebtorDeposit(debtorId, accNo);
    }

    @Transactional
    public boolean updateInsuaranceProcessIsPay(int loanId) {
        return settlementDao.updateInsuaranceProcessIsPay(loanId);
    }

    @Transactional
    public boolean updateRemainingInsuraceProcessMainList(int loanId, double insuranceBalance) {
        return settlementDao.updateRemainingInsuraceProcessMainList(loanId, insuranceBalance);
    }

    @Transactional
    public boolean saveOrUpdateLoanRescheduleDetails(LoanRescheduleDetails loanRescheduleDetails) {
        return settlementDao.saveOrUpdateLoanRescheduleDetails(loanRescheduleDetails);
    }

    @Transactional
    public List<LoanRescheduleDetails> findLoanRescheduleDetails() {
        return settlementDao.findLoanRescheduleDetails();
    }

    @Transactional
    public boolean activeLoanRescheduleDetails(int rescheduleId) {
        return settlementDao.activeLoanRescheduleDetails(rescheduleId);
    }

    @Transactional
    public boolean inActiveLoanRescheduleDetails(int rescheduleId) {
        return settlementDao.inActiveLoanRescheduleDetails(rescheduleId);
    }

    @Transactional
    public LoanRescheduleDetails findLoanRescheduleDetail(int rescheduleId) {
        return settlementDao.findLoanRescheduleDetail(rescheduleId);
    }

    @Transactional
    public boolean changeLoanRescheduleDetailStatus(int rescheduleId, int childLoanId) {
        return settlementDao.changeLoanRescheduleDetailStatus(rescheduleId, childLoanId);
    }

    @Transactional
    public List<SettlementMain> findByLoanId(int loanId) {
        return settlementDao.findByLoanId(loanId);
    }

    @Transactional
    public List<Posting> findByReferenceNo(String referenceNo) {
        return settlementDao.findByReferenceNo(referenceNo);
    }

    @Transactional
    public List<LoanPosting> findLoanPostings(int loanId) {
        return settlementDao.findLoanPostings(loanId);
    }

    @Transactional
    public double getLoanPaidOtherCharges(int loanId) {
        return settlementDao.getLoanPaidOtherCharges(loanId);
    }

    @Transactional
    public double getLoanPaidInsurenceCharges(int loanId) {
        return settlementDao.getLoanPaidInsurenceCharges(loanId);
    }

    @Transactional
    public double getLoanPaidInstallments(int loanId) {
        return settlementDao.getLoanPaidInstallments(loanId);
    }

    @Transactional
    public File batchPostingToZip(Date startDate, Date endDate) {
        return settlementDao.batchPostingToZip(startDate, endDate);
    }

    @Transactional
    public List<LoanHeaderDetails> findIssueLoans() {
        return settlementDao.findIssueLoans();
    }

    @Transactional
    public List<SettlementMain> findSettlements(int loanId, Date systemDate) {
        return settlementDao.findSettlements(loanId, systemDate);
    }

    @Transactional
    public List<StlmntReturnList> getPaymentList(String sDate, String eDate, int userBranchId) {
        return settlementDao.getPaymentList(sDate, eDate, userBranchId);
    }

    @Transactional
    public boolean isLoanChequesExist(Date systemDate) {
        return settlementDao.isLoanChequesExist(systemDate);
    }

    @Transactional
    public List<StlmntReturnList> getPaymentListForReceiptNo(String receiptNo, int userBranchId) {
        return settlementDao.getPaymentListForReceiptNo(receiptNo, userBranchId);
    }

    @Transactional
    public List<StlmntReturnList> getPaymentListForAgNo(String agNo, int userBranchId) {
        return settlementDao.getPaymentListForAgNo(agNo, userBranchId);
    }

    @Transactional
    public List<StlmntReturnList> getPaymentListForVehNo(String vehNo, int userBranchId) {
        return settlementDao.getPaymentListForVehNo(vehNo, userBranchId);
    }

    @Transactional
    public List<StlmntReturnList> getPaymentListForPayMode(int payMode, int userBranchId) {
        return settlementDao.getPaymentListForPayMode(payMode, userBranchId);
    }

    @Transactional
    public double getPaidOdiBalance(int loanId) {
        return settlementDao.getPaidOdiBalance(loanId);
    }

    @Transactional
    public RebateQuotation getRebateQuotation(int loanId) {
        return settlementDao.getRebateQuotation(loanId);
    }

    @Transactional
    public boolean addSettlementTransfer(SettlementTransfer st) {
        return settlementDao.addSettlementTransfer(st);
    }

    @Transactional
    public List<SettlementMain> loadRefundTable(String sDate, String eDate, int userLogBranchId) {
        return settlementDao.loadRefundTable(sDate, eDate, userLogBranchId);
    }

    @Transactional
    public List<SettlementMain> loadRefundTablebyAgNo(String AgreementNo, int userLogBranchId) {
        return settlementDao.loadRefundTablebyAgNo(AgreementNo, userLogBranchId);
    }

    @Transactional
    public List<SettlementMain> loadRefundTablebyVehNo(String vehicleNo, int userLogBranchId) {
        return settlementDao.loadRefundTablebyVehNo(vehicleNo, userLogBranchId);
    }

    @Transactional
    public List<SettlementMain> loadRefundTablebyCustomer(String customerName, int userLogBranchId) {
        return settlementDao.loadRefundTablebyCustomer(customerName, userLogBranchId);
    }

    @Transactional
    public SettlementMain getSettlementLoanheaderDebtorDetailsByLoanID(int loanId) {
        return settlementDao.getSettlementLoanheaderDebtorDetailsByLoanID(loanId);
    }

    @Transactional
    public Double getOverpayAmt(int loanid) {
        return settlementDao.getOverpayAmt(loanid);
    }

    @Transactional
    public boolean saveRefund(SettlementRefund sr) {
        return settlementDao.saveRefund(sr);
    }

    @Transactional
    public boolean updateSettlementMainonRefund(int loanId, Double refundAmount, String receiptNo, Date sysDate) {
        return settlementDao.updateSettlementMainonRefund(loanId, refundAmount, receiptNo, sysDate);
    }

    @Transactional
    public File batchPostingODIToZip(Date startDate, Date endDate, int vType) {
        return settlementDao.batchPostingODIToZip(startDate, endDate, vType);
    }

    @Transactional
    public void updateLoanInstallment(int loanId, double remainingInstallmentBalance, Date systemDate, String transCodeCredit, double crdAmt, String receiptNo, double PayAmount) {
        settlementDao.updateLoanInstallment(loanId, remainingInstallmentBalance, systemDate, transCodeCredit, crdAmt, receiptNo, PayAmount);
    }

}

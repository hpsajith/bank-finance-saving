package com.ites.bankfinance.serviceImpl;

import com.ites.bankfinance.dao.UtilityDao;
import com.ites.bankfinance.model.Announcement;
import com.ites.bankfinance.model.LoanMessage;
import com.ites.bankfinance.model.UserMessage;
import com.ites.bankfinance.service.UtilityService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UtilityServiceImpl implements UtilityService {

    @Autowired
    private UtilityDao utilityDao;

    @Transactional
    @Override
    public boolean saveOrUpdateAnnouncement(Announcement announcement) {
        return utilityDao.saveOrUpdateAnnouncement(announcement);
    }

    @Transactional
    @Override
    public List<Announcement> findAnnouncements() {
        return utilityDao.findAnnouncements();
    }

    @Transactional
    @Override
    public List<Announcement> findAnnouncements(int userTypeId) {
        return utilityDao.findAnnouncements(userTypeId);
    }

    @Transactional
    @Override
    public List<Announcement> findAnnouncements(int userTypeId, Date publishDate) {
        return utilityDao.findAnnouncements(userTypeId, publishDate);
    }

    @Transactional
    @Override
    public Announcement findAnnouncement(int announcementId) {
        return utilityDao.findAnnouncement(announcementId);
    }

    @Transactional
    @Override
    public boolean deleteAnnouncement(int announcementId) {
        return utilityDao.deleteAnnouncement(announcementId);
    }

    @Transactional
    @Override
    public List<Announcement> findAnnouncements(Date publishDate) {
        return utilityDao.findAnnouncements(publishDate);
    }

    @Transactional
    @Override
    public List<Announcement> findPreviousAnnouncements(Date publishDate) {
        return utilityDao.findPreviousAnnouncements(publishDate);
    }

    @Transactional
    @Override
    public List<Announcement> findPreviousAnnouncements(int userTypeId, Date publishDate) {
        return utilityDao.findPreviousAnnouncements(userTypeId, publishDate);
    }

    @Transactional
    @Override
    public boolean saveOrUpdateUserMessage(UserMessage userMessage) {
        return utilityDao.saveOrUpdateUserMessage(userMessage);
    }

    @Transactional
    @Override
    public List<UserMessage> findUserMessages() {
        return utilityDao.findUserMessages();
    }

    @Transactional
    @Override
    public List<UserMessage> findUserMessages(int userId) {
        return utilityDao.findUserMessages(userId);
    }

    @Transactional
    @Override
    public List<UserMessage> findUserMessages(Date publishDate) {
        return utilityDao.findUserMessages(publishDate);
    }

    @Transactional
    @Override
    public List<UserMessage> findUserMessages(int userId, Date publishDate) {
        return utilityDao.findUserMessages(userId, publishDate);
    }

    @Transactional
    @Override
    public List<UserMessage> findUserMessages(Date sDate, Date eDate) {
        return utilityDao.findUserMessages(sDate, eDate);
    }

    @Transactional
    @Override
    public UserMessage findUserMessage(int userMessageId) {
        return utilityDao.findUserMessage(userMessageId);
    }

    @Transactional
    @Override
    public boolean readUserMessage(int userMessageId) {
        return utilityDao.readUserMessage(userMessageId);
    }

    @Transactional
    @Override
    public boolean deleteUserMessage(int userMessageId) {
        return utilityDao.deleteUserMessage(userMessageId);
    }

    @Transactional
    @Override
    public boolean saveOrUpdateLoanMessage(LoanMessage loanMessage) {
        return utilityDao.saveOrUpdateLoanMessage(loanMessage);
    }

    @Transactional
    @Override
    public List<LoanMessage> findLoanMessages() {
        return utilityDao.findLoanMessages();
    }

    @Transactional
    @Override
    public List<LoanMessage> findLoanMessages(int loanId) {
        return utilityDao.findLoanMessages(loanId);
    }

    @Transactional
    @Override
    public List<LoanMessage> findLoanMessages(Date publishDate) {
        return utilityDao.findLoanMessages(publishDate);
    }

    @Transactional
    @Override
    public List<LoanMessage> findLoanMessages(Date sDate, Date eDate) {
        return utilityDao.findLoanMessages(sDate, eDate);
    }

    @Transactional
    @Override
    public LoanMessage findLoanMessage(int loanMessageId) {
        return utilityDao.findLoanMessage(loanMessageId);
    }

    @Transactional
    @Override
    public boolean deleteLoanMessage(int loanMessageId) {
        return utilityDao.deleteLoanMessage(loanMessageId);
    }

}

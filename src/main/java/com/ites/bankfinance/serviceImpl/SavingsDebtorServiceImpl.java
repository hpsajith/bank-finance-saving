package com.ites.bankfinance.serviceImpl;

import com.ites.bankfinance.dao.SavingsDebtorDao;
import com.ites.bankfinance.savingsModel.DebtorHeaderDetails;
import com.ites.bankfinance.service.SavingsDebtorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by AXA-FINANCE on 5/24/2018.
 */
@Service
public class SavingsDebtorServiceImpl implements SavingsDebtorService{

    @Autowired
    SavingsDebtorDao savingsDebtorDao;

    @Transactional
    public int saveDebtorDetails(DebtorHeaderDetails loanform) {
        return savingsDebtorDao.saveDebtorDetails(loanform);
    }

    @Transactional
    public void updateDebtorDetails(DebtorHeaderDetails debHeadDetails) {
        savingsDebtorDao.updateDebtorDetails(debHeadDetails);
    }

    @Override
    public List findByNic(String nic, Object bID) {
        return savingsDebtorDao.findByNic(nic , bID);
    }

    @Override
    public List findByName(String name, Object bID) {
        return savingsDebtorDao.findByName(name , bID);
    }

    @Override
    public DebtorHeaderDetails findDebtorById(int Id){
        return savingsDebtorDao.findDebtorById(Id);
    }
}

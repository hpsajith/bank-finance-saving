/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.DebtorForm;
import com.bankfinance.form.GroupCustomerForm;
import com.ites.bankfinance.dao.DebtorDao;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.service.DebtorService;
import java.io.File;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DebtorServiceImpl implements DebtorService {

    @Autowired
    DebtorDao debtorDao;

    @Transactional
    public int saveDebtorDetails(DebtorForm loanform) {
        return debtorDao.saveDebtorDetails(loanform);
    }

//    @Transactional
//    public int saveBasicLoanDetails(LoanBasicDetails lbd) {
//        return loanDAO.saveBasicLoanDetails(lbd);
//    }
    @Transactional
    public void saveLoanEmploymentDetails(DebtorEmploymentDetails led) {
        debtorDao.saveLoanEmploymentDetails(led);
    }

    @Transactional
    public void saveLoanBusinessDetails(DebtorBusinessDetails lbd) {
        debtorDao.saveLoanBusinessDetails(lbd);
    }

    @Transactional
    public void saveLoanDependentDetails(DebtorDependentDetails ldd) {
        debtorDao.saveLoanDependentDetails(ldd);
    }

    @Transactional
    public void saveLoanLiabilityDetails(DebtorLiabilityDetails lld) {
        debtorDao.saveLoanLiabilityDetails(lld);
    }

    @Transactional
    public void saveLoanAssessBankDetails(DebtorAssessBankDetails labd) {
        debtorDao.saveLoanAssessBankDetails(labd);
    }

    @Transactional
    public void saveLoanAssessVehicleDetails(DebtorAssessVehicleDetails lavd) {
        debtorDao.saveLoanAssessVehicleDetails(lavd);
    }

    @Transactional
    public void saveLoanAssessLandDetails(DebtorAssessRealestateDetails lard) {
        debtorDao.saveLoanAssessLandDetails(lard);
    }

    @Transactional
    public DebtorForm findDebtorOtherDetails(int debtorId) {
        return debtorDao.findDebtorOtherDetails(debtorId);
    }

    @Transactional
    public DebtorDependentDetails findDependent(int depedentId) {
        return debtorDao.findDependent(depedentId);
    }

    @Transactional
    public DebtorEmploymentDetails findEmployment(int id) {
        return debtorDao.findEmployment(id);
    }

    @Transactional
    public DebtorBusinessDetails findBusiness(int id) {
        return debtorDao.findBusiness(id);
    }

    @Transactional
    public DebtorLiabilityDetails findLiability(int id) {
        return debtorDao.findLiability(id);
    }

    @Transactional
    public DebtorAssessBankDetails findAssetBank(int id) {
        return debtorDao.findAssetBank(id);
    }

    @Transactional
    public DebtorAssessVehicleDetails findAssetVehicle(int id) {
        return debtorDao.findAssetVehicle(id);
    }

    @Transactional
    public DebtorAssessRealestateDetails findAssetLand(int id) {
        return debtorDao.findAssetLand(id);
    }

    @Transactional
    public List<LoanHeaderDetails> findDebtorLoanApprovalStatus(int id) {
        return debtorDao.findDebtorLoanAppStatus(id);
    }

    @Transactional
    public void updateDebtorDetails(DebtorHeaderDetails debHeadDetails) {
        debtorDao.updateDebtorDetails(debHeadDetails);
    }

    @Transactional
    public boolean saveCustomerProfilePicture(int customerId, String filePath) {
        return debtorDao.saveCustomerProfilePicture(customerId, filePath);
    }

    @Transactional
    public File findProfilePicById(int cusId) {
        return debtorDao.findProfilePicById(cusId);
    }

    @Transactional
    public int findMaxCustomerId() {
        return debtorDao.findMaxCustomerId();
    }

    @Transactional
    public List<DebtorHeaderDetails> findDebtorBuNic(String nicNo) {
        return debtorDao.findDebtorBuNic(nicNo);
    }

    @Transactional
    public List<DebtorHeaderDetails> findDebtors(boolean isDebtor) {
        return debtorDao.findDebtors(isDebtor);
    }

    @Transactional
    public List<DebtorHeaderDetails> findDebtors(boolean isDebtor, boolean isGenerate) {
        return debtorDao.findDebtors(isDebtor, isGenerate);
    }

    @Transactional
    public DebtorHeaderDetails findDebtor(int debtorId) {
        return debtorDao.findDebtor(debtorId);
    }

    @Transactional
    public List<String> findDebtorNicNo(String pattern) {
        return debtorDao.findDebtorNicNo(pattern);
    }

    @Transactional
    public List<DebtorHeaderDetails> findDebtorByName(String name) {
        return debtorDao.findDebtorByName(name);
    }

    @Transactional
    public List<DebtorHeaderDetails> findDebtorByAccNo(String accNo) {
        return debtorDao.findDebtorByAccNo(accNo);
    }

    @Transactional
    public boolean saveGroupDebtor(GroupCustomerForm customerForm) {
        return debtorDao.saveGroupDebtor(customerForm);
    }

    @Transactional
    public DebtorHeaderDetails findDebtorByMemberNo(String memberNo, Object bID) {
       return debtorDao.findDebtorByMemberNo(memberNo, bID);
    }
}

package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.SavingNewAccForm;
import com.ites.bankfinance.dao.SavingsDao;
import com.ites.bankfinance.savingsModel.*;
import com.ites.bankfinance.service.SavingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by AXA-FINANCE on 5/21/2018.
 */
@Service
public class SavingsServiceImpl implements SavingsService {
    @Autowired
    private SavingsDao savingsDao;

    @Transactional
    public Boolean saveOrUpdateSavingsType(MSavingsType formData) {
        return savingsDao.saveOrUpdateSavingsType(formData);
    }

    @Transactional
    public Boolean saveOrUpdateSubSavingsType(MSubSavingsType formData) {
        return savingsDao.saveOrUpdateSubSavingsType(formData);
    }
    @Transactional
    public List<DebtorHeaderDetails> findByNic(String nicNo, Object bID){
        return null;
    }

    @Override
    public int saveNewSavingsData(SavingNewAccForm formData) {
        return savingsDao.saveNewSavingsData(formData);
    }

    @Transactional
    public List<DebtorHeaderDetails> findByName(String nicNo, Object bID){
        return null;
    }
    @Transactional
    public List<MSavingsType> findSavingType() {
        return savingsDao.findSavingType();
    }

    @Transactional
    public List<MSubSavingsType>  findSubSavingType() {
        return savingsDao.findSubSavingType();
    }

    @Transactional
    public List<MSubSavingsCharges>  findSavingCharges() {
        return savingsDao.findSavingCharges();
    }
}

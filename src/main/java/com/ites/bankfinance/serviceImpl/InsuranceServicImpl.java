/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.ites.bankfinance.dao.InsuraceDao;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.InsuranceCompany;
import com.ites.bankfinance.model.InsuranceOtherCharge;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.service.InsuranceService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InsuranceServicImpl implements InsuranceService {

    @Autowired
    private InsuraceDao insuraceDao;

    @Transactional
    public Boolean saveOrUpdateUserInsuraceCompany(InsuranceCompany formData) {
        return insuraceDao.saveOrUpdateUserInsuraceCompany(formData);
    }

    @Transactional
    public List<InsuranceCompany> findInsuranceCompanyes() {
        return insuraceDao.findInsuranceCompanyes();
    }

    @Transactional
    public InsuranceCompany findInsuranceCompanyID(int comId) {
        return insuraceDao.findInsuranceCompanyID(comId);
    }

    @Transactional
    public Boolean inActiveCompany(int comID) {
        return insuraceDao.inActiveCompany(comID);
    }

    @Transactional
    public Boolean activeCompany(int comID) {
        return insuraceDao.activeCompany(comID);
    }

    @Transactional
    public List findLoanHeaderDetails() {
        return insuraceDao.findLoanHeaderDetails();
    }

    @Transactional
    public LoanHeaderDetails findLoanDetails(int loanId) {
        return insuraceDao.findLoanDetails(loanId);
    }

    @Transactional
    public InsuranceOtherCharge findInsOtherCharge() {
        return insuraceDao.findInsOtherCharge();
    }

    @Transactional
    public MSubLoanType findSubLoanType(int loanId) {
        return insuraceDao.findSubLoanType(loanId);
    }

    @Transactional
    public Boolean processInsurace(InsuraceProcessMain insurance) {
        return insuraceDao.processInsurace(insurance);
    }

    @Transactional
    public List findProcessLoan() {
        return insuraceDao.findProcessLoan();
    }

    @Transactional
    public InsuraceProcessMain findInsuranceDetails(int insuId) {
        return insuraceDao.findInsuranceDetails(insuId);
    }

    @Transactional
    public List findRenewInsurance() {
        return insuraceDao.findRenewInsurance();
    }

    @Transactional
    public List<MSubLoanType> findSubLoanType() {
        return insuraceDao.findSubLoanType();
    }

    @Transactional
    public List<LoanHeaderDetails> findLoanDetails() {
        return insuraceDao.findLoanDetails();
    }

    @Transactional
    public Boolean saveOrUpdateOtherCustomers(InsuranceOtherCustomers otherCustomers) {
        return insuraceDao.saveOrUpdateOtherCustomers(otherCustomers);
    }

    @Transactional
    public List<InsuranceOtherCustomers> findOtherCustomers() {
        return insuraceDao.findOtherCustomers();
    }

    @Transactional
    public InsuranceOtherCustomers findInsuranceOtherCustomer(int Id) {
        return insuraceDao.findInsuranceOtherCustomer(Id);
    }

    @Transactional
    public List findOtherInsurance() {
        return insuraceDao.findOtherInsurance();
    }

    @Transactional
    public InsuraceProcessMain findOtherInsurance(int insuId) {
        return insuraceDao.findOtherInsurance(insuId);
    }

    @Transactional
    public List<MBranch> findBranches() {
        return insuraceDao.findBranches();
    }

    @Transactional
    public Boolean registedInsurance(int insuId) {
        return insuraceDao.registedInsurance(insuId);
    }

    @Transactional
    public boolean generateInsuranceVoucher(Integer[] loans) {
        return insuraceDao.generateInsuranceVoucher(loans);
    }

    @Transactional
    public List findRegistedLoan() {
        return insuraceDao.findRegistedLoan();
    }

    @Transactional
    public Boolean saveDiliverType(int insuId, int diliverType) {
        return insuraceDao.saveDiliverType(insuId, diliverType);
    }

    @Transactional
    public InsuraceProcessMain findDiluverInsurance(int insuId) {
        return insuraceDao.findDiluverInsurance(insuId);
    }

    @Transactional
    public List findInsuranceByAgreementNo(String agreementNo) {
        return insuraceDao.findInsuranceByAgreementNo(agreementNo);
    }

    @Transactional
    public Date findDayEndDate() {
        return insuraceDao.findDayEndDate();
    }

    @Transactional
    public InsuraceProcessMain findByLoanId(int loanId) {
        return insuraceDao.findByLoanId(loanId);
    }

    @Transactional
    public boolean addInsuraceCommission(InsuraceProcessMain insurance) {
        return insuraceDao.addInsuraceCommission(insurance);
    }

    @Transactional
    public List findInsuranceByAgreementNo(String agreementNo, int status) {
        return insuraceDao.findInsuranceByAgreementNo(agreementNo, status);
    }

    @Transactional
    public List findInsuranceByPolicyNo(String policyNo, int status) {
        return insuraceDao.findInsuranceByPolicyNo(policyNo, status);
    }

    @Transactional
    public List findInsuranceByVehicleNo(String vehicleNo, int status) {
        return insuraceDao.findInsuranceByVehicleNo(vehicleNo, status);
    }

    @Transactional
    public List findInsuranceByDebtorName(String debtorName, int status) {
        return insuraceDao.findInsuranceByDebtorName(debtorName, status);
    }

    @Transactional
    public Boolean saveInsuranceDebitProcess(InsuraceProcessMain insurance) {
        return insuraceDao.saveInsuranceDebitProcess(insurance);
    }

    @Transactional
    public Boolean saveOrUpdateInsuranceProcessDelete(InsuraceProcessMain insuraceProcessMain) {
        return insuraceDao.saveOrUpdateInsuranceProcessDelete(insuraceProcessMain);
    }

    @Transactional
    public Boolean deleteInsuranceProcessMainByInsuId(int insuId, String insuCode, int loanId) {
        return insuraceDao.deleteInsuranceProcessMainByInsuId(insuId, insuCode, loanId);
    }

}

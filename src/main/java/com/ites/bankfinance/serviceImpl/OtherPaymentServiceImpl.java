/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.ites.bankfinance.dao.OtherPaymentDao;
import com.ites.bankfinance.dao.SettlementDao;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.InsuranceOtherCustomers;
import com.ites.bankfinance.model.SettlementPaymentOther;
import com.ites.bankfinance.model.SettlementVoucher;
import com.ites.bankfinance.model.SettlementVoucherCategory;
import com.ites.bankfinance.service.OtherPaymentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Amidu-pc
 */
@Service
public class OtherPaymentServiceImpl implements OtherPaymentService {

    @Autowired
    OtherPaymentDao otherPaymentDao;

    @Transactional
    public List<Chartofaccount> findChtAccountsList() {
        return otherPaymentDao.findChtAccountsList();
    }
    @Transactional
    public String addSettlmentPaymentOther(SettlementPaymentOther spo) {
        return otherPaymentDao.addSettlmentPaymentOther(spo);
    }
    @Transactional
    public String getOtherTransactionIdMax(String code) {
        return otherPaymentDao.getOtherTransactionIdMax(code);
    }
    @Transactional
    public int saveIssuePayment(SettlementVoucher vouch) {
        return otherPaymentDao.saveIssuePayment(vouch);
    }
    @Transactional
    public List<SettlementVoucherCategory> getVoucherCategorys() {
        return otherPaymentDao.getVoucherCategorys();
    }
    @Transactional
    public List<Chartofaccount> findChtAccountListByName(String accountName) {
        return otherPaymentDao.findChtAccountListByName(accountName);
    }
    @Transactional
    public List<InsuranceOtherCustomers> findChtAccountInsuranceOtherCustomer(String accName) {
       return otherPaymentDao.findChtAccountInsuranceOtherCustomer(accName);
    }

    @Transactional
    public InsuranceOtherCustomers findChtAccountDetails(String accName) {
        return otherPaymentDao.findChtAccountDetails(accName);
    }
    
}

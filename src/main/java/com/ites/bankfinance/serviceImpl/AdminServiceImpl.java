/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.AddJobsForm;
import com.bankfinance.form.RecOffColDateForm;
import com.ites.bankfinance.dao.AdminDao;
import com.ites.bankfinance.model.*;
import com.ites.bankfinance.service.AdminService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao adminDao;

    @Transactional
    public void saveAddLoanType(MLoanType mlt) {
        adminDao.saveLoanType(mlt);
    }

    @Transactional
    public MLoanType findLoanType(int id) {
        return adminDao.findLoanType(id);
    }

    @Transactional
    public List<MLoanType> findLoanType() {
        return adminDao.loadLoanType();
    }

    @Transactional
    public void saveSubLoanType(MSubLoanType subLoanType, int[] documentCheckListId, int[] documentIsCompulseryId, int[] otherChargesId, int[] chartOfAccounts, String[] chartOfAccountName) {
        adminDao.saveSubLoantype(subLoanType, documentCheckListId, documentIsCompulseryId, otherChargesId, chartOfAccounts, chartOfAccountName);
    }

    @Transactional
    public List<MSubLoanType> findSubLoanType() {
        return adminDao.loadSubLoanType();
    }

    @Transactional
    public boolean saveUser(UmUser user) {
        return adminDao.saveUser(user);
    }

    @Transactional
    public List findUser() {
        return adminDao.loadUsers();
    }

    @Transactional
    public List<MCheckList> findCheckList() {
        return adminDao.loadCheckList();
    }

    @Transactional
    public MLoanType findMLoanType(int loanTypeId) {
        return adminDao.findLoanType(loanTypeId);
    }

    @Transactional
    public List<MSubLoanChecklist> findCheckList(int subLoanId) {
        return adminDao.findCheckList(subLoanId);
    }

    @Transactional
    public List<UmUserType> findUserType() {
        return adminDao.loadUserType();
    }

    @Transactional
    public List<MApproveLevels> findAppLevel() {
        return adminDao.loadAppLevel();
    }

    @Transactional
    public List<UmUserTypeApproval> findUserApproveLevels() {
        return adminDao.findAppLevels();
    }

    @Transactional
    public void saveOrUpdateAppLevels(UmUserTypeApproval formData, int[] appLevels) {
        adminDao.saveOrUpdateAppLevels(formData, appLevels);
    }

    @Transactional
    public List<MSubTaxCharges> findOtherCharges() {
        return adminDao.findOtherCharges();

    }

    @Transactional
    public MSubLoanType findMSubLoanType(int subLoanId) {
        return adminDao.findMSubLoanType(subLoanId);
    }

    @Transactional
    public List<MSubLoanOtherChargers> findOtherCharges(int subLoanId) {
        return adminDao.findOtherChargeList(subLoanId);
    }

    @Transactional
    public Integer saveNewEmployee(Employee formData) {
        return adminDao.saveOrUpdateEmployee(formData);
    }

    @Transactional
    public List<Employee> findEmployeeList() {
        return adminDao.findEmployeeList();
    }

    @Transactional
    public Employee findEmployee(int employeeId) {
        return adminDao.findEmployee(employeeId);
    }

    @Transactional
    public List findUserName(String userName) {
        return adminDao.findUserName(userName);
    }

    @Transactional
    public List<MBranch> findBranch() {
        return adminDao.findBranch();
    }

    @Transactional
    public MWeightunits findWeightType() {
        return adminDao.findWeightType();
    }


    @Transactional
    public List<MDepartment> findDepartment(int branchId) {
        return adminDao.finfDepartment(branchId);

    }

    @Transactional
    public List<UmUser> findAllUser() {
        return adminDao.findAllUser();
    }

    @Transactional
    public List<UmUser> findUserWithOutSuperAdmin() {
        return adminDao.findUserWithOutSuperAdmin();
    }

    @Transactional
    public List<MDepartment> findDepartmentList() {
        return adminDao.findDepartmentList();
    }

    @Transactional
    public List<UmUserTypeApproval> findApproLevels(int userId) {
        return adminDao.findApproLevels(userId);
    }

    @Transactional
    public void saveOrUpdateUserSubLoan(UmUserTypeLoan formData, int[] subLoanIdList) {
        adminDao.saveOrUpdateUserSubLoan(formData, subLoanIdList);
    }

    @Transactional
    public List<UmUserTypeLoan> findUserTypeSubLoans() {
        return adminDao.findUserTypeSubLoans();

    }

    @Transactional
    public void saveDocumentList(MCheckList formData) {
        adminDao.saveOrUpdateDocumetList(formData);
    }

    @Transactional
    public void saveOtherCharges(MSubTaxCharges formData) throws Exception {
        adminDao.saveUpdateOtherCharges(formData);
    }

    @Transactional
    public MSubTaxCharges findOtherChargesList(int otherChargeId) throws Exception {
        return adminDao.findOtherChargesList(otherChargeId);

    }

    @Transactional
    public UmUser findEmpUser(int employeeId) {
        return adminDao.findEmpUser(employeeId);
    }

    @Transactional
    public int saveUserType(UmUserType userType) {
        return adminDao.saveUserType(userType);
    }

    @Transactional
    public List<UmUserType> findUserTypeList() {
        return adminDao.viewUserType();
    }

    @Transactional
    public UmUserType findEditUserType(int userTypeId) {
        return adminDao.findEditUserType(userTypeId);
    }

    @Transactional
    public List<AddJobsForm> findJobList() {
        return adminDao.findJobList();
    }

    @Transactional
    public boolean saveAddPages(AddJobsForm jobPages, String[] tabId, String[] mainId) {
        return adminDao.saveAddPages(jobPages, tabId, mainId);
    }

    @Transactional
    public int findNumOfBranch(int userTypeId) {
        return adminDao.findNumOfBranch(userTypeId);
    }

    @Transactional
    public boolean saveUserTypeBranch(UmUserBranch userBranch, int[] userTypeBranch) {
        return adminDao.saveUserTypeBranch(userBranch, userTypeBranch);
    }

    @Transactional
    public UmUser findUserForEmpID(int empID) {
        return adminDao.findUserForEmpID(empID);
    }

    @Transactional
    public List<MDepartment> findDepartment() {
        return adminDao.findDepartment();
    }

    @Transactional
    public boolean saveUserDepartment(int branchId, int empID, int[] userDepartment) {
        return adminDao.saveUserDepartment(branchId, empID, userDepartment);
    }

    @Transactional
    public List<UmUserBranch> findUserBranch(int empID) {
        return adminDao.findUserBranch(empID);
    }

    @Transactional
    public List<UmUser> findUmUser() {
        return adminDao.finfUmUser();
    }

    @Transactional
    public List<UmUser> findRManagers() {
        return adminDao.findRManagers();
    }

    @Transactional
    public List findManagerBranches(int managerUserId) {
        return adminDao.findManagerBranches(managerUserId);
    }

    @Transactional
    public List<UmUser> findOfficer() {
        return adminDao.findOfficer();
    }

    @Transactional
    public boolean saveFieldOfficer(UmFieldOfficers formData, int[] officerID) {
        return adminDao.saveFieldOfficer(formData, officerID);
    }

    @Transactional
    public List<UmFieldOfficers> findRecoveryManageres() {
        return adminDao.findRecoveryManageres();
    }

    @Transactional
    public Integer saveNewBranch(MBranch formData) {
        return adminDao.saveNewBranch(formData);
    }

    @Transactional
    public MBranch findEditBranch(int branchId) {
        return adminDao.editBranch(branchId);

    }

    @Transactional
    public Boolean inActiveBranch(int branchID) {
        return adminDao.inActiveBranch(branchID);
    }

    @Transactional
    public Boolean activeBranch(int branchID) {
        return adminDao.activeBranch(branchID);
    }

    @Transactional
    public MBranch findActiveBranch(int branchId) {
        return adminDao.findActiveBranch(branchId);
    }

    @Transactional
    public boolean saveDepartment(MDepartment formData) {
        return adminDao.saveDepartment(formData);
    }

    @Transactional
    public Boolean inActiveEmployee(int empNo) {
        return adminDao.inActiveEmployee(empNo);
    }

    @Transactional
    public Boolean activeEmployee(int empNo) {
        return adminDao.activeEmployee(empNo);
    }

    @Transactional
    public UmUser findUserName(int userId) {
        return adminDao.findUserName(userId);
    }

    @Transactional
    public List<UmUserBranch> findUserBranches(Integer userId) {
        return adminDao.findUserBranches(userId);
    }

    @Transactional
    public String findBranchName(int branchId) {
        return adminDao.findBranchName(branchId);
    }

    @Transactional
    public List<AddJobsForm> findDefineJobs(int userTypeId) {
        return adminDao.findDefineJobs(userTypeId);
    }

    @Transactional
    public List<AMaintab> findMainTabs() {
        return adminDao.findMainTabs();
    }

    @Transactional
    public List<ASubtab> findSubTabs() {
        return adminDao.findSubTabs();
    }

    @Transactional
    public List<ConfigChartofaccountLoan> findConfigChartOfAccounts(int subLoanId) {
        return adminDao.findConfigChartOfAccounts(subLoanId);
    }

    @Transactional
    public Boolean checkSubLoanType(String subLoanType) {
        return adminDao.checkSubLoanType(subLoanType);
    }

    @Override
    @Transactional
    public Integer saveNewSupplier(MSupplier mSupplier) {
        return adminDao.saveNewSupplier(mSupplier);
    }

    @Override
    @Transactional
    public MSupplier findSupplier(int supplierId) {
        return adminDao.findSupplier(supplierId);
    }

    @Override
    @Transactional
    public List<MSupplier> loadSuppliers() {
        return adminDao.loadSuppliers();
    }

    @Transactional
    public RecOffCollDates hasCollectionDates(int recOffId, int branchId) {
        return adminDao.hasCollectionDates(recOffId, branchId);
    }

    @Transactional
    public List<RecOffCollDates> getCollectionDates(int branchId) {
        return adminDao.getCollectionDates(branchId);
    }

    @Transactional
    public Integer saveOrUpdateCollectionDates(List<RecOffColDateForm> list, int branchId) {
        return adminDao.saveOrUpdateCollectionDates(list, branchId);
    }

    @Transactional
    public MCheckList findDocument(int docId) {
        return adminDao.findDocument(docId);
    }

    @Transactional
    public List<UmUserTypeLoan> findUserTypeLoanList(int userId) {
        return adminDao.findUserTypeLoanList(userId);
    }

    @Override
    @Transactional
    public boolean saveChartOfAccounts(List<Chartofaccount> cs) {
        return adminDao.saveChartOfAccounts(cs);
    }

    @Transactional
    public List<Chartofaccount> getChartOfAccounts() {
        return adminDao.getChartOfAccounts();
    }

    @Transactional
    public List<MSeizerDetails> findSeizerDetailsList() {
        return adminDao.findSeizerDetailsList();
    }

    @Transactional
    public Boolean inActiveSeizer(int seizerId) {
        return adminDao.inActiveSeizer(seizerId);
    }

    @Transactional
    public Boolean activeSeizer(int seizerId) {
        return adminDao.activeSeizer(seizerId);
    }

    @Transactional
    public Boolean saveOrUpdateSeizerDetails(MSeizerDetails seizerDetails) {
        return adminDao.saveOrUpdateSeizerDetails(seizerDetails);
    }

    @Transactional
    public MSeizerDetails findSeizerDetailsList(int seizerID) {
        return adminDao.findSeizerDetailsList(seizerID);
    }

    @Transactional
    public boolean addOrRemoveModules(String[] modules, int status) {
        return adminDao.addOrRemoveModules(modules, status);
    }

    @Transactional
    public boolean changeLoanType(int loanTypeId, int status) {
        return adminDao.changeLoanType(loanTypeId, status);
    }

    @Transactional
    public boolean changeSubLoanType(int subLoanTypeId, int status) {
        return adminDao.changeSubLoanType(subLoanTypeId, status);
    }

    @Transactional
    public String findEmoloyeeLastName(Integer marketingOfficer) {
        return adminDao.findEmoloyeeLastName(marketingOfficer);

    }

    @Transactional
    public MSystemValidation findSystemValidation() {
        return adminDao.findSystemValidation();
    }

    @Transactional
    public int maxBranchCount() {
        return adminDao.maxBranchCount();
    }

    @Transactional
    public Boolean saveOrUpdateYardDetails(MYardDetails yardDetails) {
        return adminDao.saveOrUpdateYardDetails(yardDetails);
    }

    @Transactional
    public List<MYardDetails> findYardDetailsList() {
        return adminDao.findYardDetailsList();
    }

    @Transactional
    public MYardDetails findYardDetailsList(int yardID) {
        return adminDao.findYardDetailsList(yardID);
    }

    @Transactional
    public Boolean inActiveYard(int yardId) {
        return adminDao.inActiveYard(yardId);
    }

    @Transactional
    public Boolean activeYard(int yardId) {
        return adminDao.activeYard(yardId);
    }

    @Transactional
    public Boolean saveOrUpdateVehicleDetails(VehicleType vehicleType) {
        return adminDao.saveOrUpdateVehicleDetails(vehicleType);
    }

    @Transactional
    public List<VehicleType> findVehicleTypesList() {
        return adminDao.findVehicleTypesList();
    }

    @Transactional
    public VehicleType findVehicleTypeList(int vehicleTypeId) {
        return adminDao.findVehicleTypeList(vehicleTypeId);
    }

    @Transactional
    public Boolean saveOrUpdateVehicleMakeDetails(VehicleMake vehicleMake) {
        return adminDao.saveOrUpdateVehicleMakeDetails(vehicleMake);
    }

    @Transactional
    public List<VehicleMake> findVehicleMakeList(int vehicleType) {
        return adminDao.findVehicleMakeList(vehicleType);
    }

    @Transactional
    public List<VehicleModel> findVehicleModelList(int vehicleTypeId) {
        return adminDao.findVehicleModelList(vehicleTypeId);
    }

    @Transactional
    public Boolean saveOrUpdateVehicleModelDetail(VehicleModel vehicleModel) {
        return adminDao.saveOrUpdateVehicleModelDetail(vehicleModel);
    }

    @Transactional
    public Boolean inActiveVehicle(int vehicleId) {
        return adminDao.inActiveVehicle(vehicleId);
    }

    @Transactional
    public Boolean activeVehicle(int vehiledId) {
        return adminDao.activeVehicle(vehiledId);
    }

    @Transactional
    public List<MCenter> findCenter() {
        return adminDao.findCenter();
    }

    @Transactional
    public List<MCenter> findCenter(int branchID) {
        return adminDao.findCenter(branchID);
    }

    @Transactional
    public Integer saveNewCenter(MCenter formData) {
        return adminDao.saveNewCenter(formData);
    }

    public Integer saveOrUpdateGoldValue(MWeightunits formData){return adminDao.saveOrUpdateGoldValue(formData);}

    @Transactional
    public MCenter editCenter(int centerId) {
        return adminDao.editCenter(centerId);
    }

    @Transactional
    public boolean inActiveCenter(int centerId) {
        return adminDao.inActiveCenter(centerId);
    }

    @Transactional
    public Boolean activeCenter(int centerID) {
        return adminDao.activeCenter(centerID);
    }

    @Transactional
    public List<MGroup> findGroup() {
        return adminDao.findGroup();
    }

    @Transactional
    public Integer saveNewGroup(MGroup formData) {
        return adminDao.saveNewGroup(formData);
    }

    @Transactional
    public MGroup editGroup(int groupID) {
        return adminDao.editGroup(groupID);
    }

    @Transactional
    public boolean inActiveGroup(int groupID) {
        return adminDao.inActiveGroup(groupID);
    }

    @Transactional
    public Boolean activeGroup(int groupID) {
        return adminDao.activeGroup(groupID);
    }

    @Transactional
    public List<MGroup> findGroup(int centerID) {
        return adminDao.findGroup(centerID);
    }

    @Transactional
    public List<DebtorHeaderDetails> findGroupUsers(int groupID) {
        return adminDao.findGroupUsers(groupID);
    }

    //savings
    @Override
    public boolean changeAccountType(int accountTypeId, int status) {
        adminDao.changeAccountType(accountTypeId, status);
        return false;
    }

    @Override
    public boolean changeSubAccountType(int subAccountTypeId, int status) {
        adminDao.changeSubAccountType(subAccountTypeId, status);
        return false;
    }

}

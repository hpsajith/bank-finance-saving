/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.ArriesLoan;
import com.bankfinance.form.DirectDebitCharge;
import com.bankfinance.form.Mail;
import com.bankfinance.form.RecoveryHistory;
import com.bankfinance.form.ReturnCheckPaymentList;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.dao.RecoveryDao;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.RecoveryLetterDetail;
import com.ites.bankfinance.model.RecoveryLetters;
import com.ites.bankfinance.model.RecoveryShift;
import com.ites.bankfinance.model.RecoveryVisitsDetails;
import com.ites.bankfinance.model.SeizeOrder;
import com.ites.bankfinance.model.SeizeOrderApproval;
import com.ites.bankfinance.model.SeizeYardRegister;
import com.ites.bankfinance.model.SeizeYardRegisterCheklist;
import com.ites.bankfinance.service.RecoveryService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MsD
 */
@Service
public class RecoveryServeiceImpl implements RecoveryService {

    @Autowired
    RecoveryDao recoveryDao;

    @Transactional
    public List<viewLoan> getLoanListForDue(String name, String agreemntNo, String nic, String due, int branch) {
        return recoveryDao.getLoanListForDue(name, agreemntNo, nic, due, branch);
    }

    @Transactional
    public boolean addRecoveryShiftDate(RecoveryShift formData) {
        boolean status = recoveryDao.addRecoveryShiftDate(formData);
        return status;
    }

    @Transactional
    public double getInstalmentBalance(int loanId) {
        double balance = recoveryDao.getInstalmentBalance(loanId);
        return balance;
    }

    @Transactional
    public List<viewLoan> getArrearsLoanListForDue(String name, String agreemntNo, String nic, String due, int branch) {
        return recoveryDao.getArrearsLoanListForDue(name, agreemntNo, nic, due, branch);
    }

    @Transactional
    public RecoveryShift getRecoveryShift(Integer instmntId) {
        return recoveryDao.getRecoveryShift(instmntId);
    }

    @Transactional
    public List getRecoveryShiftDates(int LoanId) {
        return recoveryDao.getRecoveryShiftDates(LoanId);
    }

    @Transactional
    public List<ReturnCheckPaymentList> getCheckPayments(String fromDate, String toDate) {
        return recoveryDao.getCheckPayments(fromDate, toDate);
    }

    @Transactional
    public boolean updateChequePayment(int checkDetaiId, String remark, int userId) {
        return recoveryDao.updateChequePayment(checkDetaiId, remark, userId);
    }

    @Transactional
    public double getOpenArrears(int loanId) {
        return recoveryDao.getOpenArrears(loanId);
    }

    @Transactional
    public List loadLetterPostingLoans() {
        return recoveryDao.loadLetterPostingLoans();
    }

    @Transactional
    public DebtorHeaderDetails findDebtorByLoanId(int loanId) {
        return recoveryDao.findDebtorByLoanId(loanId);
    }

    @Transactional
    public boolean saveLetter(RecoveryLetterDetail letter) {
        return recoveryDao.saveLetter(letter);
    }

    @Transactional
    public List<RecoveryHistory> findRecoveryLetterDetails(int loanId) {
        return recoveryDao.findRecoveryLetterDetails(loanId);
    }

    @Transactional
    public String findInvoiceDetails(int loanId) {
        return recoveryDao.findInvoiceDetails(loanId);
    }

    @Transactional
    public Date findPreviousDate(int letterId, int loanId) {
        return recoveryDao.findPreviousDate(letterId, loanId);
    }

    @Transactional
    public int findLetterTypeByLoanType(int loanId) {
        return recoveryDao.findLetterTypeByLoanType(loanId);
    }

    @Transactional
    public int saveOrUpdateRecoveryOfficer(int officerId, int loanId, String dueDate, int visitType, String comment) {
        return recoveryDao.saveOrUpdateRecoveryOfficer(officerId, loanId, dueDate, visitType, comment);
    }

    @Transactional
    public List searchRecoveryOfficer() {
        return recoveryDao.searchRecoveryOfficer();
    }

    @Transactional
    public List<RecoveryLetters> findAllLetters() {
        return recoveryDao.findAllLetters();
    }

    @Transactional
    public List findVisitDetailsByuser(String dueDate) {
        return recoveryDao.findVisitDetailsByuser(dueDate);
    }

    @Transactional
    public Object[] findRemainigInstallment(int loanId) {
        return recoveryDao.findRemainigInstallment(loanId);
    }

    @Transactional
    public List<viewLoan> findSearchLoanList(String agNo, String cusName, String nicNo, String dueDate, String lapsDate, String vehicleNo) {
        return recoveryDao.findSearchLoanList(agNo, cusName, nicNo, dueDate, lapsDate, vehicleNo);
    }

    @Transactional
    public List loanTransactionDetails(int loanId) {
        return recoveryDao.loanTransactionDetails(loanId);
    }

    @Transactional
    public boolean saveOtherRecoveryCharge(int loanId, int chargeId, int supplierId, double loanAmount, String dueDate) {
        return recoveryDao.saveOtherRecoveryCharge(loanId, chargeId, supplierId, loanAmount, dueDate);
    }

    @Transactional
    public RecoveryVisitsDetails findRecoveryVisitDetail(int recId, int loanId) {
        return recoveryDao.findRecoveryVisitDetail(recId, loanId);
    }

    @Transactional
    public boolean saveRecoveryManagerComment(int recId, String comment) {
        return recoveryDao.saveRecoveryManagerComment(recId, comment);
    }

    @Transactional
    public RecoveryVisitsDetails findRecoveryVisitDetail(int loanId) {
        return recoveryDao.findRecoveryVisitDetail(loanId);
    }

    @Transactional
    public int saveOrUpdateRecoveryVisitDetails(RecoveryVisitsDetails recoveryVisitsDetails) {
        return recoveryDao.saveOrUpdateRecoveryVisitDetails(recoveryVisitsDetails);
    }

    @Transactional
    public List<RecoveryVisitsDetails> getBySubmitDate(Date sDate, Date eDate) {
        return recoveryDao.getBySubmitDate(sDate, eDate);
    }

    @Transactional
    public List<ArriesLoan> getLoanArries(String date1, String date2) {
        return recoveryDao.getLoanArries(date1, date2);
    }

    @Transactional
    public List<SeizeOrder> getSeizeOrders() {
        return recoveryDao.getSeizeOrders();
    }

    @Transactional
    public boolean saveOrUpdateSeizeOrder(SeizeOrder seizeOrder) {
        return recoveryDao.saveOrUpdateSeizeOrder(seizeOrder);
    }

    @Transactional
    public boolean activeSeizeOrder(int orderId, int loanId) {
        return recoveryDao.activeSeizeOrder(orderId, loanId);
    }

    @Transactional
    public boolean inActiveSeizeOrder(int orderId, int loanId) {
        return recoveryDao.inActiveSeizeOrder(orderId, loanId);
    }

    @Transactional
    public List<SeizeOrder> loadSeizeApproval() {
        return recoveryDao.loadSeizeApproval();
    }

    @Transactional
    public boolean saveOrUpdateSeizeOrderApproval(SeizeOrderApproval orderApproval) {
        return recoveryDao.saveOrUpdateSeizeOrderApproval(orderApproval);
    }

    @Transactional
    public SeizeOrderApproval findSeizeOrderIssue(int seizeOrdeId, int type) {
        return recoveryDao.findSeizeOrderApproval(seizeOrdeId, type);
    }

    @Transactional
    public boolean keyIssue(int seizeOrderId) {
        return recoveryDao.keyIssue(seizeOrderId);
    }

    @Transactional
    public List<SeizeYardRegister> getYardRegisterList() {
        return recoveryDao.getYardRegisterList();
    }

    @Transactional
    public String saveOrUpdateYardRegister(SeizeYardRegister yardRegister) {
        return recoveryDao.saveOrUpdateYardRegister(yardRegister);
    }

    @Transactional
    public SeizeYardRegister findYardRegister(int seizeOrdeId) {
        return recoveryDao.findYardRegister(seizeOrdeId);
    }

    @Transactional
    public boolean saveOrUpdateYardRegisterCheckList(SeizeYardRegisterCheklist yardRegisterCheklist) {
        return recoveryDao.saveOrUpdateYardRegisterCheckList(yardRegisterCheklist);
    }

    @Transactional
    public List<SeizeYardRegisterCheklist> getYardRegisterCheklists(int seizeOrdeId) {
        return recoveryDao.getYardRegisterCheklists(seizeOrdeId);
    }

    @Transactional
    public SeizeYardRegisterCheklist findYardRegisterCheklist(int docId, int seizeOrdeId) {
        return recoveryDao.findYardRegisterCheklist(docId, seizeOrdeId);
    }

    @Transactional
    public List<SeizeOrder> loadSeizeOrdersByAgreementNo(String agreementNo) {
        return recoveryDao.loadSeizeOrdersByAgreementNo(agreementNo);
    }

    @Transactional
    public SeizeYardRegisterCheklist getYardRegisterInspectionReport(int seizeOrdeId) {
        return recoveryDao.getYardRegisterInspectionReport(seizeOrdeId);
    }

    @Transactional
    public List<SeizeOrder> getRenewalSeizeOrders() {
        return recoveryDao.getRenewalSeizeOrders();
    }

    @Transactional
    public List<SeizeOrder> getSeizeOrderHistory(int loanId) {
        return recoveryDao.getSeizeOrderHistory(loanId);
    }

    @Transactional
    public boolean deleteYardRegister(int yardRegId) {
        return recoveryDao.deleteYardRegister(yardRegId);
    }

    @Transactional
    public boolean reverseYardRegister(int seizeOrderId) {
        return recoveryDao.reverseYardRegister(seizeOrderId);
    }

    @Transactional
    public List<LoanPropertyVehicleDetails> findByLoan(String agreementNo) {
        return recoveryDao.findByLoan(agreementNo);
    }

    @Transactional
    public List<LoanPropertyVehicleDetails> findByVehicleNo(String vehicleNo) {
        return recoveryDao.findByVehicleNo(vehicleNo);
    }

    @Transactional
    public List<LoanPropertyVehicleDetails> findByVehicleEngineNo(String engineNo) {
        return recoveryDao.findByVehicleEngineNo(engineNo);
    }

    @Transactional
    public List<LoanPropertyVehicleDetails> findByVehicleChassisNo(String chassisNo) {
        return recoveryDao.findByVehicleChassisNo(chassisNo);
    }

    @Transactional
    public List loadLetterPostingByLoan(String agreementNo) {
        return recoveryDao.loadLetterPostingByLoan(agreementNo);
    }

    @Transactional
    public List loadLetterPostingByVehicle(String vehicleNo) {
        return recoveryDao.loadLetterPostingByVehicle(vehicleNo);
    }

    @Transactional
    public List<DirectDebitCharge> findDirectDebitCharges(int loanId) {
        return recoveryDao.findDirectDebitCharges(loanId);
    }

    @Transactional
    public boolean deleteDirectDebitCharge(int chargeId, int settlementId) {
        return recoveryDao.deleteDirectDebitCharge(chargeId, settlementId);
    }

    @Transactional
    public List<Mail> findMails(int letterTypeId) {
        return recoveryDao.findMails(letterTypeId);
    }

    @Transactional
    public RecoveryLetters findLetter(int letterId) {
        return recoveryDao.findLetter(letterId);
    }

    @Transactional
    public List<RecoveryLetters> findLetters(boolean flag) {
        return recoveryDao.findLetters(flag);
    }

    @Transactional
    public List<viewLoan> findSearchLoanListforReport(String memNo) {
        return recoveryDao.findSearchLoanListforReport(memNo); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    

}

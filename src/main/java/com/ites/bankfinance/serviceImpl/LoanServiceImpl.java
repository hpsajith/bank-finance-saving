/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.ApprovalDetailsModel;
import com.bankfinance.form.CustomerInfo;
import com.bankfinance.form.LoanForm;
import com.bankfinance.form.PaymentDetails;
import com.bankfinance.form.PaymentVoucher;
import com.bankfinance.form.RecoveryReportModel;
import com.bankfinance.form.viewLoan;
import com.ites.bankfinance.dao.LoanDao;
import com.ites.bankfinance.model.DebtorHeaderDetails;
import com.ites.bankfinance.model.InsuraceProcessMain;
import com.ites.bankfinance.model.LoanApprovedLevels;
import com.ites.bankfinance.model.LoanCapitalizeDetail;
import com.ites.bankfinance.model.LoanGuaranteeDetails;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.LoanInstallment;
import com.ites.bankfinance.model.LoanInstalmentSchedule;
import com.ites.bankfinance.model.LoanPendingDates;
import com.ites.bankfinance.model.LoanPropertyArticlesDetails;
import com.ites.bankfinance.model.LoanPropertyLandDetails;
import com.ites.bankfinance.model.LoanPropertyOtherDetails;
import com.ites.bankfinance.model.LoanPropertyVehicleDetails;
import com.ites.bankfinance.model.LoanRecoveryReport;
import com.ites.bankfinance.model.MCaptilizeType;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.SettlementChequeDetails;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.service.LoanService;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
    private LoanDao loanDAO;

    @Transactional
    public int saveNewLoanData(LoanForm loanForm) {
        return loanDAO.saveNewLoanData(loanForm);
    }

    @Transactional
    public int saveLoanApproval(LoanApprovedLevels loanApprove) {
        return loanDAO.saveLoanApproval(loanApprove);
    }

    @Transactional
    public int saveLoanCheckList(int listId, int loanId, String path, String description) {
        return loanDAO.saveLoanCheckList(listId, loanId, path, description);
    }
//    @Transactional
//    public List<LoanBasicDetails> findLoanDetails() {
//        return loanDAO.findLoanDetails();
//    }

    @Transactional
    public Map<String, List> findByLoanId(int loanId) {
        return loanDAO.findByLoanId(loanId);
    }

    @Transactional
    public List findByNic(String nic, Object bID) {
        return loanDAO.findByNic(nic, bID);
    }

    @Transactional
    public Boolean findByNicNo(String nic, Object bID) {
        return loanDAO.findByNicNo(nic, bID);
    }

    @Transactional
    public List findByName(String name, Object bID) {
        return loanDAO.findByName(name, bID);
    }

    @Transactional
    public List findByLoanNo(String loanNo, Object bID) {
        return loanDAO.findByLoanNo(loanNo, bID);
    }

    @Transactional
    public List findByVehicleNo(String vehicleNo) {
        return loanDAO.findByVehicleNo(vehicleNo);
    }

    @Transactional
    public DebtorHeaderDetails findById(int id) {
        return loanDAO.findById(id);
    }

    @Transactional
    public List findLoansByDebtorId(int debtorId) {
        return loanDAO.findLoansByDebtorId(debtorId);
    }

    @Transactional
    public List findGuarantorByDebtorId(int debtorId) {
        return loanDAO.findGuarantorByDebtorId(debtorId);
    }

    @Transactional
    public List findLoanByUserType(int issuType, int branch, Date startDate, Date endDate) {
        return loanDAO.findLoanByUserType(issuType, branch, startDate, endDate);
    }

    @Transactional
    public viewLoan findLoanByLoanId(int loanId) {
        return loanDAO.findLoanByLoanId(loanId);
    }

    @Transactional
    public int saveLoanPropertyVehicleDetails(LoanPropertyVehicleDetails formData) {
        return loanDAO.saveLoanPropertyVehicleDetails(formData);
    }

    @Transactional
    public int saveLoanPropertyArticleDetails(LoanPropertyArticlesDetails formData) {
        return loanDAO.saveLoanPropertyArticleDetails(formData);
    }

    @Transactional
    public int saveLoanPropertyLandDetails(LoanPropertyLandDetails formData) {
        return loanDAO.saveLoanPropertyLandDetails(formData);
    }

    @Transactional
    public int saveLoanPropertyOtherDetails(LoanPropertyOtherDetails formData) {
        return loanDAO.saveLoanPropertyOtherDetails(formData);
    }

    @Transactional
    public viewLoan findLoanStatus(int loanId) {
        return loanDAO.findLoanStatus(loanId);
    }

    @Transactional
    public String createLoanAgreementNo(int loanId) {
        return loanDAO.createLoanAgreementNo(loanId);
    }

    @Transactional
    public List createInstallmentList(LoanHeaderDetails loan) {
        return loanDAO.createInstallmentList(loan);
    }

    @Transactional
    public PaymentDetails calculatePayments(LoanHeaderDetails loan) {
        return loanDAO.calculatePayments(loan);
    }

    @Transactional
    public LoanHeaderDetails searchLoanByLoanId(int loanId) {
        return loanDAO.searchLoanByLoanId(loanId);
    }

    @Transactional
    public List findOtherChargesByLoanId(int loanId) {
        return loanDAO.findOtherChargesByLoanId(loanId);
    }

    @Transactional
    public List findGuarantorByLoanId(int loanId) {
        return loanDAO.findGuarantorByLoanId(loanId);
    }

    @Transactional
    public LoanHeaderDetails findLoanHeaderByLoanId(int loanId) {
        return loanDAO.findLoanHeaderByLoanId(loanId);
    }

    @Transactional
    public List findOtherChargesByLoand(int loanId) {
        return loanDAO.findOtherChargesByLoand(loanId);
    }

    @Transactional
    public int saveChekDetails(SettlementChequeDetails formData) {
        return loanDAO.saveChekDetails(formData);
    }

    @Transactional
    public int saveVoucher(int paymentType, String amount, int loanId) {
        return loanDAO.saveVoucher(paymentType, amount, loanId);
    }

    @Transactional
    public LoanPropertyVehicleDetails findVehicleById(int id) {
        return loanDAO.findVehicleById(id);
    }

    @Transactional
    public LoanPropertyArticlesDetails findArticleById(int id) {
        return loanDAO.findArticleById(id);
    }

    @Transactional
    public LoanPropertyLandDetails findLandById(int id) {
        return loanDAO.findLandById(id);
    }

    @Transactional
    public LoanPropertyOtherDetails findOtherById(int id) {
        return loanDAO.findOtherById(id);
    }

    @Transactional
    public List findLoanCheckListByLoanId(int loanId) {
        return loanDAO.findLoanCheckListByLoanId(loanId);
    }

    @Transactional
    public File findDocumentById(int list_id) {
        return loanDAO.findDocumentById(list_id);
    }

    @Transactional
    public String findDocumentNameById(int list_id) {
        return loanDAO.findDocumentNameById(list_id);
    }

    @Transactional
    public void updateDocumentSubmissionStaus(int loanId) {
        loanDAO.updateDocumentSubmissionStaus(loanId);
    }

    @Transactional
    public int saveRecoveryOfficerComment(LoanApprovedLevels app) {
        return loanDAO.saveRecoveryOfficerComment(app);
    }

    @Transactional
    public List findApproveLevelByUser() {
        return loanDAO.findApproveLevelByUser();
    }

    @Transactional
    public LoanApprovedLevels findloanApproval(int loanId, int type) {
        return loanDAO.findloanApproval(loanId, type);
    }

    @Transactional
    public LoanPendingDates findPendingDates(int loanId) {
        return loanDAO.findPendingDates(loanId);
    }

    @Transactional
    public int saveOrUpdatePendingDates(LoanPendingDates formData, int typeId) {
        return loanDAO.saveOrUpdatePendingDates(formData, typeId);
    }

    @Transactional
    public LoanRecoveryReport findRecoveryReportById(int loanId) {
        return loanDAO.findRecoveryReportById(loanId);
    }

    @Transactional
    public ApprovalDetailsModel findApprovalDetails(int loanId) {
        return loanDAO.findApprovalDetails(loanId);
    }

    @Transactional
    public int saveOrUpdateFieldOfficer(int loanId, int officerId) {
        return loanDAO.saveOrUpdateFieldOfficer(loanId, officerId);
    }

    @Transactional
    public List findFiledOfficers(int loanId) {
        return loanDAO.findFiledOfficers(loanId);
    }

    @Transactional
    public List<RecoveryReportModel> findrecoveryReport(int loanId) {
        return loanDAO.findrecoveryReport(loanId);
    }

    @Transactional
    public List findLoanListByFiledOfficer() {
        return loanDAO.findLoanListByFiledOfficer();
    }

    @Transactional
    public int saveRecoveryReport(int loanId, String[] comment) {
        return loanDAO.saveRecoveryReport(loanId, comment);
    }

    @Transactional
    public List findInvoiceDetailsByLoanId(int loanId) {
        return loanDAO.findInvoiceDetailsByLoanId(loanId);
    }

    @Transactional
    public void setLoanIsIssueStatus(int loanId) {
        loanDAO.setLoanIsIssueStatus(loanId);
    }

    @Transactional
    public LoanHeaderDetails findLoanBalance(int loanId) {
        return loanDAO.findLoanBalance(loanId);
    }

    @Transactional
    public List findArticleDetailsById(int loanId) {
        return loanDAO.findArticleDetailsById(loanId);
    }

    @Transactional
    public int findLoanTypeBySubType(int loanType) {
        return loanDAO.findLoanTypeBySubType(loanType);
    }

    @Transactional
    public void saveLoanInstallment(int loanId) {
        loanDAO.saveLoanInstallment(loanId);
    }

    @Transactional
    public String findLoanPeriodType(Integer loanPeriodTypeId) {
        return loanDAO.findLoanPeriodType(loanPeriodTypeId);
    }

    @Transactional
    public int[] createNewVoucher(PaymentVoucher formData) {
        return loanDAO.createNewVoucher(formData);
    }

    @Transactional
    public boolean processForPayment(int loanId) {
        return loanDAO.processForPayment(loanId);
    }

    @Transactional
    public String updateLoanPaymentStatus(int loanId) {
        return loanDAO.updateLoanPaymentStatus(loanId);
    }

    @Transactional
    public double findOtherChargeSum(int loanId) {
        return loanDAO.findOtherChargeSum(loanId);
    }

    @Transactional
    public int saveAdvancePayment(SettlementPaymentDetailCheque formData) {
        return loanDAO.saveAdvancePayment(formData);
    }

    @Transactional
    public List<MSubLoanType> findMSubLoanList() {
        return loanDAO.findMsubLoans();
    }

    @Transactional
    public int findLoanIdByAgreementNo(String agNo) {
        return loanDAO.findLoanIdByAgreementNo(agNo);
    }

    @Transactional
    public List removeLoans(int branchID) {
        return loanDAO.removeLoans(branchID);
    }

@Transactional
    public List deletedLoans(int branchID) {
        return loanDAO.deletedLoans(branchID);
    }

    @Transactional
    public int removeLoan(int loanId) {
        return loanDAO.removeLoan(loanId);
    }

    @Transactional
    public List findVouchersByDate(String vDate) {
        return loanDAO.findVouchersByDate(vDate);
    }

    @Transactional
    public List findPaymentByVoucher(int vId) {
        return loanDAO.findPaymentByVoucher(vId);
    }

    @Transactional
    public List findInstallmentByLoanNo(String loanNo) {
        return loanDAO.findInstallmentByLoanNo(loanNo);
    }

    @Transactional
    public boolean updateRental(LoanInstallment instlment) {
        return loanDAO.updateRental(instlment);
    }

    @Transactional
    public String reverseProcessLoan(int loanId, String comment) {
        return loanDAO.reverseProcessLoan(loanId, comment);
    }

    @Transactional
    public String reverseApprovedLoan(int loanId, String comment) {
        return loanDAO.reverseApprovedLoan(loanId, comment);
    }

    @Transactional
    public InsuraceProcessMain findInsuranceByLoanId(int loanId) {
        return loanDAO.findInsuranceByLoanId(loanId);
    }

    @Transactional
    public boolean removeVoucher(int vId) {
        return loanDAO.removeVoucher(vId);
    }

    @Transactional
    public boolean updateCheqDetails(SettlementPaymentDetailCheque cheq) {
        return loanDAO.updateCheqDetails(cheq);
    }

    @Transactional
    public boolean deletePayment(int transId, int type) {
        return loanDAO.deletePayment(transId, type);
    }

    @Transactional
    public Double[] findLoanInitialCharges(int loanId) {
        return loanDAO.findLoanInitialCharges(loanId);
    }

    @Transactional
    public int updateDueDate(String due, int loanId) {
        return loanDAO.updateDueDate(due, loanId);
    }

    @Transactional
    public String findCheqRecievedName(int chekId, int cheqTo) {
        return loanDAO.findCheqRecievedName(chekId, cheqTo);
    }

    @Transactional
    public DebtorHeaderDetails findDebtorHeader(int debtorId) {
        return loanDAO.findDebtorHeader(debtorId);
    }

    @Transactional
    public Boolean findInsuranceStatus(int loanId) {
        return loanDAO.findInsuranceStatus(loanId);
    }

    @Transactional
    public boolean deleteLoanPropertyArticleDetails(int articleId) {
        return loanDAO.deleteLoanPropertyArticleDetails(articleId);
    }

    @Transactional
    public Double findLossAmount(int loanId) {
        return loanDAO.findLossAmount(loanId);
    }

    @Transactional
    public List<LoanInstalmentSchedule> findInstalmentSchedule(int loanId) {
        return loanDAO.findInstalmentSchedule(loanId);
    }

    @Transactional
    public String findTotalLoanAmountWithInterest(int loanId) {
        return loanDAO.findTotalLoanAmountWithInterest(loanId);
    }

    @Transactional
    public Boolean createSchedule(LoanInstalmentSchedule instalmentSchedule, int[] insMonths, Double[] insAmounts) {
        return loanDAO.createSchedule(instalmentSchedule, insMonths, insAmounts);
    }

    @Transactional
    public LoanHeaderDetails findLoanHeaderDetails(int loanId) {
        return loanDAO.findLoanHeaderDetails(loanId);
    }

    @Transactional
    public List<LoanInstallment> findScheduleList(int loanId) {
        return loanDAO.findScheduleList(loanId);
    }

    @Transactional
    public List<MCaptilizeType> findCaptilizeTypes() {
        return loanDAO.findCaptilizeTypes();
    }

    @Transactional
    public MCaptilizeType findCaptilizeType(int capTypId) {
        return loanDAO.findCaptilizeType(capTypId);
    }

    @Transactional
    public boolean saveOrUpdateLoanCapitalizeDetail(LoanCapitalizeDetail loanCapitalizeDetail) {
        return loanDAO.saveOrUpdateLoanCapitalizeDetail(loanCapitalizeDetail);
    }

    @Transactional
    public boolean saveOrUpdateLoanCapitalizeDetail(List<LoanCapitalizeDetail> loanCapitalizeDetails) {
        return loanDAO.saveOrUpdateLoanCapitalizeDetail(loanCapitalizeDetails);
    }

    @Transactional
    public List<LoanCapitalizeDetail> findLoanCapitalizeDetails(Integer loanId) {
        return loanDAO.findLoanCapitalizeDetails(loanId);
    }

    @Transactional
    public LoanPropertyVehicleDetails findVehicleByLoanId(int loanId) {
        return loanDAO.findVehicleByLoanId(loanId);
    }

    @Transactional
    public int findLoanInstallmentCount(int loanId) {
        return loanDAO.findLoanInstallmentCount(loanId);
    }

    @Transactional
    public List<LoanGuaranteeDetails> findLoanGuranteeDetails(int loanId) {
        return loanDAO.findLoanGuranteeDetails(loanId);
    }

    @Transactional
    @Override
    public List<LoanHeaderDetails> findLoanHeaderDetailseByAggNo(String aggNo) {
        return loanDAO.findLoanHeaderDetailseByAggNo(aggNo);
    }

    @Transactional
    public Boolean clearInstalmentSchedule(int loanId) {
        return loanDAO.clearInstalmentSchedule(loanId);
    }

    @Transactional
    public double getTotalInterestAmount(Integer loanId) {
        return loanDAO.getTotalInterestAmount(loanId);
    }

    @Transactional
    public DebtorHeaderDetails findDebtorHeader(String debtorAccount) {
        return loanDAO.findDebtorHeader(debtorAccount);
    }

    @Transactional
    public List<LoanPropertyVehicleDetails> findVehicleByRegNo(String vehicleRegNo) {
        return loanDAO.findVehicleByRegNo(vehicleRegNo);
    }

    @Transactional
    public List<CustomerInfo> findLoanByIsIssuAndIsDelete() {
        return loanDAO.findLoanByIsIssuAndIsDelete();
    }

    @Transactional
    public List<LoanHeaderDetails> findLoanDetailsAndVehicleDetailsByAggNo(String aggNo) {
        return loanDAO.findLoanDetailsAndVehicleDetailsByAggNo(aggNo);
    }

    @Transactional
    public List<LoanHeaderDetails> findLoanDetailsAndVehicleDetailsByVehicleNo(String vehicleNo) {
        return loanDAO.findLoanDetailsAndVehicleDetailsByVehicleNo(vehicleNo);
    }

    @Transactional
    public boolean saveOrUpdateTerminateAgreementByLoanId(int loanId) {
        return loanDAO.saveOrUpdateTerminateAgreementByLoanId(loanId);
    }

    @Transactional
    public int saveNewLoanDataGroup(LoanForm loanData) {
        return loanDAO.saveNewLoanDataGroup(loanData);
    }

    @Transactional
    public List<DebtorHeaderDetails> findByMemNo(String memNo, Object bID) {
        return loanDAO.findByMemNo(memNo, bID);
    }

    @Transactional
    public List<LoanHeaderDetails> findActiveLoansByDebtorIDAndLoanTypeId(int debtorID, int loanTypeId) {
        return loanDAO.findActiveLoansByDebtorIDAndLoanTypeId(debtorID, loanTypeId);
    }

    @Transactional
    public String createMyNewVoucher(int loanId, double totalAmount, String chekNo, int bankAccount, String chkDate, double payingAmount, String TxtChqAmt, int chbIsACPay) {
        return loanDAO.createMyNewVoucher(loanId, totalAmount, chekNo, bankAccount, chkDate, payingAmount, TxtChqAmt, chbIsACPay);
    }

    @Transactional
    public int findVoucherIdByVoucherNo(String voucherNo) {
        return loanDAO.findVoucherIdByVoucherNo(voucherNo);
    }

    @Transactional(rollbackFor = Exception.class)
    public SettlementPaymentDetailCheque findSettlementPaymentDetailChequeByVoucherNo(String voucherNo, double amount, int loanId) {
        return loanDAO.findSettlementPaymentDetailChequeByVoucherNo(voucherNo, amount, loanId);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updatePaymentAmount(String voucherNo, double amt) {
       loanDAO.updatePaymentAmount(voucherNo, amt);
    }

    
}

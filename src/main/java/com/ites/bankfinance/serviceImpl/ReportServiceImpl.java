/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.ActiveContracts;
import com.bankfinance.form.PortfolioDetail;
import com.bankfinance.form.RmvReceivable;
import com.bankfinance.form.ServiceChargeReceivable;
import com.ites.bankfinance.dao.ReportDao;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.LoanHeaderDetails;
import com.ites.bankfinance.model.SettlementCharges;
import com.ites.bankfinance.service.ReportService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    ReportDao reportDao;

    @Transactional
    public LoanHeaderDetails findByLoanDetails(String agreeNo) {
        return reportDao.findByLoanDetails(agreeNo);
    }

    @Transactional
    public List<SettlementCharges> findBySettlementChargees(int loanID) {
        return reportDao.findBySettlementChargees(loanID);
    }

    @Transactional
    public Double findLoanBalanace(int loanID) {
        return reportDao.findLoanBalance(loanID);
    }

    @Transactional
    public String findByInsuCompanyName(Integer insuComId) {
        return reportDao.findByInsuCompanyName(insuComId);
    }

    @Transactional
    public Double findTotalPayments(int loanID) {
        return reportDao.findTotalPayments(loanID);
    }

    @Transactional
    public List<Chartofaccount> findChartofaccountList() {
        return reportDao.findChartofaccountList();
    }

    @Transactional
    public PortfolioDetail findPortfolioDetail(String agerementNo, String user_Name, String branch_Name) {
        return reportDao.findPortfolioDetail(agerementNo, user_Name, branch_Name);
    }

    @Transactional
    public double getNoOfRentalInArrears(int loanId) {
        return reportDao.getNoOfRentalInArrears(loanId);
    }

    @Transactional
    public Double[] getArrearsAmounts(int loanId) {
        return reportDao.getArrearsAmounts(loanId);
    }

    @Transactional
    public Double getOdiArrearsAmount(int loanId) {
        return reportDao.getOdiArrearsAmount(loanId);
    }

    @Transactional
    public Double getOtherChargersArrearsAmount(int loanId) {
        return reportDao.getOtherChargersArrearsAmount(loanId);
    }

    @Transactional
    public Double[] getLoanFutureDetails(int loanId) {
        return reportDao.getLoanFutureDetails(loanId);
    }

    @Transactional
    public PortfolioDetail findPortfolioDetail(int branchId) {
        return reportDao.findPortfolioDetail(branchId);
    }

    @Transactional
    public PortfolioDetail findPortfolioDetailByOfficerId(int officerId) {
        return reportDao.findPortfolioDetailByOfficerId(officerId);
    }

    @Transactional
    public PortfolioDetail findPortfolioDetailByDueDay(int dueDay) {
        return reportDao.findPortfolioDetailByDueDay(dueDay);
    }

    @Transactional
    public PortfolioDetail findPortfolioDetailByInitiateDate(String initiateFromDate, String initiateToDate) {
        return reportDao.findPortfolioDetailByInitiateDate(initiateFromDate, initiateToDate);
    }

    @Transactional
    public PortfolioDetail findPortfolioDetailByLoanAmount(String loanAmount, String lagValue) {
        return reportDao.findPortfolioDetailByLoanAmount(loanAmount, lagValue);
    }

    @Transactional
    public PortfolioDetail findPortfolioDetailByGrossRental(String grossRental, String grValue) {
        return reportDao.findPortfolioDetailByGrossRental(grossRental, grValue);
    }

    @Transactional
    public List<ActiveContracts> getActiveContract(String date1, String date2) {
        return reportDao.getActiveContract(date1, date2);
    }

    @Transactional
    public List<ActiveContracts> getNotActiveContract(String date1, String date2) {
        return reportDao.getNotActiveContract(date1, date2);
    }

    @Transactional
    public List<RmvReceivable> getRmvReceivableRecords(String date1, String date2) {
        return reportDao.getRmvReceivableRecords(date1, date2);
    }

    @Transactional
    public List<ServiceChargeReceivable> getServiceChargeReceivable(String date1, String date2) {
        return reportDao.getServiceChargeReceivable(date1, date2);
    }

    @Transactional
    public PortfolioDetail findPortfolioDetailByLoanType(int loanType) {
        return reportDao.findPortfolioDetailByLoanType(loanType);
    }
    
    

}

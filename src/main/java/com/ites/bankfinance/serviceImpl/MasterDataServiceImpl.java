/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.ites.bankfinance.dao.MasterDataDao;
import com.ites.bankfinance.model.Chartofaccount;
import com.ites.bankfinance.model.MBranch;
import com.ites.bankfinance.model.MPeriodTypes;
import com.ites.bankfinance.model.MRateType;
import com.ites.bankfinance.model.MSection;
import com.ites.bankfinance.model.MSubLoanType;
import com.ites.bankfinance.model.MSupplier;
import com.ites.bankfinance.model.UmUser;
import com.ites.bankfinance.model.VehicleMake;
import com.ites.bankfinance.model.VehicleModel;
import com.ites.bankfinance.model.VehicleType;
import com.ites.bankfinance.savingsModel.MSavingsType;
import com.ites.bankfinance.savingsModel.MSubSavingsType;
import com.ites.bankfinance.service.MasterDataService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MasterDataServiceImpl implements MasterDataService {

    @Autowired
    MasterDataDao masterData;

    @Transactional
    public List findLoanTypes() {
        return masterData.findLoanTypes();
    }

    @Transactional
    public List findSubLoanTypes(int loanType) {
        return masterData.findSubLoanTypes(loanType);
    }

    @Transactional
    public List findOtherCharges() {
        return masterData.findOthercharges();
    }

    @Transactional
    public List findUsersByUserType(int userType) {
        return masterData.findUsersByUserType(userType);
    }

    @Transactional
    public List findUsersByApproval(int appId, int subLoanId) {
        return masterData.findUsersByApproval(appId, subLoanId);
    }

    @Transactional
    public List<MSupplier> findSuppliers() {
        return masterData.findSuppliers();
    }

    @Transactional
    public List findCheckList(int loanType) {
        return masterData.findCheckList(loanType);
    }

    @Transactional
    public String findCurrencyType() {
        return masterData.findCurrencyType();
    }

    @Transactional
    public double findCurrencyPrice() {
        return masterData.findCurrencyPrice();
    }

    @Transactional
    public String findPrinterName() {
        return masterData.findPrinterName();
    }

    @Transactional
    public int findLoanTypeByLoanId(int loanId) {
        return masterData.findLoanTypeByLoanId(loanId);
    }

    @Transactional
    public List findPeriodTypes() {
        return masterData.findPeriodTypes();
    }

    @Transactional
    public List findApprovalTypes() {
        return masterData.findApprovalTypes();
    }

    @Transactional
    public List findExtraChargesBySubLoanId(int subLoanId) {
        return masterData.findExtraChargesBySubLoanId(subLoanId);
    }

    @Transactional
    public List findBankAccountsDetails() {
        return masterData.findBankAccountsDetails();
    }

    @Transactional
    public List findBankAccountsDetails2() {
        return masterData.findBankAccountsDetails2();
    }

    @Transactional
    public List findUserJobs() {
        return masterData.findUserJobs();
    }

    @Transactional
    public List<MSubLoanType> findPawningType() {
        return masterData.findPawningType();
    }

    @Transactional
    public List findArticleTypes() {
        return masterData.findArticleTypes();
    }

    @Transactional
    public List findBranchesByUsername(String username, String password) {
        return masterData.findBranchesByUsername(username, password);
    }

    @Transactional
    public void setUserLogin(int branchId) {
        masterData.setUserLogin(branchId);
    }

    @Transactional
    public void inactiveUser() {
        masterData.inactiveUser();
    }

    @Transactional
    public boolean checkUserIsBlock() {
        return masterData.checkUserIsBlock();
    }

    @Transactional
    public List findUsersByUserTypeAndBranch(int userType, int branchId) {
        return masterData.findUsersByUserTypeAndBranch(userType, branchId);
    }

    @Transactional
    public Double findLoanRate(int loanType) {
        return masterData.findLoanRate(loanType);
    }

    @Transactional
    public List<MBranch> findBranches() {
        return masterData.findBranches();
    }

    @Transactional
    public List<MRateType> findRateType() {
        return masterData.findRateType();
    }

    @Transactional
    public List<VehicleType> findVehicleType() {
        return masterData.findVehicleType();
    }

    @Transactional
    public List<VehicleMake> findVehicleMake(int vehicleType) {
        return masterData.findVehicleMake(vehicleType);
    }

    @Transactional
    public List<VehicleModel> findVehicleModel(int vehicleType, int vehicleMake) {
        return masterData.findVehicleModel(vehicleType, vehicleMake);
    }

    @Transactional
    public List<VehicleMake> findVehicleMake() {
        return masterData.findVehicleMake();
    }

    @Transactional
    public List<VehicleModel> findVehicleModel() {
        return masterData.findVehicleModel();
    }

    @Transactional
    public VehicleMake findVehicleMakeById(int vehicleMakeId) {
        return masterData.findVehicleMakeById(vehicleMakeId);
    }

    @Transactional
    public VehicleModel findVehicleModelById(int vehicleModelId) {
        return masterData.findVehicleModelById(vehicleModelId);
    }

    @Transactional
    public VehicleType findVehicleTypeById(int vehicleTypeId) {
        return masterData.findVehicleTypeById(vehicleTypeId);
    }

    @Transactional
    public MPeriodTypes findPeriodType(int periodTypeId) {
        return masterData.findPeriodType(periodTypeId);
    }

    @Transactional
    public MBranch findBranch(int branchId) {
        return masterData.findBranch(branchId);
    }

    @Transactional
    public List<MSection> findSections() {
        return masterData.findSections();
    }

    @Transactional
    public UmUser authenticate(String userName, String password) {
        return masterData.authenticate(userName, password);
    }

    @Transactional
    public List<MBranch> loadUserBranches(int userId, int sectionId) {
        return masterData.loadUserBranches(userId, sectionId);
    }

    @Transactional
    public Chartofaccount findAccount(String accountNo) {
        return masterData.findAccount(accountNo);
    }

    @Transactional
    public List<MSavingsType> findAllSavingsTypes(){return masterData.findAllSavingsTypes();}

    @Transactional
    public MSavingsType findAllSavingsTypesById(int savingsId){return masterData.findAllSavingsTypesById(savingsId);}

    @Transactional
    public List<MSubSavingsType> findAllSubSavingsTypes(){ return masterData.findAllSubSavingsTypes();}

    @Transactional
    public MSubSavingsType findAllSubSavingsTypesById(int subSavingsId){ return masterData.findAllSubSavingsTypesById(subSavingsId);}

    @Override
    public List findSubSavingsTypes(int savingsType) {
        return masterData.findSubSavingsTypes(savingsType);
    }

}

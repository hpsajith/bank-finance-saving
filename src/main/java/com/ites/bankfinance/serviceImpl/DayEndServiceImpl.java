/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.ites.bankfinance.dao.DayEndDao;
import com.ites.bankfinance.model.DayEndDate;
import com.ites.bankfinance.service.DayEndService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Uththara
 */
@Service
public class DayEndServiceImpl implements DayEndService {

    @Autowired
    DayEndDao dayDao;

    @Transactional
    public String dayEndProcess(int branchId) {
        return dayDao.dayEndProcess(branchId);
    }

    @Transactional
    public List findConfigJobs() {
        return dayDao.findConfigJobs();
    }

    @Transactional
    public String getLastDayEnd(int branchId) {
        return dayDao.getLastDayEnd(branchId);
    }

    @Transactional
    public String correctPosting(String sDate, String eDate) {
        return dayDao.correctPosting(sDate, eDate);
    }

    @Transactional
    public String findLoanOverPayment(int branchId) {
        return dayDao.findLoanOverPayment(branchId);
    }

    @Transactional
    public boolean postingCheque(String code, int id) {
        return dayDao.postingCheque(code, id);
    }

    @Transactional
    public boolean calLapsDate() {
        return dayDao.calLapsDate();
    }

    @Transactional
    public boolean calNullInstallment() {
        return dayDao.calNullInstallment();
    }

    @Transactional
    public Double findBalanceWithOverPay(int printLoanId) {
        return dayDao.findBalanceWithOverPay(printLoanId);
    }

    @Transactional
    public boolean calculateBalances() {
        return dayDao.calculateBalances();
    }

    @Transactional
    public List<DayEndDate> dayEndProcessBranches() {
       return dayDao.dayEndProcessBranches();
    }
    
    


}

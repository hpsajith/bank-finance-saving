/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.OpenLoanForm;
import com.ites.bankfinance.dao.OpenLoanDao;
import com.ites.bankfinance.service.OpenLoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ites
 */
@Service
public class OpenLoanServiceImpl implements OpenLoanService{

    @Autowired
    private OpenLoanDao openDao;
    
    @Transactional
    public void addOpenLoan(OpenLoanForm formData) {
        openDao.addOpenLoan(formData);
    }
    
    
}

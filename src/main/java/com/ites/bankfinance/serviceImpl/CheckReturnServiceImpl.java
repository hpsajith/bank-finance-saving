/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.ReturnCheckPaymentList;
import com.ites.bankfinance.dao.CheckReturnDao;
import com.ites.bankfinance.model.MSubTaxCharges;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import com.ites.bankfinance.service.CheckReturnService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ITESS
 */
@Service
public class CheckReturnServiceImpl implements CheckReturnService {

    @Autowired
    CheckReturnDao checkReturnDao;

    @Transactional
    public List<ReturnCheckPaymentList> getCheckPayments(String fromDate, String toDate) {
        return checkReturnDao.getCheckPayments(fromDate, toDate);
    }

    @Transactional
    public boolean updateChequePayment(int checkDetaiId, String remark, int userId, int stut, int bankId) {
        return checkReturnDao.updateChequePayment(checkDetaiId, remark, userId, stut, bankId);
    }

    @Transactional
    public boolean addDebitEntriesforReturnChecks(int checkDetaiId, double chargeAmount) {
        return checkReturnDao.addDebitEntriesforReturnChecks(checkDetaiId, chargeAmount);
    }

    @Transactional
    public SettlementPaymentDetailCheque getChequePaymentDetail(int checkDetaiId) {
        SettlementPaymentDetailCheque chqD = checkReturnDao.getChequePaymentDetail(checkDetaiId);
        return chqD;
    }

    @Transactional
    public boolean addCheckReturnOtherCharge(int checkDetaiId, double chargeAmount, Integer chargeTypeId) {
        return checkReturnDao.addCheckReturnOtherCharge(checkDetaiId, chargeAmount, chargeTypeId);
    }

    @Transactional
    public List<MSubTaxCharges> getChargeTypes() {
        List<MSubTaxCharges> list = checkReturnDao.getChargeTypes();
        return list;
    }

//    @Transactional
//    public boolean addCrediEntriesforReturnChecks(int checkDetaiId, double chargeAmount, int bankId){
//        return checkReturnDao.addCrediEntriesforReturnChecks(checkDetaiId,chargeAmount,bankId);
//    }
}

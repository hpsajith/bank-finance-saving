/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.serviceImpl;

import com.bankfinance.form.ChartDataSet;
import com.bankfinance.form.DayPlanTargetModel;
import com.bankfinance.form.RecOffCollection;
import com.ites.bankfinance.dao.DayPlanDao;
import com.ites.bankfinance.model.DayPlanAssigntoRecoOfficer;
import com.ites.bankfinance.model.DayPlanAssingtoRecoManager;
import com.ites.bankfinance.model.DayPlanMonthSchedule;
import com.ites.bankfinance.model.DayPlanMonthTargetAssingCed;
import com.ites.bankfinance.model.MLoanType;
import com.ites.bankfinance.service.DayPlanService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DayPlanServiceImpl implements DayPlanService {

    @Autowired
    DayPlanDao dayPlanDao;

    @Transactional
    public DayPlanTargetModel calculateTarget(String m, int loanType, int branchId) {
        return dayPlanDao.calculateTarget(m, loanType, branchId);
    }

    @Transactional
    public int saveOrUpdateMonthTarget(DayPlanMonthTargetAssingCed dpm) {
        return dayPlanDao.saveOrUpdateMonthTarget(dpm);
    }

    @Transactional
    public void saveOrUpdateAssignTarget(DayPlanAssingtoRecoManager dpa) {
        dayPlanDao.saveOrUpdateAssignTarget(dpa);
    }

    @Transactional
    public DayPlanMonthTargetAssingCed isExistDayPlan(int loanType, Date date, int branch) {
        return dayPlanDao.isExistDayPlan(loanType, date, branch);
    }

    @Transactional
    public DayPlanAssingtoRecoManager isExistAssign(int targetId) {
        return dayPlanDao.isExistAssign(targetId);
    }

    @Transactional
    public double getTargetForRecMan(int recManId, int branchId, Date tgtMon, int loanType) {
        return dayPlanDao.getTargetForRecMan(recManId, branchId, tgtMon, loanType);
    }

    @Transactional
    public void saveOrUpdateAssignTargetToRecOff(DayPlanAssigntoRecoOfficer dparo) {
        dayPlanDao.saveOrUpdateAssignTargetToRecOff(dparo);
    }

    @Transactional
    public List<DayPlanAssigntoRecoOfficer> getAssignedList(int recManId, int branchId, Date tgtMon, int loanType) {
        return dayPlanDao.getAssignedList(recManId, branchId, tgtMon, loanType);
    }

    @Transactional
    public DayPlanAssigntoRecoOfficer isExistAssignRecOff(int recOffId, int branchId, Date tgtMon, int loanType) {
        return dayPlanDao.isExistAssignRecOff(recOffId, branchId, tgtMon, loanType);
    }

    @Transactional
    public List<MLoanType> getAssignedLoanTypes(int recManId, int branchId) {
        return dayPlanDao.getAssignedLoanTypes(recManId, branchId);
    }

    @Transactional
    public List<MLoanType> getAssignedLoanTypesToRecOff(int recOffId, int branchId, Date tgtDate) {
        return dayPlanDao.getAssignedLoanTypesToRecOff(recOffId, branchId, tgtDate);
    }
    
    @Transactional
    public List<MLoanType> getAssignedLoanTypesToRecOff(int recOffId, int branchId) {
        return dayPlanDao.getAssignedLoanTypesToRecOff(recOffId, branchId);
    }    

    @Transactional
    public void saveOrUpdateDayPlanMonthSchedule(DayPlanMonthSchedule dpms) {
        dayPlanDao.saveOrUpdateDayPlanMonthSchedule(dpms);
    }

    @Transactional
    public DayPlanMonthSchedule isExistMonthSchedule(int recOffId, int loanType, int branchId, Date tgtDate) {
        return dayPlanDao.isExistMonthSchedule(recOffId, loanType, branchId, tgtDate);
    }

    @Transactional
    public double getTargetForRecOff(int recOffId, int branchId, Date sDate, Date eDate, int loanType) {
        return dayPlanDao.getTargetForRecOff(recOffId, branchId, sDate, eDate, loanType);
    }

    @Transactional
    public List<DayPlanMonthSchedule> getMonthSchedule(int recOffId, int branchId, Date sDate, Date eDate, int loanType) {
        return dayPlanDao.getMonthSchedule(recOffId, branchId, sDate, eDate, loanType);
    }

    @Transactional
    public void activeDayPlan(int id) {
        dayPlanDao.activeDayPlan(id);
    }

    @Transactional
    public void inActiveDayPlan(int id, String reason) {
        dayPlanDao.inActiveDayPlan(id, reason);
    }

    @Transactional
    public List<DayPlanMonthSchedule> getCollection(int loanType, int recOffId, int branchId, Date sDate, Date eDate) {
        return dayPlanDao.getCollection(loanType, recOffId, branchId, sDate, eDate);
    }

    @Transactional
    public List<ChartDataSet> getRecManChartDataSet(int loanType, int recManId, int branchId, int type) {
        return dayPlanDao.getRecManChartDataSet(loanType, recManId, branchId, type);
    }

    @Transactional
    public List<ChartDataSet> getRecOffChartDataSet(int loanType, int recOffId, int branchId , int type) {
        return dayPlanDao.getRecOffChartDataSet(loanType, recOffId, branchId, type);
    }

    @Override
    @Transactional
    public List<RecOffCollection> getRecoveryOfficersWithCollection(int loanType, int branchId, String month) {
        return dayPlanDao.getRecoveryOfficersWithCollection(loanType, branchId, month);
    }

    @Transactional
    public List<DayPlanMonthTargetAssingCed> getTargetAssingCeds(Date tgtDate, int branchId) {
        return dayPlanDao.getTargetAssingCeds(tgtDate, branchId);
    }

    @Transactional
    public List getRecoveryManagers(Date tgtDate, int branchId, int loanType) {
        return dayPlanDao.getRecoveryOfficers(tgtDate, branchId, loanType);
    }

    @Transactional
    public double getRecoveryOfficerCollection(int loanType, int branchId, Date sDate, Date eDate) {
        return dayPlanDao.getRecoveryOfficerCollection(loanType, branchId, sDate, eDate);
    }

    @Transactional
    public double[] getBalances(int loanType, int recOffId, int branchId, Date sDate, Date eDate) {
        return dayPlanDao.getBalances(loanType, recOffId, branchId, sDate, eDate);
    }


}

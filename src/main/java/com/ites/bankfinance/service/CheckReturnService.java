/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.service;

import com.bankfinance.form.ReturnCheckPaymentList;
import com.ites.bankfinance.model.MSubTaxCharges;
import com.ites.bankfinance.model.SettlementPaymentDetailCheque;
import java.util.List;

/**
 *
 * @author ITESS
 */
public interface CheckReturnService {
    
    public List<ReturnCheckPaymentList> getCheckPayments(String fromDate, String toDate);
               
    public boolean updateChequePayment(int checkDetaiId, String remark,int userId,int stus, int bankId);

    public boolean addDebitEntriesforReturnChecks(int checkDetaiId,double chargeAmount);

    public SettlementPaymentDetailCheque getChequePaymentDetail(int checkDetaiId);

    public boolean addCheckReturnOtherCharge(int checkDetaiId, double chargeAmount,Integer chargeTypeId);

    public List<MSubTaxCharges> getChargeTypes();

//    public boolean addCrediEntriesforReturnChecks(int checkDetaiId, double chargeAmount, int bankId);
}

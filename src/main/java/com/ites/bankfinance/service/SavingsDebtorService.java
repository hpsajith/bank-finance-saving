package com.ites.bankfinance.service;

import com.ites.bankfinance.savingsModel.DebtorHeaderDetails;

import java.util.List;

/**
 * Created by AXA-FINANCE on 5/24/2018.
 */
public interface SavingsDebtorService {
    public int saveDebtorDetails(DebtorHeaderDetails loanform);

    public DebtorHeaderDetails findDebtorById(int Id);

    public List findByNic(String nic, Object bID);

    public List findByName(String name, Object bID);

    public void updateDebtorDetails(DebtorHeaderDetails debHeadDetails);
}

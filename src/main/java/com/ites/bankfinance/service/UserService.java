/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ites.bankfinance.service;

import com.ites.bankfinance.model.Employee;
import com.ites.bankfinance.model.MCompany;

public interface UserService {

    public int findByUserName();
    
    public int findUserTypeByUserId(int userId);
    
    public Employee findEmployeeById(int empId);

    public String findApprovalsByPassword(String password);

    public boolean findLoggedUserApproval();

    public String findWorkingUserName();

    public String findWorkingBranchName(int branchID);

    public MCompany findCompanyDetails();
    
    public int findEmployeeByUserId(int userId);

    public int findUserWorkingBranch(int userId, int branchId);

    public int findUserType(int userId);
}

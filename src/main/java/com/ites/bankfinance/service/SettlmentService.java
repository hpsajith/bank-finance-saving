/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.service;

import com.bankfinance.form.LoanPosting;
import com.bankfinance.form.OtherChargseModel;
import com.bankfinance.form.RebateForm;
import com.bankfinance.form.RebateQuotation;
import com.bankfinance.form.StlmntReturnList;
import com.ites.bankfinance.model.*;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ITESS
 */
public interface SettlmentService {

    public boolean addSettlmentPayment(SettlementPayments formDat);

//    public int addCashPaymentDetails(double amount,int maxTransactionId);
    public String getTransactionIdMax(String code);

//    public int addChequePaymentDetails(SettlementPaymentDetailCheque formData);    
    public double getSumOfOtherCherges(List otherCharges);

    public List findPayTypes();

    public List findBankList();

    public List getPaidLoanInstallmnetList(int loanId);

    public boolean addSettlmentPaymentDetails(SettlementAddNewPayment formData, int maxTransactionId, int userId);

    public DebtorHeaderDetails findDebtorByLoanId(int loanId);

    public boolean addCashPaymentDetails(int userId, double d, String newTransactionId);

    public String getAccountNo(int i);

    public MBankDetails getBankDetails(int i);

    public boolean addChequePaymentDetails(SettlementPaymentDetailCheque settlementPaymentDetailCheque);

    public SettlementPaymentDetailCheque getSettlementPaymentDetailChequeDetails(String voucherNo);

    public SettlementVoucher getSettlementVoucherDetailsbyLoanId(int loanId);

    public boolean addBankDepositePaymentDetails(SettlementPaymentDetailBankDeposite settlementPaymentDetailBankDeposite);

    public String getReceiptNo(String refNo);

    public boolean addSettlementReceiptDetail(SettlementReceiptDetail settlementReceiptDetail);

    public int getCashPaymentDetailsId();

    public int getChequePaymentDetailsId();

    public int getBankDepositePaymentDetailsId();

    public String getMaxSerialNo();

    public List<SettlementRefCodeConfig> getRefCode();

    public boolean addSettlementMain(SettlementMain settlementMain);

    public boolean addSettlementMain2(SettlementMain settlementMain, String account);

    public List<ReturnList> findOpenningBalance(int loanId);

    public double getSumOfOpenningBalance(List<ReturnList> openningBalance);

    public double getOpenningBalanceWithoputODI(int loanId);

    public double findTotalPayment(int loanId);

    public double findTotalCharges(int loanId);

    public double getTotalDownPayment(int loanId);

    public double getDownPaymentBalance(int loanId);

    public double getPaidArrears(int loanId);

    public double getPaidOtherCharges(int loanId);

    public double getPaidDownPayment(int loanId);

    public double getPaidInsuarance(int loanId);

    public double getTotalInsuarance(int loanId);

    public List getDebitInstallmnetList(int loanId);

    public List<InstallmentReturnList> getDebitCreditInstallmentList(int loanId);

    public double getOverPayAmount(int loanId);

    public void addOverPayAmount(int loanId, double totalPayment, int userid, int debtorId, String newTransactionNo, String code, String receiptNo, String codeDescrpt, int branchId, Date systemDate);

    public boolean updateLoanHeaderIsDownPaymntPaid(int loanId, double initialDownPayment);

    public LoanInstallment findLoanInstallment(int loanId);

    public boolean addChargeDetails(SettlementCharges settlementCharges);

    public boolean updateInsuaranceStatus(int loanId, int status);

    public List<SettlementMain> getInstalmntWithODI(int loanId);

    public int getUserLogedBranch(int userId);

    public Date getSystemDate(int BranchId);

    public List<StlmntReturnList> getPaymentList(Date systemDate, int userBranchId);

    public SettlementPayments getPaymentDetail(int transactionId);

    public boolean addSettlementPaymentDelete(SettlementPayments settlementPayments, SettlementPaymentDeleteForm sttlmntDeleteDeleteForm);

    public boolean removeStlMainChargePayment(String receiptNo);

    public double getInstalmentBalance(int loanId);

    public void addOverPayDebit(int loanId, double overPayAmount, int userid, Integer debtorId, String newTransactionNo, String refNoOverPayCode, String receiptNo, String refNoOverPayCodeDescrpt, int userLogBranchId, Date systemDate);

    public List<OtherChargseModel> findOtherChargesByLoanId(int loanId);

    public boolean updatePaidStatus(Integer chargeID);

    public String getAdjustTransCode(String refNoAdjustment);

    public UmUser checkUserPrivileges(String uname, String pword);

    public double[] getTotalBalancesForDayEnd(Date systemDate, int loginBranchID);

    public boolean updateLoanHeaderIsDownPaymntPaid(int loanId, double initialOtherCharge, String othr);

    public double getOdiBalance(int loanId);

    public double getPaidOdiBalance(int loanId);

    public List<InstallmentReturnList> findAdjustmentList(int loanId, String refNoAdjustment);

    public double getAdjustmntBalance(int loanId, String refNoAdjustment);

    public double getTotalInterest(int loanId, String refNoAdjustment);

    public int getInstalmentCount(int loanId, String refNoInstallmentCode);

    public int getLoanIdByReceiptNo(String receiptNo);

    public RebateForm findRebateCustomerList(String agreementNo);

    public Double findRemainingInterestAmount(int loanID);

    public Boolean saveRebateLoan(RebateDetail formData);

    public int getInstalmentCount(int loanID);

    public boolean saveSMS(SmsDetails smsDetails);

    public boolean removeSMS(int loanId, String recptNo);

    public List<RebateDetail> getRebateDetails();

    public boolean deleteRebateLoan(int loanId);

    public boolean saveOrUpdateLoanCheque(LoanCheques loanCheques);

    public List<LoanCheques> getLoanChequeses();

    public List<LoanCheques> getLoanChequeses(int loanId);

    public boolean deleteLoanCheque(int chequeId, int loanId);

    public boolean setPaidLoanCheque(int chequeId, int loanId);

    public double getRebateBalance(int loanId);

    public boolean updateLoanHeaderStatus(int loanId, int type);

    public boolean updateRebateDetail(int loanId);

    public Double getLoanChequeTotAmount(int loanId);

    public List<RebateDetail> loadRebateApprovalList();

    public int findPaidInstallmentCount(int loanID);

    public RebateApprovals findRebateApprovals(int loanId, int type);

    public Boolean saveRebateApproval(RebateApprovals formData);

    public RebateDetail findRebateDetails(int loanId);

    public boolean isInitialPaymentDone(int loanId);

    public LoanHeaderDetails findLoanHeaderDetails(int printLoanId);

    public RebateDetail findRebateDetailsAfterRebate(int printLoanId);

    public SettlementPayments getPaymentDetail(String transactionNo);

    public List<LoanInstallment> findPaybleInstalmentsList(int loanId, Date systemDate);

    public boolean saveOrUpdateDebtorDeposit(DebtorDeposit debtorDeposit);

    public List<DebtorDeposit> findDebtorDeposits(int branchId);

    public DebtorDeposit findDebtorDeposit(int depositId);

    public List<DebtorDeposit> findDebtorDeposits(int debtorId, String accNo);

    public boolean updateDebtorDeposit(int debtorId, String accNo);

    public boolean updateInsuaranceProcessIsPay(int loanId);

    public boolean updateRemainingInsuraceProcessMainList(int loanId, double insuranceBalance);

    public void updateLoanInstallment(int loanId, double remainingInstallmentBalance, Date systemDate, String transCodeCredit, double crdAmt, String receiptNo, double payAmount);

    public boolean saveOrUpdateLoanRescheduleDetails(LoanRescheduleDetails loanRescheduleDetails);

    public List<LoanRescheduleDetails> findLoanRescheduleDetails();

    public boolean activeLoanRescheduleDetails(int rescheduleId);

    public boolean inActiveLoanRescheduleDetails(int rescheduleId);

    public LoanRescheduleDetails findLoanRescheduleDetail(int rescheduleId);

    public boolean changeLoanRescheduleDetailStatus(int rescheduleId, int childLoanId);

    public List<SettlementMain> findByLoanId(int loanId);

    public List<Posting> findByReferenceNo(String referenceNo);

    public List<LoanPosting> findLoanPostings(int loanId);

    public double getLoanPaidOtherCharges(int loanId);

    public double getLoanPaidInsurenceCharges(int loanId);

    public double getLoanPaidInstallments(int loanId);

    public File batchPostingToZip(Date startDate, Date endDate);

    public List<LoanHeaderDetails> findIssueLoans();

    public List<SettlementMain> findSettlements(int loanId, Date systemDate);

    public List<StlmntReturnList> getPaymentList(String sDate, String eDate, int userBranchId);

    public boolean isLoanChequesExist(Date systemDate);

    public List<StlmntReturnList> getPaymentListForReceiptNo(String receiptNo, int userBranchId);

    public List<StlmntReturnList> getPaymentListForAgNo(String agNo, int userBranchId);

    public List<StlmntReturnList> getPaymentListForVehNo(String vehNo, int userBranchId);

    public List<StlmntReturnList> getPaymentListForPayMode(int payMode, int userBranchId);

    public RebateQuotation getRebateQuotation(int loanId);

    public boolean addSettlementTransfer(SettlementTransfer st);

    public List<SettlementMain> loadRefundTable(String sDate, String eDate, int userLogBranchId);

    public List<SettlementMain> loadRefundTablebyAgNo(String AgreementNo, int userLogBranchId);

    public List<SettlementMain> loadRefundTablebyVehNo(String vehicleNo, int userLogBranchId);

    public List<SettlementMain> loadRefundTablebyCustomer(String customerName, int userLogBranchId);

    public SettlementMain getSettlementLoanheaderDebtorDetailsByLoanID(int loanId);

    public Double getOverpayAmt(int loanid);

    public boolean saveRefund(SettlementRefund sr);

    public boolean updateSettlementMainonRefund(int loanId, Double refundAmount, String receiptNo, Date sysDate);

    public File batchPostingODIToZip(Date startDate, Date endDate, int vType);

    
}

package com.ites.bankfinance.service;

import com.bankfinance.form.SavingNewAccForm;
import com.ites.bankfinance.savingsModel.*;

import java.util.List;

/**
 * Created by AXA-FINANCE on 5/21/2018.
 */
public interface SavingsService {

    public Boolean saveOrUpdateSavingsType(MSavingsType formData);

    public Boolean saveOrUpdateSubSavingsType(MSubSavingsType formData);

    public List<MSavingsType> findSavingType();

    public List<MSubSavingsType>  findSubSavingType();

    public List<MSubSavingsCharges>  findSavingCharges();

    List<DebtorHeaderDetails> findByName(String name, Object bID);

    List<DebtorHeaderDetails> findByNic(String nicNo, Object bID);

    int saveNewSavingsData(SavingNewAccForm formData);
}

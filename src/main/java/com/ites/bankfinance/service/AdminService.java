/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ites.bankfinance.service;

import com.bankfinance.form.AddJobsForm;
import com.bankfinance.form.RecOffColDateForm;
import com.ites.bankfinance.model.*;

import java.util.List;

/**
 *
 * @author SOFT
 */
public interface AdminService {

    public void saveAddLoanType(MLoanType formData);

    public MLoanType findLoanType(int id);

    public void saveSubLoanType(MSubLoanType subLoanType, int[] documentCheckListId, int[] documentIsCompulseryId, int[] otherChargesId, int[] chartOfAccounts, String[] chartOfAccountName);

    public boolean saveUser(UmUser user);

    public List<MLoanType> findLoanType();

    public List<MSubLoanType> findSubLoanType();

    public List<MCheckList> findCheckList();

    public List findUser();

    public MLoanType findMLoanType(int loanTypeId);

    public List<MSubLoanChecklist> findCheckList(int subLoanId);

    public List<UmUserType> findUserType();

    public List<MApproveLevels> findAppLevel();

    public List<UmUserTypeApproval> findUserApproveLevels() throws Exception;

    public void saveOrUpdateAppLevels(UmUserTypeApproval formData, int[] appLevels);

    public List<MSubTaxCharges> findOtherCharges();

    public MSubLoanType findMSubLoanType(int subLoanId);

    public List<MSubLoanOtherChargers> findOtherCharges(int subLoanId);

    public Integer saveNewEmployee(Employee formData);

    public List<Employee> findEmployeeList();

    public Employee findEmployee(int employeeId);

    public List findUserName(String userName);

    public List<MBranch> findBranch();

    public MWeightunits findWeightType();

    public List<MCenter> findCenter();

    public List<MCenter> findCenter(int branchID);

    public List<MDepartment> findDepartment(int branchId);

    public List<UmUser> findAllUser();

    public List<UmUser> findUserWithOutSuperAdmin();

    public List<MDepartment> findDepartmentList();

    public void saveOrUpdateUserSubLoan(UmUserTypeLoan formData, int[] subLoanIdList);

    public void saveDocumentList(MCheckList formData);

    public List<UmUserTypeApproval> findApproLevels(int userId);

    public List<UmUserTypeLoan> findUserTypeSubLoans();

    public void saveOtherCharges(MSubTaxCharges formData) throws Exception;

    public MSubTaxCharges findOtherChargesList(int otherChargeId) throws Exception;

    public UmUser findEmpUser(int employeeId);

    public int saveUserType(UmUserType formDate);

    public List<UmUserType> findUserTypeList();

    public UmUserType findEditUserType(int userTypeId);

    public List<AddJobsForm> findJobList();

    public boolean saveAddPages(AddJobsForm formDate, String[] tabId, String[] mainId);

    public int findNumOfBranch(int userTypeId);

    public boolean saveUserTypeBranch(UmUserBranch formData, int[] userTypeBranch);

    public UmUser findUserForEmpID(int empID);

    public List<MDepartment> findDepartment();

    public boolean saveUserDepartment(int branchId, int empID, int[] userDepartment);

    public List<UmUserBranch> findUserBranch(int empID);

    public List<UmUser> findUmUser();

    public List<UmUser> findRManagers();

    public List findManagerBranches(int managerUserId);

    public List<UmUser> findOfficer();

    public boolean saveFieldOfficer(UmFieldOfficers formData, int[] officerID);

    public List<UmFieldOfficers> findRecoveryManageres();

    public Integer saveNewBranch(MBranch formData);

    public MBranch findEditBranch(int branchId);

    public Boolean inActiveBranch(int branchID);

    public Boolean activeBranch(int branchID);

    public MBranch findActiveBranch(int branchId);

    public boolean saveDepartment(MDepartment formData);

    public Boolean inActiveEmployee(int empNo);

    public Boolean activeEmployee(int empNo);

    public UmUser findUserName(int userId);

    public List<UmUserBranch> findUserBranches(Integer userId);

    public String findBranchName(int branchId);

    public List<AddJobsForm> findDefineJobs(int userTypeId);

    public List<AMaintab> findMainTabs();

    public List<ASubtab> findSubTabs();

    public List<ConfigChartofaccountLoan> findConfigChartOfAccounts(int subLoanId);

    public Boolean checkSubLoanType(String subLoanType);

    public Integer saveNewSupplier(MSupplier mSupplier);

    public MSupplier findSupplier(int supplierId);

    public List<MSupplier> loadSuppliers();

    public Integer saveOrUpdateCollectionDates(List<RecOffColDateForm> list, int branchId);

    public RecOffCollDates hasCollectionDates(int recOffId, int branchId);

    public List<RecOffCollDates> getCollectionDates(int branchId);

    public MCheckList findDocument(int docId);

    public List<UmUserTypeLoan> findUserTypeLoanList(int userId);

    public List<Chartofaccount> getChartOfAccounts();

    public boolean saveChartOfAccounts(List<Chartofaccount> cs);

    public List<MSeizerDetails> findSeizerDetailsList();

    public Boolean inActiveSeizer(int seizerId);

    public Boolean activeSeizer(int seizerId);

    public Boolean saveOrUpdateSeizerDetails(MSeizerDetails seizerDetails);

    public MSeizerDetails findSeizerDetailsList(int seizerID);

    public boolean addOrRemoveModules(String[] modules, int status);

    public boolean changeLoanType(int loanTypeId, int status);

    public boolean changeSubLoanType(int subLoanTypeId, int status);

    public String findEmoloyeeLastName(Integer marketingOfficer);

    public MSystemValidation findSystemValidation();

    public int maxBranchCount();

    public Boolean saveOrUpdateYardDetails(MYardDetails yardDetails);

    public List<MYardDetails> findYardDetailsList();

    public MYardDetails findYardDetailsList(int yardID);

    public Boolean inActiveYard(int yardId);

    public Boolean activeYard(int yardId);

    public Boolean saveOrUpdateVehicleDetails(VehicleType vehicleType);

    public List<VehicleType> findVehicleTypesList();

    public VehicleType findVehicleTypeList(int vehicleTypeId);

    public Boolean saveOrUpdateVehicleMakeDetails(VehicleMake vehicleMake);

    public List<VehicleMake> findVehicleMakeList(int vehicleType);

    public List<VehicleModel> findVehicleModelList(int vehicleTypeId);

    public Boolean saveOrUpdateVehicleModelDetail(VehicleModel vehicleModel);

    public Boolean inActiveVehicle(int vehicleId);

    public Boolean activeVehicle(int vehiledId);

    public Integer saveNewCenter(MCenter formData);

    public Integer saveOrUpdateGoldValue(MWeightunits formData);

    public MCenter editCenter(int centerId);

    public boolean inActiveCenter(int centerId);

    public Boolean activeCenter(int centerID);

    public List<MGroup> findGroup();

    public List<MGroup> findGroup(int centerID);

    public Integer saveNewGroup(MGroup formData);

    public MGroup editGroup(int groupID);

    public boolean inActiveGroup(int groupID);

    public Boolean activeGroup(int groupID);

    public List<DebtorHeaderDetails> findGroupUsers(int groupID);

    //savings

    public boolean changeAccountType(int accountTypeId, int status);

    public boolean changeSubAccountType(int subAccountTypeId, int status);

//    public List<MSubLoanType> findSubAccountType();

}

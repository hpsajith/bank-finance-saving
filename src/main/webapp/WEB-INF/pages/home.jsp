<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<html>
    <head>
        <title>Login Page</title>
        <link rel="stylesheet" href="${cp}/resources/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="${cp}/resources/bootstrap/css/bootstrap-theme.css">
        <link rel="stylesheet" href="${cp}/resources/bootstrap/css/bootstrap.min.css">
        <script src="${cp}/resources/js/jquery.min.js"></script>
        <script src="${cp}/resources/bootstrap/js/bootstrap.js"></script>
        <script src="${cp}/resources/bootstrap/js/bootstrap.min.js"></script>
        <style>
            .error {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #a94442;
                background-color: #f2dede;
                border-color: #ebccd1;
            }

            .msg {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #31708f;
                background-color: #d9edf7;
                border-color: #bce8f1;
            }

            #login-box {
                width: 400px;
                padding: 20px;
                margin: 100px auto;
                -webkit-box-shadow: 11px 12px 6px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 11px 12px 6px -9px rgba(0,0,0,0.75);
                box-shadow: 11px 12px 6px -9px rgba(0,0,0,0.75);
                border: 1px solid #DDDDDD;
                background: #F8F8F8;
            }
            #login_subHeading{
                width:100%;
                margin: 0;
                padding: 10px;
                background: linear-gradient(to right, rgba(94,142,180,1) 0%, rgba(116,174,219,1) 59%, rgba(197,227,235,1) 100%);   
                font-weight: bold;
                font-family: sans-serif;
                font-size: 13pt;
            }
        </style>
    </head>
    <body onload='document.loginForm.username.focus();'>



        <div id="login-box">
            <div id="login_subHeading">User Login</div>

            <c:if test="${not empty error}">
                <div class="error">${error}</div>
            </c:if>
            <c:if test="${not empty msg}">
                <div class="msg">${msg}</div>
            </c:if>
                
            <div style="padding-top:25px">
                <form name='loginForm' action="<c:url value='/login' />" method='POST'>
                    <div class="col-xs-10" style="margin-top:10px;">
                        <input class="input-sm" type='text' name='username' value='' placeholder="User Name">
                    </div>
                    <div class="col-xs-10" style="margin-top:10px;">
                        <input class="input-sm"  type='password' name='password' value='' placeholder="Password">
                    </div>
                    <div class="col-xs-10" style="margin-top:10px;">
                        <input name="submit" type="submit" value="submit" />
                    </div>

                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}" />

                </form>
                <div style="clear: both"></div>
            </div>



        </div>

    </body>
</html>
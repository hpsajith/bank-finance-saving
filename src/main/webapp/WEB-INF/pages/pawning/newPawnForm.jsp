<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $(".newTable").chromatable(
            {
                width: "100%", // specify 100%, auto, or a fixed pixel amount
                height: "15%",
                scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
            }
        );
        $('#downPaymentId').val(0);
        $(".box").draggable();
    });

</script>

<style>
    #groupLoadBox {
        -webkit-box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        -moz-box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        /*background: #BDBDBD;*/
        background: #BDBDBD;
    }

    #form_ {
        /*        -webkit-box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);
                -moz-box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);
                box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);*/
    }
</style>

<div class="row divError"></div>
<div class="row divSuccess">${saveUpdate}</div>

<form name="pawnForm" id="newPawnForm">
    <input type="hidden" name="unitprice" id="unitprice" value="${unitList.pricePerUnit}">
    <div class="row">
        <input type="hidden" value="${branchCode}" id="branchCodeId"/>
        <input type="hidden" value="${systemDate}" id="systemDateId"/>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent1" style="margin-bottom:10px">
                <div class="col-md-2" style="text-align: left;font-weight: 600; color: black"></div>
                <div class="col-md-3"></div>

                <div class="col-md-2" style="text-align: right;font-weight: 600; color: black"><strong>Pawining
                    Id</strong></div>
                <div class="col-md-3"><input type="text" style="color:#337ab7;width: 100%" value="${pawnId}"
                                             name="pawnId"
                                             id="txtPawnId"
                                             onkeypress="return isNumber(event)"/></div>
                <div class="col-md-2"><input type="button" class="btn btn-default searchButton" onclick="searchPawn()"
                                             style="height: 25px;"></div>
            </div>
        </div>
    </div>

    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;" id="form_">
        <c:choose>
            <c:when test="${edit}">
                <div class="row"><label>Edit Loan</label></div>
            </c:when>
            <c:otherwise>
                <div class="row,msyl-text1"><label>Add New Pawning</label></div>
            </c:otherwise>
        </c:choose>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent0" style="margin-bottom:5px">
                <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Loan Type</div>
                <div class="col-md-3">
                    <select name="loanMainType" id="loanTypeList" style="width:100%">
                        <option value="0">--SELECT--</option>
                        <c:forEach var="types" items="${types}">
                            <c:choose>
                                <c:when test="${types.subLoanId==loan.loanType}">
                                    <option value="${types.subLoanId}" selected>${types.subLoanName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${types.subLoanId}">${types.subLoanName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select></div>
                <input type="hidden" name="loanType" value="${loan.loanType}"/>

                <div class="col-md-2" style="text-align: right;font-weight: 600; color: black"><strong>Rate</strong>
                </div>
                <div class="col-md-3"><input type="text" name="loanRate" value="${loan.loanRate}"
                                             style="width: 100%;" readonly/></div>
                <div class="col-md-2 edit" style="margin-top: 5px;" onclick="editCharge(-1, 1)"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent3" style="margin-bottom:5px">
                <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Due Date</div>
                <div class="col-md-3"><input type="text" id="dueDayTxt" name="dueDay" class="txtCalendar"
                                             value="${loan.dueDay}" style="width: 100%;"/></div>

                <div class="col-md-2" style="text-align: right;font-weight: 600; color: black">Period</div>
                <div class="col-md-3"><input type="text" name="loanPeriod" value="${loan.loanPeriod}"
                                             style="width: 100%"/></div>
                <div class="col-md-2">
                    <select name="periodType" name="periodType" style="height: 25px">
                        <option value="1">Month</option>
                        <option value="4">Week</option>
                        <option value="3">Dates</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="msyl-gredient2" style="width: 99%;padding-top: 8px">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent4" style="margin-bottom:5px">
                    <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Gold Weight</div>
                    <div class="col-md-3"><input type="text" style="width: 103%;text-align: center" id="goldweight"
                                                 name="goldweight"/></div>
                    <div class="col-md-2">
                        <select name="periodType" name="periodType" style="height: 25px">
                            <option value="1">grams</option>
                            <option value="4">pounds</option>
                        </select>
                    </div>

                    <div class="col-md-2" style="text-align: right;font-weight: 600; color: black">Market Value</div>
                    <div class="col-md-3"><label id="marketvalue" name="marketvalue"
                                                 style="text-shadow: 2px 2px 2px #005ce6;font-size: 20px;">0000.00</label>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent5" style="margin-bottom:5px">
                    <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Valuation</div>
                    <div class="col-md-3"><input name="valuation" ïd="valuation" type="text" style="width: 103%;"></div>
                    <div class="col-md-2"></div>

                    <div class="col-md-2" style="text-align: right;font-weight: 600; color: black">Loan Amount</div>
                    <div class="col-md-3"><input id="loanamount" name="loanamount" type="text" style="width: 100%;">
                    </div>

                </div>
            </div>

        </div>

        <div class="row">
            <%--customer details--%>
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div id="groupLoadBox" style="margin-top: 20px">
                    <div class="row" style="padding-top: 10px;padding-left: 15px">
                        <div class="row" style="margin-left: 12px; font-weight: bold">Customer Details</div>

                        <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent">
                            <div class="row">
                                <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">NIC</div>
                                <div class="col-md-3"><input type="text" style="width: 100%;"
                                                             value="${debtor.debtorNic}"
                                                             id="debtorNic"/></div>

                                <div class="col-md-2"
                                     style="text-align: right;padding-left: 20px; padding-bottom: 10px">
                                    <strong>Name</strong></div>
                                <div class="col-md-3"><input
                                        type="text" style="width: 100%" id="debtorName"
                                        value="${debtor.nameWithInitial}"/></div>
                                <div class="col-md-1"><input type="button"
                                                             class="btn btn-default searchButton"
                                                             id="btnSearchCustomer"
                                                             style="height: 25px">
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent7">
                            <div class="row">
                                <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Address
                                </div>
                                <div class="col-md-3"> <textarea name="" id="debtorAddress"
                                                                 style="width: 100%">${debtor.debtorPersonalAddress}</textarea>
                                </div>

                                <div class="col-md-2"
                                     style="text-align: right;padding-left: 20px; padding-bottom: 10px">
                                    <strong>Contact No</strong></div>
                                <div class="col-md-3"><input type="text"
                                                             style="width: 100%"
                                                             id="debtorTelephone"
                                                             value="${debtor.debtorTelephoneMobile}"/></div>

                                <input type="hidden" id="debID" name="debtorId" value="${debtor.debtorId}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--customer serch end --%>

            <%--article start--%>
            <div class="col-md-12 col-lg-12 col-sm-12" style="align-content: center">
                <div class="row" style="margin-top: 25px;">
                    <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Invoice Details</div>
                    <div class="col-md-3"><input type="text" style="width: 100%"/></div>
                    <div class="col-md-2"><input type="button" id="btnAddProperty" class="btn btn-default searchButton"
                                                 style="height: 25px"/></div>
                </div>
                <div class="row" style="overflow-y: auto">
                    <div class="col-md-8 col-lg-8 col-sm-8" style="align-content: center">
                        <table id="tblArticle" class="dc_fixed_tables table-bordered" width="100%" border="0"
                               cellspacing="0" cellpadding="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Description</th>
                                <th>Total Weight</th>
                                <th>Net Weight</th>
                                <th>Carrot Rate</th>
                                <th>Price</th>
                                <th>edit</th>
                                <th>remove</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="invoice" items="${invoise}">
                                <tr style="text-align:right" id="v_tr${invoice.articleId}">
                                    <td></td>
                                    <td>${invoice.articleIdentification}</td>
                                    <td>${invoice.articleWeight1}</td>
                                    <td>${invoice.articleWeight2}</td>
                                    <td>${invoice.articleCarrateRate}</td>
                                    <td>${invoice.articlePrice}</td>
                                    <td>
                                        <a onclick="editPropertyForm(2,${invoice.articleId})" class="edit"></a>
                                    </td>
                                    <td>
                                        <a onclick="deleteArticle(${invoice.articleId})" class="delete"></a>
                                    </td>
                                </tr>
                            </c:forEach>
                            <tr style="text-align:right" id="totalRow" style="font-weight: bold">
                                <td>Total</td>
                                <td></td>
                                <td>${t_weight}</td>
                                <td>${n_weight}</td>
                                <td>${carror_rate}</td>
                                <td>${total_value}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row" id="hidArticle">
                    <input type='hidden' id='pa${invoice.articleId}' name='proprtyType' value='2'/>
                    <c:forEach var="invoice" items="${invoise}">
                        <input type='hidden' name='properyId' id='pap{invoice.articleId}'
                               value='${invoice.articleId}'/>
                    </c:forEach>
                </div>
            </div>
            <%--article end--%>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <%--other chargers start--%>
                <div class="row" style="margin-top: 25px;margin-left: 3px">
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        <div class="row" style="width: 260px;">
                            <table id="otherChargesTable"
                                   class="table table-striped table-bordered table-hover table-condensed table-edit">
                                <caption style="color: #000; font-weight: 600; font-size: 12px;">Add Other Charges
                                </caption>
                                <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:choose>
                                    <c:when test="true">
                                        <c:forEach var="other" items="${otherChargers}">
                                            <tr id="${other.chargeID}">
                                                <c:choose>
                                                    <c:when test="${other.isPrecentage}">
                                                        <c:set var="isPres" value="1"></c:set>
                                                        <c:set var="precentg" value="${other.rate}%"></c:set>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:set var="isPres" value="0"></c:set>
                                                        <c:set var="precentg" value="${other.rate}"></c:set>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:choose>
                                                    <c:when test="${other.isAdded}">
                                                        <c:set var="isAdd" value="1"></c:set>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:set var="isAdd" value="0"></c:set>
                                                    </c:otherwise>
                                                </c:choose>
                                                <td>${other.description}
                                                    <input type='hidden' value='${isPres}' id='t${other.id}'/>
                                                    <input type='hidden' value='${other.chargeID}'
                                                           id='cha${other.id}'/>
                                                    <input type='hidden' id='isadd${other.id}' value='${isAdd}'/>
                                                </td>
                                                <td><a onclick="editCharge(${other.id}, 1)"
                                                       id='aaa${other.id}'>${precentg}</a>
                                                    <input type='hidden' value='${other.rate}' id='c${other.id}'/>
                                                </td>
                                                <td>
                                                    <a onclick="editCharge(${other.id}, 2)"
                                                       style="text-align: right;"
                                                       id='hidAmnt${other.id}'>${other.amount}</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        <tr>
                                            <td colspan="2" style="text-align: right"><label>Total</label></td>
                                            <td style="text-align: right"><label id='totalOther'>${total}</label>
                                            </td>
                                        </tr>
                                    </c:when>
                                </c:choose>
                                </tbody>
                            </table>
                        </div>
                        <div class="row" id="hidCharges">
                            <c:forEach var="other" items="${otherChargers}">
                                <input type="hidden" name="chragesId" value="${other.id}"/>
                                <input type="hidden" name="chragesRate" value="${other.rate}"/>
                                <input type="hidden" name="chragesAmount" value="${other.amount}"/>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4"
                         style=" margin-top: 34px; background: #F2F2F2; width: 300px;">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12"><label>Loan Details</label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5" style="">Loan Amount</div>
                            <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                           id="viewLoanAmount"
                                                                           value="${loan.loanAmount}" readonly/>
                            </div>
                        </div>
                        <input type="hidden" class="decimalNumber" id="viewInvestment"
                               value="${loan.loanInvestment}"/>
                        <input type="hidden" class="decimalNumber" id="viewDownPayment"
                               value="${loan.loanDownPayment}"/>
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">Period</div>
                            <div class="col-md-7 col-lg-7 col-sm-7"><input style="text-align:right" type="text"
                                                                           id="viewLoanPeriod"
                                                                           value="${loan.loanPeriod}" readonly/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">Interest Rate</div>
                            <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" style="text-align:right"
                                                                           id="viewRate" value="${loan.loanRate}"
                                                                           name="" readonly/></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">Interest</div>
                            <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                           id="viewInterest" name="loanInterest"
                                                                           value="${loan.loanInterest}" readonly/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">Total Interest</div>
                            <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                           id="viewTotalInterest" name=""
                                                                           value="${totalInterest}" readonly/></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">Installment</div>
                            <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                           id="viewInstallment"
                                                                           name="loanInstallment"
                                                                           value="${loan.loanInstallment}"
                                                                           readonly/>
                            </div>
                        </div>
                        <div class="row" style="border-top: 1px solid #222; padding-top: 3px; margin-top: 3px; ">
                            <div class="col-md-5 col-lg-5 col-sm-5">Total</div>
                            <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" id="viewTotal"
                                                                           class="decimalNumber" disabled="true"
                                                                           value="${totalAmount}"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4" style="margin-top: 33px;width: 300px;">

                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5" style=""><input placeholder="Approval 3"
                                                                                    type="text"
                                                                                    id="supplierName"
                                                                                    value="${approve.app_name3}"><input
                                    type="hidden" name="ced" id="supplierId" value="${approve.app_id3}"/></div>
                            <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnSupplier"
                                                                           class="btn btn-default searchButton"
                                                                           style="height: 25px;margin-left: 11px;"/>
                            </div>
                        </div>
                    </div>
                </div>
                <%--other chargers end--%>
            </div>
        </div>

        <c:choose>
            <c:when test="${edit}">
                <div class="row panel-footer" style="padding-top: 10px;">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           value="Cancel"/></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           onclick="uploadDocuments()"
                                                                           value="Upload Documents"/></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           id="btnUpdate" onclick="saveLoanForm()"
                                                                           value="Update"/></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        <div class="col-md-4 col-lg-4 col-sm-4"></div>
                        <div class="col-md-4 col-lg-4 col-sm-4"></div>
                        <div class="col-md-4 col-lg-4 col-sm-8"><input type="button"
                                                                       class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                       id="btnProcess"
                                                                       onclick="processLoanToApproval()"
                                                                       value="Process" style="width: 100px"/></div>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row panel-footer" style="padding-top: 10px;">
                    <div class="col-md-6 col-lg-6 col-sm-6"></div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           value="Exit"/></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           value="Cancel"/></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           id="btnSave" onclick="saveLoanForm()"
                                                                           value="Save"/></div>
                        </div>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>
            </c:otherwise>
        </c:choose>
        <input type="hidden" id="pricePerUnit" name="pricePerUnit" value="${units.pricePerUnit}"/>
        <input type="hidden" id="gurant_debtr_type"/>
        <input type="hidden" name="loanDownPayment" value="0.00"/>
        <input type="hidden" name="loanInvestment" value="${loan.loanInvestment}"/>
        <div class="row" id="hiddenContent"></div>
    </div>
    </div>
</form>
</div>

<!--Search Customer-->
<div class="searchCustomer box"
     style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto; left: 15%"
     id="popup_searchCustomer">

    <div id="message_newLonForm" class="searchCustomer"
         style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
    <div id="docDivCheck" class="searchCustomer" style="width: 65%;border: 0px solid #5e5e5e;"></div>
    <div class="searchCustomer" id="editRate_div"
         style="width: 40%; margin-left: 10%; margin-top: 10%; border:4px solid #1992d1;padding-right: 5px"></div>
    <input type="hidden" value="${message}" id="testMessage"/>
    <!--BlockUI-->
    <div class="searchCustomer" style=" width: 1000px; top:5%; left:10%;" id="popup_addProperty">
        <div class="row" style="margin:15 0 0 0; height: 60%; overflow-y: auto; width:100%" id="formPropertyContent">
            <!--Form content goes here-->
        </div>
    </div>
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label>Add Articles</label></div>
    <div class="row" style="padding: 10px;">

    </div>
    <div class="row" style="margin:15 0 0 0; height: 60%; overflow-y: auto; width:100%" id="formPropertyContent">

    </div>
</div>

<script type="text/javascript">

    //calander perform
    $(".txtCalendar").datepicker({
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd'
    });

    //gold weight calculation
    $(document).ready(function () {
        $('#goldweight').keypress(function () {
            var weight = $("#goldweight").val();
        });

        //Block Load SearchCustomer Form
        $(document).ready(function () {
            $('#btnLoadGuarantor').click(function () {
                ui('#popup_searchCustomer');
                loadSearchCustomer(2);
            });
            $('#btnSearchCustomerExit').click(function () {
                unblockui();
            });

            //Block Laod GuarantorForm(Load SearchCustomer Form)
            $('#btnSearchCustomer').click(function () {
                ui('#popup_searchCustomer');
                loadSearchCustomer(1);
            });
            $('#btnSearchCustomerExit').click(function () {
                unblockui();
            });

            //Block Laod SaleParsonName Form
            $('#btnLoadSaleParsonName').click(function () {
                ui('#popup_searchOfficers');
                $("#app_title").text("Approval 1");
                searchUserType(1);
            });
            $('#btnSearchUseCancel').click(function () {
                unblockui();
            });

            //Block Load Recovery Manager Form
            $('#btnRecoveryManager').click(function () {
                ui('#popup_searchOfficers');
                $("#app_title").text("Approval 2");
                searchUserType(2);
            });
            $('#btnSearchUseCancel').click(function () {
                unblockui();
            });

            //Block Load Supplier Form
            $('#btnSupplier').click(function () {
                ui('#popup_searchOfficers');
                $("#app_title").text("Approval 3");
                searchUserType(3);
            });
            $('#btnSearchUseCancel').click(function () {
                unblockui();
            });

            //Block Load property Form
            $('#btnAddProperty').click(function () {
                loadPawningPropertyForm();
            });
            $('#btnSearchUseCancel').click(function () {
                unblockui();
            });


        });


        $(function () {

            //focus select loantype option
            $('select[name^="loanTypeList"]').eq(1).focus();

            jQuery.extend(jQuery.expr[':'], {
                focusable: function (el, index, selector) {
                    return $(el).is('a, button, :input, [tabindex]');
                }
            });

            //got to next input from enter key
            $(document).on('keypress', 'input,select,textarea', function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    // Get all focusable elements on the page
                    var $canfocus = $(':focusable');
                    var index = $canfocus.index(this) + 1;
                    if (index >= $canfocus.length)
                        index = 0;
                    $canfocus.eq(index).focus();
                }
            });

            //press enter on button
            $('input[type="button"]').on("keypress", function (eve) {
                var key = eve.keyCode || e.which;
                if (key == 13) {
                    $(this).click();
                }
                return false;
            });

            //enter loan amount and goto next input using tab/enter
            $('input[name=loanAmount]').keydown(function (e) {
                var code = e.keyCode || e.which;
                if (code === 9 || code === 13) {
                    var loanAmount = $(this).val();
//                var f_loanAmount = parseFloat(loanAmount);

                    $("#viewLoanAmount").autoNumeric();
                    $("#viewLoanAmount").val(loanAmount);
                    calculateLoan();
                }
            });

            //focus other input field other than loan amount
            $("input[name=loanAmount]").blur(function () {
                var loanAmount = $(this).val();
                $("#viewLoanAmount").autoNumeric();
                $("#viewLoanAmount").val(loanAmount);
                calculateLoan();
                calculateInvestment();
            });
            //focus other input field other than loan period
            $("input[name=loanPeriod]").blur(function () {
                var period = $(this).val();
                $("#viewLoanPeriod").val(period);
                calculateLoan();
            });
            //focus other input field other than loan downpayment
            $("input[name=loanDownPayment]").blur(function () {
                calculateInvestment();
            });

            //enter loan period and goto next input using tab
            $('input[name=loanPeriod]').keydown(function (e) {
                var code = e.keyCode || e.which;
                if (code === 9 || code === 13) {
                    var period = $(this).val();
                    $("#viewLoanPeriod").val(period);
                    calculateLoan();
                }
            });
            $("input[name=loanAmount]").blur(function () {
                var period = $(this).val();
                $("#viewLoanPeriod").val(period);
                calculateLoan();
            });

            //format desimal numbers
            $('.decimalNumber').autoNumeric();

            //change loantype change loan rate
            $('#loanTypeList').change(function () {
                var selected = $(this).find('option:selected');
                var loanTypeId = selected.val();
                if (loanTypeId > 0) {
                    $.ajax({
                        url: '/AxaBankFinance/findLoanRate/' + loanTypeId,
                        success: function (data) {
                            $("#viewRate").val(data);
                            $("input[name=loanRate]").val(data);
                            $("input[name=loanType]").val(loanTypeId);
                            calculateLoan();
                            $("#loanAmountId").focus();
                            loadOtherCharges(loanTypeId);
                        }
                    });


                }
            });

        });
    });

    //    Edit Rate
    function editRate() {
//        var loanRate = $(subLoan).val();
        alert("loanRate");
    }

    $('#goldweight').on('keyup', function () {
        var weight = $(this).val();
        var price = $('#pricePerUnit').val();
        // alert(weight+"  "+price+"  "+price*weight+"  Working Done")
        $("#marketvalue").html(weight * price);
    });

    //load other charges types
    function loadOtherCharges(subId) {
        $.getJSON('/AxaBankFinance/otherChargesByLoanId/' + subId, function (data) {
            $('#otherChargesTable tbody').html("");

            for (var i = 0; i < data.length; i++) {
                var description = data[i].description;
                var isPresentage = data[i].isPrecentage;
                var charge, t, rateCharge;
                if (isPresentage) {
                    charge = data[i].taxRate;
                    rateCharge = charge + "%";
                    t = 1;
                } else {
                    charge = data[i].amount;
                    rateCharge = charge;
                    t = 0;
                }
                var id = data[i].id;
                var is_added = 0;
                var isAdded = data[i].isAdded;
                if (isAdded) {
                    is_added = 1;
                }
                $('#otherChargesTable tbody').append("<tr id='" + id + "'>" +
                    "<td>" + description + "<input type='hidden' value='" + t + "' id='t" + i + "'/><input type='hidden' value='" + id + "' id='cha" + i + "'/><input type='hidden' id='isadd" + i + "' value='" + is_added + "'/></td>" +
                    "<td><input type='hidden' value='" + charge + "' id='c" + i + "'/>" + "<a href='#' onclick='editCharge(" + i + ",1)' id='aaa" + i + "'>" + rateCharge + "</a></td>" +
                    "<td style='text-align:right' id='amount" + i + "'><a href='#' onclick='editCharge(" + i + ",2)' style='text-align: right;' id='hidAmnt" + i + "'></a></td></tr>");
            }
            $('#otherChargesTable tbody').append("<tr><td colspan='2'><label>Total</label></td><td class='decimalNumber'><label id='totalOther'><label></td>");
        });

    }


    //click loan other charges row
    var totalCharges = 0.00;
    var chargesAddedToLoan = 0.00;

    function calculateCharge() {
        totalCharges = 0.00;
        var i = 0;
        var loanAmount = $("input[name=loanAmount]").val();
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("last");
            } else {
                var labelId = "amount" + i;
                var amount;
                var chargeType = $("#t" + i).val();
                var charge = $("#c" + i).val();
                var isAdded = $("#isadd" + i).val();

                if (chargeType == 1) {
                    if (loanAmount != "") {
                        amount = ((charge * parseFloat(loanAmount)) / 100).toFixed(2);
                    } else {
                        amount = (parseFloat(0)).toFixed(2);
                    }
                } else {
                    amount = (parseFloat(charge)).toFixed(2);
                }
                if (isAdded == 1) {
                    chargesAddedToLoan = chargesAddedToLoan + amount;
                }

                if (isAdded == 0) {
                    totalCharges = totalCharges + parseFloat(amount);
                }

                $("#hidAmnt" + i).text(amount);
                $("#totalOther").text(totalCharges.toFixed(2));
            }
            i++;
        });
    }

    function loadSearchCustomer(type) {
        $("#gurant_debtr_type").val(type);
        $.ajax({
            url: '/AxaBankFinance/searchCustomer',
            success: function (data) {
                $('#popup_searchCustomer').html("");
                $('#popup_searchCustomer').html(data);

            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    function unloadSearchCustomer() {
        unblockui();
    }

    function saveLoanForm() {

        var propid = $("input[name=properyId]").val();
        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function (data) {
                if (!data) {
                    var validate = validateForm();
                    SetOtherCharges();
                    calculateLoan();
                    if (validate) {
                        document.getElementById("btnSave").disabled = true;
                        $.ajax({
                            url: '/AxaBankFinance/saveNewPawningForm',
                            type: 'POST',
                            data: $("#newLoanForm").serialize(),
                            success: function (data) {
                                $('#formContent').html(data);
                                $(".divSuccess").css({'display': 'block'});
                                $("html, body").animate({scrollTop: 0}, "slow");
                            },
                            error: function () {
                                alert("Error Loading...");
                            }
                        });
                    } else {
                        $(".divError").html("");
                        $(".divError").css({'display': 'block'});
                        $(".divError").html(errorMessage);
                        errorMessage = "";
                        $("html, body").animate({scrollTop: 0}, "slow");
                    }
                } else {
                    warning("User Is Blocked", "message_newLonForm");
                }
            },
            error: function () {
                alert("Connection is refuced by the server...");
            }
        });
    }

    function SetOtherCharges() {
        var i = 0;
        $("#hidCharges").html("");
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("lastRO");
            } else {
                console.log("ROW" + i);
                var chargeId = $("#cha" + i).val();
                var charge = $("#c" + i).val();
                console.log(charge);
                var amount = $("#hidAmnt" + i).text();
                $("#hidCharges").append("<input type='hidden' name='chragesId' value='" + chargeId + "'/>" +
                    "<input type='hidden' name='chragesRate' value='" + charge + "'/>" +
                    "<input type='hidden' name='chragesAmount' value='" + amount + "'/>");
            }
            i++;
        });
    }


    function calculateLoan() {
        var loanTYpe = $(this).find('option:selected').val();
        if (loanTYpe == 0)
            return;
        var amount = $("input[name=loanInvestment]").val();
        if (amount == null || amount == "")
            return;
        var period = $("#viewLoanPeriod").val();
        if (period == null || period == "")
            return;

        var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));

        var newRate = $("#viewRate").val();
        var rate = newRate.replace('%', '');
        var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);
        var totalInterest = (parseFloat(interest) * period).toFixed(2);
        var totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
        var installment = (parseFloat(totalAmount) / period).toFixed(2);

        var total = parseFloat(totalAmount).toFixed(2);

        $("#viewInstallment").val(installment);
        $("#viewInterest").val(interest);
        $("#viewTotalInterest").val(totalInterest);
        $("#viewTotal").val(total);
    }

    function calculateInvestment() {
        var loanAmount = $("input[name=loanAmount]").val();
        var downPayment = $("input[name=loanDownPayment]").val();
        if (loanAmount != "" && downPayment != "") {
            var f_amount = parseFloat(loanAmount);
            var f_downpayment = parseFloat(downPayment);
            if (f_amount > f_downpayment) {
                var investment = f_amount - f_downpayment;
                $("input[name=loanInvestment]").val(investment);
                $("#viewDownPayment").val(downPayment);
                $("#viewInvestment").val(investment);
                calculateCharge();
            } else {
                alert("Loan amount should be greater than down payment");
                $("input[name=loanDownPayment]").css("border", "1px solid red");
                $("input[name=loanDownPayment]").focus();
            }
        }
    }

    var errorMessage = "";

    function validateForm() {
        var validate = true;

        var loanTYpe = $("#loanTypeList").find('option:selected').val();
        if (loanTYpe == 0) {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Select LoanType";
            $("#loanTypeList").addClass("txtError");
        } else {
            $("#loanTypeList").removeClass("txtError");
        }
        var loanAmount = $('input[name=loanAmount]').val();
        if (loanAmount == null || loanAmount == "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Enter Loan Amount";
            $("#loanAmountId").addClass("txtError");
        } else {
            $("#loanAmountId").removeClass("txtError");
        }

        var period = $('input[name=loanPeriod]').val();
        if (period == null || period == "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Enter Loan Time Period";
            $('input[name=loanPeriod]').addClass("txtError");
        } else {
            $('input[name=loanPeriod]').removeClass("txtError");
        }
        var debtorID = $("#debID").val();
        if (debtorID == null || debtorID == "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Select a Debtor";
            $("#debtorName").addClass("txtError");
        } else {
            $("#debtorName").removeClass("txtError");
        }

        //validate approvelevels
        var app3 = $("#supplierName").val();
        if (app3 === "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "You Should add final approval";
            $("#supplierName").addClass("txtError");
        } else {
            $("#supplierName").removeClass("txtError");
        }

        return validate;
    }

    //add sales person,recovery manager, supplier
    function searchUserType(type) {
        $("#hidOfficerType").val(type);
        var loanTypeId = $("input[name=loanType]").val();
        if (loanTypeId === "") {
            loanTypeId = 0;
        }
        $.ajax({
            url: '/AxaBankFinance/SearchOfficers/' + type + "/" + loanTypeId,
            success: function (data) {
                var tbody = $("#tblOfficers tbody");
                tbody.html("");
                for (var i = 0; i < data.length; i++) {
                    tbody.append("<tr onclick='clickOfficer(this)'><td>" + data[i].empId + "</td>" +
                        "<td>" + data[i].userName + "</td>" +
                        "<td style='display:none'>" + data[i].userId + "</td></tr>");
                }
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //click supplier row and add him to loan form
    var o_empNo = "", o_name = "", o_userId = "";

    function addOfficerToLoanForm() {
        var type = $("#hidOfficerType").val();
        if (o_userId == "") {
            alert("Please Select a name");
            return;
        } else {
            if (type == 1) {
                $("#salesPersonId").val(o_userId);
                $("#salesPersonName").val(o_name);
            } else if (type == 2) {
                $("#recoveryPersonId").val(o_userId);
                $("#recoveryPersonName").val(o_name);
            } else if (type == 3) {
                $("#supplierId").val(o_userId);
                $("#supplierName").val(o_name);
            }
        }
        o_empNo = "", o_name = "", o_userId = "";
        unblockui();
    }

    //click supplier,recovery, sales offices table row
    function clickOfficer(tr) {
        var selected = $(tr).hasClass("highlight");
        $("#tblOfficers tbody tr").removeClass("highlight");
        if (!selected)
            $(tr).addClass("highlight");

        var tableRow = $(tr).children("td").map(function () {
            return $(this).text();
        }).get();
        o_empNo = $.trim(tableRow[0]);
        o_name = $.trim(tableRow[1]);
        o_userId = $.trim(tableRow[2]);
    }


    //close sales person, recovery officer, supplier pop up
    function cancelOfficersTable() {
//        o_id = "", o_name = "", o_address = "";
//        unblockui();
    }

    //other property
    function loadPawningPropertyForm() {
//        var select = $("#slctProperty").find('option:selected').val();
        var select = 2;
        $.ajax({
            url: '/AxaBankFinance/loadPropertForm/' + select,
            success: function (data) {
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Save");
                ui('#popup_addProperty');
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //edit property
    function editPropertyForm(type, id) {
        $.ajax({
            url: '/AxaBankFinance/editPropertForm/' + type + '/' + id,
            success: function (data) {
                ui('#popup_addProperty');
//                $("#slctProperty").css("display", "none");
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Update");
            },
            error: function () {
                alert("Error Loading...");
            }
        });
        calculateTotalValues();
    }

    //search customer by nic
    $('#debtorNic').keyup(function (e) {
        var nicNo = $(this).val();
        if (nicNo.length === 10) {
            $.ajax({
                dataType: 'json',
                url: '/AxaBankFinance/searchByNic_new/' + nicNo,
                success: function (data) {
                    var debId = data.debtorId;
                    if (debId > 0) {
                        var name = "", nic = "", address = "", telephone = "";
                        if (data.nameWithInitial !== null)
                            name = data.nameWithInitial;
                        if (data.debtorNic !== null)
                            nic = data.debtorNic;
                        if (data.debtorPersonalAddress !== null)
                            address = data.debtorPersonalAddress;
                        if (data.debtorTelephoneMobile !== null)
                            telephone = data.debtorTelephoneMobile;

                        $("#debID").val(debId);
                        $("#debtorNic").val(nic);
                        $("#debtorName").val(name);
                        $("#debtorAddress").val(address);
                        $("#debtorTelephone").val(telephone);

                        var url2 = "/AxaBankFinance/viewProfilePicture/" + debId;
                        $("#customerProfilePicture").html("<img src='" + url2 + "' ></img>");

                    } else {
                        alert("Search Result Not found");
                        $("#debtorNic").val("");
                        $("#debID").val("");
                        $("#debtorName").val("");
                        $("#debtorAddress").val("");
                        $("#debtorTelephone").val("");
                    }

                },
                error: function () {
                    alert("Error Loading...");
                }
            });
        }
    });

    $('input[name=loanId]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            searchLoan();
        }
    });


    //search loan by loan id
    function searchLoan() {
        var loanId = $("input[name=loanId]").val();
        var view = "message_newLonForm";

        if (loanId !== "") {
            $.ajax({
                url: '/AxaBankFinance/searchPawningDeatilsById/' + loanId,
                success: function (data) {
                    $('#formContent').html(data);
                    document.getElementById("txtLoanId").readOnly = true;
                    var msg = $("#testMessage").val();
                    if (msg != "") {
                        warning(msg, view);
                    }
//                    var flag = "${hasData}";
////                    alert(flag);
//                    if (flag === "0") {
//                        $("#message_newLonForm").html(data);
//                        ui("#message_newLonForm");
//                    } else if (flag === "1") {
//                        $('#formContent').html(data);
//                        document.getElementById("txtLoanId").readOnly = true;
//
//                    }

                },
                error: function () {
                    alert("Error Loading...");
                }
            });

        } else {
            alert("Enter Loan Id");
        }

    }

    //proceed button
    function processLoanToApproval() {
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loanDocumentSubmissionStatus/" + loan_id,
            success: function (data) {
                var process = data;
                var view = "message_newLonForm";
                if (process == 1) {
                    //cannot process
                    var msg = "You have to submit minimum documents to proceed";
                    warning(msg, view);
                } else if (process == 2) {
                    //process with pending dates
                    var msg = "Do you want to proceed loan without completing all documents";
                    dialog(msg, view, loan_id);
                } else {
                    //procees successfully
                    ui("#proccessingPage");
                    viewAllLoans(0);
                }
            },
            error: function () {

            }
        });
    }

    //document upload button
    function uploadDocuments() {
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loadDocumentChecking",
            data: {loan_id: loan_id},
            success: function (data) {
                $("#docDivCheck").html(data);
                ui('#docDivCheck');
            }
        });
    }

    //edit charge
    function editCharge(row, type) {
        console.log(row);
        $.ajax({
            url: "/AxaBankFinance/loadEditRate/" + row + "/" + type,
            success: function (data) {
                $("#editRate_div").html(data);
                ui("#editRate_div");
            }
        });
    }
</script>
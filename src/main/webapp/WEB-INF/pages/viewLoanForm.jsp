<div style="font-size: 12px; border: 1px solid #BDBDBD; border-radius: 2px;">
    <div class="row">
        <div class="col-md-8">
            <!--table div-->
            <div class="row">
                <div class="col-md-7">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Debtor Details</caption>
                        <thead>
                            <tr>
                                <th>Loan No</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact No</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Anna</td>
                                <td>Kaluthara south</td>
                                <td>0715544789</td>
                            </tr>                            
                        </tbody>
                    </table>
                </div>
                <div class="col-md-5">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Dependent Details</caption>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Debbie</td>
                                <td>Colombo</td>
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>  
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Employment Details</caption>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Anna</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Business Details</caption>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Firstname</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Anna</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Bank Details</caption>
                        <thead>
                            <tr>
                                <th>Bank</th>
                                <th>Account Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>BOC</td>
                                <td>Ran kekulu</td>
                            </tr>
                            <tr>
                                <td>NTB</td>
                                <td>Wanitha</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Land Details</caption>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Firstname</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Anna</td>
                            </tr>                          
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Vehcle Details</caption>
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Model</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Car</td>
                                <td>Honda Civic</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Liability Details</caption>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Description</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>jhkjhjkhkh</td>
                                <td>150000</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover table-condensed table-edit">
                        <caption>Guarantee Details</caption>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Anna</td>
                                <td>Mathara</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Debbie</td>
                                <td>Kandy</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--End table div-->
        </div>
        <div class="col-md-4" style="border-left:1px solid #e2e2e2;border-bottom: 1px solid #e2e2e2;">
            <!--right side menu-->
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12" style="font-size: 13px;">
                    <div class="row" style="background: #98B5CE;  padding: 4px; margin-bottom: 20px;"><label>Loan Details</label></div>
                    <div class="row" >
                        <div class="col-md-4">Amount</div>
                        <div class="col-md-8">158932.00</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Period</div>
                        <div class="col-md-8">36 Months</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Rate</div>
                        <div class="col-md-4">5%</div>
                        <div class="col-md-4"><a href="" >Edit</a></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Installment</div>
                        <div class="col-md-8">5000.00</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Interest</div>
                        <div class="col-md-8">570.00</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Other Charges</div>
                        <div class="col-md-8">350.00</div>
                    </div>
                    <div class="row" style="border-top: 1px solid #222; font-weight: bold;" >
                        <div class="col-md-4">Total</div>
                        <div class="col-md-8">5789.00</div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="row" style="background: #98B5CE;  padding: 4px; margin-bottom: 20px;"><label>Approve Details</label></div>
                    <div class="row">
                        <div class="col-md-4">Level 1</div> 
                        <div class="col-md-4" style="color: green">Passed</div> 
                        <div class="col-md-4"><a href="">View</a></div> 
                    </div>
                    <div class="row">
                        <div class="col-md-4">Level 2</div> 
                        <div class="col-md-4" style="color: red">Reject</div> 
                        <div class="col-md-4"><a href="">View</a></div> 
                    </div>
                    <div class="row">
                        <div class="col-md-4">Level 3</div> 
                        <div class="col-md-4" style="color: orange">Processing</div> 
                        <div class="col-md-4"><a href="">View</a></div> 
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-12" style="height: 190px;; overflow: auto;">
                    <div class="row" style="background: #98B5CE;  padding: 4px; margin-bottom: 20px;"><label>Check List</label></div>
                    <div class='row' style="padding: 5px 10px;">
                        <table>
                            <tr>
                                <td style="width:30%"><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                            <tr>
                                <td><input type='checkbox'/></td>
                                <td>letter of abc</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--End right side menu-->
        </div>
    </div><!--end of details content-->            
</div><!--end of Container-->
<div class="row">
    <div class="col-md-1">
        <input type="button" class="btn-edit btn-group-vertical" onclick="" value="Gurantee"/>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn-edit btn-group-vertical" onclick="" value="Employe"/>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn-edit btn-group-vertical" onclick="" value="Business"/>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn-edit btn-group-vertical" onclick="" value="Asset"/>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn-edit btn-group-vertical" onclick="" value="Liability"/>
    </div>
    <div class="col-md-1">
        <input type="button" class="btn-edit btn-group-vertical" onclick="" value="Dependent"/>
    </div>
</div>
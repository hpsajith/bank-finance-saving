<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row" id="pending_div">
    <div class="col-md-12" style="padding: 1px;">
<!--        <input type="hidden" value="${loanId}" id="hid_loanId"/>-->
        <div class="row" style="padding: 5px; margin: -10 5 30 5; background: #adadad">
            <div class="col-md-2 " style="padding: 1"><strong>Message</strong></div>
            <div class="col-md-4 "></div>
            <div class="col-md-6"></div>
        </div>

        <div class="row" style="margin-top: 15px; padding: 2px 25px">
            <div class="col-md-2 imgWanning" style="margin-top: -5px;"></div>
            <div class="col-md-10 "style="padding: 7 115 2 0 ;">${message}</div>
        </div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-5"></div>
            <div class="col-md-3"><input type="button" onclick="unblockui()" class="btn btn-default col-md-12" value="No"/></div>
            <div class="col-md-3"><input type="button" onclick="conform(${methodType})" class="btn btn-default col-md-12" value="Yes"/></div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <input type="hidden" id="txtLoanId" value="${loanID}"/>
    <input type="hidden" id="txtOrderId" value="${orderID}"/>

</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<!---------methodType------------------
methodType == 1(Remove Loan)
methodType == 2(Seize vehical Key Issue)
methodType == 3(Rebate Loan delete)

--------------------------------------->
<script>
    function conform(type) {
        if (type == 1) {
            var loanId = $("#txtLoanId").val();
            $.ajax({
                url: '/AxaBankFinance/removeLoanSuccess/' + loanId,
                success: function (data) {
                    if (data) {
                        $('#msgDiv').html(data);
                        ui('#msgDiv');
                        setTimeout(
                                function () {
                                    loadRemoveLoans();
                                }, 3000);
                    }
                },
                error: function () {
                    alert("Error Loading...");
                }

            });
        } else if (type == 2) {
            var orderId = $("#txtOrderId").val();
            if (orderId != null) {
                $.ajax({
                    url: "/AxaBankFinance/RecoveryController/keyIssue",
                    type: 'GET',
                    data: {"seizeOrderId": orderId},
                    success: function (data) {
                        $('#msgDiv').html("");
                        $('#msgDiv').html(data);
                        ui('#msgDiv');
                        setTimeout(function () {
                            loadSeizeOrderApprovals();
                        }, 1500);
                    }
                });
            }
        } else if (type == 3) {
            var loanId = $("#txtLoanId").val();
            $.ajax({
                url: '/AxaBankFinance/SettlementController/deleteRebateLoan/' + loanId,
                success: function (data) {
                    $('#message').html('');
                    $('#message').html(data);
                    loadRebateLoanList();
                    ui('#message');
                },
                error: function () {
                    alert("Error Rebate Form Loading..");
                }
            });
        }
    }

</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row" id="pending_div">

    <div class="col-md-12" style="padding: 1px;">
        <div class="row" style="padding: 5px; margin: -10px 5px 30px 5px; background: #adadad">
            <div class="col-md-2 " style="padding: 1px"><strong>Message</strong></div>
            <div class="col-md-4 "></div>
            <div class="col-md-6"></div>
        </div>

        <div class="row" style="margin-top: 15px; padding: 2px 25px">
            <div class="col-md-2 imgWanning" style="margin-top: -5px;"></div>
            <div class="col-md-10 "style="padding: 7px 115px 2px 0px ;">${message}</div>
        </div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-5"></div>
            <div class="col-md-3"><input type="button" onclick="unblockui()" class="btn btn-default col-md-12" value="No"/></div>
            <div class="col-md-3"><input type="button" onclick="conform(${methodType})" class="btn btn-default col-md-12" value="Yes"/></div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <input type="hidden" id="txtLoanId" value="${loanId}"/>
    <input type="hidden" id="txtInsuId" value="${insuId}"/>
    <input type="hidden" id="txtInsuCode" value="${insuCode}"/>

</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<!---------methodType------------------
methodType == 1(Remove Loan)
methodType == 2(Seize vehical Key Issue)
methodType == 3(Rebate Loan delete)

--------------------------------------->
<script>
    function conform(type) {
        if (type === 1) {
            var loanId = $("#txtLoanId").val();
            var insuId = $("#txtInsuId").val();
            var insuCode = $("#txtInsuCode").val();
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/deleteInsurance',
                data: {"loanID": loanId, "insuID": insuId, "insuCode": insuCode},
                success: function(data) {
                    if (data) {
                        $('#msgDiv').html(data);
                        ui('#msgDiv');
                        setTimeout(
                                function() {
                                    loadviewProcessInsurance();
                                }, 3000);
                    }
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        }
    }

</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row" id="pending_div">
    <div class="col-md-12" style="padding: 1px;">
        <input type="hidden" value="${loanId}" id="hid_loanId"/>
        <div class="row" style="padding: 5px; margin: -10 5 30 5; background: #adadad">
            <div class="col-md-2 " style="padding: 1"><strong>Message</strong></div>
            <div class="col-md-4 "></div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" style="margin-top: 15px; padding: 2px 8px">
            <div class="col-md-1 "></div>
            <div class="col-md-10 ">${message}</div>
            <div class="col-md-1 "></div>

        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-5"></div>
            <div class="col-md-3"><input type="button" onclick="unblockui()" class="btn btn-default col-md-12" value="No"/></div>
            <div class="col-md-3"><input type="button" onclick="yesNoAlert(${type})" class="btn btn-default col-md-12" value="Yes"/></div>
            <div class="col-md-1"></div>
        </div>
    </div>

</div>

<script>
    function yesNoAlert(type) {
        var loanId = $("#hid_loanId").val();
        if (type == 1) {
            //pop up pending dates
            $.ajax({
                url: "/AxaBankFinance/loadPendingDate/" + loanId,
                success: function(data) {
                    $("#pending_div").html("");
                    $("#pending_div").html(data);
                }
            });
        } else if (type == 2) {
            //procees for payment page
            loadLoanProcessPage(loanId);
            unblockui();
        }
    }

    function savePendingDates(btbId) {
        var typeId = $("#MPeriodTypes_id").val();
        document.getElementById(btbId).disabled = true;
        $.ajax({
            url: "/AxaBankFinance/savePendingDates/" + typeId,
            data: $("#form_pending").serialize(),
            type: 'POST',
            success: function(data) {
                $("#message_pendingDates").html(data);
                ui("#message_pendingDates");
                var p = $("#hid_pendingstatus").val();
                if (p == 1) {
                    var loanId = $("#hid_pendingLoanId").val();
//                    generateAgreementNo(loanId);
                    viewAllLoans(0);
                }
            }
        });
    }
</script>
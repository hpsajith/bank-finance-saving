<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblRemove").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>

<table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblRemove"  >
    <thead>
        <tr>
            <th>Loan Id</th>
            <th>Member No</th>
            <th>Loan Type</th>
            <th>Debtor Name</th>
            <th>Loan Amount</th>
            <th>Remove</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="loans" items="${loanList}">
            <tr id="${loans[0]}">
                <td>${loans[0]}</td>
                <td>${loans[4]}</td>
                <td>${loans[1]}</td>
                <td>${loans[2]}</td>
                <td>${loans[3]}</td>
                <td><div id="remove${loans[0]}" class="cancel_letter" style="margin-left: 30px" onclick="removeLoan(${loans[0]})"</td>
            </tr>
        </c:forEach>
    </tbody>

</table>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>
    function removeLoan(loanId) {
        $.ajax({
            url: '/AxaBankFinance/removeLoan/' + loanId,
            success: function(data) {
                if (data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    
                }
            },
            error: function() {
                alert("Error Loading...");
            }

        });
    }
</script>



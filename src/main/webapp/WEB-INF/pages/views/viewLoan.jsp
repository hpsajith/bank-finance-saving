<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });

</script>

<h4></h4>
<h6></h6>
<div class="container-fluid">
    <div class="row" style="overflow-y: auto;">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-5">Loan Process Date</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanDate}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Loan End Date</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanLapsDate != null ? loan.loanLapsDate : 'Pending Loan'}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Due Date</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanDueDate}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Down Payment</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanDownPayment}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Investment</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanInvestment}"/></div>
            </div> 
            <div class="row">
                <div class="col-md-5">Loan Amount</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanAmount}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Loan Rate</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanInterestRate}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Period</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanPeriod}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Installment</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanInstallment}"/></div>
            </div>
            <div class="row">
                <div class="col-md-5">Interest</div>
                <div class="col-md-6"><input type="text" name="" value="${loan.loanInterest}"/></div>
            </div>
                <%--<div class="row" style="border-top: 1px solid #222; padding-top: 5px; margin-top: 5px; ">--%>
                    <%--<div class="col-md-5">Total</div>--%>
                    <%--<div class="col-md-6"><input type="text" name="" value="${Total}"/></div>--%>
                <%--</div>--%>
        </div>

        <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 1">
            <table class="table table-striped table-bordered table-hover table-condensed table-edit" id="guranterTable" style="margin-top: 10px;">
                <caption>Debtor Details</caption>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>NIC</th>
                        <th>Address</th>
                        <th>Telephone</th>
                    </tr>
                </thead>
                <tbody>

                    <tr onclick="ViewPopup(${debtorDetails.debtorId})">
                        <td>${debtorDetails.debtorName}</td>
                        <td>${debtorDetails.debtorNic}</td>
                        <td>${debtorDetails.debtorPersonalAddress}</td>
                        <td>${debtorDetails.debtorTelephoneMobile}</td>
                    </tr>

                </tbody>
            </table>

        </div> 

        <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 1">
            <table class="table table-striped table-bordered table-hover table-condensed table-edit" id="guranterTable" style="margin-top: 10px;">
                <caption>Guarantor Details</caption>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>NIC</th>
                        <th>Address</th>
                        <th>Telephone</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="dis" items="${guarantors}">
                        <tr onclick="ViewPopup(${dis.debtId})">
                            <td>${dis.debtName}</td>
                            <td>${dis.debtNic}</td>
                            <td>${dis.debtAddress}</td>
                            <td>${dis.debtTp}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

        </div>
    </div>
</div>
<div id="editCustomer" hidden="true" style="overflow-y: auto;height: 50%;margin-top: 10px; width: 100% "></div>


<script>

    function ViewPopup(id) {
        $.ajax({
            url: '/AxaBankFinance/viewcustomor/' + id,
            success: function(data) {
                $('#editCustomer').html(data);
                ui('#editCustomer');
            }
        });
    }

    function loadLoanEmployementForm() {
        $.ajax({
            url: '/AxaBankFinance/basicEmpForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanBusinessForm() {
        $.ajax({
            url: '/AxaBankFinance/basicBusForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanAssesBankForm() {
        $.ajax({
            url: '/AxaBankFinance/basicAsseForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanLiabilityForm() {
        $.ajax({
            url: '/AxaBankFinance/basicLiaForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanDependentForm() {
        $.ajax({
            url: '/AxaBankFinance/basicDepeForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
</script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row fixed-table">
        <div class="table-content">
            <c:choose>
                <c:when test="${message}">        
                    <table class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header" id="tblCustomer" style="margin-top: 10px;">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>NIC No</th>
                                <th>Address</th>
                                <th>Mobile No</th>
                                <th>Member No</th>
                            </tr>
                        </thead>
                        <tbody>                    
                            <c:forEach var="debtor" items="${debtorList}">
                                <c:choose>
                                    <c:when test="${debtor.debtorAccountNo!=''}">
                                        <tr id="${debtor.debtorId}">
                                        </c:when>
                                        <c:otherwise>
                                        <tr id="${debtor.debtorId}" style="background: #D3D3D3;">
                                        </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                        <c:when test="${debtor.debtorName!=null}">
                                            <td>${debtor.debtorName}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorNic!=null}">
                                            <td>${debtor.debtorNic}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorPersonalAddress!=null}">
                                            <td>${debtor.debtorPersonalAddress}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorTelephoneMobile!=null}">
                                            <td>${debtor.debtorTelephoneMobile}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                            <c:choose>
                                        <c:when test="${debtor.memberNumber!=null}">
                                            <td>${debtor.memberNumber}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td style="display: none;">${debtor.debtorAccountNo}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise><label style="color:red">No Result Found!!!</label></c:otherwise>
            </c:choose>
        </div>
    </div>
</div>


<script>
    //click table row

    $(function () {
        $('#tblCustomer tbody').find('tr').click(function () {
            var debid = $(this).attr("id");
            // alert('You clicked row '+ ($(this).index()+1) +"--"+did);            

            var tableRow = $(this).children("td").map(function () {
                return $(this).text();
            }).get();
            name = $.trim(tableRow[0]);
            nic = $.trim(tableRow[1]);
            address = $.trim(tableRow[2]);
            telephone = $.trim(tableRow[3]);
            accNo = $.trim(tableRow[4]);
            memberNo = $.trim(tableRow[4]);


            dId = debid;
            $.ajax({
                url: '/AxaBankFinance/searchLoanById/' + debid,
                success: function (data) {
                    $('#searchedLoans').html(data);
                },
                error: function () {
                    alert("Error Loading...");
                }
            });

        });
    })

</script>

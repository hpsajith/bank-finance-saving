<%-- 
    Document   : instalmentSchedule
    Created on : Apr 6, 2016, 4:27:32 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        $("#tblInstalmentSchedule").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>

<div class="container-fluid" style="padding: 1px">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Instalment Schedule</strong>
    </div>
    <div class="row">
        <!--<div class="col-md-2" style="margin-top: 5px;width: 20%"><label id="lblInst" style="font-weight: bold; font-size: 25px">Total Amount </label></div>-->
        <!--<div class="col-md-5"><input type="text" id="txtTotalAmount" readonly="" value="${totalAmount}" style="font-weight: bold; font-size: 30px;color: #0088cc"/></div>-->
        <div class="col-md-6" style="text-align: left;margin-top: 5px"><label style="font-weight: bold; font-size: 20px" class="msgTextField"id="msg"></label></div>
        <div class="col-md-1"></div>
    </div>
    <form id="instalmentSchedule" name="formInstalmentSchedule">
        <div class="row">
            <div class="col-md-5">
                <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblInstalmentSchedule"  >
                    <thead>
                        <tr>
                            <th>Month</th>
                            <th>Instalment Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="insList" items="${insScheduleList}">
                            <tr id="${insList.insMonth}">
                                <td>${insList.insMonth}</td>
                                <td><input type="text" name="monthlyInstallment"   value="${insList.monthlyInstallment}" id="insAmount${insList.insMonth}" style="width: 100%"/></td>  
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="col-md-7" style="overflow-y: auto" id="divScheduleList"></div>
        </div>
        <input type="hidden" name="loanId" id="txtLoanId" value="${loanID}">
        <input type="hidden" id="txtRealValue" value="${totalAmount}">
        <input type="hidden" id="txtloanStatus" value="${loanStatus}">
        <div id="hideInstalmentAmount"></div>
        <div id="hideInstalmentMonth"></div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
    <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="viewAllLoans(0)"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Clear Schedule" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="clearSchedule(${loanID})"/></div>
            <c:choose>
                <c:when test="${loanStatus == 0}">
                <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btncreateSchedule" value="Create Schedule" onclick="createSchedule()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                </c:when>
                <c:otherwise>
                <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btncreateSchedule1" value="Create Schedule" disabled="" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                </c:otherwise>
            </c:choose> 
                <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" onclick="viewScheduleList(${loanID})" id="btnSaveSchedule" value="Save Instalment Schedule"  class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>
    $(function () {
        var loanStatus = $("#txtloanStatus").val();
        if (loanStatus > 0) {
            $("#msg").html("Can't Create Instalment Schedule");
        }
    });

//    function calculateTotal(id) {
//        var insAmount = $("#insAmount" + id).val();
//        var totalAmount = $("#txtTotalAmount").val();
//        totalAmount = totalAmount.replace(',', '');
//        totalAmount = parseInt(totalAmount);
//        var rental = 0.00;
//        rental = parseInt(insAmount);
//        var subTotal = totalAmount - rental;
//
//        if (subTotal < -1) {
//            $("#msg").html("Total Amount expired");
//            $("#insAmount" + id).val(0.00);
//            $("#txtTotalAmount").val(0.00);
//        }
//        $("#txtTotalAmount").val(subTotal);
//    }

    function createSchedule() {
        document.getElementById("btncreateSchedule").disabled = true;
        setInstalmentAmounts();
        $.ajax({
            url: '/AxaBankFinance/createSchedule',
            type: 'post',
            data: $('#instalmentSchedule').serialize(),
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');

                loanInsSchedule($("#txtLoanId").val());
                document.getElementById("btncreateSchedule").disabled = false;
                $('#divScheduleList').html("");


            },
            error: function () {
                alert("Can't Update Instalment Schedule..!");
            }
        });
    }

    function clearSchedule(id) {
        $.ajax({
            url: '/AxaBankFinance/clearInstallmentSchedule/' + id,
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                loanInsSchedule(id);
            },
            error: function () {
                alert("Can't Clear Instalment Schedule..!"); 
            }
        });
    }

    function setInstalmentAmounts() {
        $('#tblInstalmentSchedule tbody tr').each(function () {
            var insId = $(this).attr('id');
            var insAmount = $('#insAmount' + insId).val();
            $("#hideInstalmentMonth").append("<input type = 'hidden' name = 'instalmentMonth' value = '" + insId + "'/>");
            $("#hideInstalmentAmount").append("<input type = 'hidden' name = 'instalmentAmount' value = '" + insAmount + "'/>");
        });
    }

    function viewScheduleList(loanId) {
        $.ajax({
            url: '/AxaBankFinance/viewInsScheduleList/' + loanId,
            success: function (data) {
                $('#divScheduleList').html("");
                $('#divScheduleList').html(data);
            },
            error: function () {
                alert("Can't View Instalment Schedule..!");
            }
        });
    }
</script>
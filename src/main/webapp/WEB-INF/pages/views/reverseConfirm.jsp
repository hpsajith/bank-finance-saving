<div class="row" style="padding: 20px; border: 2px solid #5e5e5e;">
    <div class="col-md-12">
        <div class="row">
            <label style="text-align: left">Do You Want to Reverse This Loan!!</label>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-2">Comment</div>
            <div class="col-md-4"><textarea id="reversComment"></textarea></div>
        </div>
        <input type="hidden" value="${loanId}" id="loan_id_revers"/>
        <input type="hidden" value="${type}" id="type_revers"/>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-3"><input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"/></div>
            <div class="col-md-3"><input type="button" class="btn btn-default col-md-12" value="Save" onclick="reverseLoan()"/></div>            
        </div>
    </div>
</div>

<script>
    function reverseLoan() {
        var loanId = $("#loan_id_revers").val();
        var type = $("#type_revers").val();
        var comment = $("#reversComment").text();
        $.ajax({
            url: '/AxaBankFinance/reversLoan',
            data: {loanId: loanId, type: type, comment: comment},
            success: function(data) {
                unblockui();
                $('#message_dialog').html(data);
                ui("#message_dialog");
//                if(type==1){
//                    viewAllLoans(0);
//                }
//                else{
//                    viewAllLoans(1);
//                }
            },
            error: function() {
            }
        });

    }
</script>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblViewLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>

<c:if test="${type == 0}">
    <label style="color: #08C; font-size: 14px"> Pending Loans</label>
</c:if>
<c:if test="${type == 1}">
    <label style="color: #08C; font-size: 14px"> Approved Loans </label>
</c:if>
<c:if test="${type == 3}">
    <label style="color: #08C; font-size: 14px"> Active Loans </label>
</c:if>

<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
    <div class="col-md-1">Start Date</div>
    <div class="col-md-2"><input type="text" id="txt_startDate" class="txtCalendar" value="${sdate}"></div>
    <div class="col-md-1">End Date</div>
    <div class="col-md-2"><input type="text" id="txt_endDate" class="txtCalendar" value="${edate}"/></div>
    <div class="col-md-1"><input type="button" class="btn btn-default searchButton" id="btnSearchLoan"
                                 style="height: 25px"></div>
    <input type="hidden" value="${type}" id="txt_isissue"/>
</div>


<div class="row">
    <div id="groupComponent">
        <div class="col-md-1" style="font-weight: bold">Branch</div>
        <div class="col-md-3">

            <select name="branchID" style="width: 172px" id="branchID">
                <option value="0">-select branch-</option>
                <c:forEach var="branch" items="${branchList}">
                    <c:choose>
                        <c:when test="${branch.branchId == group.branchID}">
                            <option selected="selected" value="${branch.branchId}">${branch.branchName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${branch.branchId}">${branch.branchName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>

            <label id="msgBranchId" class="msgTextField"></label>
        </div>

        <div class="col-md-1" style="font-weight: bold">Center</div>
        <div class="col-md-3">
            <div id="loadingImg1"><img src="/AxaBankFinance/resources/img/loading_small.gif" alt="loading..."></div>
            <div id="centerDropDown">----</div>

            <label id="msgCenterId" class="msgTextField"></label>
        </div>

        <div class="col-md-1" style="font-weight: bold">Group</div>
        <div class="col-md-3">

            <div id="loadingImg2"><img src="/AxaBankFinance/resources/img/loading_small.gif" alt="loading..."></div>
            <div id="groupDropDown">----</div>

            <label id="msgCenterId" class="msgTextField"></label>
        </div>
    </div>
</div>
<!--<hr>-->

<div class="row" style="padding-top: 10px">


    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0"
           id="tblViewLoan">
        <thead>
        <tr>
            <th style="text-align: center">#</th>
            <th style="text-align: center">No</th>
            <th style="text-align: center">Name</th>
            <th style="text-align: center">Loan</th>
            <th style="text-align: center">Loan Date</th>
            <th style="text-align: center">Due Date</th>
            <th style="text-align: center">Type</th>
            <th style="text-align: center">Period</th>
            <th style="text-align: center">Rental</th>
            <th style="text-align: center">Documents</th>
            <c:forEach var="approve" items="${approval}">
                <th style="text-align: center">${approve.appDescription}</th>
            </c:forEach>
            <th style="text-align: center">Status</th>
            <th style="text-align: center">Branch</th>
            <th style="text-align: center">Group</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="loan" items="${loanList}">
            <tr>
                <td style="text-align: center"><input type="checkbox" ${loan.loanStatus==2?'disabled':''}
                                                      class="loanCheckBox" id="loanCheck${loan.loanId}"
                                                      onclick="selectLoan(${loan.loanId})"/></td>
                <c:choose>
                    <c:when test="${loan.loanIsIssue==0}">
                        <td style="text-align: center">${loan.loanId}</td>
                    </c:when>
                    <c:otherwise>
                        <td style="text-align: left">${loan.agreementNo} </br>
                            ------------------------- </br> ${loan.loanBookNo}</td>
                    </c:otherwise>
                </c:choose>
                <td style="text-align: center">${loan.debtorHeaderDetails.debtorName}</td>
                <td style="text-align: right">${loan.loanAmount}</td>
                <td style="text-align: center">${loan.loanDate}</td>
                <td style="text-align: center">${loan.dueDate}</td>
                <td style="text-align: center">${loan.loanType}</td>
                <td style="text-align: center">${loan.loanPeriod}</td>
                <td style="text-align: right">${loan.loanInstallment}</td>

                <!--Document submission status-->
                <c:choose>
                    <c:when test="${loan.loanDocumentSubmission==1}">
                        <td style="text-align: center"><a onclick="submitDocuments(${loan.loanId})">Not Complete</a>
                        </td>
                    </c:when>
                    <c:when test="${loan.loanDocumentSubmission==2}">
                        <td style="text-align: center"><a onclick="submitDocuments(${loan.loanId})">Complete</a></td>
                    </c:when>
                    <c:when test="${loan.loanDocumentSubmission==3}">
                        <td style="text-align: center"><a onclick="submitDocuments(${loan.loanId})">Pending</a></td>
                    </c:when>
                </c:choose>

                <!--Approve levels-->
                <!--check user approvals-->
                <c:set var="app1" value="false"></c:set>
                <c:set var="app2" value="false"></c:set>
                <c:set var="app3" value="false"></c:set>
                <c:forEach var="userApprove" items="${userApproval}">
                    <c:choose>
                        <c:when test="${userApprove.MApproveLevels.appId==1 && userApprove.userId==loan.userId && loan.empId==loan.userApproval.app_id1}">
                            <c:set var="app1" value="true"></c:set>
                        </c:when>
                        <c:when test="${userApprove.MApproveLevels.appId==2 && userApprove.userId==loan.userId && loan.empId==loan.userApproval.app_id2}">
                            <c:set var="app2" value="true"></c:set>
                        </c:when>
                        <c:when test="${userApprove.MApproveLevels.appId==3 && userApprove.userId==loan.userId && loan.empId==loan.userApproval.app_id3}">
                        <c:set var="app3" value="true"></c:set>
                        </c:when>
                    </c:choose>
                </c:forEach>
                <c:set var="approve1" value="false"></c:set>
                <c:set var="approve2" value="false"></c:set>
                <c:set var="approve3" value="false"></c:set>
                <c:forEach var="loanApp" items="${loan.loanApprovals}">
                    <c:choose>
                        <c:when test="${loanApp.loanId eq loan.loanId}">
                            <c:choose>
                                <c:when test="${loanApp.approvedLevel==1}">
                                    <c:set var="approve1" value="true"></c:set>
                                </c:when>
                                <c:when test="${loanApp.approvedLevel==2}">
                                    <c:set var="approve2" value="true"></c:set>
                                </c:when>
                                <c:when test="${loanApp.approvedLevel==3}">
                                    <c:set var="approve3" value="true"></c:set>
                                </c:when>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <c:set var="approve1" value="false"></c:set>
                            <c:set var="approve2" value="false"></c:set>
                            <c:set var="approve3" value="false"></c:set>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <!--///////-->

                <!--approval 1-->
                <c:choose>
                    <c:when test="${approve1}">
                        <td style="text-align: center">Approved<br/>${app_date}</td>
                    </c:when>
                    <c:when test="${loan.userApproval.app_id1==0}">
                        <td style="text-align: center">-</td>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${loan.userApproval.app_id1==0}">
                                <td style="text-align: center">-</td>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${loan.loanApprovalStatus<2}">
                                        <c:choose>
                                            <c:when test="${app1}">

                                                <td style="text-align: center"><a href="#"
                                                                                  onclick="editApproval(${loan.loanId}, 1)">not
                                                    approved</a></td>
                                            </c:when>
                                            <c:otherwise>
                                                <td style="text-align: center">
                                                    Pending Approva<br/>${loan.userApproval.app_name1}
                                                    </td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>

                                    <c:otherwise>

                                        <c:forEach var="loanApproval" items="${loan.loanApprovals}">
                                            <c:if test="${loanApproval.approvedLevel==1}">
                                                <c:set var="app_date" value="${loanApproval.approvedDate}"></c:set>
                                            </c:if>
                                        </c:forEach>

                                        <c:choose>
                                            <c:when test="${app1}">
                                                <c:if test="${type == 0 || type == 1}">
                                                    <td style="text-align: center"><a href="#"
                                                                                      onclick="editApproval(${loan.loanId}, 1)">Approved</a><br/>${app_date}
                                                    </td>
                                                </c:if>
                                                <c:if test="${type == 3}">
                                                    <td style="text-align: center">Approved<br/>${app_date}</td>
                                                </c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <c:choose>
                                                    <c:when test="${app2 || app3}">
                                                        <td style="text-align: center"><a href="#"
                                                                                          onclick="viewApproval(${loan.loanId}, 1)">Approved</a><br/>${app_date}
                                                        </td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td>Approved<br/>${app_date}</td>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>

                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
                <!--approval 2-->

                <c:choose>
                    <c:when test="${approve2}">
                        <td style="text-align: center">Approved<br/>${app_date}</td>
                    </c:when>
                    <c:when test="${loan.userApproval.app_id2==0}">
                        <td style="text-align: center">-</td>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${approve1 || loan.userApproval.app_id1==0}">
                                <c:choose>
                                    <c:when test="${loan.userApproval.app_id2==0}">
                                        <td style="text-align: center">-</td>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${loan.loanApprovalStatus<3}">
                                                <c:choose>
                                                    <c:when test="${app2}">
                                                        <td style="text-align: center"><a href="#"
                                                                                          onclick="editApproval(${loan.loanId}, 2)">not
                                                            approved</a></td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td style="text-align: center">
                                                            Pending Approval<br/>${loan.userApproval.app_name2}</td>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="loanApproval" items="${loan.loanApprovals}">
                                                    <c:if test="${loanApproval.approvedLevel==2}">
                                                        <c:set var="app_date"
                                                               value="${loanApproval.approvedDate}"></c:set>
                                                    </c:if>
                                                </c:forEach>
                                                <c:choose>
                                                    <c:when test="${app2}">
                                                        <c:if test="${type == 0 || type == 1}">
                                                            <td style="text-align: center"><a href="#"
                                                                                              onclick="editApproval(${loan.loanId}, 2)">Approved</a><br/>${app_date}
                                                            </td>
                                                        </c:if>
                                                        <c:if test="${type == 3}">
                                                            <td style="text-align: center">Approved<br/>${app_date}</td>
                                                        </c:if>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:choose>
                                                            <c:when test="${app3}">
                                                                <td style="text-align: center"><a href="#"
                                                                                                  onclick="viewApproval(${loan.loanId}, 2)">Approved</a><br/>${app_date}
                                                                </td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td style="text-align: center">Approved<br/>${app_date}
                                                                </td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>

                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <td style="text-align: center">Pending Approval<br/>${loan.userApproval.app_name2}</td>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>


                <!--approval 3-->
                <c:choose>
                    <c:when test="${approve3}">
                        <td style="text-align: center">Approved<br/>${app_date}</td>
                    </c:when>
                    <c:otherwise>

                        <c:choose>
                            <c:when test="${approve2 || (loan.userApproval.app_id2==0 && approve1) || (loan.userApproval.app_id1==0 && loan.userApproval.app_id2==0)}">
                                <c:choose>
                                    <c:when test="${loan.userApproval.app_id3==0}">
                                        <td style="text-align: center">-</td>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${loan.loanApprovalStatus<4}">
                                                <c:choose>
                                                    <c:when test="${app3}">
                                                        <td style="text-align: center"><a href="#"
                                                                                          onclick="editApproval(${loan.loanId}, 3)">not
                                                            approved</a></td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td style="text-align: center">
                                                            Pending Approval <br/>${loan.userApproval.app_name3}</td>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>

                                            <c:otherwise>
                                                <c:forEach var="loanApproval" items="${loan.loanApprovals}">
                                                    <c:if test="${loanApproval.approvedLevel==3}">
                                                        <c:set var="app_date"
                                                               value="${loanApproval.approvedDate}"></c:set>
                                                    </c:if>
                                                </c:forEach>
                                                <c:choose>
                                                    <c:when test="${app3}">
                                                        <c:if test="${type == 0 || type == 1}">
                                                            <td style="text-align: center"><a href="#"
                                                                                              onclick="editApproval(${loan.loanId}, 3)">Approved</a><br/>${app_date}
                                                            </td>
                                                        </c:if>
                                                        <c:if test="${type == 3}">
                                                            <td style="text-align: center">Approved<br/>${app_date}</td>
                                                        </c:if>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td style="text-align: center">Approved<br/>${app_date}</td>
                                                    </c:otherwise>

                                                </c:choose>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>

                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <td style="text-align: center">Pending Approval<br/>${loan.userApproval.app_name3}</td>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>

                <!------------------>

                <!--loan status-->
                <c:choose>
                    <c:when test="${loan.loanStatus==0}">
                        <td style="text-align: center"><label style="color: orange">Processing</label></td>
                    </c:when>
                    <c:when test="${loan.loanStatus==1}">
                        <td style="text-align: center"><label style="color: green">Approved</label></td>
                    </c:when>
                    <c:when test="${loan.loanStatus==2}">
                        <td style="text-align: center"><label style="color: red">Reject</label></td>
                    </c:when>
                </c:choose>
                <!--Branch Code-->
                <c:forEach var="branches" items="${branch}">
                    <c:if test="${branches.branchId == loan.branchId}">
                        <td style="width: 40px;text-align: center">${branches.branchName}</td>
                    </c:if>
                </c:forEach>

                <td style="text-align: right">${loan.group}</td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<!-- Approve Level 1-->
<div class="searchCustomer" style="width:40%; margin: 5% 10%" id="popup_approval">

</div>

<!-- btn Panel-->
<div class="row" style="margin-top: 25px;">
    <c:choose>
        <c:when test="${type==0}">
            <div class="col-md-3"><input type="button" id="btnInstalmentSchedule" value="Instalment Schedule"
                                         class="btn btn-default btnLoans col-md-12"></div>
        </c:when>
        <c:otherwise>
            <div class="col-md-3"><input type="button" id="btnInstalmentSchedule" value="Instalment Schedule"
                                         class="btn btn-default btnLoans col-md-12" style="display: none"></div>
        </c:otherwise>
    </c:choose>
    <div class="col-md-2"><input type="button" id="btnViewMandate" value="View Mandate"
                                 class="btn btn-default btnLoans col-md-12" onclick=""></div>
    <!--<div class="col-md-2"><input type="button" id="btnViewProfile" value="Profile" class="btn btn-default btnLoans col-md-12" onclick=""></div>-->
    <div class="col-md-2"><input type="button" id="btnViewLoan" value="View Loan"
                                 class="btn btn-default btnLoans col-md-12" onclick=""></div>

    <c:choose>
        <c:when test="${type==0}">
            <c:choose>
                <c:when test="${app3}">
                    <div class="col-md-2"><input type="button" id="btnReversLoan" value="Reverse"
                                                 class="btn btn-default btnLoans col-md-12" onclick=""></div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-2"><input type="button" id="btnReversLoan" value="Reverse"
                                                 class="btn btn-default btnLoans col-md-12" disabled=""></div>
                </c:otherwise>
            </c:choose>
            <div class="col-md-2"><input type="button" id="btnProcess" value="Process"
                                         class="btn btn-default btnLoans col-md-12" onclick="processApprovedLoan()">
            </div>
        </c:when>
        <c:when test="${type==1}">
            <c:choose>
                <c:when test="${app3}">
                    <div class="col-md-2"><input type="button" id="btnReversApprovLoan" value="Reverse"
                                                 class="btn btn-default btnLoans col-md-12" onclick=""></div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-2"><input type="button" id="btnReversApprovLoan" value="Reverse"
                                                 class="btn btn-default btnLoans col-md-12" disabled=""></div>
                </c:otherwise>
            </c:choose>
            <div class="col-md-2"><input type="button" id="btnPayments" value="Payments"
                                         class="btn btn-default btnLoans col-md-12" onclick=""></div>
        </c:when>
    </c:choose>


</div>
<form method="GET" action="/AxaBankFinance/generateMandateReport" target="blank" id="formPDFMandateView">
    <input type="hidden" name="mandateLoanId" id="txtMandateLoanId" value=""/>
</form>
<div id="docDivCheck" class="searchCustomer" style="width: 85%;border: 0px solid #5e5e5e;margin-left: -15%;"></div>
<div id="message_dialog" class="searchCustomer"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="message_dueDate" class="searchCustomer"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;">
    <div class="row">
        <div class="col-md-12">
            <div class="row">Enter Loan Due Date</div>
            <div class="row">
                <span class="ui-helper-hidden-accessible"><input type="text"/></span>
            </div>
            <div class="row">
                <div class="col-md-3"><label>Due Date</label></div>
                <div class="col-md-8"><input type="text" id="dueDatetxt" class="txtCalendar"/></div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button" class="btn btn-default col-md-12" value="Cancel"
                                             onclick="unblockui()"/></div>
                <div class="col-md-4"><input type="button" class="btn btn-default col-md-12" value="Save"
                                             onclick="addDueDate()"/></div>
            </div>
        </div>
    </div>
</div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..."/>
</div>
<script>

    $(document).ready(function () {
        $(".btnLoans").prop("disabled", true);
        $("#loadingImg1").hide();
        $("#loadingImg2").hide();

        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });

        $('#btnSearchLoan').click(function () {
            var type = $("#txt_isissue").val();
            var startDate = $("#txt_startDate").val();
            var endDate = $("#txt_endDate").val();
            ui('#proccessingPage');
            $.ajax({
                data: {start: startDate, end: endDate},
                url: '/AxaBankFinance/viewAllLoans/' + type,
                success: function (data) {
                    $('#formContent').html(data);
                }
            });
        });
    });

    //load document submission form
    function submitDocuments(loanId) {
        $.ajax({
            url: "/AxaBankFinance/loadDocumentChecking",
            data: {loan_id: loanId},
            success: function (data) {
                $("#docDivCheck").html(data);
                ui('#docDivCheck');
            }
        });
    }

    //load approval pop
    function popUpApproval(form) {


    }

    function editApproval(loanId, appId) {
        var form = new FormData();
        form.append("loanId", loanId);
        form.append("appLevelId", appId);
        form.append("type", 1);

        $.ajax({
            url: "/AxaBankFinance/recoveryReportEdit?${_csrf.parameterName}=${_csrf.token}",
            type: 'POST',
            data: form,
            processData: false,
            contentType: false,
            success: function (data) {
                //                $("input[name=loanId]").val(loanId);
                $("#popup_approval").html("");
                $("#popup_approval").html(data);
                ui('#popup_approval');
            }
        });
    }

    function viewApproval(loanId, appId) {
        var form = new FormData();
        form.append("loanId", loanId);
        form.append("appLevelId", appId);
        form.append("type", 2);
        $.ajax({
            url: "/AxaBankFinance/recoveryReportEdit?${_csrf.parameterName}=${_csrf.token}",
            type: 'POST',
            data: form,
            processData: false,
            contentType: false,
            success: function (data) {
                $("#popup_approval").html("");
                $("#popup_approval").html(data);
                ui('#popup_approval');
            }
        });
    }

    var LOANID;
    function selectLoan(loanId) {
        if ($('#loanCheck' + loanId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#loanCheck' + loanId).prop('checked', true);
            $(".btnLoans").prop("disabled", false);
            LOANID = loanId;
        } else {
            $(".btnLoans").prop("disabled", true);
        }
    }
    //reverse button action
    $("#btnReversLoan").click(function () {

        var id = LOANID;
        loadReverseConfirmation(id, 1);
    });

    //reverse button for approved loan
    $("#btnReversApprovLoan").click(function () {

        var id = LOANID;
        loadReverseConfirmation(id, 2);
    });

    //view mandate
    $("#btnViewMandate").click(function () {
        var id = LOANID;
        $("#txtMandateLoanId").val(id);
        $("#formPDFMandateView").submit();
    });

    function loadReverseConfirmation(id, type) {
        $.ajax({
            url: '/AxaBankFinance/loadReversConfirm',
            data: {id: id, type: type},
            success: function (data) {
                $('#message_dialog').html(data);
                ui("#message_dialog");
            },
            error: function () {
            }
        });
    }

    //view loan button action
    $("#btnViewLoan").click(function () {
        var id = LOANID;
        $.ajax({
            url: '/AxaBankFinance/viewLoanDetails/' + id,
            success: function (data) {
                $('#formContent').html(data);

            },
            error: function () {
            }
        });
    });


    //Instalment Schedule action
    $("#btnInstalmentSchedule").click(function () {
        var id = LOANID;
        loanInsSchedule(id);
    });

    function loanInsSchedule(id) {
        $.ajax({
            url: '/AxaBankFinance/viewInstalmentSchedule/' + id,
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });

    }


    //proccess button action
    function processApprovedLoan() {
        var id = LOANID;
        var due = $("#dueDatetxt").val();

        if (due === "") {
            due = "NO";
        }
        var view = "message_dialog";
        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function (data) {
                if (!data) {
                    $.ajax({
                        url: "/AxaBankFinance/loanProcessForPayments/" + id + "/" + due,
                        date: {due: due},
                        success: function (data) {
                            if (data === 0) {
                                //loan is rejected
                                var message = "Loan has been rejected!!!";
                                warning(message, view);
                            } else if (data === 1) {
                                //user has no approval
                                var message = "You have no Authorization to proceed!!!";
                                warning(message, view);
                            } else if (data === 2) {
                                //loan is not approved
                                var message = "Loan is not approved!!!";
                                warning(message, view);
                            } else if (data === 3) {
                                //loan is proceed for payments
                                var message = "Loan is proceeded for payments.";
                                //                    success(message,view);
                                viewAllLoans(1);
                            } else if (data === 5) {
                                //payments are not complete
                                var message = "Create Payble Account";
                                warning(message, view);
                            } else if (data === 6) {
                                //add due date
                                ui("#message_dueDate");
                            } else if (data === 7) {
                                var message = "Insurance Must Be Add Before Process";
                                warning(message, view);
                            } else {
                                //loan is already proceeded
                                var message = "This loan is already proceeded for payments";
                                success(message, view);
                            }
                        },
                        error: function () {

                        }
                    });
                } else {
                    warning("User Is Blocked", view);
                }
            },
            error: function () {
                alert("Connection is refuced by the server...");
            }
        });
    }

    //make payments
    $("#btnPayments").click(function () {
        var id = LOANID;
        var view = "message_dialog";
        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function (data) {
                if (!data) {
                    loadLoanProcessPage(id);
                } else {
                    warning("User Is Blocked", view);
                }
            },
            error: function () {
                alert("Connection is refuced by the server...");
            }
        });
    });

    function loadLoanProcessPage(id) {
        $.ajax({
            url: '/AxaBankFinance/beforeProcessLoan/' + id,
            success: function (data) {
                if (data == "OK") {
                    processLoan(id);
                } else {
                    warning(data, "message_dialog");
                }
            },
            error: function () {
            }
        });
    }
    function addDueDate() {
        var due = $("#dueDatetxt").val();
        var id = LOANID;
        $.ajax({
            url: "/AxaBankFinance/addDueDate/" + id + "/" + due,
            success: function (data) {
                if (data == 1) {
                    processApprovedLoan();
                } else {
                    warning("Error Saving Due Date", "message_dialog");
                }
            },
            error: function () {
            }
        });
    }
    function processLoan(id) {
        $.ajax({
            url: '/AxaBankFinance/processLoan/' + id,
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function generateAgreementNo(id) {
        $.ajax({
            url: '/AxaBankFinance/generateAgreementNo/' + id,
            success: function (data) {
                //                alert(data);
            },
            error: function () {
            }
        });
    }


    $("#branchID").change(function () {

        var branchId = $("#branchID").val();
        $('#loanTypeGroup').hide("slow");

        $.ajax({
            url: "/AxaBankFinance/loadCenter/" + branchId,
            beforeSend: function (xhr) {
                $("#loadingImg1").show();
                $("#centerDropDown").html("");
            },
            success: function (data) {
                $("#centerDropDown").html("");
                $("#centerDropDown").html(data);
                $("#loadingImg1").hide();
            }
        });

    });


    function getGroup() {

        var centeID = $("#centeID").val();
        $('#loanTypeGroup').hide("slow");

        $.ajax({
            url: "/AxaBankFinance/loadGroup/" + centeID,
            beforeSend: function (xhr) {
                $("#loadingImg2").show();
                $("#groupDropDown").html("");
            },
            success: function (data) {
                $("#loadingImg2").hide();
                $("#groupDropDown").html("");
                $("#groupDropDown").html(data);
            }
        });
    }


</script>
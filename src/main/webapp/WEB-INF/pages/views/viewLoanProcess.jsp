<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblInstalments").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "58%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });
</script>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<div class="container-fluid">
    <div class="row" style="margin-bottom: 15px;">        
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-5" style="font-weight: bold">Agreement No</div>
                <div class="col-md-7"><input type="text" name="" value="${agreementNo}" style="text-align: left;width: 100%" readonly/></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-5" style="font-weight: bold">Member No</div>
                <div class="col-md-7"><input type="text" name="" value="${memberNo}" style="text-align: left;width: 100%" readonly/></div>
            </div>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4" style="text-align: right">
            <c:choose>
                <c:when test="${loanStatus==0}">
                    <div class="col-md-12" style="text-align: center;background: #F6CECE;width: 100%">
                        <label style="font-size: 20px; font-family: times new roman">${message}</label>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-12" style="text-align: center;background: #BEF781;width: 100%">
                        <label style="font-size: 20px; font-family: times new roman">${message}</label>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="row" style="border: 1px solid #0088cc; margin-top: 8px;">
                <div class="row" style="margin: 10px;">
                    <div class="row"><div class="col-md-12"><strong>Loan Details</strong></div></div>
                    <input type="hidden" name="hidLoanId" value="${loan.loanId}" />
                    <div class="row">
                        <div class="col-md-5">Loan Process Date</div>
                        <div class="col-md-6"><input type="text" name="" value="${loan.loanStartDate}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div    class="row">
                        <div class="col-md-5">Due Date</div>
                        <div class="col-md-6"><input type="text" name="" value="${loanDueDate}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Loan End Date</div>
                        <div class="col-md-6"><input type="text" name="" value="${loanEndDate}" style="text-align: right" readonly="true"/></div>
                    </div>                    
                    <div class="row">
                        <div class="col-md-5">Loan Amount</div>
                        <div class="col-md-6"><input type="text" name="" value="${pay.loanAmount}" id="viewLoanAmount" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Capitalize Amount</div>
                        <div class="col-md-6"><input type="text" name="" value="${pay.capitalizeAmount}" id="viewCapitalizeAmount" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Down Payment</div>
                        <div class="col-md-6"><input type="text" name="" value="${pay.downPayment}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Investment</div>
                        <div class="col-md-6"><input type="text" name="" value="${pay.investment}" style="text-align: right" readonly="true"/></div>
                    </div> 
                    <div class="row">
                        <div class="col-md-5">Insurance Charge</div>
                        <div class="col-md-6"><input type="text" name="" value="${insuCharge}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Loan Rate</div>
                        <div class="col-md-6"><input type="text" name="" value="${loan.loanInterestRate}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Period</div>
                        <div class="col-md-6"><input type="text" name="" value="${loan.loanPeriod}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Installment</div>
                        <div class="col-md-6"><input type="text" name="" value="${pay.installment}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Interest</div>
                        <div class="col-md-6"><input type="text" name="" value="${pay.interest}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Total Interest</div>
                        <div class="col-md-6"><input type="text" name="" value="${pay.totalInterest}" style="text-align: right" readonly="true"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">Other Charges</div>
                        <div class="col-md-4"><input type="text" name="" value="${pay.othercharges}" style="text-align: right" readonly="true"/></div>
                        <div class="col-md-3"><button class="btn btn-default col-md-12" onclick="viewOtherCharges()" style="margin-left: 10px;">View</button></div>
                    </div>
                    <div class="row" style="border-top: 1px solid #222; padding-top: 5px; margin-top: 5px; ">
                        <div class="col-md-5" style="font-weight: bold;">Issue Amount</div>
                        <div class="col-md-6"><input type="text" name="" value="${issuAmount}" id="issueAmount_id" style="text-align: right; font-weight: bold" readonly="true"/></div>
                    </div>
                    <input type="hidden" value="${other}" id="withOthercharges_id"/>
                    <input type="hidden" value="${loan.loanInvestment}" id="withoutOthercharges_id"/>

                    <div class="row" style="margin-top: 20px">
                        <c:choose>
                            <c:when test="${isPaid}">
                                <div class="col-md-1"><input type="checkbox" name="" id="checkOtherPayment" checked="true" disabled="true"/></div>
                                <div class="col-md-4">Advanced Payment</div>
                                <div class="col-md-3"></div>
                                <div class="col-md-3"></div>
                                <div class="col-md-1"></div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-1"><input type="checkbox" name="" id="checkOtherPayment" disabled="true"/></div>
                                <div class="col-md-4">Advanced Payment<input type="hidden" value="${advance}" id="txtAdvance"/></div>
                                <div class="col-md-5">
                                    <c:choose>
                                        <c:when test="${voucher!=''}">
                                            <input type="button" class="btn btn-default col-md-12" id="btnPrintChq" value="Print Cheque" onclick="printAdvancePayment('${voucher}')"/>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="button" class="btn btn-default col-md-12" id="btnPrintChq" value="Print Cheque" disabled="true"/>
                                        </c:otherwise>
                                    </c:choose>                                    
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row" style="border: 1px solid #0075b0;margin-top: 10px; display: none; background: #F2F7FC;" id="formCash">
                <div class="row" style="margin: 10px">
                    <div class="col-md-3">Amount</div>
                    <div class="col-md-6"><input type="text" id="txtfinalLoanAmount" /></div>
                </div>
            </div>
        </div>
        <div class="col-md-7" style="margin-left: 0px;">
            <div class="row">
                <div class="col-md-12">
                    <table id="tblInstalments"class="dc_fixed_tables table-bordered" style="text-align: right; width: 100%">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Interest</th>
                                <th>Principle</th>
                                <th>Installment</th>
                                <th>Payment</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>                        
                            <c:forEach var="installment" items="${insList}">
                                <tr>
                                    <td>${installment.dueDate}</td>
                                    <td>${installment.insInterest}</td>
                                    <td>${installment.insPrinciple}</td>
                                    <td>${installment.insAmount}</td>
                                    <td>${installment.insRoundAmount}</td>
                                    <td>${installment.loanBalance}</td>   
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-8" style="color: red; font-weight: bold;">
                        Excess Amount: ${loss}
                    </div>                   
                </div>
            </div>
        </div>
    </div>

    <form method="GET" action="/AxaBankFinance/generateMandateReport" target="blank" id="formPDFMandate">
        <input type="hidden" name="mandateLoanId"  id="txtMandateLoanId" value="${loan.loanId}"/>
    </form>               
    <div class="row panel-footer">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-default col-md-12" onclick="closeLoanProcess()" id="btnExit"><span class="fa fa-times"></span> Exit</button>
                    </div>
<!--                    <div class="col-md-6">
                        <button class="btn btn-default col-md-12" onclick="printVoucher()" id="btnPrintVou"><span class="fa fa-print"></span> My Voucher</button>

                        <c:choose>
                            <c:when test="${voucher!=''}">
                                <button class="btn btn-default col-md-12" onclick="printVoucherPDF()" id="btnPrintMandate"><span class="fa fa-print"></span> Voucherx</button>
                            </c:when>
                            <c:otherwise>
                                <button class="btn btn-default col-md-12" disabled="true"><span class="fa fa-print"></span> Vouchery</button>
                            </c:otherwise>
                        </c:choose>
                    </div>-->
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <button class="btn btn-default col-md-12" onclick="makePayments()" id="btnPrintMandate"><span class="fa fa-print"></span> Payments</button>
                    </div>
                    <!--                    <div class="col-md-4">
                                            <button class="btn btn-default col-md-12" onclick="printMandate()" id="btnPrintMandate"><span class="fa fa-print"></span>Mandate</button>
                                        </div>-->
                    <div class="col-md-4">
                        <button class="btn btn-default col-md-12" onclick="printAgreement()" id="btnAgreement"><span class="fa fa-print"></span> Agreement</button>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-default col-md-12" onclick="printReceipt()" id="btnRecpt"><span class="fa fa-print"></span> Receipt</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<form method="GET" action="/AxaBankFinance/generateLoanAgreement" target="blank" id="formLoanAgrment">
    <input type="hidden" name="agreementLoanId"  value="${loan.loanId}" />
</form>
<form method="GET" action="/AxaBankFinance/generatePDFVoucher" target="blank" id="formPDFVoucher2">
    <input type="hidden" name="hidVoucherId"  id="txtHidVoucherId" value="${voucherId}"/>
    <input type="hidden" name="hidVoucherType"  id="txtHidVoucherType" value="1"/>
</form>
<form method="GET" action="/AxaBankFinance/ReportController/printAcceptanceReceipt" target="blank" id="formLoanRecpt">
    <input type="hidden" name="loanId"  id="txtAcceptanceLoanId" value="${loan.loanId}"/>
</form>

<form method="GET" action="/AxaBankFinance/ReportController/generateLoanVoucher" target="blank" id="formLoanVoucher">
    <input type="hidden" name="loanId"  value="${loan.loanId}" />
</form>

<div class="searchCustomer" id="divOtherCharges" style="width: 35%; height: 50%; margin-top: 5%; margin-left: 7%; padding: 20px 40px;">
    <div class="row" style="height: 80%; overflow: auto;" >
        <table class="table table-bordered table-edit table-hover table-responsive dataTable" id="tblPrintOtherCharges">
            <caption>Other Charges</caption>
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Rate</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-9"></div>
        <div class="col-md-3"><input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"/></div>
        <!--<div class="col-md-3"><input type="button" class="btn btn-default col-md-12" value="Print"/></div>-->
    </div>
</div>
<div id="message_loanProcess" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div class="searchCustomer" id="loan_makePayment" style="width: 50%; margin-top: 0%; margin-left: 5%;"></div>
<script src="${cp}/resources/js/toWord.js" type="text/javascript"></script>
<script>
            $(function () {
                $(".txtCalendar").datepicker({
                    dateFormat: 'yy-mm-dd'
                });

                $('#checkOtherPayment').click(function () {
                    if ($(this).is(':checked')) {
                        var loanAmount = $("#withOthercharges_id").val();
                        $("#issueAmount_id").val(loanAmount);
                        $("#txtfinalLoanAmount").val(loanAmount);
                        $("#finalchekAmount").val(loanAmount);

                    } else {
                        var amountWithCharge = $("#withoutOthercharges_id").val()
                        $("#issueAmount_id").val(amountWithCharge);
                        $("#txtfinalLoanAmount").val(amountWithCharge);
                        $("#finalchekAmount").val(amountWithCharge);
                    }
                });

            });

            function viewOtherCharges() {
                var loanId = $('input[name=hidLoanId]').val();
                $.getJSON('/AxaBankFinance/findOtherCharges/' + loanId, function (data) {
                    $("#tblPrintOtherCharges tbody").html("");
                    var total = 0.00;
                    for (var i = 0; i < data.length; i++) {
                        var description = data[i][0];
                        var rate = data[i][1];
                        var amount = data[i][2];
                        total = total + amount;
                        $("#tblPrintOtherCharges tbody").append("<tr><td>" + description + "</td><td>" + rate + "</td><td style='text-align:right'>" + amount + "</td></tr>");
                    }
                    $("#tblPrintOtherCharges tbody").append("<tr><td colspan='3' style='text-align:right; font-weight:bold; '>" + total + "</td></tr>");
                    ui("#divOtherCharges");
                });
            }

            function closeLoanProcess() {
                viewAllLoans(1);
            }

            function printMandate() {
                $("#formPDFMandate").submit();
            }
            function printAgreement() {
                $("#formLoanAgrment").submit();
            }
            function printReceipt() {
                $("#formLoanRecpt").submit();
            }

            function makePayments() {
                var loanId = $('input[name=hidLoanId]').val();
                $.ajax({
                    url: "/AxaBankFinance/loadPaymentsPage/" + loanId,
                    success: function (data) {
                        $("#loan_makePayment").html("");
                        $("#loan_makePayment").html(data);
                        ui("#loan_makePayment");
                    }
                });
            }

            function printAdvancePayment(voucherNo) {
                document.getElementById("btnPrintChq").disabled = true;
                var loanId = $('input[name=hidLoanId]').val();
                var amount = $("#txtAdvance").val();
                $.ajax({
                    url: "/AxaBankFinance/loadAdvancedPayment",
                    data: {loanId: loanId, voucher: voucherNo, amount: amount},
                    success: function (data) {
                        $("#loan_makePayment").html(data);
                        ui("#loan_makePayment");
                    }
                });
            }

            function printVoucherPDF() {
                $("#formPDFVoucher2").submit();
            }

            function printVoucher() {
                $("#formLoanVoucher").submit();

            }

</script>
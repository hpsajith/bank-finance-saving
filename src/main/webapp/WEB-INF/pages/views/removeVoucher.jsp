<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblRemove").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="row" style="margin: 20px">
    <div class="col-md-2"></div>
    <div class="col-md-3">Date</div>
    <div class="col-md-3"><input type="text" class="txtCalendar" id="searchVouchDate" value="${vdate}"/></div>
</div>
<table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblRemove"  >
    <thead>
        <tr>
            <th>Agreement NO</th>
            <th>Voucher No</th>
            <th>Amount</th>
            <th>Type</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="voucher" items="${voucherList}">
            <tr id="${voucher.id}">
                <td>${voucher.agreementNo}</td>
                <td>${voucher.voucherNo}</td>
                <td>${voucher.amount}</td>
                <c:choose>
                    <c:when test="${voucher.voucherCatogery==1}">
                        <td>Loan</td>
                    </c:when>
                    <c:when test="${voucher.voucherCatogery==2}">
                        <td>Cash/Bank</td>
                    </c:when>
                    <c:when test="${voucher.voucherCatogery==3}">
                        <td>Western Union</td>
                    </c:when>
                    <c:when test="${voucher.voucherCatogery==4}">
                        <td>Foreign Cash</td>
                    </c:when>
                    <c:when test="${voucher.voucherCatogery==5}">
                        <td>Other</td>
                    </c:when>
                    <c:otherwise>
                        <td>Opening</td>
                    </c:otherwise>
                </c:choose>
                <td><div id="editV${voucher.id}" class="edit" style="margin-left: 30px" onclick="editVoucher(${voucher.id})"</td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<div id="msgDiv_voucher" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<div class="searchCustomer" id="editPayments_div" style="width: 70%; margin-left: -6%; margin-top: 10%;"></div>
<script>
    $(document).ready(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            onSelect: function(data) {
                var vDate = $(this).val();
//                alert(vDate);
                $.ajax({
                    url: '/AxaBankFinance/loadVoucherByDate',
                    data: {vDate: vDate},
                    success: function(data) {
                        $('#formContent').html(data);
                    },
                    error: function() {
                    }
                });
            }
        });
        
    });
    function editVoucher(voucherId) {
        $.ajax({
            url: '/AxaBankFinance/editVoucher/' + voucherId,
            success: function(data) {
                if (data) {
                    $('#editPayments_div').html("");
                    $('#editPayments_div').html(data);
                    ui('#editPayments_div');
                }
            },
            error: function() {
                alert("Failed loading Payment details ...");
            }
        });
    }
</script>



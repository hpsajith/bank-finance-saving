<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
//        $("#tbl_pay").chromatable({
//            width: "100%", // specify 100%, auto, or a fixed pixel amount
//            height: "60%",
//            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
//        });
    });
</script>

<div class="close_div"  onclick="unblockui()">X</div>
<div class="row" style="margin-left: 10px; text-align: left; font-size: 15px;"><label>Voucher Details</label></div>
<div class="row" style="padding: 5px;margin-top: 20px; height: 350px; overflow: auto;">
    <input type="hidden" value="${vocherId}" id="hidVoucherNo"/>
    <table class="dc_fixed_tables table-bordered newTable" border="0" cellspacing="0" cellpadding="0" id="tbl_pay"  >
        <thead>
            <tr>
                <th>Transaction No</th>
                <th>Type</th>
                <th>Amount</th>
                <th>Cheq No</th>
                <th>Bank</th>
                <th>Realize Date</th>
                <th>Remark</th>
                <th>Edit</th>
                <th>Remove</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="payment" items="${voucherList}">
                <tr id="${payment.type}${payment.transId}">
                    <td>${payment.transactionNo}</td>                
                    <td><select id="payType${payment.transId}">
                            <c:choose>
                                <c:when test="${payment.type=='CASH'}">
                                    <option value="1" selected="">Cash</option>
                                    <option value="2">Cheque</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="1">Cash</option>
                                    <option value="2" selected="">Cheque</option>
                                </c:otherwise>
                            </c:choose>
                        </select></td>
                    <td>${payment.amount}</td>
                    <c:choose>
                        <c:when test="${payment.type=='CASH'}">
                            <td></td>
                            <td></td>
                            <td></td>
                        </c:when>
                        <c:otherwise>
                            <td><input type="text" class="col-md-12" id="cheqNo${payment.transId}" name="" value="${payment.cheqNo}"/></td>                                            
                            <td>
                                <select name="bank" id="bankId${payment.transId}" class="col-md-12">  
                                    <c:forEach var="banki" items="${banks}">
                                        <c:choose>
                                            <c:when test="${payment.bank==banki.bankId}">
                                                <option value="${banki.bankId}" selected="">${banki.configBankName}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${banki.bankId}">${banki.configBankName}</option>
                                            </c:otherwise>
                                        </c:choose>                            
                                    </c:forEach>
                                </select>
                            </td>
                            <td><input type="text" value="${payment.realizeDate}" id="relizeDate${payment.transId}"</td>
                            </c:otherwise>
                        </c:choose>
                    <td><input type="text" name="" id="comment${payment.transId}" value="" class="col-md-12"/></td>
                    <td><input type="button" class="btn btn-default btn-edit" value="Edit" onclick="editPayment(${payment.transId})"/></td>
                    <td><input type="button" class="btn btn-default btn-edit" value="Remove" onclick="deletePayment(${payment.transId})"/></td>                    
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div class="row" style="margin-top: 10px;">
    <div class="col-md-7"></div>
    <div class="col-md-2"><input type="button" class="btn btn-default col-md-12" value="Cancel" id="cancelVouchBut" onclick="unblockui()"/></div>
    <div class="col-md-3"><input type="button" class="btn btn-default col-md-12" value="Remove Voucher" id="removeVouchBut" onclick="removeVoucher()"/></div>
</div>

<script>

    function editPayment(id) {
        var bankId = $("#bankId" + id).val();
        var paymentType = $("#payType" + id).val();
        var cheqNo = $("#cheqNo" + id).val();
        var relizeDate = $("#relizeDate" + id).val();
        var comment = $("#comment" + id).val();
        if (validateFields(id)) {
            $.ajax({
                url: '/AxaBankFinance/editPaymentType?${_csrf.parameterName}=${_csrf.token}',
                type: 'POST',
                data: {trnsId: id, bank: bankId, type: paymentType, cheqNo: cheqNo, rDate: relizeDate, comment: comment},
                success: function(data) {
                    loadVouchers();
                    $('#msgDiv_voucher').html("");
                    $('#msgDiv_voucher').html(data);
                    ui('#msgDiv_voucher');

                },
                error: function() {
                    alert("Failed loading Payment details ...");
                }
            });
        }
    }

    function deletePayment(id) {
        var paymentType = $("#payType" + id).val();
        $.ajax({
            url: '/AxaBankFinance/deletePayment?${_csrf.parameterName}=${_csrf.token}',
            type: 'POST',
            data: {trnsId: id, type: paymentType},
            success: function(data) {
                loadVouchers();
                $('#msgDiv_voucher').html("");
                $('#msgDiv_voucher').html(data);
                ui('#msgDiv_voucher');

            },
            error: function() {
                alert("Failed loading Payment details ...");
            }
        });
    }

    function removeVoucher() {
        var vouId = $("#hidVoucherNo").val();
        $.ajax({
            url: '/AxaBankFinance/removeVoucher/' + vouId,
            success: function(data) {
                loadVouchers();
                $('#msgDiv_voucher').html("");
                $('#msgDiv_voucher').html(data);
                ui('#msgDiv_voucher');

            },
            error: function() {
                alert("Failed loading Payment details ...");
            }
        });
    }

    function validateFields(id) {
//        var bankId = $("#bankId" + id).val();
//        var paymentType = $("#payType" + id).val();
//        var cheqNo = $("#cheqNo" + id).val();
//        var relizeDate = $("#relizeDate" + id).val();
//        var comment = $("#comment" + id).val();
//        if (comment == "") {
//            $("#comment" + id).css({'border', '1px solid red'});
//            return false;
//        }
        return true;
    }

</script>
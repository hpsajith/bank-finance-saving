<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<h3 class="page-header">Loan Details</h3><br/>
<div id="tblLoan">
    <div class="row">
        <div clas="col-md-6">
            <c:forEach var="loanBasic" items="${basic}">
                <c:if test="${loanBasic!=null}">                    
                    <label>Full Name:${loanBasic.loanFullName}</label>
                </c:if>
            </c:forEach>
        </div>
        <div clas="col-md-6">
            <c:forEach var="dependent" items="${dependent}">
                <c:if test="${dependent!=null}">
                </c:if>
            </c:forEach>
        </div>
    </div>
    <div class="row">
        <div clas="col-md-6">
            <c:forEach var="employemnt" items="${business}">
                <c:if test="${employemnt!=null}">                    
                </c:if>
            </c:forEach>
        </div>
        <div clas="col-md-6">
            <c:forEach var="business" items="${business}">
                <c:if test="${business!=null}">
                </c:if>
            </c:forEach>
        </div>
    </div>
    <div class="row">
        <div clas="col-md-6">
            <c:forEach var="asset" items="${asset}">
                <c:if test="${asset!=null}">                    
                    <label>Full Name:${asset.loanFullName}</label>
                </c:if>
            </c:forEach>
        </div>
        <div clas="col-md-6">
            <c:forEach var="liability" items="${liability}">
                <c:if test="${liability!=null}">
                </c:if>
            </c:forEach>
        </div>
    </div>
    <div class="row">
        <c:forEach var="liability" items="${liability}">
            <c:if test="${liability!=null}">
                <div clas="col-md-4">
                    
                </div>
            </c:if>
        </c:forEach>
    </div>



</div>
<p class="btnPanel">
    <button onclick="loadLoanEmployementForm()" class="btn btn-primary" id="btnDependent">Employement</button>
    <button onclick="loadLoanBusinessForm()" class="btn btn-primary" id="btnLiability">Business</button>
    <button onclick="loadLoanDependentForm()" class="btn btn-primary" id="btnAssest">Dependent</button>
    <button onclick="loadLoanAssesBankForm()" class="btn btn-primary" id="btnBusiness">Asset</button>
    <button onclick="loadLoanLiabilityForm()" class="btn btn-primary" id="btnBusiness">Liability</button>
    <button onclick="" class="btn btn-primary" id="btnGuarantee">Guarantee</button>
    <button onclick="" class="btn btn-primary" id="btnBusiness">Article</button>
</p>

<script>
    function loadLoanEmployementForm() {
        $.ajax({
            url: '/AxaBankFinance/basicEmpForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanBusinessForm() {
        $.ajax({
            url: '/AxaBankFinance/basicBusForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanAssesBankForm() {
        $.ajax({
            url: '/AxaBankFinance/basicAsseForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanLiabilityForm() {
        $.ajax({
            url: '/AxaBankFinance/basicLiaForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
    function loadLoanDependentForm() {
        $.ajax({
            url: '/AxaBankFinance/basicDepeForm',
            success: function(data) {
                $('#mainContentRight').html(data);
            }
        });
    }
</script>

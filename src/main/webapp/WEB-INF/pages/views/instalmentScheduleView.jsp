<%-- 
    Document   : instalmentScheduleView
    Created on : Apr 12, 2016, 9:47:03 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        $("#tblInstalmentScheduleView").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });

</script>

<style>
    /* tr{
            background-color: bcddff;
        }*/
    tr:hover {
        background-color: e56b70;
    }
</style>
<!--<label>** Total principle:-${tot}</label>
<label> </label>
<label>** Total interest-${instr}</label>
<label> </label>
<label>** Total instalments:-${ins}</label>-->
<div class="row">
<label style="color: #0088cc;font-size: 16px">Monthly Instalment Schedule</label>
    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblInstalmentScheduleView"  >
        <thead>
            <tr>
                <th>Month</th>
                <th>Interest</th>
                <th>Principle</th>
                <th>Installment</th>
                <th>Balance</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="scheduleList" items="${scheduleView}">
                <tr>
                    <td style="text-align: center">${scheduleList.insId}</td>
                    <td style="text-align: right">${scheduleList.insInterest}</td>
                    <td style="text-align: right">${scheduleList.insPrinciple}</td>
                    <td style="text-align: right">${scheduleList.insRoundAmount}</td>
                    <td style="text-align: right">${scheduleList.loanBalance}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style=" margin-top: 10px; padding: 10;  background: #F2F7FC;">
        <div class="col-md-3">
            <strong><label>${name}</label></strong>
        </div> 
        <!--        <div class="col-md-4">
                    <strong><span class="fa fa-credit-card"></span>  9015205274V </strong> 
                </div>-->

        <div class="col-md-5">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <!--<strong> <span class="fa fa-money"></span> </strong>-->
            </div>
        </div>
    </div>
    <div class="row fixed-table">
        <div class="table-content">

            <c:choose>
                <c:when test="${message}">
                    <p style="color: #777; text-align: left;">Previous Loan Details</p>
                    <table class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header" style="margin-top: 10px;">
                        <thead>
                            <tr>
                                <th>Agreement No</th>
                                <th>Date</th>
                                <th>Loan Amount</th>
                                <th>Period</th>
                                <th>Rate</th>
                                <th>Status</th>
                                <th>Branch</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="loan" items="${loanList}">
                                <tr >
                                    <td>${loan.loanAgreementNo}</td>
                                    <td>${loan.loanDate}</td>
                                    <td>${loan.loanAmount}</td>
                                    <c:choose>
                                        <c:when test="${loan.loanPeriodType==1}">
                                            <td>${loan.loanPeriod} Month</td>
                                        </c:when>
                                        <c:when test="${loan.loanPeriodType==2}">
                                            <td>${loan.loanPeriod} Year</td>
                                        </c:when>
                                        <c:when test="${loan.loanPeriodType==3}">
                                            <td>${loan.loanPeriod} Days</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>${loan.loanPeriod} Weeks</td>
                                        </c:otherwise>
                                    </c:choose>


                                    <td>${loan.loanInterestRate}</td>
                                    <c:choose>
                                        <c:when test="${loan.loanSpec==0}">
                                            <c:choose>
                                                <c:when test="${loan.loanStatus==0}">
                                                    <td style="color: orange;">Processing For Loan</td>
                                                </c:when>
                                                <c:when test="${loan.loanStatus==1}">
                                                    <td style="color: green;">On Going</td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td style="color: red;">Rejected</td>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==1}">
                                            <td style="color: red;">Laps</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==2}">
                                            <td style="color: red;">Legal</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==3}">
                                            <td style="color: red;">Seize</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==4}">
                                            <td style="color: green;">Full Paid</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==5}">
                                            <td style="color: green;">Rebate</td>
                                        </c:when>
                                    </c:choose>  
                                    <c:forEach var="branch" items="${MBranch}">
                                        <c:if test="${loan.branchId == branch.branchId}">
                                            <td>${branch.branchName}</td>
                                        </c:if>       
                                    </c:forEach>

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <label style="font-size: 13px; margin-top: 15px; color: #0075b0">No any previous loans</label>
                </c:otherwise>
            </c:choose>

            <c:choose>
                <c:when test="${hasGuarntors}">
                    <p style="color: #777; text-align: left;">Previous Guarantor Details</p>
                    <table class="table table-bordered table-striped table-hover table-condensed table-edit ">
                        <thead>
                            <tr>
                                <th>Agreement No</th>
                                <th>Customer Name</th>
                                <th>Loan Type</th>
                                <th>Amount</th>
                                <th>Period</th>
                                <th>Status</th>
                                <th>Branch</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="guarantors" items="${gurantor}">
                                <tr>
                                    <td>${guarantors.agreementNo}</td>
                                    <td>${guarantors.debtorName}</td>
                                    <td>${guarantors.loanType}</td>
                                    <td>${guarantors.loanAmount}</td>
                                    <td>${guarantors.loanPeriod}</td>
                                    <c:choose>
                                        <c:when test="${guarantors.loanSpec==0}">
                                            <c:choose>
                                                <c:when test="${guarantors.loanStatus==0}">
                                                    <td style="color: orange;">Processing</label></td>
                                                </c:when>
                                                <c:when test="${guarantors.loanStatus==1}">
                                                    <td style="color: green;">On Going</label></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td style="color: red;">Reject</label></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${guarantors.loanSpec==1}">
                                                    <td style="color: red;">Laps</td>
                                                </c:when>
                                                <c:when test="${guarantors.loanSpec==2}">
                                                    <td style="color: red;">Legal</td>
                                                </c:when>
                                                <c:when test="${guarantors.loanSpec==3}">
                                                    <td style="color: red;">Seize</td>
                                                </c:when>
                                                <c:when test="${guarantors.loanSpec==4}">
                                                    <td style="color: red;">Full Paid</td>
                                                </c:when>
                                                <c:when test="${guarantors.loanSpec==5}">
                                                    <td style="color: red;">Rebate</td>
                                                </c:when>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>

                                    <c:forEach var="branch" items="${MBranch}">
                                        <c:if test="${guarantors.branchId == branch.branchId}">
                                            <td>${branch.branchName}</td>
                                        </c:if>       
                                    </c:forEach>         
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:when>
            <c:otherwise>
                <label style="font-size: 13px; margin-top: 15px; color: #0075b0">No any previous guarantee history</label>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</div>  
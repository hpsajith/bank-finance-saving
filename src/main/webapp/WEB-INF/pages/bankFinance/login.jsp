
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<html>
    <head>
        <title>Login Page</title>
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />

        <link href="${cp}/resources/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="${cp}/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--<link href="${cp}/resources/css/admin-2.css" rel="stylesheet" type="text/css"/>-->
        <script src="${cp}/resources/js/jquery.min.js" type="text/javascript"></script>
        <!--<script src="${cp}/resources/js/admin-2.js" type="text/javascript"></script>-->
        <script src="${cp}/resources/js/jquery-ui.js" type="text/javascript"></script>
        <script src="${cp}/resources/js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="${cp}/resources/js/jquery-ui-1.10.4.js" type="text/javascript"></script>
        <link href="${cp}/resources/css/styles.css" rel="stylesheet" type="text/css"/>
        <style>
            .error {
                padding: 2px;
                margin-bottom:3px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #a94442;
                background-color: #f2dede;
                border-color: #ebccd1;
            }

            .msg {
                padding: 2px;
                margin-bottom: 3px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #31708f;
                background-color: #d9edf7;
                border-color: #bce8f1;
            }
            .box{
                background: #e6e6e6 ;
                text-align: center;
                border-radius: 10px;
                height: 45%;
                opacity: 0.8;
                filter: alpha(opacity=85);

            }
            body {
                background-image:url(${pageContext.request.contextPath}/resources/img/loginScreen.png);
                background-repeat:no-repeat;
                background-size: cover;
                background-attachment: fixed;
            }
            .login {
                height: 35%;
                width: 37.9999%;
                position: absolute;
                left:0; right:0;
                top:0; bottom:0;
                margin:auto;
            }
        </style>

    </head>

    <body>
        <div class="row">
            <div class="col-md-3 col-lg-3 col-sm-3"></div>
            <div class="box col-md-6 col-lg-6 col-sm-6 login" style="height: 45%">
                <label id="txtSingUp">Sign In</label>
                <div id="divError1" style="display: none"></div>
                <div id="divError3" class="error" style="display: none"></div>
                <c:if test="${not empty error}">
                    <div id="divError2" class="error">${error}</div>
                </c:if>
                <c:if test="${not empty msg}">
                    <div id="divMsg" class="msg">${msg}</div>
                </c:if>
                <form id="loginForm" action="<c:url value='/login' />" method='POST' autocomplete='off'>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">
                            <label class="" style="margin-top: 8px">Username</label>
                        </div>
                        <div class="col-md-9 col-sm-9 col-lg-9">
                            <input class="form-control" placeholder="Username" name="username" type="text" style="width: 100%" autofocus autocomplete="off" value="${username}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">
                            <label class="" style="margin-top: 8px">Password</label>
                        </div>
                        <div class="col-md-9 col-sm-9 col-lg-9">
                            <input class="form-control" placeholder="Password" name="password" style="width: 100%" type="password" autofocus autocomplete="off" value="${password}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-lg-8"></div>
                        <div class="col-md-4 col-sm-4 col-lg-4">
                            <button class="btn btn-primary btn-block" id="aunthticatBut"><span
                                    class="glyphicon glyphicon-lock"> </span> Authenticate</button>
                        </div>
                    </div>
                    <div class="row" id="sectionDiv" style="display: none">
                        <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left"><label class="" style="margin-top: 8px">Division</label></div>
                        <div class="col-md-9 col-sm-9 col-lg-9">
                            <select class="form-control" autocomplete="off" id="selSection">
                            </select>
                        </div>
                    </div>
                    <div class="row" id="branchDiv" style="display: none">
                        <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left"><label class="" style="margin-top: 8px">Branch</label></div>
                        <div class="col-md-9 col-sm-9 col-lg-9">
                            <select class="form-control" autocomplete="off" id="selBranch" disabled>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="branchId" id="txtBranchId" value="1"/>
                    <input type="hidden" name="sectionId" id="txtSectionId" />
                    <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                </form>
                <div style="clear: both"></div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-lg-3"></div>
                    <div class="col-md-9 col-sm-9 col-lg-9">
                        <div class="row">                            
                            <div class="col-md-6 col-sm-6 col-lg-6" ><input type="button" value="Cancel" name="cancel" class="btn btn-primary btn-block" style="width: 100%" id="cancelButton" onclick="window.location.reload();"/></div>
                            <div class="col-md-6 col-sm-6 col-lg-6"><input type="button" value="Login" name="submit" class="btn btn-primary btn-block" style="width: 100%" id="loginButton" disabled/></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3"></div>
        </div>
       <div class="row" style='text-align:center;position:absolute;bottom:0; width:100%;'>
            <label id="itesCopyright">Copyright <span class="glyphicon glyphicon-copyright-mark"></span> 2017 Version 3.0 <a style="color: #222"href="http://www.itessoftware.com" target="_blank">ITES Software </a>(Pvt) LTD, All rights reserved.</label>
        </div>
    </body>
</html>

<script>

    var userId = null;
    $(function () {
        $("#loginButton").css('display', 'none');
        $("#cancelButton").css('display', 'none');

        $('#aunthticatBut').click(function (event) {
            event.preventDefault();
            if (loginFormValidate()) {
                authenticate();
            }
        });

        $("#selSection").change(function () {
            var section_id = $("#selSection").find('option:selected').val();
            if (userId !== null && section_id !== "0") {
                $.ajax({
                    url: "/AxaBankFinance/loadUserBranches/" + userId + "/" + section_id,
                    success: function (data) {
                        if (data.length > 0) {
                            $("#selBranch").html("");
                            for (var i = 0; i < data.length; i++) {
                                $("#selBranch").append("<option value='" + data[i].branchId + "'>" + data[i].branchName + "</option>");
                            }
                            $("#selBranch").removeAttr("disabled");
                            $("#loginButton").removeAttr("disabled");
                        } else {
                            $("#selBranch").html("");
                            $("#selBranch").attr("disabled", true);
                            $("#loginButton").attr("disabled", true);
                        }
                    }
                });
            } else {
                $("#selBranch").html("");
                $("#selBranch").attr("disabled", true);
                $("#loginButton").attr("disabled", true);
            }
        });

        $('#loginButton').click(function (event) {
            var branch_id = $("#selBranch").find('option:selected').val();
            $("#txtBranchId").val(branch_id);
            if (branch_id != "" && branch_id !== "0" && branch_id != null) {
                $("#txtBranchId").val(branch_id);
                saveBranchId(branch_id);
            } else {
                alert("Select Branch");
            }
        });

    });

    function saveBranchId(branchId) {
        var branchName = $("#selBranch").find('option:selected').text();
        $.ajax({
            url: "/AxaBankFinance/setBranchId/" + branchId + "/" + branchName,
            success: function (data) {
                if (data === 1) {
                    $("#loginForm").submit();
                } else {
                    $("#divError3").css('display', 'block');
                    $("#divError3").html("User login license has been exceeded. Please contact Admin for further assistance");
                }
            }
        });
    }

    function loginFormValidate() {
        var ret = false;
        var username = $("input[name=username]").val().trim();
        var password = $("input[name=password]").val().trim();
        if (username !== "" && password !== "") {
            ret = true;
        }
        return ret;
    }

    function authenticate() {
        var username = $("input[name=username]").val();
        var password = $("input[name=password]").val();
        $.ajax({
            url: "/AxaBankFinance/authenticate/" + username + "/" + password,
            success: function (data) {
                if (data.exist) {
                    $("#divError1").css('display', 'none');
                    $("#divError2").css('display', 'none');
                    $("#divError1").removeClass("error");
                    $("#divError1").text("");
                    userId = data.userId;
                    $("#selSection").html("");
                    $("#selSection").append("<option value='0'>-- SELECT --</option>");
                    for (var i = 0; i < data.sections.length; i++) {
                        $("#selSection").append("<option value='" + data.sections[i].id + "'>" + data.sections[i].sectionName + "</option>");
                    }
                    $("#sectionDiv").css('display', 'block');
                    $("#branchDiv").css('display', 'block');
                    $("#selBranch").html("");
                    $("#aunthticatBut").css('display', 'none');
                    $("#loginButton").css('display', 'block');
                    $("#cancelButton").css('display', 'block');
                } else {
                    $("#divError1").css('display', 'block');
                    $("#divError2").css('display', 'none');
                    $("#divMsg").css('display', 'none');
                    $("#divError1").addClass("error");
                    $("#divError1").text("Invalid username and password!");
                    userId = null;
                    $("#selSection").html("");
                    $("#sectionDiv").css('display', 'none');
                    $("#branchDiv").css('display', 'none');
                    $("#selBranch").html("");
                    $("#aunthticatBut").css('display', 'block');
                    $("#loginButton").css('display', 'none');
                    $("#loginButton").attr("disabled", true);
                    $("#cancelButton").css('display', 'none');
                }
            }
        });
    }


</script>



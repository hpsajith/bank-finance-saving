<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<html lang="en">
<head>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>

    <link href="${cp}/resources/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/commonCss.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/dataGrid.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/ion.rangeSlider.skinHTML5.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/admin-2.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
    <link href="${cp}/resources/css/my-style.css" rel="stylesheet" type="text/css"/>


    <script src="${cp}/resources/js/jquery.min.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/admin-2.js" type="text/javascript"></script>
    <script src="${cp}/resources/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/metisMenu.min.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/metisMenu.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/responsive-table.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/autoNumeric.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/jquery-ui.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/validation.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/datagrid.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/message.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/ion.rangeSlider.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/ion.rangeSlider.min.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/sweetalert2.min.js" type="text/javascript"></script>
    <script src="${cp}/resources/js/jquery.maskedinput.js" type="text/javascript"></script>



    <script>
        function ui(msg) {
            $.blockUI({message: $(msg),
                css: {
                    border: 'none',
                    height: 'auto',
                    'cursor': 'auto',
                    'width': 'auto',
                    'top': 1,
                    'left': ($(window).width() - 700) / 2
                }
            });
        }
        function unblockui() {
            $.unblockUI();
        }
        function formSubmit() {
            inactiveUser();
        }
        function pageExit() {
            $('#formContent').html("");
        }

    </script>
    <style type="text/css">
        .badge-notify {
            background: none repeat scroll 0 0 #eb4e4f;
            border-radius: 8px;
            height: 16px;
            left: -10px;
            position: relative;
            top: -10px;
            width: 17px;
        }

        body {
            max-width: 100%;
            overflow-x: hidden;
        }
    </style>
</head>
<body>
<c:url value="" var="logoutUrl"/>
<form action="${logoutUrl}" method="post" id="logoutForm">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="row navbar-header" style="height: 50px;">
        <div class="">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

    </div>

    <div class="row">
        <div class="col-md-7" style="">
            <div class="row" style="">
                <div class="col-md-4 col-lg-4 col-sm-4" style="margin-top: 1%">
                    <img id="comLogo" src="/AxaBankFinance/viewCompanyLogo" class="logo"/>
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8" style="">
                    <div class="col-md-12 col-lg-12 col-sm-12" id="companyName"
                         style="color: #337ab7;padding: 1px">${companyName}</div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-4 branchTell" style="color: #337ab7">
                            T.P: ${comTellNo}</div>
                        <div class="col-md-8 col-lg-8 col-sm-8 branchTell" style="color: #337ab7">
                            Email: ${comEmail}</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <ul class="nav navbar-top-links navbar-right" style="margin-top: -22px">
                    <li class="dropdown">
                        <c:choose>
                            <c:when test="${userMessages.size() > 0}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="font-size: 0">
                                    <i class="fa fa-envelope fa-fw" style="font-size: 20px"></i>
                                    <span class="badge badge-notify">
                                                <label style="font-size: 10px">${userMessages.size()}</label>
                                            </span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-envelope fa-fw"></i><i class="fa fa-caret-down"></i>
                                </a>
                            </c:otherwise>
                        </c:choose>
                        <ul class="dropdown-menu dropdown-messages">
                            <c:choose>
                                <c:when test="${userMessages.size() > 0}">
                                    <c:forEach var="userMessage" items="${userMessages}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <li>
                                                    <a href="#" onclick="loadUserMessage(${userMessage.id})">
                                                        <div>
                                                            <strong>${userMessage.umUserByUId.userName}</strong>
                                                            <span class="pull-right text-muted">
                                                                        <em>${userMessage.publishDate}</em>
                                                                    </span>
                                                        </div>
                                                        <div>${userMessage.subject}</div>
                                                    </a>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li>
                                                    <a href="#" onclick="loadUserMessage(${userMessage.id})">
                                                        <div>
                                                            <strong>${userMessage.umUserByUId.userName}</strong>
                                                            <span class="pull-right text-muted">
                                                                        <em>${userMessage.publishDate}</em>
                                                                    </span>
                                                        </div>
                                                        <div>${userMessage.subject}</div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <li>
                                        <a class="text-center" href="#">
                                            <strong>You have no any messages</strong>
                                        </a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>

                    <!-- /.dropdown Task -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks">
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 1</strong>
                                            <span class="pull-right text-muted">40% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar"
                                                 aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 2</strong>
                                            <span class="pull-right text-muted">20% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar"
                                                 aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 20%">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 3</strong>
                                            <span class="pull-right text-muted">60% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar"
                                                 aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 60%">
                                                <span class="sr-only">60% Complete (warning)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Tasks</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>

                    <!-- /.dropdown-alert-->
                    <li class="dropdown">
                        <c:choose>
                            <c:when test="${loanChequesExist}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="font-size: 0">
                                    <i class="fa fa-bell fa-fw" style="font-size: 18px"></i>
                                    <span class="badge badge-notify">
                                                <label style="font-size: 10px">1</label>
                                            </span>
                                </a>
                                <ul class="dropdown-menu dropdown-alerts">
                                    <li>
                                        <a href="#" onclick="printPDChequesToDateReport()">
                                            <div>
                                                <i class="fa fa-download fa-fw"></i> PD Cheque
                                                <span class="pull-right text-muted small">${systemDate}</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </c:when>
                            <c:otherwise>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-alerts">
                                    <li>
                                        <a href="#">
                                            <div>
                                                <i class="fa fa-comment fa-fw"></i> No any Alerts
                                                <span class="pull-right text-muted small">${systemDate}</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </c:otherwise>
                        </c:choose>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown-admin-Panel -->
                    <c:if test="${pageContext.request.userPrincipal.name != null}">
                        <a href="#">Welcome ${pageContext.request.userPrincipal.name}</a>
                    </c:if>
                    <li class="dropdown">

                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Change Password</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="javascript:formSubmit()"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                </ul>

            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9 col-sm-9 branch" style="color: #337ab7;text-align: right">${branch}</div>
                <div class="col-md-3 col-lg-3 col-sm-3 branch"
                     style="color: #337ab7;text-align: left">${systemDate}</div>
            </div>
        </div>
    </div>
    <div class="row" id="redLine" style=""></div>


    <!-- /.navbar-Left-->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <!--nav-first-level-->

                <c:forEach var="mainTab" items="${jobList}">
                    <c:choose>
                        <c:when test="${mainTab.subList.size()>0}">
                            <li>
                                <a href="#"><i class="${mainTab.mainCode}"></i> ${mainTab.mainTabName}<span
                                        class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <c:forEach var="subTab" items="${mainTab.subList}">
                                        <li>
                                            <a href="#" onclick="${subTab.refPage}"><i
                                                    class="${subTab.subTabCode}"></i> ${subTab.subTabName}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <a href="#" onclick="${mainTab.refPage}"><i
                                        class="${mainTab.mainCode}"></i> ${mainTab.mainTabName}</a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
<!--main section-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 page-header" id="formContent"></div>
    </div>
    <div id="proccessingPage" class="processingDivImage">
        <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..."/>
    </div>
    <form method="GET" action="/AxaBankFinance/ReportController/printLoanChequesToDateReport" target="blank"
          id="formPDChequeToDate"></form>
</div>
</body>
</html>
<script type="text/javascript">
    function loadLoanForm(loanType, title) {
        $.ajax({
            url: '/AxaBankFinance/basicLoanForm',
            success: function (data) {
                $('#formContent').html(data);
                $('#formHeading').text(title);
                $('#loanTypeId').val(loanType);
            },
            error: function () {
            }
        });
    }
    function loadLoanEmployementForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/basicEmpForm',
            success: function (data) {
                $('#formContent').html(data);
            }
        });
    }
    function loadLoanBusinessForm() {
        $.ajax({
            url: '/AxaBankFinance/basicBusForm',
            success: function (data) {
                $('#mainContentLeft').html(data);
            }
        });
    }
    function loadLoanAssesBankForm() {
        $.ajax({
            url: '/AxaBankFinance/basicAsseForm',
            success: function (data) {
                $('#mainContentLeft').html(data);
            }
        });
    }
    function loadLoanLiabilityForm() {
        $.ajax({
            url: '/AxaBankFinance/basicLiaForm',
            success: function (data) {
                $('#mainContentLeft').html(data);
            }
        });
    }
    function loadLoanDependentForm() {
        $.ajax({
            url: '/AxaBankFinance/basicDepeForm',
            success: function (data) {
                $('#mainContentLeft').html(data);
            }
        });
    }

    function loadNewLoanForm() {
        $.ajax({
            url: '/AxaBankFinance/newLoanForm',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadDeletedLoans() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/deletedLoanForm',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function viewAllLoans(type) {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/viewAllLoans/' + type,
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadActiveLoans(){
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/viewActiveLoans',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function viewRejectLoans() {
        var type = 0;
        $.ajax({
            url: '/AxaBankFinance/viewAllLoans/' + type,
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadAddLoanTypeForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadLoanTypeFormMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadSubLoanTypeForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/subLoanType',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadAddUserMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/addUser',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadApproveLevelForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/addApprovalMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadNewEmployeeForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/newEmployee',
            success: function (data) {
                $('#formContent').html(data)
            },
            error: function () {
            }
        });
    }

    function loadRecoverOfficerPage2() {
        $.ajax({
            url: '/AxaBankFinance/loadFiledOfficerPage2',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadUserTypeLoanMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadUserTypeLoanMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadDocumentListMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadDocumentListMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadOtherChargesMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadOtherChargesMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadNewPawningForm() {
        $.ajax({
            url: '/AxaBankFinance/loadNewPawning',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }


    function loadAddUserTypeForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddUserTypeMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadAddBranchMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddBranchMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadOpeningLoan() {
        $.ajax({
            url: '/AxaBankFinance/openLoanForm',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadFieldOfficerMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadFieldOfficerMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadSettlementsForm() {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/newSettlementForm',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function dayEndProcess() {
        $.ajax({
            url: '/AxaBankFinance/DayEndController/loadDayEndProcess',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function commonDayEndProcess() {
        $.ajax({
            url: '/AxaBankFinance/DayEndController/loadCommonDayEnd',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadInsuranceCompanyMain() {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadInsuranceComMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadAddInsuranceMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadAddInsuranceMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadviewProcessInsurance() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadViewProcessInsurance',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadRenewInsuranceMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadRenewInsuranceMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadInsuCustomerMain() {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadInsuCustomerMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadViewOtherInsuranceMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadOtherInsuranceMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }


    function inactiveUser() {
        $.ajax({
            url: '/AxaBankFinance/inactiveUserLog',
            success: function (data) {
                if (data == 1) {
                    document.getElementById("logoutForm").submit();
                }
            },
            error: function () {
            }
        });
    }

    function loadOpenLoan() {
        $.ajax({
            url: '/AxaBankFinance/loadOpenLoanForm',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadMainDebtor() {
        $.ajax({
            url: '/AxaBankFinance/loadDebtorSearch',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadCorrectPayment() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadCorrectPayment',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadRecoveryArrearsLoans() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadRecoveryArrearsLoans',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadRecoveryLetterPostedLoans() {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadRecoveryLetterPostedLoans',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadRecoverOfficerPage() {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadFiledOfficerPage',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    //loadCheckPayment
    function loadCheckPayment() {
        $.ajax({
            url: '/AxaBankFinance/CheckReturnController/loadCheckPayment',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadOtherPayment() {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadOtherPayment',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadCashierReport() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadCashierReport',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadOtherPayment() {
        $.ajax({
            url: '/AxaBankFinance/OtherPaymentController/loadOtherPayment',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadLedgerReport() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadLedgerReport',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadAllLoanReport() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadAllLoanReport',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function pageAuthentication() {
        $.ajax({
            url: '/AxaBankFinance/checkPageAuthentication',
            success: function (data) {
                if (!data) {
                    window.location.reload();
                }
            },
            error: function () {
                alert("Error");
            }
        });
    }
    function loadInsuranceListReport() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadInsuranceList',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadBasicLoanDetailsReport() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadBasicLoanDetails',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loanInsuranceRegistry() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loanInsuranceRegistry',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadBalanceSheetPage() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadBalanceSheet',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadMonthlyTargetCED() {
        $.ajax({
            url: '/AxaBankFinance/DayEndController/loadMonthlyTargetCED',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadRemoveLoans() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/removeCurrentLoan',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadVouchers() {
        $.ajax({
            url: '/AxaBankFinance/loadVoucherByDate',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadInstallment() {
        $.ajax({
            url: '/AxaBankFinance/loadLoanInstallment',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }


    function loanRemoveLoanList() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadRemoveLoanList',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadSearchLoan() {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadSearchLoan',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadAddSupplierMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddSupplierMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loanRebateLoan() {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadRebateLoan',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
                alert("Msd");
            }
        });
    }
    function loadTargetMain() {
        var currDate = new Date();
        var month = Number(currDate.getMonth()) + 1;
        var tgtDate = currDate.getFullYear() + '-0' + month + '-' + '01';
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/loadTargetMain',
            data: {"tgtDate": tgtDate},
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadTargetAssignMain() {
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/loadTargetAssignMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadMonthlyCollection() {
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/loadMonthlyCollection',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadRMCollection() {
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/loadRMCollection',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }
    function loadROCollection() {
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/loadROCollection',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadChartOfAccount() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadChartOfAccount',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function loadRebateLoanList() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadRebateLoanList',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function loadLoanCalView() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadLoanCalView',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function loadPrintChequeRecvNote() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadPrintChequeRecvNote',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function loadRebateApprovalList() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadRebateApprovalList',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }
    function loadSeizeOrderPage() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadSeizeOrderPage',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function loadSeizeOrderApprovals() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadSeizeOrderApprovals',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function loadSeizeYardRegisterPage() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadSeizeYardRegisterPage',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }
    function loadNewSeizerMain() {
        $.ajax({
            url: '/AxaBankFinance/loadNewSeizerMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }
    function loadSeizeOrderRenewPage() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadSeizeOrderRenewPage',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function loadCustomerDetail() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadCustomerDetail',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadAddDeposit() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadAddDepositMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadRescheduleApprovalMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadLoanRescheduleApprovalMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {

            }
        });
    }

    function findRescheduleLoanDeatilsById(rescheduleId) {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/findRescheduleLoanDeatilsById/' + rescheduleId,
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function loadAnnouncementMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/UtilityController/loadAnnouncementMain',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function loadMessageMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/UtilityController/loadMessageMain',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function loadUserMessage(userMessageId) {
        $.ajax({
            url: '/AxaBankFinance/UtilityController/readUserMessage/' + userMessageId,
            success: function (data) {
                if (data !== null) {
                    swal({title: data.subject, text: data.message, allowOutsideClick: false});
                }
            },
            error: function () {

            }
        });
    }

    function loadFileDownloadPage() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/FileDownloadController/loadFileDownloadPage',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }


    function loadAddYardMainPage() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddYardMainPage',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function loadAddVehicleMainPage() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddVehicleMainPage',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function loadSearchFacilityPage() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadSearchFacilityPage',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function printPDChequesToDateReport() {
        $("#formPDChequeToDate").submit();
    }

    function loadEnvelopePrint() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadEnvelopePrint',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function loadMailListPage() {
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadMailListPage',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            },
            error: function () {

            }
        });
    }

    function loadViewInsuranceDebitCharge() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadViewInsuranceDebitCharge',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadDeletedInsuranceMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadDeletedInsuranceMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadContracts() {
        $.ajax({
            url: "/AxaBankFinance/ReportController/showContractReport",
            type: 'GET',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            }, error: function () {
                alert('Error in Loading...');
            }
        });
    }

    function loadReceivableReport() {
        $.ajax({
            url: "/AxaBankFinance/ReportController/showReceivableReport",
            type: 'GET',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            }, error: function () {
                alert('Error in Loading...');
            }
        });
    }

    function loadSearchforRefund() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadOverpayrefund',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadrefundreport() {
//                                                        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadoprefundrportform',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadAgreementTerminationMain() {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadAgreementTerminationMain",
            type: 'GET',
            success: function (data) {
                $('#formContent').html(data);
                unblockui();
            }, error: function () {
                alert('Error in Loading...');
            }
        });
    }

    function loadAddCenterMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddCenterMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadAddGroupMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddGroupMain',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }


    function loadNewCustomerMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/newLoanGroupCustomerForm',
            success: function (data) {
                $('#formContent').html(data);
                ui("");
            },
            error: function () {
            }
        });
    }

    function loanInstallmentMain() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/ReportController/loadLoanInstallments',
            success: function (data) {
                $('#formContent').html(data);
                ui("");
            },
            error: function () {
            }
        });
    }

    function loadMainNewCustomer() {
        $.ajax({
            url: '/AxaBankFinance/sepNewCustomer',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    // saving

    function loadAddAccountTypeForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/loadAddAccountTypeForm',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });

    }

    function loadSubAccountTypeForm() {
        ui("#proccessingPage");
        $.ajax({
            url: '/AxaBankFinance/subAccountTypes',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadSavingsNewCustomer() {
        $.ajax({
            url: '/AxaBankFinance/SavingsDebtorController/savingsNewCustomer',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadNewAccountForm() {
        $.ajax({
            url: '/AxaBankFinance/SavingsDebtorController/newSavingForm',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadSavingsDebtor() {
        $.ajax({
            url: '/AxaBankFinance/SavingsDebtorController/loadSavingsDebtorSearch',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function loadAddNewGoldPrice() {
        $.ajax({
            url: '/AxaBankFinance/loadAddNewGoldPrice',
            success: function (data) {
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

</script>






<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/18/2018
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $(".newTable").chromatable(
            {
                width: "100%", // specify 100%, auto, or a fixed pixel amount
                height: "15%",
                scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
            }
        );
        $('#downPaymentId').val(0);
        $(".box").draggable();
    });
</script>

<style>
    #groupLoadBox {
        -webkit-box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        -moz-box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        /*background: #BDBDBD;*/
        background: #BDBDBD;
    }

    #form_ {
        /*        -webkit-box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);
                -moz-box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);
                box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);*/
    }
</style>

<div class="row divError"></div>
<div class="row divSuccess">${saveUpdate}</div>

<form name="savingForm" id="newSavingForm">
    <div class="row">
        <input type="hidden" value="${branchCode}" id="branchCodeId"/>
        <input type="hidden" value="${systemDate}" id="systemDateId"/>
        <div class="col-md-4">
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;" id="form_">

        <div class="col-md-12 col-lg-12 col-sm-12">
            <input type="hidden" value="${edit}" id="testEdit"/>
            <c:choose>
                <c:when test="${edit}">
                    <div class="row"><label style="color: #08C; font-size: 14px">Edit Saving</label></div>
                </c:when>
                <c:otherwise>
                    <div class="row"><label style="color: #08C; font-size: 14px">Add New Saving</label></div>
                </c:otherwise>
            </c:choose>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent1" style="margin-bottom:15px ">
                    <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Member Number</div>
                    <div class="col-md-3"><input type="text" name="savingBookNo" value="${savingBookNo}"
                                                 required="true" style="width: 100%;" id="savingBookNoTxt"
                                                 class="col-md-3 col-lg-3 col-sm-2"
                                                 onchange="memberNumberPattern()" onblur="getMemberDetails()"/></div>

                    <div class="col-md-2" style="text-align: right;padding-left: 15px; padding-bottom: 10px"><strong>Saving
                        Id</strong></div>
                    <div class="col-md-3"><input type="text" style="color:#337ab7;width: 100%"
                                                 value="${savingId}" id="txtSavingId"
                                                 name="savingId" onkeypress="return isNumber(event)"/></div>
                    <div class="col-md-2">
                        <input type="button" class="btn btn-default searchButton"
                               onclick="searchSaving()"
                               style="height: 25px;"></div>
                    <input type="hidden" value="${savingId}" id="hSavingId"/>
                </div>
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="row">

                    <div class="row" style="  padding-top: 10px; border-radius: 7px;" id="groupLoadBox">
                        <div class="row" style="margin-left: 12px; font-weight: bold">Customer Details</div>

                        <!--CUSTOMER TABLE-->
                        <div id="groupUserTableDiv"></div>
                        <br/>

                        <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent">
                            <div class="row">
                                <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">NIC</div>
                                <div class="col-md-3"><input type="text" style="width: 100%;"
                                                             value="${debtor.debtorNic}"
                                                             readonly="true" id="debtorNic"/></div>

                                <div class="col-md-2"
                                     style="text-align: right;padding-left: 20px; padding-bottom: 10px">
                                    <strong>Name</strong></div>
                                <div class="col-md-3"><input type="text" style="width: 100%" id="debtorName"
                                                             readonly="true"
                                                             value="${debtor.debtorNameWithIni}"/></div>
                                <div class="col-md-1"><input type="button" class="btn btn-default searchButton"
                                                             id="btnSearchCustomer" style="height: 25px"></div>

                            </div>
                            <div class="row">
                                <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Address
                                </div>
                                <div class="col-md-3"><textarea name="" id="debtorAddress" readonly="true"
                                                                style="width: 100%">${debtor.debtorPersonalAddress}</textarea>
                                </div>
                                <div class="col-md-2"
                                     style="text-align: right;padding-left: 20px; padding-bottom: 10px">
                                    <strong>Contact No</strong></div>
                                <div class="col-md-3"><input type="text" readonly="true" style="width: 100%"
                                                             id="debtorTelephone"
                                                             value="${debtor.debtorTelephoneMobile}"/>
                                </div>
                            </div>

                            <input type="hidden" id="debID" name="debtorId" value="${debtor.debtorId}"/>
                            <input type="hidden" id="memberNumber" name="memberNumber" value="${debtor.memberNumber}"/>
                            <br/>
                        </div>

                        <div class="col-md-2 col-lg-2 col-sm-2" style="margin-left: -30px; margin-top: -20px;">
                            <div id="customerProfilePicture" height=" 120" width="120"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="row"><label style="text-align: left;padding-left: 10%;font-size: 12px; color: red"
                                            id="msgNicExits" class="msgTextField"></label></div>

                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent2" style="margin-bottom:15px ">
                            <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Saving Type
                            </div>

                            <div class="col-md-3">
                                <select name="savingTypeList" id="savingTypeList" style="width: 100%">
                                    <option>--SELECT--</option>
                                    <c:forEach var="savingType" items="${savingTypeList}">
                                        <option value="${savingType.savingsTypeId}">${savingType.savingsTypeName}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-md-2" style="text-align: right;padding-left: 15px; padding-bottom: 10px">
                                <strong>Sub Saving</strong></div>
                            <input type="hidden" id="hSubSavingType" value="${savingSubTypesList}"/>
                            <div class="col-md-3">
                                <select name="savingSubTypesList" id="savingSubTypesList" style="width: 100%">
                                    <%--<c:forEach var="savingSubTypesList" items="${hSubSavingType}">--%>
                                    <%--<option value="${subtypes.subSavingsId}">${subtypes.subSavingsName}</option>--%>
                                    <%--</c:forEach>--%>
                                </select>
                            </div>
                            <input type="hidden" name="savingType" value="${savings.savingsType}"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent3" style="margin-bottom:15px ">
                            <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Rate
                                (Annual)
                            </div>
                            <div class="col-md-3"><input type="text" id="subInterestRate" name="subInterestRate"
                                                         style="width: 100%" value="${subtypes.subInterestRate}"
                                                         readonly/></div>
                            <div class="col-md-1 edit" style="margin-top:5px;" onclick="editCharge(-1, 1)"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12" id="singleComponent4" style="margin-bottom:15px ">
                            <div class="col-md-2" style="text-align: left;font-weight: 600; color: black">Amount</div>
                            <div class="col-md-3"><input type="text" name="savingAmount" value="${savings.savingAmount}"
                                                         style="width: 100%"
                                                         id="savingAmountId" class=""
                                                         onkeypress="return checkNumbers(this)"/>
                            </div>
                            <div class="col-md-1 edit" title="Saving Capitalize" style="margin-top:5px;"
                                 onclick="loadSavingsCapitalizeForm()">
                            </div>
                        </div>
                    </div>

                    <div class="row"
                         style="display: none; border:1px solid #00bb8c; border-radius: 1px;margin-bottom: 5px;margin-top: 5px">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-5" style="width: 26%"><label>Book No</label></div>
                                <div class="col-md-3"><input type="text" name="bookNo"/></div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-left: 10px">
                            <div class="row">
                                <div class="col-md-5"><label>Start Date</label></div>
                                <div class="col-md-7"><input type="text" class="txtCalendar" name="startDate"/></div>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-left: 72px">
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                </div>
            </div>

            <c:choose>
                <c:when test="${edit}">
                    <div class="row panel-footer" style="padding-top: 20px;">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                               value="Cancel"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                               onclick="uploadDocuments()"
                                                                               value="Upload Documents"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                               id="btnUpdate"
                                                                               onclick="saveSavingsForm()"
                                                                               value="Update"/></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-8"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           id="btnProcess"
                                                                           onclick="processLoanToApproval()"
                                                                           value="Process" style="width: 100px"/></div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row panel-footer" style="padding-top: 10px;">
                        <div class="col-md-6 col-lg-6 col-sm-6"></div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-success col-md-12 col-lg-12 col-sm-12"
                                                                               style="border-radius: 5px;" id="btnSave"
                                                                               onclick="saveSavingsForm()"
                                                                               value="Save"/>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-danger col-md-12 col-lg-12 col-sm-12"
                                                                               style="border-radius: 5px;"
                                                                               value="Cancel"/>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-warning col-md-12 col-lg-12 col-sm-12"
                                                                               style="border-radius: 5px;"
                                                                               value="Exit"/>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </c:otherwise>
            </c:choose>

            <input type="hidden" id="gurant_debtr_type"/>
            <div class="row" id="hiddenContent"></div>
            <div class="row" id="divCapitalizeTypes"></div>
            <%--<input type="hidden" value="${savings.loanAmount}" style="width: 100%" id="loanAmountId2"/>--%>
            <%--<input type="hidden" name="loanRescheduleId" value="${savings.loanRescheduleId}" style="width: 100%"/>--%>
        </div>
    </div>
</form>
</div>

<!--Search Customer-->
<div class="searchCustomer box"
     style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto; left: 15%"
     id="popup_searchCustomer">

</div>

<!--Search supplier,sales Manager, recovery manager-->
<div class="searchCustomer" style="margin-top: 5%; margin-left: 15%;" id="popup_searchOfficers">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label id="app_title"></label></div>
    <div class="row" style="margin: 10px;">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    Search By Name
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <input type="text" name="" class="col-md-12 col-lg-12 col-sm-12" style="padding: 1px"/>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1"></div>
            </div>
        </div>
    </div>

    <div class="row col-md-10" style="margin:20px; height: 80%; overflow-y: auto">
        <table class="table table-bordered table-edit table-hover table-responsive" id="tblOfficers"
               style="font-size: 12px;">
            <thead>
            <tr>
                <td>Emp No</td>
                <td>Name</td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4"></div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" onclick="addOfficerToLoanForm();"><span
                            class="glyphicon glyphicon-ok-circle"></span> Add
                    </button>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" id="btnSearchUseCancel" onclick="cancelOfficersTable();">
                        <span class="glyphicon glyphicon-remove-circle"></span> Cancel
                    </button>
                </div>
            </div>
        </div>
        <input type="hidden" id="hidOfficerType"/>
    </div>
</div>
<div id="message_newLonForm" class="searchCustomer box"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="docDivCheck" class="searchCustomer box" style="width: 85%;border: 0px solid #5e5e5e;margin-left:-15%"></div>
<div class="searchCustomer box" id="editRate_div"
     style="width: 40%; margin-left: 10%; margin-top: 10%; border:4px solid #1992d1"></div>
<input type="hidden" value="${message}" id="testMessage"/>
<div id="divLoanCapitalizeForm" class="searchCustomer"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..."/>
</div>
<!--BlockUI-->

<script type="text/javascript">

    //format desimal numbers
    $('.decimalNumber').autoNumeric();

    $('#savingTypeList').change(function () {
        var savingType = $("#savingTypeList").find('option:selected').val();
        if (savingType !== "0") {
            $.ajax({
                url: "/AxaBankFinance/subAccountTypes/" + savingType,
                success: function (data) {
                    if (data.length > 0) {
                        $("#savingSubTypesList").html("");
                        for (var i = 0; i < data.length; i++) {
                            $("#savingSubTypesList").append("<option value='" + data[i].subSavingsId + "'>" + data[i].subSavingsName + "</option>");
                        }
                        $("#savingSubTypesList").removeAttr("disabled");

                    } else {
                        $("#savingSubTypesList").html("");
                        $("#savingSubTypesList").attr("disabled", true);

                    }
                }
            });
        } else {
            $("#savingSubTypesList").html("");
            $("#savingSubTypesList").attr("disabled", true);
        }
    });

    $('#savingSubTypesList').click(function () {
        var subSavingType = $("#savingSubTypesList").find('option:selected').val();

        if (subSavingType !== "0") {
            $.ajax({
                url: "/AxaBankFinance/subAccountTypes/" + subSavingType,
                success: function (data) {
                    if (data.length > 0) {
                        $("#subInterestRate").html("");
                        for (var i = 0; i < data.length; i++) {
                            $("#subInterestRate").val(data[i].subInterestRate);
                        }
                        $("#subInterestRate").removeAttr("disabled");

                    } else {
                        $("#subInterestRate").html("");
                        $("#subInterestRate").attr("disabled", true);

                    }
                }
            });
        } else {
            $("#subInterestRate").html("");
            $("#subInterestRate").attr("disabled", true);
        }
    });

    function clickSavingsSubtype(data) {
        var savingsTypeId = $(data).find("input").val();
        var rate = $(data).find('a').text();
        $("#viewRate").val(rate);
        $("input[name=subInterestRate]").val(rate);
        $("input[name=savingsTypeId]").val(savingsTypeId);
        unblockui();
        $("#savingAmountId").focus();
        loadOtherCharges(savingsTypeId);
    }

    function sayHello(a) {
        alert("say hello " + $(this).val());
    }

    function getSameInstallment() {
//    var totalAmount =
    }

    function getCustomInstallment() {

    }

    function setEqualAmount(cb) {

// validate the account amounts and the periods

        var accountAmount = $("#accountId").val();

        var debtors = $(".debtorTable tr").length - 1;
        var singleValue = loanAmount / debtors;

        $(".userAmount").each(function () {
            $(this).val(singleValue);
        });

        if (cb.checked == true) {

            if (loanAmount == 0) {

                $("#checkEqualAmount").prop("checked", false);
                $("#loanAmountId").focus();

                $(".divError").html("");
                $(".divError").css({'display': 'block'});
                $(".divError").html("Enter loan amount");
                $("html, body").animate({scrollTop: 0}, "slow");
                $("#loanAmountId").addClass("txtError");
                return;
            } else {
                $(".divError").html("");
                $(".divError").css({'display': 'none'});
                $(".divError").html("");
                $("#loanAmountId").removeClass("txtError");
            }

            $(".userAmount").each(function () {
                $(this).prop("readonly", true);
            });

        } else {
            $(".userAmount").each(function () {
                $(this).prop("readonly", false);
                $(this).val("");

            });
        }

    }

    function setEqualPeriod(cb) {

// validate the  loan amounts and the periods

        var periods = $("#loanPeriods").val();

        $(".userPeriod").each(function () {
            $(this).val(periods);
        });


        if (cb.checked == true) {

            if (periods == 0) {
                $("#checkEqualPerod").prop("checked", false);
                $("#loanPeriods").focus();

                $(".divError").html("");
                $(".divError").css({'display': 'block'});
                $(".divError").html("Enter loan period");
                $("html, body").animate({scrollTop: 0}, "slow");
                $("#loanPeriods").addClass("txtError");
                return;
            } else {
                $(".divError").html("");
                $(".divError").css({'display': 'none'});
                $(".divError").html("");
                $("#loanPeriods").removeClass("txtError");
            }

            $(".userPeriod").each(function () {
                $(this).prop("readonly", true);
            });
        } else {
            $(".userPeriod").each(function () {
                $(this).prop("readonly", false);
                $(this).val("");
            });
        }
    }

    function setEqualRate(cb) {

// validate the  loan amounts and the periods

        var rate = $("#loanRateId").val();


        if (rate == 0) {
            $("#checkEqualRate").prop("checked", false);
            $("#loanRateId").focus();

            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html("Enter loan rate");
            $("html, body").animate({scrollTop: 0}, "slow");
            $("#loanRateId").addClass("txtError");
            return;
        } else {
            $(".divError").html("");
            $(".divError").css({'display': 'none'});
            $(".divError").html("");
            $("#loanRateId").removeClass("txtError");

            $(".userRate").each(function () {
                $(this).val(rate);
            });
            calGroupData();
        }


        if (cb.checked == true) {
            $(".userRate").each(function () {
                $(this).prop("readonly", true);
            });
        } else {
            $(".userRate").each(function () {
                $(this).prop("readonly", false);
                $(this).val("");
            });
        }
    }

    function handleChange(cb) {
        $("#groupAccountID").val("0");
        if (cb.checked == true) {
            $('#groupComponent').show("slow");
            $('#singleComponent').hide("slow");
            $("#branchID").prop("disabled", false);
            $("#centeID").prop("disabled", false);
            $("#groupID").prop("disabled", false);
            $("#isGroupLoan").val("1");
            $('#guarantorDetailsDiv').hide("2500");
            $('#singleLoanSummaryDiv').hide("2500");


        } else {
            $('#groupComponent').hide('slow');
            $('#singleComponent').show("slow");
            $("#branchID").prop("disabled", true);
            $("#centeID").prop("disabled", true);
            $("#groupID").prop("disabled", true);
            $("#isGroupLoan").val("0");
            $('#loanTypeGroup').hide("slow");
            $('#guarantorDetailsDiv').show("2500");
            $('#singleLoanSummaryDiv').sho("2500");
        }

        $("#branchID").prop("selectedIndex", 0);
        $("#centeID").hide();
        $("#groupID").hide();
        $("#groupUserTableDiv").html("");

    }

    function getGroupUsers() {
        $('#groupUserTableDiv').hide("slow");
        var groupID = $("#groupID").val();

        $("#groupAccountID").val(groupID);


        $.ajax({
            url: "/AxaBankFinance/loadGroupUsers/" + groupID,
            beforeSend: function (xhr) {
                $("#loadingImg3").show();
                $("#groupUserTableDiv").html("");

            },
            success: function (data) {

                $('#loanTypeGroup').show("slow");

                $("#groupUserTableDiv").html("");
                $("#groupUserTableDiv").html(data);
                $("#loadingImg3").hide();
                $('#groupUserTableDiv').show("slow");
            }
        });

    }

    var accountCapitalizeDetails = [];

    //Block Load SearchCustomer Form
    $(document).ready(function () {

        $("#loadingImg1").hide();
        $("#loadingImg2").hide();
        $('#groupComponent').hide();
        $('#groupUserTableDiv').hide();
        $("#loadingImg3").hide();
        $("#isGroupLoan").val("0");
        $("#groupAccountID").val("0");
        $('#accountTypeGroup').hide();

        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });

        $("#txtRateType").val("FR");

        $('#btnLoadGuarantor').click(function () {
            ui('#popup_searchCustomer');
            loadSearchCustomer(2);
        });
        $('#btnSearchCustomerExit').click(function () {
            unblockui();
        });

        //Block Load GuarantorForm(Load SearchCustomer Form)
        $('#btnSearchCustomer').click(function () {
            ui('#popup_searchCustomer');
            loadSearchCustomer(1);
        });
        $('#btnSearchCustomerExit').click(function () {
            unblockui();
        });

        //Block Load Marketing Officer Form
        $('#btnLoadMarketingOfficer').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign Marketing Officer");
            searchUserType(0);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });
        //Block Laod Marketing Manager Form
        $('#btnLoadSaleParsonName').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 1st approval");
            searchUserType(1);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load Recovery Manager Form
        $('#btnRecoveryManager').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 2nd approval");
            searchUserType(2);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load Supplier Form
        $('#btnSupplier').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 3rd approval");
            searchUserType(3);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load property Form
        $('#btnAddProperty').click(function () {
            ui('#popup_addProperty');
            loadPropertyForm();
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });
    });

    $(function () {
        jQuery.extend(jQuery.expr[':'], {
            focusable: function (el, index, selector) {
                return $(el).is('a, button, :input, [tabindex]');
            }
        });

        //got to next input from enter key
        $(document).on('keypress', 'input,select,textarea', function (e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(':focusable');
                var index = $canfocus.index(this) + 1;
                if (index >= $canfocus.length)
                    index = 0;
                $canfocus.eq(index).focus();
            }
        });

        //press enter on button
        $('input[type="button"]').on("keypress", function (eve) {
            var key = eve.keyCode || e.which;
            if (key == 13) {
                $(this).click();
            }
            return false;
        });

        $('input[name=loanAmount]').keyup(function (e) {

            var loanAmount = $("#loanAmountId").val().trim();
            if (loanAmount !== "") {
                $("#loanAmountId").removeClass("txtError");
                $("#loanAmountId2").val(loanAmount);
                $("#viewInvestment").val(loanAmount);
            }

            $("#viewLoanAmount").autoNumeric();
            $("#viewLoanAmount").val(loanAmount);

            calculateCharge();
        });

        //focus other input field other than loan period
        $("input[name=loanPeriod]").keyup(function () {
            var period = $(this).val();
            $("#viewLoanPeriod").val(period);
        });

        //focus other input field other than loan downpayment
        $("input[name=loanDownPayment]").keyup(function () {
            calculateInvestment();

        });


        $('input[name=loanId]').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (code === 13) {
                searchAccount();
            }
        });

    });

    function sameLoanValidationForCustomer(loanTypeId) {
        var savingsBookNo = $("#savingsBookNoTxt").val();
        var debtorID = $("#debID").val();
        if (savingsBookNo === "" || savingsBookNo === null) {
//            alert("Member Number Cannot Be Empty.!");
            document.getElementById("btnSave").disabled = true;
        } else {
            $.ajax({
                url: '/AxaBankFinance/loanValidation/' + loanTypeId + "/" + debtorID,
                success: function (data) {
                    if (data > 0) {
                        $("#msgNicExits").html("This Customer has a On Going Loan.!");
                        $("#loanNic_id").addClass("txtError");
                        $("#loanNic_id").focus();
                        document.getElementById("btnSave").disabled = true;
                    } else {
                        $("#loanNic_id").removeClass("txtError");
                        $("#msgNicExits").html("");
                        $("#msgNicExits").removeClass("msgTextField");
                        $("#msgNicExits").html("");
                        document.getElementById("btnSave").disabled = false;
                    }
                }
            });
        }
    }

    //    Edit Rate
    function editRate() {
//        var loanRate = $(subLoan).val();
        alert("loanRate");
    }

    //load other charges types
    function loadOtherCharges(subId) {
        $.getJSON('/AxaBankFinance/otherChargesByLoanId/' + subId, function (data) {
            $('#otherChargesTable tbody').html("");

            for (var i = 0; i < data.length; i++) {
                var description = data[i].description;
                var isPresentage = data[i].isPrecentage;
                var charge, t, rateCharge;
                if (isPresentage) {
                    charge = data[i].taxRate;
                    rateCharge = charge + "%";
                    t = 1;
                } else {
                    charge = data[i].amount;
                    rateCharge = charge;
                    t = 0;
                }
                var id = data[i].id;
                var is_added = 0;
                var isAdded = data[i].isAdded;
                if (isAdded) {
                    is_added = 1;
                }
                $('#otherChargesTable tbody').append("<tr id='" + id + "'>" +
                    "<td>" + description + "<input type='hidden' value='" + t + "' id='t" + i + "'/><input type='hidden' value='" + id + "' id='cha" + i + "'/><input type='hidden' id='isadd" + i + "' value='" + is_added + "'/></td>" +
                    "<td><input type='hidden' value='" + charge + "' id='c" + i + "'/>" + "<a href='#' onclick='editCharge(" + i + ",1)' id='aaa" + i + "'>" + rateCharge + "</a></td>" +
                    "<td style='text-align:right' id='amount" + i + "'><a href='#' onclick='editCharge(" + i + ",2)' style='text-align: right;' id='hidAmnt" + i + "'></a></td></tr>");
            }
            $('#otherChargesTable tbody').append("<tr><td colspan='2'><label>Total</label></td><td class='decimalNumber'><label id='totalOther'><label></td>");
        });

    }

    //click loan other charges row
    var totalCharges = 0.00;
    var chargesAddedToLoan = 0.00;

    function calculateCharge() {
        totalCharges = 0.00;
        var i = 0;
        var loanAmount = $("input[name=loanAmount]").val();
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("last");
            } else {
                var labelId = "amount" + i;
                var amount;
                var chargeType = $("#t" + i).val();
                var charge = $("#c" + i).val();
                var isAdded = $("#isadd" + i).val();

                if (chargeType == 1) {
                    if (loanAmount != "") {
                        amount = ((charge * parseFloat(loanAmount)) / 100).toFixed(2);
                    } else {
                        amount = (parseFloat(0)).toFixed(2);
                    }
                } else {
                    amount = (parseFloat(charge)).toFixed(2);
                }
                if (isAdded == 1) {
                    chargesAddedToLoan = chargesAddedToLoan + amount;
                }

                if (isAdded == 0) {
                    totalCharges = totalCharges + parseFloat(amount);
                }

                $("#hidAmnt" + i).text(amount);
                $("#totalOther").text(totalCharges.toFixed(2));
            }
            i++;
        });
    }

    function loadSearchCustomer(type) {
        $("#gurant_debtr_type").val(type);
        $.ajax({
            url: '/AxaBankFinance/searchCustomer',
            success: function (data) {
                $('#popup_searchCustomer').html("");
                $('#popup_searchCustomer').html(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    function unloadSearchCustomer() {
        unblockui();
    }

    function saveSavingsForm() {
        //check user is block
        var edit = '${edit}';

        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function (data) {
                if (!data) {
                    var validate = validateForm();
                    if (validate) {
                        if (!edit) {
                            document.getElementById("btnSave").disabled = true;
                        }
                        ui("#proccessingPage");
                        saveSaving();
                    } else {
                        alert("Error 02");
                        $(".divError").html("");
                        $(".divError").css({'display': 'block'});
                        $(".divError").html(errorMessage);
                        errorMessage = "";
                        $("html, body").animate({scrollTop: 0}, "slow");
                    }
                } else {
                    warning("User Is Blocked", "message_newLonForm");
                }
            },
            error: function () {
                alert("Error 03");
                alert("Error Loading...");
            }
        });
    }

    function saveSaving() {
        $.ajax({
            url: '/AxaBankFinance/SavingsController/saveNewSavingsForm',
            type: 'POST',
            data: $("#newSavingForm").serialize(),
            success: function (data2) {
                $('#formContent').html(data2);
                // $(".divSuccess").css({'display': 'block'});
                $("html, body").animate({scrollTop: 0}, "slow");
                alert("Savings Account Saved Successfully..!")
            },
            error: function () {
                alert("Error Loading...");
            }
        });
        $.unblockUI();
    }

    function SetOtherCharges() {
        var i = 0;
        $("#hidCharges").html("");
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("lastRO");
            } else {
                console.log("ROW" + i);
                var chargeId = $("#cha" + i).val();
                var charge = $("#c" + i).val();
                console.log(charge);
                var amount = $("#hidAmnt" + i).text();
                $("#hidCharges").append("<input type='hidden' name='chragesId' value='" + chargeId + "'/>" +
                    "<input type='hidden' name='chragesRate' value='" + charge + "'/>" +
                    "<input type='hidden' name='chragesAmount' value='" + amount + "'/>");
            }
            i++;
        });
    }

    $('#txtRateType').change(function () {

    });

    function parseNumber(n) {
        var f = parseFloat(n); //Convert to float number.
        return isNaN(f) ? 0 : f; //treat invalid input as 0;
    }

//     function calGroupData() {
//
//         var loanType = $("#loanTypeList").find('option:selected').val();
//         var table = document.getElementById("tabela");
//         var rowCount = table.rows.length - 1;
//
//         if (loanType == 0)
//             return;
//         var downPayment = 0;
//         var loanAmount = $("input[name=loanAmount]").val();
//
//
//         loanAmount = loanAmount / rowCount;
//
//
//         if (loanAmount == "")
//             return;
//         var investment = 0.00;
//         if (downPayment > 0) {
//             investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
//         } else {
//             investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
//         }
// //        $("input[name=loanInvestment]").val(investment);
//
// //        var amount = $("input[name=loanInvestment]").val();
//         var amount = loanAmount;
//         if (amount == null || amount == "")
//             return;
//         var period = $("#viewLoanPeriod").val();
//         if (period == null || period == "")
//             return;
//
// //        var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
//         var decimalAmount = amount;
//         var periodType = $("#cmbPeriodType").find('option:selected').val();
//
//
//         var newRate = $("#viewRate").val();
//         var rate = newRate.replace('%', '');
//         var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);
//
//         var totalInterest, totalAmount, installment, total;
//
//
//         if (periodType === '3') {
//             if (loanType === '10') {// For STLs
//                 totalInterest = ((parseFloat(interest) * period)).toFixed(2);
//                 totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
//                 installment = (parseFloat(totalAmount) / period).toFixed(2);
//                 total = parseFloat(totalAmount).toFixed(2);
//
//             } else {
//                 totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
//                 totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
//                 installment = (parseFloat(totalAmount) / period).toFixed(2);
//                 total = parseFloat(totalAmount).toFixed(2);
//             }
//         } else {
//             interest = interest / period;
//             totalInterest = (parseFloat(interest) * period).toFixed(2);
//             totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
//             installment = (parseFloat(totalAmount) / period).toFixed(2);
//             total = parseFloat(totalAmount).toFixed(2);
//
//         }
//
//         var tbl = document.getElementById("tabela");
//
//         var rowCount = $(".debtorTable tr").length;
//
//         for (var i = 1; i < rowCount; i++) {
//             tbl.rows[i].cells[7].children[0].value = interest;
//             tbl.rows[i].cells[8].children[0].value = totalInterest;
//             tbl.rows[i].cells[9].children[0].value = installment;
//             tbl.rows[i].cells[10].children[0].value = total;
//         }
//
//         $("#viewInstallment").val(installment);
//         $("#viewInterest").val(interest);
//         $("#viewTotalInterest").val(totalInterest);
//         $("#viewTotal").val(total);
//
//     }
    
//     function calculateInvestment() {
//         var loanAmount = $("input[name=loanAmount]").val();
//         var downPayment = $("input[name=loanDownPayment]").val();
//         if (loanAmount !== "" && downPayment !== "") {
//             var f_amount = parseFloat(loanAmount);
//             var f_downpayment = parseFloat(downPayment);
//             if (f_amount > f_downpayment) {
//                 var investment = 0.00;
//                 investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
//
//                 $("input[name=loanInvestment]").val(investment);
//                 $("#viewDownPayment").val(downPayment);
//                 $("#viewInvestment").val(investment);
// //                calculateCharge();
//             } else {
//                 alert("Loan amount should be greater than down payment");
//                 $("input[name=loanDownPayment]").css("border", "1px solid red");
//                 $("input[name=loanDownPayment]").focus();
//             }
//         }
//     }

    var errorMessage = "";

    function validateForm() {

        var validate = true;

        // var savingsBookNo = $("#savingsBookNoTxt").val();

        // if (savingsBookNo === null || savingsBookNo === '') {
        //     validate = false;
        //     $(".divSuccess").css({'display': 'none'});
        //     errorMessage = errorMessage + "<br>" + "Enter Member Number";
        //     $("#savingsBookNoTxt").addClass("txtError");
        // } else {
        //     $("#savingsBookNoTxt").removeClass("txtError");
        // }

//         var systemDate = $("#dueDayTxt").val();
//         var splitSystemDateId = systemDate.substring(8, 10);
//         $("#dueDayTxt").val(splitSystemDateId);
//         $("#dueDateTxt").val(systemDate);
//
//         var branchCodeId = $("#branchCodeId").val();
//         var branchCode = $("#savingsBookNoTxt").val();
//         var splitBranchCode = branchCode.substring(0, 2);
//         if (branchCodeId !== splitBranchCode) {
//             validate = false;
//             $(".divSuccess").css({'display': 'none'});
//             errorMessage = errorMessage + "<br>" + "Invalid Member Number";
//             $("#branchCodeId").addClass("txtError");
//         } else {
// //            $("#savingsBookNoTxt").removeClass("txtError");
//         }

        // if (groupLoan == 1) {
        //
        //     if (branchID == 0) {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Select Branch";
        //         $("#loanTypeList").addClass("txtError");
        //     } else {
        //         $("#loanTypeList").removeClass("txtError");
        //     }
        //
        //     if (centerID == 0) {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Select Center";
        //         $("#loanTypeList").addClass("txtError");
        //     } else {
        //         $("#loanTypeList").removeClass("txtError");
        //     }
        //
        //     if (groupID == 0) {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Select Group";
        //         $("#loanTypeList").addClass("txtError");
        //     } else {
        //         $("#loanTypeList").removeClass("txtError");
        //     }
        // }

        // var loanType = $("#loanTypeList").find('option:selected').val();
        // if (loanType === 0) {
        //     validate = false;
        //     $(".divSuccess").css({'display': 'none'});
        //     errorMessage = errorMessage + "<br>" + "Select LoanType";
        //     $("#loanTypeList").addClass("txtError");
        // } else {
        //     $("#loanTypeList").removeClass("txtError");
        // }
        // var loanAmount = $('input[name=loanAmount]').val();
        // if (loanAmount == null || loanAmount == "") {
        //     validate = false;
        //     $(".divSuccess").css({'display': 'none'});
        //     errorMessage = errorMessage + "<br>" + "Enter Loan Amount";
        //     $("#loanAmountId").addClass("txtError");
        // } else {
        //     $("#loanAmountId").removeClass("txtError");
        // }

        // var period = $('input[name=loanPeriod]').val();
        // if (period == null || period == "") {
        //     validate = false;
        //     $(".divSuccess").css({'display': 'none'});
        //     errorMessage = errorMessage + "<br>" + "Enter Loan Time Period";
        //     $('input[name=loanPeriod]').addClass("txtError");
        // } else {
        //     $('input[name=loanPeriod]').removeClass("txtError");
        // }

        // if (!$('#checkBoxLoanType').is(":checked")) {
        //     var debtorID = $("#debID").val();
        //     if (debtorID == null || debtorID == "") {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Select a Debtor";
        //         $("#debtorName").addClass("txtError");
        //     } else {
        //         $("#debtorName").removeClass("txtError");
        //     }
        // }

        //validate approvelevels
        // var app1 = $("#salesPersonName").val();
        // var app2 = $("#recoveryPersonName").val();
        // var app3 = $("#supplierName").val();
        // if (app1 === "") {
        //     var comment1 = $("#app1_comment").val();
        //     if (comment1 === "") {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Add Comment If you are skipped first approval";
        //         $("#app1_comment").addClass("txtError");
        //     } else {
        //         $("#app1_comment").removeClass("txtError");
        //     }
        // } else {
        //     $("#app1_comment").removeClass("txtError");
        // }
        // if (app2 === "") {
        //     var comment2 = $("#app2_comment").val();
        //     if (comment2 === "") {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Add Comment If you are skipped second approval";
        //         $("#app2_comment").addClass("txtError");
        //     } else {
        //         $("#app2_comment").removeClass("txtError");
        //     }
        // } else {
        //     $("#app2_comment").removeClass("txtError");
        // }
        // if (app3 === "") {
        //     validate = false;
        //     $(".divSuccess").css({'display': 'none'});
        //     errorMessage = errorMessage + "<br>" + "You Should add final approval";
        //     $("#supplierName").addClass("txtError");
        // } else {
        //     $("#supplierName").removeClass("txtError");
        // }

        // var duedate = $("input[name=dueDay]").val();
        // if (duedate == "") {
        //     validate = false;
        //     $(".divSuccess").css({'display': 'none'});
        //     errorMessage = errorMessage + "<br>" + "Please Enter Due Date";
        //     $("input[name=dueDay]").addClass("txtError");
        // } else {
        //     if (duedate > 31 || duedate == 0) {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Please Enter Valid Date for Due Date";
        //         $("input[name=dueDay]").addClass("txtError");
        //     } else {
        //         $("input[name=dueDay]").removeClass("txtError");
        //     }
        // }

        // var loanId = $("input[name=loanId]").val();
        // var hideLoan = $('#hLoanId').val();
        //
        // if (loanId !== "") {
        //     if (loanId != hideLoan) {
        //         validate = false;
        //         $(".divSuccess").css({'display': 'none'});
        //         errorMessage = errorMessage + "<br>" + "Can't Update this Loan" + "<br>" + "Invalide Loan Id";
        //     }
        // }

        return validate;
    }

    //add sales person,recovery manager,
    function searchUserType(type) {
        $("#hidOfficerType").val(type);
        var loanTypeId = $("input[name=loanType]").val();
        if (loanTypeId === "") {
            loanTypeId = 0;
        }
        $.ajax({
            url: '/AxaBankFinance/SearchOfficers/' + type + "/" + loanTypeId,
            success: function (data) {
                var tbody = $("#tblOfficers tbody");
                tbody.html("");
                for (var i = 0; i < data.length; i++) {
                    tbody.append("<tr onclick='clickOfficer(this)'><td>" + data[i].empId + "</td>" +
                        "<td>" + data[i].userName + "</td>" +
                        "<td style='display:none'>" + data[i].userId + "</td></tr>");
                }
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //search customer by nic
    $('#debtorNic').keyup(function (e) {

        var nicNo = $(this).val();
//        if (nicNo.length === 10) {
        $.ajax({
            dataType: 'json',
            url: '/AxaBankFinance/searchByNic_new/' + nicNo,
            success: function (data) {
                var debId = data.debtorId;
                if (debId > 0) {
                    var name = "", nic = "", address = "", telephone = "", memberNo = "";
                    if (data.nameWithInitial !== null)
                        name = data.nameWithInitial;
                    if (data.debtorNic !== null)
                        nic = data.debtorNic;
                    if (data.debtorPersonalAddress !== null)
                        address = data.debtorPersonalAddress;
                    if (data.debtorTelephoneMobile !== null)
                        telephone = data.debtorTelephoneMobile;
                    if (data.memberNumber !== null)
                        memberNo = data.memberNumber;

                    $("#debID").val(debId);
                    $("#debtorNic").val(nic);
                    $("#debtorName").val(name);
                    $("#debtorAddress").val(address);
                    $("#debtorTelephone").val(telephone);
                    $("#memberNumber").val(memberNo);

                    var url2 = "/AxaBankFinance/viewProfilePicture/" + debId;
                    $("#customerProfilePicture").html("<img src='" + url2 + "' ></img>");

                } else {
                    alert("Search Result Not found");
                    $("#debtorNic").val("");
                    $("#debID").val("");
                    $("#debtorName").val("");
                    $("#debtorAddress").val("");
                    $("#debtorTelephone").val("");
                    $("#memberNumber").val("");
                }

            },
            error: function () {
                alert("Error Loading...");
            }
        });
//        }
    });

    //search loan by loan id
    function searchAccount() {

        var loanId = $("input[name=loanId]").val();
        var view = "message_newLonForm";
        if (loanId !== "") {
            $.ajax({
                url: '/AxaBankFinance/searchLoanDeatilsById/' + loanId,
                success: function (data) {
                    $('#formContent').html(data);
                    document.getElementById("txtLoanId").readOnly = true;
                    var msg = $("#testMessage").val();
                    if (msg != "") {
                        warning(msg, view);
                    }
                },
                error: function () {
                    alert("Error Loading...");
                }
            });

        } else {
            alert("Enter Loan Id");
        }

    }

    //proceed button
    function processLoanToApproval() {
        document.getElementById("btnProcess").disabled = true;
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loanDocumentSubmissionStatus/" + loan_id,
            success: function (data) {
                var process = data;
                var view = "message_newLonForm";
                if (process == 1) {
                    //cannot process
                    var msg = "You have to submit minimum documents to proceed";
                    warning(msg, view);
                } else if (process == 2) {
                    //process with pending dates
                    var msg = "Do you want to proceed loan without completing all documents";
                    dialog(msg, view, loan_id);
                } else {
                    //procees successfully
                    ui("#proccessingPage");
                    viewAllLoans(0);
                }
            },
            error: function () {

            }
        });
    }

    //document upload button
    function uploadDocuments() {
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loadDocumentChecking",
            data: {loan_id: loan_id},
            success: function (data) {
                $("#docDivCheck").html(data);
                ui('#docDivCheck');
            }
        });
    }

    //edit charge
    function editCharge(row, type) {
        console.log(row);
        $.ajax({
            url: "/AxaBankFinance/loadEditRate/" + row + "/" + type,
            success: function (data) {
                $("#editRate_div").html(data);
                ui("#editRate_div");
            }
        });
    }

    function loadAccountCapitalizeForm() {
        var accountAmount = $("#accountId").val().trim();
        var accountId = $("#hAccountId").val().trim();
        if (accountAmount !== "") {
            $("#accountAmountId").removeClass("txtError");
            if (accountId !== "") {
                editLoanCapitalizeForm(accountId);
            } else {
                addLoanCapitalizeForm();
            }
        } else {
            $("#accountId").addClass("txtError");
            $("#accountId").focus();
        }
    }

    function setCapitalizeTypes() {
        if (loanCapitalizeDetails.length !== 0) {
            $('#divCapitalizeTypes').html("");
            for (var i = 0; i < loanCapitalizeDetails.length; i++) {
                $('#divCapitalizeTypes').append("<input type = 'hidden' name = 'capitalizeAmount' value = '" + loanCapitalizeDetails[i].amount + "'/>" +
                    "<input type = 'hidden' name = 'capitalizeId' value ='" + loanCapitalizeDetails[i].MCaptilizeType.id + "'/>");
            }
        }
    }

    function loadSavingsCapitalizeForm() {
        $.ajax({
            url: "/AxaBankFinance/loadLoanCapitalizeForm",
            success: function (data) {
                $("#divLoanCapitalizeForm").html("");
                $("#divLoanCapitalizeForm").html(data);
                if (loanCapitalizeDetails.length !== 0) {
                    loadLoanCapitalizeDetail();
                    ui("#divLoanCapitalizeForm");
                } else {
                    ui("#divLoanCapitalizeForm");
                }
            }
        });
    }

    function editLoanCapitalizeForm(loanId) {
        var loanAmount = "${loan.loanAmount}";
        loanAmount = parseFloat(loanAmount);
        var capitalizeAmount = "${capitalizeAmount}";
        capitalizeAmount = parseFloat(capitalizeAmount);
        loanAmount = loanAmount - capitalizeAmount;
        $("#loanAmountId2").val(loanAmount);
        $.ajax({
            url: "/AxaBankFinance/editLoanCapitalizeForm/" + loanId,
            success: function (data) {
                $("#divLoanCapitalizeForm").html("");
                $("#divLoanCapitalizeForm").html(data);
                if (loanCapitalizeDetails.length !== 0) {
                    loadLoanCapitalizeDetail();
                    ui("#divLoanCapitalizeForm");
                } else {
                    ui("#divLoanCapitalizeForm");
                }
            }
        });
    }

    $("#branchID").change(function () {

        var branchId = $("#branchID").val();
        $('#loanTypeGroup').hide("slow");
        $('#tabela').hide("slow");

        $.ajax({
            url: "/AxaBankFinance/loadCenter/" + branchId,
            beforeSend: function (xhr) {
                $("#loadingImg1").show();
                $("#centerDropDown").html("");
            },
            success: function (data) {
                $("#centerDropDown").html("");
                $("#centerDropDown").html(data);
                $("#loadingImg1").hide();
            }
        });

    });

    function getGroup() {
        var centeID = $("#centeID").val();
        $('#loanTypeGroup').hide("slow");
        $('#tabela').hide("slow");

        $.ajax({
            url: "/AxaBankFinance/loadGroup/" + centeID,
            beforeSend: function (xhr) {
                $("#loadingImg2").show();
                $("#groupDropDown").html("");
            },
            success: function (data) {
                $("#loadingImg2").hide();
                $("#groupDropDown").html("");
                $("#groupDropDown").html(data);
            }
        });
    }

    function memberNumberPattern() {
        $("#savingsBookNoTxt").mask("a*/999/99/999");
    }

    function getMemberDetails() {
        var savingsBookNo = $("#savingsBookNoTxt").val();
        var changeSavingsBookNo = savingsBookNo.replace(/[^a-zA-Z0-9]/g, '-');

        if (savingsBookNo === "" || savingsBookNo === null) {
//            alert("Member Number Cannot Be Empty.!");
            document.getElementById("btnSave").disabled = true;
        } else {
            $.ajax({
                url: '/AxaBankFinance/debtorAdd/' + changeSavingsBookNo,
                success: function (data) {
                    if (data !== null) {
                        var debtorId = "";
                        var debtorNic = "";
                        var debtorPersonalAddress = "";
                        var debtorTelephoneMobile = "";
                        var debtorName = "";
                        for (var p in data) {
                            var result = "";

                            if (data.hasOwnProperty(p)) {
                                result += p + " , " + data[p] + "\n";
                                if (p === 'debtorId') {
                                    debtorId = data[p];
                                }
                                if (p === 'debtorNic') {
                                    debtorNic = data[p];
                                }
                                if (p === 'debtorPersonalAddress') {
                                    debtorPersonalAddress = data[p];
                                }
                                if (p === 'debtorTelephoneMobile') {
                                    debtorTelephoneMobile = data[p];
                                }
                                if (p === 'debtorName') {
                                    debtorName = data[p];
                                }
                            }
                        }

                        $('#debID').val(debtorId);
                        $('#debtorNic').val(debtorNic);
                        $("#debtorName").val(debtorName);
                        $("#debtorAddress").val(debtorPersonalAddress);
                        $("#debtorTelephone").val(debtorTelephoneMobile);

                        document.getElementById("btnSave").disabled = false;
                    } else {
                        alert("Else");
                        $('#debID').val('');
                        $('#debtorNic').val('');
                        $("#debtorName").val('');
                        $("#debtorAddress").val('');
                        $("#debtorTelephone").val('');
                        document.getElementById("btnSave").disabled = true;
                    }
                }
            });
        }

    }
</script>
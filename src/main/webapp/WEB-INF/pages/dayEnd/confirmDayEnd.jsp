<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row" id="pending_div">
    <div class="col-md-12" style="padding: 1px;">
        <div class="row" style="padding: 5px; margin: -10 5 30 5; background: #ABC147">
            <div class="col-md-8 " style="padding: 1; text-align: left;"><strong>Confirm Day End  -  ${branch}</strong></div>
            <div class="col-md-4 "></div>
        </div>
        <div class="row" style="margin-top: 15px; padding: 2px 8px">
            <div class="col-md-11 "><label>Do you want to proceed Day End to <u>${dayEndDate}</u></label></div>
            <div class="col-md-1 "></div>

        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-5"></div>
            <div class="col-md-3"><input type="button" onclick="unblockui()" class="btn btn-default col-md-12" value="No"/></div>
            <div class="col-md-3"><input type="button" onclick="processDayEnd()" class="btn btn-default col-md-12" value="Yes"/></div>
            <div class="col-md-1"></div>
        </div>
    </div>

</div>

<script>
    function processDayEnd() {
        $("#dayEndBut").prop("disabled", true);
        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function (data) {
                if (!data) {
                    ui("#proccessingPage");
                    $.ajax({
                        url: '/AxaBankFinance/DayEndController/dayEndProcess',
                        success: function (data) {
                            $("#message_dayEnd").html(data);
                            ui("#message_dayEnd");
//                            $("#dayEndBut").prop("disabled", false);
                           // updateDayEndStatus();
                        },
                        error: function () {
                        }
                    });
                } else {
                    warning("User Is Blocked", "message_dayEnd");
                    $("#dayEndBut").prop("disabled", false);
                }
            },
            error: function () {
                alert("Connection is refused by the server...");
            }
        });
    }


    function updateDayEndStatus() {
         $.ajax({
            url: '/AxaBankFinance/DayEndController/updateDayEndStatus',
            success: function(data) {
                $("#confirm_dayEnd").html(data);
                ui("#confirm_dayEnd");
            },
            error: function() {
                alert("Connection is refuced by the server...");
            }
        });
    }

</script>
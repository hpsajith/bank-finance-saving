<%-- 
    Document   : confirmCommonDayEnd
    Created on : Apr 16, 2016, 10:35:40 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row" id="">
    <form name="dayEndBranchListForm" id="dayEndBranchList">
        <div class="col-md-12" style="padding: 1px;">
            <div class="row" style="padding: 5px; margin: -10 5 30 5; background: #ABC147">
                <div class="col-md-8 " style="padding: 1; text-align: left;"><strong>Confirm Day End This Branches </strong></div>
                <div class="col-md-4 "></div>
                <div class="row" style="font-size: 14px;color: #843534"><marquee>${branchName}</marquee></div>
            </div>
            <div class="row" style="margin-top: 15px; padding: 2px 8px">
                <div class="col-md-11 "><label>Do you want to proceed Day End</label></div>
                <div class="col-md-1 "></div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-5"></div>
                <div class="col-md-3"><input type="button" onclick="unblockui()" class="btn btn-default col-md-12" value="No"/></div>
                <div class="col-md-3"><input type="button" onclick="processCOmmonDayEnd()" class="btn btn-default col-md-12" value="Yes"/></div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <c:forEach var="bb" items="${dayEndBranchId}">
            <c:set var="BB" value="${BB}${bb},"></c:set>
        </c:forEach>
    </form>
</div>

<script>
    function processCOmmonDayEnd() {
        var branches = '${BB}';
        if (branches === "") {
            alert("Can't Continue.!! Didn't Select the Branches");
        } else {
            $("#dayEndBut").prop("disabled", true);
            $.ajax({
                url: '/AxaBankFinance/checkIsBlock',
                success: function (data) {
                    if (!data) {
                        ui("#proccessingPage");
                        $.ajax({
                            url: '/AxaBankFinance/DayEndController/commonDayEndProcess',
                            data: {"branches": branches},
                            success: function (data) {
                                $('#formContent').html("");
                                $('#formContent').html(data);
                                unblockui();
                                $("#dayEndBut").prop("disabled", false);
                            },
                            error: function () {
                            }
                        });
                    } else {
                        warning("User Is Blocked", "message_dayEnd");
                        $("#dayEndBut").prop("disabled", false);
                    }
                },
                error: function () {
                    alert("Connection is refuced by the server...");
                }
            });
        }
    }
</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row">
    <div class="col-md-4">
        <label style="font-size: 16px;">${lastDay}</label>
    </div>
    <div class="col-md-4">
        <input type="button" value="DAY END" onclick="confirmProcessDayEnd()" id="dayEndBut" class="btn btn-block btn-default"/>                       
    </div>
    <div class="col-md-4">

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <c:forEach var="job" items="${jobs}">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-3" style="font-size: 14px; font-weight: bold;">${job.processId}  ${job.processDescription}</div>
                <div class="col-md-6" style="border-bottom: 2px solid green"></div>
                <!--<div class="col-md-6" style="border-bottom: 2px solid green"><label style="font-size: 12px; color: #0088cc; font-weight: bold;">Success</label></div>-->
                <div class="col-md-2"></div>
            </div>
        </c:forEach>
    </div>
</div>

<div class="row" style="margin-top: 50px;">
    <c:choose>
        <c:when test="${userName=='ites'}">
            <div class="col-md-7" style="border:1px solid #737373; padding:20px;">
                <div class="row">
                    <div class="col-md-4"><label>Start date</label></div>
                    <div class="col-md-4"><input type="text" name="start"></div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><label>End Date</label></div>   
                    <div class="col-md-4"><input type="text" name="end"></div>   
                    <div class="col-md-4"></div>   
                </div>
                <div class="row">
                    <div class="col-md-4"><input type="button" value="Send" class="btn btn-block" onclick="correctPosting()"/></div>
                </div>
                <div class="row" style="margin-top: 20px; border-top: 1px solid #5e5e5e;">
                    <label>Code</label><input type="text" id="chqCode"/>
                    <label>Id</label><input type="text" id="chqId"/>
                    <input type="button" id="chqpostBut" onclick="postingCheq()" class="btn btn-edit" value="Post"/>
                </div>
            </div>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>    
</div>

<!--<div class="row" style="margin-top: 10px;">
    <div class="col-md-4">
        <input type="button" value="Balances" onclick="calculateBalances()" id="btnBalances" class="btn btn-block btn-default"/>                       
    </div>
</div>-->
<!--// do not delete-->
<!--<div class="row">
    <div class="col-md-4"><input type="button" value="Calculate Laps Date 1" class="btn btn-block" onclick="calLapsDate()"/></div>
    <div class="col-md-4"><input type="button" value="Calculate Installment 2" class="btn btn-block" onclick="calInstallment()"/></div>
</div>-->

<div id="message_dayEnd" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="confirm_dayEnd" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script>
    function confirmProcessDayEnd() {
        $.ajax({
            url: '/AxaBankFinance/DayEndController/confirmDayEnd',
            success: function(data) {
                $("#confirm_dayEnd").html(data);
                ui("#confirm_dayEnd");
            },
            error: function() {
                alert("Connection is refused by the server...");
            }
        });
    }
 

    function correctPosting() {
        var sdate = $("input[name=start]").val();
        var edate = $("input[name=end]").val();
        $.ajax({
            url: '/AxaBankFinance/DayEndController/dayEndProcessCorrection',
            data: {start: sdate, end: edate},
            success: function(data) {
                $("#message_dayEnd").html(data);
                ui("#message_dayEnd");
            },
            error: function() {
            }
        });

    }
    
    function postingCheq() {
        $("#chqpostBut").prop("disabled", true);
        var code = $("#chqCode").val();
        var id = $("#chqId").val();
        $.ajax({
            url: '/AxaBankFinance/DayEndController/postingCheq',
            data: {code: code, id: id},
            success: function(data) {
                $("#chqpostBut").prop("disabled", false);
                $("#message_dayEnd").html(data);
                ui("#message_dayEnd");
            },
            error: function() {
            }
        });

    }
    function calLapsDate() {
        $.ajax({
            url: '/AxaBankFinance/DayEndController/calLapsDate',
            success: function(data) {
                $("#message_dayEnd").html(data);
                ui("#message_dayEnd");
            }
        });
    }


    function calInstallment() {
        $.ajax({
            url: '/AxaBankFinance/DayEndController/calNullInstallment',
            success: function(data) {
                $("#message_dayEnd").html(data);
                ui("#message_dayEnd");
            }
        });
    }

//    function calculateBalances() {
//        ui("#proccessingPage");
//        $.ajax({
//            url: "/AxaBankFinance/DayEndController/calculateBalances",
//            success: function(data) {
//                console.log(data);
//                unblockui();
//            }
//        });
//    }

</script>

<%-- 
    Document   : commonDayEndPage
    Created on : Apr 16, 2016, 10:07:11 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        $("#tblDayEndBranch").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "60%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        pageAuthentication();
    });
</script>
<div class="row" >
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <label style="font-size: 16px;color: #FF0000">${warningMessage}</label>
        <label style="font-size: 16px;color: #00bb8c">${successMessage}</label>
    </div>
    <div class="col-md-3"></div>

</div>
<div class="row">
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-5">
                <label style="font-size: 16px;">${lastDay}</label>
            </div>
            <div class="col-md-6">
                <input type="button" value="DAY END" onclick="confirmCommonDayEnd()" id="dayEndBut" class="btn btn-block btn-default"/>                       
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <c:forEach var="job" items="${jobs}">
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-5" style="font-size: 14px; font-weight: bold;">${job.processId}  ${job.processDescription}</div>
                        <div class="col-md-6" style="border-bottom: 2px solid green"></div>
                        <!--<div class="col-md-4" style="border-bottom: 2px solid green"><label style="font-size: 12px; color: #0088cc; font-weight: bold;">Success</label></div>-->
                        <div class="col-md-1"></div>
                    </div>
                </c:forEach>
            </div>
        </div>

    </div>
    <div class="col-md-5">
        <form name="branchForm" id="dayEndBranch">
            <table id="tblDayEndBranch" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>System Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="branchs" items="${branchList}">
                        <tr id="${branchs[0]}">
                            <c:forEach var="status" items="${dayEndStatus}">
                                <c:if test="${status.branchId == branchs[0]}">
                                    <c:choose>
                                        <c:when test="${status.status == 1}">
                                            <td><input type="checkbox" id="branch${branchs[0]}"</td>
                                            </c:when>
                                            <c:otherwise>
                                            <td><input type="checkbox" checked="" id="branch${branchs[0]}"</td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:forEach>

                            <c:forEach var="bra" items="${allBranchs}">
                                <c:if test="${bra.branchId == branchs[0]}">
                                    <td>${bra.branchName}</td>
                                </c:if>
                            </c:forEach>
                            <td>${branchs[1]}</td>
                            <c:forEach var="status" items="${dayEndStatus}">
                                <c:if test="${status.branchId == branchs[0]}">
                                    <c:choose>
                                        <c:when test="${status.status == 0}">
                                            <td style="color: #0075b0">stop</td>
                                        </c:when>
                                        <c:when test="${status.status == 1}">
                                            <td style="color: #00bb8c">Finish</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td style="color: #FF0000">Error</td>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <input type="hidden" id="hideBranches"/>
        </form>
    </div>
</div>

<div id="message_common_dayEnd" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="confirm_common_dayEnd" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>


<script>
    function confirmCommonDayEnd() {
        setBranches();
        $.ajax({
            url: '/AxaBankFinance/DayEndController/confirmCommonDayEnd',
            type: 'POST',
            data: $('#dayEndBranch').serialize(),
            success: function (data) {
                $("#confirm_common_dayEnd").html(data);
                ui("#confirm_common_dayEnd");
            },
            error: function () {
                alert("Connection is refuced by the server...");
            }
        });
    }

    function setBranches() {
        $('#tblDayEndBranch tbody tr').each(function () {
            var branchId = $(this).attr('id');
            if ($('#branch' + branchId).is(':checked')) {
                $('#hideBranches').append("<input type = 'hidden' name = 'branches' value = '" + branchId + "'/>");
            }
        });

    }
</script>

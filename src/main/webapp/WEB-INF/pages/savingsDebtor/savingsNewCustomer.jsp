<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/18/2018
  Time: 9:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="container-fluid">
    <div class="row divError"></div>
    <div class="col-md-12" style="border:1px solid #C8C8C8; border-radius: 2px;">
        <strong>Add Customer</strong>
        <input type="hidden" value="${branchCode}" id="branchCodeId"/>

        <button type="button" class="btn btn-info btn-lg" style="display:none;" id="modelBoxId" data-toggle="modal"
                data-target="#myModal"></button>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" style="color: #002eb4"><strong>Save New Customer</strong></h5>
                    </div>
                    <div class="modal-body">
                        <p>Customer Saved Successfully.!!</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <form id="save_form_temp" name="debtorForm" autocomplete="off">
            <input type="hidden" value="${debtor.debtorNic}" id="debtorNic"/>
            <div class="row" style="margin-top: 5px">
                <div class="col-md-12" style=" margin: 10px;">
                    <legend>Basic details</legend>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Full Name</div>
                        <div class="col-md-10"><input style="width: 100%" type="text" name="debtorName"
                                                      value="${debtor.debtorName}" id="debtorName_id"
                                                      maxlength="200"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Name with
                            Initials
                        </div>
                        <div class="col-md-10"><input type="text" name="debtorNameWithIni" style="width: 100%"
                                                      value="${debtor.debtorNameWithIni}" id="debtorNameWithIni_id"
                                                      maxlength="200"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>NIC</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="debtorNic"
                                                     value="${debtor.debtorNic}" id="debtorNic_id" maxlength="12"
                                                     onblur="checkDebtorByNic(this)"/>
                            <label style="text-align: left" id="msgNicExits" class="msgTextField"></label>
                        </div>
                        <div class="col-md-2" style="text-align: right"><span style="color: red">*</span>Date Of Birth
                        </div>
                        <div class="col-md-4"><input class="txtCalendar" type="text" style="width: 100%"
                                                     value="${debtor.debtorDob}" name="debtorDob" id="debtorDob_id"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Email</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="debtorEmail" id="debtorEmail_id"
                                                     value="${debtor.debtorEmail}" onblur="validateEmail(this)"
                                                     maxlength="40"/></div>
                        <div class="col-md-2" style="text-align: right">Fax</div>
                        <div class="col-md-4"><input type="text" name="debtorFax" id="debtorFax_id" style="width: 100%"
                                                     id="fax" value="${debtor.debtorFax}" maxlength="10"
                                                     onkeyup="checkNumbers(this)"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Personal Address
                        </div>
                        <div class="col-md-4"><textarea name="debtorPersonalAddress" style="width: 100%" id="debtorPersonalAddress_id"
                                                        class="address" maxlength="200">${debtor.debtorPersonalAddress}</textarea>
                        </div>
                        <div class="col-md-2" style="text-align:right"><span style="color: red">*</span>Formal Address
                        </div>
                        <div class="col-md-4"><textarea name="debtorFormalAddress" style="width: 100%"
                                                        id="debtorFormalAddress_id" class="address"
                                                        maxlength="200">${debtor.debtorFormalAddress}</textarea></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Nationality</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="debtorNationality"
                                                     id="debtorNationality_id"
                                                     value="${debtor.debtorNationality}"
                                                     maxlength="40"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Job Information</div>
                        <div class="col-md-10"><input type="text" name="jobInformation" style="width: 100%"
                                                      value="${debtor.jobInformation}" id="jobInformation_id"/></div>
                    </div>
                    <div class="msyl-gredient2" style="margin-top: 20px;margin-bottom: 10px">
                        <div class="row" style="padding-top: 10px;padding-left: 25px">
                            <div class="row">
                                <div class="col-md-12" style="text-align: left"><strong>Contact Numbers</strong></div>
                            </div>
                            <div class="row" style="margin-left: 20px">
                                <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Home</div>
                                <div class="col-md-3">
                                    <input type="text" style="width: 100%;" name="debtorTelephoneHome" id="debtorTelephoneHome_id"
                                           value="${debtor.debtorTelephoneHome}" class="number" maxlength="10"
                                           onblur="checkTelNumbers(this, '#msgHome')" onkeypress="return checkNumbers(this)"/>
                                    <label id="msgHome"></label>
                                </div>
                                <div class="col-md-2" style="text-align: right"><span style="color: red">*</span>Work</div>
                                <div class="col-md-3">
                                    <input type="text" style="width: 100%;" name="debtorTelephoneWork"
                                           id="debtorTelephoneWork_id" value="${debtor.debtorTelephoneWork}" class="number"
                                           maxlength="10" onblur="checkTelNumbers(this, '#msgWork')"
                                           onkeypress="return checkNumbers(this)"/>
                                    <label id="msgWork"></label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 20px">
                                <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Mobile</div>
                                <div class="col-md-3">
                                    <input type="text" style="width: 100%;" name="debtorTelephoneMobile"
                                           id="debtorTelephoneMobile_id" value="${debtor.debtorTelephoneMobile}" class="number"
                                           maxlength="10" onblur="checkTelNumbers(this, '#msgMobile')"
                                           onkeypress="return checkNumbers(this)"/>
                                    <label id="msgMobile"></label>
                                </div>
                                <div class="col-md-2" style="text-align: right">Police Station</div>
                                <div class="col-md-3"><input type="text" name="debtorPoliceStation"
                                                             value="${debtor.debtorPoliceStation}" style="width: 100%"/></div>
                            </div>
                            <div class="row" style="margin-left: 20px; margin-bottom: 15px">
                                <div class="col-md-2" style="text-align: left">Post Office</div>
                                <div class="col-md-3"><input type="text" name="debtorPostOffice" value="${debtor.debtorPostOffice}"
                                                             style="width: 100%"/></div>
                                <div class="col-md-2" style="text-align: right">G.M.Office</div>
                                <div class="col-md-3"><input type="text" name="debtorAgOffice"
                                                             value="${debtor.debtorAgOffice}" style="width: 100%"/></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4" style="text-align: left"><strong>Picture</strong></div>
                                <div class="col-md-6"><input style="width: 100%" type="file" value="browse"
                                                             id="myPicture"/></div>
                                <div class="col-md-2" id="myProfilePic"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4" style="text-align: right"><strong>Location</strong></div>
                                <div class="col-md-8"><input style="width: 100%" type="file" value="browse"/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <c:choose>
                <c:when test="${edit}">
                    <div class="" style="margin-top: 5px">
                        <div class="col-md-12">
                            <div class="row" style="margin-bottom: 15px">
                                <div class="col-md-2">
                                    <input type="button" class="btn btn-default col-md-12" style="" value="Cancel"
                                           onclick="unblockui()"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="button" id="btnSaveDebtor" class=" btn btn-default col-md-12" style=""
                                           value="Update" onclick="saveNewDebtor()"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="" style="margin-top: 5px">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="row" style="margin-bottom: 15px">
                                <div class="col-md-6">
                                    <input type="button" class="btn btn-default col-md-12" style="" value="Cancel"
                                           onclick="unblockui()"/>
                                </div>
                                <div class="col-md-6">
                                    <input id="btnSaveDebtor" type="button" class=" btn btn-default col-md-12" style=""
                                           value="Save" onclick="saveNewDebtor()"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <%--<input type="hidden" name="saveDebtorId" value="${id}" id="txtDebtorId"/>--%>
            <input type="hidden" name="debtorIsDebtor" value="${debtor.debtorIsDebtor}" id="savedebId"/>
            <%--<input type="hidden" name="saveAccountNo" value="${debtor.saveAccountNo}" id="txtSaveAccountNo"/>--%>
            <%--<input type="hidden" name="isGenerate" value="${debtor.isGenerate}" id="txtIsGenerate"/>--%>
        </form>
    </div>

    <div class="row">
        <div class="col-md-6">
            <c:if test="${employee.size()>0}">
                <label>Employee Details</label>
                <table id="tblEmplyee"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>ID No</th>
                        <th>Company Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="emp" items="${employee}">
                        <tr onclick="selectTableRow(2,${emp.companyId})">
                            <td>#</td>
                            <td>${emp.companyName}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

        </div>
        <div class="col-md-6">
            <c:if test="${business.size()>0}">
                <label>Business Details</label>
                <table id="tblBusiness"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Business Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="busi" items="${business}">
                        <tr onclick="selectTableRow(3,${busi.businessId})">
                            <td>#</td>
                            <td>${busi.businessName}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>
<!--form content-->
<div id="debtorForms" class="searchCustomer" style="width: 45%; ">
</div>
<!--//message-->
<div id="message_newCustomer" class="searchCustomer"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script>

    $('.txtCalendar').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        yearRange: "-100:+0",
    });

    $(function () {
//$('.txtCalendar').addClass('blockMsg');
        $("#myPicture").on('change', prepareLoad);
        $('#modelBoxId').hide();

    });

    var files = null;

    function prepareLoad(event) {
        console.log(' event fired' + event.target.files[0].name);
        files = event.target.files;
    }

    //save profile picture

    function uploadProfilePicture(data) {
        var customerId = 0;
        var oMyForm = new FormData();
        if (files != null)
            oMyForm.append("file", files[0]);
        else
            oMyForm.append("file", null);

        oMyForm.append("customerId", customerId);
        var guarantor_debtor_type = $("#gurant_debtr_type").val();
        $.ajax({
            url: '/AxaBankFinance/uploadProfilePicture?${_csrf.parameterName}=${_csrf.token}',
            data: oMyForm,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (result) {
                if (result) {
                    console.log("IF" + result);
                    $('#searchCustomerContent').html('');
                    $('#searchCustomerContent').html(data);
                    var url2 = "/AxaBankFinance/viewProfilePicture/" + 0;
                    $("#myProfilePic").html("<img src='" + url2 + "' ></img>");

                    var nic = $("input[name=debtorNic]").val();
                    var name = $("input[name=debtorNameWithIni]").val();
                    var address = $("#debtorPersonalAddress_id").val();
                    var telephone = $("input[name=debtorTelephoneHome]").val();
                    var debtorId = $("input[name=debtorId]").val();
                    var debtorAccNo = $("input[name=accountNo]").val();

                } else {
                    alert("save false");
                }
            },
            error: function () {
                var message = "File size exceeds the configured maximum file size";
                var view = "message_newCustomer";
                error(message, view);
            }
        });
    }

    function saveNewDebtor() {
        var ar = [document.getElementById('debtorName_id'), document.getElementById('debtorNic_id'), document.getElementById('debtorNameWithIni_id')];
        var ar1 = [document.getElementById('debtorPersonalAddress_id'), document.getElementById('debtorFormalAddress_id')];
        var ar2 = [document.getElementById('debtorTelephoneHome_id'), document.getElementById('debtorTelephoneWork_id'), document.getElementById('debtorTelephoneMobile_id')];

        if (files != null) {
            if (files[0].size > 102400) {
                alert("File size exceeds the configured maximum file size");
                return;
            }
        }

        var jobInfo = $("#jobInformation_id").val();
//        var centerDay = document.getElementById("centerDay_id").value;

        if (jobInfo === '') {
            validate = false;
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html("Please Enter the Job Information");
            $("#jobInformation_id").addClass("txtError");
        } else {
            $("#jobInformation_id").removeClass("txtError");
            $(".divError").css({'display': 'none'});
        }


        if (validateFields(ar) === true && validateOneField(ar1, '.address') === true && validateOneField(ar2, '.number') === true) {
            if (checkNIC("debtorNic_id") === true) {

                document.getElementById("btnSaveDebtor").disabled = true;
                $.ajax({
                    url: '/AxaBankFinance/SavingsDebtorController/savingsDebtorSave',
                    type: 'POST',
                    data: $('#save_form_temp').serialize(),
                    success: function (data) {
//                        uploadProfilePicture(data);
                        document.getElementById("btnSaveDebtor").disabled = false;
                        modelBoxTrigger();
                        $("#save_form_temp").trigger('reset');
                    },
                    error: function () {
                        alert("error");
                    }
                });
            }
        } else {

        }

    }

    function modelBoxTrigger() {
        $("#modelBoxId").trigger("click");
    }

    function selectAssetRow(type, id) {
        addAsset();
        $('#slctAsset').prop('disabled', true);
        if (type == 1) {
            asset = id;
            addAssetBank();
        } else if (type == 2) {
            asset = id;
            addAssetVehicle();
        } else if (type == 3) {
            asset = id;
            addAssetLand();
        }
    }

    function addDependent() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewDependentForm/' + debtorId + "/" + dependentId,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                dependentId = 0;
            }
        });
    }

    function addAssetBank() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetBankForm/' + debtorId + "/" + asset,
            success: function (data) {
                $("#assetContent").html(data);
                asset = 0;
            }
        });
    }


    function checkDebtorByNic(nicNo) {
        nictoLowerCase(nicNo);
        var detorId = document.getElementById('txtDebtorId').value;
        var nic = nicNo.value;
        if (detorId == null || detorId == 0) {
            $.ajax({
                url: '/AxaBankFinance/checkDebtorNic/' + nic,
                success: function (data) {
                    if (data === 1) {
                        $("#msgNicExits").html("Debtor is already exits..!");
                        $("#debtorNic_id").addClass("txtError");
                        $("#debtorNic_id").focus();
                    } else {
                        $("#debtorNic_id").removeClass("txtError");
                        $("#msgNicExits").html("");
                        $("#msgNicExits").removeClass("msgTextField");
                    }
                }
            });
        }
    }

</script>
<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/24/2018
  Time: 3:35 PM
  To change this template use File | Settings | File Templates.
--%>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>

<div class="container-fluid" id="searchCustomerContent" style="border:1px solid #BDBDBD;  border-radius: 2px;">
    <input type="hidden" id="gurant_debtr_type">
    <div class="row" style="background: #F2F7FC">
        <div class="col-md-12">
            <div class="row" style="padding: 10px;">
                <div class="col-md-3"><label>Search Customer</label></div>
                <div class="col-md-9"></div>
            </div>
            <div class="row" style="margin-top: 10px; padding: 10px">
                <div class="col-md-5">
                    <div class="col-md-4">NIC No</div>
                    <div class="col-md-8">
                        <input type="text" style="width:100%" name="" id="searchByNic"/>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="col-md-4">Customer Name</div>
                    <div class="col-md-8">
                        <input type="text" style="width:100%" name="" id="searchByName"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="searchedCustomer" style="height: 60%; overflow: auto;">
    </div>
    <div class="row">
        <div class="searchCustomer" id="editCust_div" style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto" >
        </div>
    </div>
</div>


<script>
    var name, nic, address, telephone, dId = "", accNo;
    $(function() {
        $('#searchByNic').keyup(function(e) {
            var nicNo = $(this).val();
            if (nicNo.length == 10) {
                $.ajax({
                    url: '/AxaBankFinance/SavingsDebtorController/searchByNic3/' + nicNo,
                    success: function(data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function() {
                        alert("Error Loading...");
                    }
                });
            }
        });

        $('#searchByName').keyup(function(e) {
            var name = $(this).val();
            if (name.length > 0) {
                $.ajax({
                    url: '/AxaBankFinance/SavingsDebtorController/searchByName3/' + name,
                    success: function(data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function() {
                        alert("Error Loading...");
                    }
                });
            }
        });
    });

    function editCustomer(dId) {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewSavingsCustomers/'+debtorId,
            success: function(data) {
                $('#editCust_div').html('');
                $('#editCust_div').append(data);
                ui("#editCust_div");
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }


</script>

<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/25/2018
  Time: 8:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $('.dataTable').dataTable({
            "scrollY": "150px",
            "scrollCollapse": true,
            "paging": false
        });
    });

</script>
<div class="container-fluid" style="height: 100%;overflow-y: auto;width: 100%">

    <div class="col-md-12" style="border:1px solid #C8C8C8; border-radius: 2px; margin: 4px; ">
        <legend>Basic Details</legend>
        <form id="savings_update_form_temp" name="debtorSavingsHeaderDetail" autocomplete="off">
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-3">Full Name</div>
                        <div class="col-md-9"><input style="width: 100%" value="${debtor.debtorName}" type="text"
                                                     name="debtorName" id="debtorName_id" maxlength="200"/></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">NIC</div>
                                <div class="col-md-8"><input type="text" value="${debtor.debtorNic}" style="width: 100%"
                                                             name="debtorNic" id="debtorNic_id" maxlength="10"
                                                             onblur="nictoLowerCase(this)"/></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">DOB</div>
                                <div class="col-md-8"><input type="text" class="txtCalendar" value="${debtor.debtorDob}"
                                                             style="width: 100%" name="debtorDob" id="debtorDob_id"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">Name with Initials</div>
                        <div class="col-md-9"><input style="width: 100%" value="${debtor.debtorNameWithIni}" type="text"
                                                     name="debtorNameWithIni" id="debtorName_id" maxlength="200"/></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">Email</div>
                                <div class="col-md-10"><input type="text" value="${debtor.debtorEmail}"
                                                              style="width: 100%" name="debtorEmail" id="debtorEmail_id"
                                                              onblur="validateEmail(this)" maxlength="40"/></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">Fax</div>
                                <div class="col-md-8"><input type="text" value="${debtor.debtorFax}" name="debtorFax"
                                                             style="width: 100%" id="debtorFax_id" maxlength="10"
                                                             onkeyup="checkNumbers(this)"/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-3">Personal Address</div>
                        <div class="col-md-9"><textarea name="debtorPersonalAddress" id="debtorPersonalAddress_id"
                                                        style="width: 100%"
                                                        class="address">${debtor.debtorPersonalAddress}</textarea></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-4">Formal Address</div>
                        <div class="col-md-8"><textarea name="debtorFormalAddress" id="debtorFormalAddress_id"
                                                        style="width: 100%"
                                                        class="address">${debtor.debtorFormalAddress}</textarea></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">

                    <div class="row">
                        <div class="col-md-4">Job Information</div>
                        <div class="col-md-8"><input type="text" value="${debtor.jobInformation}" style="width: 100%;"
                                                     name="jobInformation" id="jobInformationId"/></div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">Contact Home</div>
                        <div class="col-md-8"><input type="text" value="${debtor.debtorTelephoneHome}"
                                                     style="width: 100%;" name="debtorTelephoneHome" class="number"
                                                     id="debtorTelephoneHome_id" maxlength="10"
                                                     onkeyup="checkNumbers(this)" onblur="checkTelNumbers(this)"/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Contact Work</div>
                        <div class="col-md-8"><input type="text" value="${debtor.debtorTelephoneWork}"
                                                     style="width: 100%;" name="debtorTelephoneWork" class="number"
                                                     id="debtorTelephoneWork_id" maxlength="10"
                                                     onkeyup="checkNumbers(this)"/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Contact Mobile</div>
                        <div class="col-md-8"><input type="text" value="${debtor.debtorTelephoneMobile}"
                                                     style="width: 100%;" name="debtorTelephoneMobile" class="number"
                                                     id="debtorTelephoneMobile_id" maxlength="10"
                                                     onkeyup="checkNumbers(this)"/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Nationality</div>
                        <div class="col-md-8"><input type="text" style="width: 100%" name="debtorNationality"
                                                     id="debtorNationality_id"
                                                     value="${debtor.debtorNationality}"
                                                     maxlength="40"/></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-4">Police Station</div>
                        <div class="col-md-8"><input type="text" value="${debtor.debtorPoliceStation}"
                                                     name="debtorPoliceStation" id="debtorPoliceStation_id"
                                                     style="width: 100%"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Post Office</div>
                        <div class="col-md-8"><input type="text" value="${debtor.debtorPostOffice}"
                                                     name="debtorPostOffice" id="debtorPostOffice_id"
                                                     style="width: 100%"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">G.M.Office</div>
                        <div class="col-md-8"><input type="text" value="${debtor.debtorAgOffice}"
                                                     name="debtorAgOffice" id="debtorAgOffice_id" style="width: 100%"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">Profile Picture</div>
                        <div class="col-md-4"><input style="width: 100%" value="${debtor.debtorImage}" type="text"
                                                     name="debtorImage" id="debtorMap_id"/></div>
                        <div class="col-md-4"><input style="width: 100%" type="button" value="browse"/></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">Location</div>
                        <div class="col-md-4"><input style="width: 100%" value="${debtor.debtorMap}" type="text"
                                                     name="debtorMap" id="debtorImage_id"/></div>
                        <div class="col-md-4"><input style="width: 100%" type="button" value="browse"/></div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="debtorId" value="${id}" id="txtDebtorId_edit"/>
            <%--<input type="hidden" name="debtorAccountNo" value="${debtor.loanAccountNo}" id="txtDebtorAccountNo"/>--%>
            <input type="hidden" name="branchId" value="${debtor.branchId}" id="txtBranchId"/>
            <div class="row" style="margin-top: 10px;"></div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

    </div>
    <div class="row">
        <div class="col-md-10">

        </div>
        <div class="col-md-2">
            <input type="button" id="updateBtnId" class="btn btn-default col-md-12" value="Update"
                   onclick="updateDebtor()"/>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;margin-bottom: 20px">
        <div class="col-md-2">
            <input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"/>
        </div>
    </div>
</div>
<!--Temp data-->
<input type="hidden" value="${debtor.debtorName}" id="tempName">
<input type="hidden" value="${debtor.debtorNic}" id="tempNic">
<input type="hidden" value="${debtor.debtorPersonalAddress}" id="tempAddress">
<input type="hidden" value="${debtor.debtorTelephoneHome}" id="tempTelephone">
<input type="hidden" value="${debtor.debtorId}" id="tempid">


<!--form content-->
<div id="debtorForms" class="searchCustomer" style="width: 45%;  margin-top:10%; margin-left: 5%">
</div>

<script>

    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });

    var dependentId = 0, empId = 0, busi = 0, liability = 0, asset = 0;
    function updateDebtor() {
        var ar = [document.getElementById('debtorName_id'), document.getElementById('debtorNic_id'), document.getElementById('debtorDob_id')];
        var ar1 = [document.getElementById('debtorPersonalAddress_id'), document.getElementById('debtorFormalAddress_id')];
        var ar2 = [document.getElementById('debtorTelephoneHome_id'), document.getElementById('debtorTelephoneWork_id'), document.getElementById('debtorTelephoneMobile_id')];
        if (validateFields(ar) === true && validateOneField(ar1, '.address') === true && validateOneField(ar2, '.number') === true) {
            if (checkNIC("debtorNic_id") === true) {
                $.ajax({
                    url: '/AxaBankFinance/SavingsDebtorController/updateSavingsDebtorForm',
                    type: 'POST',
                    data: $('#savings_update_form_temp').serialize(),
                    success: function (data) {
                        alert(data);
                    },
                    error: function () {
                        alert("errror");
                    }
                });
            }
        }
    }

    function unLoadForm(dId) {
        unblockui();
        editCustomer(dId);
    }

    function memberNumber() {
        $("#memberNumber_id").mask("a*/999/99/999");
    }


</script>
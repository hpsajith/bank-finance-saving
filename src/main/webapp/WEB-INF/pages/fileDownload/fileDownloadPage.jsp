<%-- 
    Document   : fileDownloadPage
    Created on : Jun 23, 2016, 11:29:38 AM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<form method="GET" action="/AxaBankFinance/FileDownloadController/downloadPostingReports" target="blank" id="formBatchPostingReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px; margin-top: 1.5%">
        <div class="row" style="margin: 5 8 2 0"><legend>Download Posting Reports</legend></div>
        <div class="row" style="">
            <div class="col-md-1" style="text-align: right">
                <label style="margin-top: 3%">Start Date</label>
            </div>
            <div class="col-md-2">
                <input type="text" id="txtSysDate" name="sDate" class="txtCalendar" style="width: 100%" required/>
<!--                <label id="msgTxtSysDate" class="msgTextField"></label>-->
            </div>
            <div class="col-md-1" style="text-align: right">
                <label style="margin-top: 3%">End Date</label>
            </div>
            <div class="col-md-2">
                <input type="text" id="txtSysEndDate" name="eDate" class="txtCalendar" style="width: 100%" required/>
            </div>
            <div class="col-md-2">
                <input type="button" value="Download" onclick="downloadBatchPostingReport()" class="btn btn-edit col-md-12" style="margin-bottom: 10px; margin-top: -2%"/>
            </div>
            <div class="col-md-2">
                 <label id="msgTxtSysDate" class="msgTextField"></label>
            </div>
        </div>
    </div>
</form>

<form method="GET" action="/AxaBankFinance/FileDownloadController/downloadODIPostingReport" target="blank" id="formBatchPostingODIReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px; margin-top: 1.5%">
        <div class="row" style="margin: 5 8 2 0"><legend>Download Posting ODI Report</legend></div>
        <div class="row" style="">
            <div class="col-md-1" style="text-align: right">
                <label style="margin-top: 3%">Start Date</label>
            </div>
            <div class="col-md-2">
                <input type="text" id="txtStartDate" name="startDate" class="txtCalendar" style="width: 100%"/>
                <label id="msgTxtStartDate" class="msgTextField"></label>
            </div>
            <div class="col-md-1" style="text-align: right">
                <label style="margin-top: 3%">End Date</label>
            </div>
            <div class="col-md-2">
                <input type="text" id="txtEndDate" name="endDate" class="txtCalendar" style="width: 100%"/>
                <label id="msgTxtEndDate" class="msgTextField"></label>
            </div>
            <div class="col-md-2">
                <div style="width: 70px"><input type="radio" name="vType" value="1" onclick="unblockDwnButton()"> Motors</div>
                <div style="width: 80px"><input type="radio" name="vType" value="2" onclick="unblockDwnButton()"> 2 Wheelers </div>
                <div style="width: 70px"><input type="radio" name="vType" value="3" onclick="unblockDwnButton()"> Both </div>
            </div>
            <div class="col-md-2" id="divDwnBtnId">
                <input type="button" id="dwnBtnId" value="Download" onclick="downloadODIBatchPostingReport()" class="btn btn-edit col-md-12" style="margin-bottom: 10px; margin-top: -2%"/>
            </div>

        </div>
    </div>
</form>


<script type="text/javascript">

    var sDate = '${systemDate}';

    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            maxDate: new Date(sDate)
        });
    });

    $(document).ready(function () {
        document.getElementById("dwnBtnId").disabled = true;
    });

    function downloadBatchPostingReport() {
        var sysDate = $("#txtSysDate").val();
        var sysEndDate = $("#txtSysEndDate").val();
        if (sysDate !== "" && sysEndDate !== "") {
            $("#formBatchPostingReport").submit();
        } else {
            $("#msgTxtSysDate").html("Incorrect Date Range");
//            $("#txtSysDate").addClass("txtError");
//            $("#txtSysDate").focus();
        }
    }

    function downloadODIBatchPostingReport() {
        var startDate = $("txtStartDate").val();
        var endDate = $("txtEndDate").val();

        if (startDate !== "" && endDate !== "") {
            $("#formBatchPostingODIReport").submit();
        } else if (startDate === "") {
            $("#msgTxtStartDate").html("Incorrect Date Range");
            $("#txtStartDate").addClass("txtError");
            $("#txtStartDate").focus();
        }
    }

    function unblockDwnButton() {
        document.getElementById("dwnBtnId").disabled = false;
    }

</script>

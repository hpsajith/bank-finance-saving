<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<div style="border: 1px solid #BDBDBD; background: #f9f9f9; padding: 5px;">
<div class="row"><label class="h3" id="formHeading" style="margin-left: 20px;"></label></div>
<form id="loan_form_temp" name="tempLoanForm">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="row">
                <fieldset>
                    <div class="col-md-2">Amount(Rs)</div>
                    <div class="col-md-3"><input type="text" name="loanAmount" style="width:110px;" /></div>
                    <div class="col-md-1">Period</div>
                    <div class="col-md-2"><input type="text" name="loanPeriod" style="width:70px;" /></div>
                    <div class="col-md-3">
                        <select name="loanPeriodType">
                            <option value="1">Month</option>
                            <option value="2">Year</option>
                            <option value="3">Days</option>
                        </select>
                    </div>
                </fieldset>
            </div>
        </div>                    
    </div>
    <div class="row">
        <div class="col-md-5" style="border:1px solid #C8C8C8; padding: 5px; border-radius: 2px; margin: 4px;">
            <fieldset>
                <legend>Basic details</legend>
                <div class="row">
                    <div class="col-md-2" style="text-align: left;">Name</div>
                    <div class="col-md-10"><input type="text" name="loanFullName" style="width:100%; margin-left: -20px;"/></div>
                </div>
                <div class="row">
                    <div class="col-md-6" >                            
                        <div class="row">
                            <div class="col-md-3">NIC No</div>
                            <div class="col-md-9"><input type="text" name="loanNic" /></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">DOB</div>
                            <div class="col-md-9"><input type="text" name="loanDob"/></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Personal Address</div>
                            <div class="col-md-9"><textarea row="2" name="loanAddress"></textarea></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Formal Address</div>
                            <div class="col-md-9"><textarea row="2" name="loanPostAddress"></textarea></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Email</div>
                            <div class="col-md-9"><input type="text" name="loanEmail"/></div>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        Contact Numbers
                        <div class="row">
                            <div class="col-md-4" >Home<input type="text" name="loanTelephoneHome" style="width: 78px;"/></div>
                            <div class="col-md-4" >Work<input type="text" name="loanTelephoneOffice" style="width: 78px;"/></div>
                            <div class="col-md-4" >Mobile<input type="text" name="loanTelephoneMobile" style="width: 78px;"/></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Fax</div>
                            <div class="col-md-8"><input type="text" name="loanFax"/></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Post Office</div>
                            <div class="col-md-8"><input type="text" name="loanPostOffice"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">G.M.Office</div>
                            <div class="col-md-8"><input type="text" name="loanSecretaryOffice"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Police Station</div>
                            <div class="col-md-8"><input type="text" name="loanPolisStation"></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3" style="border:1px solid #C8C8C8; padding: 5px; border-radius: 2px; margin: 4px;">
            <fieldset>
                <legend>Employment details</legend>
                <div class="row">
                    <div class="col-md-3">Company Name </div>                            
                    <div class="col-md-8"><input type="text" name="companyName"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Address </div>                            
                    <div class="col-md-8"><textarea name="companyAddress"></textarea> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Phone No </div>                            
                    <div class="col-md-8"><input type="text" name="companyTelephone"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Designation </div>                            
                    <div class="col-md-8"><input type="text" name="designation"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Time Period </div>                            
                    <div class="col-md-8"><input type="text" name="employementPeriod"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Net Salary </div>                            
                    <div class="col-md-8"><input type="text" name="netIncome"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Gross Salary </div>                            
                    <div class="col-md-8"><input type="text" name="grossIncome"/> </div>                            
                </div>
            </fieldset>
        </div>                
        <div class="col-md-3" style="border:1px solid #C8C8C8; padding: 5px; border-radius: 2px; margin: 4px;">
            <fieldset>
                <legend>Business Details</legend>
                <div class="row">
                    <div class="col-md-3">Business Name </div>                            
                    <div class="col-md-8"><input type="text" name="businessName"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Address </div>                            
                    <div class="col-md-8"><textarea name="businessAddress"></textarea> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Phone No </div>                            
                    <div class="col-md-8"><input type="text" name="businessTelephone"/></div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Nature </div>                            
                    <div class="col-md-8"><input type="text" name="businessNature"/>  </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Income </div>                            
                    <div class="col-md-8"><input type="text" name="businessIncome"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Other Income </div>                            
                    <div class="col-md-8"><input type="text" name="otherIncome"/> </div>                            
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row">                
        <div class="col-md-3" style="border:1px solid #C8C8C8; padding: 5px; border-radius: 2px; margin: 4px;">
            <fieldset>
                <legend>Dependent Details</legend>
                <div class="row">
                    <div class="col-md-3">Name </div>                            
                    <div class="col-md-8"><input type="text" name="dependentName"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">NIC No </div>                            
                    <div class="col-md-8"><input type="text" name="dependentNic"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Job </div>                            
                    <div class="col-md-8"><input type="text" name="dependentJob"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Occupation </div>                            
                    <div class="col-md-8"><input type="text" name="dependentOccupation"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Phone No </div>                            
                    <div class="col-md-8"><input type="text" name="dependentTelephone"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Salary </div>                            
                    <div class="col-md-8"><input type="text" name="dependentNetSalary"/>  </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Office Name </div>                            
                    <div class="col-md-8"><input type="text" name="dependentOfficeName"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Office Address </div>                            
                    <div class="col-md-8"><textarea name="dependentOfficeAddress"></textarea> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-3">Office Telephone No </div>                            
                    <div class="col-md-8"><textarea name="dependentOfficeTelephone"></textarea> </div>                            
                </div>
            </fieldset>
        </div>
        <div class="col-md-5" style="border:1px solid #C8C8C8; padding: 5px; border-radius: 2px; margin: 4px;">
            <fieldset>
                <legend>Asset details</legend>
                <div class="row">     
                    <div class="col-md-6">
                        <fieldset>
                            <label>Bank Details</label>
                            <div class="row">
                                <div class="col-md-4">Bank Name</div>
                                <div class="col-md-8"><input type="text" name="bankName"/> </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-4">Branch</div>
                                <div class="col-md-8"><input type="text" name="bankBranchName"/> </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-4">Account Type</div>
                                <div class="col-md-8"><input type="text" name="bankAccountType"/> </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-4">Account No</div>
                                <div class="col-md-8"><input type="text" name="bankAccountNo"/> </div> 
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6" >
                        <fieldset>
                            <label>Real Estate Details</label>
                            <div class="row">
                                <div class="col-md-4">Deed No</div>
                                <div class="col-md-8"><input type="text" name="deedNo"/> </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-4">Location</div>
                                <div class="col-md-8"><input type="text" name="landPlace"/> </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-4">Commissioner Name</div>
                                <div class="col-md-8"><input type="text" name="commisionerName"/> </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-4">Date</div>
                                <div class="col-md-8"><input type="text" name="RDate"/> </div> 
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">     
                    <fieldset>
                        <label style="margin-top: 5px;">Vehicle Details</label>
                        <div class="row">
                            <div class="col-md-2">Reg No</div>
                            <div class="col-md-4"><input type="text" name="vehicleRegNo"/> </div> 
                            <div class="col-md-2">Type</div>
                            <div class="col-md-4"><input type="text" name="vehicleType"/> </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-2">Model</div>
                            <div class="col-md-4"><input type="text" name="vehicleModel"/> </div> 
                            <div class="col-md-2">Price</div>
                            <div class="col-md-4"><input type="text" name="vehicleValue"/> </div> 
                        </div>
                    </fieldset>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3" style="border:1px solid #C8C8C8; padding: 5px; border-radius: 2px; margin: 4px;">
            <fieldset>
                <legend>Liability Details</legend>
                <div class="row">
                    <div class="col-md-4">Description </div>                            
                    <div class="col-md-8"><input type="text" name="liabilityDescription"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-4">Organization </div>                            
                    <div class="col-md-8"><input type="text" name="organization"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-4">type </div>                            
                    <div class="col-md-8"><input type="text" name="liabilityType"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-4">Amount </div>                            
                    <div class="col-md-8"><input type="text" name="liabilityAmount"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-4">Date </div>                            
                    <div class="col-md-8"><input type="text" name="liabilityDate"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-4">Installment </div>                            
                    <div class="col-md-8"><input type="text" name="liabilityInstallment"/> </div>                            
                </div>
                <div class="row">
                    <div class="col-md-4">No of Installment </div>                            
                    <div class="col-md-8"><input type="text" name="liabilityInstallmentCount"/> </div>                            
                </div>
            </fieldset>                            
        </div>
    </div>
    <!--                <div class="row">
                        <div class="col-lg-12">
                        <fieldset>
                            <legend>Guarantee Details</legend>
                            <div class="row" id="guaranteeData"></div>
                        </fieldset>
                        </div>
                    </div>-->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <input type="hidden" name="loantype" value="1" id="loanTypeId"/>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-2">
            <input type="button" class="btn-edit btn-group-vertical" style="width: 90%;" value="Save" onclick="submitTempLoanDetails()"/>
        </div>
        <div class="col-md-2">
            <input type="button" class="btn-edit btn-group-vertical" style="width: 90%;" value="Cancel" onclick="cancelLoanForm()"/>
        </div>
    </div>    

</form>
    <div style="clear: both"></div>
</div>
<script>
    function submitTempLoanDetails(){ 
        $.ajax({
            url: '/AxaBankFinance/saveLoanFrom',
            type: 'POST',
            data: $('#loan_form_temp').serialize(),
            success: function(data) {
                $('#formContent').html("");
                $('#formContent').html(data);
            },
            error: function() {
                alert("errror");
            }
        });
    }
    
    function cancelLoanForm(){
        $('#formContent').html("");
    }
</script>
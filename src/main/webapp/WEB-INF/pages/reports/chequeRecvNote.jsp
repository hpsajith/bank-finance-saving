<%-- 
    Document   : chequeRecvNote
    Created on : Jan 13, 2016, 5:19:40 AM
    Author     : IT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>



<div class="row" style="margin-bottom: 10px;padding: 5px">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>View PD Cheque List</legend></div>
        <form method="GET" action="/AxaBankFinance/ReportController/PrintPdChequeList" target="blank" id="formPDChequeList">
            <div class="col-md-2">Date</div>
            <div class="col-md-3"><input type="text" name="date1" id="date1id" value="" class="txtCalendar" style="width: 80%"/></div>
           <div class="col-md-1">To</div>
            <div class="col-md-3"><input type="text" name="date2" id="date2id" value="" class="txtCalendar" style="width: 80%"/></div>
            <div class="col-md-3" style="margin-bottom: 5px"><input type="button" id="btnPringPD"value="Print" onclick="printPDChequeList()" class="btn btn-edit col-md-12"/></div>
        </form>  
    </div>
</div>

<div class="row" style="">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend> PD Cheque Received Letter</legend></div>
        <form method="GET" action="/AxaBankFinance/ReportController/PrintChequeRecvNote" target="blank" id="formChequeRecvNoteReport">
            <div class="col-md-2">Agreement No</div>
            <div class="col-md-3"><input type="text" name="aggNo" class=""/></div>
            <div class="col-md-3" style="margin-bottom: 5px"><input type="button" value="Print" onclick="viewChequeRecvNote()" class="btn btn-edit col-md-12"/></div>
            <div class="col-md-4"></div>
        </form>  
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });
    function viewChequeRecvNote() {
        $("#formChequeRecvNoteReport").submit();
    }

    function printPDChequeList() {
        $("#formPDChequeList").submit();
    }
</script>
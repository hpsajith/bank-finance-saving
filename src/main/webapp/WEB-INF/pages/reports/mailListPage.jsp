<%-- 
    Document   : mailListPage
    Created on : Sep 12, 2016, 11:47:28 AM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<form method="GET" action="/AxaBankFinance/ReportController/printMailList" target="blank" id="formMailList">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px; margin-top: 1.5%">
        <div class="row" style="margin: 5 8 2 0"><legend>Print Mail List</legend></div>
        <div class="row" style="">
            <div class="col-md-2" style="text-align: right">
                <label style="margin-top: 3%">Letter Type</label>
            </div>
            <div class="col-md-3">
                <select id="inpLetterType" name="letterId" style="width: 100%">
                    <option value="0">-- SELECT --</option>
                    <c:forEach var="letter" items="${letters}">
                        <option value="${letter.letterId}">${letter.description}</option>
                    </c:forEach>
                </select>
                <label id="msgInpLetterType" class="msgTextField"></label>
            </div>
            <div class="col-md-3">
                <input type="button" value="Print" onclick="printMailList()" class="btn btn-edit col-md-12" style="margin-bottom: 10px; margin-top: -2%"/>
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">

    function printMailList() {
        var letterId = $("#inpLetterType").find("option:selected").val();
        if (letterId !== "0") {
            $("#formMailList").submit();
        } else {
            $("#msgInpLetterType").html("Select Letter Type");
            $("#inpLetterType").addClass("txtError");
            $("#inpLetterType").focus();
        }
    }
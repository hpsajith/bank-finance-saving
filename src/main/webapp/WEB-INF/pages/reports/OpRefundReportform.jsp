<%-- 
    Document   : OpRefundReportform
    Created on : Sep 28, 2016, 11:27:36 AM
    Author     : Chanaka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div>
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row"  style="margin: 5 8 2 0">
            <legend>
                Over Payment Refund Report
            </legend>
        </div>
        <form method="GET" action="/AxaBankFinance/ReportController/generateOPRefundReport" target="blank">

            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2">
                    <label>Start Date</label>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <input type="text" class="txtCalendar" required="true"  name="sdate" id="sDate" >
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">
                    <label>End Date</label>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <input type="text" class="txtCalendar" required="true"  name="edate" id="eDate" required="true">
                </div>
            </div><br/>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2">

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <input type="submit" class="btn btn-default col-md-6" value="Generate Report">
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">

                </div>
            </div>
        </form>
    </div>

</div>
<script>
    $(".txtCalendar").datepicker({
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd'
    });
   
   
</script>
</script>

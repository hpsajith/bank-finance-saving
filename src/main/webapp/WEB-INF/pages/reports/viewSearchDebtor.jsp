<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-md-12">
    <div class="row fixed-table">
        <div class="table-content">
            <c:choose>
                <c:when test="${message}">        
                    <!--<table class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header" id="tblCustomer" style="margin-top: 10px;">-->
                    <table id="tblCustomer_report" class="dc_table dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">  
                    <thead>
                            <tr>
                                <th>Name</th>
                                <th>NIC No</th>
                                <th>Address</th>
                                <th>Mobile No</th>
                                <th>Account No</th>
                            </tr>
                        </thead>
                        <tbody>                    
                            <c:forEach var="debtor" items="${debtorList}">
                                <tr id="trr${debtor.debtorId}" onclick="clickDebAccount(${debtor.debtorId})">
                                    <c:choose>
                                        <c:when test="${debtor.debtorName!=null}">
                                            <td>${debtor.debtorName}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorNic!=null}">
                                            <td>${debtor.debtorNic}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorPersonalAddress!=null}">
                                            <td>${debtor.debtorPersonalAddress}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorTelephoneMobile!=null}">
                                            <td>${debtor.debtorTelephoneMobile}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorAccountNo!=null}">
                                            <td>${debtor.debtorAccountNo}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise><label class="delete">No Result Found!!!</label></c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<script>
    //click table row    
    function clickDebAccount(id){
        var selected = $("#trr"+id).hasClass("highlight");
        $("#tblCustomer_report tbody tr").removeClass("highlight");
        if (!selected)
            $("#trr"+id).addClass("highlight");
        
//        $("#trr"+id).css("background-color", "yellow");
        $("#btnReportLegerCustomer").prop("disabled", false);
        $("#cusId_").val(id);
    }
</script>

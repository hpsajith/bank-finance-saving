<%-- 
    Document   : cashierReport
    Created on : Aug 6, 2015, 12:40:06 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<form method="GET" action="/AxaBankFinance/ReportController/printCashierReport" target="blank" id="formcashierReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Cashier Report</legend></div>

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2">Date</div>
            <input type="hidden" id="reportid" name="report"/>
            <div class="col-md-3"><input type="text" name="date1" id="date1id" value="" class="txtCalendar" style="width: 80%"/></div>
            <div class="col-md-3" ><input type="text" name="date2" id="date2id" value="" class="txtCalendar" style="width: 80%"/></div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-2">Loan Type</div>
            <div class="col-md-4">
                <select id="loantypeid" name="loantype">
                    <option value="0">--Select Loan Type--</option>
                    <c:forEach var="loantype" items="${subLoanList}">
                        <option value="${loantype.subLoanId}"> ${loantype.subLoanName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-6"> </div>
        </div>
        <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="col-md-2"></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrint" value="Print" onclick="printCashierReport()" class="btn btn-default col-md-10"/></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrintAll" value="Print All" onclick="printCashierReportAll()" class="btn btn-default col-md-10"/></div>
            <div class="col-md-4"><input type="button" name="" id="btnPrintAll" value="Print Other Payments" onclick="printCashierOtherPaymentsReports()" class="btn btn-default col-md-10"/></div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });
    function printCashierReport() {
        $("#reportid").val("N");
        if ($("#date1id").val().trim() == "" || $("#date2id").val().trim() == "") {
            alert("Please Select Date Range!");
            return;
        }
        if ($("#loantypeid").val() == 0) {
            alert("Please Select a Loan Type!");
            return;
        }
        generatePDf();
    }
    function printCashierReportAll() {
        $("#reportid").val("A");
        if ($("#date1id").val().trim() == "" || $("#date2id").val().trim() == "") {
            alert("Please Select Date Range!");
            return;
        }
        generatePDf();
    }
    function printCashierOtherPaymentsReports() {
        $("#reportid").val("P");
        if ($("#date1id").val().trim() == "" || $("#date2id").val().trim() == "") {
            alert("Please Select Date Range!");
            return;
        }
//        if ($("#paymentsId").val() == 0) {
//            alert("Please Select a Other Payments!");
//            return;
//        }
        generatePDf();
    }

    function generatePDf() {
        $("#formcashierReport").submit();

    }


</script>




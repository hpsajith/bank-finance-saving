<%-- 
    Document   : loanInstallments
    Created on : Aug 6, 2015, 12:40:06 PM
    Author     : Harshana
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>

<!--<form method="GET" action="/AxaBankFinance/ReportController/PrintLoanIntallmentReport" target="blank" id="formBasicLoanDetails">-->
<form method="GET" >
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend><label>Loan Installments </label></legend></div>

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2"><label>Member No</label></div>
            <div class="col-md-3"><input type="text" id="txtMemberNo" value=""  style="width: 80%"/></div>
            <!--<div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnSearch" value="View" onclick="printReport()" class="btn btn-default col-md-12"/></div>-->
            <div class="col-md-4" ></div>
        </div>
    </div>
</form>

<!--<div id="message_Report" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>-->
<div id="viewSearchedLoan" class="searchCustomer" style="width: 70%;height: 75%"></div>


<script type="text/javascript">
    $(function () {
        $("#btnReportLegerCustomer").prop("disabled", true);

        $(".txtCalendar").datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $('#cusNic_id').keyup(function (e) {
            var nicNo = $(this).val();
            if (nicNo.length === 10) {
                $.ajax({
                    url: '/AxaBankFinance/ReportController/searchByNicReport/' + nicNo,
                    success: function (data) {
                        $('#searchedCustomerR').html(data);
                    },
                    error: function () {
                        alert("Network Error");
                    }
                });
            }
        });

        $('#cusName_id').keyup(function (e) {
            var name = $(this).val();
            if (name.length > 4) {
                $.ajax({
                    url: '/AxaBankFinance/ReportController/searchByNameReport/' + name,
                    success: function (data) {
                        $('#searchedCustomerR').html(data);
                    },
                    error: function () {
                        alert("Network Error");
                    }
                });
            }
        });
    });


//    function printReport() {
//
//        var agreementNo = $("#txtMemberNo").val();
//        if (agreementNo === "") {
//            alert("Please Enter the Member Number");
//        } else {
//            generatePDf();
//        }
//
//    }

    function printRescheduleLoanList() {
        generateRescheduleLoansPDf()();
    }

    function printCustomerLedgerReport() {
        var agNo = $("input[name=agreementNo_]").val();
        if (agNo === "") {
            warning("Enter Agreement No", "message_Report");
            return;
        }
        var newLoanNo = agNo.replace(/\//g, "-");
        $("#newAgNo").val(newLoanNo);
        var toDate = $("#sDate_").val();
        var fromDate = $("#eDate_").val();
        if (toDate === "" || fromDate === "") {
            warning("Enter Time Period", "message_Report");
            return;
        }
        $("#formCustomerLeger").submit();
    }

    function generatePDf() {
        $("#formBasicLoanDetails").submit();
    }

    function generateRescheduleLoansPDf() {
        $("#rescheduleLoanListFormId").submit();
    }

    function printCustomerTotalReport() {
        var toDate = $("#fromDate_").val();
        var fromDate = $("#toDate_").val();
        if (toDate === "" || fromDate === "") {
            warning("Enter Time Period", "message_Report");
            return;
        }
        $("#formCustomerTotalLeger").submit();
    }

    $('#txtMemberNo').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            searchLoan();
        }
    });

    function searchLoan() {
        var memberNo = $("#txtMemberNo").val();
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loanSearchforReport?${_csrf.parameterName}=${_csrf.token}',
            data: {memNo: memberNo},
            type: 'POST',
            success: function (data) {
                $("#viewSearchedLoan").html(data);
                ui("#viewSearchedLoan");
            },
            error: function () {
                alert("Cannot Connect to the server");
            }
        });
    }

</script>




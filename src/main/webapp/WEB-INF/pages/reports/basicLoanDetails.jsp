<%-- 
    Document   : cashierReport
    Created on : Aug 6, 2015, 12:40:06 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>

<form method="GET" action="/AxaBankFinance/ReportController/PrintBasicLoanDetailsReport" target="blank" id="formBasicLoanDetails">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend><label>Basic Loan Details </label></legend></div>

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2"><label>Agreement No</label></div>
            <div class="col-md-3"><input type="text" name="agreementNo" value=""  style="width: 80%"/></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnSearch" value="View" onclick="printCashierReport()" class="btn btn-default col-md-12"/></div>
            <div class="col-md-4" ></div>
        </div>
    </div>
</form>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px; margin-top: 15px;">
    <div class="row" style="margin: 5 8 2 0"><legend><label>View Loan Account </label></legend></div>
    <form method="POST" action="/AxaBankFinance/ReportController/PrintCustomerLedgerReport" target="blank" id="formCustomerLeger">
        <div class="row">
            <div class="col-md-2"><label>Start Date</label></div>
            <div class="col-md-2"><input type="text" name="sDate" id="sDate_" class="txtCalendar"/></div>
            <div class="col-md-2"></div>
            <div class="col-md-2"><label>End Date</label></div>
            <div class="col-md-2"><input type="text" name="eDate" id="eDate_" class="txtCalendar"/></div>
            <div class="col-md-2"></div>
        </div>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2"><label>Agreement No</label></div>
            <div class="col-md-4"><input type="text" name="agreementNo_" value=""  style="width: 80%"/></div>
            <input type="hidden" id="newAgNo" name="agNoNew"/>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-2" ></div>
        <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnReportLeger" value="View" onclick="printCustomerLedgerReport()" class="btn btn-default col-md-12"/></div>
        <div class="col-md-4" ></div>
    </div>
</div>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px; margin-top: 15px;">
    <div class="row" style="margin: 5 8 2 0"><legend><label>View Customer Account </label></legend></div>
    <form method="POST" action="/AxaBankFinance/ReportController/PrintCustomerTotalLedgerReport" target="blank" id="formCustomerTotalLeger">
        <div class="row">
            <div class="col-md-2"><label>Start Date</label></div>
            <div class="col-md-2"><input type="text" name="cusFromDate" id="fromDate_" class="txtCalendar"/></div>
            <div class="col-md-2"></div>
            <div class="col-md-2"><label>End Date</label></div>
            <div class="col-md-2"><input type="text" name="cusToDate" id="toDate_" class="txtCalendar"/></div>
            <div class="col-md-2"></div>
            <input type="hidden" name="cusId" id="cusId_"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </div>
    </form>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-2"><label>Customer Name</label></div>
        <div class="col-md-3"><input type="text" name="cusName_" id="cusName_id" value=""  style="width: 80%"/></div>
        <div class="col-md-1"></div>
        <div class="col-md-2"><label>Customer NIC</label></div>
        <div class="col-md-3"><input type="text" name="cusNic_" id="cusNic_id" value=""  style="width: 80%"/></div>        
    </div>
    <div class="row" id="searchedCustomerR" style="height:10%; overflow: auto;">                
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-2" ></div>
        <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnReportLegerCustomer" value="View" onclick="printCustomerTotalReport()" class="btn btn-default col-md-12"/></div>
        <div class="col-md-4" ></div>
    </div>
</div>
<div id="message_Report" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>


<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px; margin-top: 15px;">
    <div class="row" style="margin: 5 8 2 0"><legend><label>View Reschedule Loans </label></legend></div>
    <form method="POST" action="/AxaBankFinance/ReportController/PrintRescheduleLoansReport" target="blank" id="rescheduleLoanListFormId">
        <div class="row">
            <div class="col-md-2"><label>Reschedule Loans</label></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnReportLeger" value="View" onclick="printRescheduleLoanList()" class="btn btn-default col-md-12"/></div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>


<script type="text/javascript">
    $(function () {
        $("#btnReportLegerCustomer").prop("disabled", true);

        $(".txtCalendar").datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $('#cusNic_id').keyup(function (e) {
            var nicNo = $(this).val();
            if (nicNo.length === 10) {
                $.ajax({
                    url: '/AxaBankFinance/ReportController/searchByNicReport/' + nicNo,
                    success: function (data) {
                        $('#searchedCustomerR').html(data);
                    },
                    error: function () {
                        alert("Network Error");
                    }
                });
            }
        });

        $('#cusName_id').keyup(function (e) {
            var name = $(this).val();
            if (name.length > 4) {
                $.ajax({
                    url: '/AxaBankFinance/ReportController/searchByNameReport/' + name,
                    success: function (data) {
                        $('#searchedCustomerR').html(data);
                    },
                    error: function () {
                        alert("Network Error");
                    }
                });
            }
        });
    });


    function printCashierReport() {
        generatePDf();
    }

    function printRescheduleLoanList() {
        generateRescheduleLoansPDf()();
    }

    function printCustomerLedgerReport() {
        var agNo = $("input[name=agreementNo_]").val();
        if (agNo === "") {
            warning("Enter Agreement No", "message_Report");
            return;
        }
        var newLoanNo = agNo.replace(/\//g, "-");
        $("#newAgNo").val(newLoanNo);
        var toDate = $("#sDate_").val();
        var fromDate = $("#eDate_").val();
        if (toDate === "" || fromDate === "") {
            warning("Enter Time Period", "message_Report");
            return;
        }
        $("#formCustomerLeger").submit();
    }

    function generatePDf() {
        $("#formBasicLoanDetails").submit();
    }

    function generateRescheduleLoansPDf() {
        $("#rescheduleLoanListFormId").submit();
    }

    function printCustomerTotalReport() {
        var toDate = $("#fromDate_").val();
        var fromDate = $("#toDate_").val();
        if (toDate === "" || fromDate === "") {
            warning("Enter Time Period", "message_Report");
            return;
        }
        $("#formCustomerTotalLeger").submit();
    }

</script>




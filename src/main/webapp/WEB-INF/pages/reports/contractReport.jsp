<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>

<%--Active Contract Report--%>
<form action="/AxaBankFinance/ReportController/pirntActiveContractReport" method="GET" target="blanck"
      id="activeContractForm">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0">
            <legend>Active Contract Report</legend>
        </div>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2">Start Date</div>
            <div class="col-md-3">
                <input type="text" name="date1" id="date1id" class="txtCalendar " style="width: 100%"/>
            </div>
            <div class="col-md-2">End Date</div>
            <div class="col-md-3">
                <input type="text" name="date2" id="date2id" class="txtCalendar" style="width: 100%"/>
            </div>
            <div class="col-md-2">
                <input type="button" value="Print Report" onclick="btnPrintActiveContract()" class="btn btn-default col-md-10" style="margin-top: -3%"/>
            </div>
        </div>
    </div>
</form>

<form action="/AxaBankFinance/ReportController/pirntNotActiveContractReport" method="GET" target="blanck"
      id="notActiveContractForm">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0">
            <legend>Initiated But Not Active Contract Report</legend>
        </div>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2">Start Date</div>
            <div class="col-md-3">
                <input type="text" name="date1" id="startDate" class="txtCalendar" style="width: 100%"/>
            </div>
            <div class="col-md-2">End Date</div>
            <div class="col-md-3">
                <input type="text" name="date2" id="endDate" value="" class="txtCalendar" style="width: 100%"/>
            </div>
            <div class="col-md-2">
                <input type="button" value="Print Report" onclick="btnPrintNotActiveContract()" class="btn btn-default col-md-10" style="margin-top: -3%"/>
            </div>
        </div>
    </div>
</form>

<script>
    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });


    function btnPrintActiveContract() {

        var date1 = $("#date1id").val();
        var date2 = $("#date2id").val();

        if (date1 == "") {
            alert("Select the start date");
            $("#date1id").focus();
        } else if (date2 == "") {
            alert("Select the end date");
            $("#date2id").focus();
        } else if (new Date(date1) > new Date(date2)) {
            alert("End date cannot be older than start date");
            $("#date2id").focus();
        } else {
            $("#activeContractForm").submit();
        }

    }

    function btnPrintNotActiveContract() {
        var date1 = $("#startDate").val();
        var date2 = $("#endDate").val();

        if (date1 == "") {
            alert("Select the start date");
            $("#date1id").focus();
        } else if (date2 == "") {
            alert("Select the end date");
            $("#date2id").focus();
        } else if (new Date(date1) > new Date(date2)) {
            alert("End date cannot be older than start date");
            $("#date2id").focus();
        } else {
            $("#notActiveContractForm").submit();
        }
    }

</script>
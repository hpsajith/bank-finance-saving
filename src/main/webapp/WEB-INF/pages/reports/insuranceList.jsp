<%-- 
    Document   : cashierReport
    Created on : Aug 6, 2015, 12:40:06 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row" style="margin-bottom: 10px;padding: 5px">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>View All Insurance Details </legend></div>
        <form method="GET" action="/AxaBankFinance/ReportController/PrintInsuranceListReport" target="blank" id="formInsuranceList">
            <div class="row">
                <div class="col-md-2">Branch</div>
                <div class="col-md-6">
                    <select name ="branchId" id="txtBranch" style="width: 80%">
                        <c:forEach var="branch" items="${branchList}">
                            <option value="${branch.branchId}">${branch.branchName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-2">Insurance Company</div>
                <div class="col-md-6">
                    <select name ="insuCompanyId" id="txtInsuCompany" style="width: 80%">
                        <c:forEach var="insCom" items="${companyList}">
                            <option value="${insCom.comId}">${insCom.comName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-2">Date</div>
                <div class="col-md-2"><input type="text" name="date1" value="" class="txtCalendar" style="width: 100%"/></div>
                <div class="col-md-2"><input type="text" name="date2" value="" class="txtCalendar" style="width: 100%"/></div>
                <div class="col-md-3"></div>
                <div class="col-md-3"><input type="button" name="" id="btnSearch" value="Print" onclick="printInsuranceList()" class="btn btn-default col-md-12"/></div>
            </div>
        </form>
    </div>
</div>
<div class="row" style="margin-bottom: 10px;padding: 5px">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>View Insurance Summary Details</legend></div>
        <form method="GET" action="/AxaBankFinance/ReportController/PrintInsuranceSummaryReport" target="blank" id="formInsuranceSummary">
            <div class="col-md-1">From</div>
            <div class="col-md-3"><input type="text" name="sDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-1"></div>
            <div class="col-md-1">To</div>
            <div class="col-md-3"><input type="text" name="eDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-3" style="margin-bottom: 5px"><input type="button" value="Print" onclick="printInsuranceSummaryReport()" class="btn btn-default col-md-12"/></div>
        </form>  
    </div>
</div>
<div class="row" style="margin-bottom: 10px;padding: 5px">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>View Insurance Commission Details</legend></div>
        <form method="GET" action="/AxaBankFinance/ReportController/PrintInsuranceCommissionReport" target="blank" id="formInsuranceComm">
            <div class="col-md-1">From</div>
            <div class="col-md-3"><input type="text" name="sDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-1"></div>
            <div class="col-md-1">To</div>
            <div class="col-md-3"><input type="text" name="eDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-3" style="margin-bottom: 5px"><input type="button" value="Print" onclick="printInsuranceCommReport()" class="btn btn-default col-md-12"/></div>
        </form>  
    </div>
</div>
<div class="row" style="margin-bottom: 10px;padding: 5px">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>View Insurance Renewal Details</legend></div>
        <form method="GET" action="/AxaBankFinance/ReportController/PrintInsuranceRenewalReport" target="blank" id="formInsuranceRenew">
            <div class="col-md-1">From</div>
            <div class="col-md-3"><input type="text" name="sDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-1"></div>
            <div class="col-md-1">To</div>
            <div class="col-md-3"><input type="text" name="eDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-3" style="margin-bottom: 5px"><input type="button" value="Print" onclick="printInsuranceRenewReport()" class="btn btn-default col-md-12"/></div>
        </form>  
    </div>
</div>
<div class="row" style="margin-bottom: 10px;padding: 5px">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>View Insurance Outstanding Details</legend></div>
        <form method="GET" action="/AxaBankFinance/ReportController/PrintInsuranceOutstandingReport" target="blank" id="formInsuranceOutstanding">
            <div class="col-md-1">From</div>
            <div class="col-md-3"><input type="text" name="sDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-1"></div>
            <div class="col-md-1">To</div>
            <div class="col-md-3"><input type="text" name="eDate" class="txtCalendar" style="width: 100%"/></div>
            <div class="col-md-3" style="margin-bottom: 5px"><input type="button" value="Print" onclick="printInsuranceOutStdReport()" class="btn btn-default col-md-12"/></div>
        </form>  
    </div>
</div>
<script type="text/javascript">

    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });

    function printInsuranceList() {
        generatePDf();
    }

    function generatePDf() {
        $("#formInsuranceList").submit();
    }

    function printInsuranceSummaryReport() {
        $("#formInsuranceSummary").submit();
    }

    function printInsuranceCommReport() {
        $("#formInsuranceComm").submit();
    }
    
    function printInsuranceRenewReport() {
        $("#formInsuranceRenew").submit();
    }
    
    function printInsuranceOutStdReport() {
        $("#formInsuranceOutstanding").submit();
    }


</script>




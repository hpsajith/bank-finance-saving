<%-- 
    Document   : cashierReport
    Created on : Aug 6, 2015, 12:40:06 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<form method="GET" action="/AxaBankFinance/ReportController/printRemoveLoansReport" target="blank" id="formRemoveLoansReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Cashier Report</legend></div>

        <div class="row">
            <div class="col-md-2">Loan Type</div>
            <div class="col-md-4">
                  <input type="hidden" id="reportid" name="report"/>
                <select id="loantypeid" name="loantype">
                    <option value="0">--Select Loan Type--</option>
                    <c:forEach var="loantype" items="${subLoanList}">
                        <option value="${loantype.subLoanId}"> ${loantype.subLoanName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="col-md-2"></div>
            <div class="col-md-3" style="margin-left: 0"><input type="button" name="" id="btnPrint" value="Print" onclick="printRemoveLoansReport()" class="btn btn-default col-md-10"/></div>
            <div class="col-md-1"></div>
            <div class="col-md-3" style="margin-left: -40px"><input type="button" name="" id="btnPrintAll" value="Print All" onclick="printRemoveLoansAllReport()" class="btn btn-default col-md-10"/></div>
        </div>
    </div>
</form>

<script type="text/javascript">

    function printRemoveLoansReport() {
        $("#reportid").val("N");
        if ($("#loantypeid").val() == 0) {
            alert("Please Select a Loan Type!");
            return;
        }
        generatePDf();
    }
    function printRemoveLoansAllReport() {
        $("#reportid").val("A");
        generatePDf();
    }

    function generatePDf() {
        $("#formRemoveLoansReport").submit();

    }


</script>




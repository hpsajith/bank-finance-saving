<%-- 
    Document   : customerDetail
    Created on : Feb 24, 2016, 11:35:21 AM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();

    });
</script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="row" style="margin: 5 8 2 0"><legend>Portfolio/Arrears Analysis Report</legend></div>
    <div class="row" style="">
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdAgreementNo" name="rdGrp" onclick="changeSelection(1)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">By Agreement No</label>
        </div>
        <div class="col-md-2">
            <input type="text" id="txtAgreementNo" name="agreementNo" style="width: 100%" disabled/>
        </div>
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdBranch" name="rdGrp" onclick="changeSelection(2)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">By Branch</label>
        </div>
        <div class="col-md-2">
            <select id="inpBranch" name="branchId" disabled style="width: 100%">
                <option value="0">All Branch</option>
                <c:forEach var="branch" items="${branches}">
                    <option value="${branch.branchId}">${branch.branchName}</option>
                </c:forEach>
            </select>
        </div>
    </div>

    <!--#############################-->
    <div class="row" style="">
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdFollowUpOfficer" name="rdGrp" onclick="changeSelection(3)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">Follow-Up Officer</label>
        </div>
        <div class="col-md-2">
            <select id="inpFollowUpOfficerId" name="officerId" disabled style="width: 100%">
                <option value="0">--SELECT--</option>
                <c:forEach var="names" items="${nameList}">
                    <option value="${names[3]}">${names[1]}</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdDueDay" name="rdGrp" onclick="changeSelection(4)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">Due Day</label>
        </div>
        <div class="col-md-2">
            <input type="number" id="txtDueDay" min="0" value="0" name="dueDay" style="width: 100%" disabled/>
        </div>
    </div>

    <div class="row" style="">
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdLoans" name="rdGrp" onclick="changeSelection(8)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">Loan Types</label>
        </div>
        <div class="col-md-2">
            <select id="inpLoans" name="officerId" disabled style="width: 100%">
                <option value="0">--SELECT--</option>
                <c:forEach var="subLoans" items="${subLoanTypeList}">
                    <option value="${subLoans.subLoanId}">${subLoans.subLoanName}</option>
                </c:forEach>
            </select>
        </div>

    </div>
    <hr>

    <div class="row" style="">
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdDueDay" name="rdGrp" onclick="changeSelection(6)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">Loan Amount Granted</label>
        </div>
        <div class="col-md-6">
            <input type="number" id="txtLoanAmountGranted" style="width: 29%; margin-right: 10px" >
            <label class="radio-inline"><input type="radio" id="laRadio1" value="1" name="optradio1">Above</label>
            <label class="radio-inline"><input type="radio" id="laRadio2" value="2" name="optradio1">Below</label>
        </div>
    </div>

    <div class="row" style="">
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdDueDay" name="rdGrp" onclick="changeSelection(7)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">Gross Rental</label>
        </div>
        <div class="col-md-6">
            <input type="number" id="txtGrossRental" style="width: 29%; margin-right: 10px" >
            <label class="radio-inline"><input type="radio" id="grRadio1" value="1" name="optradio2">Above</label>
            <label class="radio-inline"><input type="radio" id="grRadio2" value="2" name="optradio2">Below</label>
        </div>
    </div>
    <hr>

    <div class="row" style="">
        <div class="col-md-1" style="text-align: right">
            <input type="radio" id="rdDueDay" name="rdGrp" onclick="changeSelection(5)"/>
        </div>
        <div class="col-md-2" style="text-align: left">
            <label style="margin-top: 3%">Initiate Date</label>
        </div>
        <div class="col-md-3">
            From Date : 
            <input type="text" id="txtInitiateDate_from" style="width: 65%" class="txtCalendar" >
        </div>
        <div class="col-md-3">
            To Date :
            <input type="text" id="txtInitiateDate_to" style="width: 65%" class="txtCalendar" >
        </div>

        <div class="col-md-2">
            <input type="button" value="Print" onclick="loadPortfolioPage()" class="btn btn-edit col-md-12" style="margin-bottom: 10px;margin-top: -2%"/>
        </div>
    </div>

    <!--#############################-->

</div>
<form method="GET" action="/AxaBankFinance/ReportController/printLoanPostingReport" target="blank" id="formLoanPostingReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px; margin-top: 1.5%">
        <div class="row" style="margin: 5 8 2 0"><legend>General Ledger Entries Report</legend></div>
        <div class="row" style="">
            <div class="col-md-2" style="text-align: right">
                <label style="margin-top: 3%">Agreement No</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="txtAggNo" name="aggNo" style="width: 100%"/>
            </div>
            <div class="col-md-2" style="text-align: right">
                <label style="margin-top: 3%">Export As</label>
            </div>
            <div class="col-md-2">
                <select name="type" id="inpType" style="width: 100%">
                    <option value="A" selected>PDF</option>
                    <option value="B">Excel</option>
                </select>
            </div>
            <div class="col-md-3">
                <input type="button" value="Print" onclick="printLoanPostingReport()" class="btn btn-edit col-md-12" style="margin-bottom: 10px; margin-top: -2%"/>
            </div>
        </div>
    </div>
</form>
<form method="GET" action="/AxaBankFinance/ReportController/printCustomerBaseByLoanTypeReport" target="blank" id="formByLoanType">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;margin-top: 1%">
        <div class="row" style="margin: 5 8 2 0"><legend>Customer Base By Loan Type</legend></div>
        <div class="row" style="">
            <div class="col-md-2" style="text-align: right">
                <label style="margin-top: 3%">Loan Type</label>
            </div>
            <div class="col-md-3">
                <select id="inpLoanType" name="loanTypeId">
                    <option value="0">--SELECT--</option>
                    <c:forEach var="loanType" items="${subLoanTypes}">
                        <option value="${loanType.subLoanId}">${loanType.subLoanName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-3">
                <input type="button" value="Print" onclick="printByLoanTypeReport()" class="btn btn-edit col-md-12" style="margin-bottom: 10px;margin-top: -2%"/>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</form>
<form method="GET" action="/AxaBankFinance/ReportController/printRebateandFullyPaidList" target="blank" id="formRebateList">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Fully Paid and Rebate Loans</legend></div>
        <div class="row" style="">
            <div class="col-md-2">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
                <input type="button" value="Print" onclick="printRebateandFullyPaidReport()" class="btn btn-edit col-md-12" style="margin-bottom: 10px;margin-top: -2%"/>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</form>
<form method="GET" action="/AxaBankFinance/ReportController/printSMSReport" target="blank" id="formSMSReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>SMS Sent Report</legend></div>
        <div class="row" style="">
            <div class="col-md-2" style="text-align: right">
                <label style="margin-top: 3%">Send Date</label>
            </div>
            <div class="col-md-3">
                From Date : 
                <input type="text" id="fromDateId" name="fromDate" style="width: 65%" class="txtCalendar" required="true" >
            </div>
            <div class="col-md-3">
                To Date :
                <input type="text" id="toDateId" name="toDate" style="width: 65%" class="txtCalendar" required="true" >
            </div>
            <div class="col-md-3">
                <input type="button" value="Print" onclick="printSMSReport()" class="btn btn-edit col-md-12" style="margin-bottom: 10px;margin-top: -2%"/>
            </div>
            <div class="col-md-0"></div>
        </div>
    </div>
</form>
<form method="GET" action="/AxaBankFinance/ReportController/printCustomerInfoDetails" target="blank" id="formCustomerReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>View Customer Info Report</legend></div>
        <div class="row" style="">
            <div class="col-md-2">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
                <input type="button" value="Print" onclick="printCustomerInfo()" class="btn btn-edit col-md-12" style="margin-bottom: 10px;margin-top: -2%"/>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</form>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%; height: 75%"></div>
<script type="text/javascript">

    $(document).ready(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });

        $("#txtAgreementNo").attr("disabled", true);
        $("#inpBranch").attr("disabled", true);
        $("#inpFollowUpOfficerId").attr("disabled", true);
        $("#txtDueDay").attr("disabled", true);
        $("#txtInitiateDate_from").attr("disabled", true);
        $("#txtInitiateDate_to").attr("disabled", true);
        $("#txtLoanAmountGranted").attr("disabled", true);
        $("#laRadio1").attr("disabled", true);
        $("#laRadio2").attr("disabled", true);
        $("#txtGrossRental").attr("disabled", true);
        $("#grRadio1").attr("disabled", true);
        $("#grRadio2").attr("disabled", true);
    });

    var status = 0;
    function printByLoanTypeReport() {
        $("#formByLoanType").submit();
    }
    function printRebateandFullyPaidReport() {
        $("#formRebateList").submit();
    }
    function printSMSReport() {
        var fromDate = $("#fromDateId").val();
        var toDate = $("#toDateId").val();

        if (fromDate !== "" && toDate !== "") {
            $("#formSMSReport").submit();
        }else{
            alert("Please Select the Date Range!");
        }

    }

    function changeSelection(type) {
        status = type;
        if (type === 1) {
            $("#txtAgreementNo").attr("disabled", false);
            $("#inpBranch").attr("disabled", true);
            $("#inpFollowUpOfficerId").attr("disabled", true);
            $("#txtDueDay").attr("disabled", true);
            $("#txtInitiateDate_from").attr("disabled", true);
            $("#txtInitiateDate_to").attr("disabled", true);
            $("#txtLoanAmountGranted").attr("disabled", true);
            $("#laRadio1").attr("disabled", true);
            $("#laRadio2").attr("disabled", true);
            $("#txtGrossRental").attr("disabled", true);
            $("#grRadio1").attr("disabled", true);
            $("#grRadio2").attr("disabled", true);
            $("#inpLoans").attr("disabled", true);
        } else if (type === 2) {
            $("#txtAgreementNo").attr("disabled", true);
            $("#inpBranch").attr("disabled", false);
            $("#inpFollowUpOfficerId").attr("disabled", true);
            $("#txtDueDay").attr("disabled", true);
            $("#txtInitiateDate_from").attr("disabled", true);
            $("#txtInitiateDate_to").attr("disabled", true);
            $("#txtLoanAmountGranted").attr("disabled", true);
            $("#laRadio1").attr("disabled", true);
            $("#laRadio2").attr("disabled", true);
            $("#txtGrossRental").attr("disabled", true);
            $("#grRadio1").attr("disabled", true);
            $("#grRadio2").attr("disabled", true);
            $("#inpLoans").attr("disabled", true);
        } else if (type === 3) {
            $("#txtAgreementNo").attr("disabled", true);
            $("#inpBranch").attr("disabled", true);
            $("#inpFollowUpOfficerId").attr("disabled", false);
            $("#txtDueDay").attr("disabled", true);
            $("#txtInitiateDate_from").attr("disabled", true);
            $("#txtInitiateDate_to").attr("disabled", true);
            $("#txtLoanAmountGranted").attr("disabled", true);
            $("#laRadio1").attr("disabled", true);
            $("#laRadio2").attr("disabled", true);
            $("#txtGrossRental").attr("disabled", true);
            $("#grRadio1").attr("disabled", true);
            $("#grRadio2").attr("disabled", true);
            $("#inpLoans").attr("disabled", true);
        } else if (type === 4) {
            $("#txtAgreementNo").attr("disabled", true);
            $("#inpBranch").attr("disabled", true);
            $("#inpFollowUpOfficerId").attr("disabled", true);
            $("#txtDueDay").attr("disabled", false);
            $("#txtInitiateDate_from").attr("disabled", true);
            $("#txtInitiateDate_to").attr("disabled", true);
            $("#txtLoanAmountGranted").attr("disabled", true);
            $("#laRadio1").attr("disabled", true);
            $("#laRadio2").attr("disabled", true);
            $("#txtGrossRental").attr("disabled", true);
            $("#grRadio1").attr("disabled", true);
            $("#grRadio2").attr("disabled", true);
            $("#inpLoans").attr("disabled", true);
        } else if (type === 5) {
            $("#txtAgreementNo").attr("disabled", true);
            $("#inpBranch").attr("disabled", true);
            $("#inpFollowUpOfficerId").attr("disabled", true);
            $("#txtDueDay").attr("disabled", true);
            $("#txtInitiateDate_from").attr("disabled", false);
            $("#txtInitiateDate_to").attr("disabled", false);
            $("#txtLoanAmountGranted").attr("disabled", true);
            $("#laRadio1").attr("disabled", true);
            $("#laRadio2").attr("disabled", true);
            $("#txtGrossRental").attr("disabled", true);
            $("#grRadio1").attr("disabled", true);
            $("#grRadio2").attr("disabled", true);
            $("#inpLoans").attr("disabled", true);
        } else if (type === 6) {
            $("#txtAgreementNo").attr("disabled", true);
            $("#inpBranch").attr("disabled", true);
            $("#inpFollowUpOfficerId").attr("disabled", true);
            $("#txtDueDay").attr("disabled", true);
            $("#txtInitiateDate_from").attr("disabled", true);
            $("#txtInitiateDate_to").attr("disabled", true);
            $("#txtLoanAmountGranted").attr("disabled", false);
            $("#laRadio1").attr("disabled", false);
            $("#laRadio2").attr("disabled", false);
            $("#txtGrossRental").attr("disabled", true);
            $("#grRadio1").attr("disabled", true);
            $("#grRadio2").attr("disabled", true);
            $("#inpLoans").attr("disabled", true);
        } else if (type === 7) {
            $("#txtAgreementNo").attr("disabled", true);
            $("#inpBranch").attr("disabled", true);
            $("#inpFollowUpOfficerId").attr("disabled", true);
            $("#txtDueDay").attr("disabled", true);
            $("#txtInitiateDate_from").attr("disabled", true);
            $("#txtInitiateDate_to").attr("disabled", true);
            $("#txtLoanAmountGranted").attr("disabled", true);
            $("#laRadio1").attr("disabled", true);
            $("#laRadio2").attr("disabled", true);
            $("#txtGrossRental").attr("disabled", false);
            $("#grRadio1").attr("disabled", false);
            $("#grRadio2").attr("disabled", false);
            $("#inpLoans").attr("disabled", true);
        } else if (type === 8) {
            $("#txtAgreementNo").attr("disabled", true);
            $("#inpBranch").attr("disabled", true);
            $("#inpFollowUpOfficerId").attr("disabled", true);
            $("#txtDueDay").attr("disabled", true);
            $("#txtInitiateDate_from").attr("disabled", true);
            $("#txtInitiateDate_to").attr("disabled", true);
            $("#txtLoanAmountGranted").attr("disabled", true);
            $("#laRadio1").attr("disabled", true);
            $("#laRadio2").attr("disabled", true);
            $("#txtGrossRental").attr("disabled", true);
            $("#grRadio1").attr("disabled", true);
            $("#grRadio2").attr("disabled", true);
            $("#inpLoans").attr("disabled", false);
        }
    }

    function loadPortfolioPage() {
        var agreementNo = $('#txtAgreementNo').val().trim();
        var branchId = $('#inpBranch').find("option:selected").val();
        var officerId = $('#inpFollowUpOfficerId').find("option:selected").val();
        var dueDay = $('#txtDueDay').val().trim();
        var initiateFromDate = $('#txtInitiateDate_from').val();
        var initiateToDate = $('#txtInitiateDate_to').val();
        var loanAmountGranted = $('#txtLoanAmountGranted').val();
        var loanType = $('#inpLoans').find("option:selected").val();

        var lagValue;
        if (document.getElementById('laRadio1').checked) {
            lagValue = document.getElementById('laRadio1').value;
        } else if (document.getElementById('laRadio2').checked) {
            lagValue = document.getElementById('laRadio2').value;
        }

        var grossRental = $('#txtGrossRental').val();
        var grValue;
        if (document.getElementById('grRadio1').checked) {
            grValue = document.getElementById('grRadio1').value;
        } else if (document.getElementById('grRadio2').checked) {
            grValue = document.getElementById('grRadio2').value;
        }

        if (agreementNo !== "" || branchId !== "0" || officerId !== "0" || dueDay !== "0" || dueDay !== "" || initiateFromDate !== "" || initiateToDate !== "" || loanAmountGranted !== "" || grossRental !== "" || loanType !== "0") {
            $.ajax({
                url: '/AxaBankFinance/ReportController/loadPortfolioPage',
                data: {'agreNo': agreementNo, 'branchId': branchId, 'status': status, 'officerId': officerId, 'dueDay': dueDay, 'initiateFromDate': initiateFromDate, 'initiateToDate': initiateToDate, 'loanAmountGranted': loanAmountGranted, 'lagValue': lagValue, 'grossRental': grossRental, 'grValue': grValue, 'loanType': loanType},
                success: function (data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                },
                error: function () {
                    alert("Can't Create Report");
                }
            });
        } else {
            var message = "";
            if (agreementNo === "") {
                message = message + "Enter Agreement No\n";
            }
            if (branchId === "0") {
                message = message + "Select Branch";
            }
            if (officerId === "0") {
                message = message + "Select Officer";
            }
            if (dueDay === "") {
                message = message + "Select Due Day";
            }
            if (loanAmountGranted === "") {
                message = message + "Select Loan Amount";
            }
            if (initiateFromDate === "") {
                message = message + "Select Initiate From Date";
            }
            if (initiateToDate === "") {
                message = message + "Select Initiate To Date";
            }
            if (loanType === "0") {
                message = message + "Select Loan Type";
            }
            alert(message);
        }
    }

    function printLoanPostingReport() {
        $("#formLoanPostingReport").submit();
    }

    function printCustomerInfo() {
        $("#formCustomerReport").submit();
    }
</script>
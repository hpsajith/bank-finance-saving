<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>


<div class="row" style="margin: 5 8 2 0"><legend><label>Loan Installments (Ledger Report)</label></legend></div>
<div id="row" style="overflow: auto; height: 80%">
    <form method="GET" action="/AxaBankFinance/ReportController/PrintLoanIntallmentReport" target="blank" id="formBasicLoanDetails">
        <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

            <div class="row" style="margin-bottom: 10px" hidden="true">
                <div class="col-md-2"><label>Member No</label></div>
                <div class="col-md-3"><input type="text" id="txtLoanId"  value="${loanId}" name="loanId"  style="width: 80%"/></div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </form>
    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0"
           id="searchLoanTablee">
        <thead>
            <tr>
                <th>L.Index </th>
                <th>Agreement No</th>
                <th>Member No</th>
                <th>Customer Name</th>
                <th>Loan Amount</th>
                <th>Period</th>
                <th>Status</th>
                <th>Print</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="loans" items="${loanList}">
                <c:choose>
                    <c:when test="${loans.loanSpec == 6}">
                        <tr id="tr${loans.loanId}" style="cursor: pointer; background-color: #f3a3a3">
                            <td>${loans.loanCount}</td>
                            <td>${loans.agreementNo}</td>
                            <td>${loans.loanBookNo}</td>
                            <td>${loans.debtorName}</td>
                            <td>${loans.loanAmount}</td>
                            <td>${loans.loanPeriod}</td>
                            <c:choose>
                                <c:when test="${loans.loanSpec == 0}">
                                    <td>On Going</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 1}">
                                    <td>Laps</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 2}">
                                    <td>Legal</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 3}">
                                    <td>Seize</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 4}">
                                    <td>Fully Paid</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 5}">
                                    <td>Rebate</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 6}">
                                    <td>Terminate</td>
                                </c:when>
                                <c:otherwise>
                                </c:otherwise>
                            </c:choose>
                            <td><input type="button" name="" id="btnSearch" value="Print" onclick="printReport(${loans.loanId})" class="btn btn-sm col-md-12"/></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr id="tr${loans.loanId}" style="cursor: pointer">
                            <td>${loans.loanCount}</td>
                            <td>${loans.agreementNo}</td>
                            <td>${loans.loanBookNo}</td>
                            <td>${loans.debtorName}</td>
                            <td>${loans.loanAmount}</td>
                            <td>${loans.loanPeriod}</td>
                            <c:choose>
                                <c:when test="${loans.loanSpec == 0}">
                                    <td style="color: green">On Going</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 1}">
                                    <td style="color: red">Laps</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 2}">
                                    <td style="color: red">Legal</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 3}">
                                    <td style="color: red">Seize</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 4}">
                                    <td style="color: green">Fully Paid</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 5}">
                                    <td style="color: blue">Rebate</td>
                                </c:when>
                                <c:when test="${loans.loanSpec == 6}">
                                    <td style="color: red">Terminate</td>
                                </c:when>
                                <c:otherwise>
                                </c:otherwise>
                            </c:choose>
                            <td><input type="button" name="" id="btnSearch" value="Print" onclick="printReport(${loans.loanId})" class="btn btn-sm col-md-12"/></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-2"><input type="button" value="Close" onclick="unblockui()" class="btn btn-edit col-md-12"/>
    </div>
    <div class="col-md-1"></div>
</div>
<script>
    function clickLoan(id) {
        var selected = $("tr" + id).hasClass("highlight");
        $("#tblOfficers tbody tr").removeClass("highlight");
        if (!selected)
            $("tr" + id).addClass("highlight");
        $.ajax({
            url: '/AxaBankFinance/ReportController/PrintLoanIntallmentReport/',
            data: {loanId: id},
            success: function (data) {
                unblockui();
                $('#recoveSeachLoan').html(data);
            },
            error: function () {
                alert("Error Loading...3");
            }
        });
    }

    function printReport(id) {
        document.getElementById("txtLoanId").value = id;

        if (id === "") {
            alert("Please Enter the Member Number");
        } else {
            generatePDf();

        }
    }

    function generatePDf() {
        $("#formBasicLoanDetails").submit();
    }
</script>
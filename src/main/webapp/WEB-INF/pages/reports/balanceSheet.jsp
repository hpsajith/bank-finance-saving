<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
                
<form method="GET" action="/AxaBankFinance/ReportController/PrintDayEndBalanceSheet" target="blank" id="formbalanceReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Day End Cashier Balance Sheet</legend></div>
        <div class="row" style="">
            <div class="col-md-3"><input type="text" name="sysDate" value="${systemDate}" class="txtCalendar"/></div>
            <div class="col-md-3"><input type="button" value="Print" onclick="viewBalanceSheet()" class="btn btn-edit col-md-12"/></div>
            <div class="col-md-4"></div>
            <div class="col-md-2"></div>
        </div>
    </div>
</form>                

<form method="GET" action="/AxaBankFinance/ReportController/printArriesReport" target="blank" id="formArriesReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Arries Report</legend></div>
        <div class="row" style="">
            <div class="col-md-2">Date</div>
            <div class="col-md-3"><input type="text" name="date1" id="date1id" value="" class="txtCalendar" style="width: 80%"/></div>
            <div class="col-md-3" ><input type="text" name="date2" id="date2id" value="" class="txtCalendar" style="width: 80%"/></div>
            <div class="col-md-4"><input type="button" id="btnArries"value="Print" onclick="printArriesReport()" class="btn btn-edit col-md-12"/></div>
        </div>
    </div>
</form>


<script>
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });
    function viewBalanceSheet() {
        $("#formbalanceReport").submit();
    }
    function printArriesReport() {
        //document.getElementById("btnArries").disabled = true;
         $("#formArriesReport").submit();
            
    }
    
</script>
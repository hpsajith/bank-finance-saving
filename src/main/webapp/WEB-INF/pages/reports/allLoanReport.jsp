<%-- 
    Document   : cashierReport
    Created on : Aug 6, 2015, 12:40:06 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<form method="GET" action="/AxaBankFinance/ReportController/printAllLoanByBranch" target="blank" id="formcashierReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Cashier Report</legend></div>

        <div class="row" style="margin-bottom: 10px">
            
        </div>
        <div class="row">
            <div class="col-md-2">Select Branch</div>
            <div class="col-md-4">
                <select style="width: 80%" id="branchId_" name="branchId" >
                    <option value="0">-select the branch-</option>
                    <c:forEach var="v_branch" items="${branchList}">
                        <c:choose>
                            <c:when test="${v_branch.isActive == true}">
                                <option value="${v_branch.branchId}" >${v_branch.branchName}</option>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </select>

            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="col-md-2"></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrint" value="Print" onclick="generatePDfLoan()" class="btn btn-default col-md-10"/></div>
            <div class="col-md-1"></div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });
//    function printCashierReport() {
//        $("#reportid").val("N");
//        if($("#date1id").val().trim()=="" || $("#date2id").val().trim()==""){
//            alert("Please Select Date Range!");
//            return;
//        }
//        if($("#loantypeid").val()==0){
//            alert("Please Select a Loan Type!");
//            return;
//        }
//        generatePDf();
//    }
//    function printCashierReportAll() {
//        $("#reportid").val("A");
//        if($("#date1id").val().trim()=="" || $("#date2id").val().trim()==""){
//            alert("Please Select Date Range!");
//            return;
//        }
//        generatePDf();
//    }

    function generatePDfLoan() {
        $("#formcashierReport").submit();

    }


</script>




<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<form method="GET" action="/AxaBankFinance/ReportController/printLedgerReport" target="blank" id="formledgerReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Ledger Report</legend></div>

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2">Date</div>
            <div class="col-md-3"><input type="text" id="date1id" name="date1" value="" class="txtCalendar" style="width: 80%"/>&emsp;&emsp;To</div>
            <div class="col-md-3" ><input type="text" id="date2id" name="date2" value="" class="txtCalendar" style="width: 80%"/></div>
            <input type="hidden" id="reportid" name="report"/>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-2">Loan Type</div>
            <div class="col-md-4">
                <select id="loantypeid" name="loantype">
                    <option value="0">--Select Loan Type--</option>
                    <c:forEach var="loantype" items="${subLoanList}">
                        <option value="${loantype.subLoanId}"> ${loantype.subLoanName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" style="margin-top: 20px;margin-bottom: 10px;">
            <div class="col-md-2"></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrint" value="Print" onclick="printLedgerReport()" class="btn btn-default col-md-10"/></div>
            <div class="col-md-1"></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrintAll" value="Print All" onclick="printLedgerReportAll()" class="btn btn-default col-md-10"/></div>
        </div>

    </div>
</form>

<form method="GET" action="/AxaBankFinance/ReportController/printVoucherReport" target="blank" id="formVoucherReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Voucher Details</legend></div>

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2">Date</div>
            <div class="col-md-3"><input type="text" id="datev1id" name="datev1" value="" class="txtCalendar" style="width: 80%"/>&emsp;&emsp;To</div>
            <div class="col-md-3" ><input type="text" id="datev2id" name="datev2" value="" class="txtCalendar" style="width: 80%"/></div>
            <div class="col-md-4"></div>
        </div>
        <!--        <div class="row">
                    <div class="col-md-2">Loan Type</div>
                    <div class="col-md-4">
                        <select id="loantypeid" name="loantype">
                            <option value="0">--Select Loan Type--</option>
        <c:forEach var="loantype" items="${subLoanList}">
            <option value="${loantype.subLoanId}"> ${loantype.subLoanName}</option>
        </c:forEach>
    </select>
</div>
<div class="col-md-6"></div>
</div>-->
        <div class="row" style="margin-top: 20px;margin-bottom: 10px;">
            <div class="col-md-2"></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrint" value="Print" onclick="printVoucherReport()" class="btn btn-default col-md-10"/></div>
            <div class="col-md-1"></div>
            <!--<div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrintAll" value="Print All" onclick="printLedgerReportAll()" class="btn btn-default col-md-10"/></div>-->
        </div>

    </div>
</form>

<!--<form method="GET" action="/AxaBankFinance/ReportController/PrintMonthlyBusiness" target="blank" id="formMonthBusiReport">
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
        <div class="row" style="margin: 5 8 2 0"><legend>Monthly Business</legend></div>

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-2">Date</div>
            <div class="col-md-3"><input type="text" id="mfDate_id" name="mfDate" value="" class="txtCalendar" style="width: 80%"/>&emsp;&emsp;To</div>
            <div class="col-md-3" ><input type="text" id="mtDate_id" name="mtDate" value="" class="txtCalendar" style="width: 80%"/></div>

            <div class="col-md-4"></div>
        </div>

        <div class="row" style="margin-top: 20px;margin-bottom: 10px;">
            <div class="col-md-2"></div>
            <div class="col-md-3" style="margin-left: -32px"><input type="button" name="" id="btnPrint" value="Print" onclick="printMonthlyReport()" class="btn btn-default col-md-10"/></div>
            <div class="col-md-1"></div>           
        </div>

    </div>
</form>-->

<script type="text/javascript">
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });
    function printLedgerReport() {
        $("#reportid").val("N");
        if ($("#date1id").val().trim() == "" || $("#date2id").val().trim() == "") {
            alert("Please Select Date Range!");
            return;
        }
        if ($("#loantypeid").val() == 0) {
            alert("Please Select a Loan Type!");
            return;
        }
        generatePDf();
    }

    function printVoucherReport() {
        if ($("#datev1id").val().trim() == "" || $("#datev2id").val().trim() == "") {
            alert("Please Select Date Range!");
            return;
        }
        $("#formVoucherReport").submit();
    }

    function printLedgerReportAll() {
        $("#reportid").val("A");
        if ($("#date1id").val().trim() == "" || $("#date2id").val().trim() == "") {
            alert("Please Select Date Range!");
            return;
        }
        generatePDf();
    }

    function generatePDf() {
        $("#formledgerReport").submit();
    }

//    function printMonthlyReport() {
//        if ($("#mfDate_id").val().trim() == "" || $("#mtDate_id").val().trim() == "") {
//            alert("Please Select Date Range!");
//            return;
//        }
//        $("#formMonthBusiReport").submit();
//    }
</script>

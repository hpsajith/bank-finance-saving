<%-- 
    Document   : portFolioPage
    Created on : Apr 28, 2016, 8:20:38 AM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>

<form method="GET" action="/AxaBankFinance/ReportController/genaratePortfolioReport" id="formPortfolioReport">
    <div class="container-fluid " style="border:1px solid #BDBDBD; border-radius: 2px;height: 90%">
        <div class="row" style="margin: 0 8 2 0"><legend>Select Report Headers</legend></div>
        <input type="hidden" name="agreementNo" id="txtAgreementNo" value="${agreementNo}" />
        <input type="hidden" name="branchId" id="txtBranchId" value="${branchId}" />
        <input type="hidden" name="status" id="txtStatus" value="${status}" />
        <input type="hidden" name="officerId" id="txtOfficerId" value="${officerId}" />
        <input type="hidden" name="dueDay" id="txtdueDay" value="${dueDay}" />
        <input type="hidden" name="initiateFromDate" id="txtInitiateFromDate" value="${initiateFromDate}" />
        <input type="hidden" name="initiateToDate" id="txtinItiateToDate" value="${initiateToDate}" />
        <input type="hidden" name="loanAmountGranted" id="txtLoanAmount"  value="${loanAmountGranted}"/>
        <input type="hidden" name="lagValue" id="txtLagValue" value="${lagValue}"  />
        <input type="hidden" name="grossRental" id="txtLagValue" value="${grossRental}"  />
        <input type="hidden" name="grValue" id="txtLagValue" value="${grValue}"  />
        <input type="hidden" name="loanType" id="txtLagValue" value="${loanType}"  />
        <div class="row" style="overflow-y: auto;height: 90%">
            <table class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblFields">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Field Name</th>
                    </tr>
                </thead>
                <body>
                    <c:forEach var="field" items="${fields}">
                    <tr id="${field.index}">
                        <td><input type="checkbox" checked="true" id="fd${field.index}"/></td>
                        <td>${field.name}</td>
                    </tr>
                </c:forEach>
                </body>
            </table>
        </div>

    </div>
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-5"></div>
        <div class="col-md-1"></div>
        <div class="col-md-3"><input type="button" onclick="unblockui()" class="btn btn-default col-md-12" value="Cancel"/></div>
        <div class="col-md-3"><input type="button" onclick="printPortfolioReport()" class="btn btn-default col-md-12" value="Print"/></div>
    </div>
    <div id="hideFields"></div>
</form>
<script>
    function printPortfolioReport() {
        setFields();
        $("#formPortfolioReport").submit();
        //ui("#proccessingPage");
    }

    function setFields() {
        $('#hideFields').html("");
        $('#tblFields tbody tr').each(function () {
            var field = $(this).attr('id');
            if ($('#fd' + field).is(':checked')) {
                var fieldName = $(this).find('td:eq(1)');
                $('#hideFields').append("<input type = 'hidden' name = 'fields' value = '" + fieldName.text() + "'/>");
            }
        });

    }
</script>


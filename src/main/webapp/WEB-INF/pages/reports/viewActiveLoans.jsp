
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<script>
    $(document).ready(function () {
        pageAuthentication();
        unblockui();
    });
</script>

<form method="GET" action="/AxaBankFinance/ReportController/printActiveCheques" target="blank" id="printActiveCheques">
    <label style="color: #08C; font-size: 14px"> Active Loans- Print Cheque</label>

<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
    <div class="col-md-1">Start Date</div>
    <div class="col-md-2"><input type="text" id="txt_startDate" class="txtCalendar" value="${sdate}"></div>
    <div class="col-md-1">End Date</div>
    <div class="col-md-2"><input type="text" id="txt_endDate" class="txtCalendar" value="${edate}"/></div>
    <div class="col-md-1"><input type="button" class="btn btn-default searchButton" id="btnSearchLoan"
                                 style="height: 25px"></div>
    <%--<input type="hidden" value="${type}" id="txt_isissue"/>--%>
</div>

<div class="row" style="padding-top: 10px;overflow: auto">
    <input type="hidden" id="Loan_id" name="Loan_id"/>

    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0"
           id="tblViewLoan">
        <thead>
        <tr>
            <th style="text-align: center">#</th>
            <th style="text-align: center">No</th>
            <th style="text-align: center">Name</th>
            <th style="text-align: center">Loan</th>
            <th style="text-align: center">Loan Date</th>
            <th style="text-align: center">Due Date</th>
            <th style="text-align: center">Type</th>
            <th style="text-align: center">Period</th>
            <th style="text-align: center">Rental</th>
            <th style="text-align: center">Branch</th>
            <th style="text-align: center">Cheque</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="loan" items="${loanList}">
            <tr>
                <td style="text-align: center"><input type="checkbox" ${loan.loanStatus==2?'disabled':''}
                                                      class="loanCheckBox" id="loanCheck${loan.loanId}"
                                                      onclick="selectLoan(${loan.loanId})"/></td>
                <c:choose>
                    <c:when test="${loan.loanIsIssue==0}">
                        <td style="text-align: center">${loan.loanId}</td>
                    </c:when>
                    <c:otherwise>
                        <td style="text-align: left">${loan.agreementNo} </br>
                            ------------------------- </br> ${loan.loanBookNo}</td>
                    </c:otherwise>
                </c:choose>
                <td style="text-align: center">${loan.debtorHeaderDetails.debtorName}</td>
                <td style="text-align: right">${loan.loanAmount}</td>
                <td style="text-align: center">${loan.loanDate}</td>
                <td style="text-align: center">${loan.dueDate}</td>
                <td style="text-align: center">${loan.loanType}</td>
                <td style="text-align: center">${loan.loanPeriod}</td>
                <td style="text-align: right">${loan.loanInstallment}</td>
                <!--Branch Code-->
                <c:forEach var="branches" items="${branch}">
                    <c:if test="${branches.branchId == loan.branchId}">
                        <td style="width: 40px;text-align: center">${branches.branchName}</td>
                    </c:if>
                </c:forEach>
                <td style="text-align: right"><a href="#"
                                                 onclick="printActiveCheques(${loan.loanId}, 1)">print</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</div>

<!-- btn Panel-->
<div class="row" style="margin-top: 25px;">
</div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..."/>
</div>
</form>
<script>

    $(document).ready(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });

        $('#btnSearchLoan').click(function () {
            var startDate = $("#txt_startDate").val();
            var endDate = $("#txt_endDate").val();
            ui('#proccessingPage');
            $.ajax({
                data: {start: startDate, end: endDate},
                url: '/AxaBankFinance/viewActiveLoans',
                success: function (data) {
                    $('#formContent').html(data);
                }
            });
        });
    });

    var LOANID;
    function selectLoan(loanId) {
        if ($('#loanCheck' + loanId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#loanCheck' + loanId).prop('checked', true);
            $(".btnLoans").prop("disabled", false);
            LOANID = loanId;
        } else {
            $(".btnLoans").prop("disabled", true);
        }
    }

    function printActiveCheques(loanId) {
        $("#Loan_id").val(loanId);
                $("#printActiveCheques").submit();
    }

</script>
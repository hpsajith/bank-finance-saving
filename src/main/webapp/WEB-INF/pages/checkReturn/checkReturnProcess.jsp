<%-- 
    Document   : checkReturnProcess
    Created on : Jul 22, 2015, 1:45:51 PM
    Author     : ITESS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>

    <div class="col-md-12 col-lg-12 col-sm-12" style="display:block;">

        <c:choose>
            <c:when test="${priviledge}">
                <form name="checkReturnForm" id="checkReturnForm" > 

                    <input type="hidden" value="${checkDetaiId}" name="checkDetaiId" id="checkDetaiId" readonly="true">    
                    <input type="hidden" value="${systemDate}" name="systemDate" id="systemDate" readonly="true">  
                    <input type="hidden" value="${loanAggNo}" id="loanAggNo">  

                    <div class="row"></div>
                    <h1>Change The Cheque Status !</h1>
                    <div class="row"></div>

                    <!--<div class="row divError" style="padding:10px; "></div>-->  
                    <label id="msgRealizeDate" class="msgTextField"></label> 
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-4">Realize Date </div>
                        <div class="col-md-6 col-lg-6 col-sm-6"><input type="text" value="${detail.realizeDate}" style="width:100%" name="realizeDate" id="realizeDate" readonly="true"/></div>
                    </div>
                    <label id="msgReason" class="msgTextField"></label> 
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-4">Cheque Return note* </div>
                        <div class="col-md-6 col-lg-6 col-sm-6"><input type="text" value="${remark}" style="width:100%" name="remark" id="remark"/></div>
                    </div>
                    <label id="msgChargeType" class="msgTextField"></label> 
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-4">Charge Type* </div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <select name="chargeType" id="txtchargeType" class="col-md-12" style="padding: 1">
                                <option value="0">--SELECT--</option>
                                <c:forEach var="charges" items="${chargeTypeList}">
                                    <option value="${charges.subTaxId}">${charges.subTaxDescription}</option>
                                </c:forEach>
                            </select></div>
                    </div>
                    <label id="msgAmount" class="msgTextField"></label> 
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-4">Check Return Charge* </div>
                        <div class="col-md-6 col-lg-6 col-sm-6"><input type="text" value="0" style="width:100%" name="chargeAmount" id="chargeAmount"/></div>
                    </div>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-md-4">Bank Name</div>
                        <div class="col-md-6">
                            <select name="bankAccount" id="bankAccId" class="col-md-12" style="padding: 1">  
                                <c:forEach var="bank" items="${banks}">
                                    <option value="${bank.bankId}">${bank.configBankName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div  class="col-md-4 "><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
                    <div  class="col-md-4 "><input type="button" id="changeCheckStatus"  value="Set As Return" onclick="changeCheck(1)" class="btn btn-danger col-md-12 col-sm-12 col-lg-12"/></div>
                    <div  class="col-md-4 "><input type="button"  value="Cheque is Realized" onclick="changeCheck(2)" class="btn btn-success col-md-12 col-sm-12 col-lg-12"/></div>
                </form>
            </c:when>
            <c:otherwise>
                <h3>You don't have privileges !</h3>
                <div  class="col-md-3 "><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
                </c:otherwise>
            </c:choose>

    </div>    
</div>

<script>
    function changeCheck(status) {
        var validatef = true;
        if (status === 1) {
            validatef = validateForm();
        }
        var validateD = validateDate();
        if (validatef && validateD) {
            document.getElementById('changeCheckStatus').disabled = true;
            var checkDetaiId = $('#checkDetaiId').val();
            var loanAggNo = $('#loanAggNo').val();
            var remark = $('#remark').val();
            var chargeAmount = $('#chargeAmount').val();
            var chargeType = $('#txtchargeType').val();
            var bank = $("#bankAccId").val();
            $.ajax({
                url: '/AxaBankFinance/CheckReturnController/saveCheckReturn',
                type: 'GET',
                data: {"checkDetaiId": checkDetaiId, "aggNo" : loanAggNo, "remark": remark, "status": status, "chargeAmount": chargeAmount, "chargeType": chargeType, "bank": bank},
                success: function(data) {
                    $('#divProcess').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    loadCheqPayments();
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        } else {
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $("#msgReason").html(errorMessage);
            $("#msgAmount").html(errorMessage2);
            $("#msgRealizeDate").html(errorMessage3);
            $("#msgChargeType").html(errorMessage4);

            errorMessage = "";
            errorMessage2 = "";
            errorMessage3 = "";
            errorMessage4 = "";

//                $("html, body").animate({scrollTop: 0}, "slow");
        }
    }

    var errorMessage = "";
    var errorMessage2 = "";
    var errorMessage4 = "";
    function validateForm() {
        var validate = true;
        var remark = $('input[name=remark]').val();
        if (remark === null || remark === "") {
            validate = false;
            errorMessage = errorMessage + "Enter Remark";
            $("#remark").addClass("txtError");
        } else {
            $("#remark").removeClass("txtError");
        }
        var chargeAmount = $('input[name=chargeAmount]').val();
        if (chargeAmount === null || chargeAmount === "0") {
            validate = false;
            errorMessage2 = errorMessage2 + "Enter Charge Amount";
            $("#chargeAmount").addClass("txtError");
        } else {
            $("#chargeAmount").removeClass("txtError");
        }

        var chargeType = $('input[name=chargeType]').val();
        if (chargeType === 0 || chargeAmount === "0") {
            validate = false;
            errorMessage4 = errorMessage4 + "Select Charge Type";
            $("#chargeType").addClass("txtError");
        } else {
            $("#chargeType").removeClass("txtError");
        }
//        chargeAmount

        return validate;
    }
    var errorMessage3 = "";
    function validateDate() {
        var validate = true;
        var realizeDate = $('input[name=realizeDate]').val();
        var systemDate = $('input[name=systemDate]').val();
        if (realizeDate <= systemDate) {
            $("#realizeDate").removeClass("txtError");
        } else {
            validate = false;
            errorMessage3 = errorMessage3 + "Realize Date Not Exceed";
            $("#realizeDate").addClass("txtError");
        }
        return validate;
    }

    function loadCheqPayments() {

        $.ajax({
            url: '/AxaBankFinance/CheckReturnController/loadCheckPayment',
            success: function(data) {
                $('#formContent').html("");
                $('#formContent').html(data);
            },
            error: function() {
            }
        });
    }
</script>
<%-- 
    Document   : fieldOfficerSearchPage
    Created on : Jul 8, 2015, 4:07:55 PM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#fieldOfficerSearchTable").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>
<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption"> 
    <div class="row"><label style="margin-left: 20px;">Search Letter / Visit Details</label></div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Agreement</div>
            <div class="col-md-8">
                <input type="text"  style="width:100%" id="txtAggNo" onkeyup="searchByAggNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtVehNo" onkeyup="searchByVehNo()"/>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
<div class="row" style="margin-top: 10px" id="tblLetters">
    <table class="dc_fixed_tables table-bordered" id="fieldOfficerSearchTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th style="text-align: center">Agreement No</th>
                <th style="text-align: center">Vehicle No</th>
                <th style="text-align: center">Customer Name</th>
                <th style="text-align: center">Due Date</th> 
                <th style="text-align: center">Arrears Amount</th>
                <th style="text-align: center">History</th>
                <th style="text-align: center">Comment</th>
                <th style="text-align: center">Charge</th>
                <th style="text-align: center">Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="loans" items="${loanList}">

                <tr style="background-color:${loans.color}">  
                    <c:choose>
                        <c:when test="${loans.letteId>9}">
                            <c:set var="isLetter" value="n"></c:set>
                        </c:when>
                        <c:otherwise>
                            <c:set var="isLetter" value="y"></c:set>
                        </c:otherwise>
                    </c:choose>

                <!--<td>${loans.loanId}</td>-->
                    <td id="agg${loans.loanId}${isLetter}" style="text-align: center">${loans.agreementNo}</td>  
                    <td id="book{loans.loanId}${isLetter}" style="text-align: center">${loans.vehicleNo}</td>  
                    <td style="text-align: center">${loans.debtorName}</td>                  
                    <td id="due${loans.loanId}${isLetter}" style="text-align: center">${loans.dueDate}</td>                  
                    <td><input type="text" value="${loans.arresAmount}" style="width:100px;text-align: right" id="amnt${loans.loanId}${isLetter}" readonly=""/></td>
                    <td style="text-align: center"><div class="view_hist" onclick="loadLetterHistory(${loans.loanId})" title="View History"></div></td>                
                    <td><input type="text" id="comme${loans.loanId}${isLetter}"/></td>
                    <td><input type="text" value="${loans.odi}" id="chrg${loans.loanId}${isLetter}" style="width:50px;text-align: right"/></td>
                    <td style="text-align: center">
                        <div class="row">
                            <c:choose>
                                <c:when test="${loans.letteId>9}">
                                    <!--visit loans-->
                                    <c:choose>
                                        <c:when test="${loans.recoverId==0}">
                                            <div class="col-md-6"> <div id="post${loans.loanId}${isLetter}" class="assign_off" onclick="sendVisit(${loans.loanId},${loans.letteId}, 0)"></div></div>
                                            </c:when>
                                            <c:otherwise>
                                            <div class="col-md-6"> <div id="assgn${loans.loanId}${isLetter}" class="edit_officer" onclick="loadManagerComment(${loans.loanId},${loans.letteId},${loans.recoverId})"></div></div>
                                            </c:otherwise>
                                        </c:choose>                                    
                                    <div class="col-md-6"><div id="cancel${loans.loanId}${isLetter}" class="cancel_letter" onclick="sendVisit(${loans.loanId},${loans.letteId}, 1)"></div></div>
                                    </c:when>
                                    <c:otherwise>
                                    <!--letters-->
                                    <div class="col-md-6"><div id="post{loans.loanId}${isLetter}" class="posting_letter" onclick="postLetter(${loans.loanId},${loans.letteId}, 0)" title="Post Letter"></div></div>
                                    <div class="col-md-6"><div id="cancel${loans.loanId}${isLetter}" class="cancel_letter" onclick="postLetter(${loans.loanId},${loans.letteId}, 1)" title="Cancel Letter"></div></div>
                                    </c:otherwise>
                                </c:choose>                        
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div class="row" style="margin-top: 20px;">
    <div class="col-md-9">
        <div class="row">
            <c:forEach var="lett" items="${letters}">
                <div class="col-md-2">
                    <c:if test="${lett.isActive}">
                        <label>${lett.letterName}</label>
                        <div style="width:10px; height: 10px; background: ${lett.color}"></label> </div>
                    </c:if>
                </div>
            </c:forEach>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-4">
                <label>Visit 1</label>
                <div style="width:10px; height: 10px; background: #F2FFC9"></label> </div>
            </div>
            <div class="col-md-4">
                <label>Visit 2</label>
                <div style="width:10px; height: 10px; background: #ABC147"></label> </div>
            </div>
            <div class="col-md-4">
                <label>Visit 3</label>
                <div style="width:10px; height: 10px; background: #608D4E"></label> </div>
            </div>
        </div>
    </div>
</div>
<!--letters form-->
<form name="letterPs" id="letterPost_form">
    <input type="hidden" name="loanId"/>
    <input type="hidden" name="letterId"/>
    <input type="hidden" name="agreementNo"/>
    <input type="hidden" name="dueDate"/>
    <input type="hidden" name="balance"/>
    <input type="hidden" name="LetterCharge"/>
    <input type="hidden" name="letterStatus"/>
    <input type="hidden" name="comment"/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<!--cancel visit-->
<form name="cancelVisitForm" id="cancelVisit_form">
    <input type="hidden" name="loan_id" id="canc_loanId"/>
    <input type="hidden" name="dueDate" id="canc_duedate"/>
    <input type="hidden" name="officer_id" value="0"/>
    <input type="hidden" name="visitType" id="canc_visitType"/>
    <input type="hidden" name="comment" id="canc_comment"/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<div id="letterDetals" class="searchCustomer" style="width: 85%;border: 0px solid #5e5e5e;margin-left: -15%;">
</div>

<div id="message_posting" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div class="row">
    <form method="GET" action="/AxaBankFinance/RecoveryController/generateLetter" target="blank" id="printLetterForm">
        <input type="hidden" name="rletterId" id="rletterId" value="1"/>   
        <input type="hidden" name="loanId_l" id="rloanId"/>   
        <input type="hidden" name="letterId_l" />   
    </form>
</div>
<div class="searchCustomer" id="recoveryOff" style="width: 40%; height:23%; margin-top: 15%; margin-left: 8%; border:2px solid #444">
</div>
<div class="searchCustomer" style="width:40%; margin: 5% 10%" id="popup_recovery_approval" ></div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..." />
</div>
<script>

    function postLetter(loanId, letterId, isCancel) {

        var isLetter = 'y'

        var agreementNo = $("#agg" + loanId + isLetter).text();
        var charge = $("#chrg" + loanId + isLetter).val();
        var arearsAmount = $("#amnt" + loanId + isLetter).val();
        var comment = $("#comme" + loanId + isLetter).val();
        var dueDate = $("#due" + loanId + isLetter).text();

        $("input[name=loanId]").val(loanId);
        $("input[name=letterId]").val(letterId);
        $("input[name=agreementNo]").val(agreementNo);
        $("input[name=dueDate]").val(dueDate);
        $("input[name=balance]").val(arearsAmount);
        $("input[name=LetterCharge]").val(charge);
        $("input[name=comment]").val(comment);

        var hasComment = true;
        if (isCancel == 0) {
            //LetterPost
            $("input[name=letterStatus]").val(1);
        } else {
            //Letter Cancel
            $("input[name=letterStatus]").val(2);
            if (comment == "") {
                hasComment = false;
            }
        }

        if (hasComment) {
            $("#post" + loanId + isLetter).css("background", "#123");
            $("#cancel" + loanId + isLetter).css("background", "#321");

            $("#post" + loanId + isLetter).prop("disabled", true);
            $("#cancel" + loanId + isLetter).prop("disabled", true);
            $("#comme" + loanId + isLetter).removeClass("txtError");
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/saveLetter/',
                data: $("#letterPost_form").serialize(),
                type: 'POST',
                success: function (data) {
                    $('#message_posting').html("");
                    $('#message_posting').html(data);
                    ui('#message_posting');
                    loadRecoveryArrearsLoans();
                    $("input[name=loanId_l]").val(loanId);
                    $("input[name=letterId_l]").val(letterId);
                    if (isCancel == 0) {
                        printLetter();
                    }
                },
                error: function () {
                    alert("Error Loading letters");
                }
            });
        } else {
            $("#comme" + loanId + isLetter).addClass("txtError");
            warning("Add Comment", "message_posting");
        }
    }


    function sendVisit(loan_Id, letterId, isCancel) {
        var due_Date = $("#due" + loan_Id + 'n').text();
        var comment = $("#comme" + loan_Id + 'n').val();
        var visitType = 0;
        if (letterId == 10) {
            visitType = 1;
        } else if (letterId == 11) {
            visitType = 2;
        } else {
            visitType = 2;
        }
        if (isCancel == 0) {
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/assignRecoveryOfficer/',
                data: {loanId: loan_Id, dueDate: due_Date, visitType: visitType, comment: comment},
                success: function (data) {
                    $('#recoveryOff').html("");
                    $('#recoveryOff').html(data);
                    ui('#recoveryOff');
                },
                error: function () {
                    alert("Error Loading letters");
                }
            });
        } else {
            if (comment === "") {
                $("#comme" + loan_Id + 'n').addClass("txtError");
                warning("Add Comment", "message_posting");
            } else {
                $("#comme" + loan_Id + 'n').removeClass("txtError");
                $("#canc_duedate").val(due_Date);
                $("#canc_comment").val(comment);
                $("#canc_loanId").val(loan_Id);
                $("#canc_visitType").val(visitType);

                $.ajax({
                    url: "/AxaBankFinance/RecoveryController/saveRecoverOfficer",
                    data: $("#cancelVisit_form").serialize(),
                    type: "POST",
                    success: function (data) {
                        $("#message_posting").html(data);
                        ui("#message_posting");
                        loadRecoveryArrearsLoans();
                    },
                    error: function () {
                        alert("Error Loading...");
                    }
                });
            }
        }
    }

    function loadManagerComment(loan_Id, letterId, recId) {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadManagerComment/',
            data: {loanId: loan_Id, letterId: letterId, recId: recId},
            success: function (data) {
                $('#popup_recovery_approval').html("");
                $('#popup_recovery_approval').html(data);
                ui('#popup_recovery_approval');
            },
            error: function () {
                alert("Error Loading letters");
            }
        });
    }

    function loadLetterHistory(loanId) {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadLetterHistory/',
            data: {loanId: loanId},
            success: function (data) {
                $('#letterDetals').html("");
                $('#letterDetals').html(data);
                ui('#letterDetals');
            },
            error: function () {
                alert("Error Loading letters");
            }
        });
    }

    function printLetter() {
        $("#printLetterForm").submit();
    }

    function searchByAggNo() {
        var aggNo = $("#txtAggNo").val().trim();
        if (aggNo.length > 9) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/loadRecoveryArrearsLoansByAgNo",
                data: {"agNo": aggNo},
                success: function (data) {
                    $("#tblLetters").html("");
                    $("#tblLetters").html(data);
                }
            });
        }
    }

    function searchByVehNo() {
        var vehNo = $("#txtVehNo").val().trim();
        if (vehNo.length >= 4) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/loadRecoveryArrearsLoansByVehNo",
                data: {"vehNo": vehNo},
                success: function (data) {
                    $("#tblLetters").html("");
                    $("#tblLetters").html(data);
                }
            });
        }
    }

    function viewLetter(loanId, letterId) {
        $("input[name=loanId_l]").val(loanId);
        $("input[name=letterId_l]").val(letterId);
        $("#printLetterForm").submit();
    }


</script>


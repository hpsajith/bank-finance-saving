<%-- 
    Document   : seizeYardRegisterForm
    Created on : Feb 3, 2016, 11:00:08 AM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Add to Yard</strong>
    </div>
    <form name="yardRegisterForm" id ="yardRegister">
        <div class="container-fluid">
            <input type="hidden" name="yardId" value="${yardRegister.yardId}" id="txtYardId"/>
            <input type="hidden" name="seizeOrderId" value="${yardRegister.seizeOrderId}" id="txtSeizeOrderId"/>
            <input type="hidden" name="loanId" value="${yardRegister.loanId}" id="txtLoanId"/>
            <input type="hidden" name="yardOfficerId" value="${yardRegister.yardOfficerId}" id="txtYardOfficerId"/>
            <input type="hidden" name="yardDate" value="${yardRegister.yardDate}" id="txtYardDate"/>
            <div class="row" >
                <div class="col-md-4" style="text-align: left">Seizer's Charge</div>
                <div class="col-md-8"><input type="text" name="seizerCharge" id="txtSeizeCharge" value="${yardRegister.seizerCharge}" onkeypress="return checkNumbers(this)" style="width: 100%"/></div>                
            </div>
            <div class="row" >
                <div class="col-md-4" style="text-align: left">Sizing commission </div>
                <div class="col-md-8"><input type="text" name="seizingCommission" id="txtSeizingCommission" onkeypress="return checkNumbers(this)" value="${yardRegister.seizingCommission}" style="width: 100%"/></div>                
            </div>
            <div class="row" >
                <div class="col-md-4" style="text-align: left">Remark</div>
                <div class="col-md-8"><textarea rows="3" name="remark" id="txtRemark" style="width: 100%">${yardRegister.remark}</textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div> 
                    <c:if test="${yardRegister.yardId == null}">
                    <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="saveYardRegister()"></div>
                    </c:if>
            </div>
            <label style="margin-right: 25%;" class="redmsg">Can't Change to the Seizing Charges after the Save...!</label>
        </div>        
    </form>
</div> 
<div id="message_save" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">

    function saveYardRegister() {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/saveOrUpdateYardRegister',
            type: 'POST',
            data: $("#yardRegister").serialize(),
            success: function(data) {
                $('#message_save').html("");
                $('#message_save').html(data);
                ui("#message_save");
                setTimeout(function() {
                    loadSeizeYardRegisterPage();
                }, 1500);
            }
        });
    }
</script>


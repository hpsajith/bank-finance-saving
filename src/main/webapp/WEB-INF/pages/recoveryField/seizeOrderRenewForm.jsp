<%-- 
    Document   : seizeOrderRenewForm
    Created on : Feb 11, 2016, 5:02:00 PM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Re-New Seize Order</strong>
    </div>
    <form name="renewalSeizeOrderForm" id ="renewalSeizeOrder">
        <div class="col-md-12">
            <input type="hidden" name="loanId" id="txtRLoanId"/>
            <input type="hidden" name="debtorId" id="txtRDebtorId"/>
            <input type="hidden" name="vehicleNo" id="txtRvehicleNo"/>
            <input type="hidden" name="arrearsRental" id="txtRArrearsRental"/>
            <input type="hidden" name="seizerId" id="txtRSeizerId"/>
            <input type="hidden" name="orderDate" id="txtROrderDate"/>
            <input type="hidden" name="approval_1" id="txtRApproval1"/>
            <input type="hidden" name="approval_2" id="txtRApproval2"/>
            <input type="hidden" name="isDelete" id="txtRIsDelete"/>
            <input type="hidden" name="isKeyIssue" id="txtRIsKeyIssue"/>
            <input type="hidden" name="keyIssueDate" id="txtRKeyIssueDate"/>
            <input type="hidden" name="isRenew" id="txtRIsRenew"/>
            <div class="row" style="margin-top: 5px;">
                <div class="col-md-2">Reason</div>
                <div class="col-md-10"><textarea rows="3" name="renewalRemark" id="txtRenewalRemark" style="width: 100%"></textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div> 
                <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="saveRenewalSeizeOrder()"></div>
            </div>
        </div>        
    </form>
</div> 
<div id="message_save" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">

    function saveRenewalSeizeOrder() {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/saveRenewalSeizeOrder',
            type: 'POST',
            data: $("#renewalSeizeOrder").serialize(),
            success: function(data) {
                $('#message_save').html("");
                $('#message_save').html(data);
                ui("#message_save");
                setTimeout(function() {
                    loadSeizeOrderRenewPage();
                }, 1500);
            }
        });
    }
</script>


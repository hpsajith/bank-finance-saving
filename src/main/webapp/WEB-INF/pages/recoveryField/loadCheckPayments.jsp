<%-- 
    Document   : loadCheckPayments
    Created on : Jul 22, 2015, 9:59:29 AM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">      
    <div class="col-md-2">
        <h4>${systemDate}</h4>     
    </div>
</div> 
<div class="row" id="searchOption">  
    <div class="col-md-4">
        <div class="col-md-4">Due Date</div>
        <div class="col-md-8">
            <input type="text"  class="txtCalendar"  value="${fromDate}" style="width:100%" name="" id="fromDate"/>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-4">Due Date</div>
        <div class="col-md-8">
            <input type="text"  class="txtCalendar2"  value="${toDate}" style="width:100%" name="" id="toDate"/>
        </div>
    </div>    
</div>    

<div id="onloadchechPayments">
    <table class="dc_fixed_tables table-bordered" id="fieldOfficerTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th></th>
                <th>Agreement No</th>
                <th>Debtor Account No</th>    
                <th>Receipt No</th>
                <th>Bank Name</th>
                <th>Cheque No</th>
                <th>Realize Date</th>
                <th>Paid Amount</th>    
            </tr>
        </thead>
        <tbody>
            <c:forEach var="loans" items="${checkPaymentList}">                
                <c:choose>
                    <c:when test="${loans.isReturn}">
                        <tr style="background-color: #e4b9c0"> 
                            <td></td>        
                    </c:when>
                    <c:otherwise>
                        <tr> 
                            <td><input type="checkbox" class="paymentCheckBox" id="payment${loans.chequeId}" onclick="selectLoanD(${loans.chequeId})"/></td>     
                    </c:otherwise>
                </c:choose>              
                
                <td>${loans.agreementNo}</td>
                <td>${loans.debtorAccountNo}</td>    
                <td>${loans.receiptNo}</td>
                <td>${loans.bankName}</td>
                <td>${loans.chequeNo}</td>
                <td>${loans.realizeDate}</td>
                <td>${loans.amount}</td>    
        </c:forEach>
        </tbody>
    </table>
</div>
        
<div id="searcedCheckPayments"></div> 
<div id="divProcess" class="jobsPopUp" style="width: 40%;height: 30%"></div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>
    $(function() {
         $(".txtCalendar").datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });
        
        $(".txtCalendar2").datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            onSelect: function(selected, evnt) {
                searchByDueDate(selected);
            }
        });
    });
    
     function searchByDueDate(selected){
            var fromDate =$('#fromDate').val();
            var toDate = selected;
            if (toDate.length > 0) {
                $.ajax({
                    url: '/AxaBankFinance/RecoveryController/searchCheqPayments/',
                    data:{"fromDate":fromDate,"toDate":toDate},
                    success: function(data) {
                        $('#onloadchechPayments').html("");
                        $('#searcedCheckPayments').html(data);
                    },
                    error: function() {
                        alert("Error Loading...3");
                    }
                });
            }
        }
        
           function selectLoanD(checkDetaiId) {
        if ($('#payment' + checkDetaiId).is(':checked')) {
            $('.paymentCheckBox').prop('checked', false);
            $('#payment' + checkDetaiId).prop('checked', true);
                        
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/checkReturnProcess',
                data: {"checkDetaiId":checkDetaiId},
                success: function(data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }
    }

</script>
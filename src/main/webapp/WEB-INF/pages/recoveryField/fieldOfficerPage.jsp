<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        unblockui();
         $(".box").draggable();
    });

</script>

<div class="row">      
    <div class="col-md-2">
        <h4>${systemDate}</h4>     
    </div>
    <div class="col-md-8"></div>
    <div class="col-md-2">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <div class="refresh_2" value="Refresh" style="width:100%" name="" id="buttnRefresh"> </div>
        </div>
    </div>  
</div>  

<div class="row" style="margin-top: 10px; padding: 5px" id="searchOption">   
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Due Date</div>
            <div class="col-md-8">
                <input type="text"  class="txtCalendar"  value="${searchByDue}" style="width:100%" name="" id="searchByDue"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Laps Date</div>
            <div class="col-md-8">
                <input type="text"  class="txtCalendar"  value="" style="width:100%" name="" id="searchByLaps"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8">
                <input type="text"  value="${agreementNo}" style="width:100%" name="" id="searchByAgr"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Customer Name</div>
            <div class="col-md-8">
                <input type="text" value="${name}" style="width:100%" name="" id="searchByName"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">NIC No</div>
            <div class="col-md-8">
                <input type="text" value="${nic}"  style="width:100%" name="" id="searchByNic"/>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="arrearsLoans" name="" value="${arrearsLoans}">

<div class="row" id="onloadDueLoans" style="overflow : auto; height: 80%;width: 102%">
    <table class="dc_fixed_tables table-bordered newTable" width="120%" border="0" cellspacing="1" cellpadding="1" id="tblViewRecoveryShift2" >
        <thead>
            <tr>
                <th>#</th>
                <th>Agreement No</th>
                <th>Book No</th>
                <th>Customer Name</th>
                <th>Telephone</th>
                <th>Address</th>
                <th>Due Date</th>       
                <th>Shift Date</th>
                <th>Remark</th>                
                <th>Installment</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="loans" items="${loanList}">
                <c:choose>
                    <c:when test="${loans.msgSameShiftDate}">
                        <tr style="background-color:#7BB0E6">  
                            <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${loans.loanId}" onclick="selectLoanD(${loans.loanId},${loans.instlmntId},${loans.msgDateShifted})"/></td>    
                            </c:when>
                            <c:when test="${loans.msgAfterShiftDate}">
                        <tr style="background-color:#9acfea">  
                            <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${loans.loanId}" onclick="selectLoanD(${loans.loanId},${loans.instlmntId},${loans.msgDateShifted})"/></td>    
                            </c:when>
                            <c:otherwise>
                        <tr>
                            <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${loans.loanId}" onclick="selectLoanD(${loans.loanId},${loans.instlmntId},${loans.msgDateShifted})"/></td>
                            </c:otherwise>        
                        </c:choose>
                            <!--<td>${loans.loanId}</td>-->
                    <td>${loans.agreementNo}</td>
                    <td>${loans.loanBookNo}</td>
                    <td>${loans.debtorName}</td>
                    <!--<td>${loans.nic}</td>-->
                    <td>${loans.debtorTelephone}</td>
                    <td width="100px">${loans.debtorAddress}</td>
                    <td>${loans.dueDate}</td>    
                    <td><input type="text" readonly="true" value="${loans.shiftDate}" style="width:100px"/></td>                 
                    <td><input type="text" readonly="true" value="${loans.remark}" style="width:100px"/></td>
                    <td>${loans.loanAmount}</td>
                    <c:choose>
                        <c:when test="${loans.loanSpec==0}">
                            <c:choose>
                                <c:when test="${loans.loanStatus==0}">
                                    <td style="color: orange;">Processing For Loan</td>
                                </c:when>
                                <c:when test="${loans.loanStatus==1}">
                                    <td style="color: green;">On Going</td>
                                </c:when>
                                <c:otherwise>
                                    <td style="color: red;">Rejected</td>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:when test="${loans.loanSpec==1}">
                            <td style="color: red;">Laps</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==2}">
                            <td style="color: red;">Legal</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==3}">
                            <td style="color: red;">Seize</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==4}">
                            <td style="color: green;">Full Paid</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==5}">
                            <td style="color: green;">Rebate</td>
                        </c:when>
                    </c:choose> 
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div id="message_shiftDates" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<div id="searcedDueLoans"></div>        
<div id="divProcess" class="searchCustomer box" style="width: 75%;margin-left: -7%; margin-top: 5%"></div>
<script>

    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selected, evnt) {
                searchByDueDate(selected);
            }

        });
    });

    function selectLoanD(loanId, instmntId, msgDateShifted) {
        if ($('#ins' + loanId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#ins' + loanId).prop('checked', true);
            var searchByDue = $('#searchByDue').val();
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/recoveryProcess',
                data: {"loanID": loanId, "searchByDue": searchByDue, "instmntId": instmntId, "msgDateShifted": msgDateShifted},
                success: function (data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }
    }

    $('#searchByName').keyup(function (e) {
        var name = $(this).val();
        var searchByDue = $('#searchByDue').val();
        var arrearsLoans = $('#arrearsLoans').val();
        if (name.length > 0) {
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/searchByName/',
                data: {"name": name, "searchByDue": searchByDue, "arrearsLoans": arrearsLoans},
                success: function (data) {
                    $('#onloadDueLoans').html("");
                    $('#onloadDueLoans').html(data);
                },
                error: function () {
                    alert("Error Loading...3");
                }
            });
        }
    });

    $('#searchByNic').keyup(function (e) {
        var nic = $(this).val();
        var searchByDue = $('#searchByDue').val();
        var arrearsLoans = $('#arrearsLoans').val();
        if (nic.length > 0) {
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/searchByName/',
                data: {"nic": nic, "searchByDue": searchByDue, "arrearsLoans": arrearsLoans},
                success: function (data) {
                    $('#onloadDueLoans').html("");
                    $('#onloadDueLoans').html(data);
                },
                error: function () {
                    alert("Error Loading...3");
                }
            });
        }
    });

    $('#searchByAgr').keyup(function (e) {
        var agry = $(this).val();
        var searchByDue = $('#searchByDue').val();
        var arrearsLoans = $('#arrearsLoans').val();
        if (agry.length > 0) {
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/searchByName/',
                data: {"agry": agry, "searchByDue": searchByDue, "arrearsLoans": arrearsLoans},
                success: function (data) {
                    $('#onloadDueLoans').html("");
                    $('#onloadDueLoans').html(data);
                },
                error: function () {
                    alert("Error Loading...3");
                }
            });
        }
    });

    $('#buttnRefresh').click(function (e) {
        var agry = $(this).val();
        var searchByDue = $('#searchByDue').val();
        var nic = $('#searchByNic').val();
        var name = $('#searchByName').val();
        var arrearsLoans = $('#arrearsLoans').val();
        if (agry.length > 0) {
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/searchByName/',
                data: {"agry": agry, "searchByDue": searchByDue, "nic": nic, "name": name, "arrearsLoans": arrearsLoans},
                success: function (data) {
                    $('#onloadDueLoans').html("");
                    $('#onloadDueLoans').html(data);
                },
                error: function () {
                    alert("Error Loading...3");
                }
            });
        }
    });

    function searchByDueDate(selected) {
        var searchByDue = selected;
        var arrearsLoans = $('#arrearsLoans').val();
        if (searchByDue.length > 0) {
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/searchByName/',
                data: {"searchByDue": searchByDue, "arrearsLoans": arrearsLoans},
                success: function (data) {
                    $('#onloadDueLoans').html("");
                    $('#onloadDueLoans').html(data);
                },
                error: function () {
                    alert("Error Loading...3");
                }
            });
        }
    }
</script>
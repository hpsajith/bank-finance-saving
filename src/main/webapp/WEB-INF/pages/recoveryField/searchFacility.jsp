<%-- 
    Document   : searchFacility
    Created on : Jul 20, 2016, 12:09:21 PM
    Author     : ITESS
--%>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblSearchFacility").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row" style="background: #F2F7FC"> 
    <div class="row"><label style="margin-left: 20px;">Search Facility</label></div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="serachByAgNo" onkeyup="searchVehcileByAggNo()" value="${agreementNo}"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="searchByVehNo" onkeyup="searchVehcileByVehNo()" value="${vehicleNo}"/>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<div class="row" style="margin-top: 2%">
    <table id="tblSearchFacility" class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th style="text-align: center">Agreement No</th>
                <th style="text-align: center">Vehicle No</th>
                <th style="text-align: center">Type</th>
                <th style="text-align: center">Make</th>
                <th style="text-align: center">Model</th>
                <th style="text-align: center">Engine No</th>
                <th style="text-align: center">Chassis No</th>
                <th style="text-align: center">#</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="vehicleDetail" items="${vehicleDetails}">
                <tr>
                    <td style="text-align: center">${vehicleDetail.loanHeaderDetails.loanAgreementNo}</td>
                    <td style="text-align: center">${vehicleDetail.vehicleRegNo}</td>
                    <td style="text-align: center">${vehicleDetail.vType.vehicleTypeName}</td>
                    <td style="text-align: center">${vehicleDetail.vMake.vehicleMakeName}</td>
                    <td style="text-align: center">${vehicleDetail.vModel.vehicleModelName}</td>
                    <td style="text-align: center">${vehicleDetail.vehicleEngineNo}</td>
                    <td style="text-align: center">${vehicleDetail.vehiclChassisNo}</td>
                    <td style="text-align: center"><div class='edit' title="Edit" href='#' onclick="findVehicleDetail(${vehicleDetail.vehicleId})"></div></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div class="searchCustomer" style=" width: 1000px; top:5%; left:10%;" id="popup_addProperty">
    <div class="row" style="margin:15 0 0 0; height: 60%; overflow-y: auto; width:100%" id="formPropertyContent">
        <!--Form content goes here-->
    </div>
</div>
<script type="text/javascript">

    function searchVehcileByAggNo() {
        var aggNo = $("#serachByAgNo").val().trim();
        if (aggNo.length > 9) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/loadSearchFacilityPageByAggNo",
                data: {"aggNo": aggNo},
                success: function (data) {
                    $("#formContent").html("");
                    $("#formContent").html(data);
                }
            });
        }
    }

    function searchVehcileByVehNo() {
        var vehNo = $("#searchByVehNo").val().trim();
        if (vehNo.length >= 4) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/loadSearchFacilityPageByVehNo",
                data: {"vehNo": vehNo},
                success: function (data) {
                    $("#formContent").html("");
                    $("#formContent").html(data);
                }
            });
        }
    }

    function findVehicleDetail(vehId) {
        $.ajax({
            url: '/AxaBankFinance/editPropertForm/1' + '/' + vehId,
            success: function (data) {
                ui('#popup_addProperty');
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }
</script>
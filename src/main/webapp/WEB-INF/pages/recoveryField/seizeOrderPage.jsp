<%-- 
    Document   : seizeOrderPage
    Created on : Jan 25, 2016, 4:17:38 PM
    Author     : IT
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblSeizeOrder").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="row" style="margin-top: 10px; padding: 5">
    <div class="col-md-5">
        <div class="col-md-4">Agreement No</div>
        <div class="col-md-7">
            <input type="text" style="width:100%" name="agreementNo" id="txtSearchByAgreeNo"/>
        </div>
        <div class="col-md-1" style="padding: 1">
            <input type="button" class="btn btn-default searchButton"  id="btnSearchAgreeNo" onclick="createSeizeOrderByAgreeNo()" style="height: 25px">
        </div>
    </div>
    <div class="col-md-7">
        <div class="col-md-7" style="margin-left: 10%">
            <label style="color: red; font-size: 17px">${message}</label>
        </div>
    </div>
</div>
<div class="row" >
    <table id="tblSeizeOrder" class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th style="text-align: center">Order Code</th>
                <th style="text-align: center">Agreement No</th>
                <th style="text-align: center">Debtor Name</th>
                <th style="text-align: center">Article No</th>
                <th style="text-align: center">Arrears</th>
                <th style="text-align: center">Seizer</th>
                <th style="text-align: center">#</th>
                <th style="text-align: center">In-Active</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="seizeOrder" items="${seizeOrders}">
                <c:choose>
                    <c:when test="${seizeOrder.pk!=null}">
                        <c:choose>
                            <c:when test="${seizeOrder.isDelete}">
                                <tr id="${seizeOrder.loanId}" style='background-color: #B8B8B8 ;color: #000000'>
                                </c:when>
                                <c:otherwise>
                                <tr id="${seizeOrder.loanId}" style="background-color:#99CCFF">
                                </c:otherwise>   
                            </c:choose>                            
                        </c:when>
                        <c:otherwise>
                        <tr id="${seizeOrder.loanId}" style="background-color:#76F7B7">
                        </c:otherwise>        
                    </c:choose>
            <input type="hidden" name="pk" value="${seizeOrder.pk}" id="txtPk${seizeOrder.loanId}"/>
            <td style="text-align: center"><input type="hidden" name="seizeCode" value="${seizeOrder.seizeCode}" id="txtSeizeCode${seizeOrder.loanId}"/>${seizeOrder.seizeCode}</td>
            <td style="text-align: center"><input type="hidden" name="loanId" value="${seizeOrder.loanId}" id="txtLoanId${seizeOrder.loanId}"/>${seizeOrder.loanAggNo}</td>
            <td style="text-align: center"><input type="hidden" name="debtorId" value="${seizeOrder.debtorId}" id="txtDebtorId${seizeOrder.loanId}"/>${seizeOrder.debtorName}</td>
            <td style="text-align: center">
                <input type="text" name="vehicleNo" id="txtArticleNo${seizeOrder.loanId}" value="${seizeOrder.vehicleNo}"/>
                <label id="msgTxtArticleNo${seizeOrder.loanId}" class="msgTextField"></label>
            </td>
            <td style="text-align: center"><input type="hidden" name="arrearsRental" id="txtArrearsRental${seizeOrder.loanId}" value="${seizeOrder.arrearsRental}"/>${seizeOrder.arrearsRental}</td>
            <td style="text-align: center">
                <select id="inputSeizers${seizeOrder.loanId}" name="seizerId">
                    <c:choose>
                        <c:when test="${seizeOrder.seizerId!=null}">
                            <c:forEach var="seizer" items="${seizers}">
                                <c:choose>
                                    <c:when test="${seizeOrder.seizerId==seizer.seizerId}">
                                        <option value="${seizer.seizerId}" selected>${seizer.seizerName}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${seizer.seizerId}">${seizer.seizerName}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>                              
                        </c:when>
                        <c:otherwise>
                            <option value="0">--SELECT--</option>
                            <c:forEach var="seizer" items="${seizers}">
                                <option value="${seizer.seizerId}">${seizer.seizerName}</option>
                            </c:forEach>       
                        </c:otherwise>
                    </c:choose>                         
                </select>
                <label id="msgInputSeizers${seizeOrder.loanId}" class="msgTextField"></label>
            </td>
            <td style="text-align: center">
                <c:choose>
                    <c:when test="${seizeOrder.pk!=null}">
                        <c:choose>
                            <c:when test="${seizeOrder.isDelete}">
                                <input type="button" id="btnApprove" value="Update" class='btn btn-default' onclick="saveSeiezeOrder(${seizeOrder.loanId})" disabled/>
                            </c:when>
                            <c:otherwise>
                                <input type="button" id="btnApprove" value="Update" class='btn btn-default' onclick="saveSeiezeOrder(${seizeOrder.loanId})"/>
                            </c:otherwise>   
                        </c:choose>                        
                    </c:when>
                    <c:otherwise>
                        <input type="button" id="btnApprove" value="Create" class='btn btn-default' onclick="saveSeiezeOrder(${seizeOrder.loanId})"/>
                    </c:otherwise>
                </c:choose>
            </td>
            <td style="text-align: center">
                <c:choose>
                    <c:when test="${seizeOrder.pk!=null}">
                        <c:choose>
                            <c:when test="${seizeOrder.isDelete}">
                                <div class="btnActive" href='#' title="Active" onclick="activeSeizeOrder(${seizeOrder.pk},${seizeOrder.loanId})"></div>
                            </c:when>
                            <c:otherwise>
                                <div class="delete" href='#' title="In-Active" onclick="inActiveSeizeOrder(${seizeOrder.pk},${seizeOrder.loanId})"></div>
                            </c:otherwise>   
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <div class="x" href='#'></div>
                    </c:otherwise>
                </c:choose>
            </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<div class="row" style="margin-top: 20px;">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-3">
                <label>Created</label>
                <div style="width:50px; height: 10px; background: #99CCFF"></label> </div>
            </div>
            <div class="col-md-3">
                <label>New</label>
                <div style="width:50px; height: 10px; background: #76F7B7"></label> </div>
            </div>
            <div class="col-md-3">
                <label>In-Active</label>
                <div style="width:50px; height: 10px; background: #B8B8B8"></label> </div>
            </div>            
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
<!--<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}../resources/img/processing.gif" alt="Loading..." />
</div>-->
<input type="hidden" value="${isDefault}" id="txtDefault"/>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    var curr = new Date();
    var sysDate = curr.getFullYear() + "-" + curr.getMonth() + "-" + curr.getDate();

    function seizeOrderCon(loanId) {
        this.pk = $("#txtPk" + loanId).val();
        this.seizeCode = $("#txtSeizeCode" + loanId).val();
        this.orderDate = sysDate;
        this.loanId = String(loanId);
        this.debtorId = $("#txtDebtorId" + loanId).val();
        this.vehicleNo = $("#txtArticleNo" + loanId).val();
        this.arrearsRental = $("#txtArrearsRental" + loanId).val();
        this.seizerId = $("#inputSeizers" + loanId).find("option:selected").val();
        if ($("#txtDefault").val() === "1") {
            this.isDefault = "true";
        } else {
            this.isDefault = "false";
        }
        this.isKeyIssue = "false";
        this.isRenew = "false";
    }

    function saveSeiezeOrder(loanId) {
        if (feildValidate(loanId)) {
            document.getElementById("btnApprove").disabled = true;
            var seizeOrder = new seizeOrderCon(loanId);
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/saveOrUpdateSeizeOrder?${_csrf.parameterName}=${_csrf.token}",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(seizeOrder),
                success: function(data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    document.getElementById("btnApprove").disabled = false;
                    setTimeout(function() {
                        loadSeizeOrderPage();
                    }, 1500);
                }
            });
        }
    }

    function feildValidate(loanId) {
        var vehicleNo = $("#txtArticleNo" + loanId).val();
        var seizer = $("#inputSeizers" + loanId).find("option:selected").val();
        if (vehicleNo === null || vehicleNo === "") {
            $("#msgTxtArticleNo" + loanId).text("Enter Article No");
            $("#txtArticleNo" + loanId).addClass("txtError");
            $("#txtArticleNo" + loanId).focus();
            return false;
        } else {
            $("#msgTxtArticleNo" + loanId).text("");
            $("#txtArticleNo" + loanId).removeClass("txtError");
        }

        if (seizer === "0") {
            $("#msgInputSeizers" + loanId).text("Select a Seizer");
            $("#inputSeizers" + loanId).addClass("txtError");
            $("#inputSeizers" + loanId).focus();
            return false;
        } else {
            $("#msgInputSeizers" + loanId).text("");
            $("#inputSeizers" + loanId).removeClass("txtError");
        }

        return true;
    }

    function activeSeizeOrder(pk, loanId) {
        if (pk !== null && loanId !== null) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/activeSeizeOrder",
                type: 'GET',
                data: {"orderId": pk, "loanId": loanId},
                success: function(data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadSeizeOrderPage();
                    }, 1500);
                },
                error: function() {
                    console.log("Error in active");
                }
            });
        }
    }

    function inActiveSeizeOrder(pk, loanId) {
        if (pk !== null && loanId !== null) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/inActiveSeizeOrder",
                type: 'GET',
                data: {"orderId": pk, "loanId": loanId},
                success: function(data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadSeizeOrderPage();
                    }, 1500);
                },
                error: function() {
                    console.log("Error in In-active");
                }
            });
        }
    }

    function createSeizeOrderByAgreeNo() {
        var agree = $('#txtSearchByAgreeNo').val();
        if (agree != null) {
//            ui("#proccessingPage");
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/createSeizeOrderByAgreementNo',
                data: {"agreeNo": agree},
                success: function(data) {
                    $('#tblSeizeOrder tbody').remove();
                    $('#formContent').html(data);
                },
                error: function() {
                    alert("Loan Not Found");
                }
            });
//            unblockui();
        } else {
            alert("Please Enter Valid Agreement No.");
        }
    }

    $(function() {
        $('input[name=agreementNo]').keydown(function(e) {
            var code = e.keyCode || e.which;
            if (code === 13) {
                createSeizeOrderByAgreeNo();
            }
        });
    });


</script>
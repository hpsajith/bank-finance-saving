<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Recovery Manager Approval</strong>
    </div>
    <form id="form_recoveryManagerReport" name="n_recoveryManagerReport">
        <div class="row" style="margin-top: 10px;">
            <!--//field officer comment-->
            <c:choose>
                <c:when test="${recovery.visitStatus==1}">
                    <div class="col-md-12" style="margin-top: 20px; text-align: left; margin-left: 20px;">
                        <div class="row"><label style="color:red"> Recovery Report is Pending</label></div>
                        <div class="row"><p>Assign to ${officerName} on ${recovery.systemDate}</p></div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row" style="text-align: left">
                        <div class="col-md-8" style="text-align: left">
                            <label style="color: #4cae4c">Recovery report is submitted</label><br/>
                            <p>By ${officerName}</p>                        
                        </div>
                        <div class="col-md-4">                            
                            <input type="button" class="btn btn-default col-md-12" value="View" onclick="viewRecoveryReport2();"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Assign Date:${recovery.systemDate}</div>
                        <div class="col-md-6">Submitted Date:${recovery.submitDate}</div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-md-12">
            <label id="lblapproveTitle"></label>
            <input type="hidden" name="loanId" id="txtLoanId_manager" value="${recovery.loanId}"/>
            <input type="hidden" name="recId" id="txtRecId_manager" value="${recovery.id}"/>
            <input type="hidden" name="dueDate" id="txtdueDate_manager" value="${recovery.dueDate}"/>
            <input type="hidden" name="vType" id="txtvType_manager" value="${recovery.visitType}"/>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Comment</div>
                <div class="col-md-10"><textarea rows="3" name="approvedComment" style="width: 100%">${approve.approvedComment}</textarea></div>
            </div>

            <div class="row" style="margin-top: 15px">  

                <div class="col-md-4">
                    <c:if test="${recovery.visitStatus==1}">
                        <input type="button" value="Change Officer" class="btn btn-default col-md-12" onclick="changeFieldOfficer2()"/>
                    </c:if>
                </div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                    <c:choose>
                        <c:when test="${recovery.visitStatus==1}">
                        <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="saveManagerComment(${recovery.id})" disabled></div>
                        </c:when>
                        <c:otherwise>
                        <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="saveManagerComment(${recovery.id})"></div>
                        </c:otherwise>
                    </c:choose>
            </div>
        </div> 
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
    <form method="POST" action="/AxaBankFinance/ReportController/viewRecoverReport" target="blank" id="viewVisitReport_Id">
        <input type="hidden" name="visit_Id" value="${recovery.id}"/> 
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>
<div id="message_recoveryReport" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<!--//view report-->
<div class="searchCustomer" id="reportBox" style="width: 60%; margin-left: 3%">
    <div class="close_div" onclick="unloadReportView()"><label>X</label></div>
    <div style="clear:both"></div>
    <div class="row" id="report_title" style="margin-left: 15px; margin-right: 15px;">Recovery Report</div>
    <div class="row" id="report_content" style="margin-top: 15px; padding:5px; height: 400px; overflow-y: auto;">
    </div>
</div>


<script>
    function changeFieldOfficer2() {
        var loanId = $("#txtLoanId_manager").val();
        var dueDate = $("#txtdueDate_manager").val();
        var vType = $("#txtvType_manager").val();
        var comment = "";
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/assignRecoveryOfficer/',
            data: {loanId: loanId, dueDate: dueDate, visitType: vType, comment: comment},
            success: function(data) {
                $('#recoveryOff').html("");
                $('#recoveryOff').html(data);
                ui('#recoveryOff');
            },
            error: function() {
                alert("Error Loading letters");
            }
        });
    }

    function saveManagerComment(id) {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/saveMangerComment',
            type: 'POST',
            data: $("#form_recoveryManagerReport").serialize(),
            success: function(data) {
                $("#message_posting").html("");
                $("#message_posting").html(data);
                ui("#message_posting");
                loadRecoveryArrearsLoans();
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    function viewRecoveryReport2() {
        $("#viewVisitReport_Id").submit();
    }
</script>
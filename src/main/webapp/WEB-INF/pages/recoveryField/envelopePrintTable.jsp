<%-- 
    Document   : envelopePrintTable
    Created on : Sep 8, 2016, 3:45:57 PM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tblEnvPrint").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
    });
</script>
<table id="tblEnvPrint" class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0"
       cellpadding="0">
    <thead>
    <tr>
        <th style="text-align: center">Account No</th>
        <th style="text-align: center">Name</th>
        <th style="text-align: center">Address</th>
        <th style="text-align: center">Registered</th>
        <th style="text-align: center">Size</th>
        <th style="text-align: center">#</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="debtor" items="${debtors}">
        <tr>
            <td style="text-align: center">${debtor.debtorAccountNo}</td>
            <td style="text-align: center">${debtor.debtorName}</td>
            <td style="text-align: center">${debtor.debtorPersonalAddress}</td>
            <td style="text-align: center">
                <input type="checkbox" class="alert-success" onclick="check()" id="rPost"><br>
            </td>
            <td style="text-align: center">
                <select id="inpSize_${debtor.debtorId}" style="width: 100%">
                    <option value="0">-- SELECT --</option>
                    <option value="1">Size (L)</option>
                    <option value="2">Size (M)</option>
                    <option value="3">Size (S)</option>
                </select>
            </td>
            <td style="text-align: center">
                <a href="#" onclick="printEnvelope(${debtor.debtorId})">Print Envelope</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<form method="GET" action="/AxaBankFinance/ReportController/printEnvelope" target="blank" id="formEnvelope">
    <input type="hidden" name="debtorId" id="txtDebtorId"/>
    <input type="hidden" name="size" id="txtSize"/>
    <input type="hidden" name="postType" id="txtRpost"/>
</form>
<script type="text/javascript">
    function printEnvelope(debtorId) {
        if (debtorId > 0) {
            var size = $("#inpSize_" + debtorId).val();
            if (size !== "0") {
                $("#txtDebtorId").val(debtorId);
                $("#txtSize").val(size);
                $("#formEnvelope").submit();
            }
        }
    }

    function check() {
        var checkedValue = $('.alert-success:checked').val();
        if (checkedValue === "on") {
            $("#txtRpost").val(1);
        } else {
            $("#txtRpost").val(0);
        }
    }
</script>
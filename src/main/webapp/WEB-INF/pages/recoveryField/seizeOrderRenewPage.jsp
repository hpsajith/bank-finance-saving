<%-- 
    Document   : seizeOrderRenewPage
    Created on : Feb 11, 2016, 4:31:48 PM
    Author     : IT
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblSeizeOrder").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="row" >
    <table id="tblSeizeOrder" class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th style="text-align: center">Agreement No</th>
                <th style="text-align: center">Debtor Name</th>
                <th style="text-align: center">Article No</th>
                <th style="text-align: center">Arrears</th>
                <th style="text-align: center">Seizer</th>
                <th style="text-align: center">#</th>
                <th style="text-align: center">#</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="seizeOrder" items="${seizeOrderRenewalList}">
                <tr>
                    <td style="text-align: center">
                        <input type="hidden" name="loanId" id="txtLoanId${seizeOrder.loanId}" value="${seizeOrder.loanId}"/>
                        ${seizeOrder.loanAggNo}
                    </td>
                    <td style="text-align: center">
                        <input type="hidden" name="debtorId" id="txtDebtorId${seizeOrder.loanId}" value="${seizeOrder.debtorId}"/>
                        ${seizeOrder.debtorName}
                    </td>
                    <td style="text-align: center">
                        <input type="hidden" name="vehicleNo" id="txtArticleNo${seizeOrder.loanId}" value="${seizeOrder.vehicleNo}"/>
                        ${seizeOrder.vehicleNo}
                    </td>
                    <td style="text-align: center">
                        <input type="hidden" name="arrearsRental" id="txtArrearsRental${seizeOrder.loanId}" value="${seizeOrder.arrearsRental}"/>
                        ${seizeOrder.arrearsRental}
                    </td>
                    <td style="text-align: center">
                        <input type="hidden" name="seizerId" id="txtSeizerId${seizeOrder.loanId}" value="${seizeOrder.seizerId}"/>
                        ${seizeOrder.serizerName}
                    </td>
                    <td style="text-align: center">
                        <input type="button" value="Re-New" class="btn btn-default" onclick="renewSeizeOrder(${seizeOrder.loanId})"/>
                    </td>
                    <td style="text-align: center">
                        <input type="button" value="History" class="btn btn-default" onclick="viewSeizeOrderHistory(${seizeOrder.loanId})"/>
                    </td>
            <input type="hidden" name="orderDate" id="txtOrderDate${seizeOrder.loanId}" value="${seizeOrder.orderDate}"/>
            <input type="hidden" name="approval_1" id="txtApproval1_${seizeOrder.loanId}" value="${seizeOrder.approval_1}"/>
            <input type="hidden" name="approval_2" id="txtApproval2_${seizeOrder.loanId}" value="${seizeOrder.approval_2}"/>
            <input type="hidden" name="isDelete" id="txtIsDelete${seizeOrder.loanId}" value="${seizeOrder.isDelete}"/>
            <input type="hidden" name="isKeyIssue" id="txtIsKeyIssue${seizeOrder.loanId}" value="${seizeOrder.isKeyIssue}"/>
            <input type="hidden" name="keyIssueDate" id="txtKeyIssueDate${seizeOrder.loanId}" value="${seizeOrder.keyIssueDate}"/>
            <input type="hidden" name="isRenew" id="txtIsRenew${seizeOrder.loanId}" value="${seizeOrder.isRenew}"/>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<div class="searchCustomer" style="width:40%; margin: 5% 10%" id="popup_seizeOrderRenew_Form"></div>  
<div class="searchCustomer" style="width:80%;margin: 5% -12%" id="popup_seizeOrderHistory_Page"></div>  
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    function renewSeizeOrder(loanId) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadSeizeOrderRenewForm",
            type: 'GET',
            success: function(data) {
                $('#popup_seizeOrderRenew_Form').html("");
                $('#popup_seizeOrderRenew_Form').html(data);
                $("#txtRLoanId").val($("#txtLoanId" + loanId).val());
                $("#txtRDebtorId").val($("#txtDebtorId" + loanId).val());
                $("#txtRvehicleNo").val($("#txtArticleNo" + loanId).val());
                $("#txtRArrearsRental").val($("#txtArrearsRental" + loanId).val());
                $("#txtRSeizerId").val($("#txtSeizerId" + loanId).val());
                $("#txtROrderDate").val($("#txtOrderDate" + loanId).val());
                $("#txtRApproval1").val($("#txtApproval1_" + loanId).val());
                $("#txtRApproval2").val($("#txtApproval2_" + loanId).val());
                $("#txtRIsDelete").val($("#txtIsDelete" + loanId).val());
                $("#txtRIsKeyIssue").val($("#txtIsKeyIssue" + loanId).val());
                $("#txtRKeyIssueDate").val($("#txtKeyIssueDate" + loanId).val());
                $("#txtRIsRenew").val($("#txtIsRenew" + loanId).val());
                ui('#popup_seizeOrderRenew_Form');
            }
        });
    }

    function viewSeizeOrderHistory(loanId) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadSeizeOrderHistoryPage",
            type: 'GET',
            data: {"loanId": loanId},
            success: function(data) {
               $("#popup_seizeOrderHistory_Page").html("");
               $("#popup_seizeOrderHistory_Page").html(data);
               ui('#popup_seizeOrderHistory_Page');
            }
        });
    }

</script>
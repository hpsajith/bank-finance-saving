<%-- 
    Document   : envelopePrint
    Created on : Sep 8, 2016, 3:45:18 PM
    Author     : admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        unblockui();
    });
</script>
<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption"> 
    <div class="row"><label style="margin-left: 20px;">Search Debtor Address</label></div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Debtor Name</div>
            <div class="col-md-8">
                <input type="text"  style="width:100%" id="txtDebName_" onkeyup="searchByDebName()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Account No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtDebAccNo_" onkeyup="searchByDebAccNo()"/>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<div class="row" style="margin-top: 10px" id="divEnvPrint">
    <!--table content goes here-->
</div>
<script type="text/javascript">

    function searchByDebName() {
        var debName = $("#txtDebName_").val().trim();
        if (debName.length > 3) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/loadEnvelopePrintTableByName",
                data: {"debName": debName},
                success: function (data) {
                    $("#divEnvPrint").html("");
                    $("#divEnvPrint").html(data);
                }
            });
        }
    }

    function searchByDebAccNo() {
        var debAccNo = $("#txtDebAccNo_").val().trim();
        if (debAccNo.length >= 5) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/loadEnvelopePrintTableByAccNo",
                data: {"debAccNo": debAccNo},
                success: function (data) {
                    $("#divEnvPrint").html("");
                    $("#divEnvPrint").html(data);
                }
            });
        }
    }

</script>
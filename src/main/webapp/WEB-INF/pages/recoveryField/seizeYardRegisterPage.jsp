<%-- 
    Document   : seizeYardRegisterPage
    Created on : Feb 3, 2016, 10:59:49 AM
    Author     : IT
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblYardRegister").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="row" >
    <table id="tblYardRegister" class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th style="text-align: center">#</th>
                <th style="text-align: center">Seize Code</th>
                <th style="text-align: center">Yarding Date</th>
                <th style="text-align: center">Agreement No</th>
                <th style="text-align: center">Debtor Name</th>
                <th style="text-align: center">Seizer/Officer</th>
                <th style="text-align: center">Article No</th>
                <th style="text-align: center">Add To</th>
                <th style="text-align: center">Delete</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="yardRegister" items="${yardRegisters}">
                <c:choose>
                    <c:when test="${yardRegister.yardId!=null}">     
                        <tr id="${yardRegister.seizeOrderId}" style='background-color: #99CCFF ;color: #000000'>
                        </c:when>
                        <c:otherwise>
                        <tr id="${yardRegister.seizeOrderId}" style="background-color:#B8B8B8">
                        </c:otherwise>        
                    </c:choose>                
                    <td style="text-align: center">
                        <input type="checkbox" id="chx${yardRegister.seizeOrderId}" class="keyChx" onchange="changeSelection(${not empty yardRegister.yardId?yardRegister.yardId:0},${yardRegister.seizeOrderId})"/>
                    </td>
                    <td style="text-align: center">${yardRegister.seizeCode}</td>
                    <td style="text-align: center">${yardRegister.yardDate}</td>
                    <td style="text-align: center">${yardRegister.loanAggNo}</td>
                    <td style="text-align: center">${yardRegister.debtorName}</td>
                    <td style="text-align: center">${yardRegister.yardPerson}</td>
                    <td style="text-align: center">${yardRegister.vehicleNo}</td>
                    <td style="text-align: center">
                        <a href="#" onclick="addToYard(${yardRegister.seizeOrderId},${yardRegister.loanId},${yardRegister.yardOfficerId})">Add to Yard</a>
                    </td>
                    <c:choose>
                        <c:when test="${yardRegister.yardId!=null}">
                            <td style="text-align: center"><div class="delete" href='#' onclick="loadYardRegForm(${yardRegister.yardId})"></div></td>
                            </c:when>
                            <c:otherwise>
                            <td style="text-align: center"><div class="delete" href='#' onclick="reverseYardRegister(${yardRegister.seizeOrderId})"></div></td>
                            </c:otherwise>
                        </c:choose>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div class="row" style="margin-top: 20px;">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-3">
                <label>Yard</label>
                <div style="width:50px; height: 10px; background: #99CCFF"></div>
            </div>
            <div class="col-md-3">
                <label>Pending</label>
                <div style="width:50px; height: 10px; background: #B8B8B8"></div>
            </div>          
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <input type="button" id="btnUploadImage" class="btn btn-default col-md-12" value="Upload Images" disabled onclick="uploadImages()"/>
            </div>
            <div class="col-md-6">
                <input type="button" id="btnUploadReport" class="btn btn-default col-md-12" value="Upload Report" disabled onclick="uploadReport()"/>
            </div>
        </div>
    </div>
</div>
<div class="searchCustomer" style="width:40%; margin: 5% 10%" id="popup_yardRegister_Form"></div>  
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<div id="divCheckList" class="searchCustomer" style="width: 85%;border: 0px solid #5e5e5e;margin-left:-15%;margin-top: 50px"></div>
<div id="divDelYardRegForm" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    var curr = new Date();
    var sysDate = curr.getFullYear() + "-" + curr.getMonth() + "-" + curr.getDate();

    var seizeOrderIdA;
    var yardRegIdA;
    var checked = false;

    function addToYard(seizeOrderId, loanId, yardOfficerId) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadSeizeYardRegisterForm",
            type: 'GET',
            data: {"seizeOrderId": seizeOrderId},
            success: function(data) {
                $('#popup_yardRegister_Form').html("");
                $('#popup_yardRegister_Form').html(data);
                $("#txtSeizeOrderId").val(seizeOrderId);
                $("#txtLoanId").val(loanId);
                $("#txtYardOfficerId").val(yardOfficerId);
                $("#txtYardDate").val(sysDate);
                ui('#popup_yardRegister_Form');
            }
        });
    }

    function changeSelection(yId, sId) {
        $('#chx' + sId).is(':checked');
        $('.keyChx').prop('checked', false);
        $('#chx' + sId).prop('checked', true);
        if (yId !== 0) {
            document.getElementById("btnUploadImage").disabled = false;
            document.getElementById("btnUploadReport").disabled = false;
            seizeOrderIdA = sId;
            yardRegIdA = yId;
        } else {
            document.getElementById("btnUploadImage").disabled = true;
            document.getElementById("btnUploadReport").disabled = true;
        }
        checked = true;
    }

    function uploadImages() {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadSeizeArticleImagesPage",
            type: 'GET',
            data: {"seizeOrderId": seizeOrderIdA},
            success: function(data) {
                $("#divCheckList").html("");
                $("#divCheckList").html(data);
                ui('#divCheckList');
            }
        });
    }

    function uploadReport() {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadSeizeInspectionReportPage",
            type: 'GET',
            data: {"seizeOrderId": seizeOrderIdA},
            success: function(data) {
                $("#divCheckList").html("");
                $("#divCheckList").html(data);
                ui('#divCheckList');
            }
        });
    }

    function loadYardRegForm(yardRegId) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadDeleteYardRegPswdForm",
            type: 'GET',
            success: function(data) {
                $("#divDelYardRegForm").html("");
                $("#divDelYardRegForm").html(data);
                $("#txtYardRegId").val(yardRegId);
                ui('#divDelYardRegForm');
            }
        });
    }

    function reverseYardRegister(seizeOrderId) {
        if (seizeOrderId !== null && seizeOrderId !== "") {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/reverseYardRegister",
                type: 'GET',
                data: {"seizeOrderId": seizeOrderId},
                success: function(data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadSeizeYardRegisterPage();
                    }, 1500);
                }
            });
        }
    }

</script>
<%-- 
    Document   : recoveryProcess
    Created on : Jul 8, 2015, 1:05:51 PM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>

    <div class="col-md-12 col-lg-12 col-sm-12" style="display:block;">
        <div class="row" style="background: #9CF"></div>
        <div class="row" style="text-align: left;"><label>Loan Details</label></div>
        <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2">Loan Amount</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%"  value="${loan.loanAmount}"/></div>            

            <div class="col-md-2 col-lg-2 col-sm-2">Down Payment</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" value="${loan.loanDownPayment}"/></div>

            <div class="col-md-2 col-lg-2 col-sm-2">Interest</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%"  value="${loan.loanInterest}"/></div>

        </div>
        <div class="row">    
            <div class="col-md-2 col-lg-2 col-sm-2">Loan Rate</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" value="${loan.loanInterestRate}"/></div>        

            <div class="col-md-2 col-lg-2 col-sm-2">Investment</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%"  value="${loan.loanInvestment}"/></div>

            <div class="col-md-2 col-lg-2 col-sm-2">Loan Process Date</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%"  value="${loan.loanDate}"/></div>
        </div>
        <div class="row"> 

            <div class="col-md-2 col-lg-2 col-sm-2">Book No</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%"  value="${loan.loanBookNo}"/></div>

            <div class="col-md-2 col-lg-2 col-sm-2">Installment</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" value="${loan.loanInstallment}"/></div>

            <div class="col-md-2 col-lg-2 col-sm-2">Loan End Date</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly  style="width: 100%" value="${loanEndDate}"/></div>

        </div>
        <div class="row"> 
            <div class="col-md-2 col-lg-2 col-sm-2">Agreement No</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" value="${loan.loanAgreementNo}"/></div>

            <div class="col-md-2 col-lg-2 col-sm-2">Period</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%"  value="${loan.loanPeriod}"/></div>

            <div class="col-md-2 col-lg-2 col-sm-2">Period Type</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" value="${LoanPeriodType}"/></div>
        </div>
    </div>
    <div class="row" style="border-top: 1px solid #222; padding-top: 5px; margin-top: 5px; "></div>


    <form name="RecoveryShiftForm" id="RecoveryShiftForm" > 
        <input type="hidden" value="${loan.loanId}" name="loanId" id="loanId" readonly="true">    
        <input type="hidden" value="${userId}" name="userId" id="userId" readonly="true">  
        <input type="hidden" value="${instmntId}" name="instmntId" id="instmntId" readonly="true"> 

        <div class="row"></div>
        <div class="row" style="margin-bottom:2px"><legend>Recovery</legend></div>
        <div class="row"></div>
        <div class="row">
            <div class="col-md-7">
                <c:choose>
                    <c:when test="${msgArrears}">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-3">Total Arrears</div>
                            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly="true" value="${totalArreas}"/></div>
                        </div>
                    </c:when>
                </c:choose>

                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3">Total Balance</div>
                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly="true" value="${balance}"/></div>
                </div>
                <div class="row divError" style="padding:10px; "></div>  
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3">Current due date</div>
                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" value="${searchByDue}" name="dueDate" id="dueDate" readonly="true"></div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3">Shift due date To</div>
                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" value="${shiftedDate}" class="txtCalendarRecv" name="shiftDate" id="shiftDate" ></div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3">Remark</div>
                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" value="${remark}" name="remark" id="remark" ></div> 
                    <div class="row" style="margin-bottom:10px"></div>
                    <div class="row" >
                        <div  class="col-md-3 "></div>
                        <!--<div  class="col-md-3 "><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>-->
                        <c:choose>
                            <c:when test="${msgDateShifted}">
                                <div  class="col-md-3 "><input type="button"  value="Shift Again" onclick="AddShiftDate()" class="btn btn-edit btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                                </c:when>
                                <c:otherwise>
                                <div  class="col-md-3 "><input type="button"  value="Shift" onclick="AddShiftDate()" class="btn btn-edit btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                                </c:otherwise>
                            </c:choose>   
                        <div  class="col-md-3 "></div>
                    </div>
                </div>
                </form> 
                <div class="row" style="padding:15px" ></div>
            </div>
            <div class="col-md-5">
                <div class="row" style="text-align: left;">
                    <label>Previous Shift Dates</label>
                </div>
                <div class="row">                          
                    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="">
                        <thead>
                            <tr>
                                <th>Due Date</th>
                                <th>Shift Date</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="shiftDate" items="${recoveryShiftList}">
                                <tr>   
                                    <td>${shiftDate.dueDate}</td>
                                    <td>${shiftDate.shiftDate}</td>
                                    <td>${shiftDate.remark}</td>
                                </tr>                            
                            </c:forEach>
                        </tbody>
                    </table> 
                </div>   
            </div>
        </div>  
        <div class="row"></div>        
        <div class="row"></div>
        <div class="col-md-7">
            <div class="row" style="margin-bottom:2px"><legend>Add Other Charges</legend></div>
            <form id="addRecoveryCharge" name="addRecoveryCharge_">
                <div class="row">
                    <div class="col-md-3">Due Date</div>
                    <div class="col-md-5"><input type="text" value="${searchByDue}" name="chargeDueDate" readonly="true">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">Charge</div>
                    <div class="col-md-5">
                        <select id="otherChageRecovery" name="recoveryChargeType">
                            <c:forEach var="extra" items="${extraCharge}">
                                <option value="${extra.id}">${extra.description}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">Amount</div>
                    <div class="col-md-5"><input type="text" name="recoveryChargeAmount" /></div>
                </div>
                <input type="hidden" name="recoveryChargeLoanId" value="${loan.loanId}"/>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <div class="row">
                <div  class="col-md-3 "></div>
                <div class="col-md-3"><input type="button" class="btn btn-edit btn-default" value="Add Charge" onclick="addRecoveryCharge()"/></div>
                <div  class="col-md-3 "></div>
            </div>
        </div>
        <div class="row"></div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-3"><input type="button" value="Cancel" class="btn btn-edit btn-default col-md-12" onclick="unblockui()"/></div>
            <div class="col-md-3"><input type="button" value="View History" class="btn btn-edit btn-default col-md-12" onclick="viewRecoveryHistory()"/></div>
        </div>
        <form id="recoveryHisPDF" method="GET" action="/AxaBankFinance/RecoveryController/generateHistoryPDF" target="blank">
            <input type="hidden" value="${loan.loanId}" name="recLoanID"/>
        </form>
</div>
<div id="message_dialog_charge" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script>

    $(function() {
        var startDate = $('#dueDate').val();
        $(".txtCalendarRecv").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            startDate: startDate
        });
        var loan_id = $("#loanId").val();

//        loadOtherCharge2(loan_id);

    });

//    function loadOtherCharge2(loan_id) {
//        $.getJSON('/AxaBankFinance/SettlementController/loadOtherChargeAccounts/' + loan_id, function(data) {
//            for (var i = 0; i < data.length; i++) {
//                var type = data[i].id;
//                alert(type);
//                $("#otherChageRecovery").append("<option value='" + type + "'>" + data[i].description + "</option>");
//            }
//        });
//    }


    function AddShiftDate() {

        var validatef = validateForm();
        if (validatef) {
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/saveRecoveryShiftForm',
                type: 'GET',
                data: $("#RecoveryShiftForm").serialize(),
                success: function(data) {
                    $("#message_shiftDates").html(data);
                    ui("#message_shiftDates");
                    loadRecoverOfficerPage();
                },
                error: function() {
                    warning("Error Loading...", "message_shiftDates");
                }
            });
        } else {
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html(errorMessage);
            errorMessage = "";
        }
    }

    var errorMessage = "";
    function validateForm() {
        var validate = true;
        var shiftDate = $('input[name=shiftDate]').val();

        var remark = $('input[name=remark]').val();

        if (shiftDate === null || shiftDate === "") {
            validate = false;
            errorMessage = errorMessage + "Select Shift Date";
            $("#shiftDate").addClass("txtError");
        } else {
            $("#shiftDate").removeClass("txtError");
        }

        if (remark === null || remark === "") {
            validate = false;
            errorMessage = errorMessage + "Enter Remark";
            $("#remark").addClass("txtError");
        } else {
            $("#remark").removeClass("txtError");
        }

        return validate;
    }


//    $('#letter1').click(function(e) {
//        $.ajax({
//            url: '/AxaBankFinance/RecoveryController/savePrintLetterInfo',
//            type: 'GET',
//            data: $("#RecoveryShiftForm").serialize(),
//            success: function(data) {
//                $("#printLetterForm").submit();
//            },
//            error: function() {
//                alert("Error Loading...");
//            }
//        });
//    });

    function addRecoveryCharge() {
//        var chargeId = $("#otherChageRecovery").find('option:selected').val();
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/saveRecoveryCharge',
            type: 'POST',
            data: $("#addRecoveryCharge").serialize(),
            success: function(data) {
                $("#message_dialog_charge").html("");
                $("#message_dialog_charge").html(data);
                ui("#message_dialog_charge");
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    function viewRecoveryHistory() {
        $("#recoveryHisPDF").submit();
    }
</script>
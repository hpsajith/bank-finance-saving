<%-- 
    Document   : seizeOrderIssuePage
    Created on : Feb 1, 2016, 4:34:21 PM
    Author     : IT
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblSeizeOrderIssue").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="row" >
    <table id="tblSeizeOrderIssue" class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th style="text-align: center">Seize Code</th>
                <th style="text-align: center">Agreement No</th>
                <th style="text-align: center">Debtor Name</th>
                <th style="text-align: center">Article No</th>
                <th style="text-align: center">Assigned Seizer</th>
                <th style="text-align: center">RM Approval</th>
                <th style="text-align: center">CED Approval</th>
                <th style="text-align: center">Key Issue</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="approvals" items="${seizeApproveList}">              
                <tr>
                    <td style="text-align: center">${approvals.seizeCode}</td>
                    <td style="text-align: center">${approvals.loanAggNo}</td>
                    <td style="text-align: center">${approvals.debtorName}</td>
                    <td style="text-align: center">${approvals.vehicleNo}</td>
                    <td style="text-align: center">${approvals.serizerName}</td>

                    <c:choose>
                        <c:when test="${approvals.approval_1 == 1 && approvals.approval_2 == 0}">
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 0)" class="underline-blue"style="font-style: italic">approved</a></td>
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 1)" class="underline"style="font-style: italic">not approved</a></td>
                        </c:when>
                        <c:when test="${approvals.approval_2 == 1 && approvals.approval_1 == 0}">
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 0)" class="underline"style="font-style: italic">not approved</a></td>
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 1)" class="underline-blue"style="font-style: italic"> approved</a></td>
                        </c:when>

                        <c:when test="${approvals.approval_1 == 1 && approvals.approval_2 == 1}">
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 0)" class="underline-blue"style="font-style: italic">approved</a></td>
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 1)" class="underline-blue"style="font-style: italic">approved</a></td>
                        </c:when>
                        <c:otherwise>
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 0)" class="underline" style="font">not approved</a></td>
                            <td style="text-align: center"><a  href='#' onclick="approveSeizeOrder(${approvals.pk}, 1)" class="underline" style="font">not approved</a></td>
                        </c:otherwise>
                    </c:choose>

                    <c:choose>
                        <c:when test="${approvals.approval_2 == 1}">
                            <td>
                                <input type="button" id="btnKeyIssue" class="btn btn-default col-md-12" value="Key Issue" onclick="keyIssue(${approvals.pk})"/>
                            </td> 
                        </c:when>
                        <c:otherwise>
                            <td>
                                <input type="button" id="btnKeyIssue" class="btn btn-default col-md-12" value="Key Issue" disabled/>
                            </td>                             
                        </c:otherwise>    
                    </c:choose>        
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<div class="searchCustomer" style="width:40%; margin: 5% 10%" id="popup_seizeOrderIssue_Form"></div>  
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    var OrderId;
    var checked = false;
    function approveSeizeOrder(seizeOrderId, type) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadSeizeOrderApprovalForm",
            type: 'GET',
            data: {"seizeOrderId": seizeOrderId, "type": type},
            success: function(data) {
                $('#popup_seizeOrderIssue_Form').html("");
                $('#popup_seizeOrderIssue_Form').html(data);
                $("#txtSeizeOrderId").val(seizeOrderId);
                ui('#popup_seizeOrderIssue_Form');
            }
        });
    }

    function changeSelection(seizeOrderId) {
        $('#keyRecievrChx' + seizeOrderId).is(':checked');
        $('.keyRecievrChx').prop('checked', false);
        $('#keyRecievrChx' + seizeOrderId).prop('checked', true);
        OrderId = seizeOrderId;
        checked = true;
    }

    function keyIssue(orderId) {
        if (orderId !== null) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/keyIssueConfirmation/" + orderId,
                type: 'GET',
                success: function(data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');

                }
            });
        }
    }

</script>
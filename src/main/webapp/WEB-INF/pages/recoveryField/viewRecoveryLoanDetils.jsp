<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="container-fluid" id="searchCustomerContent" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="row" style="padding: 5px; margin-bottom: 10px;background: #999; text-align: left">
        <label>Customer/Guarantor Details</label>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!--customer details-->
            <div class="row">
                <div class="col-md-2"><label>Customer</label></div>
                <div class="col-md-10"><input type="text" value="${cusName}" readonly="" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-2"> <label>NIC</label></div>
                <div class="col-md-4"><input type="text" value="${cusNic}" readonly="" style="width: 100%"/></div>
                <div class="col-md-2"><label>Telephone</label></div>
                <div class="col-md-4"><input type="text" value="${cusTelephone}" readonly="" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-2"><label>Address</label></div>
                <div class="col-md-10"><textarea style="width: 100%" readonly>${cusAddress}</textarea></div>
            </div>
        </div>
        <div class="col-md-6">
            <!--guarantor details-->
            <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="text-align: center">No</th>
                        <th style="text-align: center">Name</th>
                        <th style="text-align: center">Address</th>
                        <th style="text-align: center">Telephone</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="gurantor" items="${guarantors}">
                        <tr>
                            <td>${gurantor.debtOrder}</td>
                            <td>${gurantor.debtName}</td>
                            <td>${gurantor.debtAddress}</td>
                            <td>${gurantor.debtTp}</td>
                        </tr>
                    </c:forEach>
                </tbody>
                <tr></tr>
            </table>
        </div>
    </div>
   
    <div class="row" style="padding: 5px; margin-bottom: 10px;background: #999; text-align: left">
        <label>Loan Details</label>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2"><label>Agreement No</label></div>
                <div class="col-md-2"><input type="text" value="${agNo}" readonly/></div>
                <div class="col-md-2"><label>Period</label></div>
                <div class="col-md-2"><input type="text" value="${loan.loanPeriod}" readonly/></div>
                <div class="col-md-2"><label>Arrears Installment</label></div>
                <div class="col-md-2"><input type="text" value="${noOfInstallmentInArrears}" readonly/></div>
            </div>
            <div class="row">
                <div class="col-md-2"><label>Loan Rate</label></div>
                <div class="col-md-2"><input type="text" value="${loan.loanInterestRate} %" readonly/></div>
                <div class="col-md-2"><label>Installment</label></div>
                <div class="col-md-2"><input type="text" value="${loan.loanInstallment}" readonly/></div>
                <div class="col-md-2"><label>Arrears Total</label></div>
                <div class="col-md-2"><input type="text" value="${totalArrears}" readonly/></div>
            </div>
            <div class="row">
                <div class="col-md-2"><label>Start Date</label></div>
                <div class="col-md-2"><input type="text" value="${loan.loanStartDate}" readonly/></div>
                <div class="col-md-2"><label>Paid Installment</label></div>
                <div class="col-md-2"><input type="text" value="${paidIns}" readonly/></div>
                <div class="col-md-2"><label>1st Due Date</label></div>
                <div class="col-md-2"><input type="text" value="${loan.loanDueDate}" readonly/></div>
            </div>
            <div class="row">
                <div class="col-md-2"><label>Laps Date</label></div>
                <div class="col-md-2"><input type="text" value="${loanEndDate}" readonly/></div>
                <div class="col-md-2"><label>Remaining Installment</label></div>
                <div class="col-md-2"><input type="text" value="${remainIns}" readonly/></div>
                    <c:choose>
                        <c:when test="${nextDue==null}">
                        <div class="col-md-4"><label style="color: green">No any Installments</label></div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-2"><label>Next Due Date</label></div>
                        <div class="col-md-2"><input type="text" value="${nextDue}" readonly/></div>
                        </c:otherwise>
                    </c:choose>                
            </div>    
        </div>
    </div>
    <div class="row" style="padding: 5px; margin-bottom: 10px;margin-top: 5px;background: #999; text-align: left">
        <label>Payment History</label>
    </div>
    <div class="row">        
        <div class="col-md-12">
            <div class="row" style="border: 1px solid #0088cc; margin-top: 2px;">
                <div class="row" style="margin: 10px;">
                    <input type="hidden" name="hidLoanId" value="${loan.loanId}" />                    
                    <div class="row">
                        <div class="col-md-2"><label>Loan Amount</label></div>
                        <div class="col-md-2"><input type="text" value="${pay.loanAmount}" id="viewLoanAmount" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Down Payment</label></div>
                        <div class="col-md-2"><input type="text" value="${tDown}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Investment</label></div>
                        <div class="col-md-2"><input type="text" value="${pay.investment}" style="text-align: right" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"><label>Total Interest</label></div>
                        <div class="col-md-2"><input type="text" value="${pay.totalInterest}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Other Charges</label></div>
                        <div class="col-md-2"><input type="text" value="${tOther}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Insurance Charges</label></div>
                        <div class="col-md-2"><input type="text" value="${tInsurance}" style="text-align: right" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"><label>ODI Outstanding</label></div>
                        <div class="col-md-2"><input type="text" value="${odiBalance}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Total Receivable</label></div>
                        <div class="col-md-2"><input type="text" value="${tReceivble}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Total Paid Amount</label></div>
                        <div class="col-md-2"><input type="text" value="${totalPaid}" style="text-align: right" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"><label>Over Payments</label></div>
                        <div class="col-md-2"><input type="text" value="${totalOver}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Total Balance</label></div>
                        <div class="col-md-2"><input type="text" value="${totalBalance}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Due Balance</label></div>
                        <div class="col-md-2"><input type="text" value="${dateBalance}" style="text-align: right" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"><label>Capital Outstanding</label></div>
                        <div class="col-md-2"><input type="text" value="${futureCapital}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><label>Interest Outstanding</label></div>
                        <div class="col-md-2"><input type="text" value="${futureInterest}" style="text-align: right" readonly/></div>
                        <div class="col-md-2"><a href="#" onclick="viewLoanTransactionReceipts()" title="View Receipts">View Payments</a></div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding: 5px; margin-bottom: 10px;margin-top: 5px;background: #999; text-align: left">
        <label>View</label>
    </div>
    <div class="row">                    
        <div class="col-md-3"><input type="button" id="btnViewMandate" value="Mandate" class="btn btn-default btnLoans col-md-12" onclick=""/></div> 
        <div class="col-md-3"><input type="button" id="btnViewProfile" value="Customer" class="btn btn-default btnLoans col-md-12" onclick="viewRecoveryReport1()"/></div>        
        <div class="col-md-3"><input type="button" id="btnAddOther" value="Transactions" class="btn btn-default btnLoans col-md-12" onclick="viewLoanTransactions()"/></div>
        <div class="col-md-3"><input type="button" id="btnAddOther" value="Documents" class="btn btn-default btnLoans col-md-12" onclick="viewUploadDocument(${loan.loanId})"/></div>
    </div>
    <div class="row" style="margin-top: 2px">
        <div class="col-md-3"><input type="button" id="btnAddOther" value="Insurance" class="btn btn-default btnLoans col-md-12" onclick="viewInsurance(${loan.loanId})"/></div>
        <div class="col-md-3"><input type="button" id="btnAddOther" value="PD Cheques" class="btn btn-default btnLoans col-md-12" onclick="viewPDChequeNote()"/></div>
        <div class="col-md-3"><input type="button" id="btnAddOther" value="Notes/Messages" class="btn btn-default btnLoans col-md-12" onclick="viewLoanMessage(${loan.loanId})"/></div>
        <div class="col-md-3"><input type="button" id="btnAddOther" value="Add Charges" class="btn btn-default btnLoans col-md-12" onclick="loadRecoveryCharges(${loan.loanId})"/></div>
    </div>
    <form method="GET" action="/AxaBankFinance/generateMandateReport" target="blank" id="formPDFMandateView">
        <input type="hidden" name="mandateLoanId"  id="txtMandateLoanId" value="${loan.loanId}"/>
    </form> 
    <form method="POST" action="/AxaBankFinance/ReportController/viewRecoverReport" target="blank" id="viewRecovery1">
        <input type="hidden" name="loan_id_r" value="${loan.loanId}"/> 
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
    <form method="GET" action="/AxaBankFinance/generatePDFVoucher" target="blank" id="formPDFVoucher2">
        <input type="hidden" name="hidVoucherId"  id="txtHidVoucherId" value="${loan.voucherId}"/>
        <input type="hidden" name="hidVoucherType"  id="txtHidVoucherType" value="1"/>
    </form>
    <form method="GET" action="/AxaBankFinance/ReportController/PrintChequeRecvNote" target="blank" id="formPDChequeNote">
        <input type="hidden" name="aggNo" id="txtHidAggNo" value="${loan.loanAgreementNo}"/>
    </form> 
    <form method="GET" action="/AxaBankFinance/ReportController/printLoanTransactionsReport" target="blank" id="formTransaction">
        <input type="hidden" name="aggNo" id="txtHidAggNo" value="${loan.loanAgreementNo}"/>
    </form> 
    <form method="GET" action="/AxaBankFinance/ReportController/printLoanTransactionsReceiptReport" target="blank" id="formTransactionReceipt">
        <input type="hidden" name="aggNo" id="txtHidAggNo" value="${loan.loanAgreementNo}"/>
    </form> 
</div>
<div id="docDivCheck" class="searchCustomer" style="width: 85%;border: 0px solid #5e5e5e;margin-left: -15%;"></div>
<div id="insuranceDiv" class="jobsPopUp box" style="width: 45%;border: 0px solid #5e5e5e"></div>
<div id="loanMessageDiv" class="searchCustomer" style="width: 85%;border: 0px solid #5e5e5e;margin-left: -15%;"></div>
<div id="loanRecoveryChrgDiv" class="searchCustomer" style="width: 85%;border: 0px solid #5e5e5e;margin-left: -15%;"></div>
<script>
    $(function () {
        $("#btnViewMandate").click(function () {
            $("#formPDFMandateView").submit();
        });
    });
    function viewRecoveryReport1() {
        $("#viewRecovery1").submit();
    }
    function printVoucherPDF() {
        $("#formPDFVoucher2").submit();
    }
    //load document submission form
    function viewUploadDocument(loanId) {
        $.ajax({
            url: "/AxaBankFinance/loadDocumentChecking",
            data: {loan_id: loanId},
            success: function (data) {
                $("#docDivCheck").html(data);
                ui('#docDivCheck');
            }
        });
    }

    function loadRecoveryCharges(loanId) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadDebitChargesPage/" + loanId,
            success: function (data) {
                $("#loanRecoveryChrgDiv").html("");
                $("#loanRecoveryChrgDiv").html(data);
                ui('#loanRecoveryChrgDiv');
            }
        });
    }

    function viewInsurance(loanId) {
        $.ajax({
            url: "/AxaBankFinance/InsuraceController/findByLoanId/" + loanId,
            success: function (data) {
                $("#insuranceDiv").html("");
                $("#insuranceDiv").html(data);
                ui('#insuranceDiv');
            }
        });
    }

    function viewPDChequeNote() {
        $("#formPDChequeNote").submit();
    }

    function viewLoanMessage(loanId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadLoanMessageMain/" + loanId,
            success: function (data) {
                $("#loanMessageDiv").html("");
                $("#loanMessageDiv").html(data);
                ui('#loanMessageDiv');
            }
        });
    }

    function viewLoanTransactions() {
        $("#formTransaction").submit();
    }

    function viewLoanTransactionReceipts() {
        $("#formTransactionReceipt").submit();
    }
</script>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <label>Add Recovery Comment</label>
        </div>
        <input type="hidden" value="${loanId}" id="r_loanId"/>        
        <input type="hidden" value="${visitId}" id="r_visitId"/>        
        <div class="row">
            <div class="col-md-4">Recovery Officer Comment</div>
            <div class="col-md-8"><textarea></textarea></div>
        </div>
        <div class="row">
            <div class="col-md-4">Recovery Manager Comment</div>
            <div class="col-md-8"><textarea></textarea></div>
        </div>
        <div class="row">
            <div class="col-md-4">Upload Document</div>
            <div class="col-md-8"><input type="file" name="" id="" /></div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <input type="button" class="btn btn-default btn-edit col-md-12" value="Cancel" onclick="unblockui()"/>
            <input type="button" class="btn btn-default btn-edit col-md-12" value="save" onclick="saveRecoveryComment()"/>
        </div>
    </div>
</div>
        
<div class="searchCustomer" id="msg_recovey">

</div>

<script>
    function saveRecoveryComment(){
        var loanId = $("#r_loanId").val();
        var visitId = $("#r_visitId").val();
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/saveRecoveryComment/',
            data: {loanId:loanId,visitId:visitId},
            type: 'GET',
            success: function(data) {
                $('#msg_recovey').html("");
                $('#msg_recovey').html(data);
                ui('#msg_recovey');
            },
            error: function() {
                alert("Error Loading letters");
            }
        });        
    }
    
    function uploadRecoveryReport(){
        
        
    }
    
</script>
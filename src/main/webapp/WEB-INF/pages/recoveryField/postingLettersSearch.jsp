<%-- 
    Document   : postingLettersSearch
    Created on : Aug 11, 2016, 3:06:59 PM
    Author     : admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#fieldOfficerSearchTable").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>
<c:choose>
    <c:when test="${byDate}">
        <table class="dc_fixed_tables table-bordered" id="fieldOfficerSearchTable" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: center">Agreement No</th>
                    <th style="text-align: center">Vehicle No</th>
                    <th style="text-align: center">Customer Name</th>
                    <th style="text-align: center">Due Date</th> 
                    <th style="text-align: center">Arrears Amount</th>
                    <th style="text-align: center">History</th>
                    <th style="text-align: center">Comment</th>
                    <th style="text-align: center">Charge</th>
                    <th style="text-align: center">Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="loans" items="${loanList}">

                    <tr style="background-color:${loans.color}">  
                        <c:choose>
                            <c:when test="${loans.letteId>9}">
                                <c:set var="isLetter" value="n"></c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="isLetter" value="y"></c:set>
                            </c:otherwise>
                        </c:choose>

                <!--<td>${loans.loanId}</td>-->
                        <td id="agg${loans.loanId}${isLetter}" style="text-align: center">${loans.agreementNo}</td>  
                        <td id="book{loans.loanId}${isLetter}" style="text-align: center">${loans.vehicleNo}</td>  
                        <td style="text-align: center">${loans.debtorName}</td>                  
                        <td id="due${loans.loanId}${isLetter}" style="text-align: center">${loans.dueDate}</td>                  
                        <td><input type="text" value="${loans.arresAmount}" style="width:100px;text-align: right" id="amnt${loans.loanId}${isLetter}" readonly=""/></td>
                        <td style="text-align: center"><div class="view_hist" onclick="loadLetterHistory(${loans.loanId})" title="View History"></div></td>                
                        <td><input type="text" id="comme${loans.loanId}${isLetter}"/></td>
                        <td><input type="text" value="${loans.odi}" id="chrg${loans.loanId}${isLetter}" style="width:50px;text-align: right"/></td>
                        <td style="text-align: center">
                            <div class="row">
                                <c:choose>
                                    <c:when test="${loans.letteId>9}">
                                        <!--visit loans-->
                                        <c:choose>
                                            <c:when test="${loans.recoverId==0}">
                                                <div class="col-md-6"> <div id="post${loans.loanId}${isLetter}" class="assign_off" onclick="sendVisit(${loans.loanId},${loans.letteId}, 0)"></div></div>
                                                </c:when>
                                                <c:otherwise>
                                                <div class="col-md-6"> <div id="assgn${loans.loanId}${isLetter}" class="edit_officer" onclick="loadManagerComment(${loans.loanId},${loans.letteId},${loans.recoverId})"></div></div>
                                                </c:otherwise>
                                            </c:choose>                                    
                                        <div class="col-md-6"><div id="cancel${loans.loanId}${isLetter}" class="cancel_letter" onclick="sendVisit(${loans.loanId},${loans.letteId}, 1)"></div></div>
                                        </c:when>
                                        <c:otherwise>
                                        <!--letters-->
                                        <div class="col-md-6"><div id="post{loans.loanId}${isLetter}" class="posting_letter" onclick="postLetter(${loans.loanId},${loans.letteId}, 0)" title="Post Letter"></div></div>
                                        <div class="col-md-6"><div id="cancel${loans.loanId}${isLetter}" class="cancel_letter" onclick="postLetter(${loans.loanId},${loans.letteId}, 1)" title="Cancel Letter"></div></div>
                                        </c:otherwise>
                                    </c:choose>                        
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>        
    </c:when>
    <c:otherwise>
        <table class="dc_fixed_tables table-bordered" id="fieldOfficerSearchTable" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: center">Agreement No</th>
                    <th style="text-align: center">Vehicle No</th>
                    <th style="text-align: center">Customer Name</th>
                    <th style="text-align: center">Due Date</th> 
                    <th style="text-align: center">Arrears Amount</th>
                    <th style="text-align: center">Comment</th>
                    <th style="text-align: center">Charge</th>
                    <th style="text-align: center">View</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="loans" items="${loanList}">

                    <tr style="background-color:${loans.color}">  
                        <c:choose>
                            <c:when test="${loans.letteId>9}">
                                <c:set var="isLetter" value="n"></c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="isLetter" value="y"></c:set>
                            </c:otherwise>
                        </c:choose>

                <!--<td>${loans.loanId}</td>-->
                        <td id="agg${loans.loanId}${isLetter}" style="text-align: center">${loans.agreementNo}</td>  
                        <td id="book{loans.loanId}${isLetter}" style="text-align: center">${loans.vehicleNo}</td>  
                        <td style="text-align: center">${loans.debtorName}</td>                  
                        <td id="due${loans.loanId}${isLetter}" style="text-align: center">${loans.dueDate}</td>                  
                        <td><input type="text" value="${loans.arresAmount}" style="width:100px;text-align: right" id="amnt${loans.loanId}${isLetter}" readonly=""/></td>
                        <td><input type="text" id="comme${loans.loanId}${isLetter}"/></td>
                        <td><input type="text" value="${loans.odi}" id="chrg${loans.loanId}${isLetter}" style="width:50px;text-align: right"/></td>
                        <td style="text-align: center">
                            <div class="row">
                                <c:choose>
                                    <c:when test="${loans.letteId>9}">
                                        <!--visit loans-->
                                        <c:choose>
                                            <c:when test="${loans.recoverId==0}">
                                                <div class="col-md-6"> <div id="post${loans.loanId}${isLetter}" class="assign_off" onclick="sendVisit(${loans.loanId},${loans.letteId}, 0)"></div></div>
                                                </c:when>
                                                <c:otherwise>
                                                <div class="col-md-6"> <div id="assgn${loans.loanId}${isLetter}" class="edit_officer" onclick="loadManagerComment(${loans.loanId},${loans.letteId},${loans.recoverId})"></div></div>
                                                </c:otherwise>
                                            </c:choose>                                    
                                        <div class="col-md-6"><div id="cancel${loans.loanId}${isLetter}" class="cancel_letter" onclick="sendVisit(${loans.loanId},${loans.letteId}, 1)"></div></div>
                                        </c:when>
                                        <c:otherwise>
                                        <!--letters-->
                                        <div class="col-md-6"><div id="post{loans.loanId}${isLetter}" class="posting_letter" onclick="viewLetter(${loans.loanId},${loans.letteId})" title="View Letter"></div></div>
<!--                                        <div class="col-md-6"><div id="cancel${loans.loanId}${isLetter}" class="cancel_letter" onclick="postLetter(${loans.loanId},${loans.letteId}, 1)" title="Cancel Letter"></div></div>-->
                                    </c:otherwise>
                                </c:choose>                        
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:otherwise>
</c:choose>

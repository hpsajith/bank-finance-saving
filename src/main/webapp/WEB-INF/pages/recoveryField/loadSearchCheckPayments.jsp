<%-- 
    Document   : loadSearchCheckPayments
    Created on : Jul 22, 2015, 12:23:59 PM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table class="dc_fixed_tables table-bordered" id="fieldOfficerTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th></th>
                <th>Agreement No</th>
                <th>Debtor Account No</th>    
                <th>Receipt No</th>
                <th>Bank Name</th>
                <th>Cheque No</th>
                <th>Realize Date</th>
                <th>Paid Amount</th>    
            </tr>
        </thead>
        <tbody>
            <c:forEach var="loans" items="${checkPaymentList}">                
                <c:choose>
                    <c:when test="${loans.isReturn}">
                        <tr style="background-color: #e4b9c0"> 
                    </c:when>
                    <c:otherwise>
                        <tr> 
                    </c:otherwise>
                </c:choose>              
                <td><input type="checkbox" class="paymentCheckBox" id="payment${loans.chequeId}" onclick="selectLoanD(${loans.chequeId})"/></td>             
                <td>${loans.agreementNo}</td>
                <td>${loans.debtorAccountNo}</td>    
                <td>${loans.receiptNo}</td>
                <td>${loans.bankName}</td>
                <td>${loans.chequeNo}</td>
                <td>${loans.realizeDate}</td>
                <td>${loans.amount}</td>    
        </c:forEach>
        </tbody>
    </table>
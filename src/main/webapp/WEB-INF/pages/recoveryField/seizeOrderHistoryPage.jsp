<%-- 
    Document   : seizeOrderHistoryPage
    Created on : Mar 1, 2016, 12:38:54 PM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12" >
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Seize Order History</strong>
    </div>
    <div class="row">
        <table id="tblSeizeOrderHistory" class="table table-bordered table-edit table-hover table-responsive dataTable">
            <thead>
                <tr>
                    <th style="text-align: center">Code</th>
                    <th style="text-align: center">Key Issue Date</th>
                    <th style="text-align: center">Renewal Date</th>
                    <th style="text-align: center">Renewal Count</th>
                    <th style="text-align: center">Renewal Reason</th>
                </tr>
            </thead>
            <tbody> 
                <c:forEach var="seizeOrder" items="${seizeOrderList}">
                    <tr>
                        <td style="text-align: center">${seizeOrder.seizeCode}</td>
                        <td style="text-align: center">${seizeOrder.keyIssueDate}</td>
                        <td style="text-align: center">${seizeOrder.renewalDate}</td>
                        <td style="text-align: center">${seizeOrder.renewalCount}</td>
                        <td style="text-align: center">${seizeOrder.renewalRemark}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </div>
</div>
<div class="col-md-12 panel-footer">
    <div class="col-md-10">           
    </div>
    <div class="col-md-2">
        <div class="row">
            <button class="btn btn-default col-md-12" onclick="unblockui()"><span class="fa fa-times"></span> Exit</button>
        </div>
    </div>
</div>
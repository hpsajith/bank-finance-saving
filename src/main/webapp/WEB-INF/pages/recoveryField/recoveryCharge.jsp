<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Add Debit Charge</strong>
    </div>
    <form id="addRecoveryCharge" name="addRecoveryCharge_">
        <div class="col-md-12">
            <div class="row" style="margin-top: 10px">
                <div class="col-md-3" style="text-align: left">Supplier</div>
                <div class="col-md-9">
                    <select id="inpSuppliers" name="supplierId" style="width: 100%">
                        <c:forEach var="supplier" items="${suppliers}">
                            <option value="${supplier.supplierId}">${supplier.supplierName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-md-3" style="text-align: left">Charge Type</div>
                <div class="col-md-9">
                    <select id="otherChageRecovery" name="recoveryChargeType" style="width: 100%">
                        <c:forEach var="extra" items="${extraCharge}">
                            <option value="${extra.id}">${extra.description}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-md-3" style="text-align: left">Amount</div>
                <div class="col-md-9">
                    <input type="text" name="recoveryChargeAmount" value="0.00" style="width: 100%"/>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button"  value="Add" class="btn btn-default col-md-12" onclick="addRecoveryCharge2(${loan.loanId})"></div>
            </div>
            <input type="hidden" name="recoveryChargeLoanId" value="${loan.loanId}"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </div>
    </form>
</div> 
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>

    function addRecoveryCharge2(loanId) {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/saveRecoveryCharge',
            type: 'POST',
            data: $("#addRecoveryCharge").serialize(),
            success: function (data) {
                $("#msgDiv").html(data);
                ui('#msgDiv');
                setTimeout(function () {
                    $.ajax({
                        url: '/AxaBankFinance/RecoveryController/viewLoanDetails/',
                        data: {loanId: loanId},
                        success: function (data) {
                            unblockui();
                            $('#recoveSeachLoan').html(data);
                        },
                        error: function () {
                            alert("Error Loading...3");
                        }
                    });
                    unblockui();
                }, 500);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

</script>
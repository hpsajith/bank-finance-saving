<%-- 
    Document   : seizeArticleImagesPage
    Created on : Feb 3, 2016, 4:55:33 PM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row" style="padding: 10px">
    <div class="row" style="margin: 10px; padding: 5px; border: 1px solid #08C; font-size: 12px;">
        <div class="col-md-12"><legend><strong>Article Images</strong></legend></div>
        <div class="col-md-12" style="overflow-y: auto; height: 300px;"> 
            <table class="table table-bordered table-edit table-hover table-responsive dataTable">
                <thead>
                    <tr>
                        <th style="text-align: center">Description</th>
                        <th style="text-align: center">Select File</th>
                        <th style="text-align: center">Upload</th>
                        <th style="text-align: center">View</th>
                        <th style="text-align: center">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <c:choose>
                        <c:when test="${checkList.size() > 0}">
                            <c:set var="count" value="0" scope="page"/>
                            <c:set var="maxId" value="0" scope="page"/>
                            <c:forEach var="checkItem" items="${checkList}">
                                <tr id="rw_${checkItem.docId}">
                            <input type="hidden" id="txtDocId_${checkItem.docId}" value="${checkItem.docId}"/>
                            <input type="hidden" id="txtSeizeOrderId_${checkItem.seizeOrderId}" value="${checkItem.seizeOrderId}"/>
                            <input type="hidden" id="txtYardRegId_${checkItem.yardRegId}" value="${checkItem.yardRegId}"/>
                            <td style="text-align: center">
                                <input type="text" id="txtImageName_${checkItem.docId}" value="${checkItem.description}" style="width: 100%" />
                                <label id="msgTxtImageName_${checkItem.docId}" class="msgTextField"></label>
                            </td>
                            <td style="text-align: center">
                                <input type="file" id="fileBrowse_${checkItem.docId}" name="Browse" class="loader_1"/>
                                <label id="msgFileBrowse_${checkItem.docId}" class="msgTextField"></label>
                            </td>
                            <td style="text-align: center">
                                <input type="button" id="btnUpload_${checkItem.docId}" class="btn btn-default" style="margin-top: 4px;" value="Upload" onclick="uploadFile(${checkItem.docId})"/>
                            </td>
                            <td style="text-align: center">
                                <input type="button" id="btnView_${checkItem.docId}" class="btn btn-default" style="margin-top: 4px; width: 70px;" value="View" onclick="viewFile(${checkItem.docId},${checkItem.seizeOrderId})"/>
                            </td>
                            <td style="text-align: center">
                                <div id="status_${checkItem.docId}" class="fileComment success_file"></div>
                            </td>                                    
                            </tr>
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <c:set var="maxId" value="${checkItem.docId}" scope="page"/>
                        </c:forEach>
                        <c:if test="${count!=5}">
                            <c:forEach var="i" begin="1" end="${5-count}">
                                <c:set var="i" value="${maxId + i}" scope="page"/>
                                <tr id="rw_${i}">
                                <input type="hidden" id="txtDocId_${i}" />
                                <input type="hidden" id="txtSeizeOrderId_${i}"/>
                                <input type="hidden" id="txtYardRegId_${i}"/>                                    
                                <td style="text-align: center">
                                    <input type="text" id="txtImageName_${i}" style="width: 100%" />
                                </td>
                                <td style="text-align: center">
                                    <input type="file" id="fileBrowse_${i}" name="Browse" class="loader_1"/>
                                </td>
                                <td style="text-align: center">
                                    <input type="button" id="btnUpload_${i}" class="btn btn-default" style="margin-top: 4px;" value="Upload" onclick="uploadFile(${i})"/>
                                </td>
                                <td style="text-align: center">
                                    <input type="button" id="btnView_${i}" class="btn btn-default" style="margin-top: 4px; width: 70px;" value="View" disabled/>
                                </td>
                                <td style="text-align: center">
                                    <div id="status_${i}" ></div>
                                </td>
                                </tr>                     
                            </c:forEach>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="i" begin="1" end="5">
                            <tr id="rw_${i}">
                            <input type="hidden" id="txtDocId_${i}" />
                            <input type="hidden" id="txtSeizeOrderId_${i}"/>
                            <input type="hidden" id="txtYardRegId_${i}"/>                                    
                            <td style="text-align: center">
                                <input type="text" id="txtImageName_${i}" style="width: 100%" />
                            </td>
                            <td style="text-align: center">
                                <input type="file" id="fileBrowse_${i}" name="Browse" class="loader_1"/>
                            </td>
                            <td style="text-align: center">
                                <input type="button" id="btnUpload_${i}" class="btn btn-default" style="margin-top: 4px;" value="Upload" onclick="uploadFile(${i})"/>
                            </td>
                            <td style="text-align: center">
                                <input type="button" id="btnView_${i}" class="btn btn-default" style="margin-top: 4px; width: 70px;" value="View" disabled/>
                            </td>
                            <td style="text-align: center">
                                <div id="status_${i}" ></div>
                            </td>
                            </tr>                                
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 panel-footer">
        <div class="col-md-10">           
        </div>
        <div class="col-md-2">
            <div class="row">
                <button class="btn btn-default col-md-12" onclick="unblockui()"><span class="fa fa-times"></span> Exit</button>
            </div>
        </div>
    </div>
</div>
<div class="searchCustomer" id="documentBox" style="width: 85%; margin-left: -15%">
    <div class="close_div" onclick="unloadDocumentView()"><label>X</label></div>
    <div style="clear:both"></div>
    <div class="row" id="document_title" style="margin-left: 15px; margin-right: 15px;"></div>
    <div class="row" id="document_content" style="margin-top: 15px; padding:5px; height: 700px; overflow-y: auto;"></div>
</div>
<script type="text/javascript">

    var files = null;

    $(function() {
        $(".loader_1").on('change', prepareLoad);
    });

    function prepareLoad(event)
    {
        console.log('event fired' + event.target.files[0].name);
        files = event.target.files;
    }

    function uploadFile(id) {
        if (feildValidate(id)) {
            var oForm = new FormData();
            var docId = $("#txtDocId_" + id).val();
            var description = $("#txtImageName_" + id).val();
            if (docId === "") {
                oForm.append("docId", "0");
            } else {
                oForm.append("docId", docId);
            }
            oForm.append("seizeOrderId", seizeOrderIdA);
            oForm.append("yardRegId", yardRegIdA);
            oForm.append("description", description);
            oForm.append("file", files[0]);
            document.getElementById("btnUpload_" + id).value = 'Uploading..';
            $.ajax({
                dataType: 'html',
                url: "/AxaBankFinance/uploadArticleImage?${_csrf.parameterName}=${_csrf.token}",
                data: oForm,
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                success: function(data) {
                    document.getElementById("btnUpload_" + id).value = 'Upload';
                    files = null;
                    if (data) {
                        $("#status_" + id).addClass("fileComment success_file");
                    } else {
                        $("#status_" + id).addClass("fileComment warning_file");
                    }
                    document.getElementById("btnView_" + id).disabled = false;
                    RefreshPage();
                },
                error: function() {
                    console.log("Error in upload article images");
                }
            });
        }
    }

    function feildValidate(id) {
        var imageName = $("#txtImageName_" + id).val();
        if (imageName === null || imageName === "") {
            $("#msgTxtImageName_" + id).text("Enter Image Name");
            $("#txtImageName_" + id).addClass("txtError");
            $("#txtImageName_" + id).focus();
            return false;
        } else {
            $("#msgTxtImageName_" + id).text("");
            $("#txtImageName_" + id).removeClass("txtError");
        }

        if (files === null) {
            $("#msgFileBrowse_" + id).text("Select a File");
            return false;
        } else {
            $("#msgFileBrowse_" + id).text("");
        }
        return true;
    }

    function viewFile(docId, seizeOrderId) {
        var url = "/AxaBankFinance/viewArticleImage?docId=" + docId + "&" + "seizeOrderId=" + seizeOrderId;
        $("#document_title").html("<legend style='text-align:left; font-weight:&" + "bold'>" + "Article Image" + "</legend>");
        $("#document_content").html("<img src='" + url + "' ></img>");
        ui('#documentBox');
    }

    function unloadDocumentView() {
        unblockui();
        $("#document_title").html("");
        $("#document_content").html("");
        ui("#divCheckList");
    }

    function RefreshPage() {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadSeizeArticleImagesPage",
            type: 'GET',
            data: {"seizeOrderId": seizeOrderIdA},
            success: function(data) {
                $("#divCheckList").html("");
                $("#divCheckList").html(data);
                ui('#divCheckList');
            }
        });
    }

</script>

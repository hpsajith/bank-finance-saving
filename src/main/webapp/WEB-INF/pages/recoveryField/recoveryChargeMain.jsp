<%-- 
    Document   : recoveryChargeMain
    Created on : Aug 30, 2016, 1:00:43 PM
    Author     : admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row" style="padding: 10px">
    <div class="row" style="margin: 10px; padding: 5px; border: 1px solid #08C; font-size: 12px;">
        <div class="col-md-12"><legend><strong>Direct Debit Charges</strong></legend></div>
        <div class="col-md-12" style="overflow-y: auto; height: 300px;"> 
            <table class="table table-bordered table-edit table-hover table-responsive dataTable">
                <thead>
                    <tr>
                        <th style="text-align: center">Debit Date</th>
                        <th style="text-align: center">Supplier</th>
                        <th style="text-align: center">Description</th>
                        <th style="text-align: center">Debit Amount</th>
                        <th style="text-align: center">Delete</th>                                    
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="debitCharge" items="${debitCharges}">
                        <tr id="${debitCharge.chargeId}">
                            <td style="text-align: center">${debitCharge.debitDate}</td>
                            <td style="text-align: center">${debitCharge.supplierName}</td>
                            <td style="text-align: center">${debitCharge.description}</td>
                            <td style="text-align: right">${debitCharge.debitAmount}</td>
                            <td style="text-align: center">
                                <c:choose>
                                    <c:when test="${debitCharge.isPaid == 0}">
                                        <div class='delete' title="Delete" href='#' onclick="deleteRecoveryCharge(${loanId},${debitCharge.chargeId},${debitCharge.settlementId})"></div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class='delete' href='#' style="cursor: not-allowed"></div>
                                    </c:otherwise>
                                </c:choose>
                            </td>                                            
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>  
    </div>
    <div class="col-md-12 panel-footer">
        <div class="col-md-8">           
        </div>
        <div class="col-md-2">
            <button class="btn btn-default col-md-12" onclick="loadRecoveryChargeForm(${loanId})">Add New</button>
        </div>
        <div class="col-md-2">
            <button class="btn btn-default col-md-12" onclick="unblockui()">Cancel</button>
        </div>
    </div>
</div>
<div id="recoveryChrgeDiv" class="searchCustomer" style="width: 40%;margin: 5% 10%; border: 0px solid #5e5e5e;">
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    function loadRecoveryChargeForm(loanId) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/loadRecoveryCharge",
            data: {loanId: loanId},
            success: function (data) {
                $("#recoveryChrgeDiv").html(data);
                ui('#recoveryChrgeDiv');
            }
        });
    }

    function deleteRecoveryCharge(loanId, chargeId, settlementId) {
        $.ajax({
            url: "/AxaBankFinance/RecoveryController/deleteDebitCharge/" + chargeId + "/" + settlementId,
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function () {
                    $.ajax({
                        url: '/AxaBankFinance/RecoveryController/viewLoanDetails/',
                        data: {loanId: loanId},
                        success: function (data) {
                            unblockui();
                            $('#recoveSeachLoan').html(data);
                        },
                        error: function () {
                            alert("Error Loading...3");
                        }
                    });
                    unblockui();
                }, 500);
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

</script>

<%-- 
    Document   : checkReturnProcess
    Created on : Jul 22, 2015, 1:45:51 PM
    Author     : ITESS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>

    <div class="col-md-12 col-lg-12 col-sm-12" style="display:block;">

        <c:choose>
            <c:when test="${priviledge}">
                <form name="checkReturnForm" id="checkReturnForm" > 

                    <input type="hidden" value="${checkDetaiId}" name="checkDetaiId" id="checkDetaiId" readonly="true">    

                    <div class="row"></div>
                    <h1>Add To Return Cheque List !</h1>
                    <div class="row"></div>

                    <!--<div class="row divError" style="padding:10px; "></div>-->  
                    <label id="msgReason" class="msgTextField"></label> 
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3">Cheque Return note * </div>
                        <div class="col-md-6 col-lg-6 col-sm-6"><input type="text" value="${remark}" style="width:100%" name="remark" id="remark"/></div>
                    </div>

                    <div  class="col-md-3 "><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
                    <div  class="col-md-6 "><input type="button"  value="Set As Return" onclick="changeCheckStatus()" class="btn btn-success col-md-12 col-sm-12 col-lg-12"/></div>
                </form>
            </c:when>
            <c:otherwise>
                <h3>You don't have privileges !</h3>
                <div  class="col-md-3 "><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
                </c:otherwise>
            </c:choose>

    </div>    
</div>

<script>

    function changeCheckStatus() {

        var validatef = validateForm();
        if (validatef) {
            
            var checkDetaiId = $('#checkDetaiId').val();
            var remark = $('#remark').val();
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/saveCheckReturn',
                type: 'GET',
                data:{"checkDetaiId":checkDetaiId,"remark":remark},
                success: function(data) {
                    $('#divProcess').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    loadCheqPayments();
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        } else {
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $("#msgReason").html(errorMessage);

            errorMessage = "";

//                $("html, body").animate({scrollTop: 0}, "slow");
        }
    }

    var errorMessage = "";
    function validateForm() {
        var validate = true;
        var remark = $('input[name=remark]').val();
        if (remark === null || remark === "") {
            validate = false;
            errorMessage = errorMessage + "Enter Remark";
            $("#remark").addClass("txtError");
        } else {
            $("#remark").removeClass("txtError");
        }

        return validate;
    }
    
        function loadCheqPayments(){

            $.ajax({
                url: '/AxaBankFinance/RecoveryController/loadCheckPayment',
                success: function(data) {
                $('#formContent').html("");
                $('#formContent').html(data);
                },
                error: function() {
                }
            });
    }
</script>
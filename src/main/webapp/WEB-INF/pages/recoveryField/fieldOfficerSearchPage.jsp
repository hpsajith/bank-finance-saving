<%-- 
    Document   : fieldOfficerSearchPage
    Created on : Jul 8, 2015, 4:07:55 PM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>
    $(document).ready(function () {
        pageAuthentication();
        unblockui();
    });

</script>

<div class="row" style="height: 90%;width: 102%" >
    <table class="dc_fixed_tables table-bordered newTable" width="120%" border="0" cellspacing="0" cellpadding="0" id="tblViewRecoveryShift1" >
        <thead>
            <tr>
                <th></th>
                <th>Agreement No</th>
                <th>Book No</th>
                <th>Customer Name</th>
                <th>Telephone</th>
                <th>Address</th>
                <th>Due Date</th>       
                <th>Shift Date</th>
                <th>Remark</th>
                <th>Installment</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="loans" items="${loanList}">
                <c:choose>
                    <c:when test="${loans.msgSameShiftDate}">
                        <tr style="background-color:#7BB0E6">  
                            <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${loans.loanId}" onclick="selectLoanD(${loans.loanId},${loans.instlmntId},${loans.msgDateShifted})"/></td>    
                            </c:when>
                            <c:when test="${loans.msgAfterShiftDate}">
                        <tr style="background-color:#9acfea">  
                            <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${loans.loanId}" onclick="selectLoanD(${loans.loanId},${loans.instlmntId},${loans.msgDateShifted})"/></td>    
                            </c:when>
                            <c:otherwise>
                        <tr>
                            <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${loans.loanId}" onclick="selectLoanD(${loans.loanId},${loans.instlmntId},${loans.msgDateShifted})"/></td>
                            </c:otherwise>        
                        </c:choose> 
                    <td>${loans.agreementNo}</td>    
                    <td>${loans.loanBookNo}</td>
                    <td>${loans.debtorName}</td>
                    <td>${loans.debtorTelephone}</td>
                    <td width="100px">${loans.debtorAddress}</td>
                    <td>${loans.dueDate}</td>  
                    <td><input type="text" readonly="true" value="${loans.shiftDate}" style="width:100px"/></td>                        
                    <td><input type="text" readonly="true" value="${loans.remark}" style="width:100px"/></td>
                    <td>${loans.loanAmount}</td>
                    <c:choose>
                        <c:when test="${loans.loanSpec==0}">
                            <c:choose>
                                <c:when test="${loans.loanStatus==0}">
                                    <td style="color: orange;">Processing For Loan</td>
                                </c:when>
                                <c:when test="${loans.loanStatus==1}">
                                    <td style="color: green;">On Going</td>
                                </c:when>
                                <c:otherwise>
                                    <td style="color: red;">Rejected</td>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:when test="${loans.loanSpec==1}">
                            <td style="color: red;">Laps</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==2}">
                            <td style="color: red;">Legal</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==3}">
                            <td style="color: red;">Seize</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==4}">
                            <td style="color: green;">Full Paid</td>
                        </c:when>
                        <c:when test="${loans.loanSpec==5}">
                            <td style="color: green;">Rebate</td>
                        </c:when>
                    </c:choose> 
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div id="message_shiftDates" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>


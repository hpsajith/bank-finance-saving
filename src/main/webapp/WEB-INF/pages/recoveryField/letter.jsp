<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="row" style="padding: 10px">
    <div class="row" style="margin: 10px; padding: 5px; border: 1px solid #08C; font-size: 12px;">
        <div class="row"><input type="hidden" id="hidTxtLoanId" value="${loanDetails.loanId}" /></div>
        <div class="col-md-12"><legend><strong>Recovery History</strong></legend></div>
        <div class="col-md-12" style="overflow-y: auto; height: 300px;"> 
            <c:choose>
                <c:when test="${hasHistory}">
                    <label>Letter Details</label>
                    <table class="table table-bordered table-edit table-hover table-responsive dataTable">
                        <thead>
                            <tr>
                                <th style="text-align: center">Send Date</th>
                                <th style="text-align: center">Letter Type</th>
                                <th style="text-align: center">Due Date</th>
                                <th style="text-align: center">Arrears Amount</th>
                                <th style="text-align: center">Letter Charge</th>
                                <th style="text-align: center">Comment</th>
                                <th style="text-align: center">Status</th>
                            </tr>
                        </thead>
                        <c:forEach var="record" items="${letterList}">
                            <c:if test="${record.letters.size() > 0}">
                                <tbody>
                                    <c:forEach var="letter" items="${record.letters}">
                                        <tr>
                                            <td style="text-align: center">${letter.systemDate}</td>
                                            <td style="text-align: center">
                                                <c:if test="${letter.letterId == 1}">
                                                    NOT
                                                </c:if>
                                                <c:if test="${letter.letterId == 2}">
                                                    LOT
                                                </c:if>
                                            </td>
                                            <td style="text-align: center">${letter.dueDate}</td>
                                            <td style="text-align: right">${letter.balance}</td>
                                            <td style="text-align: right">${letter.odi}</td>                            
                                            <td style="text-align: center">${letter.comment}</td>                            
                                            <c:choose>
                                                <c:when test="${letter.letterStatus==1}">
                                                    <td style="color: blue;text-align: center"><b>Posted</b></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td style="color: red;text-align: center"><b>Cancel</b></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </c:forEach>
                    </table>
                    <label>Visits Details</label>
                    <table class="table table-bordered table-edit table-hover table-responsive dataTable">
                        <thead>
                            <tr>
                                <th style="text-align: center">Visit Date</th>
                                <th style="text-align: center">Visit Type</th>
                                <th style="text-align: center">Recovery Officer</th>
                                <th style="text-align: center">Comment</th>
                                <th style="text-align: center">Status</th>
                                <th style="text-align: center">View</th>
                            </tr>
                        </thead>
                        <c:forEach var="record" items="${letterList}">
                            <c:if test="${record.visits.size()>0}">
                                <tbody>
                                    <c:forEach var="visit" items="${record.visits}">
                                        <tr>
                                            <td style="text-align: center">${visit.systemDate}</td>
                                            <c:choose>
                                                <c:when test="${visit.visitType==1}">
                                                    <td style="text-align: center">1st Visit</td>  
                                                </c:when>
                                                <c:when test="${visit.visitStatus==2}">
                                                    <td style="text-align: center">2nd Visit</td>  
                                                </c:when>
                                                <c:otherwise>
                                                    <td style="text-align: center">3rd Visit</td>  
                                                </c:otherwise>
                                            </c:choose>
                                            <td style="text-align: center">${visit.officerName}</td>
                                            <td style="text-align: center">${visit.visitComment}</td>    
                                            <c:choose>
                                                <c:when test="${visit.visitStatus==1}">
                                                    <td style="color: yellow;text-align: center">Pending</td>  
                                                </c:when>
                                                <c:when test="${visit.visitStatus==2}">
                                                    <td style="color: red;text-align: center">Cancel</td>  
                                                </c:when>
                                                <c:otherwise>
                                                    <td style="color: green;text-align: center">Submitted</td>  
                                                </c:otherwise>
                                            </c:choose>
                                            <td style="text-align: center">${visit.reportPath}</td>                                                   
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </c:forEach>
                    </table>
                </c:when>
                <c:otherwise>
                    <label style="color:blue">No any Previous Details</label>
                </c:otherwise>
            </c:choose>
        </div>  
    </div>
    <div class="col-md-12 panel-footer">
        <div class="col-md-8">           
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2">
            <button class="btn btn-default col-md-12" onclick="unblockui()">Cancel</button>
        </div>
    </div>
</div>

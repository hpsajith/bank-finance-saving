<%-- 
    Document   : seizeYardRegisterPswdForm
    Created on : Feb 19, 2016, 3:10:44 PM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<h3>Need Valid Privileges...!!!</h3>
<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="col-md-12 col-lg-12 col-sm-12" style="display:block;">
        <label id="msgPswd" class="msgTextField"></label>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4">Password</div>
            <div class="col-md-8 col-lg-8 col-sm-8"><input type="password" id="txtPswd" style="width: 100%"></div>
        </div> 
        <div class="row">
            <input type="hidden" id="txtYardRegId"/>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>    
            <div class="col-md-4"><input type="button"  onclick="deleteYarRegister()" class="btn btn-success col-md-12 col-sm-12 col-lg-12" value="Delete"/></div>    
        </div>
    </div>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">
    function deleteYarRegister() {
        var yardRegId = $("#txtYardRegId").val();
        var pswd = $("#txtPswd").val().trim();
        if (pswd !== null && pswd !== "") {
            if (yardRegId !== "") {
                $.ajax({
                    url: "/AxaBankFinance/RecoveryController/deleteYardRegister",
                    type: 'GET',
                    data: {"yardRegId": yardRegId, "pswd": pswd},
                    success: function(data) {
                        $('#msgDiv').html("");
                        $('#msgDiv').html(data);
                        ui('#msgDiv');
                        setTimeout(function() {
                            loadSeizeYardRegisterPage();
                        }, 1500);
                    }
                });
            }
        } else {
            $("#msgPswd").text("Enter Password..");
            $("#txtPswd").addClass("txtError");
            $("#txtPswd").focus();
        }
    }
</script>
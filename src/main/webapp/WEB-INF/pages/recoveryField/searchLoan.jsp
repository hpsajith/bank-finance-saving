<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#fieldOfficerTablee").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>
<style>
    /*    #nic_suggesions{border: 1px solid #F0F0F0;background-color:#C8EEFD;margin: 2px 0px;padding:40px;}*/
    #country-list{float:left;list-style:none;margin:0;padding:0;width:190px;}
    #country-list li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;}
    #country-list li:hover{background:#F0F0F0;cursor: pointer}
</style>
<div id="proccessingPage_recover" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..." />
</div>
<div class="row">      
    <div class="col-md-2">
        <h4>${systemDate}</h4>     
    </div>
    <div class="col-md-8"></div>
    <div class="col-md-2">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <div class="refresh_2" value="Refresh" style="width:100%" name="" id="buttnRefresh"> </div>
        </div>
    </div>  
</div>  

<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption"> 
    <div class="row"><label style="margin-left: 20px;">Search Loan</label></div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Due Date</div>
            <div class="col-md-8">
                <input type="text"  class="txtCalendar"  value="${searchByDue}" style="width:100%" name="" id="searchByDue_"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Laps Date</div>
            <div class="col-md-8">
                <input type="text"  class="txtCalendar"  value="" style="width:100%" name="" id="searchByLaps_"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Member/Agree.No</div>
            <div class="col-md-8">
                <input type="text"  value="${agreementNo}" style="width:100%" name="" id="searchByAgr_"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Customer Name</div>
            <div class="col-md-8">
                <input type="text" value="${name}" style="width:100%" name="" id="searchByName_"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">NIC No</div>
            <div class="col-md-8">
                <input type="text" value="${nic}"  style="width:100%" name="" id="searchByNic_" onkeyup="findDebtorNicNo()">
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text" value="${vehicleNo}"  style="width:100%" name="" id="searchByVehicleNo"/>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div id="nic_suggesions" style="display: none;height: 100px;overflow-x: hidden;overflow-y: auto"></div>
        </div>
    </div>
    <div class="col-md-4">
    </div>
</div>
<div class="row" style="border-top: 0px solid #777"></div>
<div class="row" id="recoveSeachLoan">
    <!--loan Details go here-->
</div>

<div id="searcedDueLoans"></div>        
<div id="viewSearchedLoan" class="searchCustomer" style="width: 70%;height: 75%"></div>

<script>
    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selected, evnt) {
                searchByDueDate(selected);
            }

        });
    });

    function selectLoanD(loanId, instmntId, msgDateShifted) {
        if ($('#ins' + loanId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#ins' + loanId).prop('checked', true);
            var searchByDue = $('#searchByDue').val();
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/recoveryProcess',
                data: {"loanID": loanId, "searchByDue": searchByDue, "instmntId": instmntId, "msgDateShifted": msgDateShifted},
                success: function (data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }
    }

    $('#searchByName_').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            searchLoan();
        }
    });

//    $('#searchByNic_').keydown(function(e) {
//        var code = e.keyCode || e.which;
//        if (code === 13) {
//            searchLoan();
//        }
//    });

    $('#searchByAgr_').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            searchLoan();
        }
    });

    $('#searchByDue_').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            searchLoan();
        }
    });
    $('#searchByLaps_').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            searchLoan();
        }
    });
    $('#searchByVehicleNo').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            searchLoan();
        }
    });

    function findDebtorNicNo() {
        var nicNo = $("#searchByNic_").val().trim();
        if (nicNo.length > 0) {
            $.ajax({
                url: "/AxaBankFinance/findDebtorNicNo/" + nicNo,
                type: 'GET',
                success: function (data) {
                    if (data.length > 0) {
                        var nicSugg = "<ul id='country-list'>";
                        for (var i = 0; i < data.length; i++) {
                            nicSugg = nicSugg + '<li onClick="clickNic(\'' + data[i] + '\')">' + data[i] + '</li>';
                        }
                        nicSugg = nicSugg + "</ul>";
                        $("#nic_suggesions").html("");
                        $("#nic_suggesions").html(nicSugg);
                        $("#nic_suggesions").css("display", "block");
                        $("#nic_suggesions").css("background", "#FFF");
                    } else {
                        $("#nic_suggesions").html("");
                        $("#nic_suggesions").css("display", "none");
                    }
                }
            });
        } else {
            $("#nic_suggesions").html("");
            $("#nic_suggesions").css("display", "none");
        }
    }

    function clickNic(nicNo) {
        $("#searchByNic_").val(nicNo);
        $("#nic_suggesions").html("");
        $("#nic_suggesions").css("display", "none");
        searchLoan();
    }

    function searchLoan() {
        var agreementNo = $("#searchByAgr_").val();
        var dueDate = $("#searchByDue_").val();
        var lapsDate = $("#searchByLaps_").val();
        var cusName = $("#searchByName_").val();
        var nicNo = $("#searchByNic_").val();
        var vehicleNo = $("#searchByVehicleNo").val();
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/searchLoanByRecovery?${_csrf.parameterName}=${_csrf.token}',
            data: {agNo: agreementNo, name: cusName, dueDate: dueDate, lapDate: lapsDate, nic: nicNo, vehicleNo: vehicleNo},
            type: 'POST',
            success: function (data) {
                $("#viewSearchedLoan").html(data);
                ui("#viewSearchedLoan");
            },
            error: function () {
                alert("Cannot Connect to the server");
            }
        });
    }
</script>
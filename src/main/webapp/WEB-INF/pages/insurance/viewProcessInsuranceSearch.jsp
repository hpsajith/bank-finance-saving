<%-- 
    Document   : viewProcessInsuranceSearch
    Created on : Aug 24, 2016, 12:15:15 PM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblViewProcessLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "85%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>
<table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblViewProcessLoan"  >
    <thead>
        <tr>
            <th style="text-align: center">#</th>
            <th style="text-align: center">Loan Id</th>
            <th style="text-align: center">Agreement No</th>
            <th style="text-align: center">Policy No</th>
            <th style="text-align: center">Vehicle No</th>
            <th style="text-align: center">Loan</th>
            <th style="text-align: center">Premium</th>
            <th style="text-align: center">Value</th>
            <th style="text-align: center">Debtor</th>
        </tr>
    </thead>
    <tbody> 
        <c:forEach var="insLoans" items="${processLoan}">
            <tr id="${insLoans[1]}">
                <td style="text-align: center">
                    <input type="checkbox" name="loan" class="loanCheckBox" id="ins${insLoans[1]}" onclick="selectLoan(${insLoans[0]},${insLoans[1]},${insLoans[5]})"/>
                </td>
                <td style="text-align: center">${insLoans[1]}</td>
                <td style="text-align: center">${insLoans[2]}</td>
                <td style="text-align: center">${insLoans[3]}</td>
                <td style="text-align: center">${insLoans[4]}</td>
                <td style="text-align: center">${insLoans[5]}</td>
                <td style="text-align: right">${insLoans[6]}</td>
                <td style="text-align: right">${insLoans[7]}</td>
                <td style="text-align: center">${insLoans[8]}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>


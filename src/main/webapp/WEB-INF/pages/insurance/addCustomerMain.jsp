<%-- 
    Document   : addFieldOfficeMain
    Created on : May 18, 2015, 4:57:44 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
         pageAuthentication();
        $("#tblCustomer").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "90%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });
    
</script>

</script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-6" id="divCustomerForm"></div>

    <div class="col-md-1"></div>

    <div class="col-md-5" style="margin:35 0 10 0">
        <table id="tblCustomer" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Nic Number</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="cust" items="${customerList}">
                    <tr id="${cust.insuCustomerId}">
                        <td>${cust.insuInitial}</td>
                        <td>${cust.insuNic}</td>
                        <td><div class="edit" href='#' onclick="clickToEdit(${cust.insuCustomerId})"></div></td>
                    </tr>
                </c:forEach>


            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>

<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadInsuCustomerForm',
            success: function(data) {
                $('#divCustomerForm').html(data);
            }
        });
    });
    function clickToEdit(Id) {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/viewEditOtherCustomer/' + Id,
            success: function(data) {
                $('#divCustomerForm').html("");
                $('#divCustomerForm').html(data);
                document.getElementById("btnSaveOtherCustomer").value = "Update";
            }
        });
    }
</script>

<%-- 
    Document   : viewInsuranceMain
    Created on : Jun 15, 2015, 5:35:22 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
          pageAuthentication();
        $("#tblViewProcessLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>

<div class="row" >
    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblViewProcessLoan"  >
        <thead>
            <tr>
                <th></th>
                <th>Loan Id</th>
                <th>Sub Loan Type</th>
                <th>Loan Amount</th>
                <th>Debtor Name</th>
                <th>Value</th>
                <th>Loan Date</th>
                <th>Insurance Date</th>
                <th>Expiry Date</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="renewInsurance" items="${renewInsu}">
                <tr id="${renewInsurance.insuId}">
                    <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${renewInsurance.insuId}" onclick="selectRenewInsu(${renewInsurance.insuId},${renewInsurance.insuLoanId},${renewInsurance.insuValue})"/></td>
                    <td>${renewInsurance.insuLoanId}</td>
                    <c:forEach var="loanType" items="${subLoanType}">
                        <c:if test="${loanType.subLoanId == renewInsurance.insuLoanType}">
                            <td>${loanType.subLoanName}</td>
                        </c:if>
                    </c:forEach>
                    <c:forEach var="loanHeader" items="${loanDetails}">
                        <c:if test="${loanHeader.loanId == renewInsurance.insuLoanId}">
                            <td>${loanHeader.loanAmount}</td>
                            <td>${loanHeader.debtorHeaderDetails.debtorName}</td>
                        </c:if>
                    </c:forEach>
                    <td>${renewInsurance.insuValue}</td>
                    <c:forEach var="loanHeader" items="${loanDetails}">
                        <c:if test="${loanHeader.loanId == renewInsurance.insuLoanId}">
                            <td>${loanHeader.loanDate}</td>
                        </c:if>
                    </c:forEach>
                    <td>${renewInsurance.actionTime}</td>
                    <td>${renewInsurance.expiryDate}</td>

                </tr>
            </c:forEach>

        </tbody>
    </table>
</div>
<div id="divProcess" class="jobsPopUp" style="width: 45%;height: 60%"></div>
<input type="hidden" id="renew" value="1"/>
<script>
    function selectRenewInsu(insuId,loanId, value) {
        var renew = document.getElementById("renew").value;
        if ($('#ins' + insuId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#ins' + insuId).prop('checked', true);
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/insuranceProcess',
                data: {"loanID": loanId, "loanValue": value,"renewInsu": renew},
                success: function(data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }

    }
</script>
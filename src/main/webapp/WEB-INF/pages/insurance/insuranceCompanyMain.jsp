<%-- 
    Document   : addBranchMain
    Created on : May 8, 2015, 2:17:39 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function() {
          pageAuthentication();
        $("#tblInCompany").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-6" id="divInsuraceForm"></div>
    <div class="col-md-6" style="margin:50 1 10 -1">
        <table id="tblInCompany" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Edit</th>
                    <th>InActive</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="com" items="${companyList}">
                    <c:choose>
                        <c:when test="${com.isActive == false}">
                            <tr id="${com.comId}" style="background-color: #BDBDBD;color: #888">
                                <td>${com.comName}</td>
                                <td><div class="" href='#' ></div></td>
                                <td><div class="btnActive" href='#' id="btnInactive" onclick="clickToAtctive(${com.comId})"></div></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                             <tr id="${com.comId}">
                                <td>${com.comName}</td>
                                <td><div class="edit" href='#' onclick="clickToEdit(${com.comId})"></div></td>
                                <td><div class="delete" href='#' id="btnInactive" onclick="clickToDelete(${com.comId})"></div></td>
                            </tr>
                        </c:otherwise>
                    </c:choose>

                </c:forEach>

            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>
<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/loadInsuranceForm',
            success: function(data) {
                $("#divInsuraceForm").html(data);
            }
        });
    });

    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/editInsuraceCompany/' + id,
            success: function(data) {
                $('#divInsuraceForm').html("");
                $('#divInsuraceForm').html(data);
                document.getElementById("btnSaveCompany").value = "Update";
            }
        });

    }

    function clickToDelete(id) {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/inActiveInsuraceCompany/' + id,
            success: function(data) {
                if (data == true) {
                    loadInsuranceCompanyMain();
                }
            }
        });

    }

    function clickToAtctive(id) {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/activeInsuraceCompany/' + id,
            success: function(data) {
                if (data == true) {
                    loadInsuranceCompanyMain();
                }
            }
        });
    }

</script>

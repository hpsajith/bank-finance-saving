
<%-- 
    Document   : viewInsuranceMain
    Created on : Jun 15, 2015, 5:35:22 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblViewLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "85%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });

</script>   
<div id="divOtherInsu" class="jobsPopUp" style="width: 45%;height: 45%"></div>
<div class="row" >
    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblViewLoan"  >
        <thead>
            <tr>
                <th></th>
                <th>Cover Note No</th>
                <th>Customer Name</th>
                <th>Vehicle No</th>
                <th>Premium</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="insOther" items="${otherInsu}">
                <tr id="${insOther.insuId}">
                    <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${insOther.insuId}" onclick="selectOtherInsu(${insOther.insuId})"/></td>
                    <td>${insOther.insuCoverNoteNo}</td>
                    <c:forEach var="cus" items="${customerList}">
                        <c:if test="${cus.insuCustomerId == insOther.insuDebtorId}">
                            <td>${cus.insuInitial}</td>
                        </c:if>
                    </c:forEach>
                    <td>${insOther.vehicleNo}</td>
                    <td>${insOther.insuTotalPremium}</td>
                    <td>${insOther.actionTime}</td>
                </tr>
            </c:forEach>

        </tbody>
    </table>
</div>
<div id="divProcess" class="jobsPopUp" style="width: 45%;height: 55%"></div>

<script>
    function selectOtherInsu(insuID) {
        if ($('#ins' + insuID).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#ins' + insuID).prop('checked', true);
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/editOtherInsurace/' + insuID,
                success: function(data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }

    }
</script>
<%-- 
    Document   : processInsurance
    Created on : Jun 16, 2015, 4:40:01 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>



<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>


    <form name="processInsuranceForm" id="other_Insurance">
        <div class="row" style="margin: -27 8 2 0"><legend><strong>Other Insurance</strong></legend></div>
        <input type="hidden" name="insuId" value="${otherInsurance.insuId}"/>
        <div class="row" style="margin-bottom:5px">
            <div class="col-md-4">  <label class="labelMessage" id="otherMsg"></label></div>
            <div class="col-md-8"></div>
        </div>
        <div class="row">
            <div class="col-md-3" style="">Cover Note No<label class="redmsg">*</label></div>
            <div class="col-md-8" style="margin-left: -8px" ><input type="text" id="txtCoverNote" value="${otherInsurance.insuCoverNoteNo}" 
                                                                    name="insuCoverNoteNo"  style="width: 80%" />
                <label id="msgCover" class="msgTextField"></label> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-lg-3" style="margin-left: -19px">Branch<label class="redmsg">*</label></div> 
            <div class="col-md-8 col-sm-8 col-lg-8" style="margin-left: 9px">
                <select name="branchId" id="branch" style="width: 80%">
                    <c:forEach var="v_branch" items="${branches}">
                        <c:choose>
                            <c:when test="${otherInsurance.branchId == v_branch.branchId}">
                                <option value="${v_branch.branchId}" selected="">${v_branch.branchName}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${v_branch.branchId}">${v_branch.branchName}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <label id="msgBranch" class="msgTextField"></label>

            </div>
            <div class="col-md-1 col-sm-1 col-lg-1 "></div> 
        </div>

        <div class="row">
            <div class="col-md-3" style="margin-left: 4px">Customer Name<label class="redmsg">*</label></div>
            <div class="col-md-8" style="margin-left: -12px" >
                <c:choose>
                    <c:when test="${otherInsurance.insuId != null}">
                        <select name="insuDebtorId" id="customer"  style="width: 80%">
                            <option value="0">--SELECT--</option>
                            <c:forEach var="e_customer" items="${otherCustomer}">
                                <c:if test="${otherInsurance.insuDebtorId == e_customer.insuCustomerId}">
                                    <option value="${otherInsurance.insuDebtorId}" selected="">${e_customer.insuInitial}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </c:when>
                    <c:otherwise>
                        <select name="insuDebtorId" id="customer" style="width: 80%">
                            <option value="0">--SELECT--</option>
                            <c:forEach var="v_customer" items="${customerList}">
                                <option value="${v_customer.insuCustomerId}">${v_customer.insuInitial}</option>
                            </c:forEach>
                        </select>
                    </c:otherwise>
                </c:choose>
                <label id="msgCustomer" class="msgTextField"></label> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-3" style="margin-left: -12px" >Company <label class="redmsg">*</label></div>
            <div class="col-md-8" style="margin-left: 3px" >
                <select name ="insuCompanyId" id="txtCompany" style="width: 80%">
                    <option value="0">SELECT</option>
                    <c:forEach var="insCom" items="${companyList}">
                        <option value="${insCom.comId}">${insCom.comName}</option>
                    </c:forEach>
                </select>
                <label id="msgCompany" class="msgTextField"></label> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-3" style="margin-left: -9px">Vehicle No<label class="redmsg">*</label></div>
            <div class="col-md-8" style="" ><input type="text" id="txtVehicalNo" 
                                                   name="vehicleNo" value="${otherInsurance.vehicleNo}" style="width: 80%" />
                <label id="msgVehicle" class="msgTextField"></label> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-3" style="margin-left: -13px">Premium <label class="redmsg">*</label></div>
            <div class="col-md-8" style="margin-left: 5px" ><input type="text" id="txtPrimium" 
                                                                   name="insuTotalPremium" value="${otherInsurance.insuTotalPremium}" onkeypress="return checkNumbers(this)" style="width: 80%" />
                <label id="msgPremium" class="msgTextField"></label> 
            </div>
        </div>
        <input type="hidden" name="otherInsu" value="1" id="other"/>
        <div class="row" style="margin-top: 10px;margin-bottom: 10px;margin-left: 172px">
             <div  class="col-md-1 "></div>
            <div  class="col-md-4 "><input type="button" name="" value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"/></div>
            <div  class="col-md-4 "><input type="button" name="" id="btnSaveOtherInsu" value="Process" onclick="processOtherInsurance()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
            <div  class="col-md-3 "></div>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
    </form>

</div>
<script>
    function processOtherInsurance() {
        if (fieldValidate()) {
            var other = document.getElementById("other").value;
            document.getElementById("btnSaveOtherInsu").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/saveInsurance/' + other,
                type: 'POST',
                data: $('#other_Insurance').serialize(),
                success: function(data) {
                    if (data != null) {
                        $('#otherMsg').text("Process Successfully..!");
                    }
                    setTimeout(function() {
                        otherInsurance();
                    }, 1000);

                },
                error: function() {
                    alert("Error Loding..");
                }
            });
        }

    }

    function fieldValidate() {
        var coverNoteNo = document.getElementById('txtCoverNote');
        var customerName = $("#customer").find('option:selected').val();
        var vehicleNo = document.getElementById('txtVehicalNo');
        var premium = document.getElementById('txtPrimium');

        if (coverNoteNo.value == "" || coverNoteNo.value == null) {
            $("#msgCover").html("Cover Note No must be filled out");
            $("#txtFname").addClass("txtError");
            $("#txtFname").focus();
            return false;
        } else {
            $("#txtFname").removeClass("txtError");
            $("#msgCover").html("");
            $("#msgCover").removeClass("msgTextField");
        }
        if (customerName == 0) {
            $("#msgCustomer").html("Select Customer Name");
            $("#customer").addClass("txtError");
            $("#customer").focus();
            return false;
        } else {
            $("#customer").removeClass("txtError");
            $("#msgCustomer").html("");
            $("#msgCustomer").removeClass("msgTextField");
        }
        if (vehicleNo.value == "" || vehicleNo.value == null) {
            $("#msgVehicle").html("Vehicle Registration No must be filled out");
            $("#txtVehicalNo").addClass("txtError");
            $("#txtVehicalNo").focus();
            return false;
        } else {
            $("#txtVehicalNo").removeClass("txtError");
            $("#msgVehicle").html("");
            $("#msgVehicle").removeClass("msgTextField");
        }
        if (premium.value == "" || premium.value == null) {
            $("#msgPremium").html("Premium No must be filled out");
            $("#txtPrimium").addClass("txtError");
            $("#txtPrimium").focus();
            return false;
        } else {
            $("#txtPrimium").removeClass("txtError");
            $("#msgPremium").html("");
            $("#msgPremium").removeClass("msgTextField");
        }
        return true;

    }
</script>
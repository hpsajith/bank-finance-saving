<%-- 
    Document   : viewInsuranceMain
    Created on : Jun 15, 2015, 5:35:22 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblInsuRegistry").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "75%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>
<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption"> 
    <div class="row"><label style="margin-left: 20px;">Search Insurance Registry</label></div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-4">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtSearchAgreementNo" onkeyup="findInsuranceByAgreementNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Policy No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtSearchPolicyNo" onkeyup="findInsuranceByPolicyNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtSearchVehicleNo" onkeyup="findInsuranceByVehicleNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Debtor Name</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtDebtorName" onkeyup="findInsuranceByDebtorName()"/>
            </div>
        </div>
    </div>
</div>
<div class="row" id="divinsuranceRegSearch">
    <table class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblInsuRegistry">
        <thead>
            <tr>
                <th style="text-align: center">#</th>
                <th style="text-align: center">Agreement No</th>
                <th style="text-align: center">Policy No</th>
                <th style="text-align: center">Vehicle No</th>
                <th style="text-align: center">Loan</th>
                <th style="text-align: center">Premium</th>
                <th style="text-align: center">Value</th>
                <th style="text-align: center">Debtor</th>
                <th style="text-align: center">#</th>
                <th style="text-align: center">#</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="insLoans" items="${registedLoans}">
                <c:if test="${insLoans[9] == 3}">
                    <tr id="${insLoans[0]}" style="background-color: #99CCFF">
                    </c:if>
                    <td id="${insLoans[1]}" style="text-align: center"><input type="checkbox" name="loan" class="loanCheckBox" id="ins${insLoans[0]}"/></td>
                    <td style="text-align: center">${insLoans[2]}</td>
                    <td style="text-align: center">${insLoans[3]}</td>
                    <td style="text-align: center">${insLoans[4]}</td>
                    <td style="text-align: right">${insLoans[5]}</td>
                    <td style="text-align: right">${insLoans[6]}</td>
                    <td style="text-align: right">${insLoans[7]}</td>
                    <td style="text-align: center">${insLoans[8]}</td>
                    <td style="text-align: center">
                        <c:choose>
                            <c:when test="${insLoans[9] == 3}">
                                <a href="#" onclick="loadInsuCommissionForm(${insLoans[0]})" title="Add Commission">Add</a>
                            </c:when>
                            <c:otherwise>
                                <a href="#" style="cursor: not-allowed">Add</a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td style="text-align: center">
                        <c:choose>
                            <c:when test="${insLoans[10]}">
                                <a href="#" onclick="" title="Approve Commission">Approve</a>
                            </c:when>
                            <c:otherwise>
                                <a href="#" style="cursor: not-allowed">Approve</a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div class="row" style="margin-top: 5px">
    <div class="col-md-3">
        <input type="button" value="Generate Voucher" onclick="generateVoucher()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/>
    </div>
    <div class="col-md-5"></div>
    <div class="col-md-4"></div>
</div>
<form method="GET" action="/AxaBankFinance/ReportController/printInsuranceVoucher" target="blank" id="formInsuVoucher">
    <input type="hidden" id="hidden_array" name="hiddenArray" >
</form>  
<div id="divDiliver" class="jobsPopUp" style="width: 30%;height: 30%"></div>
<div id="divProcess" class="jobsPopUp box" style="width: 45%"></div>
<script>
//    function selectLoan(insuId) {
////        alert(insuId);
//        if ($('#ins' + insuId).is(':checked')) {
//            $('.loanCheckBox').prop('checked', false);
//            $('#ins' + insuId).prop('checked', true);
//            $.ajax({
//                url: '/AxaBankFinance/InsuraceController/insuranceCardIsDeliver',
//                data: {"insuID": insuId},
//                success: function(data) {
//                    $('#divDiliver').html("");
//                    $('#divDiliver').html(data);
//                    ui('#divDiliver');
//                }
//            });
//        }
//    }

    var loanIds = [];
    function getLoanIds() {
        loanIds = [];
        var trs = $("#tblInsuRegistry tbody").find("tr");
        for (var i = 0; i < trs.length; i++) {
            var tr = trs[i];
            var tds = tr.children;
            if (tds.length > 0) {
                var td = tds[0];
                if (td.children[0].checked) {
                    var loanId = td.id;
                    loanIds.push(loanId);
                }
            }
        }
    }

    function generateVoucher() {
        getLoanIds();
        if (loanIds.length > 0) {
            //console.log(loanIds);
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/generateInsuranceVoucher/" + loanIds,
                success: function (data) {
                    if (data) {
                        loanInsuranceRegistry();
                        $("#hidden_array").val(loanIds);
                        $("#formInsuVoucher").submit();
                    }
                }
            });
        }
    }

    function loadInsuCommissionForm(insuId) {
        if (insuId !== null) {
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/processInsuranceCommission/' + insuId,
                success: function (data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }
    }

    function findInsuranceByAgreementNo() {
        var agreementNo = $("#txtSearchAgreementNo").val().trim();
        if (agreementNo.length >= 9) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByAgreementNo",
                data: {"agreementNo": agreementNo, "status": "2"},
                success: function (data) {
                    $("#divinsuranceRegSearch").html("");
                    $("#divinsuranceRegSearch").html(data);
                }
            });
        }
    }

    function findInsuranceByPolicyNo() {
        var policyNo = $("#txtSearchPolicyNo").val().trim();
        if (policyNo.length >= 5) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByPolicyNo",
                data: {"policyNo": policyNo, "status": "2"},
                success: function (data) {
                    $("#divinsuranceRegSearch").html("");
                    $("#divinsuranceRegSearch").html(data);
                }
            });
        }
    }

    function findInsuranceByVehicleNo() {
        var vehNo = $("#txtSearchVehicleNo").val().trim();
        if (vehNo.length >= 4) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByVehicleNo",
                data: {"vehicleNo": vehNo, "status": "2"},
                success: function (data) {
                    $("#divinsuranceRegSearch").html("");
                    $("#divinsuranceRegSearch").html(data);
                }
            });
        }
    }

    function findInsuranceByDebtorName() {
        var debName = $("#txtDebtorName").val().trim();
        if (debName.length >= 4) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByDebtorName",
                data: {"debtorName": debName, "status": "2"},
                success: function (data) {
                    $("#divinsuranceRegSearch").html("");
                    $("#divinsuranceRegSearch").html(data);
                }
            });
        }
    }

</script>
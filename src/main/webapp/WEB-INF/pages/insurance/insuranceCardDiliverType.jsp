
<%-- 
    Document   : viewInsuranceMain
    Created on : Jun 15, 2015, 5:35:22 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });

</script>

<div class="container-fluid" id="divDiliver" style="border:1px solid #BDBDBD; border-radius: 2px;display: block">
    <div class="row"><legend><strong>Insurance Card Issue</strong></legend></div>
    <form name="formDiliver" id="diliver">
        <input type="hidden" id="insu_Id" value="${insu_ID}"
               <div class="row">
            <c:choose>
                <c:when test="${diliverType == 1}">

                    <div class="col-md-1"><input type="radio" name="check" checked="true" value="1"/></div>
                    <div class="col-md-3" style="padding: 1">Register Post</div>
                    <div class="col-md-1"><input type="radio" name="check" value="2"/></div>
                    <div class="col-md-3" style="padding: 1">Hand Over</div>
                    <div class="col-md-4"></div>
                </c:when>
                <c:when test="${diliverType == 2}">
                    <div class="col-md-1"><input type="radio" name="check" value="1"/></div>
                    <div class="col-md-3" style="padding: 1">Register Post</div>
                    <div class="col-md-1"><input type="radio" name="check" checked="true" value="2"/></div>
                    <div class="col-md-3" style="padding: 1">Hand Over</div>
                    <div class="col-md-4"></div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-1"><input type="radio" name="check" value="1"/></div>
                    <div class="col-md-3" style="padding: 1">Register Post</div>
                    <div class="col-md-1"><input type="radio" name="check" value="2"/></div>
                    <div class="col-md-3" style="padding: 1">Hand Over</div>
                    <div class="col-md-4"></div>
                </c:otherwise>
            </c:choose>
            <div class="row" style="margin-top: 5px"></div>

            <div class="row" style="margin-top: 10px">
                <div  class="col-md-5 "></div>
                <div  class="col-md-3 "><input type="button" name="" id="" value="Deliver" onclick="saveDeliver()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                <div  class="col-md-3 "><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 " onclick="unblockui()"/></div>
                <div  class="col-md-1 "></div>
            </div>

    </form>
</div>

<script>
    var diliverType = 0;
    $(function() {
        $("input:radio[name=check]").click(function() {
            diliverType = $(this).val();
        });
    });

    function saveDeliver() {
        var insuID = document.getElementById("insu_Id").value;

        $.ajax({
            url: '/AxaBankFinance/InsuraceController/saveDiliverType/',
            data: {"insu_ID": insuID, "diliver": diliverType},
            success: function(data) {
                if (data) {
                    unblockui();
                    loanInsuranceRegistry();
                }
            }
        });
    }
</script>



<%-- 
    Document   : addFieldOfficerForm
    Created on : May 18, 2015, 4:58:41 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function() {
        $("#tblOfficer").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<form name="addInsuCustomerForm" id="addInsuCustomer">
    <div class="row"><legend><strong>Add Insurance Customer</strong></legend></div>
    <input type="hidden" name="insuCustomerId" value="${otherCustomer.insuCustomerId}"/>
    <input type="hidden" name="insuCustAccountNo" value="${otherCustomer.insuCustAccountNo}"/>
    <div class="row">
        <div class="col-md-4">Full Name <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtFname" name="insuFullName" value="${otherCustomer.insuFullName}" style="width: 100%"/>
            <label id="msgFname" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Name With Initial <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtLname" name="insuInitial" value="${otherCustomer.insuInitial}"style="width: 100%"/>
            <label id="msgLname" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">Address <label class="redmsg">*</label></div>
        <div class="col-md-8"><textarea type="text" id="txtAddress" name="insuAddress" style="width: 100%">${otherCustomer.insuAddress}</textarea>
            <label id="msgAddress" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Contact Mobile <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtMobileNo" name="insuMobile" onblur="checkTelNumbers(this)"
                                     onkeypress="return checkNumbers(this)" maxlength="10" value="${otherCustomer.insuMobile}"style="width: 100%"/>
            <label id="msgMobile" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Contact Home</div>
        <div class="col-md-8"><input type="text" name="insuHome" value="${otherCustomer.insuHome}" 
                                     onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 100%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Email</div>
        <div class="col-md-8"><input type="text" name="insuEmail" onblur="validateEmail(this, '#msgEmail1')" value="${otherCustomer.insuEmail}"style="width: 100%"/>
            <label id="msgEmail1"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Sex</div>
        <div class="col-md-8">
            <div class="row"> 
                <c:choose>
                    <c:when test="${otherCustomer.insuGender == 0}">
                        <div class="col-md-1"><input type="radio" checked="checked" name="insuGender" value="0"/></div>
                        <div class="col-md-3">Male</div>
                        <div class="col-md-1"><input type="radio" name="insuGender" value="1"/></div>
                        <div class="col-md-3">Female</div>
                    </c:when>                    
                    <c:when test="${otherCustomer.insuGender == 1}">
                        <div class="col-md-1"><input type="radio" name="insuGender" value="0"/></div>
                        <div class="col-md-3">Male</div>
                        <div class="col-md-1"><input type="radio" checked="checked" name="insuGender" value="1"/></div>
                        <div class="col-md-3">Female</div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-1"><input type="radio" name="insuGender" value="0"/></div>
                        <div class="col-md-3">Male</div>
                        <div class="col-md-1"><input type="radio" name="insuGender" value="1"/></div>
                        <div class="col-md-3">Female</div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">Nic Number <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtNicNo" name="insuNic" onblur="nictoLowerCase(this)" maxlength="10" 
                                     value="${otherCustomer.insuNic}"style="width: 100%"/>
            <label id="msgNic" class="msgTextField"></label>
        </div>
    </div>



    <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
        <div  class="col-md-5 col-sm-5 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveOtherCustomer" value="Save" onclick="saveInsuCustomer()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>

    </div>

    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <div id="hideOfficer" style="display: none"></div>
</form>
<script>
    function saveInsuCustomer() {
        if (fieldValidate()) {
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/saveOrUpdateCustomer',
                type: 'POST',
                data: $('#addInsuCustomer').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    loadInsuCustomerMain();
                },
                error: function() {
                    alert("Error Loding..");
                }

            });
        }
    }

    function fieldValidate() {
        var fName = document.getElementById("txtFname").value;
        var lName = document.getElementById("txtLname").value;
        var address = document.getElementById("txtAddress").value;
        var mobile = document.getElementById("txtMobileNo").value;
        var nic = document.getElementById("txtNicNo").value;

        if (fName == null || fName == "") {
            $("#msgFname").html("First Name must be filled out");
            $("#txtFname").addClass("txtError");
            $("#txtFname").focus();
            return false;
        } else {
            $("#txtFname").removeClass("txtError");
            $("#msgFname").html("");
            $("#msgFname").removeClass("msgTextField");
        }
        if (lName == null || lName == "") {
            $("#msgLname").html("Last Name must be filled out");
            $("#txtLname").addClass("txtError");
            $("#txtLname").focus();
            return false;
        } else {
            $("#txtLname").removeClass("txtError");
            $("#msgLname").html("");
            $("#msgLname").removeClass("msgTextField");
        }
        if (address == null || address == "") {
            $("#msgAddress").html("Address must be filled out");
            $("#txtAddress").addClass("txtError");
            $("#txtAddress").focus();
            return false;
        } else {
            $("#txtAddress").removeClass("txtError");
            $("#msgAddress").html("");
            $("#msgAddress").removeClass("msgTextField");
        }
        if (mobile == null || mobile == "") {
            $("#msgMobile").html("Mobile No must be filled out");
            $("#txtMobileNo").addClass("txtError");
            $("#txtMobileNo").focus();
            return false;
        } else {
            $("#txtMobileNo").removeClass("txtError");
            $("#msgMobile").html("");
            $("#msgMobile").removeClass("msgTextField");
        }
        if (nic == null || nic == "") {
            $("#msgNic").html("Nic Number must be filled out");
            $("#txtNicNo").addClass("txtError");
            $("#txtNicNo").focus();
            return false;
        } else {
            $("#txtNicNo").removeClass("txtError");
            $("#msgNic").html("");
            $("#msgNic").removeClass("msgTextField");
        }
        return true;
    }

</script>
<%-- 
    Document   : addBranchForm
    Created on : May 8, 2015, 2:17:55 PM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form name="insuraceCompanyForm" id="insuraceCom">
    <div class="row" style="margin: 5 8 2 0"><legend>Add New Company</legend></div>
    <input type="hidden" name="comId" id="txtBranchId" value="${company.comId}"/>
    <div class="row">
        <div class="col-md-4">Company Name <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtComName" name="comName" value="${company.comName}" style="width: 80%"/>
            <label id="msgComName" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Company Last Name</div>
        <div class="col-md-8"><input type="text" id="" name="comLname"  value="${company.comLname}" style="width: 80%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Address <label class="redmsg">*</label></div>
        <div class="col-md-8"><textarea type="text" id="txtAddress" name="comAddress" style="width: 80%">${company.comAddress}</textarea>
            <label id="msgAddress" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Contact Mobile <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtMobileNo" name="comMobileNo" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" value="${company.comMobileNo}"style="width: 80%"/>
            <label id="msgMobile"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Office No <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtTeleNo" name="comTelephone" value="${company.comTelephone}" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/>
            <label id="msgOffice"></label>.
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Fax No</div>
        <div class="col-md-8"><input type="text" name="comFaxNo"  value="${company.comFaxNo}"style="width: 80%"/>
            <label id="msgFaxNo"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Email <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtEmail" name="comEmailNo" onblur="validateEmail(this, '#msgEmail2')" value="${company.comEmailNo}" style="width: 80%"/>
            <label id="msgEmail2"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Web Address </div>
        <div class="col-md-8"><input type="text" id="txtWebAddress" name="comWebAddresss" onblur="nictoLowerCase(this)" value="${company.comWebAddresss}"style="width: 80%"/>
            <label id="msgWeb" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">BAP Rate </div>
        <div class="col-md-8"><input type="text" id="txtBccRate" name="bccRate"  maxlength="10"  value="${company.bccRate}" style="width: 80%"/>
            <label id="msgBccRate" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">SRC Rate </div>
        <div class="col-md-8"><input type="text" id="txtSrRate" name="srRate"  maxlength="10" value="${company.srRate}" style="width: 80%"/>
            <label id="msgSrRate" class="msgTextField"></label>
        </div>
    </div>

    <div class="row" style="margin-top: 5px">
        <div  class="col-md-4"></div>
        <div  class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3"><input type="button" name="" value="Save"id="btnSaveCompany" onclick="saveInsuraceCompany()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
        <div  class="col-md-2"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<script>
    function saveInsuraceCompany() {
        if (fieldValidate()) {
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/saveOrUpdateInsuranceCom',
                type: 'POST',
                data: $('#insuraceCom').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadInsuranceCompanyMain();
                    }, 2000);
                },
                error: function() {
                    alert("Error Loding..");
                }

            });
        }

    }

    function fieldValidate() {
        var comName = document.getElementById('txtComName');
        var address = document.getElementById('txtAddress');
        var mobilNo = document.getElementById('txtMobileNo');
        var TelNo = document.getElementById('txtTeleNo');
        var email = document.getElementById('txtEmail');

        if (comName.value == "" || comName == null) {
            $("#msgComName").html("Company Name must be filled out");
            $("#txtComName").addClass("txtError");
            $("#txtComName").focus();
            return false;
        } else {
            $("#txtComName").removeClass("txtError");
            $("#msgComName").html("");
            $("#msgComName").removeClass("msgTextField");
        }
        if (address.value == "" || address == null) {
            $("#msgAddress").html("Address must be filled out");
            $("#txtAddress").addClass("txtError");
            $("#txtAddress").focus();
            return false;
        } else {
            $("#txtAddress").removeClass("txtError");
            $("#msgAddress").html("");
            $("#msgAddress").removeClass("msgTextField");
        }
        if (mobilNo.value == "" || mobilNo == null) {
            $("#msgMobile").html("Mobile No must be filled out");
            $("#txtMobileNo").addClass("txtError");
            $("#txtMobileNo").focus();
            return false;
        } else {
            $("#txtMobileNo").removeClass("txtError");
            $("#msgMobile").html("");
            $("#msgMobile").removeClass("msgTextField");
        }
        if (TelNo.value == "" || TelNo == null) {
            $("#msgOffice").html("Office  No must be filled out");
            $("#txtTeleNo").addClass("txtError");
            $("#txtTeleNo").focus();
            return false;
        } else {
            $("#txtTeleNo").removeClass("txtError");
            $("#msgOffice").html("");
            $("#msgOffice").removeClass("msgTextField");
        }
        if (email.value == "" || email == null) {
            $("#msgEmail2").html("Email Address No must be filled out");
            $("#txtEmail").addClass("txtError");
            $("#txtEmail").focus();
            return false;
        } else {
            $("#txtEmail").removeClass("txtError");
            $("#msgEmail2").html("");
            $("#msgEmail2").removeClass("msgTextField");
        }
        return true;
    }
</script>
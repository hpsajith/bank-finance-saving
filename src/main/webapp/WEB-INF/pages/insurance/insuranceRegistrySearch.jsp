<%-- 
    Document   : insuranceRegistrySearch
    Created on : Aug 24, 2016, 12:15:34 PM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblInsuRegistry").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "75%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>
<table class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblInsuRegistry">
    <thead>
        <tr>
            <th style="text-align: center">#</th>
            <th style="text-align: center">Agreement No</th>
            <th style="text-align: center">Policy No</th>
            <th style="text-align: center">Vehicle No</th>
            <th style="text-align: center">Loan</th>
            <th style="text-align: center">Premium</th>
            <th style="text-align: center">Value</th>
            <th style="text-align: center">Debtor</th>
            <th style="text-align: center">#</th>
            <th style="text-align: center">#</th>
        </tr>
    </thead>
    <tbody> 
        <c:forEach var="insLoans" items="${registedLoans}">
            <c:if test="${insLoans[9] == 3}">
                <tr id="${insLoans[0]}" style="background-color: #99CCFF">
                </c:if>
                <td id="${insLoans[1]}" style="text-align: center"><input type="checkbox" name="loan" class="loanCheckBox" id="ins${insLoans[0]}"/></td>
                <td style="text-align: center">${insLoans[2]}</td>
                <td style="text-align: center">${insLoans[3]}</td>
                <td style="text-align: center">${insLoans[4]}</td>
                <td style="text-align: right">${insLoans[5]}</td>
                <td style="text-align: right">${insLoans[6]}</td>
                <td style="text-align: right">${insLoans[7]}</td>
                <td style="text-align: center">${insLoans[8]}</td>
                <td style="text-align: center">
                    <c:choose>
                        <c:when test="${insLoans[9] == 3}">
                            <a href="#" onclick="loadInsuCommissionForm(${insLoans[0]})" title="Add Commission">Add</a>
                        </c:when>
                        <c:otherwise>
                            <a href="#" style="cursor: not-allowed">Add</a>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td style="text-align: center">
                    <c:choose>
                        <c:when test="${insLoans[10]}">
                            <a href="#" onclick="" title="Approve Commission">Approve</a>
                        </c:when>
                        <c:otherwise>
                            <a href="#" style="cursor: not-allowed">Approve</a>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>

<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/15/2016
  Time: 1:11 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>
    <form name="processInsuranceForm" id="processIns">
        <div class="row" style="margin: -27 8 2 0">
            <legend><strong>Add Insurance Debit Amount</strong></legend>
        </div>
        <%--<input type="hidden" name="insuId" id="txtInsuID" value="${insuDetails.insuId}"/>--%>
        <input type="hidden" name="insuLoanId" value="${loanDetails.loanId}"/>
        <input type="hidden" name="insuValue" value="${insuDetails.insuValue}"/>
        <input type="hidden" name="insuCommissionRecDate" id="txtInsuCommissionRecDate"
               value="${insuDetails.insuCommissionRecDate}"/>
        <input type="hidden" name="insuCommissionCheqNo" id="txtInsuCommissionCheqNo"
               value="${insuDetails.insuCommissionCheqNo}"/>
        <input type="hidden" name="insuCommissionRecAmount" id="txtInsuCommissionRecAmount"
               value="${insuDetails.insuCommissionRecAmount}"/>
        <input type="hidden" name="insuCommissionDiffer" id="txtInsuCommissionDiffer"
               value="${insuDetails.insuCommissionDiffer}"/>
        <input type="hidden" name="isCommRec" id="txtIsCommRec" value="${insuDetails.isCommRec}"/>
        <input type="hidden" name="insuStatus" id="txtInsuStatus" value="${insuDetails.insuStatus}"/>
        <input type="hidden" id="isUpdate" value="1"/>
        <input type="hidden" id="Is_DayEnd" value="${IsDayEnd}"/>
        <input type="hidden" name="renewCount" value="${insuDetails.renewCount}"/>
        <input type="hidden" id="renewInsurance" value="2"/>
        <label id="lblInsuUpadate" class="redmsg" style="margin-right: 70%"></label>
        <c:choose>
            <c:when test="${insuDetails.isOtherCompany == false}">
                <div class="row" style="margin-bottom:  5px">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"><input type="checkbox" value="1" name="isOtherCompany"
                                                 onclick="clickOtherCompany()" id="otherCompany"/></div>
                    <div class="col-md-4" style="text-align: left">Other Insurance Company</div>
                    <div class="col-md-1" style=""></div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row" style="margin-bottom:  5px">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"><input type="checkbox" checked="" value="1" name="isOtherCompany"
                                                 onclick="clickOtherCompany()" id="otherCompany"/></div>
                    <div class="col-md-4" style="text-align: left">Other Insurance Company</div>
                    <div class="col-md-1"></div>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Branch <label class="redmsg">*</label></div>
            <div class="col-md-8">
                <select name="branchId" id="branch" style="width: 100%">
                    <c:forEach var="v_branch" items="${branches}">
                        <c:if test="${v_branch.branchId == insuDetails.branchId}">
                            <option value="${v_branch.branchId}" selected="">${v_branch.branchName}</option>
                        </c:if>
                        <option value="${v_branch.branchId}">${v_branch.branchName}</option>
                    </c:forEach>
                </select>
                <label id="msgBranch" class="msgTextField"></label>
            </div>
        </div>
        <div class="row" id="otherCompanyRow">
            <div class="col-md-4" style="text-align: left">Company Name<label class="redmsg">*</label></div>
            <div class="col-md-8"><input type="text" id="txtOtherCompany" name="otherComName"
                                         value="${insuDetails.otherComName}" style="width: 100%"/>
                <label id="msgOtherCom" class="msgTextField"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Cover Note No</div>
            <div class="col-md-8"><input type="text" name="insuCoverNoteNo"
                                         value="${insuDetails.insuCoverNoteNo}" style="width: 100%"/></div>
        </div>
        <c:if test="${insuDetails.insuDebtorId == loanDetails.debtorHeaderDetails.debtorId}">
            <div class="row">
                <div class="col-md-4" style="text-align: left">Customer Name</div>
                <div class="col-md-8"><input type="text"
                                             value="${loanDetails.debtorHeaderDetails.nameWithInitial}"
                                             style="width: 100%" readonly=""/></div>
                <input type="hidden" name="insuDebtorId" value="${insuDetails.insuDebtorId}"/>
            </div>
        </c:if>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Loan Type</div>
            <c:if test="${insuDetails.insuLoanType == loanType.subLoanId}">
                <div class="col-md-8"><input type="text" value="${loanType.subLoanName}" style="width: 100%"
                                             readonly=""/></div>
                <input type="hidden" name="insuLoanType" value="${loanType.subLoanId}"/>
            </c:if>
        </div>
        <c:choose>
            <c:when test="${loanDetails.loanIsIssue == 0}">
                <div class="row">
                    <div class="col-md-4" style="text-align: left">Company <label class="redmsg">*</label></div>
                    <div class="col-md-8">
                        <select name="insuCompanyId" id="txtCompany" style="width: 100%"
                                onchange="findInsuranceCompany()">
                            <c:forEach var="insCom" items="${companyList}">
                                <c:choose>
                                    <c:when test="${insuDetails.insuCompanyId == insCom.comId}">
                                        <option value="${insCom.comId}"
                                                selected="true">${insCom.comName}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${insCom.comId}">${insCom.comName}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                        <label id="msgCompany" class="msgTextField"></label>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <div class="col-md-4" style="text-align: left">Company <label class="redmsg">*</label></div>
                    <div class="col-md-8">
                        <select name="insuCompanyId" id="txtCompany" style="width: 100%">
                            <c:forEach var="insCom" items="${companyList}">
                                <c:if test="${insuDetails.insuCompanyId == insCom.comId}">
                                    <option value="${insCom.comId}" selected="true">${insCom.comName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        <label id="msgCompany" class="msgTextField"></label>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">Sum Insured</div>
            <div class="col-md-8"><input type="text" name="insuValue" value="${insuDetails.insuValue}"
                                         style="width: 100%" readonly/>
            </div>
        </div>
        <c:choose>
            <c:when test="${loanDetails.loanIsIssue == 0}">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">Basic Premium <label
                            class="redmsg">*</label></div>
                    <div class="col-md-8"><input type="text" id="txtBasicPremium" name="insuBasicPremium"
                                                 value="${insuDetails.insuBasicPremium}"
                                                 onblur="calCommission()" style="width: 100%"/>
                        <label id="msgBasicPremium" class="msgTextField"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">SRCC <label
                            class="redmsg">*</label>
                    </div>
                    <div class="col-md-8"><input type="text" id="txtSrcc" name="insuSrcc"
                                                 value="${insuDetails.insuSrcc}" style="width: 100%"
                                                 onblur="calCommission()"/>
                        <label id="msgSrcc" class="msgTextField"></label>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">Basic Premium <label
                            class="redmsg">*</label></div>
                    <div class="col-md-8"><input type="text" id="txtBasicPremium" name="insuBasicPremium"
                                                 value="${insuDetails.insuBasicPremium}" style="width: 100%"
                                                 readonly/>
                        <label id="msgBasicPremium" class="msgTextField"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">SRCC <label
                            class="redmsg">*</label>
                    </div>
                    <div class="col-md-8"><input type="text" id="txtSrcc" name="insuSrcc"
                                                 value="${insuDetails.insuSrcc}" style="width: 100%" readonly/>
                        <label id="msgSrcc" class="msgTextField"></label>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${loanDetails.loanIsIssue == 0}">
                <div class="row">
                    <div class="col-md-4" style="text-align: left">Premium <label class="redmsg">*</label></div>
                    <div class="col-md-8"><input type="text" id="txtPremium"
                                                 onkeypress="return checkNumbers(this)"
                                                 onkeyup="calTotalPremium()"
                                                 onblur="calTotalPremium()" name="insuPremium"
                                                 value="0.00" style="width: 100%"/>
                        <label id="msgPremium" class="msgTextField"></label>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <div class="col-md-4" style="text-align: left">Premium <label class="redmsg">*</label></div>
                    <div class="col-md-8"><input type="text" id="txtPremium"
                                                 onkeypress="return checkNumbers(this)" name="insuPremium"
                                                 onkeyup="calTotalPremium()"
                                                 value="0.00" style="width: 100%"
                    />
                        <label id="msgPremium" class="msgTextField"></label>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Commission</div>
            <div class="col-md-8"><input type="text" id="txtOtherCharge" name="insuOtherCharge"
                                         value="${insuDetails.insuOtherCharge}"
                                         onkeypress="return checkNumbers(this)" style="width: 100%" readonly/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Total Premium</div>
            <div class="col-md-8"><input type="text" id="txtTotalPremium" name="insuTotalPremium"
                                         value="0.00" style="width: 100%"
                                         required/></div>
        </div>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Grace Period</div>
            <div class="col-md-1"><input type="checkbox" id="chqGracePeriod" checked=""/></div>
            <div class="col-md-7">
                <input type="text" class="txtCalendar1" id="txtinsuGracePeriod" name="insuGracePeriod"
                       value="${insuDetails.insuGracePeriod}" style="width: 100%"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Expiry Date</div>
            <div class="col-md-8"><input type="text" class="txtCalendar" id="txtExpiryDate" name="expiryDate"
                                         value="${insuDetails.expiryDate}" readonly="" style="width: 100%"/>
                <label id="msgExpDate" class="msgTextField"></label>
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
            <c:choose>
                <c:when test="${byLoanSearch}">
                    <div class="col-md-4 "></div>
                    <div class="col-md-4 "></div>
                    <div class="col-md-4 "><input type="button" name="" value="Cancel"
                                                  class="btn btn-default col-md-12 col-sm-12 col-lg-12"
                                                  onclick="unblockui()"/></div>
                </c:when>
                <c:otherwise>
                    <!-- <div class="col-md-4 "><input type="button" name="" value="Add To Payment"
                    class="btn btn-default col-md-12 "
                    onclick="registedInsurance(${insuDetails.insuId})"/></div>
                    -->
                    <div class="col-md-4 "></div>
                    <div class="col-md-4 "><input type="button" name="" id="btnSaveinsurance" value="Process"
                                                  onclick="processInsurance()"
                                                  class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                    <div class="col-md-4 "><input type="button" name="" value="Cancel"
                                                  class="btn btn-default col-md-12 col-sm-12 col-lg-12"
                                                  onclick="unblockui()"/></div>
                </c:otherwise>
            </c:choose>
        </div>
        <input type="hidden" name="otherInsu" value="0" id="other"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>

    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>
<script type="text/javascript">
    $(function () {

        var dateToday = $("#dayEndDate").val();
        var dates = $(".txtCalendar1").datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: "+1w",
            changeYear: true,
            minDate: dateToday,
            showButtonPanel: true,
            onSelect: function (selectedDate) {
                var option = this.id == "from" ? "minDate" : "maxDate",
                        instance = $(this).data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
        clickTo();
    });
    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
        document.getElementById("otherCompanyRow").hidden = true;
        var myDate = new Date();
        var year = myDate.getFullYear();
        var day = myDate.getDate();
        var month = myDate.getMonth();
        myDate.setYear(year + 1);
        myDate.setDate(day - 1);
        myDate.setMonth(month + 1);
        var exDay = myDate.getFullYear() + "-" + myDate.getMonth() + "-" + myDate.getDate();
        $('#txtExpiryDate').val(exDay);
    });

    function clickTo() {
        if ($('#chqGracePeriod').prop("checked")) {
            document.getElementById('txtinsuGracePeriod').disabled = true;
        } else {
            document.getElementById('txtinsuGracePeriod').disabled = false;
        }
    }

    var bpRate = 0;
    var srccRate = 0;

    function findInsuranceCompany() {
        var companyId = $("#txtCompany").find("option:selected").val();
        if (companyId !== null || companyId !== "0") {
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/findInsuranceCompany/' + companyId,
                success: function (data) {
                    if (data !== null) {
                        bpRate = data.bccRate;
                        srccRate = data.srRate;
                        $("#btnSaveinsurance").removeAttr("disabled");
                    }
                }
            });
        } else {
            $("#btnSaveinsurance").attr("disabled", true);
        }
    }

    function calTotalPremium() {
        var premium = parseFloat($("#txtPremium").val()).toFixed(2);
        var total = (parseFloat(premium)).toFixed(2);

        if (total !== "") {
           $('#txtTotalPremium').val(total);
        }
    }

    function calCommission() {
        var basicPremium = parseFloat($("#txtBasicPremium").val()).toFixed(2);
        var srcc = parseFloat($("#txtSrcc").val()).toFixed(2);
        var bpComm = (basicPremium * bpRate) / 100;
        var srccComm = (srcc * srccRate) / 100;
        var totComm = (parseFloat(bpComm) + parseFloat(srccComm)).toFixed(2);
        $("#txtOtherCharge").val(totComm);
    }


    function processInsurance() {
        var isUpdate = document.getElementById('isUpdate').value;
        var other = document.getElementById("other").value;
        if (fieldValidate()) {
            document.getElementById("btnSaveinsurance").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/saveInsuranceDebitProcess/' + other,
                type: 'POST',
                data: $('#processIns').serialize(),
                success: function (data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function () {
                        var renew = document.getElementById('renewInsurance').value;
                        if (isUpdate.value == 0 || renew == 0) {
                            loadAddInsuranceMain();
                        } else {
                            //loadAddInsuranceMain();
                            //loadviewProcessInsurance();
                        }
                    }, 2000);
                },
                error: function () {
                    alert("Error Loding..");
                }
            });
        }
    }

    function registedInsurance(insuId) {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/registedInsurance/' + insuId,
            success: function (data) {
                if (data) {
                    unblockui();
                    loadviewProcessInsurance();
                }
            }

        });
    }

    function clickOtherCompany() {
        if ($("#otherCompany").prop("checked")) {
            $("#txtOtherCompany").focus();
            $("#txtPremium").prop('disabled', true);
            $("#txtCompany").prop('disabled', true);
            $("#txtOtherCharge").prop('disabled', true);
            $("#txtExpiryDate").addClass('txtError');
            $("#txtOtherCompany").addClass('txtError');
            document.getElementById("otherCompanyRow").hidden = false;
        } else {
            document.getElementById("otherCompanyRow").hidden = true;
            $("#txtPremium").prop('disabled', false);
            $("#txtCompany").prop('disabled', false);
            $("#txtOtherCharge").prop('disabled', false);
            $("#txtExpiryDate").removeClass('txtError');
            $("#txtOtherCompany").removeClass('txtError');
        }
    }

    function fieldValidate() {

        var expDate = document.getElementById('txtExpiryDate');
        var premium = document.getElementById('txtPremium');
        var insuCompany = $("#txtCompany").find('option:selected').val();
        var branches = $("#branch").find('option:selected').val();
        if ($("#otherCompany").prop("checked")) {
            var otherCompanyName = document.getElementById('txtOtherCompany');
            if (otherCompanyName.value == "" || otherCompanyName == null) {
                $("#msgOtherCom").html("Company Name must be filled out");
                $("#txtOtherCompany").addClass("txtError");
                $("#txtOtherCompany").focus();
                return false;
            } else {
                $("#txtOtherCompany").removeClass("txtError");
                $("#msgOtherCom").html("");
                $("#msgOtherCom").removeClass("msgTextField");
            }
            return true;
        } else {
            if (branches == 0) {
                $("#msgBranch").html("Select Insurance Company");
                $("#branch").addClass("txtError");
                $("#branch").focus();
                return false;
            } else {
                $("#branch").removeClass("txtError");
                $("#msgBranch").html("");
                $("#msgBranch").removeClass("msgTextField");
            }
            if (insuCompany == 0) {
                $("#msgCompany").html("Select Insurance Company");
                $("#txtCompany").addClass("txtError");
                $("#txtCompany").focus();
                return false;
            } else {
                $("#txtCompany").removeClass("txtError");
                $("#msgCompany").html("");
                $("#msgCompany").removeClass("msgTextField");
            }
            if (premium.value == "" || premium.value == null) {
                $("#msgPremium").html("Primium must be filled out");
                $("#txtPremium").addClass("txtError");
                $("#txtPremium").focus();
                return false;
            } else {
                $("#txtPremium").removeClass("txtError");
                $("#msgPremium").html("");
                $("#msgPremium").removeClass("msgTextField");
            }
            if (expDate.value == "" || expDate.value == null) {
                $("#msgExpDate").html("Expiry Date must be filled out");
                $("#txtExpiryDate").addClass("txtError");
                $("#txtExpiryDate").focus();
                return false;
            } else {
                $("#txtExpiryDate").removeClass("txtError");
                $("#msgExpDate").html("");
                $("#msgExpDate").removeClass("msgTextField");
            }
            return true;
        }


    }


</script>



<%-- 
    Document   : processInsuranceCommission
    Created on : Aug 4, 2016, 10:50:34 AM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>
    <form name="processInsuranceForm" id="processIns">
        <div class="row" style="margin: -27 8 2 0"><legend><strong>Add Insurance Commission</strong></legend></div>
        <input type="hidden" name="insuId" value="${insuDetails.insuId}" />
        <input type="hidden" name="insuCode" value="${insuDetails.insuCode}" />
        <input type="hidden" name="insuLoanId" value="${insuDetails.insuLoanId}" />
        <input type="hidden" name="insuCoverNoteNo" value="${insuDetails.insuCoverNoteNo}" />
        <input type="hidden" name="insuValue" value="${insuDetails.insuValue}" />
        <input type="hidden" name="insuDebtorId" value="${insuDetails.insuDebtorId}" />
        <input type="hidden" name="insuLoanType" value="${insuDetails.insuLoanType}" />
        <input type="hidden" name="insuCompanyId" value="${insuDetails.insuCompanyId}" />
        <input type="hidden" name="insuBasicPremium" value="${insuDetails.insuBasicPremium}" />
        <input type="hidden" name="insuSrcc" value="${insuDetails.insuSrcc}" />
        <input type="hidden" name="insuPremium" value="${insuDetails.insuPremium}" />
        <input type="hidden" name="insuTotalPremium" value="${insuDetails.insuTotalPremium}" />
        <input type="hidden" name="insuGracePeriod" value="${insuDetails.insuGracePeriod}" />
        <input type="hidden" name="isOtherInsurance" value="${insuDetails.isOtherInsurance}" />
        <input type="hidden" name="isOtherCompany" value="${insuDetails.isOtherCompany}" />
        <input type="hidden" name="vehicleNo" value="${insuDetails.vehicleNo}" />
        <input type="hidden" name="isRegisted" value="${insuDetails.isRegisted}" />
        <input type="hidden" name="isDiliver" value="${insuDetails.isDiliver}" />
        <input type="hidden" name="insuStatus" value="${insuDetails.insuStatus}" />
        <input type="hidden" name="registedDate" value="${insuDetails.registedDate}" />
        <input type="hidden" name="otherComName" value="${insuDetails.otherComName}" />
        <input type="hidden" name="expiryDate" value="${insuDetails.expiryDate}" />
        <input type="hidden" name="isPay" value="${insuDetails.isPay}" />
        <input type="hidden" name="branchId" value="${insuDetails.branchId}" />
        <input type="hidden" name="renewCount" value="${insuDetails.renewCount}"/>
        <c:choose>
            <c:when test="${insuDetails.isCommRec}">
                <div class="row" style="margin-bottom:  5px">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"><input type="checkbox" value="1" name="isCommRec" checked/></div>
                    <div class="col-md-4" style="text-align: left">Total Commission Received</div>
                    <div class="col-md-1" ></div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row" style="margin-bottom:  5px">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"><input type="checkbox" value="1" name="isCommRec"/></div>
                    <div class="col-md-4" style="text-align: left">Total Commission Received</div>
                    <div class="col-md-1"></div>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">Commission</div>
            <div class="col-md-8" ><input type="text" id="txtCommission" name="insuOtherCharge" value="${insuDetails.insuOtherCharge}" style="width: 100%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">Receive Amount</div>
            <div class="col-md-8" ><input type="text" id="txtRecAmount" name="insuCommissionRecAmount" onblur="calculateDifference()" onkeyup="checkRecCommission()" value="${insuDetails.insuCommissionRecAmount}" style="width: 100%" /></div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">Difference</div>
            <div class="col-md-8" ><input type="text" id="txtDiffer" name="insuCommissionDiffer" value="${insuDetails.insuCommissionDiffer}" style="width: 100%" /></div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: left">Cheque No</div>
            <div class="col-md-8" ><input type="text" id="txtCheqNo" name="insuCommissionCheqNo" value="${insuDetails.insuCommissionCheqNo}" style="width: 100%" /></div>
        </div>
        <div class="row">
            <div class="col-md-4" style="text-align: left">Receive Date</div>
            <div class="col-md-8"><input type="text" class="txtCalendar" id="txtRecDate" name="insuCommissionRecDate" value="${insuDetails.insuCommissionRecDate}" style="width: 100%" />
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
            <div  class="col-md-4 "></div>
            <div  class="col-md-4 "><input type="button" name="" id="btnSaveinsurance" value="Save" onclick="addInsuCommision()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
            <div  class="col-md-4 "><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>                            
        </div>
        <input type="hidden" name="otherInsu" value="0" id="other"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
    </form>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>
<script type="text/javascript">

    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });

    function addInsuCommision() {
        var other = document.getElementById("other").value;
        document.getElementById("btnSaveinsurance").disabled = true;
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/saveInsuCommission/' + other,
            type: 'POST',
            data: $('#processIns').serialize(),
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function () {
                    loanInsuranceRegistry();
                }, 1500);
            },
            error: function () {
                alert("Error Loding..");
            }
        });

    }

    function calculateDifference() {
        var commission = parseFloat($("#txtCommission").val()).toFixed(2);
        var recCommission = parseFloat($("#txtRecAmount").val()).toFixed(2);
        var difference = (parseFloat(commission) - parseFloat(recCommission)).toFixed(2);
        $('#txtDiffer').val(difference);
    }

    function checkRecCommission() {
        var commission = $("#txtCommission").val();
        var recCommission = $("#txtRecAmount").val();
        if (parseFloat(recCommission) > parseFloat(commission)) {
            $("#txtRecAmount").val("");
            $('#txtDiffer').val("");
        }
    }

</script>

<%--
  Created by IntelliJ IDEA.
  User: Sahan Ekanayake
  Date: 9/15/2016
  Time: 11:03 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblViewProcessLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "85%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>
<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption">
    <div class="row"><label style="margin-left: 20px;">Insurance Debit Search</label></div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-4">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtSearchAgreementNo" onkeyup="findInsuranceByAgreementNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Policy No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtSearchPolicyNo" onkeyup="findInsuranceByPolicyNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtSearchVehicleNo" onkeyup="findInsuranceByVehicleNo()"/>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-4">
            <div class="col-md-4">Debtor Name</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" id="txtDebtorName" onkeyup="findInsuranceByDebtorName()"/>
            </div>
        </div>
    </div>
</div>
<div class="row" id="divInsuranceSearch">
    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0"
           id="tblViewProcessLoan">
        <thead>
        <tr>
            <th style="text-align: center">#</th>
            <th style="text-align: center">Loan Id</th>
            <th style="text-align: center">Agreement No</th>
            <th style="text-align: center">Policy No</th>
            <th style="text-align: center">Vehicle No</th>
            <th style="text-align: center">Loan</th>
            <th style="text-align: center">Premium</th>
            <th style="text-align: center">Value</th>
            <th style="text-align: center">Debtor</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="insLoans" items="${processLoan}">
            <tr id="${insLoans[1]}">
                <td style="text-align: center">
                    <input type="checkbox" name="loan" class="loanCheckBox" id="ins${insLoans[1]}"
                           onclick="selectLoan(${insLoans[0]},${insLoans[1]},${insLoans[5]})"/>
                </td>
                <td style="text-align: center">${insLoans[1]}</td>
                <td style="text-align: center">${insLoans[2]}</td>
                <td style="text-align: center">${insLoans[3]}</td>
                <td style="text-align: center">${insLoans[4]}</td>
                <td style="text-align: center">${insLoans[5]}</td>
                <td style="text-align: right">${insLoans[6]}</td>
                <td style="text-align: right">${insLoans[7]}</td>
                <td style="text-align: center">${insLoans[8]}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<div id="divProcess" class="jobsPopUp box" style="width: 45%"></div>
<script>

   function selectLoan(insuId, loanId, value) {
        if ($('#ins' + loanId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#ins' + loanId).prop('checked', true);
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/insuranceDebitProcess',
                data: {"insuID": insuId, "loanID": loanId, "loanValue": value},
                success: function (data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }
    }

    function findInsuranceByAgreementNo() {
        var agreementNo = $("#txtSearchAgreementNo").val().trim();
        if (agreementNo.length >= 9) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByAgreementNo",
                data: {"agreementNo": agreementNo, "status": "1"},
                success: function (data) {
                    $("#divInsuranceSearch").html("");
                    $("#divInsuranceSearch").html(data);
                }
            });
        }
    }

    function findInsuranceByPolicyNo() {
        var policyNo = $("#txtSearchPolicyNo").val().trim();
        if (policyNo.length >= 5) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByPolicyNo",
                data: {"policyNo": policyNo, "status": "1"},
                success: function (data) {
                    $("#divInsuranceSearch").html("");
                    $("#divInsuranceSearch").html(data);
                }
            });
        }
    }

    function findInsuranceByVehicleNo() {
        var vehNo = $("#txtSearchVehicleNo").val().trim();
        if (vehNo.length >= 4) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByVehicleNo",
                data: {"vehicleNo": vehNo, "status": "1"},
                success: function (data) {
                    $("#divInsuranceSearch").html("");
                    $("#divInsuranceSearch").html(data);
                }
            });
        }
    }

    function findInsuranceByDebtorName() {
        var debName = $("#txtDebtorName").val().trim();
        if (debName.length >= 4) {
            $.ajax({
                url: "/AxaBankFinance/InsuraceController/findInsuranceByDebtorName",
                data: {"debtorName": debName, "status": "1"},
                success: function (data) {
                    $("#divInsuranceSearch").html("");
                    $("#divInsuranceSearch").html(data);
                }
            });
        }
    }
</script>

<%-- 
    Document   : viewInsuranceMain
    Created on : Jun 15, 2015, 5:35:22 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblViewLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "60%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
        $(".box").draggable();
    });

</script>
<div class="row" style="background: #F2F7FC">
    <div class="col-md-12">
        <div class="row" style="padding: 5px;">
            <div class="col-md-3"><label>Search Loan</label></div>
            <div class="col-md-9"></div>
        </div>
        <div class="row" style="margin-top: 10px; padding: 5">
            <div class="col-md-5">
                <div class="col-md-4">Agreement No</div>
                <div class="col-md-7">
                    <input type="text" style="width:100%" name="" id="searchByAgreeNo"/>
                </div>
                <div class="col-md-1" style="padding: 1">
                    <input type="button" class="btn btn-default searchButton" id="btnSearchAgreeNo" onclick="searchByAgreeNo()" style="height: 25px">
                </div>

            </div>
            <div class="col-md-7">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <input type="button" class="btn btn-default col-md-12" id="" value="Other Insurance(Third-Party)" onclick="otherInsurance()"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divOtherInsu" class="jobsPopUp box" style="width: 45%;height: 55%"></div>
<div class="row" >
    <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblViewLoan"  >
        <thead>
            <tr>
                <th></th>
                <th>Loan Id</th>
                <th>Agreement No</th>
                <th>Loan Amount</th>
                <th>Value</th>
                <th>Debtor Name</th>
                <th>Loan Date</th>
            </tr>
        </thead>
        <tbody> 
            <c:forEach var="insLoans" items="${viewLoan}">
                <tr id="${insLoans[0]}">
                    <td><input type="checkbox" name="loan" class="loanCheckBox" id="ins${insLoans[0]}" onclick="selectLoan(${insLoans[0]},${insLoans[5]})"/></td>
                    <td>${insLoans[0]}</td>
                    <td>${insLoans[1]}</td>
                    <td>${insLoans[2]}</td>
                    <td>${insLoans[5]}</td>
                    <td>${insLoans[3]}</td>
                    <td>${insLoans[4]}</td>
                </tr>
            </c:forEach>

        </tbody>
    </table>
</div>
<div id="divProcess" class="jobsPopUp box" style="width: 45%"></div>

<script>
    function selectLoan(loanId, value) {
        if ($('#ins' + loanId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#ins' + loanId).prop('checked', true);
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/insuranceProcess',
                data: {"loanID": loanId, "loanValue": value},
                success: function(data) {
                    $('#divProcess').html("");
                    $('#divProcess').html(data);
                    ui('#divProcess');
                }
            });
        }

    }
    function otherInsurance() {
        $.ajax({
            url: '/AxaBankFinance/InsuraceController/popUpOtherInsurance',
            success: function(data) {
                $('#divOtherInsu').html("");
                $('#divOtherInsu').html(data);
                ui('#divOtherInsu');
            }
        });
    }
    function searchByAgreeNo() {
        var agree = $('#searchByAgreeNo').val();
        if (agree != null) {
            $.ajax({
                url: '/AxaBankFinance/InsuraceController/searchInsuranceByAgreementNo',
                data: {"agreeNo": agree},
                success: function(data) {
                    $('#tblViewLoan tbody').remove();
                    $('#formContent').html(data);
                },
                error: function() {
                    alert("LS and HP Loans Not Found");
                }
            });
        } else {
            alert("Please Enter Valid Agreement No.");
        }
    }
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row">
    <div class="col-md-11" style="border:1px solid #C8C8C8; border-radius: 2px; margin: 20px; ">
        <form name="employementLoanForm" id="formEmployement" autocomplete="off">
            <legend>Employeement Form</legend>
            <input type="hidden" name="debtorId" value="${emp.debtorId}" id="e_did"/>
            <input type="hidden" name="companyId" value="${emp.companyId}"/>
            <div class="row">
                <div class="col-md-4">Company Name</div>
                <div class="col-md-8"><input type="text" name="companyName" id="companyName_id" value="${emp.companyName}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Company Address</div>
                <div class="col-md-8"><textarea name="companyAddress" id="companyAddress_id" style="width: 100%">${emp.companyAddress}</textarea></div>
            </div>
            <div class="row">
                <div class="col-md-4">Telephone No</div>
                <div class="col-md-8"><input type="text" name="companyTelephone" value="${emp.companyTelephone}" style="width: 100%" maxlength="10" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Designation</div>
                <div class="col-md-8"><input type="text" name="designation" value="${emp.designation}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Working Period</div>
                <div class="col-md-8"><input type="text" name="employementPeriod" value="${emp.employementPeriod}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Gross Income</div>
                <div class="col-md-8"><input type="text" name="grossIncome" value="${emp.grossIncome}" style="width: 100%" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Net Income</div>
                <div class="col-md-8"><input type="text" name="netIncome" value="${emp.netIncome}" style="width: 100%" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-12">
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                        <!--<input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unLoadForm($('#e_did').val())"/>-->
                        <input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unLoadForm($('#e_did').val())"/>
                    </div>
                    <div class="col-md-3">
                        <input type="button" class="btn btn-default col-md-12" value="Save" onclick="saveUpdateEmployement()"/>
                    </div>
                </div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>
</div>

<script>
    function saveUpdateEmployement() {
        
        var ar = [document.getElementById('companyName_id'), document.getElementById('companyAddress_id')];
        if (validateFields(ar) === true) {
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateEmployement',
                type: 'POST',
                data: $("#formEmployement").serialize(),
                success: function(data) {
                    alert(data);
                    var dId = $('#e_did').val();
                    unLoadForm(dId);
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        }
    }
 
</script>


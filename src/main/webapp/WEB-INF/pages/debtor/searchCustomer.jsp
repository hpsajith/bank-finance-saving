<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="container-fluid" id="searchCustomerContent" style="border:1px solid #BDBDBD;  border-radius: 2px;">
    <input type="hidden" id="gurant_debtr_type">
    <div class="row" style="background: #F2F7FC">
        <div class="col-md-12">
            <div class="row" style="padding: 10px;">
                <div class="col-md-3"><label id="labelTitl"></label></div>
                <div class="col-md-9"></div>
            </div>
            <div class="row" style="margin-top: 10px; padding: 10">
                <div class="col-md-5">
                    <div class="col-md-4">Member No</div>
                    <div class="col-md-8">
                        <input type="text" style="width: 100%" name="" id="searchById" onchange="memberNumber()"/>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="col-md-4">Customer Name</div>
                    <div class="col-md-8">
                        <input type="text" style="width:100%" name="" id="searchByName"/>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 0px; padding: 0">


                <div class="col-md-5">
                    <div class="col-md-4">NIC No</div>
                    <div class="col-md-8">
                        <input type="text" style="width:100%" name="" id="searchByNic"/>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row" id="searchedCustomer" style="height: 30%; overflow: auto;">                
    </div>
    <div class="row" id="searchedLoans" style="height: 45%; overflow: auto;">                
    </div>

    <div class="" id="searchButton">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="row" style="margin-bottom: 10px;margin-top: 10px">
                <div class="col-md-3">
                    <input type="button" class="btn btn-default col-md-12" id="btnSearchCustomerExit" value="Exit" onclick="unblockui()"/>
                </div>
                <div class="col-md-3">
                    <input type="button" class="btn btn-default col-md-12" onclick="clearForm()" value="Clear"/> 
                </div>
                <div class="col-md-3">
                    <input type="button" class="btn btn-default col-md-12" onclick="loadNewCustomer()" value="New" disabled="true"/>
                </div>
                <div class="col-md-3">
                    <input type="button" class="btn btn-default col-md-12" id="btnAdd" onclick="selectCustomer()" value="Add"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var name, nic, address, telephone, dId = "", accNo, memberNo;
    $(function () {
        var type = $("#gurant_debtr_type").val();
        if (type == 1) {
//            Customer
            $("#labelTitl").text("Search Customer");
        } else {
//            Gurantor
            $("#labelTitl").text("Search Guarantor");
        }

        $('#searchByNic').keyup(function (e) {
            var nicNo = $(this).val();
            if (nicNo.length == 10) {
                $.ajax({
                    url: '/AxaBankFinance/searchByNic/' + nicNo,
                    success: function (data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function () {
                        alert("Error Loading...");
                    }
                });
            }
        });

        $('#searchByName').keyup(function (e) {
            var name = $(this).val();
            if (name.length > 0) {
                $.ajax({
                    url: '/AxaBankFinance/searchByName/' + name,
                    success: function (data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function () {
                        alert("Error Loading...");
                    }
                });
            }
        });

    });

    $('#searchById').keyup(function (e) {
        var memNo = $(this).val();
        if (memNo.length > 4) {
            $.ajax({
                url: '/AxaBankFinance/SettlementController/searchByMemNo',
                data: {"memNo": memNo},
                success: function (data) {
                    $('#searchedCustomer').html(data);
                },
                error() {
                    alert("Error loading....");
                }
            });
        }


    });

    function loadNewCustomer() {
        $.ajax({
            url: '/AxaBankFinance/newCustomer',
            success: function (data) {
                $('#searchCustomerContent').html('');
                $('#searchCustomerContent').append(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }
    function clearForm() {
        $('#newFormContent').html('');
        $('#searchedCustomer').html('');
        $('#searchedLoans').html('');
    }

    function selectCustomer() {

        var guarantor_debtor_type = $("#gurant_debtr_type").val();
        var type = "";
        if (guarantor_debtor_type == 1)
            type = "Debtor";
        else
            type = "Guarantor";

        if (dId != null && dId != "") {
            document.getElementById("btnAdd").disabled = true;
            unloadSearchCustomer();
            if (guarantor_debtor_type == 1) {
                $("#debID").val(dId);
                $("#debtorNic").val(nic);
                $("#debtorName").val(name);
                $("#debtorAddress").val(address);
                $("#debtorTelephone").val(telephone);
                $("#txtAccountNo").val(accNo);
                $("#loanBookNoTxt").val(memberNo);
                var url2 = "/AxaBankFinance/viewProfilePicture/" + dId;
                $("#customerProfilePicture").html("<img src='" + url2 + "' ></img>");
            } else {
                $("#guarantorName").val(name);
                $("#gurantorNic").val(nic);
                var table = document.getElementById("guranterTable");
                var rowCount = table.rows.length;
                $('#guranterTable tbody').append('<tr id="' + dId + '"><td>' +
                        rowCount + '</td><td>' + name + '</td><td>' + nic + '</td><td>' +
                        address + '</td><td>' + telephone + '</td><td>' +
                        '<div onclick="" class="edit"></div><div class="delete" onclick="deleteRow(' + dId + ')" ></div>' + '</td></tr>');
                $("#guarantorIDs").append("<input type='hidden' name='guarantorID' id='g_hid" + dId + "' value='" + dId + "'>");

            }
        } else
            alert("Select " + type);

    }

    function editCustomer(dId) {
        var debtorId = dId;
        $.ajax({
            url: '/AxaBankFinance/editDebtorFrom/' + debtorId,
            success: function (data) {
                ui('#popup_searchCustomer');
                $('#searchCustomerContent').html('');
                $('#searchCustomerContent').append(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }
    function unLoadForm(dId) {
//        unblockui();        
        editCustomer(dId);
    }


</script>

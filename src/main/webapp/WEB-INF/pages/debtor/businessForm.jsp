<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row">
    <div class="col-md-11" style="border:1px solid #C8C8C8; border-radius: 2px; margin: 20px; ">
        <form name="businessLoanForm" id="formBusiness" autocomplete="off">
            <legend>Business Form</legend>
            <input type="hidden" name="debtorId" value="${bus.debtorId}" id="b_did"/>
            <input type="hidden" name="businessId" value="${bus.businessId}"/>
            <div class="row">
                <div class="col-md-4">Business Name</div>
                <div class="col-md-8"><input type="text" name="businessName" id="businessName_id" value="${bus.businessName}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Address</div>
                <div class="col-md-8"><textarea name="businessAddress" id="businessAddress_id" style="width: 100%">${bus.businessAddress}</textarea></div>
            </div>
            <div class="row">
                <div class="col-md-4">Telephone No</div>
                <div class="col-md-8"><input type="text" name="businessTelephone" value="${bus.businessTelephone}" style="width: 100%" maxlength="10" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Nature of Business</div>
                <div class="col-md-8"><input type="text" name="businessNature" value="${bus.businessNature}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Income</div>
                <div class="col-md-8"><input type="text" name="businessIncome" value="${bus.businessIncome}" style="width: 100%" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Other Income</div>
                <div class="col-md-8"><input type="text" name="otherIncome" value="${bus.otherIncome}" style="width: 100%" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-12">
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                        <!--<input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unLoadForm($('#b_did').val())"/>-->
                        <input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unLoadForm($('#b_did').val())"/>
                    </div>
                    <div class="col-md-3">
                        <input type="button" class="btn btn-default col-md-12" value="Save" onclick="saveUpdateBusiness()"/>
                    </div>
                </div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>
</div>


<script>
    function saveUpdateBusiness() {
        
        var ar = [document.getElementById('businessName_id'), document.getElementById('businessAddress_id')];
        if (validateFields(ar) === true) {
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateBusiness',
                type: 'POST',
                data: $("#formBusiness").serialize(),
                success: function(data) {
                    alert(data);
                    var dId = $('#b_did').val();
                    unLoadForm(dId);
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        }
    }

</script>



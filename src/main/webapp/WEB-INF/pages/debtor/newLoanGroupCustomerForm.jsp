<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script>
    $(document).ready(function () {
        pageAuthentication();
        $(".newTable").chromatable({
//            width: "auto", // specify 100%, auto, or a fixed pixel amount
            height: "46%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
//        $(".box").draggable();
    });

</script>
<div class="row divError"></div>
<div class="row divSuccess">${saveUpdate}</div>
<form name="loanForm" id="newLoanForm">

    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;" id="form_">

        <div class="col-md-12 col-lg-12 col-sm-12">
            <input type="hidden" value="${edit}" id="testEdit"/>

            <div class="row" ><label>Add Group customers</label></div>

            <div class="  container-fluid">
                <br/>
                <div class="col-md-1" style="font-weight: bold">Branch</div>
                <div class="col-md-3">

                    <select name="branchID" style="width: 172px" id="branchID" >
                        <option value="0"  >-select branch-</option>
                        <c:forEach var="branch" items="${branchList}">
                            <c:choose>
                                <c:when test="${branch.branchId == group.branchID}">
                                    <option selected="selected" value="${branch.branchId}">${branch.branchName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${branch.branchId}">${branch.branchName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>

                    <label id="msgBranchId" class="msgTextField"></label>
                </div>

                <div class="col-md-1" style="font-weight: bold">Center</div>
                <div class="col-md-3">
                    <div id="loadingImg1"><img src="/AxaBankFinance/resources/img/loading_small.gif" alt="loading..."></div>
                    <div id="centerDropDown">----</div>

                    <label id="msgCenterId" class="msgTextField"></label>
                </div>

                <div class="col-md-1" style="font-weight: bold">Group</div>
                <div class="col-md-3">

                    <div id="loadingImg2"><img src="/AxaBankFinance/resources/img/loading_small.gif" alt="loading..."></div>
                    <div id="groupDropDown">----</div>

                    <label id="msgCenterId" class="msgTextField"></label>
                </div>
            </div>
            <hr/>

            <!--//Add Group customers-->
            <div class="row" style="background: #F2F2F2; padding-top: 10px;">



                <div class="row" style="margin-left: 12px; font-weight: bold">Group Customer Details</div>






                <div class="row">
                    <div class="col-md-10 col-lg-10 col-sm-10">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-sm-4" style="margin-left:15px; font-weight: 600">NIC</div>
                                    <div class="col-md-7 col-lg-7 col-sm-7" style="margin-left:-12px; padding-left: 1;"><input type="text" style="width: 100%;"  id="gurantorNic"/></div>
                                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-7 col-sm-7">
                                <div class="row">
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Name</div>
                                    <div class="col-md-8 col-lg-8 col-sm-8" style="margin-right: -25px"><input type="text" style="width: 100%" id="guarantorName" /></div>
                                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" class="btn btn-default searchButton" id="btnLoadGuarantor" style="height: 25px"></div>
                                    <!--                                <div class="col-md-1 col-lg-1 col-sm-1"></div>-->
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        <!--<div class="row" style="border:1px solid #BDBDBD; height: 12%; border-radius: 2px;">IMAGE</div>-->
                    </div>
                </div>
                <div class="row" style="overflow-y: auto; margin-left: 20px;">
                    <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 1">
                        <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="guranterTable">

                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th style="width: 30%">Name</th>
                                    <th style="width: 10%">NIC</th>
                                    <th style="width: 35%">Address</th>
                                    <th style="width: 10%">Telephone</th>
                                    <th style="width: 10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="gurantor" items="${gurantors}">
                                    <tr id="${gurantor.debtId}">
                                        <td>${gurantor.debtOrder}</td>
                                        <td>${gurantor.debtName}</td>
                                        <td>${gurantor.debtNic}</td>
                                        <td>${gurantor.debtAddress}</td>
                                        <td>${gurantor.debtTp}</td>
                                        <td><div class="delete" onclick="deleteRow(${gurantor.debtId})" ></div></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" id="guarantorIDs">
                    <c:forEach var="gurantor" items="${gurantors}">
                        <input type="hidden" id="g_hid${gurantor.debtId}" name="guarantorID" value="${gurantor.debtId}" />
                    </c:forEach>
                </div>
            </div>



            <c:choose>
                <c:when test="${edit}">
                    <div class="row panel-footer" style="padding-top: 10px;">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" value="Cancel"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"> <input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" onclick="uploadDocuments()" value="Upload Documents"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" id="btnUpdate"onclick="saveLoanForm()" value="Update"/></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-8"> <input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" id="btnProcess" onclick="processLoanToApproval()" value="Process" style="width: 100px"/></div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row panel-footer" style="padding-top: 10px;">
                        <div class="col-md-6 col-lg-6 col-sm-6"></div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4"></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" id="btnSave" onclick="saveLoanForm()" value="Save"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" value="Clear"/></div>
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </div>
                </c:otherwise>
            </c:choose>

            <input type="hidden" id="gurant_debtr_type" />
            <div class="row" id="hiddenContent"></div>
            <div class="row" id="divCapitalizeTypes"></div>
            <input type="hidden" value="${loan.loanAmount}" style="width: 100%" id="loanAmountId2"/>
            <input type="hidden" name="loanRescheduleId" value="${loan.loanRescheduleId}" style="width: 100%"/>
        </div>
    </div>
</form>
</div>

<!--Sub Loan Types-->
<div class="searchCustomer box" style="width: 35%; left:40%; top:20%" id="popup_subLoanTypes">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row"  style="padding: 20px">
        <table id="tblsubtypes" class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header">
            <thead><th>Loan Type</th></thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<!--Search Customer-->
<div class="searchCustomer box" style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto; left: 15%" id="popup_searchCustomer">

</div>
<!--Add Other Property -->
<div class="searchCustomer" style="height: 70%; width: 70%; overflow-y: auto; top:10%; left:15%" id="popup_addProperty">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label>Add Other Property</label></div>
    <div class="row" style="padding: 10px;">
        <div class="col-md-4 col-lg-4 col-sm-4">Property Type</div>
        <div class="col-md-8 col-lg-8 col-sm-8">
            <select id="slctProperty" style="width: 90%">
                <option value="0">--SELECT--</option>
                <option value="1">Vehicle</option>
                <option value="3">Land</option>
                <option value="4">Other</option>
            </select>
        </div>
    </div>
    <div class="row" style="margin:15 0 0 0; height: 60%;width:100%" id="formPropertyContent">

    </div>
</div>
<!--Search supplier,sales Manager, recovery manager-->
<div class="searchCustomer" style="margin-top: 5%; margin-left: 15%;" id="popup_searchOfficers">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label id="app_title"></label></div>
    <div class="row" style="margin: 10px;">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    Search By Name
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <input type="text" name="" class="col-md-12 col-lg-12 col-sm-12" style="padding: 1"/>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1"></div>
            </div>
        </div>
    </div>

    <div class="row col-md-10" style="margin:20px; height: 80%; overflow-y: auto">
        <table class="table table-bordered table-edit table-hover table-responsive" id="tblOfficers" style="font-size: 12px;">
            <thead>
                <tr>
                    <td>Emp No</td>
                    <td>Name</td>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="row" >
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4"></div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" onclick="addOfficerToLoanForm();"><span class="glyphicon glyphicon-ok-circle"></span> Add</button>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" id="btnSearchUseCancel" onclick="cancelOfficersTable();"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                </div>
            </div>
        </div>
        <input type="hidden" id="hidOfficerType"/>
    </div>
</div>
<div id="message_newLonForm" class="searchCustomer box" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="docDivCheck" class="searchCustomer box" style="width: 85%;border: 0px solid #5e5e5e;margin-left:-15%"></div>
<div class="searchCustomer box" id="editRate_div" style="width: 40%; margin-left: 10%; margin-top: 10%; border:4px solid #1992d1"></div>
<input type="hidden" value="${message}" id="testMessage"/>
<div id="divLoanCapitalizeForm" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..." />
</div>
<!--BlockUI-->
<script type="text/javascript">

    var loanCapitalizeDetails = [];

    //Block Load SearchCustomer Form
    $(document).ready(function () {

        $("#loadingImg1").hide();
        $("#loadingImg2").hide();


        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });

        $('#btnLoadGuarantor').click(function () {
            ui('#popup_searchCustomer');
            loadSearchCustomer(2);
        });
        $('#btnSearchCustomerExit').click(function () {
            unblockui();
        });

        //Block Laod GuarantorForm(Load SearchCustomer Form)    
        $('#btnSearchCustomer').click(function () {
            ui('#popup_searchCustomer');
            loadSearchCustomer(1);
        });
        $('#btnSearchCustomerExit').click(function () {
            unblockui();
        });

        //Block Laod Marketing Officer Form    
        $('#btnLoadMarketingOfficer').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign Marketing Officer");
            searchUserType(0);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });
        //Block Laod Marketing Manager Form    
        $('#btnLoadSaleParsonName').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 1st approval");
            searchUserType(1);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load Recovery Manager Form    
        $('#btnRecoveryManager').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 2nd approval");
            searchUserType(2);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load Supplier Form    
        $('#btnSupplier').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 3rd approval");
            searchUserType(3);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load property Form    
        $('#btnAddProperty').click(function () {
            ui('#popup_addProperty');
            loadPropertyForm();
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });
    });



    $(function () {

        calculateLoan();

//        $("input[name=loanDownPayment]").val(0.00);
        //focus select loantype option
        $('select[name^="loanTypeList"]').eq(1).focus();

        //disabl enter key for submitting form
//        $(window).keydown(function(event) {
//            if (event.keyCode == 13) {
//                event.preventDefault();
//                return false;
//            }
//        });
        jQuery.extend(jQuery.expr[':'], {
            focusable: function (el, index, selector) {
                return $(el).is('a, button, :input, [tabindex]');
            }
        });

        //got to next input from enter key
        $(document).on('keypress', 'input,select,textarea', function (e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(':focusable');
                var index = $canfocus.index(this) + 1;
                if (index >= $canfocus.length)
                    index = 0;
                $canfocus.eq(index).focus();
            }
        });

        //press enter on button
        $('input[type="button"]').on("keypress", function (eve) {
            var key = eve.keyCode || e.which;
            if (key == 13) {
                $(this).click();
            }
            return false;
        });

        //enter loan amount and goto next input using tab/enter
        $('input[name=loanAmount]').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (code === 9 || code === 13) {
                var loanAmount = $(this).val();
//                var f_loanAmount = parseFloat(loanAmount);

                $("#viewLoanAmount").autoNumeric();
                $("#viewLoanAmount").val(loanAmount);

                calculateLoan();
                calculateCharge();
            }
        });

        //focus other input field other than loan amount
        $("input[name=loanAmount]").blur(function () {
            var loanAmount = $(this).val();
            $("#viewLoanAmount").autoNumeric();
            $("#viewLoanAmount").val(loanAmount);
            calculateLoan();
            calculateInvestment();
        });
        //focus other input field other than loan period
        $("input[name=loanPeriod]").blur(function () {
            var period = $(this).val();
            $("#viewLoanPeriod").val(period);
            calculateLoan();
        });
        //focus other input field other than loan downpayment
        $("input[name=loanDownPayment]").blur(function () {
            calculateInvestment();
        });

        //enter loan period and goto next input using tab
        $('input[name=loanPeriod]').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (code === 9 || code === 13) {
                var period = $(this).val();
                $("#viewLoanPeriod").val(period);
                calculateLoan();
            }
        });
        $('input[name=loanId]').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (code === 13) {
                searchLoan();
            }
        });
        $("input[name=loanAmount]").blur(function () {
            var period = $(this).val();
            $("#viewLoanPeriod").val(period);
            calculateLoan();
        });

        //format desimal numbers
        $('.decimalNumber').autoNumeric();

        //load loan types
//        $.getJSON('/AxaBankFinance/loadLoanType', function(data) {
//            for (var i = 0; i < data.length; i++) {
//                var name = data[i].loanTypeName;
//                var id = data[i].loanTypeId;
//                $('#loanTypeList').append("<option value='" + id + "' >" + name + "</option>");
//            }
//        });



        //change loantype change loan rate
        $('#loanTypeList').change(function () {
            var selected = $(this).find('option:selected');
            var loanType = selected.val();
            if (loanType > 0) {
                $.getJSON('/AxaBankFinance/subLoanTypes/' + loanType, function (data) {
                    ui('#popup_subLoanTypes');
                    $("#tblsubtypes tbody").html("");
                    for (var i = 0; i < data.length; i++) {
                        $("#tblsubtypes tbody").append("<tr onclick='clickLoanSubtype(this)'><td>" + data[i].subLoanName +
                                "<input type='hidden' value='" + data[i].subLoanId + "'/></td><td style='display:none'><a onclick='editRate()'>" + data[i].subInterestRate + "</a></td></tr>");
                    }
                });
            }
        });
        //change oter property type 
        $('#slctProperty').change(function () {
            loadPropertyForm();
        });

        //change periodType
        $('#cmbPeriodType').change(function () {
            calculateLoan();
        });

    });

//    click subloanType
    function clickLoanSubtype(tr) {
        var loanTypeId = $(tr).find("input").val();
        var rate = $(tr).find('a').text();
        $("#viewRate").val(rate);
        $("input[name=loanRate]").val(rate);
        $("input[name=loanType]").val(loanTypeId);
        calculateLoan();
        unblockui();
        $("#loanAmountId").focus();
        loadOtherCharges(loanTypeId);
    }

//    Edit Rate
    function editRate() {
//        var loanRate = $(subLoan).val();
        alert("loanRate");
    }

//load other charges types
    function loadOtherCharges(subId) {
        $.getJSON('/AxaBankFinance/otherChargesByLoanId/' + subId, function (data) {
            $('#otherChargesTable tbody').html("");

            for (var i = 0; i < data.length; i++) {
                var description = data[i].description;
                var isPresentage = data[i].isPrecentage;
                var charge, t, rateCharge;
                if (isPresentage) {
                    charge = data[i].taxRate;
                    rateCharge = charge + "%";
                    t = 1;
                } else {
                    charge = data[i].amount;
                    rateCharge = charge;
                    t = 0;
                }
                var id = data[i].id;
                var is_added = 0;
                var isAdded = data[i].isAdded;
                if (isAdded) {
                    is_added = 1;
                }
                $('#otherChargesTable tbody').append("<tr id='" + id + "'>" +
                        "<td>" + description + "<input type='hidden' value='" + t + "' id='t" + i + "'/><input type='hidden' value='" + id + "' id='cha" + i + "'/><input type='hidden' id='isadd" + i + "' value='" + is_added + "'/></td>" +
                        "<td><input type='hidden' value='" + charge + "' id='c" + i + "'/>" + "<a href='#' onclick='editCharge(" + i + ",1)' id='aaa" + i + "'>" + rateCharge + "</a></td>" +
                        "<td style='text-align:right' id='amount" + i + "'><a href='#' onclick='editCharge(" + i + ",2)' style='text-align: right;' id='hidAmnt" + i + "'></a></td></tr>");
            }
            $('#otherChargesTable tbody').append("<tr><td colspan='2'><label>Total</label></td><td class='decimalNumber'><label id='totalOther'><label></td>");
        });
    }

    //click loan other charges row
    var totalCharges = 0.00;
    var chargesAddedToLoan = 0.00;
    function calculateCharge() {
        totalCharges = 0.00;
        var i = 0;
        var loanAmount = $("input[name=loanAmount]").val();
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("last");
            } else {
                var labelId = "amount" + i;
                var amount;
                var chargeType = $("#t" + i).val();
                var charge = $("#c" + i).val();
                var isAdded = $("#isadd" + i).val();

                if (chargeType == 1) {
                    if (loanAmount != "") {
                        amount = ((charge * parseFloat(loanAmount)) / 100).toFixed(2);
                    } else {
                        amount = (parseFloat(0)).toFixed(2);
                    }
                } else {
                    amount = (parseFloat(charge)).toFixed(2);
                }
                if (isAdded == 1) {
                    chargesAddedToLoan = chargesAddedToLoan + amount;
                }

                if (isAdded == 0) {
                    totalCharges = totalCharges + parseFloat(amount);
                }

                $("#hidAmnt" + i).text(amount);
                $("#totalOther").text(totalCharges.toFixed(2));
            }
            i++;
        });
    }

    function loadSearchCustomer(type) {
        $("#gurant_debtr_type").val(type);
        $.ajax({
            url: '/AxaBankFinance/searchCustomer',
            success: function (data) {
                $('#popup_searchCustomer').html("");
                $('#popup_searchCustomer').html(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    function unloadSearchCustomer() {
        unblockui();
    }

    function saveLoanForm() {
        //check user is block
        var edit = '${edit}';
        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function (data) {
                if (!data) {
                    var validate = validateForm();
                    SetOtherCharges();
                    calculateLoan();
                    setCapitalizeTypes();
                    if (validate) {
                        if (!edit) {
                            document.getElementById("btnSave").disabled = true;
                        }
                        ui("#proccessingPage");
                        $.ajax({
                            url: '/AxaBankFinance/saveNewGroupCustomer',
                            type: 'POST',
                            data: $("#newLoanForm").serialize(),
                            success: function (data) {
                                unblockui();
                                $('#formContent').html(data);
                                $(".divSuccess").css({'display': 'block'});
                                $("html, body").animate({scrollTop: 0}, "slow");
                            },
                            error: function () {
                                alert("Error Loading...");
                            }
                        });
                    } else {
                        $(".divError").html("");
                        $(".divError").css({'display': 'block'});
                        $(".divError").html(errorMessage);
                        errorMessage = "";
                        $("html, body").animate({scrollTop: 0}, "slow");
                    }
                } else {
                    warning("User Is Blocked", "message_newLonForm");
                }
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    function SetOtherCharges() {
        var i = 0;
        $("#hidCharges").html("");
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("lastRO");
            } else {
                console.log("ROW" + i);
                var chargeId = $("#cha" + i).val();
                var charge = $("#c" + i).val();
                console.log(charge);
                var amount = $("#hidAmnt" + i).text();
                $("#hidCharges").append("<input type='hidden' name='chragesId' value='" + chargeId + "'/>" +
                        "<input type='hidden' name='chragesRate' value='" + charge + "'/>" +
                        "<input type='hidden' name='chragesAmount' value='" + amount + "'/>");
            }
            i++;
        });
    }

    $('#txtRateType').change(function () {
        calculateLoan();
    });


    function calculateLoan() {
//        var loanType = $(this).find('option:selected').val();
        var loanType = $("#loanTypeList").find('option:selected').val();
        var loanRateType = $("#txtRateType").find('option:selected').val();
        if (loanRateType === "FR") {
            if (loanType === 0) {
                return;
            }
            var downPayment = $("input[name=loanDownPayment]").val();
            var loanAmount = $("input[name=loanAmount]").val();
            if (loanAmount === "") {
                return;
            }


            var investment = 0.00;
            if (downPayment > 0) {
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
            } else {
                investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
            }
            $("input[name=loanInvestment]").val(investment);

            var amount = $("input[name=loanInvestment]").val();
            if (amount === null || amount === "")
                return;
            var period = $("#viewLoanPeriod").val();
            if (period === null || period === "")
                return;

            var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
            var periodType = $("#cmbPeriodType").find('option:selected').val();

            var newRate = $("#viewRate").val();
            var rate = newRate.replace('%', '');
            var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);
//            interest = interest / period;
            var totalInterest, totalAmount, installment, total;

            if (periodType === '3') {
                if (loanType === '10') {// For STLs
                    totalInterest = ((parseFloat(interest) * period)).toFixed(2);
                    totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                    installment = (parseFloat(totalAmount) / period).toFixed(2);
                    total = parseFloat(totalAmount).toFixed(2);

                } else {
                    totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
                    totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                    installment = (parseFloat(totalAmount) / period).toFixed(2);
                    total = parseFloat(totalAmount).toFixed(2);
                }
            } else {
                interest = interest / period;
                totalInterest = (parseFloat(interest) * period).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = (parseFloat(totalAmount) / period).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);

            }

            $("#viewInstallment").val(installment);
            $("#viewInterest").val(interest);
            $("#viewTotalInterest").val(totalInterest);
            $("#viewTotal").val(total);

            //reducing interest calculation
        } else {
            if (loanType == 0)
                return;
            var downPayment = $("input[name=loanDownPayment]").val();
            var loanAmount = $("input[name=loanAmount]").val();
            if (loanAmount == "")
                return;
            var investment = 0.00;
            if (downPayment > 0) {
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
            } else {
                investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
            }
            $("input[name=loanInvestment]").val(investment);

            var amount = $("input[name=loanInvestment]").val();
            if (amount == null || amount == "")
                return;
            var period = $("#viewLoanPeriod").val();
            if (period == null || period == "")
                return;

            var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
            var periodType = $("#cmbPeriodType").find('option:selected').val();

            var newRate = $("#viewRate").val();
            var rate = newRate.replace('%', '');
            rate = rate / 1200;
            var interest = (parseFloat(decimalAmount) * rate).toFixed(2);
            var totalInterest, totalAmount, installment, total;

            if (periodType === 3) {
                totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = decimalAmount * rate / (1 - (Math.pow(1 / (1 + rate), period / 30)));
                installment = parseFloat(installment).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);
            } else {
                totalInterest = (parseFloat(interest) * period).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = decimalAmount * rate / (1 - (Math.pow(1 / (1 + rate), period)));
                installment = parseFloat(installment).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);
            }
            $("#viewInstallment").val(installment);
            $("#viewInterest").val(interest);
            $("#viewTotalInterest").val(totalInterest);
            $("#viewTotal").val(total);

        }
    }

    function calculateInvestment() {
        var loanAmount = $("input[name=loanAmount]").val();
        var downPayment = $("input[name=loanDownPayment]").val();
        if (loanAmount != "" && downPayment != "") {
            var f_amount = parseFloat(loanAmount);
            var f_downpayment = parseFloat(downPayment);
            if (f_amount > f_downpayment) {
                var investment = 0.00;
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);

                $("input[name=loanInvestment]").val(investment);
                $("#viewDownPayment").val(downPayment);
                $("#viewInvestment").val(investment);
//                calculateCharge();
            } else {
                alert("Loan amount should be greater than down payment");
                $("input[name=loanDownPayment]").css("border", "1px solid red");
                $("input[name=loanDownPayment]").focus();
            }
        }
    }

    var errorMessage = "";

    function validateForm() {

        var validate = true;
        var groupID = $("#groupID").find('option:selected').val();
        var branchID = $("#branchID").find('option:selected').val();
        var centerID = $("#centeID").find('option:selected').val();
        var rowCount = document.getElementById('guranterTable').rows.length;

        if (branchID == 0) {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "-Select Branch";
            $("#loanTypeList").addClass("txtError");
        } else {
            $("#loanTypeList").removeClass("txtError");
        }

        if (centerID == 0) {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "-Select Center";
            $("#loanTypeList").addClass("txtError");
        } else {
            $("#loanTypeList").removeClass("txtError");
        }

        if (groupID == 0) {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "-Select Group";
            $("#loanTypeList").addClass("txtError");
        } else {
            $("#loanTypeList").removeClass("txtError");
        }

        if (rowCount == 1) {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "-Add customers to table to assing for the selected group";
            $("#loanTypeList").addClass("txtError");
        } else {
            $("#loanTypeList").removeClass("txtError");
        }

        return validate;
    }

    //add sales person,recovery manager,
    function searchUserType(type) {
        $("#hidOfficerType").val(type);
        var loanTypeId = $("input[name=loanType]").val();
        if (loanTypeId === "") {
            loanTypeId = 0;
        }
        $.ajax({
            url: '/AxaBankFinance/SearchOfficers/' + type + "/" + loanTypeId,
            success: function (data) {
                var tbody = $("#tblOfficers tbody");
                tbody.html("");
                for (var i = 0; i < data.length; i++) {
                    tbody.append("<tr onclick='clickOfficer(this)'><td>" + data[i].empId + "</td>" +
                            "<td>" + data[i].userName + "</td>" +
                            "<td style='display:none'>" + data[i].userId + "</td></tr>");
                }
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //click supplier row and add him to loan form
    var o_empNo = "", o_name = "", o_userId = "";
    function addOfficerToLoanForm() {
        var type = $("#hidOfficerType").val();
        if (o_userId == "") {
            alert("Please Select a name");
            return;
        } else {
            if (type == 0) {
                $("#marketingOfficerId").val(o_userId);
                $("#txtMarketingOfficer").val(o_name);
            } else if (type == 1) {
                $("#salesPersonId").val(o_userId);
                $("#salesPersonName").val(o_name);
            } else if (type == 2) {
                $("#recoveryPersonId").val(o_userId);
                $("#recoveryPersonName").val(o_name);
            } else if (type == 3) {
                $("#supplierId").val(o_userId);
                $("#supplierName").val(o_name);
            }
        }
        o_empNo = "", o_name = "", o_userId = "";
        unblockui();
    }

    //click supplier,recovery, sales offices table row
    function clickOfficer(tr) {
        var selected = $(tr).hasClass("highlight");
        $("#tblOfficers tbody tr").removeClass("highlight");
        if (!selected)
            $(tr).addClass("highlight");

        var tableRow = $(tr).children("td").map(function () {
            return $(this).text();
        }).get();
        o_empNo = $.trim(tableRow[0]);
        o_name = $.trim(tableRow[1]);
        o_userId = $.trim(tableRow[2]);
    }







    //close sales person, recovery officer, supplier pop up
    function cancelOfficersTable() {
//        o_id = "", o_name = "", o_address = "";
//        unblockui();
    }

    //other property
    function loadPropertyForm() {
        var select = $("#slctProperty").find('option:selected').val();
        $.ajax({
            url: '/AxaBankFinance/loadPropertForm/' + select,
            success: function (data) {
//                $("#slctProperty").css("display", "block");
                ui('#popup_addProperty');
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Save");
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //edit property
    function editPropertyForm(type, id) {
        $.ajax({
            url: '/AxaBankFinance/editPropertForm/' + type + '/' + id,
            success: function (data) {
                ui('#popup_addProperty');
                //$("#slctProperty").css("display", "none");
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Update");
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //search customer by nic
    $('#debtorNic').keyup(function (e) {
        var nicNo = $(this).val();
        if (nicNo.length === 10) {
            $.ajax({
                dataType: 'json',
                url: '/AxaBankFinance/searchByNic_new/' + nicNo,
                success: function (data) {
                    var debId = data.debtorId;
                    if (debId > 0) {
                        var name = "", nic = "", address = "", telephone = "";
                        if (data.nameWithInitial !== null)
                            name = data.nameWithInitial;
                        if (data.debtorNic !== null)
                            nic = data.debtorNic;
                        if (data.debtorPersonalAddress !== null)
                            address = data.debtorPersonalAddress;
                        if (data.debtorTelephoneMobile !== null)
                            telephone = data.debtorTelephoneMobile;

                        $("#debID").val(debId);
                        $("#debtorNic").val(nic);
                        $("#debtorName").val(name);
                        $("#debtorAddress").val(address);
                        $("#debtorTelephone").val(telephone);

                        var url2 = "/AxaBankFinance/viewProfilePicture/" + debId;
                        $("#customerProfilePicture").html("<img src='" + url2 + "' ></img>");

                    } else {
                        alert("Search Result Not found");
                        $("#debtorNic").val("");
                        $("#debID").val("");
                        $("#debtorName").val("");
                        $("#debtorAddress").val("");
                        $("#debtorTelephone").val("");
                    }

                },
                error: function () {
                    alert("Error Loading...");
                }
            });
        }
    });

    //search gurantor by nic
    $('#gurantorNic').keyup(function (e) {
        var nicNo = $(this).val();
        console.log("enter+" + nicNo);
        if (nicNo.length === 10) {
            $.ajax({
                dataType: 'json',
                url: '/AxaBankFinance/searchByNic_new/' + nicNo,
                success: function (data) {
                    var debId = data.debtorId;
                    if (debId > 0) {
                        var name = "", nic = "", address = "", telephone = "";
                        if (data.nameWithInitial !== null)
                            name = data.nameWithInitial;
                        if (data.debtorNic !== null)
                            nic = data.debtorNic;
                        if (data.debtorPersonalAddress !== null)
                            address = data.debtorPersonalAddress;
                        if (data.debtorTelephoneMobile !== null)
                            telephone = data.debtorTelephoneMobile;

                        $("#guarantorName").val(name);
                        $("#gurantorNic").val(nic);
                        var table = document.getElementById("guranterTable");
                        var rowCount = table.rows.length;
                        $('#guranterTable tbody').append('<tr id="' + debId + '"><td>' +
                                rowCount + '</td><td>' + name + '</td><td>' + nic + '</td><td>' +
                                address + '</td><td>' + telephone + '</td><td>' +
                                '</div><div class="delete" onclick="deleteRow(' + debId + ')" ></div>' + '</td></tr>');
                        $("#guarantorIDs").append("<input type='hidden' name='guarantorID' id='g_hid" + debId + "' value='" + debId + "'>");

                    } else {
                        alert("Search Result Not found");
                        $("#guarantorName").val("");
                        $("#gurantorNic").val("");
                    }

                },
                error: function () {
                    alert("Error Loading...");
                }
            });
        }
    });

//delete gurantor
    function deleteRow(dId) {
        $('#' + dId).remove();
        $('#g_hid' + dId).remove();
        $("#guarantorName").val("");
    }

//search loan by loan id
    function searchLoan() {

        var loanId = $("input[name=loanId]").val();
        var view = "message_newLonForm";
        if (loanId !== "") {
            $.ajax({
                url: '/AxaBankFinance/searchLoanDeatilsById/' + loanId,
                success: function (data) {
                    $('#formContent').html(data);
                    document.getElementById("txtLoanId").readOnly = true;
                    var msg = $("#testMessage").val();
                    if (msg != "") {
                        warning(msg, view);
                    }
                },
                error: function () {
                    alert("Error Loading...");
                }
            });

        } else {
            alert("Enter Loan Id");
        }

    }

    //proceed button
    function processLoanToApproval() {
        document.getElementById("btnProcess").disabled = true;
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loanDocumentSubmissionStatus/" + loan_id,
            success: function (data) {
                var process = data;
                var view = "message_newLonForm";
                if (process == 1) {
                    //cannot process
                    var msg = "You have to submit minimum documents to proceed";
                    warning(msg, view);
                } else if (process == 2) {
                    //process with pending dates
                    var msg = "Do you want to proceed loan without completing all documents";
                    dialog(msg, view, loan_id);
                } else {
                    //procees successfully
                    ui("#proccessingPage");
                    viewAllLoans(0);
                }
            },
            error: function () {

            }
        });
    }

    //document upload button
    function uploadDocuments() {
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loadDocumentChecking",
            data: {loan_id: loan_id},
            success: function (data) {
                $("#docDivCheck").html(data);
                ui('#docDivCheck');
            }
        });
    }

    //edit charge 
    function editCharge(row, type) {
        console.log(row);
        $.ajax({
            url: "/AxaBankFinance/loadEditRate/" + row + "/" + type,
            success: function (data) {
                $("#editRate_div").html(data);
                ui("#editRate_div");
            }
        });
    }

    function loadLoanCapitalizeForm() {
        var loanAmount = $("#loanAmountId").val().trim();
        var loanId = $("#hLoanId").val().trim();
        if (loanAmount !== "") {
            $("#loanAmountId").removeClass("txtError");
            if (loanId !== "") {
                editLoanCapitalizeForm(loanId);
            } else {
                addLoanCapitalizeForm();
            }
        } else {
            $("#loanAmountId").addClass("txtError");
            $("#loanAmountId").focus();
        }
    }

    $("#loanAmountId").on("keyup", function () {
        var loanAmount = $("#loanAmountId").val().trim();
        if (loanAmount !== "") {
            $("#loanAmountId").removeClass("txtError");
            $("#loanAmountId2").val(loanAmount);
        }
    });

    function setCapitalizeTypes() {
        if (loanCapitalizeDetails.length !== 0) {
            $('#divCapitalizeTypes').html("");
            for (var i = 0; i < loanCapitalizeDetails.length; i++) {
                $('#divCapitalizeTypes').append("<input type = 'hidden' name = 'capitalizeAmount' value = '" + loanCapitalizeDetails[i].amount + "'/>" +
                        "<input type = 'hidden' name = 'capitalizeId' value ='" + loanCapitalizeDetails[i].MCaptilizeType.id + "'/>");
            }
        }
    }

    function addLoanCapitalizeForm() {
        $.ajax({
            url: "/AxaBankFinance/loadLoanCapitalizeForm",
            success: function (data) {
                $("#divLoanCapitalizeForm").html("");
                $("#divLoanCapitalizeForm").html(data);
                if (loanCapitalizeDetails.length !== 0) {
                    loadLoanCapitalizeDetail();
                    ui("#divLoanCapitalizeForm");
                } else {
                    ui("#divLoanCapitalizeForm");
                }
            }
        });
    }

    function editLoanCapitalizeForm(loanId) {
        var loanAmount = "${loan.loanAmount}";
        loanAmount = parseFloat(loanAmount);
        var capitalizeAmount = "${capitalizeAmount}";
        capitalizeAmount = parseFloat(capitalizeAmount);
        loanAmount = loanAmount - capitalizeAmount;
        $("#loanAmountId2").val(loanAmount);
        $.ajax({
            url: "/AxaBankFinance/editLoanCapitalizeForm/" + loanId,
            success: function (data) {
                $("#divLoanCapitalizeForm").html("");
                $("#divLoanCapitalizeForm").html(data);
                if (loanCapitalizeDetails.length !== 0) {
                    loadLoanCapitalizeDetail();
                    ui("#divLoanCapitalizeForm");
                } else {
                    ui("#divLoanCapitalizeForm");
                }
            }
        });
    }


    $("#branchID").change(function () {

        var branchId = $("#branchID").val();

        $.ajax({
            url: "/AxaBankFinance/loadCenter/" + branchId,
            beforeSend: function (xhr) {
                $("#loadingImg1").show();
                $("#centerDropDown").html("");
            },
            success: function (data) {
                $("#centerDropDown").html("");
                $("#centerDropDown").html(data);
                $("#loadingImg1").hide();
            }
        });

    });


    function getGroup() {

        var centeID = $("#centeID").val();

        $.ajax({
            url: "/AxaBankFinance/loadGroup/" + centeID,
            beforeSend: function (xhr) {
                $("#loadingImg2").show();
                $("#groupDropDown").html("");
            },
            success: function (data) {
                $("#loadingImg2").hide();
                $("#groupDropDown").html("");
                $("#groupDropDown").html(data);
            }
        });
    }



</script>
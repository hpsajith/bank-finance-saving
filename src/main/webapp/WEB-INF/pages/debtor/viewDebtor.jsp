<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row fixed-table">
        <div class="table-content">
            <c:choose>
                <c:when test="${message}">        
                    <table class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header" id="tblCustomer2" style="margin-top: 10px;">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>NIC No</th>
                                <th>Address</th>
                                <th>Mobile No</th>
                            </tr>
                        </thead>
                        <tbody>                    
                            <c:forEach var="debtor" items="${debtorList}">
                                <c:choose>
                                    <c:when test="${debtor.debtorAccountNo!=''}">
                                        <tr id="${debtor.debtorId}">
                                        </c:when>
                                        <c:otherwise>
                                        <tr id="${debtor.debtorId}" style="background: #D3D3D3;">
                                        </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                        <c:when test="${debtor.debtorName!=null}">
                                            <td>${debtor.debtorName}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorNic!=null}">
                                            <td>${debtor.debtorNic}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorPersonalAddress!=null}">
                                            <td>${debtor.debtorPersonalAddress}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${debtor.debtorTelephoneMobile!=null}">
                                            <td>${debtor.debtorTelephoneMobile}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td style="display: none;">${debtor.debtorAccountNo}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise><label style="color:red">No Result Found!!!</label></c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#tblCustomer2 tbody').find('tr').click(function() {
            var debid = $(this).attr("id");
            // alert('You clicked row '+ ($(this).index()+1) +"--"+did);

            var tableRow = $(this).children("td").map(function() {
                return $(this).text();
            }).get();
//            alert("Debtor:" + debid);
            $.ajax({
                url: '/AxaBankFinance/viewcustomor/'+debid,
                success: function(data) {
                    $('#editCust_div').html('');
                    $('#editCust_div').append(data);
                    ui('#editCust_div');
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        });
    });
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row">
    <div class="col-md-11" style="border:1px solid #C8C8C8; border-radius: 2px; margin: 20px; ">
        <form name="dependentLoanForm" id="formDependent" autocomplete="off">
            <legend>Dependent Details</legend>
            <input type="hidden" name="debtorId" value="${dep.debtorId}" id="d_did"/>
            <input type="hidden" name="dependentId" value="${dep.dependentId}"/>
            <div class="row">
                <div class="col-md-4">Full Name</div>
                <div class="col-md-8"><input type="text" name="dependentName" id="dependentName_id" value="${dep.dependentName}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4"><span style="color: red">*</span>NIC No</div>
                <div class="col-md-8"><input type="text" name="dependentNic" id="dependentNic_id" value="${dep.dependentNic}" style="width: 100%" maxlength="10" onblur="nictoLowerCase(this)"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Telephone No</div>
                <div class="col-md-8"><input type="text" name="dependentTelephone" value="${dep.dependentTelephone}" style="width: 100%" maxlength="10" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Job</div>
                <div class="col-md-8"><input type="text" name="dependentJob" value="${dep.dependentJob}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Occupation</div>
                <div class="col-md-8"><input type="text" name="dependentOccupation" value="${dep.dependentOccupation}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Income</div>
                <div class="col-md-8"><input type="text" name="dependentNetSalary" value="${dep.dependentNetSalary}" style="width: 100%" onkeyup="checkNumbers(this)"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Office Name</div>
                <div class="col-md-8"><input type="text" name="dependentOfficeName" value="${dep.dependentOfficeName}" style="width: 100%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4"><span style="color: red">*</span>Office Address</div>
                <div class="col-md-8"><textarea name="dependentOfficeAddress" id="dependentOfficeAddress_id" style="width: 100%" maxlength="200">${dep.dependentOfficeAddress}</textarea></div>
            </div>
            <div class="row">
                <div class="col-md-4">Office Telephone</div>
                <div class="col-md-8"><input type="text" name="dependentOfficeTelephone" value="${dep.dependentOfficeTelephone}" style="width: 100%" onkeyup="checkNumbers(this)" maxlength="10"/></div>
            </div>
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                        <input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unLoadForm($('#d_did').val())"/>
                        <!--<input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"/>-->
                    </div>
                    <div class="col-md-3">
                        <input type="button" class="btn btn-default col-md-12" value="Save" onclick="saveUpdateDependent()"/>
                    </div>
                </div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>
</div>

<script>
    function saveUpdateDependent() {

        var ar = [document.getElementById('dependentName_id'), document.getElementById('dependentNic_id'), document.getElementById('dependentOfficeAddress_id')];
        if (validateFields(ar) === true) {
            if (checkNIC("dependentNic_id") === true) {
                $.ajax({
                    url: '/AxaBankFinance/saveOrUpdateDependent',
                    type: 'POST',
                    data: $("#formDependent").serialize(),
                    success: function(data) {
                        alert(data);
                        var dId = $('#d_did').val();
                        unLoadForm(dId);
                    },
                    error: function() {
                        alert("Error Loading...");
                    }
                });
            }
        }
        
    }

</script>
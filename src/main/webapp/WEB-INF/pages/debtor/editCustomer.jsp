<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $('.dataTable').dataTable({
            "scrollY": "150px",
            "scrollCollapse": true,
            "paging": false
        });
    });

</script>
<div class="container-fluid" style="height: 100%;overflow-y: auto;width: 100%">

    <div class="col-md-12" style="border:1px solid #C8C8C8; border-radius: 2px; margin: 4px; ">
        <legend>Basic Details</legend>
        <form id="loan_update_form_temp" name="debtorHeaderDetail" autocomplete="off">
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-3">Full Name</div>
                        <div class="col-md-9"><input style="width: 100%" value="${debtor.loanFullName}" type="text"
                                                     name="debtorName" id="debtorName_id" maxlength="200"/></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">NIC</div>
                                <div class="col-md-8"><input type="text" value="${debtor.loanNic}" style="width: 100%"
                                                             name="debtorNic" id="debtorNic_id" maxlength="10"
                                                             onblur="nictoLowerCase(this)"/></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">DOB</div>
                                <div class="col-md-8"><input type="text" class="txtCalendar" value="${debtor.loanDob}"
                                                             style="width: 100%" name="debtorDob" id="debtorDob_id"/>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">Name with Initials</div>
                        <div class="col-md-9"><input style="width: 100%" value="${debtor.nameWithInitials}" type="text"
                                                     name="nameWithInitial" id="debtorName_id" maxlength="200"/></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">Email</div>
                                <div class="col-md-10"><input type="text" value="${debtor.loanEmail}"
                                                              style="width: 100%" name="debtorEmail" id="debtorEmail_id"
                                                              onblur="validateEmail(this)" maxlength="40"/></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">Fax</div>
                                <div class="col-md-8"><input type="text" value="${debtor.loanFax}" name="debtorFax"
                                                             style="width: 100%" id="debtorFax_id" maxlength="10"
                                                             onkeyup="checkNumbers(this)"/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-3">Personal Address</div>
                        <div class="col-md-9"><textarea name="debtorPersonalAddress" id="debtorPersonalAddress_id"
                                                        style="width: 100%"
                                                        class="address">${debtor.loanAddress}</textarea></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-4">Formal Address</div>
                        <div class="col-md-8"><textarea name="debtorFormalAddress" id="debtorFormalAddress_id"
                                                        style="width: 100%"
                                                        class="address">${debtor.loanPostAddress}</textarea></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">

                    <div class="row">
                        <div class="col-md-4">Member No</div>
                        <div class="col-md-8"><input type="text" value="${debtor.memberNumber}" style="width: 100%;"
                                                     name="memberNumber" id="memberNumber_id" maxlength="200"
                                                     onchange="memberNumber()"/></div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">Job Information</div>
                        <div class="col-md-8"><input type="text" value="${debtor.jobInformation}" style="width: 100%;"
                                                     name="jobInformation" id="jobInformationId"/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Center Day</div>
                        <div class="col-md-8"><select name="centerDay" selected="${debtor.centerDay}"
                                                      style="width: 100%" value="${debtor.centerDay}"
                                                      id="centerDay_id">
                            <c:choose>
                                <c:when test="${debtor.centerDay == 'Sunday'}">
                                    <option value="Sunday" selected="true">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </c:when>
                                <c:when test="${debtor.centerDay == 'Monday'}">
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday" selected="true">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </c:when>
                                <c:when test="${debtor.centerDay == 'Tuesday'}">
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday" selected="true">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </c:when>
                                <c:when test="${debtor.centerDay == 'Wednesday'}">
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday" selected="true">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </c:when>
                                <c:when test="${debtor.centerDay == 'Thursday'}">
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday" selected="true">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </c:when>
                                <c:when test="${debtor.centerDay == 'Friday'}">
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday" selected="true">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </c:when>
                                <c:when test="${debtor.centerDay == 'Saturday'}">
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday" selected="true">Saturday</option>
                                </c:when>

                                <c:otherwise>
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">Contact Home</div>
                        <div class="col-md-8"><input type="text" value="${debtor.loanTelephoneHome}"
                                                     style="width: 100%;" name="debtorTelephoneHome" class="number"
                                                     id="debtorTelephoneHome_id" maxlength="10"
                                                     onkeyup="checkNumbers(this)" onblur="checkTelNumbers(this)"/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Contact Work</div>
                        <div class="col-md-8"><input type="text" value="${debtor.loanTelephoneOffice}"
                                                     style="width: 100%;" name="debtorTelephoneWork" class="number"
                                                     id="debtorTelephoneWork_id" maxlength="10"
                                                     onkeyup="checkNumbers(this)"/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Contact Mobile</div>
                        <div class="col-md-8"><input type="text" value="${debtor.loanTelephoneMobile}"
                                                     style="width: 100%;" name="debtorTelephoneMobile" class="number"
                                                     id="debtorTelephoneMobile_id" maxlength="10"
                                                     onkeyup="checkNumbers(this)"/></div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-4">Police Station</div>
                        <div class="col-md-8"><input type="text" value="${debtor.loanPolisStation}"
                                                     name="debtorPoliceStation" id="debtorPoliceStation_id"
                                                     style="width: 100%"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Post Office</div>
                        <div class="col-md-8"><input type="text" value="${debtor.loanPostOffice}"
                                                     name="debtorPostOffice" id="debtorPostOffice_id"
                                                     style="width: 100%"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">G.M.Office</div>
                        <div class="col-md-8"><input type="text" value="${debtor.loanSecretaryOffice}"
                                                     name="debtorAgOffice" id="debtorAgOffice_id" style="width: 100%"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">Bank Branch</div>
                        <div class="col-md-8"><input style="width: 100%" value="${debtor.bankBranch}" type="text"
                                                     name="bankBranch" id="bankBranch_id"/></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">Bank A/C Name</div>
                        <div class="col-md-8"><input style="width: 100%" value="${debtor.accountName}" type="text"
                                                     name="accountName" id="accountName_id"/></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">Bank A/C No</div>
                        <div class="col-md-8"><input style="width: 100%" value="${debtor.accountNo}" type="text"
                                                     name="accountNo" id="accountNo_id"/></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">Profile Picture</div>
                        <div class="col-md-4"><input style="width: 100%" value="${debtor.loanImage}" type="text"
                                                     name="debtorMap" id="debtorMap_id"/></div>
                        <div class="col-md-4"><input style="width: 100%" type="button" value="browse"/></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">Location</div>
                        <div class="col-md-4"><input style="width: 100%" value="${debtor.loanLoacationMap}" type="text"
                                                     name="debtorImage" id="debtorImage_id"/></div>
                        <div class="col-md-4"><input style="width: 100%" type="button" value="browse"/></div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="debtorId" value="${id}" id="txtDebtorId_edit"/>
            <input type="hidden" name="debtorAccountNo" value="${debtor.loanAccountNo}" id="txtDebtorAccountNo"/>
            <input type="hidden" name="branchId" value="${debtor.loanBranch}" id="txtBranchId"/>
            <div class="row" style="margin-top: 10px;"></div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

    </div>
    <div class="row">
        <div class="col-md-10">

        </div>
        <div class="col-md-2">
            <input type="button" id="updateBtnId" class="btn btn-default col-md-12" value="Update"
                   onclick="updateDebtor()"/>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <c:if test="${dependent.size()>0}">
                <label>Dependent Details</label>
                <table id="tblDependent"
                       class="table table-striped table-bordered table-hover table-condensed table-edit" cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>NIC</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="dep" items="${dependent}">
                        <tr onclick="selectTableRow(1,${dep.dependentId})">
                            <td>#</td>
                            <td>${dep.dependentName}</td>
                            <td>${dep.dependentNic}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

        </div>
        <div class="col-md-4">
            <c:if test="${employee.size()>0}">
                <label>Employee Details</label>
                <table id="tblEmplyee"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>ID No</th>
                        <th>Company Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="emp" items="${employee}">
                        <tr onclick="selectTableRow(2,${emp.companyId})">
                            <td>#</td>
                            <td>${emp.companyName}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

        </div>
        <div class="col-md-4">
            <c:if test="${business.size()>0}">
                <label>Business Details</label>
                <table id="tblBusiness"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Business Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="busi" items="${business}">
                        <tr onclick="selectTableRow(3,${busi.businessId})">
                            <td>#</td>
                            <td>${busi.businessName}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <c:if test="${bank.size()>0 || vehicle.size()>0 || land.size()>0}">
                <label>Asset Details</label>
                <table id="tblAsset"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${bank.size()>0}">
                        <c:forEach var="bank" items="${bank}">
                            <tr onclick="selectAssetRow(1,${bank.bankId})">
                                <td>#</td>
                                <td>Bank</td>
                                <td>${bank.bankName}&nbsp;${bank.bankBranchName}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <c:if test="${vehicle.size()>0}">
                        <c:forEach var="vehicle" items="${vehicle}">
                            <tr onclick="selectAssetRow(2,${vehicle.vehicleId})">
                                <td>#</td>
                                <td>Vehicle</td>
                                <td>${vehicle.vehicleType}&nbsp;${vehicle.vehicleModel}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <c:if test="${land.size()>0}">
                        <c:forEach var="land" items="${land}">
                            <tr onclick="selectAssetRow(3,${land.realEstateId})">
                                <td>#</td>
                                <td>Real Estate</td>
                                <td>${land.deedNo}&nbsp;${land.landPlace}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </c:if>
        </div>
        <div class="col-md-6">
            <c:if test="${liability.size()>0}">
                <label>Liability Details</label>
                <table id="tblLiability"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Description</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="lia" items="${liability}">
                        <tr onclick="selectTableRow(4,${lia.liabilityId})">
                            <td>#</td>
                            <td>${lia.liabilityDescription}</td>
                            <td>${lia.liabilityAmount}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;margin-bottom: 20px">
        <div class="col-md-10">
            <div class="col-md-3">
                <input type="button" class="btn btn-default col-md-12" value="Dependent" onclick="addDependent()"/>
            </div>
            <div class="col-md-3">
                <input type="button" class="btn btn-default col-md-12" value="Employment" onclick="addEmplyment()"/>
            </div>
            <div class="col-md-2">
                <input type="button" class="btn btn-default col-md-12" value="Business" onclick="addBusiness()"/>
            </div>
            <div class="col-md-2">
                <input type="button" class="btn btn-default col-md-12" value="Asset" onclick="addAsset()"/>
            </div>
            <div class="col-md-2">
                <input type="button" class="btn btn-default col-md-12" value="Liability" onclick="addLiability()"/>
            </div>
        </div>
        <div class="col-md-2">
            <input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"/>
        </div>
    </div>
</div>
<!--Temp data-->
<input type="hidden" value="${debtor.loanFullName}" id="tempName">
<input type="hidden" value="${debtor.loanNic}" id="tempNic">
<input type="hidden" value="${debtor.loanAddress}" id="tempAddress">
<input type="hidden" value="${debtor.loanTelephoneHome}" id="tempTelephone">
<input type="hidden" value="${debtor.loanDebtorId}" id="tempid">


<!--form content-->
<div id="debtorForms" class="searchCustomer" style="width: 45%;  margin-top:10%; margin-left: 5%">
</div>

<script>

    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });

    var dependentId = 0, empId = 0, busi = 0, liability = 0, asset = 0;
    function updateDebtor() {
        var ar = [document.getElementById('debtorName_id'), document.getElementById('debtorNic_id'), document.getElementById('debtorDob_id')];
        var ar1 = [document.getElementById('debtorPersonalAddress_id'), document.getElementById('debtorFormalAddress_id')];
        var ar2 = [document.getElementById('debtorTelephoneHome_id'), document.getElementById('debtorTelephoneWork_id'), document.getElementById('debtorTelephoneMobile_id')];
        if (validateFields(ar) === true && validateOneField(ar1, '.address') === true && validateOneField(ar2, '.number') === true) {
            if (checkNIC("debtorNic_id") === true) {
                $.ajax({
                    url: '/AxaBankFinance/updateDebtorForm',
                    type: 'POST',
                    data: $('#loan_update_form_temp').serialize(),
                    success: function (data) {
                        alert(data);
                    },
                    error: function () {
                        alert("errror");
                    }
                });
            }
        }
    }

    function unLoadForm(dId) {
        unblockui();
        editCustomer(dId);
    }
    function addDependent() {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewDependentForm/' + debtorId + "/" + dependentId,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                dependentId = 0;
            }
        });
    }

    function addEmplyment() {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewEmplyementForm/' + debtorId + "/" + empId,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                empId = 0;
            }
        });
    }

    function addBusiness() {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewBusinessForm/' + debtorId + "/" + busi,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                busi = 0;
            }
        });
    }

    function addLiability() {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewLiabilityForm/' + debtorId + "/" + liability,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                liability = 0;
            }
        });
    }

    function addAsset() {
        //var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetForms',
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
            }
        });
    }

    function addAssetBank() {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetBankForm/' + debtorId + "/" + asset,
            success: function (data) {
                $("#assetContent").html(data);
                asset = 0;
            }
        });
    }

    function addAssetVehicle() {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetVehicleForm/' + debtorId + "/" + asset,
            success: function (data) {
                $("#assetContent").html(data);
                asset = 0;
            }
        });
    }

    function addAssetLand() {
        var debtorId = $("#txtDebtorId_edit").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetLandForm/' + debtorId + "/" + asset,
            success: function (data) {
                $("#assetContent").html(data);
                asset = 0;
            }
        });
    }

    function selectTableRow(type, id) {
        var debtorId = $("#txtDebtorId").val();
        if (type == 1) {
            dependentId = id;
            addDependent();
        } else if (type == 2) {
            empId = id;
            addEmplyment();
        } else if (type == 3) {
            busi = id;
            addBusiness();
        } else if (type == 4) {
            liability = id;
            addLiability();
        }
    }

    function memberNumber() {
        $("#memberNumber_id").mask("a*/999/99/999");
    }


</script>
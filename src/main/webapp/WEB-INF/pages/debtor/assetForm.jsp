<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row">
    <div class="col-md-10" style="border:1px solid #C8C8C8; border-radius: 2px; margin: 20px; ">
        <legend>Asset Details</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-6">Select Asset Type</div>
                <div class="col-md-6">
                    <select id="slctAsset">
                        <option value="1">Bank</option>
                        <option value="2">Vehicle</option>
                        <option value="3">Real Estate</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row" id="assetContent" style="margin: 15px; height:260px; ">
            
        </div>
    </div>
</div>
<script>
    $(function() {
        addAssetBank();
        
        $('#slctAsset').change(function() {
            var selected = $(this).find('option:selected').val();
            if (selected == 1) {
                addAssetBank();
            } else if (selected == 2) {
                addAssetVehicle();
            } else if (selected == 3) {
                addAssetLand();
            }
        });
    })


    
    
</script> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="container-fluid">
    <div class="row divError"></div>
    <div class="col-md-12" style="border:1px solid #C8C8C8; border-radius: 2px;">
        <strong>Add Customer</strong>
        <input type="hidden" value="${branchCode}" id="branchCodeId"/>
        <button type="button" class="btn btn-info btn-lg" style="display:none;" id="modelBoxId" data-toggle="modal"
                data-target="#myModal"></button>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" style="color: #002eb4"><strong>Save New Customer</strong></h5>
                    </div>
                    <div class="modal-body">
                        <p>Customer Saved Successfully.!!</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <form id="loan_form_temp" name="debtorForm" autocomplete="off">
            <div class="row" style="margin-top: 5px">
                <div class="col-md-12" style=" margin: 10px;">
                    <legend>Basic details</legend>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Member Number
                        </div>
                        <div class="col-md-4"><input style="width: 100%" type="text" name="memberNumber"
                                                     value="${debtor.loanFullName}" id="memberNumber_id" maxlength="200"
                                                     onchange="memberNumber()" onblur="validateMemberNo()"/></div>
                        <div class="col-md-3 col-lg-3 col-sm-3" style="color: black"><strong>Ex:</strong> <label
                                style="color: blue"> AA/123/12/123</label></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Full Name</div>
                        <div class="col-md-10"><input style="width: 100%" type="text" name="loanFullName"
                                                      value="${debtor.loanFullName}" id="loanFullName_id"
                                                      maxlength="200"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Name with
                            Initials
                        </div>
                        <div class="col-md-10"><input type="text" name="nameWithInitials" style="width: 100%"
                                                      value="${debtor.nameWithInitials}" id="nameIni_id"
                                                      maxlength="200"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>NIC</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="loanNic"
                                                     value="${debtor.loanNic}" id="loanNic_id" maxlength="12"
                                                     onblur="checkDebtorByNic(this)"/>
                            <label style="text-align: left" id="msgNicExits" class="msgTextField"></label>
                        </div>
                        <div class="col-md-2" style="text-align: right"><span style="color: red">*</span>Date Of Birth
                        </div>
                        <div class="col-md-4"><input class="txtCalendar" type="text" style="width: 100%"
                                                     value="${debtor.loanDob}" name="loanDob" id="loanDob_id"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Email</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="loanEmail" id="loanEmail"
                                                     value="${debtor.loanEmail}" onblur="validateEmail(this)"
                                                     maxlength="40"/></div>
                        <div class="col-md-2" style="text-align: right">Fax</div>
                        <div class="col-md-4"><input type="text" name="loanFax" id="loanFax" style="width: 100%"
                                                     id="fax" value="${debtor.loanFax}" maxlength="10"
                                                     onkeyup="checkNumbers(this)"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Personal Address
                        </div>
                        <div class="col-md-4"><textarea name="loanAddress" style="width: 100%" id="loanAddress_id"
                                                        class="address" maxlength="200">${debtor.loanAddress}</textarea>
                        </div>
                        <div class="col-md-2" style="text-align:right"><span style="color: red">*</span>Formal Address
                        </div>
                        <div class="col-md-4"><textarea name="loanPostAddress" style="width: 100%"
                                                        id="loanPostAddress_id" class="address"
                                                        maxlength="200">${debtor.loanPostAddress}</textarea></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Job Information
                        </div>
                        <div class="col-md-10"><input type="text" name="jobInformation" style="width: 100%"
                                                      value="${debtor.jobInformation}" id="jobInformation_id"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Center Day</div>
                        <div class="col-md-10">
                            <select name="centerDay" value="${debtor.centerDay}" id="centerDay_id">
                                <option value="0">-Select Center Day-</option>
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" style="text-align: left"><strong>Contact Numbers</strong></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Home</div>
                        <div class="col-md-4">
                            <input type="text" style="width: 100%;" name="loanTelephoneHome" id="loanTelephoneHome_id"
                                   value="${debtor.loanTelephoneHome}" class="number" maxlength="10"
                                   onblur="checkTelNumbers(this, '#msgHome')" onkeypress="return checkNumbers(this)"/>
                            <label id="msgHome"></label>
                        </div>
                        <div class="col-md-2" style="text-align: right"><span style="color: red">*</span>Work</div>
                        <div class="col-md-4">
                            <input type="text" style="width: 100%;" name="loanTelephoneOffice"
                                   id="loanTelephoneOffice_id" value="${debtor.loanTelephoneOffice}" class="number"
                                   maxlength="10" onblur="checkTelNumbers(this, '#msgWork')"
                                   onkeypress="return checkNumbers(this)"/>
                            <label id="msgWork"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Mobile</div>
                        <div class="col-md-4">
                            <input type="text" style="width: 100%;" name="loanTelephoneMobile"
                                   id="loanTelephoneMobile_id" value="${debtor.loanTelephoneMobile}" class="number"
                                   maxlength="10" onblur="checkTelNumbers(this, '#msgMobile')"
                                   onkeypress="return checkNumbers(this)"/>
                            <label id="msgMobile"></label>
                        </div>
                        <div class="col-md-2" style="text-align: right">Police Station</div>
                        <div class="col-md-4"><input type="text" name="loanPolisStation"
                                                     value="${debtor.loanPolisStation}" style="width: 100%"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Post Office</div>
                        <div class="col-md-4"><input type="text" name="loanPostOffice" value="${debtor.loanPostOffice}"
                                                     style="width: 100%"/></div>
                        <div class="col-md-2" style="text-align: right">G.M.Office</div>
                        <div class="col-md-4"><input type="text" name="loanSecretaryOffice"
                                                     value="${debtor.loanSecretaryOffice}" style="width: 100%"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4" style="text-align: left">Picture</div>
                                <div class="col-md-6"><input style="width: 100%" type="file" value="browse"
                                                             id="myPicture"/></div>
                                <div class="col-md-2" id="myProfilePic"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4" style="text-align: right">Location</div>
                                <div class="col-md-8"><input style="width: 100%" type="file" value="browse"/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" style="text-align: left"><strong>Bank Details</strong></div>
            </div>
            <div class="row">
                <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Bank</div>
                <div class="col-md-4">
                    <input type="text" style="width: 100%;" name="bankBranch" id="loanBranName_id"
                           value="${debtor.bankBranch}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2" style="text-align: left"><span style="color: red">*</span>Account Name</div>
                <div class="col-md-4">
                    <input type="text" style="width: 100%;" name="accountName" id="loanAccName_id"
                           value="${debtor.accountName}"/>
                </div>

                <div class="col-md-2" style="text-align: right"><span style="color: red">*</span>Account Number</div>
                <div class="col-md-4">
                    <input type="text" style="width: 100%;" name="accountNo" id="loanAccNumber_id"
                           value="${debtor.accountNo}" class="number" onkeypress="return checkNumbers(this)"/>
                </div>
            </div>


            <div class="row" style="margin-top: 5px">
                <div class="col-md-12" style="margin: 10px; ">
                    <legend>Dependent Details</legend>
                    <input type="hidden" name="dependentId" value="${dependentId}"/>
                    <div class='row'>
                        <div class="col-md-2" style="text-align: left">Name</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="dependentName"
                                                     id="dependentName_id" value="${debtor.dependentName}"/></div>
                        <div class="col-md-2" style="text-align: right">NIC</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="dependentNic"
                                                     id="dependentNic_id" value="${debtor.dependentNic}" maxlength="12"
                                                     onblur="nictoLowerCase(this)"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Telephone</div>
                        <div class="col-md-4">
                            <input type="text" style="width: 100%" name="dependentTelephone" id="dependentTelephone_id"
                                   value="${debtor.dependentTelephone}" maxlength="10"
                                   onblur="checkTelNumbers(this, '#msgTp')" onkeypress="return checkNumbers(this)"/>
                            <label id="msgTp"></label>
                        </div>
                        <div class="col-md-2" style="text-align: right">Job</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="dependentJob"
                                                     value="${debtor.dependentJob}"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Occupation</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="dependentOccupation"
                                                     value="${debtor.dependentOccupation}"/></div>
                        <div class="col-md-2" style="text-align: right">Income</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" onkeyup="checkNumbers(this)"
                                                     name="dependentNetSalary" value="${debtor.dependentNetSalary}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Office Name</div>
                        <div class="col-md-4"><input type="text" style="width: 100%" name="dependentOfficeName"
                                                     value="${debtor.dependentOfficeName}"/></div>
                        <div class="col-md-2" style="text-align: right">Office Telephone</div>
                        <div class="col-md-4"><input type="text" name="dependentOfficeTelephone"
                                                     id="dependentOfficeTelephone"
                                                     value="${debtor.dependentOfficeTelephone}" style="width: 100%"
                                                     maxlength="10" onkeyup="checkNumbers(this)"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="text-align: left">Office Address</div>
                        <div class="col-md-4"><textarea name="dependentOfficeAddress" id="dependentOfficeAddress_id"
                                                        style="width: 100%">${debtor.dependentOfficeAddress}</textarea>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
            </div>
            <c:choose>
                <c:when test="${edit}">
                    <div class="" style="margin-top: 5px">
                        <div class="col-md-12">
                            <div class="row" style="margin-bottom: 15px">
                                <div class="col-md-2">
                                    <input type="button" class="btn btn-default col-md-12" style="" value="Cancel"
                                           onclick="unblockui()"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="button" class="btn btn-default col-md-12" value="Employment"
                                           onclick="addEmplyment()"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="button" class="btn btn-default col-md-12" value="Business"
                                           onclick="addBusiness()"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="button" class="btn btn-default col-md-12" value="Asset"
                                           onclick="addAsset()"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="button" class="btn btn-default col-md-12" value="Liability"
                                           onclick="addLiability()"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="button" id="btnSaveDebtor" class=" btn btn-default col-md-12" style=""
                                           value="Update" onclick="saveNewDebtor()"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="" style="margin-top: 5px">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="row" style="margin-bottom: 15px">
                                <div class="col-md-6">
                                    <input type="button" class="btn btn-default col-md-12" style="" value="Cancel"
                                           onclick="unblockui()"/>
                                </div>
                                <div class="col-md-6">
                                    <input id="btnSaveDebtor" type="button" class=" btn btn-default col-md-12" style=""
                                           value="Save" onclick="saveNewDebtor()"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" name="loanDebtorId" value="${id}" id="txtDebtorId"/>
            <input type="hidden" name="loanIsDebtor" value="${debtor.loanIsDebtor}" id="loandebId"/>
            <input type="hidden" name="loanAccountNo" value="${debtor.loanAccountNo}" id="txtLoanAccountNo"/>
            <input type="hidden" name="isGenerate" value="${debtor.isGenerate}" id="txtIsGenerate"/>

        </form>
    </div>

    <div class="row">
        <!--        <div class="col-md-4">
        <c:if test="${dependent.size()>0}">
            <label>Dependent Details</label>
            <table id="tblDependent" class="table table-striped table-bordered table-hover table-condensed table-edit" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>NIC</th>
                    </tr>
                </thead>
                <tbody>
            <c:forEach var="dep" items="${dependent}">
                <tr onclick="selectTableRow(1,${dep.dependentId})">
                    <td>#</td>
                    <td>${dep.dependentName}</td>
                    <td>${dep.dependentNic}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
        </c:if>                    

    </div>        -->
        <div class="col-md-6">
            <c:if test="${employee.size()>0}">
                <label>Employee Details</label>
                <table id="tblEmplyee"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>ID No</th>
                        <th>Company Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="emp" items="${employee}">
                        <tr onclick="selectTableRow(2,${emp.companyId})">
                            <td>#</td>
                            <td>${emp.companyName}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

        </div>
        <div class="col-md-6">
            <c:if test="${business.size()>0}">
                <label>Business Details</label>
                <table id="tblBusiness"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Business Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="busi" items="${business}">
                        <tr onclick="selectTableRow(3,${busi.businessId})">
                            <td>#</td>
                            <td>${busi.businessName}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <c:if test="${bank.size()>0 || vehicle.size()>0 || land.size()>0}">
                <label>Asset Details</label>
                <table id="tblAsset"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${bank.size()>0}">
                        <c:forEach var="bank" items="${bank}">
                            <tr onclick="selectAssetRow(1,${bank.bankId})">
                                <td>#</td>
                                <td>Bank</td>
                                <td>${bank.bankName}&nbsp;${bank.bankBranchName}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <c:if test="${vehicle.size()>0}">
                        <c:forEach var="vehicle" items="${vehicle}">
                            <tr onclick="selectAssetRow(2,${vehicle.vehicleId})">
                                <td>#</td>
                                <td>Vehicle</td>
                                <td>${vehicle.vehicleType}&nbsp;${vehicle.vehicleModel}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <c:if test="${land.size()>0}">
                        <c:forEach var="land" items="${land}">
                            <tr onclick="selectAssetRow(3,${land.realEstateId})">
                                <td>#</td>
                                <td>Real Estate</td>
                                <td>${land.deedNo}&nbsp;${land.landPlace}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </c:if>
        </div>
        <div class="col-md-6">
            <c:if test="${liability.size()>0}">
                <label>Liability Details</label>
                <table id="tblLiability"
                       class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header"
                       style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Description</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="lia" items="${liability}">
                        <tr onclick="selectTableRow(4,${lia.liabilityId})">
                            <td>#</td>
                            <td>${lia.liabilityDescription}</td>
                            <td>${lia.liabilityAmount}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>
<!--form content-->
<div id="debtorForms" class="searchCustomer" style="width: 45%; ">
</div>
<!--//message-->
<div id="message_newCustomer" class="searchCustomer"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script>

    $('.txtCalendar').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        yearRange: "-100:+0",
    });

    $(function () {
//$('.txtCalendar').addClass('blockMsg');
        $("#myPicture").on('change', prepareLoad);
        $('#modelBoxId').hide();

    });

    var files = null;
    function prepareLoad(event) {
        console.log(' event fired' + event.target.files[0].name);
        files = event.target.files;
    }

    //save profile picture

    function uploadProfilePicture(data) {
        var customerId = 0;
        var oMyForm = new FormData();
        if (files != null)
            oMyForm.append("file", files[0]);
        else
            oMyForm.append("file", null);

        oMyForm.append("customerId", customerId);
        var guarantor_debtor_type = $("#gurant_debtr_type").val();
        $.ajax({
            url: '/AxaBankFinance/uploadProfilePicture?${_csrf.parameterName}=${_csrf.token}',
            data: oMyForm,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (result) {
                if (result) {
                    console.log("IF" + result);
                    $('#searchCustomerContent').html('');
                    $('#searchCustomerContent').html(data);
                    var url2 = "/AxaBankFinance/viewProfilePicture/" + 0;
                    $("#myProfilePic").html("<img src='" + url2 + "' ></img>");

                    var nic = $("input[name=loanNic]").val();
                    var name = $("input[name=nameWithInitials]").val();
                    var address = $("#loanAddress_id").val();
                    var telephone = $("input[name=loanTelephoneMobile]").val();
                    var debtorId = $("input[name=loanDebtorId]").val();
                    var debtorAccNo = $("input[name=debtorAccNo]").val();

                    if (guarantor_debtor_type == 1) {
                        $("#debID").val(debtorId);
                        $("#debtorNic").val(nic);
                        $("#debtorName").val(name);
                        $("#debtorAddress").val(address);
                        $("#debtorTelephone").val(telephone);
                        $("#txtAccountNo").val(debtorAccNo);

                        var url3 = "/AxaBankFinance/viewProfilePicture/" + debtorId;
                        $("#customerProfilePicture").html("<img src='" + url3 + "' ></img>");
                    }
                } else {
                    alert("save false");
                }
            },
            error: function () {
                var message = "File size exceeds the configured maximum file size";
                var view = "message_newCustomer";
                error(message, view);
            }
        });
    }

    function saveNewDebtor() {

        var ar = [document.getElementById('loanFullName_id'), document.getElementById('loanNic_id'), document.getElementById('loanDob_id'), document.getElementById('memberNumber_id'), document.getElementById('nameIni_id'), document.getElementById('jobInformation_id'), document.getElementById('centerDay_id')];
        var ar1 = [document.getElementById('loanAddress_id'), document.getElementById('loanPostAddress_id')];
        var ar2 = [document.getElementById('loanTelephoneHome_id'), document.getElementById('loanTelephoneOffice_id'), document.getElementById('loanTelephoneMobile_id')];
//        var ar3 = [document.getElementById('dependentName_id'), document.getElementById('dependentNic_id'), document.getElementById('dependentOfficeAddress_id')];

        if (files != null) {
            if (files[0].size > 102400) {
                alert("File size exceeds the configured maximum file size");
                return;
            }
        }

        var jobInfo = $("#jobInformation_id").val();
        var centerDay = document.getElementById("centerDay_id").value;

        if (jobInfo === '') {
            validate = false;
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html("Please Enter the Job Information");
            $("#jobInformation_id").addClass("txtError");
        } else {
            $("#jobInformation_id").removeClass("txtError");
            $(".divError").css({'display': 'none'});
        }

        if (centerDay === '0') {
            validate = false;
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html("Please Select Center Day");
            $("#centerDay_id").addClass("txtError");
        } else {
            $("#centerDay_id").removeClass("txtError");
            $(".divError").css({'display': 'none'});
        }

        if (validateFields(ar) === true && validateOneField(ar1, '.address') === true && validateOneField(ar2, '.number') === true) {
            if (checkNIC("loanNic_id") === true) {
                var guarantor_debtor_type = $("#gurant_debtr_type").val();

                if (guarantor_debtor_type == 2) {
                    $('#loandebId').val(0);
                } else {
                    $('#loandebId').val(1);
                }

                document.getElementById("btnSaveDebtor").disabled = true;
                $.ajax({
                    url: '/AxaBankFinance/sepSaveDebtorFrom',
                    type: 'POST',
                    data: $('#loan_form_temp').serialize(),
                    success: function (data) {
                        uploadProfilePicture(data);
                        document.getElementById("btnSaveDebtor").disabled = false;
                        modelBoxTrigger();
                        $("#loan_form_temp").trigger('reset');
                    },
                    error: function () {
                        alert("errror");
                    }
                });
            }
        } else {

        }

    }

    function modelBoxTrigger() {
        $("#modelBoxId").trigger("click");
    }


    var dependentId = 0, empId = 0, busi = 0, liability = 0, asset = 0;
    function selectTableRow(type, id) {
        var debtorId = $("#txtDebtorId").val();
        if (type == 1) {
            dependentId = id;
            addDependent();
        } else if (type == 2) {
            empId = id;
            addEmplyment();
        } else if (type == 3) {
            busi = id;
            addBusiness();
        } else if (type == 4) {
            liability = id;
            addLiability();
        }
    }
    function selectAssetRow(type, id) {
        addAsset();
        $('#slctAsset').prop('disabled', true);
        if (type == 1) {
            asset = id;
            addAssetBank();
        } else if (type == 2) {
            asset = id;
            addAssetVehicle();
        } else if (type == 3) {
            asset = id;
            addAssetLand();
        }
    }

    function addDependent() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewDependentForm/' + debtorId + "/" + dependentId,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                dependentId = 0;
            }
        });
    }

    function addEmplyment() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewEmplyementForm/' + debtorId + "/" + empId,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                empId = 0;
            }
        });
    }

    function addBusiness() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewBusinessForm/' + debtorId + "/" + busi,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                busi = 0;
            }
        });
    }

    function addLiability() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewLiabilityForm/' + debtorId + "/" + liability,
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
                liability = 0;
            }
        });
    }

    function addAsset() {
        //var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetForms',
            success: function (data) {
                $("#debtorForms").html(data);
                ui('#debtorForms');
            }
        });
    }

    function addAssetBank() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetBankForm/' + debtorId + "/" + asset,
            success: function (data) {
                $("#assetContent").html(data);
                asset = 0;
            }
        });
    }

    function addAssetVehicle() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetVehicleForm/' + debtorId + "/" + asset,
            success: function (data) {
                $("#assetContent").html(data);
                asset = 0;
            }
        });
    }

    function addAssetLand() {
        var debtorId = $("#txtDebtorId").val();
        $.ajax({
            url: '/AxaBankFinance/viewAssetLandForm/' + debtorId + "/" + asset,
            success: function (data) {
                $("#assetContent").html(data);
                asset = 0;
            }
        });
    }

    function checkDebtorByNic(nicNo) {
        nictoLowerCase(nicNo);
        var detorId = document.getElementById('txtDebtorId').value;
        var nic = nicNo.value;
        if (detorId == null || detorId == 0) {
            $.ajax({
                url: '/AxaBankFinance/checkDebtorNic/' + nic,
                success: function (data) {
                    if (data === 1) {
                        $("#msgNicExits").html("Debtor is already exits..!");
                        $("#loanNic_id").addClass("txtError");
                        $("#loanNic_id").focus();
                    } else {
                        $("#loanNic_id").removeClass("txtError");
                        $("#msgNicExits").html("");
                        $("#msgNicExits").removeClass("msgTextField");
                    }
                }
            });
        }
    }

    function memberNumber() {
        $("#memberNumber_id").mask("a*/999/99/999");
    }

    function validateMemberNo() {
        $("#memberNumber_id").mask("a*/999/99/999");
        var branchCodeId = $("#branchCodeId").val();
        var branchCode = $("#memberNumber_id").val();
        var splitBranchCode = branchCode.substring(0, 2);


        if (branchCodeId !== splitBranchCode) {

            validate = false;
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html("Invalid Member Number");
            $("#memberNumber_id").addClass("txtError");
            document.getElementById("btnSaveDebtor").disabled = true;
        } else {
            $("#memberNumber_id").removeClass("txtError");
            $(".divError").css({'display': 'none'});
            document.getElementById("btnSaveDebtor").disabled = false;
        }
    }

</script>
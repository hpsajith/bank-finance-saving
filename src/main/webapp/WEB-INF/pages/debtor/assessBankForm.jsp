<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<!--Asset Bank Form-->
<div class="col-md-10">
    <form name="assetBankDetails" id="formBank" autocomplete="off">
        <input type="hidden" name="debtorId" value="${bank.debtorId}" id="ab_did"/>
        <input type="hidden" name="bankId" value="${bank.bankId}"/>
        <div class="row">
            <div class="col-md-4">Bank Name</div>
            <div class="col-md-8"><input type="text" name="bankName" id="bankName_id" value="${bank.bankName}" style="width: 100%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Branch</div>
            <div class="col-md-8"><input type="text" name="bankBranchName" id="bankBranchName_id" value="${bank.bankBranchName}" style="width: 100%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Account Type</div>
            <div class="col-md-8"><input type="text" name="bankAccountType" id="bankAccountType_id" value="${bank.bankAccountType}" style="width: 100%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Account No</div>
            <div class="col-md-8"><input type="text" name="bankAccountNo" id="bankAccountNo_id" value="${bank.bankAccountNo}" style="width: 100%"/></div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unLoadForm($('#ab_did').val())"/>
                </div>
                <div class="col-md-4">
                    <input type="button" class="btn btn-default col-md-12" value="Save" onclick="saveUpdateAssetBank()"/>
                </div>
            </div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>

<script>
    function saveUpdateAssetBank() {

        var ar = [document.getElementById('bankName_id'), document.getElementById('bankBranchName_id'), document.getElementById('bankAccountType_id'), document.getElementById('bankAccountNo_id')];
        if (validateFields(ar) === true) {
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateAssetBank',
                type: 'POST',
                data: $("#formBank").serialize(),
                success: function(data) {
                    alert(data);
                    var dId = $('#ab_did').val();
                    unLoadForm(dId);
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        }
    }
</script>
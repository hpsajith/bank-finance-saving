<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<!--Asset Land Form-->
<div class="col-md-10">
    <form name="assetLandDetails" id="formLand" autocomplete="off">
        <input type="hidden" name="debtorId" value="${land.debtorId}" id="al_did"/>
        <input type="hidden" name="realEstateId" value="${land.realEstateId}"/>
        <div class="row">
            <div class="col-md-4">Deed No</div>
            <div class="col-md-8"><input type="text" name="deedNo" id="deedNo_id" value="${land.deedNo}" style="width: 100%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Location</div>
            <div class="col-md-8"><input type="text" name="landPlace" id="landPlace_id" value="${land.landPlace}" style="width: 100%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Commissioner Name</div>
            <div class="col-md-8"><input type="text" name="commisionerName" id="commisionerName_id" value="${land.commisionerName}" style="width: 100%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Registration Date</div>
            <div class="col-md-8"><input type="text" class="txtCalendar" name="RDate" id="RDate_id" value="${land.RDate}" style="width: 100%"/></div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unLoadForm($('#al_did').val())"/>
                </div>
                <div class="col-md-4">
                    <input type="button" class="btn btn-default col-md-12" value="Save" onclick="saveUpdateAssetLand()"/>
                </div>
            </div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>

<script>
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            yearRange: '1920:2015'
        });
    });
    function saveUpdateAssetLand() {

        var ar = [document.getElementById('deedNo_id'), document.getElementById('landPlace_id'), document.getElementById('commisionerName_id'), document.getElementById('RDate_id')];
        if (validateFields(ar) === true) {
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateAssetLand',
                type: 'POST',
                data: $("#formLand").serialize(),
                success: function(data) {
                    alert(data);
                    var dId = $("#al_did").val();
                    unLoadForm(dId);
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        }
    }
</script>
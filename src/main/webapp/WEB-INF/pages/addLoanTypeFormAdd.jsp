<%-- 
    Document   : addLoanTypeFormAdd
    Created on : Mar 13, 2015, 12:49:39 PM
    Author     : SOFT
--%>
<form name="addLoanTypeForm" id="addLoanType">
    <div class="row" style="margin: 0 8 2 0"><legend>Add Loan Type</legend></div>
    <div class="row">
        <input type="hidden" name="loanTypeId" value="${loanType.loanTypeId}"/>
        <div class="col-md-5 col-sm-5 col-lg-5 ">Loan Type Name</div> 
        <div class="col-md-7 col-sm-7 col-lg-7"><input type="text"name="loanTypeName" id="txtLoanTypeName" style="width: 100%" value="${loanType.loanTypeName}"/></div> 
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5 ">Loan Type Code</div>    
        <div class="col-md-7 col-sm-7 col-lg-7"><input type="text"name="loanTypeCode" id="txtLoanTypeCode" style="width: 100%" value="${loanType.loanTypeCode}"/></div> 
    </div>
    <div class="row" style="margin : 20 0 10 0;overflow-y: auto; height: 500px">
        <table id="viewLoanCheckList" class="table tab-content table-bordered table-responsive table-edit table-fixed-header">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Description</th>
                    <th>compulsory</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="row" id="hideCheckList"></div>

    <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
        <div  class="col-md-6 col-sm-6 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Save" onclick="saveUpdateLoanType()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<script type="text/javascript">
    function saveUpdateLoanType() {
        if (fieldValidate()) {
            setCheckList();
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateLoanType',
                type: 'POST',
                data: $('#addLoanType').serialize(),
                success: function(data) {
                    alert(data);
                    loadAddLoanTypeForm();
                },
                error: function() {
                    alert("Error Loading..!");
                }
            });
        }
    }
    function setCheckList() {
        var i = 0;
        $('#viewLoanCheckList tbody tr').each(function(i) {
            if ($('#checkList' + i).is(':checked')) {
                var checkId = $('#checkList' + i).val();
                var compulsory = $('#com' + i).find("option:selected").val();
                $("#hideCheckList").append("<input type = 'hidden' name = 'checkListID' value = '" + checkId + "'/>" +
                        "<input type = 'hidden' name = 'checkCompulsory' value = '" + compulsory + "' />");
            }
            i++;
        });
    }
    function fieldValidate() {
        var loanTypeName = document.getElementById('txtLoanTypeName');
        var loanTypeCode = document.getElementById('txtLoanTypeCode');
        if (loanTypeName.value == null || loanTypeName.value == "") {
            alert("Loan Type Name must be filled out");
            $("#txtLoanTypeName").addClass("txtError");
            return false;
        } else {
            $("#txtLoanTypeName").removeClass("txtError");
        }
        if (loanTypeCode.value == null || loanTypeCode.value == "") {
            alert("Loan Type Code must be filled out");
            $("#txtLoanTypeCode").addClass("txtError");
            return false;
        } else {
            $("#txtLoanTypeCode").removeClass("txtError");
        }
        return true;
    }
    $(function() {
        //load checkList
        $.getJSON('/AxaBankFinance/loadCheckList', function(data) {
            for (var i = 0; i < data.length; i++) {
                var checkListId = data[i].id;
                var description = data[i].listDescription;
                $('#viewLoanCheckList tbody').append("<tr id='" + checkListId + "'>" +
                        "<td><input type='checkbox' id ='checkList" + i + "' value='" + checkListId + "' /></td>" +
                        "<td>" + description + "</td>" +
                        "<td><select id ='com" + i + "'><option value='0'>No</option><option value ='1'>Yes</option></select>");
            }
        });
    });
</script>

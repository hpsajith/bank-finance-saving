<%-- 
    Document   : addUserMessageForm
    Created on : May 11, 2016, 9:03:06 AM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Add User Message</strong>
    </div>
    <form name="userMessageForm" id="addUserMessageForm">
        <input type="hidden" id="txtUserMessageId" name="id" value="${userMessage.id}"/>
        <input type="hidden" id="txtIsRead" name="isRead" value="${userMessage.isRead}"/>
        <div class="col-md-12">
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Subject</div>
                <div class="col-md-10">
                    <input type="text" id="txtSubject" name="subject" value="${userMessage.subject}" style="width: 100%"/>
                    <label id="msgTxtSubject" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Message</div>
                <div class="col-md-10">
                    <textarea rows="6" id="txtMessage" name="message" style="width: 100%">${userMessage.message}</textarea>
                    <label id="msgTxtMessage" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-md-2">To</div>
                <div class="col-md-10">
                    <select id="inputUser" name="userId" style="width: 100%">
                        <option value="0">-- SELECT --</option>
                        <c:forEach var="user" items="${users}">
                            <c:choose>
                                <c:when test="${userMessage.umUserByUserId.userId == user.userId}">
                                    <option value="${user.userId}" selected>${user.userName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${user.userId}">${user.userName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="saveUserMessage()"></div>
            </div>
        </div>  
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div> 
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    function formValidate() {
        return true;
    }

    function saveUserMessage() {
        if (formValidate()) {
            $.ajax({
                url: "/AxaBankFinance/UtilityController/saveOrUpdateUserMessage",
                type: 'POST',
                data: $("#addUserMessageForm").serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadMessageMain();
                    }, 1500);
                },
                error: function() {
                    alert("Error Loding..");
                }
            });
        }
    }


</script>

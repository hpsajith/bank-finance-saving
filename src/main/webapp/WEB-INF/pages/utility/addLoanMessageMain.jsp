<%-- 
    Document   : addLoanMessageMain
    Created on : Jul 26, 2016, 3:48:53 PM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row" style="padding: 10px">
    <div class="row" style="margin: 10px; padding: 5px; border: 1px solid #08C; font-size: 12px;">
        <div class="row"><input type="hidden" id="hidTxtLoanId" value="${loanDetails.loanId}" /></div>
        <div class="col-md-12"><legend><strong>Notes and Messages</strong></legend></div>
        <div class="row" style="color: #8a6d3b;text-align: left; font-weight: bold; margin-left: 30px; font-size: 15px">
            Agreement No - ${loanDetails.loanAgreementNo}
        </div>
        <div class="col-md-12" style="overflow-y: auto; height: 300px;"> 
            <table class="table table-bordered table-edit table-hover table-responsive dataTable">
                <thead>
                    <tr>
                        <th style="text-align: center">Date of Note</th>
                        <th style="text-align: center">Message</th>
                        <th style="text-align: center">Officer Code</th>
                        <th style="text-align: center">Edit</th>
                        <th style="text-align: center">Delete</th>                                    
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="loanMessage" items="${loanMessages}">
                        <tr id="${loanMessage.id}">
                            <td style="text-align: center">${loanMessage.publishDate}</td>
                            <td style="text-align: left">${loanMessage.message}</td>
                            <td style="text-align: center">${loanMessage.umUser.userName}</td>
                            <td style="text-align: center">
                                <div class='edit' title="Edit" href='#' onclick="editLoanMessage(${loanMessage.id})"></div>
                            </td>
                            <td style="text-align: center">
                                <div class='delete' title="Delete" href='#' onclick="deleteLoanMessage(${loanMessage.id})"></div>
                            </td>                                            
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>  
    </div>
    <div class="col-md-12 panel-footer">
        <div class="col-md-8">           
        </div>
        <div class="col-md-2">
            <button class="btn btn-default col-md-12" onclick="loadLoanMessageForm()">Add New</button>
        </div>
        <div class="col-md-2">
            <button class="btn btn-default col-md-12" onclick="unblockui()">Cancel</button>
        </div>
    </div>
</div>
<div id="divLoanMessageForm" class="searchCustomer" style="width: 40%;margin: 5% 10%; border: 0px solid #5e5e5e;">
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    function loadLoanMessageForm() {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadLoanMessageForm",
            type: 'GET',
            success: function (data) {
                $('#divLoanMessageForm').html(data);
                $("#txtLoanAggNo").val('${loanDetails.loanAgreementNo}');
                $("#txtLoanId").val('${loanDetails.loanId}');
                $("#txtType").val('1');
                ui('#divLoanMessageForm');
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function editLoanMessage(loanMessageId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadLoanMessageForm/" + loanMessageId,
            type: 'GET',
            success: function (data) {
                $('#divLoanMessageForm').html(data);
                $("#txtType").val('1');
                ui('#divLoanMessageForm');
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function deleteLoanMessage(loanMessageId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/deleteLoanMessage/" + loanMessageId,
            type: 'GET',
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

</script>

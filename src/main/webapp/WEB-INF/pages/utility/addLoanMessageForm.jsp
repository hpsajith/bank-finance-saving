<%-- 
    Document   : addLoanMessageForm
    Created on : May 11, 2016, 9:03:58 AM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Add Loan Message</strong>
    </div>
    <form name="loanMessageForm" id="addLoanMessageForm">
        <input type="hidden" id="txtUserMessageId" name="id" value="${loanMessage.id}"/>
        <input type="hidden" id="txtIsActive" name="isActive" value="${loanMessage.isActive}"/>
        <input type="hidden" id="txtType"/>
        <div class="col-md-12">
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Subject</div>
                <div class="col-md-10">
                    <input type="text" id="txtSubject" name="subject" value="${loanMessage.subject}" style="width: 100%"/>
                    <label id="msgTxtSubject" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Message</div>
                <div class="col-md-10">
                    <textarea rows="6" id="txtMessage" name="message" style="width: 100%">${loanMessage.message}</textarea>
                    <label id="msgTxtMessage" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-md-2">Loan</div>
                <div class="col-md-10">
                    <input type="text" id="txtLoanAggNo" style="width: 100%" value="${loanMessage.loanHeaderDetails.loanAgreementNo}"/>
                    <input type="hidden" id="txtLoanId" name="loanId" value="${loanMessage.loanHeaderDetails.loanId}"/>
                </div>
            </div>
            <!--            <div class="row" style="margin-top: 2px" id="divSearchLoan">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <select id="inpLoan" style="width: 100%" disabled>
            <c:if test="${loanMessage!=null}">
                <option value="${loanMessage.loanHeaderDetails.loanId}">${loanMessage.loanHeaderDetails.loanAgreementNo}</option>
            </c:if>
        </select>
    </div>
</div>-->
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="saveLoanMessage()"></div>
            </div>
        </div>  
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div> 
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    function formValidate() {
        return true;
    }

    function saveLoanMessage() {
        if (formValidate()) {
            var type = $("#txtType").val();
            $.ajax({
                url: "/AxaBankFinance/UtilityController/saveOrUpdateLoanMessage",
                type: 'POST',
                data: $("#addLoanMessageForm").serialize(),
                success: function (data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    if (type !== '1') {
                        setTimeout(function () {
                            loadMessageMain();
                        }, 1500);
                    }
                },
                error: function () {
                    alert("Error Loding..");
                }
            });
        }
    }

    function searchLoan() {
        var loanAggNo = $("#txtLoanAggNo").val().trim();
        if (loanAggNo !== null && loanAggNo !== "") {
            $.ajax({
                url: "/AxaBankFinance/findLoanHeaderDetailseByAggNo",
                type: 'GET',
                data: {"aggNo": loanAggNo},
                success: function (data) {
                    var suggesstion = "<option value='0'>--SELECT--</option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            suggesstion = suggesstion + "<option value='" + data[i].loanId + "' onclick='selectLoan(" + data[i].loanId + ")'>" + data[i].loanAgreementNo + "</option>";
                        }
                        $("#inpLoan").html("");
                        $("#inpLoan").html(suggesstion);
                        $("#inpLoan").removeAttr("disabled");
                    }
                }
            });
        } else {
            $("#inpLoan").html("");
            $("#inpLoan").attr("disabled", true);
        }
    }

    function selectLoan(loanId) {
        $("#txtLoanId").val(loanId);
        $("#txtLoanAggNo").val("");
    }


</script>



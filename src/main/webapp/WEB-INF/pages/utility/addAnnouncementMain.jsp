<%-- 
    Document   : addAnnouncementMain
    Created on : May 9, 2016, 11:58:32 AM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<c:set var="today" value="<%= new java.util.Date()%>"></c:set>
    <script type="text/javascript">
        $(document).ready(function() {
            pageAuthentication();
            unblockui();
        });
    </script>
    <div class="container-fluid" style="margin-top: 20px">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">Post Announcements</div>
                <div class="panel-body">
                <c:if test="${isAdmin}">
                    <div class="row" style="margin-bottom: 12px">
                        <div class="col-md-4">
                            <input type="button" id="btnAddAnnouncement" value="Add New" onclick="loadAnnouncementForm()" class="btn btn-success"/>
                        </div>
                        <div class="col-md-8"></div>
                    </div>
                </c:if>
                <div class="panel-group" id="accordion">
                    <label style="color: #4a9fe9"><strong>Today - (<f:formatDate pattern="yyyy-MM-dd" value="${today}"></f:formatDate>)</strong></label>
                    <c:forEach var="announcement" items="${announcements}" varStatus="status">
                        <c:choose>
                            <c:when test="${status.first}">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_${announcement.id}">${announcement.subject}</a>
                                        </h3>
                                        <c:if test="${isAdmin}">
                                            <div class='delete pull-right' title="Delete" href='#' onclick="deleteAnnouncement(${announcement.id})" style="margin-top: 3px"></div>
                                            <div class='edit pull-right' title="Edit" href='#' onclick="editAnnouncement(${announcement.id})" style="margin-right: 15px; margin-top: 3px"></div>
                                        </c:if>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="collapse_${announcement.id}" class="panel-collapse collapse in">
                                        <div class="panel-body">${announcement.announcement}</div>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_${announcement.id}">${announcement.subject}</a>
                                        </h3>
                                        <c:if test="${isAdmin}">
                                            <div class='delete pull-right' title="Delete" href='#' onclick="deleteAnnouncement(${announcement.id})" style="margin-top: 3px"></div>
                                            <div class='edit pull-right' title="Edit" href='#' onclick="editAnnouncement(${announcement.id})" style="margin-right: 15px; margin-top: 3px"></div>
                                        </c:if>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="collapse_${announcement.id}" class="panel-collapse collapse">
                                        <div class="panel-body">${announcement.announcement}</div>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <div id="prvAnnouncements"></div>
                </div> 
                <div class="row">
                    <div class="col-md-4">
                        <a href="#" id="linkShowEarlier" onclick="loadPreviousAnnouncements()">Show Previous</a>
                    </div>
                    <div class="col-md-8"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divAnnouncementForm" class="searchCustomer" style="width: 40%;margin: 5% 10%; border: 0px solid #5e5e5e;">
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    function loadAnnouncementForm() {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadAnnouncementForm",
            type: 'GET',
            success: function(data) {
                $('#divAnnouncementForm').html(data);
                ui('#divAnnouncementForm');
            },
            error: function() {
                alert("Error Loding..");
            }
        });
    }

    function editAnnouncement(announcementId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadAnnouncementForm/" + announcementId,
            type: 'GET',
            success: function(data) {
                $('#divAnnouncementForm').html(data);
                ui('#divAnnouncementForm');
            },
            error: function() {
                alert("Error Loding..");
            }
        });
    }

    function deleteAnnouncement(announcementId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/deleteAnnouncement/" + announcementId,
            type: 'GET',
            success: function(data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function() {
                    loadAnnouncementMain();
                }, 1500);
            },
            error: function() {
                alert("Error Loding..");
            }
        });
    }

    function loadPreviousAnnouncements() {
        ui("#proccessingPage");
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadPreviousAnnouncementMain",
            type: 'GET',
            success: function(data) {
                $('#prvAnnouncements').html(data);
            },
            error: function() {
                alert("Error Loding..");
            }
        });
    }

</script>
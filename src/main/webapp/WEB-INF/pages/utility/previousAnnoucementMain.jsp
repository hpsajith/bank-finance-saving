<%-- 
    Document   : previousAnnoucementMain
    Created on : May 9, 2016, 12:36:45 PM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<c:set var="today" value="<%=new java.util.Date()%>"/>
<c:set var="yesterday" value="<%=new java.util.Date(new java.util.Date().getTime() - 60 * 60 * 24 * 1000)%>"/>
<script type="text/javascript">
    $(document).ready(function() {
        unblockui();
    });
</script>
<label style="margin-top: 10px; color: #8a6d3b"><strong>Yesterday - (<f:formatDate pattern="yyyy-MM-dd" value="${yesterday}"></f:formatDate>)</strong></label>
<c:forEach var="announcement" items="${announcements}">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title pull-left">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse_${announcement.id}">${announcement.subject}</a>
            </h3>
            <c:if test="${isAdmin}">
                <div class='delete pull-right' title="Delete" href='#' onclick="deleteAnnouncement(${announcement.id})" style="margin-top: 3px"></div>
                <div class='edit pull-right' title="Edit" href='#' onclick="editAnnouncement(${announcement.id})" style="margin-right: 15px; margin-top: 3px"></div>
            </c:if>
                <div class="clearfix"></div>
        </div>
        <div id="collapse_${announcement.id}" class="panel-collapse collapse">
            <div class="panel-body">${announcement.announcement}</div>
        </div>
    </div>
</c:forEach>
<%-- 
    Document   : addMessageMain
    Created on : May 11, 2016, 9:02:35 AM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<c:set var="today" value="<%= new java.util.Date()%>"></c:set>
    <script type="text/javascript">
        $(document).ready(function () {
            pageAuthentication();
            $("#tblUserMessage").chromatable({
                width: "100%", // specify 100%, auto, or a fixed pixel amount
                height: "25%",
                scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
            });
            $("#tblLoanMessage").chromatable({
                width: "100%", // specify 100%, auto, or a fixed pixel amount
                height: "25%",
                scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
            });
            unblockui();
        });
    </script>
    <div class="container-fluid" style="margin-top: 20px">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">Add Note/Messages</div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom: 12px">
                        <div class="col-md-4">
                            <input type="button" id="btnAddUserMessage" value="Add New" onclick="loadUserMessageForm()" class="btn btn-success"/>
                        </div>
                        <div class="col-md-8"></div>
                    </div>
                    <label style="color: #4a9fe9"><strong>User Related Messages - (<f:formatDate pattern="yyyy-MM-dd" value="${today}"></f:formatDate>)</strong></label>
                    <div class="row fixed-table" id="divUserMessage" style="overflow-y: auto">
                        <div class="table-content">
                            <table id="tblUserMessage" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">To</th>
                                        <th style="text-align: center">From</th>
                                        <th style="text-align: center">About</th>
                                        <th style="text-align: center">Publish Date</th>
                                        <th style="text-align: center">Status</th>
                                        <th style="text-align: center">Edit</th>
                                        <th style="text-align: center">Delete</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="userMessage" items="${userMessages}">
                                    <c:if test="${userMessage.umUserByUId.userId == userId}">
                                        <tr id="${userMessage.id}">
                                            <td style="text-align: center">${userMessage.umUserByUserId.userName}</td>
                                            <td style="text-align: center">${userMessage.umUserByUId.userName}</td>
                                            <td style="text-align: center">${userMessage.subject}</td>
                                            <td style="text-align: center">${userMessage.publishDate}</td>
                                            <td style="text-align: center">
                                                <c:choose>
                                                    <c:when test="${userMessage.isRead}">
                                                        <label class="underline-blue" style="font-style: italic">Read</label>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <label class="underline">Not Read</label>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td style="text-align: center">
                                                <div class='edit' title="Edit" href='#' onclick="editUserMessage(${userMessage.id})"></div>
                                            </td>
                                            <td style="text-align: center">
                                                <div class='delete' title="Delete" href='#' onclick="deleteUserMessage(${userMessage.id})"></div>
                                            </td>                                            
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div class="dc_clear"></div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 12px;margin-top: 12px">
                    <div class="col-md-4">
                        <input type="button" id="btnAddLoanMessage" value="Add New" onclick="loadLoanMessageForm()" class="btn btn-success"/>
                    </div>
                    <div class="col-md-8"></div>
                </div>
                <label style="color: #4a9fe9"><strong>Loan Related Messages - (<f:formatDate pattern="yyyy-MM-dd" value="${today}"></f:formatDate>)</strong></label>
                    <div class="row fixed-table" id="divLoanMessage" style="overflow-y: auto">
                        <div class="table-content">
                            <table id="tblLoanMessage" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">Agreement No</th>
                                        <th style="text-align: center">Debtor</th>
                                        <th style="text-align: center">About</th>
                                        <th style="text-align: center">Publish Date</th>
                                        <th style="text-align: center">Status</th>
                                        <th style="text-align: center">Edit</th>
                                        <th style="text-align: center">Delete</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="loanMessage" items="${loanMessages}">
                                    <tr id="${loanMessage.id}">
                                        <td style="text-align: center">${loanMessage.loanHeaderDetails.loanAgreementNo}</td>
                                        <td style="text-align: center">${loanMessage.loanHeaderDetails.debtorHeaderDetails.nameWithInitial}</td>
                                        <td style="text-align: center">${loanMessage.subject}</td>
                                        <td style="text-align: center">${loanMessage.publishDate}</td>
                                        <td style="text-align: center">
                                            <c:if test="${loanMessage.isActive}">
                                                <label class="underline-blue" style="font-style: italic">Active</label>
                                            </c:if>
                                        </td>
                                        <td style="text-align: center">
                                            <div class='edit' title="Edit" href='#' onclick="editLoanMessage(${loanMessage.id})"></div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class='delete' title="Delete" href='#' onclick="deleteLoanMessage(${loanMessage.id})"></div>
                                        </td>                                            
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div class="dc_clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divUserMessageForm" class="searchCustomer" style="width: 40%;margin: 5% 10%; border: 0px solid #5e5e5e;">
</div>
<div id="divLoanMessageForm" class="searchCustomer" style="width: 40%;margin: 5% 10%; border: 0px solid #5e5e5e;">
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    function loadUserMessageForm() {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadUserMessageForm",
            type: 'GET',
            success: function (data) {
                $('#divUserMessageForm').html(data);
                ui('#divUserMessageForm');
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function editUserMessage(userMessageId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadUserMessageForm/" + userMessageId,
            type: 'GET',
            success: function (data) {
                $('#divUserMessageForm').html(data);
                ui('#divUserMessageForm');
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function deleteUserMessage(userMessageId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/deleteUserMessage/" + userMessageId,
            type: 'GET',
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function () {
                    loadMessageMain();
                }, 1500);
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }


    function loadLoanMessageForm() {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadLoanMessageForm",
            type: 'GET',
            success: function (data) {
                $('#divLoanMessageForm').html(data);
                ui('#divLoanMessageForm');
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function editLoanMessage(loanMessageId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/loadLoanMessageForm/" + loanMessageId,
            type: 'GET',
            success: function (data) {
                $('#divLoanMessageForm').html(data);
                ui('#divLoanMessageForm');
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function deleteLoanMessage(loanMessageId) {
        $.ajax({
            url: "/AxaBankFinance/UtilityController/deleteLoanMessage/" + loanMessageId,
            type: 'GET',
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function () {
                    loadMessageMain();
                }, 1500);
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

</script>

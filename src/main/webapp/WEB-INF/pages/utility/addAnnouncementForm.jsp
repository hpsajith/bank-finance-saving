<%-- 
    Document   : addAnnouncementForm
    Created on : May 9, 2016, 11:58:51 AM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Add Announcement</strong>
    </div>
    <form name="announcementForm" id="addAnnouncementForm">
        <input type="hidden" id="txtAnnouncementId" name="id" value="${announcement.id}"/>
        <div class="col-md-12">
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Subject</div>
                <div class="col-md-10">
                    <input type="text" id="txtSubject" name="subject" value="${announcement.subject}" style="width: 100%"/>
                    <label id="msgTxtSubject" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Message</div>
                <div class="col-md-10">
                    <textarea rows="6" id="txtAnnouncement" name="announcement" style="width: 100%">${announcement.announcement}</textarea>
                    <label id="msgTxtAnnouncement" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;height: 300px; overflow-y: scroll">
                <div class="col-md-2">Assign</div>
                <div class="col-md-10">
                    <table id="tblUserType" class="table table-bordered table-edit table-hover table-responsive dataTable">
                        <thead>
                            <tr>
                                <td style="text-align: center"><input type="checkbox" id="selectAll"/></td>
                                <td style="text-align: center">User Type</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="userType" items="${userTypes}">
                                <c:set var="isCheck" value="false"></c:set>
                                <c:forEach var="annUserType" items="${announcement.umUserTypeAnnouncements}">
                                    <c:if test="${annUserType.umUserType.userTypeId == userType.userTypeId}">
                                        <c:set var="isCheck" value="true"></c:set>
                                    </c:if>
                                </c:forEach>
                                <tr id="${userType.userTypeId}">
                                    <c:choose>
                                        <c:when test="${isCheck}">
                                            <td style="text-align: center"><input type="checkbox" class="case" id="check_${userType.userTypeId}" name="case" checked="true"/></td>
                                            </c:when>
                                            <c:otherwise>
                                            <td style="text-align: center"><input type="checkbox" class="case" id="check_${userType.userTypeId}" name="case" /></td>
                                            </c:otherwise>
                                        </c:choose>
                                    <td style="text-align: center">${userType.userTypeName}</td>
                                </tr>
                                <c:set var="isCheck" value="false"></c:set>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="hidUserTypes"></div>
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="saveAnnouncement()"></div>
            </div>
        </div>  
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div> 
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    $(function() {
        if ($("#txtAnnouncementId").val() === "") {
            $('.case').attr('checked', true);
        }

        $("#selectAll").click(function() {
            $('.case').attr('checked', this.checked);
        });

        $(".case").click(function() {
            if ($(".case").length === $(".case:checked").length) {
                $("#selectAll").attr("checked", "checked");
            } else {
                $("#selectAll").removeAttr("checked");
            }
        });
    });

    function formValidate() {
        return true;
    }

    function saveAnnouncement() {
        if (formValidate()) {
            setUserTypes();
            $.ajax({
                url: "/AxaBankFinance/UtilityController/saveOrUpdateAnnouncement",
                type: 'POST',
                data: $("#addAnnouncementForm").serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadAnnouncementMain();
                    }, 1500);
                },
                error: function() {
                    alert("Error Loding..");
                }
            });
        }
    }

    function setUserTypes() {
        $('#hidUserTypes').html("");
        $('#tblUserType tbody tr').each(function() {
            var userType = $(this).attr('id');
            if ($('#check_' + userType).is(':checked')) {
                $('#hidUserTypes').append("<input type = 'hidden' name = 'userTypes' value = '" + userType + "'/>");
            }
        });
    }

</script>

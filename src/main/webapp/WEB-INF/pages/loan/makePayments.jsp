<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        var voucherNo = $("#voucherNoId").val();
        if (voucherNo === null || voucherNo === '') {
            $("#btnPrintVoucher").prop("disabled", false);

        } else {
            $("#btnPrintCheq").prop("disabled", true);
            $("#btnPrintVoucher").prop("disabled", true);

        }
        var payingAmount = $("#payingAmountTxt").val();
        if (payingAmount > 0) {
            $("#btnPrintCheq").prop("disabled", false);
        } else {
            $("#btnPrintCheq").prop("disabled", true);
        }
    });
</script>
<div class="row">
    <div class="col-md-12"> 
        <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
            <strong>Make Payments</strong>
        </div>
        <form id="formPaymentData" name="paymentDetails" method="POST" action="/AxaBankFinance/ReportController/generateLoanVoucher" target="blank">
            <input type="hidden" name="loanId" value="${loanId}" />     
            <input type="hidden" name="cheqToName" id="cheque_toName1" />
            <input type="hidden" id="ChbIsACPay1" name="ChbIsACPay" />
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-7" id="paymntSuccess">

                </div>
                <div class="col-md-5">
                    <c:if test="${voucherNo!=''}">
                        <label style="font-weight: bold; font-size: 14px;">Voucher No:${voucherNo}</label>
                    </c:if>
                </div>
            </div>
            <input type="hidden" id="voucherNoId" name="voucherNo" value="${voucherNo}"/>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3"  style="text-align: left">
                    <label>Total Amount</label>
                </div>
                <div class="col-md-4">
                    <input type="text" style="text-align: right" id="totAmtId" name="totalAmount" value="${total}" readonly/>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"  style="text-align: left">
                    <label>Capitalize Amount</label>
                </div>
                <div class="col-md-4">
                    <input type="text" style="text-align: right" id="capitaAmtId" name="capitalizeAmount" value="${capitalizeAmount}" readonly/>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <c:choose>
                <c:when test="${downpayment>0}">
                    <div class="row">
                        <div class="col-md-3"  style="text-align: left">
                            <label>Down Payment</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text"  style="text-align: right" name="downPayment" id="dwnPaymentTxtId" value="${downpayment}" readonly/>
                        </div>
                        <div class="col-md-5">
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-3"  style="text-align: left">
                            <label>Down Payment</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text"  style="text-align: right" name="downPayment" value="0" readonly/>
                        </div>
                        <div class="col-md-5">
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${other>0}">
                    <div class="row">
                        <div class="col-md-3"  style="text-align: left">
                            <label>Other Charges</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text"  style="text-align: right" name="OtherCharge" id="itherChargeTxt" value="${other}" readonly/>
                        </div>
                        <div class="col-md-5">
                        </div>
                    </div>
                </c:when>
            </c:choose>


            <div class="row">
                <div class="col-md-3"  style="text-align: left">
                    <label>Insurance Charge</label>
                </div>
                <div class="col-md-4">
                    <input type="text"  style="text-align: right" name="" id="insuChargeTxt" value="${insuCharge}" readonly/>
                </div>
                <div class="col-md-5">
                </div>
            </div>

            <c:choose>
                <c:when test="${adjustment!=0}">
                    <div class="row">
                        <div class="col-md-3"  style="text-align: left">
                            <label>Payment Adjustment</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text"  style="text-align: right" name="adjustment" value="${adjustment}" readonly/>
                        </div>
                        <div class="col-md-5">
                        </div>
                    </div>
                </c:when>
            </c:choose>

            <div class="row">
                <!--                <div class="col-md-3"  style="text-align: left">
                                    <label>Advanced Payment</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text"  style="text-align: right" name="adpayment" value="${adPayment}" readonly/>
                                </div>-->
                <!--                <div class="col-md-2">
                <c:choose>
                    <c:when test="${isPaid}">
                        <label style="color: blue; font-weight: bold; margin-left: -110px;">Paid</label>
                    </c:when>
                    <c:otherwise>
                        <label style="color: goldenrod; font-weight: bold;  margin-left: -110px;">Not Paid</label>
                    </c:otherwise>
                </c:choose>
            </div>-->
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"  style="text-align: left">
                    <label>Paid Amount</label>
                </div>
                <div class="col-md-4">
                    <input type="text"  style="text-align: right" name="paidAmount" id="paidAmtIdTxt" value="${paidAmount}" readonly/>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"  style="text-align: left">
                    <label>Balance</label>
                </div>
                <div class="col-md-4">
                    <input type="text" style="text-align: right" id="txtBalanceAmount"  name="balanceAmount" value="${balance}" readonly/>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"  style="text-align: left">
                    <label>Payment Type</label>
                </div>
                <div class="col-md-4">
                    <select id="paymentsTypes" style="width: 75%" name="paymentType">                   
                        <option value="1">Cash</option>
                        <option value="2">Cheque</option>
                        <option value="3">Direct Deposit</option>
                    </select>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <div class="row" id="cheqDetailsDiv" style="margin: 20px; border: 1px solid #999; padding: 5px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-1"><input type="radio" name="cheqName" value="0" checked="" onclick="checkType(0)"/></div>
                        <div class="col-md-2"><label>Customer</label></div>
                        <div class="col-md-1"><input type="radio" name="cheqName" value="1" onclick="checkType(0)"/></div>
                        <div class="col-md-2"><label>Dealer</label></div>
                        <div class="col-md-1"><input type="radio" name="cheqName" value="2" onclick="checkType(0)"/></div>
                        <div class="col-md-2"><label>Cash</label></div>
                        <c:if test="${voucherNo!=''}">
                            <c:if test="${capitalizeAmount > 0}">
                                <div class="col-md-1"><input type="radio" name="cheqName" value="2" onclick="checkType(1)"/></div>
                                <div class="col-md-2"><label>Capitalize</label></div>
                            </c:if>
                        </c:if>
                    </div> 
                    <div class="row" id="divCapitalize" style="display: none">
                        <div class="col-md-3" style="text-align: left"><label>Capitalize Type:</label></div>
                        <div class="col-md-4">
                            <select name="capitalizeType" id="inputCapitalizeType" style="width: 100%" onchange="changeCapitalizeType()">
                                <option value="0">-- SELECT --</option>
                                <c:forEach var="capitalizeDetail" items="${capitalizeDetails}">
                                    <option value="${capitalizeDetail.amount}">${capitalizeDetail.MCaptilizeType.capitalizeType}</option>
                                </c:forEach>
                            </select>
                        </div>                    
                        <div class="col-md-5"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3" style="text-align: left"><label>Cheque Date:</label></div>
                        <div class="col-md-4"><input class="txtCalendar" type="text" style="width: 100%" value="" name="expireDate" id="exDate"/></div>
                        <div class="col-md-3" style="text-align: left">
                            <input type="checkbox" id="chqIsFull"/>Is Full Year
                            <input type="hidden" name="amountInTxt" id="amountInword" />
                        </div>  
                        <div class="col-md-2"></div>

                    </div>
                    <div class="row">
                        <div class="col-md-3" style="text-align: left"><label>Cheque No<strong style="color: red">*</strong>:</label></div>
                        <div class="col-md-4"><input id="txtCheqNo" type="text" value="" name="chekNo" style="width: 100%"/></div>                    
                        <div class="col-md-5"></div>

                    </div>
                    <div class="row">
                        <div class="col-md-3" style="text-align: left"><label>Bank Name:</label></div>
                        <div class="col-md-4">
                            <select name="bankAccount" id="bankIdTxt" style="width: 100%">  
                                <c:forEach var="bank" items="${banks}">
                                    <option value="${bank.bankId}">${bank.configBankName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-md-3" style="text-align: left">
                            <input type="checkbox" id="accPyOnly"/>A/C Payee Only </div>
                        <div class="col-md-2"></div>

                    </div>
                </div>
            </div>


            <%--//----%>
            <div class="row" id="directDepositDiv" style="margin: 20px; border: 1px solid #999; padding: 5px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3" style="text-align: left"><label>Bank:</label></div>
                        <div class="col-md-4"><input type="text" readonly="true" style="width: 100%" value="${bankBranch}" name="expireDate" id="exDate"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3" style="text-align: left"><label>Bank A/C Name:</label></div>
                        <div class="col-md-4"><input id="accountName_id" readonly="true" type="text" value="${accountName}" name="chekNo" style="width: 100%"/></div>
                        <div class="col-md-5"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3" style="text-align: left"><label>Bank A/C No:</label></div>
                        <div class="col-md-4">
                        <input id="accountNo_id" type="text" readonly="true" value="${accountNo}" name="chekNo" style="width: 100%"/>
                        </div>
                    </div>
                </div>
            </div>
            <%--//----%>

            <div class="row" style="margin-top: 10px;" >
                <div class="col-md-3"  style="text-align: left">
                    <label>Paying Amount<strong style="color: red">*</strong></label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="payingAmount" id="payingAmountTxt" value="${balance}" style="font-weight: bold; text-align: right;" onkeypress="return checkNumbers(this)" maxlength="15"/>
                </div>
                <div class="col-md-5">
                    <label id="ErrorLblAmount" style="color: red; margin-left: -30px;"></label>
                </div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
        <div class="row panel-footer" style="margin-top: 25px;">
            <div class="col-md-4">
                <div class="col-md-12">
                    <!--<button class="btn btn-default col-md-12" onclick="printCheq()" id="btnPrintCheq"><span class="fa fa-print"></span> Print Cheque</button>-->
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <button class="btn btn-default col-md-12" onclick="printMyVoucher()" id="btnPrintVoucher"><span class="fa fa-print"></span> Print Voucher</button>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-default col-md-12" onclick="printCheq()" id="btnPrintCheq"><span class="fa fa-print"></span> Print Cheque </button>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-default col-md-12" onclick="unblockui()" ><span class="fa fa-times"></span> Exit</button>
                </div>
                <!--                                <div class="col-md-4">
                                                    <button class="btn btn-default col-md-12" onclick="printVoucher()" id="btnPrintVoucher"><span class="glyphicon glyphicon-list-alt"></span>Create Voucher</button>
                                                </div>-->
            </div>

        </div>     
        <form method="GET" action="/AxaBankFinance/generatePDF" target="blank" id="formPDFCheck">
            <!--<input type="text" name="hidCheqId"  id="txtHidCheqId"/>-->
            <input type="hidden" name="loanId" value="${loanId}" />  
            <input type="hidden" id="voucherNoId" name="voucherNo" value="${voucherNo}"/>
            <input type="hidden" name="cheqType" value="1" />
            <input type="hidden" name="ChkDate" id="cheque_date" />
            <input type="hidden" name="cheqToName" id="cheque_toName" />
            <input type="hidden" id="TxtChqAmt" class="decimalField" name="TxtChqAmt" maxlength="15" />
            <input type="hidden" id="TxtChqAmtINWord" name="TxtChqAmtINWord" maxlength="500" size="100"/>
            <input type="hidden" id="IsFullDate" name="IsFullDate" />
            <input type="hidden" id="ChbIsACPay" name="ChbIsACPay" />
        </form>
    </div>

    <form method="GET" action="/AxaBankFinance/ReportController/generateLoanVoucher" target="blank" id="formLoanVoucher">
        <div class="row">
            <input type="hidden" name="loanId" value="${loanId}" />   
            <input type="hidden" name="totalAmount" id="totalAmtId" /> 
            <input type="hidden" name="capitalizeAmount" id="capAmtId" /> 
            <input type="hidden" name="downPayment" id="dwnPayAmtId" /> 
            <input type="hidden" name="OtherCharge" id="othrChargAmtId" />
            <input type="hidden" name="chekNo" id="chekNoId" /> 
            <input type="hidden" name="bankAccount" id="bankId" /> 
            <input type="hidden" name="cheqToName" id="cheque_toName1" />
            <input type="hidden" name="cheqType" value="1" /> 
            <input type="hidden" name="ChkDate" id="cheque_date1" />
            <input type="hidden" id="TxtChqAmt1" class="decimalField" name="TxtChqAmt" maxlength="15" /> 
            <input type="hidden" id="ChbIsACPay1" name="ChbIsACPay" />
            <input type="hidden" name="amountInTxt" id="amountInword1" />
        </div>
    </form> 
</div>

<script>
    $(function () {
        $(".txtCalendar").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        document.getElementById("cheqDetailsDiv").hidden = true;
        document.getElementById("directDepositDiv").hidden = true;
        $("#btnPrintCheq").prop("disabled", true);
        $('#paymentsTypes').change(function () {
            var selected = $(this).find('option:selected');
            var paymentType = selected.val();
            if (paymentType == 1) {
                document.getElementById("cheqDetailsDiv").hidden = true;
                document.getElementById("directDepositDiv").hidden = true;
                $("#btnPrintCheq").prop("disabled", true);
            } else  if (paymentType == 2){
                document.getElementById("cheqDetailsDiv").hidden = false;
                document.getElementById("directDepositDiv").hidden = true;
                var voucherNo = $("#voucherNoId").val();
                if (voucherNo === null || voucherNo === '') {
                    document.getElementById("btnPrintCheq").hidden = false;
                    $("#btnPrintCheq").prop("disabled", true);
                } else {
                    $("#btnPrintCheq").prop("disabled", false);
                    $("#cheqDetailsDiv").hide();
                    $("#directDepositDiv").hide();
                }
                var payingAmount = $("#payingAmountTxt").val();
                if (payingAmount > 0 && voucherNo === null || voucherNo === '') {
                    $("#btnPrintCheq").prop("disabled", true);
                } else {
                    $("#btnPrintCheq").prop("disabled", false);
                }
//                $("#btnPrintCheq").prop("disabled", false);
            }else{
                document.getElementById("cheqDetailsDiv").hidden = true;
                $("#btnPrintCheq").prop("disabled", true);
                document.getElementById("directDepositDiv").hidden = false;
            }

        });
        var balance = $("#txtBalanceAmount").val();
        if (balance == 0) {
            $("#payingAmountTxt").prop("disabled", true);
            $("#btnPrintCheq").prop("disabled", true);
            $("#btnPrintVoucher").prop("disabled", true);
            $("#btnPrintVou").prop("disabled", true);
        }

        $(".decimalField").autoNumeric();
        $("#payingAmountTxt").blur(function () {
            if (!isNaN(parseFloat($(this).val().trim()))) {
                var payingAmount = $(this).val();
                var a = payingAmount.split(".");
                var words = toWords(a[0]);
                var rup = words.toUpperCase() + " RUPEES ";
                if (parseFloat(a[1]) > 0) {
                    rup += " AND " + toWords(a[1]).toUpperCase() + " CENTS";
                }
                rup += " ONLY";
                $("#TxtChqAmtINWord").val(rup);
            } else {
                $("#TxtChqAmtINWord").val("");
            }
        });
    });
    function printVoucher() {
        var cheDate = $("#exDate").val();
        $("#cheque_date").val(cheDate);
        var payingAmount = $("#payingAmountTxt").val();
        var balance = $("#txtBalanceAmount").val();
        var words = toWords(payingAmount);
        $("#amountInword").val(words);
        if (parseFloat(payingAmount) > parseFloat(balance)) {
            $("#payingAmountTxt").addClass("txtError");
            $("#ErrorLblAmount").text("Please enter valid Amount");
            return;
        }
        if (payingAmount != "") {
            var voucher_no = $("input[name= voucherNo]").val();
            var downPayment = $("input[name=downPayment]").val();
            payingAmount = parseFloat(payingAmount);
            var capitalizeAmount = $("input[name=capitalizeAmount]").val();
            capitalizeAmount = parseFloat(capitalizeAmount);
            balance = parseFloat(balance);
            if ((voucher_no != null && voucher_no != '') || Math.round(payingAmount) > Math.round(downPayment)) {
                if (Math.round(payingAmount) <= Math.round(balance)) {
                    if (((balance - payingAmount) >= capitalizeAmount) || (voucher_no != null && voucher_no != '')) {
                        $("#ErrorLblAmount").text("");
                        $("#payingAmountTxt").removeClass("txtError");
                        var selected = $("#paymentsTypes").find('option:selected');
                        var paymentType = selected.val();
                        if (paymentType == 2) {
                            var cheqNo = $("#txtCheqNo").val();
                            if (cheqNo != "") {
                                $("#txtCheqNo").removeClass("txtError");
                                createVoucher();
                                $("#btnPrintCheq").prop("disabled", false);
                                $("#btnPrintVoucher").prop("disabled", true);
                                $("#btnPrintVou").prop("disabled", true);
                            } else {
                                $("#txtCheqNo").addClass("txtError");
                            }
                        } else {
                            createVoucher();
                            $("#btnPrintCheq").prop("disabled", true);
                            $("#btnPrintVoucher").prop("disabled", true);
                            $("#btnPrintVou").prop("disabled", true);
                        }
                        //$("#paymentsTypes").empty();
                    } else {
                        $("#ErrorLblAmount").html("Can not pay Capitalize Amount");
                    }

                } else {
                    $("#ErrorLblAmount").html("Please Enter the valid Paying Amount");
                }


            } else {
                //payingAmount > downpayment
                $("#ErrorLblAmount").html("PayingAmount Should be higher than DownPayments");
            }
        } else {
            $("#payingAmountTxt").addClass("txtError");
            $("#ErrorLblAmount").text("Please Enter Amount");
        }


    }

    function paymentByCheque() {
        printCheq();
    }


    function createVoucher() {
        document.getElementById("btnPrintVoucher").disabled = true;
        document.getElementById("btnPrintVou").disabled = true;
        $.ajax({
            url: "/AxaBankFinance/saveVoucherDetails",
            type: 'POST',
            data: $("#formPaymentData").serialize(),
            success: function (data) {
                var voucher_id = data[0];
                var voucher_no = $("input[name=voucherNo]").val();
                $("#txtHidVoucherId").val(voucher_id);
                var paymentType = $("#paymentsTypes").find('option:selected').val();
                $("#txtHidVoucherType").val(paymentType);
                if (voucher_id > 0) {
                    $("#paymntSuccess").html("<label class='successMsg' style='text-align:left;'>Saved Success</label>");
//                    generatePDf(2);
                    if (paymentType == 2) {
                        var chekId = data[1];
                        $("#txtHidCheqId").val(chekId);
                        printCheq();
                    }
                    unblockui();
                    var id = $("input[name=loanId]").val();
                    loadLoanProcessPage2(id);
                } else {
                    $("#paymntSuccess").html("<label class='errorMsg' style='text-align:left;>Saved Process Failed</label>");
                }
            },
            error: function () {
                alert("Error Printing");
            }
        });
    }

    function printCheq() {
        var cheqTo = $('input[name=cheqName]:checked').val();
        var payingAmount = $("#payingAmountTxt").val();
        var chqDate = $("#exDate").val();
        $("#cheque_toName").val(cheqTo);
        $("#TxtChqAmt").val(payingAmount);
        $("#cheque_date").val(chqDate);
        if ($("#accPyOnly").is(':checked')) {
            $("#ChbIsACPay").val("on");
        } else {
            $("#ChbIsACPay").val("off");
        }
        if ($("#chqIsFull").is(':checked')) {
            $("#IsFullDate").val("on");
        } else {
            $("#IsFullDate").val("off");
        }
        generatePDf(1);
    }

    function generatePDf(type) {
        if (type === 1) {
            //print check
            $("#formPDFCheck").submit();
            unblockui();
            $.ajax({
//                data: {start: startDate, end: endDate},
                url: '/AxaBankFinance/viewAllLoans/' + 3,
                success: function (data) {
                    $('#formContent').html(data);
                }
            });
        } else if (type === 2) {
            //print voucher
            $("#formPDFVoucher").submit();
        }

    }

    function loadLoanProcessPage2(id) {
        $.ajax({
            url: '/AxaBankFinance/processLoan/' + id,
            success: function (data) {
                $('#formContent').html("");
                $('#formContent').html(data);
            },
            error: function () {
            }
        });
    }

    function checkType(type) {
        if (type === 1) {
            $("#divCapitalize").css("display", "block");
            $("#payingAmountTxt").attr("readonly", true);
        } else {
            $("#divCapitalize").css("display", "none");
            $("#payingAmountTxt").attr("readonly", false);
        }
    }

    function changeCapitalizeType() {
        var amount = $("#inputCapitalizeType").find("option:selected").val();
        $("#payingAmountTxt").val(amount);
    }

    function printMyVoucher() {
        var cheDate = $("#exDate").val();
        $("#cheque_date1").val(cheDate);

        var totAmtId = $("#totAmtId").val();
        $("#totalAmtId").val(totAmtId);

        var capitaAmt = $("#capitaAmtId").val();
        $("#capAmtId").val(capitaAmt);

        var dnwAmt = $("#dwnPaymentTxtId").val();
        if (dnwAmt === '' || dnwAmt === undefined) {
            $("#dwnPayAmtId").val(0.00);
        } else {
            $("#dwnPayAmtId").val(dnwAmt);
        }

        var otherChargers = $("#itherChargeTxt").val();
        $("#othrChargAmtId").val(otherChargers);

        var CheqNo = $("#txtCheqNo").val();
        $("#chekNoId").val(CheqNo);

        var bankId = $("#bankIdTxt").val();
        $("#bankId").val(bankId);

        var payingAmount = $("#payingAmountTxt").val();
        var words = toWords(payingAmount);
        $("#cheque_toName1").val(words);

        var payingAmount = $("#payingAmountTxt").val();
        $("#TxtChqAmt1").val(payingAmount);

        if ($("#accPyOnly").is(':checked')) {
            $("#ChbIsACPay1").val(1);
        } else {
            $("#ChbIsACPay1").val(0);
        }

        printVou();
    }

    function printVou() {
        $("#formPaymentData").submit();
        unblockui();
    }


    function printMyCheque() {

    }
</script>
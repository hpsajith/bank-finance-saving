<%-- 
    Document   : addEmployeeFormView
    Created on : Apr 2, 2015, 4:10:58 PM
    Author     : SOFT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>


<select name="centeID" style="width: 172px" id="centeID" onchange="getGroup()">
    <option value="0"  >-select center-</option>
    <c:forEach var="center" items="${centerList}">
        <c:choose>
            <c:when test="${center.centerID == group.centeID}">
                <option selected="selected" value="${center.centerID}">${center.centerName}</option>
            </c:when>
            <c:otherwise>
                <option value="${center.centerID}">${center.centerName}</option>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</select>
 

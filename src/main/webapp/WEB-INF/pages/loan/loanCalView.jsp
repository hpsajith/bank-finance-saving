<%-- 
    Document   : loanCalView
    Created on : Jan 7, 2016, 2:34:47 AM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        $("#tblPaymentSche").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "40%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>
<style>
    tr{
        background-color: bcddff;
    }
    tr:hover {
        background-color: e56b70;
    }
</style>
<div class="row" style="margin-top: 20px">
    <legend><strong>Loan Calculator</strong></legend>
</div>

<div class="row"  style="margin-bottom: 5px">
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-5" style="text-align: right"><strong>Interest Rate Type</strong></div>
            <div class="col-md-7">
                <select name="loanRateCode" id="txtRateType" style="width: 100%"> 
                    <c:forEach var="rateType" items="${rateTypeList}">
                        <option value="${rateType.rateCode}">${rateType.rateType}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-8"></div>
</div>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;padding: 8px;margin-bottom: 2px">
    <div class="row">
        <div class="col-md-2" style="margin-top: 25px">
            <label>Loan Amount (Rs) : </label>
        </div>
        <div class="col-md-7">
            <div id="amountRange"></div>
        </div>
        <div class="col-md-3" style="margin-top: 25px"><input type="text" style="width: 100%"placeholder="Loan Amount" onblur="setAmount()" id="txtLoanAmount"/></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-2" style="margin-top: 25px">
            <label>Interest Rate % : </label>
        </div>
        <div class="col-md-7">
            <div id="rateRange"></div>
        </div>
        <div class="col-md-3" style="margin-top: 25px"><input type="text" style="width: 100%" onblur="setRate()" placeholder="Loan Rate" id="txtLoanRate"/></div>

    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-2" style="margin-top: 25px">
            <label>Loan Term (Months) : </label>
        </div>
        <div class="col-md-7">
            <div id="periodRange"></div>
        </div>
        <div class="col-md-3" style="margin-top: 25px"><input type="text" style="width: 100%" onblur="setPeriod()" placeholder="Loan Period" id="txtLoanPeriod"/></div>
    </div>    
    <div class="row" style="margin-top: 10px">
        <div class="col-md-3" style="margin-top: 35px">
            <label>Equal Monthly Payment   : </label>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4" style="margin-top: 25px">
            <label id="lblInst" style="font-weight: bold; font-size: 40px"></label>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-3" style="margin-top: 25px">
            <input type="button" name="" id="btnSaveAppLevels" value="Calculate" onclick="calculate()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/> 
        </div>
    </div>     
</div>

<legend><strong>Repayment Schedule</strong></legend>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;padding: 5px">
    <table id="tblPaymentSche" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>Month</th>
                <th>Installment Principle</th>
                <th>Monthly Interest</th>
                <th>Monthly Installment</th>
                <th>Balance</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>


<script>
    var loanAmount = 0;
    var rate = 0;
    var period = 0;
    $(function () {
        // amount range slider
        var $range = $("#amountRange");
        $range.ionRangeSlider({
            type: "single",
            min: 25000,
            max: 1000000,
            from: 100000,
            grid: true,
            onStart: function (data) {
                loanAmount = data.from;
            }
        });
        $range.on("change", function () {
            var $this = $(this);
            var value = $this.prop("value");
            loanAmount = value;
            $('#txtLoanAmount').val(value);
            calculateInst();
        });

        // rate range slider
        var $range = $("#rateRange");
        $range.ionRangeSlider({
            type: "single",
            min: 0,
            max: 100,
            from: 10,
            grid: true,
            onStart: function (data) {
                rate = data.from;
            }
        });
        $range.on("change", function () {
            var $this = $(this);
            var value = $this.prop("value");
            rate = value;
            $('#txtLoanRate').val(value);
            calculateInst();
        });

        // period range slider
        var $range = $("#periodRange");
        $range.ionRangeSlider({
            type: "single",
            min: 1,
            max: 72,
            from: 12,
            grid: true,
            onStart: function (data) {
                period = data.from;
            }
        });
        $range.on("change", function () {
            var $this = $(this);
            var value = $this.prop("value");
            period = value;
            $('#txtLoanPeriod').val(value);
            calculateInst();
        });
    });

    $(function () {
        $("#lblInst").text("Rs. 0.00");
        var loanAmount = $("#txtLoanAmount").val();
        var rate = $("#txtLoanRate").val();
        var period = $("#txtLoanPeriod").val();
        if (loanAmount === "" && rate === "" && period === "") {
            document.getElementById("btnSaveAppLevels").disabled = true;
        } else {
            document.getElementById("btnSaveAppLevels").disabled = false;
        }
    });

    function calculateInst() {

        var loanRateType = $("#txtRateType").find('option:selected').val();
        if (loanRateType === "FR") {
            if (loanAmount !== 0 && rate !== 0 && period !== 0) {
                loanAmount = parseFloat(loanAmount);
                rate = parseFloat(rate);
                period = parseFloat(period);
                var monthlyInterest = (loanAmount * rate) / 100;
                monthlyInterest = monthlyInterest / period;
                var totlInterest = monthlyInterest * period;
                var receivable = loanAmount + totlInterest;
                var installment = receivable / period;
                $("#lblInst").text("Rs. " + installment.toFixed(2));
            } else {
                $("#lblInst").text("Rs. 0.00");
            }
        } else {
            if (loanAmount !== 0 && rate !== 0 && period !== 0) {
                var monthlyInterest = rate / 1200;
                var installment = loanAmount * monthlyInterest / (1 - (Math.pow(1 / (1 + monthlyInterest), period)));
                $("#lblInst").text("Rs. " + installment.toFixed(2));
            } else {
                $("#lblInst").text("Rs. 0.00");
            }
        }
    }

    function calculate() {
        loanAmount = 0.00;
        rate = 0.00;
        period = 0.00;
        loanAmount = $("#txtLoanAmount").val();
        rate = $("#txtLoanRate").val();
        period = $("#txtLoanPeriod").val();
        calculateInst();
        viewPaymentSchedule(loanAmount, rate, period);
    }

    $('#txtRateType').change(function () {
        calculate();

    });


    $('#txtLoanPeriod').keydown(function (e) {
        document.getElementById("btnSaveAppLevels").disabled = false;
        var code = e.keyCode || e.which;
        if (code === 13) {
            calculate();
        }
    });

    function viewPaymentSchedule(loanAmount, rate, period) {
        $('#tblPaymentSche tbody').html("");
        loanAmount = parseFloat(loanAmount);
        rate = parseFloat(rate);
        period = parseFloat(period);
        var loanRateType = $("#txtRateType").find('option:selected').val();
        if (loanRateType === "RR") {           
            var monthlyInterest = rate / 1200;
            var installment = loanAmount * monthlyInterest / (1 - (Math.pow(1 / (1 + monthlyInterest), period)));
            installment = parseFloat(installment).toFixed(2);
            var i;
            var balance = loanAmount;
            for (i = 1; i <= period; i++) {
                var intrs = parseFloat(balance * monthlyInterest).toFixed(2);
                var principle = parseFloat(installment - intrs).toFixed(2);
                balance = parseFloat(balance - principle).toFixed(2);
                if (balance > 1) {
                } else {
                    balance = 0.00;
                }
                $('#tblPaymentSche tbody').append("<tr id ='" + i + "'>" +
                        "<td>" + i + "</td>" +
                        "<td>" + principle + "</td>" +
                        "<td>" + intrs + "</td>" +
                        "<td>" + installment + "</td>" +
                        "<td>" + balance + "</td></tr>");
            }
        } else {
            $('#tblPaymentSche tbody').html("");
            var monthlyInterest = (loanAmount * rate) / 100;
            monthlyInterest = monthlyInterest / period;
            var totlInterest = monthlyInterest * period;
            var receivable = loanAmount + totlInterest;
            var installment = receivable / period;
            var principle = installment - monthlyInterest;
            installment = parseFloat(installment).toFixed(2);
            principle = parseFloat(principle).toFixed(2);
            monthlyInterest = parseFloat(monthlyInterest).toFixed(2);
            var i;
            var balance = parseInt(loanAmount) + parseInt(totlInterest);
            balance = parseFloat(balance - installment).toFixed(2);
//             $("#lblInst").text("Rs. " + installment.toFixed(2));
            for (i = 1; i <= period; i++) {
                if (balance > 1) {
                } else {
                    balance = 0.00;
                }
                $('#tblPaymentSche tbody').append("<tr id ='" + i + "'>" +
                        "<td>" + i + "</td>" +
                        "<td>" + principle + "</td>" +
                        "<td>" + monthlyInterest + "</td>" +
                        "<td>" + installment + "</td>" +
                        "<td>" + balance + "</td></tr>");
                balance = balance - installment;
                balance = parseFloat(balance).toFixed(2);
            }
        }
    }

    function setAmount() {
        loanAmount = $("#txtLoanAmount").val();
        var slider = $("#amountRange").data("ionRangeSlider");
        slider.update({
            min: 0,
            max: 1000000,
            from: loanAmount
        });
    }

    function setRate() {
        rate = $("#txtLoanRate").val();
        var slider = $("#rateRange").data("ionRangeSlider");
        slider.update({
            min: 0,
            max: 100,
            from: rate
        });
    }

    function setPeriod() {
        document.getElementById("btnSaveAppLevels").disabled = false;
        period = $("#txtLoanPeriod").val();
        var slider = $("#periodRange").data("ionRangeSlider");
        slider.update({
            min: 0,
            max: 120,
            from: period
        });
    }


</script>
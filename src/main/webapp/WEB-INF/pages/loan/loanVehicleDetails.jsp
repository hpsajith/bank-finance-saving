<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border:  1px solid #0088cc;width: 100%">
    <div class="row" style="margin: 10px;">
        <div class="col-md-12"><legend>Vehicle Details</legend></div>
        <form name="formVehicle" id="formVehicledata">
            <div class="row">
                <input type="hidden" name="vehicleId" value="${vehicle.vehicleId}" />
                <div class="col-md-4">Dealer</div>
                <div class="col-md-8"><select name="dealerId" class="col-md-12">
                        <c:forEach var="dealer" items="${dealerList}">
                            <c:choose>
                                <c:when test="${dealer.supplierId==vehicle.dealerId}">
                                    <option selected="" value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                </select></div>
            </div>
            <div class="row">
                <input type="hidden" name="vehicleId" value="${vehicle.vehicleId}" />
                <div class="col-md-4">Reg No</div>
                <div class="col-md-8"><input type="text" name="vehicleRegNo" style="width: 100%" value="${vehicle.vehicleRegNo}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Vehicle Type</div>
                <div class="col-md-8"><input type="text" name="vehicleType" style="width: 100%" value="${vehicle.vehicleType}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Model</div>
                <div class="col-md-8"><input type="text" name="vehicleModel" style="width: 100%" value="${vehicle.vehicleModel}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Reg Date</div>
                <div class="col-md-8"><input class="txtCalendar" type="text" name="vehicleRegDate" style="width: 100%" value="${vehicle.vehicleRegDate}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Engine No</div>
                <div class="col-md-8"><input  type="text" name="vehicleEngineNo" style="width: 100%" value="${vehicle.vehicleEngineNo}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Chassis No</div>
                <div class="col-md-8"><input type="text" name="vehiclChassisNo" style="width: 100%" value="${vehicle.vehiclChassisNo}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Description</div>
                <div class="col-md-8"><textarea type="text" name="vehicleDescription" style="width: 100%">${vehicle.vehicleDescription}</textarea></div>
            </div>
            <div class="row">
                <div class="col-md-4">Price</div>
                <div class="col-md-8"><input type="text" name="vehiclePrice" style="width: 100%" value="${vehicle.vehiclePrice}"></div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button" name="" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button" name="" class="btn btn-default col-md-12 saveUpadte" value="Save" onclick="addVehicleDetalis()"/></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div> 
</div>     
<div id="msgDiv_other" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>   
<script>
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            yearRange: '1920:2015'
        });
    });
    function addVehicleDetalis() {
        $.ajax({
            url: '/AxaBankFinance/saveVehicleProperty',
            type: 'POST',
            data: $("#formVehicledata").serialize(),
            success: function(data) {
                if (data > 0) {
                    success("Successfull Saved", "msgDiv_other");
                }
                var selectId = $("#slctProperty").find('option:selected').val();
                $("#hidProperty").append("<input type='hidden' id='ve" + data + "' name='proprtyType' value='" + selectId + "'/>" +
                        "<input type='hidden' name='properyId' id='vep" + data + "' value='" + data + "'/>");
                addVehicleToTable(data);
                unblockui();
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    function addVehicleToTable(id) {
        var type = $("input[name=vehicleType]").val() + " " + $("input[name=vehicleModel]").val();
        var price = $('input[name=vehiclePrice]').val();
        var table = document.getElementById("tblProperty");
        var rowCount = table.rows.length;
        $("#tblProperty tbody").append('<tr id="v_tr' + id + '" ><td>' +
                rowCount + '</td><td>' + type + '</td><td>' +
                price + '</td><td><div onclick="editPropertyForm(1,' + id + ')" class="edit"></div><div onclick="deleteVehicle(' + id + ')" class="delete"></div>' + '</td></tr>');
    }

    function deleteVehicle(tr) {
        $("#v_tr" + tr).remove();
        $("#ve" + tr).remove();
        $("#vep" + tr).remove();
    }
</script>


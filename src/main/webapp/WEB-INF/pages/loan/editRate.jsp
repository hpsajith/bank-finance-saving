<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row" style="margin: 20px;">
    <div class="col-md-11">
        <c:choose>
            <c:when test="${hasApproval}">
                <!--has authorize to change rate-->
                <div class="row" style="text-align: left;margin-left: 10px;">
                    <label>${title}</label>
                </div>
                <div class="row">
                    <label style="color: green">${message}</label>
                </div>
                <div class="row" >                    
                    <div class="col-md-4">Enter New Rate</div>
                    <div class="col-md-5"><input type="text" id="newRate" autofocus/></div>
                    <div class="col-md-3"><label style="color: red" id="errorLableRate"></label></div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-4"></div>
                    <div class="col-md-3"><input type="button" value="Cancel" onclick="unblockui()" class="btn btn-default col-md-12"/></div>
                    <div class="col-md-3"><input type="button" value="Save" onclick="saveNewRate()" class="btn btn-default col-md-12" id="saveNewRate"/></div>
                    <div class="col-md-2"></div>
                </div>
            </c:when>
            <c:otherwise>
                <!--no authorize to change rate-->
                <div class="row" style="text-align: left;margin-left: 10px;">
                    <label>${title}</label>
                </div>                
                <div class="row">
                    <div class="col-md-3">Password</div>
                    <div class="col-md-5"><input type="password" id="newRatePw" autofocus/></div>
                    <div class="col-md-3"><label style="color: red" id="errorLable"> ${message} </label></div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-6"></div>
                    <div class="col-md-3"><input type="button" value="Cancel" onclick="unblockui()" class="btn btn-default col-md-12"/></div>
                    <div class="col-md-3"><input type="button" value="Save" onclick="checkPassword()" class="btn btn-default col-md-12"/></div>
                </div>
            </c:otherwise>
        </c:choose>        
    </div>
    <input type="hidden" id="hidRowId" value="${row}"/>
    <input type="hidden" id="hidRowTypeId" value="${type}"/>
</div>

<script>
    $(function() {
        $("#newRate").blur(function() {
            $("#saveNewRate").focus();
        });
    });


    function saveNewRate() {
        var newRate = $("#newRate").val();
        var i = $("#hidRowId").val();
        var rType = $("#hidRowTypeId").val();
        console.log(i);

        var newRateView = newRate;
        if (newRate == "") {
            $("#errorLableRate").text("Rate cannot be balnk");
            $("#newRate").addClass("txtError");
        } else {
            $("#errorLableRate").text("");
            $("#newRate").removeClass("txtError");
            
            if (rType == 1) {
                if (i >= 0) {
                    var type = $("#t" + i).val();
                    if (type == 1) {
                        newRateView = newRateView + "%";
                    }
                    $("#aaa" + i).text(newRateView);
                    $("#c" + i).val(newRate);
                    calculateCharge();
                    
                } else {
                    $("input[name=loanRate]").val(newRateView);
                    $("#viewRate").val(newRateView);                   
                    calculateLoan();
                    
                }
            }
            else {
                var loanAmount = $("input[name=loanAmount]").val();
                var type = $("#t" + i).val();
                if (type == 1) {
                    var chrgRate = (parseFloat(newRate) * 100) / parseFloat(loanAmount);
                    var newChargRate = chrgRate.toFixed(2);
                    var newChrgRateView = newChargRate + "%";
                    $("#aaa" + i).text(newChrgRateView);
                    $("#c" + i).val(newChargRate);
                }else{
                    $("#aaa" + i).text(newRate);
                    $("#c" + i).val(newRate);
                }
                calculateCharge();
            }

            unblockui();
        }

    }

    function checkPassword() {
        var password = $("#newRatePw").val();
        var rType = $("#hidRowTypeId").val();
        
        if (password == "") {
            $("#errorLable").text("Enter Password");
            $("#newRatePw").addClass("txtError");
        } else {
            $("#newRatePw").removeClass("txtError");
            $("#errorLable").text("");
            var i = $("#hidRowId").val();
            $.ajax({
                url: "/AxaBankFinance/checkUserPassword/" + password + "/" + i+"/"+rType,
                success: function(data) {
                    $("#editRate_div").html("");
                    $("#editRate_div").html(data);
                },
                error: function() {

                }
            });
        }

    }
</script>
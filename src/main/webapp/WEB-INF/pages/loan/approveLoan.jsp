<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>${title}</strong>
    </div>
    <form name="approve1" id ="formApprove1">
        <div class="col-md-12">
            <label id="lblapproveTitle"></label>
            <input type="hidden" name="loanId" id="txtLoanId_manager" value="${approve.loanId}"/>
            <input type="hidden" name="approvedLevel" id="txtApproveLevel_manager" value="${approve.approvedLevel}"/>
            <input type="hidden" name="pk" id="apprv_Pk" value="${approve.pk}"/>
            <div class="row" style="padding: 5px;">
                <c:choose>
                    <c:when test="${approve.pk>0}">
                        <c:choose>
                            <c:when test="${approve.approvedStatus}">
                                <div class="col-md-2"></div>
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="1" checked="true"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="0" /></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-2"></div>
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="1"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="0" checked="true"/></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:otherwise>                    
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-2"></div>
                        <div class="col-md-1"><input type="radio" name="approvedStatus" value="1"/></div>
                        <div class="col-md-3"><strong>Approve</strong></div>
                        <div class="col-md-1"><input type="radio" name="approvedStatus" value="0" /></div>
                        <div class="col-md-3"><strong>Reject</strong></div>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Comment</div>
                <div class="col-md-10"><textarea rows="3" name="approvedComment" style="width: 100%">${approve.approvedComment}</textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="row" style="margin-top: 15px">  
                <c:choose>
                    <c:when test="${type==1}">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                            <c:choose>
                                <c:when test="${approve.pk>0}">
                                <div class="col-md-4"><input type="button"  value="Update" class="btn btn-default col-md-12" onclick="submitLoanApproveLevels(${type})"></div>
                                </c:when>
                                <c:otherwise>
                                <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="submitLoanApproveLevels(${type})"></div>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                        <div class="col-md-8"></div>
                        <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                        </c:otherwise>
                    </c:choose>   
            </div>
        </div>        
    </form>
</div> 
<div id="message_approveLoan" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script>
    function submitLoanApproveLevels(type) {
        $.ajax({
            url: '/AxaBankFinance/saveApproveLevel',
            type: 'POST',
            data: $("#formApprove1").serialize(),
            success: function(data) {
                viewAllLoans(0);
                $('#message_approveLoan').html(data);
                ui("#message_approveLoan");
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }
</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border:  1px solid #0088cc; width: 80%">
    <div class="row" >
        <div class="col-md-12"><legend>Land Details</legend></div>
        <form name="formArticle" id="formLandData">
            <div class="row">
                <input type="hidden" name="vehicleId" value="${vehicle.vehicleId}" />
                <div class="col-md-4">Dealer</div>
                <div class="col-md-8"><select name="dealerId" class="col-md-12">
                        <c:forEach var="dealer" items="${dealerList}">
                            <c:choose>
                                <c:when test="${dealer.supplierId==vehicle.dealerId}">
                                    <option selected="" value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select></div>
            </div>
            <div class="row">
                <input type="hidden" name="landId" value="${land.landId}"/>
                <div class="col-md-4">Deed No</div>
                <div class="col-md-8"><input type="text" name="landDeedNo" style="width: 100%" value="${land.landDeedNo}"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Location</div>
                <div class="col-md-8"><input type="text" name="landPlace" style="width: 100%" value="${land.landPlace}"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Area</div>
                <div class="col-md-8"><input type="text" name="landArea" style="width: 100%" value="${land.landArea}"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Price</div>
                <div class="col-md-8"><input type="text" name="landPrice" style="width: 100%" value="${land.landPrice}"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Description</div>
                <div class="col-md-8"><textarea name="landDescription" style="width: 100%" value="${land.landDescription}"></textarea></div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button" class="btn btn-default col-md-12 saveUpadte" value="Save" onclick="addLandDetails()"></textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div> 
</div>     
<div id="msgDiv_other" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>      

<script>
    function addLandDetails() {
        $.ajax({
            url: '/AxaBankFinance/saveLandProperty',
            type: 'POST',
            data: $("#formLandData").serialize(),
            success: function(data) {
                if (data > 0) {
                    success("Successfull Saved", "msgDiv_other");
                }
                var selectId = $("#slctProperty").find('option:selected').val();
                $("#hidProperty").append("<input type='hidden' id='la" + data + "' name='proprtyType' value='" + selectId + "'/>" +
                        "<input type='hidden' id='lap" + data + "' name='properyId' value='" + data + "'/>");
                addLandToTable(data);
                unblockui();
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }
    function addLandToTable(data) {
        var type = $("input[name=landDeedNo]").val() + " " + $("input[name=landPlace]").val() + " " + $("input[name=landDescription]").val();
        var price = $('input[name=landPrice]').val();
        var table = document.getElementById("tblProperty");
        var rowCount = table.rows.length;
        $("#tblProperty tbody").append('<tr id="l_tr' + data + '" ><td>' +
                rowCount + '</td><td>' + type + '</td><td>' +
                price + '</td><td><div onclick="editPropertyForm(3,' + data + ')" class="edit"></div><div onclick="deleteLand(' + data + ')" class="delete"></div>' + '</td></tr>');
    }

    function deleteLand(tr) {
        $("#l_tr" + tr).remove();
        $("#la" + tr).remove();
        $("#lap" + tr).remove();
    }
</script>


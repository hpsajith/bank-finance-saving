<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border:  1px solid #0088cc;width: 80%">
    <div class="row" style="margin: 10px;">
        <div class="col-md-12"><legend>Other Details</legend></div>
        <form name="formOther" id="formOtherData">
            <div class="row">
                <input type="hidden" name="vehicleId" value="${vehicle.vehicleId}" />
                <div class="col-md-4">Dealer</div>
                <div class="col-md-8"><select name="dealerId" class="col-md-12">
                        <c:forEach var="dealer" items="${dealerList}">
                            <c:choose>
                                <c:when test="${dealer.supplierId==vehicle.dealerId}">
                                    <option selected="" value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                </select></div>
            </div>
            <div class="row">
                <input type="hidden" name="otherId" value="${other.otherId}"/>
                <div class="col-md-4">Property Name</div>
                <div class="col-md-8"><input type="text" name="otherType" style="width: 100%" value="${other.otherType}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Description</div>
                <div class="col-md-8"><textarea name="otherDescription" style="width: 100%" id="oth_desc_txt">${other.otherDescription}</textarea></div>
            </div>
            <div class="row">
                <div class="col-md-4">Value</div>
                <div class="col-md-8"><input type="text" name="otherPrice" style="width: 100%" value="${other.otherPrice}"></div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button" class="btn btn-default col-md-12 saveUpadte" value="Save" onclick="addOtherDetails()"></textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div> 
</div>
<div id="msgDiv_other" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>     
<script>
    function addOtherDetails() {
        $.ajax({
            url: '/AxaBankFinance/saveOtherProperty',
            type: 'POST',
            data: $("#formOtherData").serialize(),
            success: function(data) {
                if (data > 0) {
                   success("Successfull Saved","msgDiv_other");
                }
                var selectId = $("#slctProperty").find('option:selected').val();
                $("#hidProperty").append("<input type='hidden' id='ot"+data+"' name='proprtyType' value='" + selectId + "'/>" +
                        "<input type='hidden' id='otp"+data+"' name='properyId' value='" + data + "'/>");
                addOtherToTable(data);
                unblockui();
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }
    function addOtherToTable(data) {
        var type = $("input[name=otherType]").val() + " " + $("#oth_desc_txt").val();
        var price = $('input[name=otherPrice]').val();
        var table = document.getElementById("tblProperty");
        var rowCount = table.rows.length;
        $("#tblProperty tbody").append('<tr id="o_tr'+data+'" ><td>' +
                rowCount + '</td><td>' + type + '</td><td>' +
                price + '</td><td><div onclick="editPropertyForm(4,'+data+')" class="edit"></div><div onclick="deleteOther('+data+')" class="delete"></div>' + '</td></tr>');
    }
    
    function deleteOther(tr){
        $("#o_tr"+tr).remove();
        $("#ot"+tr).remove();
        $("#otp"+tr).remove();
    }
        
    function editOther(id){
        
    }
</script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $(".newTable").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "15%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<div class="row divError"></div>
<div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-1"><strong>Loan Id</strong></div>
    <div class="col-md-1"><input type="text" style="color:#337ab7;" value="${loanId}" name="loanId" onkeypress="return isNumber(event)" /></div>
    <div class="col-md-2"><input type="button" class="btn btn-default searchButton" onclick="searchLoan()" style="height: 25px; margin-left: 59px;"></div>
</div>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;" id="form_">
    <form name="loanForm" id="newLoanForm">        
        <div class="col-md-12 col-lg-12 col-sm-12">
            <input type="hidden" value="${edit}" id="testEdit"/>
            <c:choose>
                <c:when test="${edit}">
                    <div class="row" ><label>Edit Loan</label></div>
                </c:when>
                <c:otherwise>
                    <div class="row" ><label>Add New Loan</label></div>
                </c:otherwise>
            </c:choose>
            <div class="row">                                 
                <div class="col-md-3 col-lg-3 col-sm-3" style="margin-right: 10px;">
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="margin-right: -19px; font-weight: 600;">Loan Type</div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <select name="loanMainType" id="loanTypeList"> 
                                <option value="0">--SELECT--</option>
                                <c:forEach var="ltypes" items="${loanTypes}">
                                    <c:choose>
                                        <c:when test="${ltypes.loanTypeId==loan.loanMainType}">
                                            <option value="${ltypes.loanTypeId}" selected>${ltypes.loanTypeName}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${ltypes.loanTypeId}">${ltypes.loanTypeName}</option>
                                        </c:otherwise>
                                    </c:choose>                                    
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3"><input type="hidden" name="loanType" value="${loan.loanType}" /></div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Amount</div>
                        <div class="col-md-8 col-lg-8 col-sm-8"><input type="text" name="loanAmount" value="${loan.loanAmount}" style="width: 100%" id="loanAmountId" class="" onkeypress="return isNumber(event)"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Down Payment</div>
                        <div class="col-md-3 col-lg-3 col-sm-3"><input type="text" name="loanDownPayment" value="${loan.loanDownPayment}" style="width: 100%" id="" class="" onkeypress="return isNumber(event)" /></div>

                        <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Investment</div>
                        <div class="col-md-3 col-lg-3 col-sm-3"><input type="text" name="loanInvestment" value="${loan.loanInvestment}" style="width: 100%" id="" class=""  readonly /></div>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">
                    <div class="row">
                        <div class="col-md-3" style="font-weight: 600">Rate</div>
                        <div class="col-md-4"><input type="text" name="loanRate" value="${loan.loanRate}" style="width: 75px;" readonly/></div>
                        <div class="edit" style="margin-left: 45px; margin-top: 5px;" onclick="editCharge(-1)" ></div>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3" style="font-weight: 600">Period</div>
                        <div class="col-md-5 col-lg-5 col-sm-5" style="margin-right: -13px;"><input type="text" name="loanPeriod" value="${loan.loanPeriod}" style="width: 100%" /></div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <select name="periodType" name="periodType">
                                <option value="1">Month</option>
                                <option value="2">Week</option>
                                <option value="3">Dates</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-6" style="font-weight: 600">Start Date</div>
                        <div class="col-md-6" ><input type="text" name="startDate" valu="" style="width: 100%"/></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-6" style="font-weight: 600">Due Date</div>
                        <div class="col-md-6" ><input type="text" name="dueDate" valu="" style="width: 100%"/></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="row">
                        <div class="col-md-5" style="font-weight: 600">Remain no of Ins.</div>
                        <div class="col-md-4"  ><input type="text" name="remainIns" valu="" style="width: 100%"/></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="row">
                        <div class="col-md-8" style="font-weight: 600">Arrears no of Ins.</div>
                        <div class="col-md-4" ><input type="text" name="arrearsIns" valu="" style="width: 100%"/></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="row">
                        <div class="col-md-8" style="font-weight: 600">Arrears Amount</div>
                        <div class="col-md-4" ><input type="text" name="arrearsAmount" valu="" style="width: 100%"/></div>
                    </div>
                </div>
            </div>

            <div class="row" style="background: #CEECF5; padding-top: 10px;">
                <div class="row" style="margin-left: 12px; font-weight: bold">Customer Details</div>
                <div class="col-md-10 col-lg-10 col-sm-10">
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4" style="margin-right: -2px; font-weight: 600">NIC</div>
                                <div class="col-md-7 col-lg-7 col-sm-7" style="margin-left:-4px; padding-left: 1;"><input type="text" style="width: 100%;" value="${debtor.debtorNic}" id="debtorNic"/></div>
                                <div class="col-md-1 col-lg-1 col-sm-1"></div>
                            </div>
                        </div>
                        <div class="col-md-7 col-lg-7 col-sm-7">
                            <div class="row">
                                <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Name</div>
                                <div class="col-md-8 col-lg-8 col-sm-8" style="margin-right: -25px"><input type="text" style="width: 100%" id="debtorName" value="${debtor.nameWithInitial}"/></div>
                                <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" class="btn btn-default searchButton" id="btnSearchCustomer" style="height: 25px"></div>
                                <!--                                <div class="col-md-1 col-lg-1 col-sm-1"></div>-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 col-sm-3"style="margin-right: -10px; font-weight: 600">Address</div>
                                <div class="col-md-9 col-lg-9 col-sm-9"><textarea name="" id="debtorAddress" style="width: 104%">${debtor.debtorPersonalAddress}</textarea></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 col-sm-3"></div>
                                <div class="col-md-4 col-lg-4 col-sm-4" style="margin-right: -45px; font-weight: 600">Contact No</div>
                                <div class="col-md-5 col-lg-5 col-sm-5" style=""><input type="text" style="width: 102%" id="debtorTelephone" value="${debtor.debtorTelephoneMobile}"/></div>
                                <!--<div class="col-md-1 col-lg-1 col-sm-1"></div>-->
                            </div>
                        </div>
                        <input type="hidden" id="debID" name="debtorId" value="${debtor.debtorId}"/>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2" style="margin-left: -30px; margin-top: -20px;">
                    <div id="customerProfilePicture"></div>
                </div>
            </div>
            <div class="row"></div>


            <!--//gaurantor-->
            <div class="row" style="background: #F0F8FA; padding-top: 10px;">
                <div class="row" style="margin-left: 12px; font-weight: bold">Guarantor Details</div>
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-sm-10">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-sm-4" style="margin-left:15px; font-weight: 600">NIC</div>
                                    <div class="col-md-7 col-lg-7 col-sm-7" style="margin-left:-12px; padding-left: 1;"><input type="text" style="width: 100%;"  id="gurantorNic"/></div>
                                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-7 col-sm-7">
                                <div class="row">
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Name</div>
                                    <div class="col-md-8 col-lg-8 col-sm-8" style="margin-right: -25px"><input type="text" style="width: 100%" id="guarantorName" /></div>
                                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" class="btn btn-default searchButton" id="btnLoadGuarantor" style="height: 25px"></div>
                                    <!--                                <div class="col-md-1 col-lg-1 col-sm-1"></div>-->
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        <!--<div class="row" style="border:1px solid #BDBDBD; height: 12%; border-radius: 2px;">IMAGE</div>-->
                    </div>
                </div>
                <div class="row" style="overflow-y: auto; margin-left: 20px;">
                    <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 1">
                        <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="guranterTable">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>NIC</th>
                                    <th>Address</th>
                                    <th>Telephone</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="gurantor" items="${gurantors}">
                                    <tr id="${gurantor.debtId}">
                                        <td>${gurantor.debtOrder}</td>
                                        <td>${gurantor.debtName}</td>
                                        <td>${gurantor.debtNic}</td>
                                        <td>${gurantor.debtAddress}</td>
                                        <td>${gurantor.debtTp}</td>
                                        <td><div onclick="" class="edit"></div><div class="delete" onclick="deleteRow(${gurantor.debtId})" ></div></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" id="guarantorIDs">
                    <c:forEach var="gurantor" items="${gurantors}">
                        <input type="hidden" id="g_hid${gurantor.debtId}" name="guarantorID" value="${gurantor.debtId}" />
                    </c:forEach>
                </div>
            </div>



            <!--            <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                <div class="col-md-3" style="font-weight: 600;">NIC</div>
                                <div class="col-md-8"><input type="text" name="" id="g_nic"/></div>
                            </div>
                            <div class="col-md-7">
                                <div class="col-md-2 col-lg-2 col-sm-2" style="margin-right: -40px;padding-left: 1;font-weight: 600">Guarantor Name</div>
                                <div class="col-md-6 col-lg-6 col-sm-6" style="margin-right: -12px;padding-left: 1"><input type="text" style="width: 100%" id="guarantorName"/></div>
                                <div class="col-md-1 col-lg-1 col-sm-1"style="padding: 1"><input type="button" id="btnLoadGuarantor" class="btn btn-default searchButton" style="height: 25px"/></div>
                            </div>
                            
                        </div>-->

            <div class="row" style="margin-left: 5px;">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-2 col-lg-2 col-sm-2" style="margin-right: -45px;padding-left: 1; font-weight: 600">Invoice Details</div>
                        <div class="col-md-6 col-lg-6 col-sm-6" style="margin-right: -12px;padding-left: 1"><input type="text" style="width: 100%" /></div>
                        <div class="col-md-1 col-lg-1 col-sm-1" style="padding-left: 1"><input type="button" id="btnAddProperty" class="btn btn-default searchButton" style="height: 25px"/></div>
                        <div class="col-md-3 col-lg-3 col-sm-3"></div>
                    </div>

                    <div class="row" style="overflow-y: auto">
                        <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 1">
                            <table id="tblProperty" class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0">

                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Description</th>
                                        <th>Value</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="invoice" items="${invoise}">
                                        <tr id="v_tr${invoice.invoiceId}">
                                            <td>${invoice.invoiceId}</td>
                                            <td>${invoice.description}</td>
                                            <td>${invoice.invoiceValue}</td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${invoice.type==1}">
                                                        <div onclick="editPropertyForm(1,${invoice.invoiceId})" class="edit"></div>
                                                        <div class="delete" onclick="deleteVehicle(${invoice.invoiceId})" ></div>
                                                    </c:when>
                                                    <c:when test="${invoice.type==2}">
                                                        <div onclick="editPropertyForm(2,${invoice.invoiceId})" class="edit"></div>
                                                        <div class="delete" onclick="deleteArticle(${invoice.invoiceId})" ></div>
                                                    </c:when>
                                                    <c:when test="${invoice.type==3}">
                                                        <div onclick="editPropertyForm(3,${invoice.invoiceId})" class="edit"></div>
                                                        <div class="delete" onclick="deleteLand(${invoice.invoiceId})" ></div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div onclick="editPropertyForm(4,${invoice.invoiceId})" class="edit"></div>
                                                        <div class="delete" onclick="deleteOther(${invoice.invoiceId})" ></div>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row" id="hidProperty">
                        <c:forEach var="invoice" items="${invoise}">
                            <input type='hidden' id='ve${invoice.invoiceId}' name='proprtyType' value='${invoice.type}'/> 
                            <input type='hidden' name='properyId' id='vep${invoice.invoiceId}' value='${invoice.type}'/>
                        </c:forEach>
                    </div>
                </div>                
            </div>
            <div class="row" style="">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="row" style="width: 260px;">
                        <table id="otherChargesTable" class="table table-striped table-bordered table-hover table-condensed table-edit" >
                            <caption>Add Other Charges</caption>
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="true">
                                        <c:forEach var="other" items="${otherChargers}">
                                            <tr id="${other.id}">
                                                <td>${other.description}</td>
                                                <td><a onclick="editCharge(${other.id})">${other.rate}</a></td>
                                                <td>${other.amount}</td>
                                            </tr>
                                        </c:forEach>
                                        <tr>
                                            <td colspan="2" style="text-align: right"><label>Total</label></td>
                                            <td><label>${total}</label></td>
                                        </tr>
                                    </c:when>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                    <div class="row" id="hidCharges">
                        <c:forEach var="other" items="${otherChargers}">
                            <input type="hidden" name="chragesId" value="${other.id}"/>
                            <input type="hidden" name="chragesRate" value="${other.rate}"/>
                            <input type="hidden" name="chragesAmount" value="${other.amount}"/>
                        </c:forEach>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4" style=" margin-top: 34px; background: #F2F7FC; width: 300px;">
                    <div class="row"><div class="col-md-12 col-lg-12 col-sm-12"><label>Loan Details</label></div></div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="">Loan Amount</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber" id="viewLoanAmount" value="${loan.loanAmount}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="">Investment</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber" id="viewInvestment" value="${loan.loanInvestment}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="">Down Payment</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber" id="viewDownPayment" value="${loan.loanDownPayment}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Period</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input  style="text-align:right" type="text" id="viewLoanPeriod" value="${loan.loanPeriod}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Interest Rate</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" style="text-align:right" id="viewRate" value="${loan.loanRate}" name="" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Interest</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber" id="viewInterest" name="loanInterest" value="${loan.loanInterest}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Total Interest</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber" id="viewTotalInterest" name="" value="${totalInterest}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Installment</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber" id="viewInstallment" name="loanInstallment" value="${loan.loanInstallment}" readonly/></div>
                    </div>
                    <div class="row" style="border-top: 1px solid #222; padding-top: 3px; margin-top: 3px; ">
                        <div class="col-md-5 col-lg-5 col-sm-5">Total</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" id="viewTotal" class="decimalNumber" disabled="true" value="${totalAmount}"></div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4" style="margin-top: 33px;width: 300px;">
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style=""><input placeholder="Approval 1" type="text" id="salesPersonName" value="${approve.app_name1}"><input type="hidden" name="salesPerson" id="salesPersonId" value="${approve.app_id1}" /></div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnLoadSaleParsonName" class="btn btn-default searchButton" style="height: 25px;margin-left: 11px;" /></div>
                        <div class="col-md-5"><input type="text" name="comment1" value="${approve.comment1}"  placeholder="Comment" id="app1_comment"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-2" style=""><input placeholder="Approval 2" type="text" id="recoveryPersonName" value="${approve.app_name2}"><input type="hidden" name="recoveryPerson" id="recoveryPersonId" value="${approve.app_id2}" /></div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnRecoveryManager" class="btn btn-default searchButton" style="height: 25px;margin-left: 11px;" /></div>
                        <div class="col-md-5"><input type="text" name="comment2" value="${approve.comment2}"  placeholder="Comment" id="app2_comment"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style=""><input placeholder="Approval 3" type="text" id="supplierName" value="${approve.app_name3}"><input type="hidden" name="ced" id="supplierId" value="${approve.app_id3}" /></div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnSupplier" class="btn btn-default searchButton" onclick="searchUserType(3)" style="height: 25px;margin-left: 11px;"/></div>
                    </div>
                </div>
            </div>
            <c:choose>
                <c:when test="${edit}">
                    <div class="row panel-footer" style="padding-top: 10px;">                        
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="row">                                
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" value="Cancel"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"> <input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" onclick="uploadDocuments()" value="Upload Documents"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" onclick="saveLoanForm()" value="Update"/></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-8"> <input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" onclick="processLoanToApproval()" value="Process" style="width: 100px"/></div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row panel-footer" style="padding-top: 10px;">
                        <div class="col-md-6 col-lg-6 col-sm-6"></div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4"> <input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" value="Exit"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" value="Cancel"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button" class="btn btn-default col-md-12 col-lg-12 col-sm-12" onclick="saveLoanForm()" value="Save"/></div>
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </div>
                </c:otherwise>
            </c:choose>

            <input type="hidden" id="gurant_debtr_type" />
            <div class="row" id="hiddenContent"></div>
        </div>
    </form>
</div>

<!--Sub Loan Types-->
<div class="searchCustomer" style="width: 35%; left:40%; top:20%" id="popup_subLoanTypes">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row"  style="padding: 20px">
        <table id="tblsubtypes" class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header">
            <thead><th>Loan Type</th></thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<!--Search Customer-->
<div class="searchCustomer" style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto" id="popup_searchCustomer">

</div>
<!--Add Other Property -->
<div class="searchCustomer" style=" width: 500px; top:10%; left:30%;" id="popup_addProperty">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label>Add Other Property</label></div>
    <div class="row" style="padding: 10px;">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <select id="slctProperty" >
                <option value="0">--SELECT--</option>
                <option value="1">Vehicle</option>
                <option value="3">Land</option>
                <option value="4">Other</option>
            </select>
        </div>
    </div>
    <div class="row" style="margin:15 0 0 0; height: 60%; overflow-y: auto; width:100%" id="formPropertyContent">

    </div>
</div>
<!--Search supplier,sales Manager, recovery manager-->
<div class="searchCustomer" style="margin-top: 5%; margin-left: 7%" id="popup_searchOfficers">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label id="app_title"></label></div>
    <div class="row" style="margin: 10px;">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    Search By Name
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <input type="text" name="" class="col-md-12 col-lg-12 col-sm-12" style="padding: 1"/>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1"></div>
            </div>
        </div>
    </div>

    <div class="row col-md-10" style="margin:20px; height: 65%; overflow-y: auto">
        <table class="table table-bordered table-edit table-hover table-responsive" id="tblOfficers" style="font-size: 12px;">
            <thead> 
                <tr>
                    <td>Emp No</td>
                    <td>Name</td>
                </tr>
            </thead>
            <tbody> 

            </tbody>
        </table>
    </div>
    <div class="row" >
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4"></div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" onclick="addOfficerToLoanForm();"><span class="glyphicon glyphicon-ok-circle"></span> Add</button>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" id="btnSearchUseCancel" onclick="cancelOfficersTable();"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                </div>
            </div>
        </div>
        <input type="hidden" id="hidOfficerType"/>
    </div>
</div>
<div id="message_newLonForm" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="docDivCheck" class="searchCustomer" style="width: 65%;border: 0px solid #5e5e5e;"></div>
<div class="searchCustomer" id="editRate_div" style="width: 40%; margin-left: 10%; margin-top: 10%; border:4px solid #1992d1"></div>
<input type="hidden" value="${message}" id="testMessage"/>



<!--BlockUI-->
<script type="text/javascript">

    //Block Load SearchCustomer Form
    $(document).ready(function() {
        $('#btnLoadGuarantor').click(function() {
            ui('#popup_searchCustomer');
            loadSearchCustomer(2);
        });
        $('#btnSearchCustomerExit').click(function() {
            unblockui();
        });

        //Block Laod GuarantorForm(Load SearchCustomer Form)    
        $('#btnSearchCustomer').click(function() {
            ui('#popup_searchCustomer');
            loadSearchCustomer(1);
        });
        $('#btnSearchCustomerExit').click(function() {
            unblockui();
        });

        //Block Laod SaleParsonName Form    
        $('#btnLoadSaleParsonName').click(function() {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 1st approval");
            searchUserType(1);
        });
        $('#btnSearchUseCancel').click(function() {
            unblockui();
        });

        //Block Load Recovery Manager Form    
        $('#btnRecoveryManager').click(function() {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 2nd approval");
            searchUserType(2);
        });
        $('#btnSearchUseCancel').click(function() {
            unblockui();
        });

        //Block Load Supplier Form    
        $('#btnSupplier').click(function() {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 3rd approval");
            searchUserType(3);
        });
        $('#btnSearchUseCancel').click(function() {
            unblockui();
        });

        //Block Load property Form    
        $('#btnAddProperty').click(function() {
            ui('#popup_addProperty');
            loadPropertyForm();
        });
        $('#btnSearchUseCancel').click(function() {
            unblockui();
        });


    });



    $(function() {

        //focus select loantype option
        $('select[name^="loanTypeList"]').eq(1).focus();

        //disabl enter key for submitting form
//        $(window).keydown(function(event) {
//            if (event.keyCode == 13) {
//                event.preventDefault();
//                return false;
//            }
//        });
        jQuery.extend(jQuery.expr[':'], {
            focusable: function(el, index, selector) {
                return $(el).is('a, button, :input, [tabindex]');
            }
        });

        //got to next input from enter key
        $(document).on('keypress', 'input,select,textarea', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(':focusable');
                var index = $canfocus.index(this) + 1;
                if (index >= $canfocus.length)
                    index = 0;
                $canfocus.eq(index).focus();
            }
        });

        //press enter on button
        $('input[type="button"]').on("keypress", function(eve) {
            var key = eve.keyCode || e.which;
            if (key == 13) {
                $(this).click();
            }
            return false;
        });

        //enter loan amount and goto next input using tab/enter
        $('input[name=loanAmount]').keydown(function(e) {
            var code = e.keyCode || e.which;
            if (code === 9 || code === 13) {
                var loanAmount = $(this).val();
//                var f_loanAmount = parseFloat(loanAmount);

                $("#viewLoanAmount").autoNumeric();
                $("#viewLoanAmount").val(loanAmount);
                calculateLoan();
            }
        });

        //focus other input field other than loan amount
        $("input[name=loanAmount]").blur(function() {
            var loanAmount = $(this).val();
            $("#viewLoanAmount").autoNumeric();
            $("#viewLoanAmount").val(loanAmount);
            calculateLoan();
            calculateInvestment();
        });
        //focus other input field other than loan period
        $("input[name=loanPeriod]").blur(function() {
            var period = $(this).val();
            $("#viewLoanPeriod").val(period);
            calculateLoan();
        });
        //focus other input field other than loan downpayment
        $("input[name=loanDownPayment]").blur(function() {
            calculateInvestment();
        });

        //enter loan period and goto next input using tab
        $('input[name=loanPeriod]').keydown(function(e) {
            var code = e.keyCode || e.which;
            if (code === 9 || code === 13) {
                var period = $(this).val();
                $("#viewLoanPeriod").val(period);
                calculateLoan();
            }
        });
        $("input[name=loanAmount]").blur(function() {
            var period = $(this).val();
            $("#viewLoanPeriod").val(period);
            calculateLoan();
        });

        //format desimal numbers
        $('.decimalNumber').autoNumeric();

        //load loan types
//        $.getJSON('/AxaBankFinance/loadLoanType', function(data) {
//            for (var i = 0; i < data.length; i++) {
//                var name = data[i].loanTypeName;
//                var id = data[i].loanTypeId;
//                $('#loanTypeList').append("<option value='" + id + "' >" + name + "</option>");
//            }
//        });



        //change loantype change loan rate
        $('#loanTypeList').change(function() {
            var selected = $(this).find('option:selected');
            var loanType = selected.val();
            if (loanType > 0) {
                $.getJSON('/AxaBankFinance/subLoanTypes/' + loanType, function(data) {
                    ui('#popup_subLoanTypes');
                    $("#tblsubtypes tbody").html("");
                    for (var i = 0; i < data.length; i++) {
                        $("#tblsubtypes tbody").append("<tr onclick='clickLoanSubtype(this)'><td>" + data[i].subLoanName +
                                "<input type='hidden' value='" + data[i].subLoanId + "'/></td><td style='display:none'><a onclick='editRate()'>" + data[i].subInterestRate + "</a></td></tr>");
                    }
                });
            }
        });
        //change oter property type 
        $('#slctProperty').change(function() {
            loadPropertyForm();
        });



    });

//    click subloanType
    function clickLoanSubtype(tr) {
        var loanTypeId = $(tr).find("input").val();
        var rate = $(tr).find('a').text();
        $("#viewRate").val(rate);
        $("input[name=loanRate]").val(rate);
        $("input[name=loanType]").val(loanTypeId);
        calculateLoan();
        unblockui();
        $("#loanAmountId").focus();
        loadOtherCharges(loanTypeId);
    }

//    Edit Rate
    function editRate() {
//        var loanRate = $(subLoan).val();
        alert("loanRate");
    }

//load other charges types
    function loadOtherCharges(subId) {
        $.getJSON('/AxaBankFinance/otherChargesByLoanId/' + subId, function(data) {
            $('#otherChargesTable tbody').html("");

            for (var i = 0; i < data.length; i++) {
                var description = data[i].subTaxDescription;
                var isPresentage = data[i].isPrecentage;
                var charge, t, rateCharge;
                if (isPresentage) {
                    charge = data[i].subTaxRate;
                    rateCharge = charge + "%";
                    t = 1;
                }
                else {
                    charge = data[i].subTaxValue;
                    rateCharge = charge;
                    t = 0;
                }
                var id = data[i].subTaxId;
                $('#otherChargesTable tbody').append("<tr id='" + id + "'>" +
                        "<td>" + description + "<input type='hidden' value='" + t + "' id='t" + i + "'/><input type='hidden' value='" + id + "' id='cha" + i + "'/></td>" +
                        "<td><input type='hidden' value='" + charge + "' id='c" + i + "'/>" + "<a href='#' onclick='editCharge(" + i + ")' id='aaa" + i + "'>" + rateCharge + "</a></td>" +
                        "<td style='text-align:right' id='amount" + i + "'><input type='text' id='hidAmnt" + i + "' readonly/></td></tr>");

            }
            $('#otherChargesTable tbody').append("<tr><td colspan='2'><label>Total</label></td><td class='decimalNumber'><label id='totalOther'><label></td>");
        });


    }



    //click loan other charges row
    var totalCharges = 0.00;
    function calculateCharge() {
        totalCharges = 0.00;
        var i = 0;
        var loanAmount = $("input[name=loanInvestment]").val();
        $('#otherChargesTable tbody tr').each(function(i) {
            if ($(this).is(":last-child")) {
                console.log("last");
            } else {
                var labelId = "amount" + i;
                var amount;
                var chargeType = $("#t" + i).val();
                var charge = $("#c" + i).val();
                if (chargeType == 1) {
                    if (loanAmount != "") {
                        amount = ((charge * parseFloat(loanAmount)) / 100).toFixed(2);
                    } else {
                        amount = (parseFloat(0)).toFixed(2);
                    }
                } else {
                    amount = (parseFloat(charge)).toFixed(2);
                }
//                $("#" + labelId).text(amount);
                $("#hidAmnt" + i).val(amount);
                totalCharges = parseFloat(totalCharges) + parseFloat(amount);
                $("#totalOther").text(totalCharges.toFixed(2));
            }
            i++;
        });
    }

    function loadSearchCustomer(type) {
        $("#gurant_debtr_type").val(type);
        $.ajax({
            url: '/AxaBankFinance/searchCustomer',
            success: function(data) {
                $('#popup_searchCustomer').html("");
                $('#popup_searchCustomer').html(data);

            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    function unloadSearchCustomer() {
        unblockui();
    }

    function saveLoanForm() {
        var validate = validateForm();
        SetOtherCharges();
        calculateLoan();
        if (validate) {
            $.ajax({
                url: '/AxaBankFinance/saveNewLoanForm',
                type: 'POST',
                data: $("#newLoanForm").serialize(),
                success: function(data) {
                    $('#formContent').html(data);
                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        } else {
            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html(errorMessage);
            errorMessage = "";
            $("html, body").animate({scrollTop: 0}, "slow");
        }

    }

    function SetOtherCharges() {
        var i = 0;
        $('#otherChargesTable tbody tr').each(function(i) {
            if ($(this).is(":last-child")) {
                console.log("lastRO");
            } else {
                console.log("ROW" + i);
                var chargeId = $("#cha" + i).val();
                var charge = $("#c" + i).val();
                console.log(charge);
                var amount = $("#hidAmnt" + i).val();
                $("#hidCharges").append("<input type='hidden' name='chragesId' value='" + chargeId + "'/>" +
                        "<input type='hidden' name='chragesRate' value='" + charge + "'/>" +
                        "<input type='hidden' name='chragesAmount' value='" + amount + "'/>");
            }
            i++;
        });
    }


    function calculateLoan() {
        var loanTYpe = $(this).find('option:selected').val();
        if (loanTYpe == 0)
            return;
        var amount = $("input[name=loanInvestment]").val();
        if (amount == null || amount == "")
            return;
        var period = $("#viewLoanPeriod").val();
        if (period == null || period == "")
            return;

        var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));

        var newRate = $("#viewRate").val();
        var rate = newRate.replace('%', '');
        var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);
        var totalInterest = (parseFloat(interest) * period).toFixed(2);
        var totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
        var installment = (parseFloat(totalAmount) / period).toFixed(2);

        var total = parseFloat(totalAmount).toFixed(2);

        $("#viewInstallment").val(installment);
        $("#viewInterest").val(interest);
        $("#viewTotalInterest").val(totalInterest);
        $("#viewTotal").val(total);
    }

    function calculateInvestment() {
        var loanAmount = $("input[name=loanAmount]").val();
        var downPayment = $("input[name=loanDownPayment]").val();
        if (loanAmount != "" && downPayment != "") {
            var f_amount = parseFloat(loanAmount);
            var f_downpayment = parseFloat(downPayment);
            if (f_amount > f_downpayment) {
                var investment = f_amount - f_downpayment;
                $("input[name=loanInvestment]").val(investment);
                $("#viewDownPayment").val(downPayment);
                $("#viewInvestment").val(investment);
                calculateCharge();
            } else {
                alert("Loan amount should be greater than down payment");
                $("input[name=loanDownPayment]").css("border", "1px solid red");
                $("input[name=loanDownPayment]").focus();
            }
        }
    }

    var errorMessage = "";
    function validateForm() {
        var validate = true;

        var loanTYpe = $("#loanTypeList").find('option:selected').val();
        if (loanTYpe == 0) {
            validate = false;
            errorMessage = errorMessage + "<br>" + "Select LoanType";
            $("#loanTypeList").addClass("txtError");
        } else {
            $("#loanTypeList").removeClass("txtError");
        }
        var loanAmount = $('input[name=loanAmount]').val();
        if (loanAmount == null || loanAmount == "") {
            validate = false;
            errorMessage = errorMessage + "<br>" + "Enter Loan Amount";
            $("#loanAmountId").addClass("txtError");
        } else {
            $("#loanAmountId").removeClass("txtError");
        }

        var period = $('input[name=loanPeriod]').val();
        if (period == null || period == "") {
            validate = false;
            errorMessage = errorMessage + "<br>" + "Enter Loan Time Period";
            $('input[name=loanPeriod]').addClass("txtError");
        } else {
            $('input[name=loanPeriod]').removeClass("txtError");
        }
//        var debtorID = $("#debID").val();
//        if (debtorID == null || debtorID == "") {
//            validate = false;
//            errorMessage = errorMessage + "<br>" + "Select a Debtor";
//            $("#debtorName").addClass("txtError");
//        } else {
//            $("#debtorName").removeClass("txtError");
//        }
//
//        var guarantord = $('input[name=guarantorID]').val();
//        if (guarantord == null) {
//            validate = false;
//            errorMessage = errorMessage + "<br>" + "Select atleast one Guarantor";
//            $("#guarantorName").addClass("txtError");
//        } else {
//            $("#guarantorName").removeClass("txtError");
//        }

        //validate approvelevels
        var app1 = $("#salesPersonName").val();
        var app2 = $("#recoveryPersonName").val();
        var app3 = $("#supplierName").val();
        if (app1 === "") {
            var comment1 = $("#app1_comment").val();
            if (comment1 === "") {
                validate = false;
                errorMessage = errorMessage + "<br>" + "Add Comment If you are skipped first approval";
                $("#app1_comment").addClass("txtError");
            } else {
                $("#app1_comment").removeClass("txtError");
            }
        } else {
            $("#app1_comment").removeClass("txtError");
        }
        if (app2 === "") {
            var comment2 = $("#app2_comment").val();
            if (comment2 === "") {
                validate = false;
                errorMessage = errorMessage + "<br>" + "Add Comment If you are skipped second approval";
                $("#app2_comment").addClass("txtError");
            } else {
                $("#app2_comment").removeClass("txtError");
            }
        } else {
            $("#app2_comment").removeClass("txtError");
        }
        if (app3 === "") {
            validate = false;
            errorMessage = errorMessage + "<br>" + "You Should add final approval";
            $("#supplierName").addClass("txtError");
        } else {
            $("#supplierName").removeClass("txtError");
        }


        return validate;
    }

    //add sales person,recovery manager, supplier
    function searchUserType(type) {
        $("#hidOfficerType").val(type);
        var loanTypeId = $("input[name=loanType]").val();
        if (loanTypeId === "") {
            loanTypeId = 0;
        }
        $.ajax({
            url: '/AxaBankFinance/SearchOfficers/' + type + "/" + loanTypeId,
            success: function(data) {
                var tbody = $("#tblOfficers tbody");
                tbody.html("");
                for (var i = 0; i < data.length; i++) {
                    tbody.append("<tr onclick='clickOfficer(this)'><td>" + data[i].empId + "</td>" +
                            "<td>" + data[i].userName + "</td>" +
                            "<td style='display:none'>" + data[i].userId + "</td></tr>");
                }
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    //click supplier row and add him to loan form
    var o_empNo = "", o_name = "", o_userId = "";
    function addOfficerToLoanForm() {
        var type = $("#hidOfficerType").val();
        if (o_userId == "") {
            alert("Please Select a name");
            return;
        } else {
            if (type == 1) {
                $("#salesPersonId").val(o_userId);
                $("#salesPersonName").val(o_name);
            }
            else if (type == 2) {
                $("#recoveryPersonId").val(o_userId);
                $("#recoveryPersonName").val(o_name);
            }
            else if (type == 3) {
                $("#supplierId").val(o_userId);
                $("#supplierName").val(o_name);
            }
        }
        o_empNo = "", o_name = "", o_userId = "";
        unblockui();
    }

    //click supplier,recovery, sales offices table row
    function clickOfficer(tr) {
        var selected = $(tr).hasClass("highlight");
        $("#tblOfficers tbody tr").removeClass("highlight");
        if (!selected)
            $(tr).addClass("highlight");

        var tableRow = $(tr).children("td").map(function() {
            return $(this).text();
        }).get();
        o_empNo = $.trim(tableRow[0]);
        o_name = $.trim(tableRow[1]);
        o_userId = $.trim(tableRow[2]);
    }







    //close sales person, recovery officer, supplier pop up
    function cancelOfficersTable() {
//        o_id = "", o_name = "", o_address = "";
//        unblockui();
    }

    //other property
    function loadPropertyForm() {
        var select = $("#slctProperty").find('option:selected').val();
        $.ajax({
            url: '/AxaBankFinance/loadPropertForm/' + select,
            success: function(data) {
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Save");
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    //edit property
    function editPropertyForm(type, id) {
        $.ajax({
            url: '/AxaBankFinance/editPropertForm/' + type + '/' + id,
            success: function(data) {
                ui('#popup_addProperty');
                $("#slctProperty").css("display", "none");
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Udate");
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    //search customer by nic
    $('#debtorNic').keyup(function(e) {
        var nicNo = $(this).val();
        if (nicNo.length === 10) {
            $.ajax({
                dataType: 'json',
                url: '/AxaBankFinance/searchByNic_new/' + nicNo,
                success: function(data) {
                    var debId = data.debtorId;
                    if (debId > 0) {
                        var name = "", nic = "", address = "", telephone = "";
                        if (data.nameWithInitial !== null)
                            name = data.nameWithInitial;
                        if (data.debtorNic !== null)
                            nic = data.debtorNic;
                        if (data.debtorPersonalAddress !== null)
                            address = data.debtorPersonalAddress;
                        if (data.debtorTelephoneMobile !== null)
                            telephone = data.debtorTelephoneMobile;

                        $("#debID").val(debId);
                        $("#debtorNic").val(nic);
                        $("#debtorName").val(name);
                        $("#debtorAddress").val(address);
                        $("#debtorTelephone").val(telephone);

                        var url2 = "/AxaBankFinance/viewProfilePicture/" + debId;
                        $("#customerProfilePicture").html("<img src='" + url2 + "' ></img>");

                    } else {
                        alert("Search Result Not found");
                        $("#debtorNic").val("");
                        $("#debID").val("");
                        $("#debtorName").val("");
                        $("#debtorAddress").val("");
                        $("#debtorTelephone").val("");
                    }

                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        }
    });

    //search gurantor by nic
    $('#gurantorNic').keyup(function(e) {
        var nicNo = $(this).val();
        console.log("enter+" + nicNo);
        if (nicNo.length === 10) {
            $.ajax({
                dataType: 'json',
                url: '/AxaBankFinance/searchByNic_new/' + nicNo,
                success: function(data) {
                    var debId = data.debtorId;
                    if (debId > 0) {
                        var name = "", nic = "", address = "", telephone = "";
                        if (data.nameWithInitial !== null)
                            name = data.nameWithInitial;
                        if (data.debtorNic !== null)
                            nic = data.debtorNic;
                        if (data.debtorPersonalAddress !== null)
                            address = data.debtorPersonalAddress;
                        if (data.debtorTelephoneMobile !== null)
                            telephone = data.debtorTelephoneMobile;

                        $("#guarantorName").val(name);
                        $("#gurantorNic").val(nic);
                        var table = document.getElementById("guranterTable");
                        var rowCount = table.rows.length;
                        $('#guranterTable tbody').append('<tr id="' + debId + '"><td>' +
                                rowCount + '</td><td>' + name + '</td><td>' + nic + '</td><td>' +
                                address + '</td><td>' + telephone + '</td><td>' +
                                '<div onclick="" class="edit"></div><div class="delete" onclick="deleteRow(' + debId + ')" ></div>' + '</td></tr>');
                        $("#guarantorIDs").append("<input type='hidden' name='guarantorID' id='g_hid" + debId + "' value='" + debId + "'>");

                    } else {
                        alert("Search Result Not found");
                        $("#guarantorName").val("");
                        $("#gurantorNic").val("");
                    }

                },
                error: function() {
                    alert("Error Loading...");
                }
            });
        }
    });

//delete gurantor
    function deleteRow(dId) {
        $('#' + dId).remove();
        $('#g_hid' + dId).remove();
        $("#guarantorName").val("");
    }

//search loan by loan id
    function searchLoan() {
        var loanId = $("input[name=loanId]").val();
        var view = "message_newLonForm";
        if (loanId !== "") {
            $.ajax({
                url: '/AxaBankFinance/searchLoanDeatilsById/' + loanId,
                success: function(data) {
                    $('#formContent').html(data);
                    var msg = $("#testMessage").val();
                    if (msg != "") {
                        warning(msg, view);
                    }
                },
                error: function() {
                    alert("Error Loading...");
                }
            });

        } else {
            alert("Enter Loan Id");
        }

    }

    //proceed button
    function processLoanToApproval() {
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loanDocumentSubmissionStatus/" + loan_id,
            success: function(data) {
                var process = data;
                var view = "message_newLonForm";
                if (process == 1) {
                    //cannot process
                    var msg = "You have to submit minimum documents to proceed";
                    warning(msg, view);
                } else if (process == 2) {
                    //process with pending dates
                    var msg = "Do you want to proceed loan without completing all documents";
                    dialog(msg, view, loan_id);
                } else {
                    //procees successfully
                    ui("#proccessingPage");
                    viewAllLoans(0);
                }
            },
            error: function() {

            }
        });
    }

    //document upload button
    function uploadDocuments() {
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loadDocumentChecking",
            data: {loan_id: loan_id},
            success: function(data) {
                $("#docDivCheck").html(data);
                ui('#docDivCheck');
            }
        });
    }

    //edit charge 
    function editCharge(row) {
        console.log(row);
        $.ajax({
            url: "/AxaBankFinance/loadEditRate/" + row,
            success: function(data) {
                $("#editRate_div").html(data);
                ui("#editRate_div");
            }
        });
    }



</script>
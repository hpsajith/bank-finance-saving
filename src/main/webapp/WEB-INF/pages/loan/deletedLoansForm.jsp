<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblRemove").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>

<table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblRemove"  >
    <thead>
    <tr>
        <th>Loan Id</th>
        <th>Member No</th>
        <th>Loan Type</th>
        <th>Debtor Name</th>
        <th>Loan Amount</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="loans" items="${loanList}">
        <tr id="${loans[0]}">
            <td>${loans[0]}</td>
            <td>${loans[4]}</td>
            <td>${loans[1]}</td>
            <td>${loans[2]}</td>
            <td>${loans[3]}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>



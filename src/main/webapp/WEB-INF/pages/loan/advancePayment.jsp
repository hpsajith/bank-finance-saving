<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>

<div class="row" style="border: 1px solid #0075b0;margin-top: 10px; " id="formCheque">
    <div class="row"style="margin: 10px">
        <form name="checkPayment" id="formCheckPayment">
            <input type="hidden" name="customerCode" value="${loanId}" />
            <input type="hidden" name="transactionNo" value="${voucher}" />
            <div class="row"><div class="col-md-12"><legend><strong>Advanced Payment</strong></legend></div></div>
            <div class="row">
                <div class="col-md-3" style="text-align: left"><label>Cheque No</label></div>
                <div class="col-md-4"><input type="text" name="chequeNo" onfocus style="width: 100%"/></div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-3" style="text-align: left"><label>Cheque Date</label></div>
                <div class="col-md-4"><input class="txtCalendar" type="text" name="realizeDate" id="exDate_ad" style="width: 100%"/></div>
                <div class="col-md-1"><input type="checkbox" id="chqIsFull_ad"/></div>   
                <div class="col-md-3" style="text-align: left">Is Full Year</div>
            </div>

            <div class="row">
                <div class="col-md-3" style="text-align: left"><label>Bank Name</label></div>
                <div class="col-md-4" >
                    <select name="bank">  
                        <c:forEach var="banki" items="${banks}">
                            <option value="${banki.bankId}">${banki.configBankName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-md-1">
                    <input type="checkbox" id="accPyOnly_ad"/>
                </div>
                <div class="col-md-3" style="text-align: left">
                    Account Pay Only
                </div>
            </div>
            <div class="row">
                <div class="col-md-3" style="text-align: left"><label>Amount</label></div>
                <div class="col-md-4"><input type="text" name="amount" value="${amount}" id="finalchekAmount_ad" onkeypress="return checkNumbers(this)" style="width: 100%"/></div>
                <div class="col-md-4"></div>
            </div>
            <div class="row" style="display: none">
                <div class="col-md-4">Amount in Text</div>
                <div class="col-md-4"><input type="text" name="amountInTxt" id="amountInword_ad" /></div>
                <div class="col-md-4"></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-2"><input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"/></div>
                <div class="col-md-2"><input type="button" class="btn btn-default col-md-12" value="Save" onclick="saveAdvancedPayment()" id="save_advance_but"/></div>
                <div class="col-md-4"></div>
            </div>
        </form>

        <form method="GET" action="/AxaBankFinance/generatePDF" target="blank" id="formPDFCheckAdvance">
            <input type="hidden" name="hidCheqId"  id="txtHidCheq_Id"/>
            <input type="hidden" name="cheqType" value="2" />
            <input type="hidden" name="ChkDate" id="cheque_date_a" />
            <input type="hidden" name="cheqToName" id="cheque_toName_a" value="1"/>
            <input type="hidden" id="TxtChqAmt_a" class="decimalField" name="TxtChqAmt" maxlength="15" />
            <input type="hidden" id="TxtChqAmtINWord_a" name="TxtChqAmtINWord" maxlength="500" size="100"/>
            <input type="hidden" id="IsFullDate_a" name="IsFullDate" />
            <input type="hidden" id="ChbIsACPay_a" name="ChbIsACPay" />
        </form>
    </div>
</div>
<div id="message_advance" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script>
    $(function() {
        $(".txtCalendar").datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $("#finalchekAmount_ad").blur(function() {
            if (!isNaN(parseFloat($(this).val().trim()))) {
                var payingAmount = $(this).val();
                var a = payingAmount.split(".");
                var words = toWords(a[0]);
                var rup = words.toUpperCase() + " RUPEES ";
                if (parseFloat(a[1]) > 0) {
                    rup += " AND " + toWords(a[1]).toUpperCase() + " CENTS";
                }
                rup += " ONLY";
                $("#TxtChqAmtINWord_a").val(rup);
            } else {
                $("#TxtChqAmtINWord_a").val("");
            }
        });
    });

    function saveAdvancedPayment() {
        var cheDate = $("#exDate_ad").val();
        $("#cheque_date").val(cheDate);
        var payingAmount = $("#finalchekAmount_ad").val();
        var words = toWords(payingAmount);
        $("#amountInword_ad").val(words);
        $.ajax({
            url: "/AxaBankFinance/saveAdvancedPayment",
            type: 'POST',
            data: $("#formCheckPayment").serialize(),
            success: function(data) {
                $("#message_advance").html(data);
                ui("#message_advance");
                var cheqId = $("#hid_pendingstatus").val();
                if (cheqId > 0) {
                    $("#txtHidCheq_Id").val(cheqId);
                    var amount = $("#finalchekAmount_ad").val();
                    $("#TxtChqAmt_a").val(amount);
                    $("#cheque_date_a").val(cheDate);
                    if ($("#accPyOnly_ad").is(':checked')) {
                        $("#ChbIsACPay_a").val("on");
                    } else {
                        $("#ChbIsACPay_a").val("off");
                    }
                    if ($("#chqIsFull_ad").is(':checked')) {
                        $("#IsFullDate_a").val("on");
                    } else {
                        $("#IsFullDate_a").val("off");
                    }
                    if ($("#TxtChqAmtINWord_a").val() == "") {                        
                        if (!isNaN(parseFloat($("#finalchekAmount_ad").val().trim()))) {
                            var payingAmount = $("#finalchekAmount_ad").val();
                            
                            var a = payingAmount.split(".");
                            var words = toWords(a[0]);
                            var rup = words.toUpperCase() + " RUPEES ";
                            if (parseFloat(a[1]) > 0) {
                                rup += " AND " + toWords(a[1]).toUpperCase() + " CENTS";
                            }
                            rup += " ONLY";
                            $("#TxtChqAmtINWord_a").val(rup);
                        } else {
                            $("#TxtChqAmtINWord_a").val("");
                        }
                    }
                    $("#formPDFCheckAdvance").submit();
                    var l_id = $("input[name=customerCode]").val();
//                    loadLoanProcessPage2(l_id);
                }
            }
        });
    }
</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Recovery Manager Approval</strong>
    </div>
    <form id="form_recoveryReport" name="n_recoveryReport">
        <div class="row" style="margin-top: 10px;">
            <!--//field officer comment-->
            <c:choose>
                <c:when test="${recovery.recoveryStatus==1}">
                    <div class="col-md-12" style="margin-top: 20px; text-align: left; margin-left: 20px;">
                        <div class="row"><label style="color:red"> Recovery Report is Pending</label></div>
                        <div class="row"><p>Assign to ${officerName} on ${recovery.recoveryDate}</p></div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row" style="text-align: left">
                        <div class="col-md-8" style="text-align: left">
                            <label style="color: #4cae4c">Recovery report is submitted</label><br/>
                            <p>By ${officerName}</p>                        
                        </div>
                        <div class="col-md-4">                            
                            <input type="button" class="btn btn-default col-md-12" value="View" onclick="viewRecoveryReport();"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Assign Date:${recovery.recoveryDate}</div>
                        <div class="col-md-6">Submitted Date:${recovery.recoverySubmitDate}</div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-md-12">
            <label id="lblapproveTitle"></label>
            <input type="hidden" name="loanId" id="txtLoanId_manager" value="${approve.loanId}"/>
            <input type="hidden" name="approvedLevel" id="txtApproveLevel_manager" value="${approve.approvedLevel}"/>
            <input type="hidden" name="pk" id="apprv_Pk" value="${approve.pk}"/>
            <div class="row" style="padding: 5px;">

                <c:choose>
                    <c:when test="${approve.pk>0}">
                        <c:choose>
                            <c:when test="${approve.approvedStatus}">
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="1" checked="true"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="0" /></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="1"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="approvedStatus" value="0" checked="true"/></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:otherwise>                    
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-1"><input type="radio" name="approvedStatus" value="1"/></div>
                        <div class="col-md-3"><strong>Approve</strong></div>
                        <div class="col-md-1"><input type="radio" name="approvedStatus" value="2" /></div>
                        <div class="col-md-3"><strong>Reject</strong></div>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Comment</div>
                <div class="col-md-10"><textarea rows="3" name="approvedComment" style="width: 100%">${approve.approvedComment}</textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="row" style="margin-top: 15px">  
                <c:choose>
                    <c:when test="${type==1}">
                        <div class="col-md-4">
                            <c:if test="${recovery.recoveryStatus==1}">
                                <input type="button" value="Change Officer" class="btn btn-default col-md-12" onclick="changeFieldOfficer()"/>
                            </c:if>
                        </div>
                        <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                            <c:choose>
                                <c:when test="${approve.pk>0}">
                                <div class="col-md-4"><input type="button"  value="Update" class="btn btn-default col-md-12" onclick="submitLoanApproveLevels2(${type})"></div>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${recovery.recoveryStatus==1}">
                                        <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="submitLoanApproveLevels2(${type})" disabled></div>
                                        </c:when>
                                        <c:otherwise>
                                        <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="submitLoanApproveLevels2(${type})"></div>
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                        <div class="col-md-8"></div>
                        <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                        </c:otherwise>
                    </c:choose>   
            </div>
        </div> 
    </form>
    <form method="POST" action="/AxaBankFinance/ReportController/viewRecoverReport" target="blank" id="viewRecovery">
        <input type="hidden" name="loan_id_r" value="${approve.loanId}"/> 
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>
<div id="message_recoveryReport" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<!--//view report-->
<div class="searchCustomer" id="reportBox" style="width: 60%; margin-left: 3%">
    <div class="close_div" onclick="unloadReportView()"><label>X</label></div>
    <div style="clear:both"></div>
    <div class="row" id="report_title" style="margin-left: 15px; margin-right: 15px;">Recovery Report</div>
    <div class="row" id="report_content" style="margin-top: 15px; padding:5px; height: 400px; overflow-y: auto;">

    </div>
</div>


<script>
    function changeFieldOfficer() {
        var loanId = $("#txtLoanId_manager").val();
        $.ajax({
            url: "/AxaBankFinance/changeFieldOfficer/" + loanId,
            success: function(data) {
                $("#message_recoveryReport").html(data);
                ui("#message_recoveryReport");
            },
            error: function() {
                alert("Error Loading");
            }
        });
    }

    function submitLoanApproveLevels2(type) {
        $.ajax({
            url: '/AxaBankFinance/saveApproveLevel',
            type: 'POST',
            data: $("#form_recoveryReport").serialize(),
            success: function(data) {
                viewAllLoans(0);
                $('#message_approveLoan').html(data);
                ui("#message_approveLoan");
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    function viewRecoveryReport() {
        $("#viewRecovery").submit();
    }
</script>
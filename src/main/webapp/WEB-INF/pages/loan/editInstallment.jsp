<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblViewins").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });


</script>

<div class="row" style="margin-top: 20px">
    <div class="col-md-2"><label>Loan Agreement NO:</label></div>
    <div class="col-md-2"><input type="text" name="" id="searchByagg" value="${aggNo}"/></div>
    <div class="col-md-2"><input type="button" name="" id="butaggNo" class="btn btn-default" value="Search" onclick="searchLoanInstallment()"/></div>
</div>
<div class="row" style="margin-top: 20px">
    <div class="col-md-12">
        <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0" cellpadding="0" id="tblViewins"  >
            <thead>
                <tr><th>No</th>
                    <th>Due Date</th>
                    <th>Capital</th>
                    <th>Interest</th>
                    <th>Total Rental</th> 
                    <th>Balance</th> 
                    <th>Remark</th> 
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>                    
                <c:forEach var="ins" items="${insList}" >
                    <c:choose>
                        <c:when test="${ins.insStatus==1}">
                            <tr style="background: #c0c0c0">
                                <td>${ins.insId}</td>
                                <td>${ins.dueDate}</td>
                                <td>${ins.insPrinciple}</td>
                                <td>${ins.insInterest}</td>
                                <td>${ins.insAmount}</td>
                                <td>${ins.loanBalance}</td>
                                <td><label style="color: #FF0000">Cannot Edit</label></td>
                                <td></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>                       
                                <td>${ins.insId}<input type="hidden" value="${ins.loanId}" id="loanIdi${ins.insId}"/></td>
                                <td><input type="text" value="${ins.dueDate}" name="dueDate" id="duedate${ins.insId}" class="txtCalendar"/></td>
                                <td><input type="text" value="${ins.insPrinciple}" name="insPrinciple" id="prncId${ins.insId}" onkeypress="return checkNumbers(this)" onkeyup="calculateTotal(${ins.insId})"/></td>
                                <td><input type="text" value="${ins.insInterest}" name="insInterest" id="interId${ins.insId}" onkeypress="return checkNumbers(this)" onkeyup="calculateTotal(${ins.insId})"/></td>
                                <td><input type="text" value="${ins.insAmount}" name="insAmount" id="totalId${ins.insId}" readonly=""/></td>
                                <td><input type="text" value="${ins.loanBalance}" name="loanBalance" id="balance${ins.insId}" readonly=""/></td>
                                <td><label style="color: #0174DF">Edited ${ins.isChange} times </label></td>
                                <td><input type="button" class="btn btn-default" value="Update" onclick="UpdateRental(${ins.insId})"/></td>                                
                            </tr>
                        </c:otherwise>
                    </c:choose>      
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div id="msg_instlmnt" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>


<script>
    $(function () {
        var dateToday = new Date();
        var dates = $(".txtCalendar").datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: "+1w",
            changeYear: true,
            minDate: dateToday,
            showButtonPanel: true,
            onSelect: function (selectedDate) {
                var option = this.id == "from" ? "minDate" : "maxDate",
                        instance = $(this).data("datepicker"),
                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });

    function calculateTotal(id) {
        var principle = $("#prncId" + id).val();
        var interest = $("#interId" + id).val();

        var total = 0.00;
        if (principle == "") {
            principle = 0.00;
        }
        if (interest == "") {
            interest = 0.00;
        }
        total = parseFloat(principle) + parseFloat(interest);

        $("#totalId" + id).val(total);
    }

    function searchLoanInstallment() {
        var loanNo = $("#searchByagg").val();
        if (loanNo.length > 0) {
            var newLoanNo = loanNo.replace(/\//g, "-");
            $.ajax({
                url: '/AxaBankFinance/findInstallemntByAggNo',
                data: {loanNo: newLoanNo},
                success: function (data) {
                    $('#formContent').html(data);
                },
                error: function () {
                    alert("Error Loading");
                }
            });
        }
    }

    function UpdateRental(id) {
        var principle = $("#prncId" + id).val();
        var interest = $("#interId" + id).val();
        var total = $("#totalId" + id).val();
        var dueDate = $("#duedate" + id).val();
        var loanId = $("#loanIdi" + id).val();

        $.ajax({
            url: "/AxaBankFinance/updateLoanRental",
            data: {loanId: loanId, insId: id, principle: principle, interest: interest, total: total, dueDate: dueDate},
            type: 'GET',
            success: function (data) {
                $('#msg_instlmnt').html(data);
                ui("#msg_instlmnt");
                searchLoanInstallment();
            },
            error: function () {
                alert("Error Loading");
            }
        });

    }

</script>

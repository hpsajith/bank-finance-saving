<%-- 
    Document   : addEmployeeFormView
    Created on : Apr 2, 2015, 4:10:58 PM
    Author     : SOFT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>

<div class="container-fluid">
    <br/>
    <br/>
    <table class="table table-hover table-hover table-condensed  well debtorTable" id="tabela" >
        <thead style="background-color: #eaeaea">
            <tr >
                <th style="text-align: center; font-size: 10px">ID</th>
                <th style="text-align: center; font-size: 10px">Name</th>
                <th style="text-align: center; font-size: 10px">NIC</th>
                <th style="text-align: center; font-size: 10px">Acc No</th>
                <th style="text-align: center; font-size: 10px">Amount</th>
                <th style="text-align: center; font-size: 10px">Period</th>
                <th style="text-align: center; font-size: 10px">Rate %</th>
                <th style="text-align: center; font-size: 10px">Interest</th>
                <th style="text-align: center; font-size: 10px">Total Interest</th>
                <th style="text-align: center; font-size: 10px">Installment</th>
                <th style="text-align: center; font-size: 10px">Total</th>
            </tr>
        </thead>
        <tbody>

            <c:set var="count" value="0" scope="page" />


            <c:forEach var="group" items="${groupUserList}">
                <c:set var="count" value="${count + 1}" scope="page"/>
                <tr>
                    <td style="font-size: 10px; width: 0px;">${group.debtorId}
                        <input type="hidden" value="${group.debtorId}" name="groupDetorIds">
                    </td>
                    <td style="font-size: 10px">${group.debtorName}</td>
                    <td style="font-size: 10px">${group.debtorNic}</td>
                    <td  style="font-size: 10px">${group.debtorAccountNo}</td>
                    <td  style="font-size: 10px"><input  type="text" name="groupDebtorAmount"  id="userAmount"  class="userAmount" style="font-weight: bold; text-align: right; width: 70px;" ></td>        
                    <td  style="font-size: 10px"><input type="text" name="groupDebtorPeriod" id="userPeriod" class="userPeriod" style="font-weight: bold; text-align: right; width: 30px;"></td>        
                    <td  style="font-size: 10px"><input type="text" name="groupDebtorRate" class="userRate" id="userRate"style="font-weight: bold; text-align: right; width: 30px;" onblur="calculateLoanForGroup(${count})"></td>        
                    <td  style="font-size: 10px"><input type="text" name="groupDebtorInterest" class="userInterest" id="userInterest" style="font-weight: bolder; text-align: right; width: 70px; color: #002eb4" readonly="true"   ></td>        
                    <td  style="font-size: 10px"><input type="text" name="groupDebtorTotalInterest" class="userTotalInterest" id="userTotalInterest" style="font-weight: bolder; text-align: right; width: 70px; color: #002eb4"  readonly="true"  ></td>        
                    <td  style="font-size: 10px"><input type="text" name="groupDebtorInstallment" class="userInstallment" id="userInstallment" style="font-weight: bolder; text-align: right; width: 70px; color: #002eb4" readonly="true"   ></td>        
                    <td  style="font-size: 10px"><input type="text" name="groupDebtorTotal" class="userTotal" id="userTotal" style="font-weight: bolder; text-align: right; width: 70px; color: #002eb4" readonly="true"  ></td>        
                </tr>
            </c:forEach>

        </tbody>
    </table>
</div>



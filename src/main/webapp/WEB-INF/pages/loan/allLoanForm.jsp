<%-- 
    Document   : addEmployeeFormView
    Created on : Apr 2, 2015, 4:10:58 PM
    Author     : SOFT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <form name="newOpenLoan_form" id="newOpenLoan_form_form">
        <div class="row"><legend><strong>Other Loans</strong></legend></div>

        <div class="row">
            <div class="col-md-4">Sub Loan Type</div>
            <div class="col-md-8">
                <select id="sunLoanId" name="loanType" class="col-md-6 col-sm-6 col-lg-6" style="padding: 1">
                    <option value="0">--Select Loan Type--</option>
                    <%--<c:forEach var="loantype" items="${subLoanList}">--%>
                        <%--<option value="${loantype.subLoanId}"> ${loantype.subLoanName}</option>--%>
                    <%--</c:forEach>--%>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">Period Type</div>
            <div class="col-md-8">
                <select name="periodType" id="" class="col-md-6 col-sm-6 col-lg-6" style="padding: 1">
                    <option value="0">--SELECT--</option> 
                    <option value="1">Month</option> 
                    <option value="2">Year</option> 
                    <option value="3">Days</option> 
                    <option value="4">Weeks</option> 
                    <option value="5">Laps</option> 
                    <option value="6">Legal</option> 
                    <option value="7">Seize</option> 
                    <option value="8">Full paid</option> 

                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Branch</div>
            <div class="col-md-8">
                <select name="branch" id="" class="col-md-6 col-sm-6 col-lg-6" style="padding: 1">
                    <option value="1">Padukka</option>
                    <option value="2">Horana</option>
                    <option value="3">Daraniyagala</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8"><input type="text" id="" name="bookNo" value="" style="width: 80%"/></div>
        </div>


        <hr/>
        <div class="row"><label style="font-weight: bold">Debtor Details</label></div>
        <div class="row">
            <div class="col-md-4">Debtor Name</div>
            <div class="col-md-8"><input type="text" id="" name="debtorName" value="" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Nic </div>
            <div class="col-md-8"><input type="text" id="txtNicNo" name="debtorNic" onblur="nictoLowerCase(this)" maxlength="10" value="${employee.nicNo}"style="width: 80%"/>
                <label id="msgNic" class="msgTextField"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Address</div>
            <div class="col-md-8"><textarea type="text" id="txtAddress1" name="debtorAdress" style="width: 80%"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Telephone No (Mobile)</div>
            <div class="col-md-8"><input type="text"  name="debtorTele1" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Telephone No (Home)</div>
            <div class="col-md-8"><input type="text"  name="debtorTele2" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Telephone No (Work)</div>
            <div class="col-md-8"><input type="text"  name="debtorTele3" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>

        <hr/>
        <div class="row">
            <div class="col-md-4">Loan Amount</div>
            <div class="col-md-8"><input type="text" name="loanAmount" onkeypress="return checkNumbers(this)"  value="0.00"style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Down Payment</div>
            <div class="col-md-8"><input type="text" name="downPayment" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">Investment</div>
            <div class="col-md-8"><input type="text" name="investment" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Due Date</div>
            <div class="col-md-8"><input type="text" name="dueDate"  value=""style="width: 80%" onkeypress="return checkNumbers(this)"/></div>
        </div>
        <!--    <div class="row">
                <div class="col-md-4">1st Due Date</div>
                <div class="col-md-8"><input type="text" name="" class="txtCalendar" value=""style="width: 80%"/></div>
            </div>-->
        <div class="row">
            <div class="col-md-4">End Date<label style="color:red;">*</label></div>
            <div class="col-md-8"><input type="text" name="lapsDate" class="txtCalendar" value=""style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Period</div>
            <div class="col-md-8"><input type="text" name="noOfInstallment" value="" style="width: 80%"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Installment</div>
            <div class="col-md-8"><input type="text" name="installment" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Interest Rate</div>
            <div class="col-md-8"><input type="text" name="rate" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Month Interest</div>
            <div class="col-md-8"><input type="text" name="monthInterest" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Paid Installment</div>
            <div class="col-md-8"><input type="text" name="paidInstallment" value="" style="width: 80%" onkeypress="return checkNumbers(this)"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Remain Installment</div>
            <div class="col-md-8"><input type="text" name="remainInstallment" value="" style="width: 80%" onkeypress="return checkNumbers(this)"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Suspend Installment</div>
            <div class="col-md-8"><input type="text" name="suspendInterest" value="0.00" style="width: 80%" onkeypress="return checkNumbers(this)"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Total Receivable</div>
            <div class="col-md-8"><input type="text" name="totalReceible" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Total Payment</div>
            <div class="col-md-8"><input type="text" name="totalPyaments" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Stock Balance</div>
            <div class="col-md-8"><input type="text" name="stockbalance" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Arrears without ODI</div>
            <div class="col-md-8"><input type="text" name="arresWithoutOdi" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Over Payment</div>
            <div class="col-md-8"><input type="text" name="OverPayment" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">No Of Arrears Installment</div>
            <div class="col-md-8"><input type="text" name="areasInstallment" value="" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>

        <hr/>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-4">Arrears Insurance</div>
                <div class="col-md-8"><input type="text" name="aresInsurance" value="0.00" style="width: 80%" onkeypress="return checkNumbers(this)"/></div>
            </div>

            <div class="row">
                <div class="col-md-4">Arrears ODI</div>
                <div class="col-md-8"><input type="text" name="aresOdi" value="0.00"  onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>

            <div class="row">
                <div class="col-md-4">Arrears Registration</div>
                <div class="col-md-8"><input type="text" name="aresRegisraion" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>

            <div class="row">
                <div class="col-md-4">Arrears Seizing</div>
                <div class="col-md-8"><input type="text" name="aresSeizing" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>

            <div class="row">
                <div class="col-md-4">Arrears Recovery</div>
                <div class="col-md-8"><input type="text" name="aresRecovery" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>

            <div class="row">
                <div class="col-md-4">Arrears Letter Posting</div>
                <div class="col-md-8"><input type="text" name="aresLetter" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>
            <div class="row">
                <div class="col-md-4">Arrears Valuation</div>
                <div class="col-md-8"><input type="text" name="aresValuation" value="0.00"  onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>

            <div class="row">
                <div class="col-md-4">Arrears Bank</div>
                <div class="col-md-8"><input type="text" name="aresBank" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>

            <div class="row">
                <div class="col-md-4">Arrears Check</div>
                <div class="col-md-8"><input type="text" name="aresCheq" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
            </div>
        </div>
        <hr/>

        <div class="row">
            <div class="col-md-4"><b>Total Arrears</b></div>
            <div class="col-md-8"><input type="text" name="totalArresrs" value="0.00" onkeypress="return checkNumbers(this)" style="width: 80%"/></div>
        </div>

        <hr/>
        <div class="row"><label style="font-weight: bold">Grantor 1 Details</label></div>
        <div class="row">
            <div class="col-md-4">Name</div>
            <div class="col-md-8"><input type="text" name="gurantor1Name" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Nic </div>
            <div class="col-md-8"><input type="text" id="txtNicNo" name="gurantor1Nic" onblur="nictoLowerCase(this)" maxlength="10" value="${employee.nicNo}"style="width: 80%"/>
                <label id="msgNic" class="msgTextField"></label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">Address</div>
            <div class="col-md-8"><textarea type="text" id="txtAddress1" name="gurantor1Adress" style="width: 80%"></textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">Telephone No 1</div>
            <div class="col-md-8"><input type="text"  name="gurantor1Tele1" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Telephone No 2</div>
            <div class="col-md-8"><input type="text"  name="gurantor1Tele2" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Telephone No 3</div>
            <div class="col-md-8"><input type="text"  name="gurantor1Tele3" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>

        <hr/>
        <div class="row"><label style="font-weight: bold">Grantor 2 Details</label></div>
        <div class="row">
            <div class="col-md-4">Name</div>
            <div class="col-md-8"><input type="text" name="gurantor2Name" value="" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Nic </div>
            <div class="col-md-8"><input type="text" id="txtNicNo" name="gurantor2Nic" onblur="nictoLowerCase(this)" maxlength="10" value="${employee.nicNo}"style="width: 80%"/>
                <label id="msgNic" class="msgTextField"></label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">Address</div>
            <div class="col-md-8"><textarea type="text" id="txtAddress1" name="gurantor2Adress" style="width: 80%"></textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">Telephone No 1</div>
            <div class="col-md-8"><input type="text"  name="gurantor2Tele1" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Telephone No 2</div>
            <div class="col-md-8"><input type="text"  name="gurantor2Tele2" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>

        <div class="row">
            <div class="col-md-4">Telephone No 3</div>
            <div class="col-md-8"><input type="text"  name="gurantor2Tele3" value="" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6"><input type="button" value="Cancel" style="width: 100%" class="btn btn-default col-md-12" onclick="pageExit()"/></div>
                    <div class="col-md-6"><input type="button" id="" style="width: 100%" value="Save" onclick="saveForm()" class="btn btn-default col-md-12"/></div>
                </div>

            </div>
        </div>


        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>
<div id="message_openLonForm" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>




<script type="text/javascript">
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });
    function saveForm() {

        var lapsdate = $("input[name=lapsDate]").val();
        if (lapsdate == "") {
            alert("Enter Laps Date")
            return;
        }
        $.ajax({
            url: '/AxaBankFinance/saveOpenLoan',
            type: 'POST',
            data: $("#newOpenLoan_form_form").serialize(),
            success: function(data) {
                $("#message_openLonForm").html(data);
                ui("#message_openLonForm");
                loadOpenLoan();
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }
</script>

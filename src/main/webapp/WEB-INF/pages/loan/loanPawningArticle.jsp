<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border:  1px solid #0088cc;width: 80%">
    <div class="row" style="margin: 10px;">
        <div class="col-md-12"><legend>Article Form</legend></div>
        <form name="formArticles" id="formArticleData">
            <div class="row">
                <input type="hidden" name="vehicleId" value="${vehicle.vehicleId}" />
                <div class="col-md-4">Dealer</div>
                <div class="col-md-8"><select name="dealerId" class="col-md-12">
                        <c:forEach var="dealer" items="${dealerList}">
                            <c:choose>
                                <c:when test="${dealer.supplierId==vehicle.dealerId}">
                                    <option selected="" value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${dealer.supplierId}">${dealer.supplierName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select></div>
            </div>
            <div class="row">
                <div class="col-md-4">Type Of Article</div>
                <div class="col-md-4">
                    <select name="articleType" id="slctArticle">
                        <option value="1">Chain</option>
                        <option value="2">Ring</option>
                        <option value="3">Pendant</option>
                        <option value="4">Necklace</option>
                        <option value="5">Bangles</option>
                        <option value="6">Bracelet</option>
                    </select>
                </div>

            </div>
            <div class="row">
                <input type="hidden" name="articleId" value="${article.articleId}"/>
                <div class="col-md-4">Total Weight</div>
                <div class="col-md-8"><input type="text" name="articleWeight1" style="width: 100%" value="${article.articleWeight1}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Net Weight</div>
                <div class="col-md-8"><input type="text" name="articleWeight2" style="width: 100%" value="${article.articleWeight2}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Carat Rate</div>
                <div class="col-md-8"><input type="text" name="articleCarrateRate" style="width: 100%" value="${article.articleCarrateRate}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Price</div>
                <div class="col-md-8"><input type="text" name="articlePrice" style="width: 100%" value="${article.articlePrice}"></div>
            </div>
            <div class="row">
                <div class="col-md-4">Identification</div>
                <div class="col-md-8"><textarea type="text" id="text_aricleIdentification" name="articleIdentification" style="width: 100%" >${article.articleIdentification}</textarea></div>
            </div>
            <div class="row">
                <div class="col-md-4">Other Comment</div>
                <div class="col-md-8"><textarea type="text" id="text_aricleComment" name="articleDamages" style="width: 100%" >${article.articleDamages}</textarea></div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button" name="" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button" name="" class="btn btn-default col-md-12 saveUpadte" value="Save" onclick="addArticleDetails()"></textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div> 
</div>     
<div id="msgDiv_other" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>  
<script>
    function addArticleDetails() {
        $.ajax({
            url: '/AxaBankFinance/saveArticleProperty',
            type: 'POST',
            data: $("#formArticleData").serialize(),
            success: function(data) {
                if (data > 0) {
                    success("Successfull Saved", "msgDiv_other");
                }
                var selectId = 2;
                $("#hidArticle").append("<input type='hidden' name='properyId' id='pap" + data + "' value='" + data + "'/><input type='hidden' value='" + selectId + "' id='pa" + data + "' name='proprtyType'/><input type='hidden' value='" + data + "' id='pap" + data + "' name='properyId'/>");
                addArticlesToTable(data);
                //unblockui();
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    function addArticlesToTable(data) {
        deleteArticle(data, 0);
        var type = $("#slctArticle").find('option:selected').text();
        var identification = $("#text_aricleIdentification").val();
        var description = $("#text_aricleComment").val();
        var price = $('input[name=articlePrice]').val();
        var totalWeight = $("input[name=articleWeight1]").val();
        var netWeight = $("input[name=articleWeight2]").val();
        var caratRate = $("input[name=articleCarrateRate]").val();

        var table = document.getElementById("tblArticle");
        var rowCount = table.rows.length - 1;
        $("#tblArticle tbody").append('<tr style="text-align:right" id="v_tr' + data + '"><td>' +
                rowCount + '</td><td>' + type + ' ' + identification + ' ' + description + ' ' + '</td>' +
                '<td>' + totalWeight + '</td>' + '<td>' + netWeight + '</td>' + '<td>' + caratRate + '</td>' +
                '<td>' + price + '</td><td><div onclick="editPropertyForm(2,' + data + ')" class="edit"></td><td></div><div class="delete" onclick="deleteArticle(' + data + ',1)" ></div></td></tr>');
        calculateTotalValues();
    }


    function deleteArticle(tr, status) {
        if (status === 0) { // update article
            $("#v_tr" + tr).remove();
            $("#pa" + tr).remove();
            $("#pap" + tr).remove();
            calculateTotalValues();
        } else {           // delete article
            $.ajax({
                url: "/AxaBankFinance/deleteLoanPropertyArticleDetails",
                type: 'GET',
                data: {"articleId": tr},
                success: function(data) {
                    console.log(data);
                }
            });
            $("#v_tr" + tr).remove();
            $("#pa" + tr).remove();
            $("#pap" + tr).remove();
            calculateTotalValues();
        }
    }

    function calculateTotalValues() {
        if ($("#totalRow").length) {
            $("#totalRow").remove();
        }
        var totalWeight = 0, netWeight = 0, carrotRate = 0, price = 0.00;

        $('#tblArticle tbody tr').each(function() {
            totalWeight = parseFloat(totalWeight) + parseFloat($(this).find("td").eq(2).text());
            netWeight = parseFloat(netWeight) + parseFloat($(this).find("td").eq(3).text());
            carrotRate = parseFloat(carrotRate) + parseFloat($(this).find("td").eq(4).text());
            price = parseFloat(price) + parseFloat($(this).find("td").eq(5).text());

        });

        $('#tblArticle tbody').append("<tr id='totalRow' style='font-weight:bold; text-align:right'><td colspan='2'>Total</td><td>" +
                totalWeight.toFixed(2) + "</td><td>" + netWeight.toFixed(2) + "</td><td>" + carrotRate.toFixed(2) + "</td><td>" + price.toFixed(2) + "</td><td></td></tr>");

    }

</script>











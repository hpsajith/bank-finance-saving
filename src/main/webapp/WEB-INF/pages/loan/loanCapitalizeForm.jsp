<%-- 
    Document   : loanCapitalizeForm
    Created on : Apr 22, 2016, 3:56:13 PM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="col-md-12 col-sm-12 col-lg-12" style="width: 100%">
    <div class="row" style="margin: 0 8 2 0"><legend><strong>Loan Capitalization</strong></legend></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12" id="divViewDeposit">
            <div class="row" style="overflow-y: auto;height: 85%">
                <table id="tblLoanCap" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="text-align: center">#</th>
                            <th style="text-align: center">Capitalize Type</th>
                            <th style="text-align: center">Capitalize Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="captilizeType" items="${captilizeTypes}">
                            <tr id="${captilizeType.id}">
                                <c:choose>
                                    <c:when test="${capitalizeDetailsSize == 0}">
                                        <td style="text-align: center"><input type="checkbox" id="check_${captilizeType.id}" onclick="check(${captilizeType.id})"/></td>
                                        <td style="text-align: center">${captilizeType.capitalizeType}</td>
                                        <td style="text-align: center"><input type="text" class="decimalNumber" id="txtCapAmount_${captilizeType.id}" value="0.00" disabled style="width: 100%"/></td>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="isCheck" value="false"></c:set>
                                            <c:set var="amount" value="0.00"></c:set>
                                            <c:forEach var="capitalizeDetail" items="${capitalizeDetails}">
                                                <c:if test="${capitalizeDetail.MCaptilizeType.id==captilizeType.id}">
                                                    <c:set var="isCheck" value="true"></c:set>
                                                    <c:set var="amount" value="${capitalizeDetail.amount}"></c:set>
                                                </c:if>
                                            </c:forEach>
                                            <c:choose>
                                                <c:when test="${isCheck}">
                                                <td style="text-align: center"><input type="checkbox" id="check_${captilizeType.id}" checked="true" onclick="check(${captilizeType.id})"/></td>
                                                <td style="text-align: center">${captilizeType.capitalizeType}</td>
                                                <td style="text-align: center"><input type="text" class="decimalNumber" id="txtCapAmount_${captilizeType.id}" value="${amount}" style="width: 100%"/></td>
                                                </c:when>
                                                <c:otherwise>
                                                <td style="text-align: center"><input type="checkbox" id="check_${captilizeType.id}" onclick="check(${captilizeType.id})"/></td>
                                                <td style="text-align: center">${captilizeType.capitalizeType}</td>
                                                <td style="text-align: center"><input type="text" class="decimalNumber" id="txtCapAmount_${captilizeType.id}" value="0.00" disabled style="width: 100%"/></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="dc_clear"></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <input type="button" value="Cancel" class="btn btn-default col-md-12" style="width: 100%" id="btnCancel" onclick="unloadUI(0)"/>
                </div>
                <div class="col-md-6">
                    <input type="button" value="Add" class="btn btn-default col-md-12" style="width: 100%" id="btnSave" onclick="saveLoanCapitalizeDetail()"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var totCapitalizeAmount = 0.00;
    var temp = [];
    var loanAmount = $("#loanAmountId2").val();
    var id = 1;
    
    function check(id) {
        var check = $("#check_" + id).is(':checked');
        if (check) {
            $("#txtCapAmount_" + id).removeAttr("disabled");
        } else {
            $("#txtCapAmount_" + id).val("0.00");
            $("#txtCapAmount_" + id).attr("disabled", true);
        }
    }

    function saveLoanCapitalizeDetail() {
        $("#tblLoanCap tbody tr").each(function() {
            var captilizeTypeId = $(this)[0].id;
            var tds = $(this).find("td");
            if (tds !== null) {
                if (tds[0].children[0].checked) {
                    var amount = tds[2].children[0].value;
                    if (amount !== "0.00") {
                        var mCaptilizeType = new Object();
                        mCaptilizeType.id = captilizeTypeId;

                        var loanCapitalizeDetail = new Object();
                        loanCapitalizeDetail.id = id;
                        loanCapitalizeDetail.MCaptilizeType = mCaptilizeType;
                        loanCapitalizeDetail.loanHeaderDetails = null;
                        loanCapitalizeDetail.amount = amount;

                        if (!contains(loanCapitalizeDetail, loanCapitalizeDetails)) {
                            totCapitalizeAmount = totCapitalizeAmount + parseFloat(amount);
                            loanCapitalizeDetails.push(loanCapitalizeDetail);
                            id++;
                            mCaptilizeType = null;
                            loanCapitalizeDetail = null;
                        }
                    }

                }
            }
        });
        $("#viewCapitalizeAmount").val(totCapitalizeAmount);
        console.log(loanCapitalizeDetails);
        unloadUI(1);
    }

    function loadLoanCapitalizeDetail() {
        temp = loanCapitalizeDetails;
        loanCapitalizeDetails = [];
        for (var i = 0; i < temp.length; i++) {
            $("#tblLoanCap tbody tr").each(function() {
                var captilizeTypeId = $(this)[0].id;
                if (temp[i].MCaptilizeType.id === captilizeTypeId) {
                    var tds = $(this).find("td");
                    if (tds !== null) {
                        tds[0].children[0].checked = true;
                        tds[2].children[0].value = temp[i].amount;
                        tds[2].children[0].disabled = false;
                    }
                }
            });
        }
        console.log(temp);
    }

    function contains(obj, list) {
        var found = list.some(function(el) {
            return el.id === obj.id;
        });
        return found;
    }

    function unloadUI(type) {
        if (type === 1) {
            loanAmount = totCapitalizeAmount + parseFloat(loanAmount);
            $("#loanAmountId").val(loanAmount);
            $("#viewLoanAmount").val(loanAmount);
        } else {
            console.log(temp);
            loanCapitalizeDetails = temp;
        }
        unblockui();
    }

</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $(".newTable").chromatable(
            {
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "15%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        }
        );
        $('#downPaymentId').val(0);
        $(".box").draggable();
    });

</script>

<style>
    #groupLoadBox {
        -webkit-box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        -moz-box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        box-shadow: 0px 0px 9px -2px rgba(79, 47, 237, 1);
        /*background: #BDBDBD;*/
        background: #BDBDBD;
    }

    #form_ {
        /*        -webkit-box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);
                -moz-box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);
                box-shadow: 0px 0px 9px -2px rgba(79,47,237,1);*/
    }
</style>

<div class="row divError"></div>
<div class="row divSuccess">${saveUpdate}</div>
<form name="loanForm" id="newLoanForm">
    <div class="row">
        <input type="hidden" value="${branchCode}" id="branchCodeId"/>
        <input type="hidden" value="${systemDate}" id="systemDateId"/>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-2" style="text-align: right"><strong>Rate Type</strong></div>
                <div class="col-md-2">
                    <select name="loanRateCode" id="txtRateType">
                        <!--disabled="true"-->
                        <c:forEach var="rateType" items="${rateTypeList}">
                            <c:choose>
                                <c:when test="${loan.loanRateCode == 'FR'}">
                                    <option value="FR" selected>${rateType.rateType}</option>
                                </c:when>
                                <c:when test="${loan.loanRateCode == 'RR'}">
                                    <option value="RR" selected>${rateType.rateType}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${rateType.rateCode}">${rateType.rateType}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-4" style="text-align: right"><strong>Loan Id</strong></div>
                <div class="col-md-4"><input type="text" style="color:#337ab7;" value="${loanId}" id="txtLoanId"
                                             name="loanId" onkeypress="return isNumber(event)"/></div>
                <input type="hidden" value="${loanId}" id="hLoanId"/>
                <div class="col-md-2"><input type="button" class="btn btn-default searchButton" onclick="searchLoan()"
                                             style="height: 25px; margin-left: 59px;"></div>
            </div>
        </div>
        <div class="col-md-1"></div>

    </div>
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;" id="form_">

        <div class="col-md-12 col-lg-12 col-sm-12">
            <input type="hidden" value="${edit}" id="testEdit"/>
            <c:choose>
                <c:when test="${edit}">
                    <div class="row"><label style="color: #08C; font-size: 14px">Edit Loan</label></div>
                </c:when>
                <c:otherwise>
                    <div class="row"><label style="color: #08C; font-size: 14px">Add New Loan</label></div>
                </c:otherwise>
            </c:choose>
            <div class="row">
                <div class="row" style="padding-left: 15px; padding-bottom: 10px">
                    <div class="col-md-1 col-lg-2 col-sm-1" style="font-weight: 600; color: black">Member Number</div>
                    <div class="col-md-1 col-lg-3 col-sm-2"><input type="text" name="loanBookNo" value="${loanBookNo}"
                                                                   required="true"  style="width: 100%;"
                                                                   id="loanBookNoTxt" class=""
                                                                   onchange="memberNumberPattern()" onblur="getMemberDetails()"/></div>
                    <!--<div class="col-md-3 col-lg-3 col-sm-3" style="color: black"><strong>Ex:</strong> <label style="color: blue"> AA/123/12/123</label></div>-->
                </div>
                <!--<hr>-->
                <div class="row" style="  padding-top: 10px; border-radius: 7px;" id="groupLoadBox">
                    <div class="row" style="margin-left: 12px; font-weight: bold">Customer Details</div>

                    <div class="  container-fluid">

                        <div class="row">
                            <div class="col-sm-2">
                                <div class="checkbox" id="checkGroup">
                                    <label>
                                        <input type="checkbox" value="" onchange='handleChange(this);'
                                               id="checkBoxLoanType" name="checkBoxLoanType">
                                        <span class="cr"><i class="cr-icon glyphicon"></i></span>
                                        <strong>Group Loan</strong>
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div id="groupComponent">
                            <div class="col-md-1" style="font-weight: bold">Branch</div>
                            <div class="col-md-3">
                                <select name="branchID" style="width: 172px" id="branchID">
                                    <option value="0">-select branch-</option>
                                    <c:forEach var="branch" items="${branchList}">
                                        <c:choose>
                                            <c:when test="${branch.branchId == group.branchID}">
                                                <option selected="selected"
                                                        value="${branch.branchId}">${branch.branchName}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${branch.branchId}">${branch.branchName}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>

                                <label id="msgBranchId" class="msgTextField"></label>
                            </div>

                            <div class="col-md-1" style="font-weight: bold">Center</div>
                            <div class="col-md-3">
                                <div id="loadingImg1"><img src="/AxaBankFinance/resources/img/loading_small.gif"
                                                           alt="loading..."></div>
                                <div id="centerDropDown">----</div>

                                <label id="msgCenterId" class="msgTextField"></label>
                            </div>

                            <div class="col-md-1" style="font-weight: bold">Group</div>
                            <div class="col-md-3">

                                <div id="loadingImg2"><img src="/AxaBankFinance/resources/img/loading_small.gif"
                                                           alt="loading..."></div>
                                <div id="groupDropDown">----</div>

                                <label id="msgCenterId" class="msgTextField"></label>
                            </div>
                        </div>
                    </div>
                    <div id="loadingImg3"><img src="/AxaBankFinance/resources/img/loading_small.gif" alt="loading...">
                    </div>

                    <div class="row">
                        <fieldset id="loanTypeGroup">

                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <div class="checkbox" id="">
                                    <label>
                                        <input type="checkbox" value="" onchange='setEqualAmount(this);'
                                               id="checkEqualAmount" name="checkEqualAmount">
                                        <span class="cr"><i class="cr-icon glyphicon"></i></span>
                                        <strong>Equal Amount</strong>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox" id="">
                                    <label>
                                        <input type="checkbox" value="" onchange='setEqualPeriod(this);'
                                               id="checkEqualPerod" name="checkEqualPerod">
                                        <span class="cr"><i class="cr-icon glyphicon"></i></span>
                                        <strong>Equal Period</strong>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox" id="">
                                    <label>
                                        <input type="checkbox" value="" onchange='setEqualRate(this);'
                                               id="checkEqualRate" name="checkEqualRate">
                                        <span class="cr"><i class="cr-icon glyphicon"></i></span>
                                        <strong>Equal Rate</strong>
                                    </label>
                                </div>
                            </div>

                        </fieldset>

                    </div>

                    <!--CUSTOMER TABLE-->
                    <div id="groupUserTableDiv"></div>
                    <br/>


                    <div class="col-md-10 col-lg-10 col-sm-10" id="singleComponent">

                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-sm-4"
                                         style="margin-right: -2px; font-weight: 600">NIC
                                    </div>
                                    <div class="col-md-7 col-lg-7 col-sm-7" style="margin-left:-4px; padding-left: 1;">
                                        <input type="text" style="width: 100%;" value="${debtor.debtorNic}" readonly="true"
                                               id="debtorNic"/></div>
                                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-7 col-sm-7">
                                <div class="row">
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Name</div>
                                    <div class="col-md-8 col-lg-8 col-sm-8" style="margin-right: -25px"><input
                                            type="text" style="width: 100%" id="debtorName" readonly="true"
                                            value="${debtor.nameWithInitial}"/></div>
                                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="button"
                                                                                   class="btn btn-default searchButton"
                                                                                   id="btnSearchCustomer"
                                                                                   style="height: 25px"></div>
                                    <!--                                <div class="col-md-1 col-lg-1 col-sm-1"></div>-->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 col-sm-3"
                                         style="margin-right: -10px; font-weight: 600">Address
                                    </div>
                                    <div class="col-md-9 col-lg-9 col-sm-9"><textarea name="" id="debtorAddress" readonly="true"
                                                                                      style="width: 104%">${debtor.debtorPersonalAddress}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 col-sm-3"></div>
                                    <div class="col-md-4 col-lg-4 col-sm-4"
                                         style="margin-right: -45px; font-weight: 600">Contact No
                                    </div>
                                    <div class="col-md-5 col-lg-5 col-sm-5" style=""><input type="text" readonly="true"
                                                                                            style="width: 102%"
                                                                                            id="debtorTelephone"
                                                                                            value="${debtor.debtorTelephoneMobile}"/>
                                    </div>
                                    <!--<div class="col-md-1 col-lg-1 col-sm-1"></div>-->
                                </div>
                            </div>
                            <input type="hidden" id="debID" name="debtorId" value="${debtor.debtorId}"/>
                            <input type="hidden" id="memberNumber" name="memberNumber" value="${debtor.memberNumber}"/>
                        </div>
                        <br/>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2" style="margin-left: -30px; margin-top: -20px;">
                        <div id="customerProfilePicture" height=" 120" width="120"></div>
                    </div>
                </div>
                <hr>
                <div class="row"> <label style="text-align: left;padding-left: 10%;font-size: 12px; color: red" id="msgNicExits" class="msgTextField"></label></div>
                <div class="col-md-3 col-lg-3 col-sm-3" style="margin-right: 10px;">
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="margin-right: -19px; font-weight: 600;">Loan Type</div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <select name="loanMainType" id="loanTypeList">
                                <option value="0">--SELECT--</option>
                                <c:forEach var="ltypes" items="${loanTypes}">
                                    <c:choose>
                                        <c:when test="${ltypes.loanTypeId==loan.loanMainType}">
                                            <option value="${ltypes.loanTypeId}"
                                                    selected>${ltypes.loanTypeName}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${ltypes.loanTypeId}">${ltypes.loanTypeName}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3"><input type="hidden" name="loanType"
                                                                       value="${loan.loanType}"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="margin-right: -19px; font-weight: 600">Rate (Annual)</div>
                        <div class="col-md-4 col-lg-4 col-sm-4"><input type="text" id="loanRateId" name="loanRate"
                                                                       value="${loan.loanRate}" style="width: 60px;"
                                                                       readonly/></div>
                        <div class="col-md-3 col-lg-3 col-sm-3 edit" style="margin-top:5px;"
                             onclick="editCharge(-1, 1)"></div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Amount</div>
                        <div class="col-md-8 col-lg-8 col-sm-8"><input type="text" name="loanAmount"
                                                                       value="${loan.loanAmount}" style="width: 100%"
                                                                       id="loanAmountId" class=""
                                                                       onkeypress="return checkNumbers(this)"/></div>
                        <div class="col-md-2 col-lg-2 col-sm-2 edit" title="Loan Capitalize" style="margin-top:5px;"
                             onclick="loadLoanCapitalizeForm()"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Down Payment</div>
                        <div class="col-md-3 col-lg-3 col-sm-3"><input id="downPaymentId" type="text"
                                                                       name="loanDownPayment"
                                                                       value="${loan.loanDownPayment}"
                                                                       style="width: 100%" id="" class=""
                                                                       onkeypress="return checkNumbers(this);"/></div>

                        <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Investment</div>
                        <div class="col-md-3 col-lg-3 col-sm-3"><input type="text" name="loanInvestment"
                                                                       value="${loan.loanInvestment}"
                                                                       style="width: 100%" id="" class="" readonly/>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3" style="font-weight: 600">Period</div>
                        <div class="col-md-6 col-lg-6 col-sm-6" style="margin-right: -25px;"><input type="text"
                                                                                                    id="loanPeriods"
                                                                                                    name="loanPeriod"
                                                                                                    value="${loan.loanPeriod}"
                                                                                                    style="width: 100%"/>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3">
                            <select name="periodType" id="cmbPeriodType">
                                <c:choose>
                                    <c:when test="${loan.periodType == 1}">
                                        <!--<option value="1" selected="true">Month</option>-->
                                        <option value="4">Week</option>
                                        <!--<option value="3">Dates</option>-->
                                    </c:when>
                                    <c:when test="${loan.periodType == 2}">
                                        <!--<option value="1">Month</option>-->
                                        <option value="4" selected="true">Week</option>
                                        <!--<option value="3">Dates</option>-->
                                    </c:when>
                                    <c:when test="${loan.periodType == 3}">
                                        <!--<option value="1">Month</option>-->
                                        <option value="4">Week</option>
                                        <!--<option value="3" selected="true">Dates</option>-->
                                    </c:when>
                                    <c:otherwise>
                                        <!--<option value="1">Month</option>-->
                                        <option value="4">Week</option>
                                        <!--<option value="3">Dates</option>-->
                                    </c:otherwise>
                                </c:choose>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">
                    <div class="row">
                        <div class="col-md-8"><label>Due Date</label></div>
                        <!--<input type="text" name="dueDay" id="dueDayy" value="${loan.dueDay}" style="margin-left: -20px; width: 65px;"  onkeypress="return isNumber(event)"/>-->
                        <div class="col-md-3"><input type="text" id="dueDayTxt" name="dueDay" class="txtCalendar "
                                                     value="${loan.dueDay}" style="margin-left: -20px; width: 75px;"/>
                        </div>
                        <div class="col-md-3"><input type="hidden" id="dueDateTxt" name="loanDueDate"
                                                     class="txtCalendar " style="margin-left: -20px; width: 75px;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row"
                 style="display: none; border:1px solid #00bb8c; border-radius: 1px;margin-bottom: 5px;margin-top: 5px">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-5" style="width: 26%"><label>Book No</label></div>
                        <div class="col-md-3"><input type="text" name="bookNo"/></div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <div class="col-md-3" style="margin-left: 10px">
                    <div class="row">
                        <div class="col-md-5"><label>Start Date</label></div>
                        <div class="col-md-7"><input type="text" class="txtCalendar" name="startDate"/></div>
                    </div>
                </div>
                <div class="col-md-3" style="margin-left: 72px">
                    <div class="row">
                        <div class="col-md-5"><label>Due Date</label></div>
                        <div class="col-md-7"><input type="text" class="txtCalendar" name="dueDate"/></div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>

            <input type="hidden" name="groupLoanID" id="groupLoanID">
            <input type="hidden" name="isGroupLoan" id="isGroupLoan">

            <!--<div class="row" style="background: #BDBDBD; padding-top: 10px; border-radius: 7px;">-->

            <div class="row"></div>
            <br/>

            <!--//gaurantor-->
            <div class="row" style="background: #F2F2F2; padding-top: 10px; padding-top: 10px; border-radius: 7px;"
                 id="guarantorDetailsDiv">
                <div class="row" style="margin-left: 12px; font-weight: bold">Guarantor Details</div>
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-sm-10">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-sm-4" style="margin-left:15px; font-weight: 600">
                                        NIC
                                    </div>
                                    <div class="col-md-7 col-lg-7 col-sm-7" style="margin-left:-12px; padding-left: 1;">
                                        <input type="text" style="width: 100%;" id="gurantorNic"/></div>
                                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-7 col-sm-7">
                                <div class="row">
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="font-weight: 600">Name</div>
                                    <div class="col-md-8 col-lg-8 col-sm-8" style="margin-right: -25px"><input
                                            type="text" style="width: 100%" id="guarantorName"/></div>
                                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="button"
                                                                                   class="btn btn-default searchButton"
                                                                                   id="btnLoadGuarantor"
                                                                                   style="height: 25px"></div>
                                    <!--<div class="col-md-1 col-lg-1 col-sm-1"></div>-->
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        <!--<div class="row" style="border:1px solid #BDBDBD; height: 12%; border-radius: 2px;">IMAGE</div>-->
                    </div>
                </div>
                <div class="row" style="overflow-y: auto; margin-left: 20px;">
                    <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 1">
                        <table class="dc_fixed_tables table-bordered newTable" width="100%" border="0" cellspacing="0"
                               cellpadding="0" id="guranterTable">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>NIC</th>
                                    <th>Address</th>
                                    <th>Telephone</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="gurantor" items="${gurantors}">
                                    <tr id="${gurantor.debtId}">
                                        <td>${gurantor.debtOrder}</td>
                                        <td>${gurantor.debtName}</td>
                                        <td>${gurantor.debtNic}</td>
                                        <td>${gurantor.debtAddress}</td>
                                        <td>${gurantor.debtTp}</td>
                                        <td>
                                            <div onclick="" class="edit"></div>
                                            <div class="delete" onclick="deleteRow(${gurantor.debtId})"></div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" id="guarantorIDs">
                    <c:forEach var="gurantor" items="${gurantors}">
                        <input type="hidden" id="g_hid${gurantor.debtId}" name="guarantorID"
                               value="${gurantor.debtId}"/>
                    </c:forEach>
                </div>
            </div>

            <div class="row" style="margin-left: 5px;">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-2 col-lg-2 col-sm-2" style="margin-right: -45px;padding-left: 1; font-weight: 600">Invoice Details</div>
                        <div class="col-md-6 col-lg-6 col-sm-6" style="margin-right: -12px;padding-left: 1"><input type="text" style="width: 100%"/></div>
                        <div class="col-md-1 col-lg-1 col-sm-1" style="padding-left: 1"><input type="button" id="btnAddProperty"
                                                                                               class="btn btn-default searchButton"
                                                                                               style="height: 25px"/>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3"></div>
                    </div>

                    <div class="row" style="overflow-y: auto">
                        <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 1">
                            <table id="tblProperty" class="dc_fixed_tables table-bordered newTable" width="100%"
                                   border="0" cellspacing="0" cellpadding="0">

                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Description</th>
                                        <th>Value</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="invoice" items="${invoise}">
                                        <tr id="v_tr${invoice.invoiceId}">
                                            <td>${invoice.invoiceId}</td>
                                            <td>${invoice.description}</td>
                                            <td>${invoice.invoiceValue}</td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${invoice.type==1}">
                                                        <div onclick="editPropertyForm(1,${invoice.invoiceId})"
                                                             class="edit"></div>
                                                        <div class="delete"
                                                             onclick="deleteVehicle(${invoice.invoiceId})"></div>
                                                    </c:when>
                                                    <c:when test="${invoice.type==2}">
                                                        <div onclick="editPropertyForm(2,${invoice.invoiceId})"
                                                             class="edit"></div>
                                                        <div class="delete"
                                                             onclick="deleteArticle(${invoice.invoiceId})"></div>
                                                    </c:when>
                                                    <c:when test="${invoice.type==3}">
                                                        <div onclick="editPropertyForm(3,${invoice.invoiceId})"
                                                             class="edit"></div>
                                                        <div class="delete"
                                                             onclick="deleteLand(${invoice.invoiceId})"></div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div onclick="editPropertyForm(4,${invoice.invoiceId})"
                                                             class="edit"></div>
                                                        <div class="delete"
                                                             onclick="deleteOther(${invoice.invoiceId})"></div>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row" id="hidProperty">
                        <c:forEach var="invoice" items="${invoise}">
                            <input type='hidden' id='ve${invoice.invoiceId}' name='proprtyType'
                                   value='${invoice.type}'/>
                            <input type='hidden' name='properyId' id='vep${invoice.invoiceId}'
                                   value='${invoice.invoiceId}'/>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="row" style="">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="row" style="width: 260px;">
                        <table id="otherChargesTable"
                               class="table table-striped table-bordered table-hover table-condensed table-edit">
                            <caption style="color: #000; font-weight: 600; font-size: 12px;">Add Other Charges</caption>
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="true">
                                        <c:forEach var="other" items="${otherChargers}">
                                            <tr id="${other.chargeID}">
                                                <c:choose>
                                                    <c:when test="${other.isPrecentage}">
                                                        <c:set var="isPres" value="1"></c:set>
                                                        <c:set var="precentg" value="${other.rate}%"></c:set>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:set var="isPres" value="0"></c:set>
                                                        <c:set var="precentg" value="${other.rate}"></c:set>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:choose>
                                                    <c:when test="${other.isAdded}">
                                                        <c:set var="isAdd" value="1"></c:set>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:set var="isAdd" value="0"></c:set>
                                                    </c:otherwise>
                                                </c:choose>
                                                <td>${other.description}
                                                    <input type='hidden' value='${isPres}' id='t${other.id}'/>
                                                    <input type='hidden' value='${other.chargeID}' id='cha${other.id}'/>
                                                    <input type='hidden' id='isadd${other.id}' value='${isAdd}'/>
                                                </td>
                                                <td><a onclick="editCharge(${other.id}, 1)"
                                                       id='aaa${other.id}'>${precentg}</a>
                                                    <input type='hidden' value='${other.rate}' id='c${other.id}'/>
                                                </td>
                                                <td>
                                                    <a onclick="editCharge(${other.id}, 2)" style="text-align: right;"
                                                       id='hidAmnt${other.id}'>${other.amount}</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        <tr>
                                            <td colspan="2" style="text-align: right"><label>Total</label></td>
                                            <td style="text-align: right"><label id='totalOther'>${total}</label></td>
                                        </tr>
                                    </c:when>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                    <div class="row" id="hidCharges">
                        <c:forEach var="other" items="${otherChargers}">
                            <input type="hidden" name="chragesId" value="${other.id}"/>
                            <input type="hidden" name="chragesRate" value="${other.rate}"/>
                            <input type="hidden" name="chragesAmount" value="${other.amount}"/>
                        </c:forEach>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4"
                     style=" margin-top: 34px; background: #F2F2F2; width: 300px; border-radius: 5px"
                     id="singleLoanSummaryDiv">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12"><label>Loan Details</label></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="">Loan Amount</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                       id="viewLoanAmount" value="${loan.loanAmount}"
                                                                       readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="">Capitalize</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                       id="viewCapitalizeAmount"
                                                                       value="${capitalizeAmount}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="">Investment</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                       id="viewInvestment"
                                                                       value="${loan.loanInvestment}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style="">Down Payment</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                       id="viewDownPayment"
                                                                       value="${loan.loanDownPayment}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Period</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input style="text-align:right" type="text"
                                                                       id="viewLoanPeriod" value="${loan.loanPeriod}"
                                                                       readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Interest Rate</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" style="text-align:right"
                                                                       id="viewRate" value="${loan.loanRate}" name=""
                                                                       readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Interest</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                       id="viewInterest" name="loanInterest"
                                                                       value="${loan.loanInterest}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Total Interest</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                       id="viewTotalInterest" name=""
                                                                       value="${totalInterest}" readonly/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">Installment</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" class="decimalNumber"
                                                                       id="viewInstallment" name="loanInstallment"
                                                                       value="${loan.loanInstallment}" readonly/></div>
                    </div>
                    <div class="row" style="border-top: 1px solid #222; padding-top: 3px; margin-top: 3px; ">
                        <div class="col-md-5 col-lg-5 col-sm-5">Total</div>
                        <div class="col-md-7 col-lg-7 col-sm-7"><input type="text" id="viewTotal" class="decimalNumber"
                                                                       disabled="true" value="${totalAmount}"></div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4" style="margin-top: 33px;width: 300px;">
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style=""><input placeholder="Marketing Officer"
                                                                                type="text" id="txtMarketingOfficer"
                                                                                value="${markOfficer}"><input
                                                                                type="hidden" name="marketingOfficer" id="marketingOfficerId"
                                                                                value="${loan.marketingOfficer}"/></div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnLoadMarketingOfficer"
                                                                       class="btn btn-default searchButton"
                                                                       style="height: 25px;margin-left: 11px;"/></div>
                        <div class="col-md-5 col-lg-5 col-sm-5"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5"><input placeholder="Approval 1" type="text"
                                                                       id="salesPersonName"
                                                                       value="${approve.app_name1}"/><input
                                                                       type="hidden" name="salesPerson" id="salesPersonId" value="${approve.app_id1}"/></div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnLoadSaleParsonName"
                                                                       class="btn btn-default searchButton"
                                                                       style="height: 25px;margin-left: 11px;"/></div>
                        <div class="col-md-5 col-lg-5 col-sm-5"><input type="text" name="comment1"
                                                                       value="${approve.comment1}" placeholder="Comment"
                                                                       id="app1_comment"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-2" style=""><input placeholder="Approval 2" type="text"
                                                                                id="recoveryPersonName"
                                                                                value="${approve.app_name2}"><input
                                                                                type="hidden" name="recoveryPerson" id="recoveryPersonId" value="${approve.app_id2}"/>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnRecoveryManager"
                                                                       class="btn btn-default searchButton"
                                                                       style="height: 25px;margin-left: 11px;"/></div>
                        <div class="col-md-5 col-lg-5 col-sm-5"><input type="text" name="comment2"
                                                                       value="${approve.comment2}" placeholder="Comment"
                                                                       id="app2_comment"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5" style=""><input placeholder="Approval 3" type="text"
                                                                                id="supplierName"
                                                                                value="${approve.app_name3}"><input
                                                                                type="hidden" name="ced" id="supplierId" value="${approve.app_id3}"/></div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="button" id="btnSupplier"
                                                                       class="btn btn-default searchButton"
                                                                       onclick="searchUserType(3)"
                                                                       style="height: 25px;margin-left: 11px;"/></div>
                        <div class="col-md-5 col-lg-5 col-sm-5"></div>
                    </div>
                </div>
            </div>
            <br/>
            <c:choose>
                <c:when test="${edit}">
                    <div class="row panel-footer" style="padding-top: 20px;">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                               value="Cancel"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                               onclick="uploadDocuments()"
                                                                               value="Upload Documents"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                               id="btnUpdate" onclick="saveLoanForm()"
                                                                               value="Update"/></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-8"><input type="button"
                                                                           class="btn btn-default col-md-12 col-lg-12 col-sm-12"
                                                                           id="btnProcess"
                                                                           onclick="processLoanToApproval()"
                                                                           value="Process" style="width: 100px"/></div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row panel-footer" style="padding-top: 10px;">
                        <div class="col-md-6 col-lg-6 col-sm-6"></div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-success col-md-12 col-lg-12 col-sm-12"
                                                                               style="border-radius: 5px;" id="btnSave"
                                                                               onclick="saveLoanForm()" value="Save"/>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-danger col-md-12 col-lg-12 col-sm-12"
                                                                               style="border-radius: 5px;"
                                                                               value="Cancel"/></div>
                                <div class="col-md-4 col-lg-4 col-sm-4"><input type="button"
                                                                               class="btn btn-warning col-md-12 col-lg-12 col-sm-12"
                                                                               style="border-radius: 5px;"
                                                                               value="Exit"/></div>
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </c:otherwise>
            </c:choose>

            <input type="hidden" id="gurant_debtr_type"/>
            <div class="row" id="hiddenContent"></div>
            <div class="row" id="divCapitalizeTypes"></div>
            <input type="hidden" value="${loan.loanAmount}" style="width: 100%" id="loanAmountId2"/>
            <input type="hidden" name="loanRescheduleId" value="${loan.loanRescheduleId}" style="width: 100%"/>
        </div>
    </div>
</form>
</div>

<!--Sub Loan Types-->
<div class="searchCustomer box" style="width: 35%; left:40%; top:20%" id="popup_subLoanTypes">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row" style="padding: 20px">
        <table id="tblsubtypes"
               class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header">
            <thead>
            <th>Loan Type</th>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<!--Search Customer-->
<div class="searchCustomer box"
     style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto; left: 15%"
     id="popup_searchCustomer">

</div>
<!--Add Other Property -->
<div class="searchCustomer" style="height: 70%; width: 70%; overflow-y: auto; top:10%; left:15%" id="popup_addProperty">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label>Add Other Property</label></div>
    <div class="row" style="padding: 10px;">
        <div class="col-md-4 col-lg-4 col-sm-4">Property Type</div>
        <div class="col-md-8 col-lg-8 col-sm-8">
            <select id="slctProperty" style="width: 90%">
                <option value="0">--SELECT--</option>
                <option value="1">Vehicle</option>
                <option value="3">Land</option>
                <option value="4">Other</option>
            </select>
        </div>
    </div>
    <div class="row" style="margin:15 0 0 0; height: 60%;width:100%" id="formPropertyContent">

    </div>
</div>
<!--Search supplier,sales Manager, recovery manager-->
<div class="searchCustomer" style="margin-top: 5%; margin-left: 15%;" id="popup_searchOfficers">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;"><label id="app_title"></label></div>
    <div class="row" style="margin: 10px;">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    Search By Name
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <input type="text" name="" class="col-md-12 col-lg-12 col-sm-12" style="padding: 1"/>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1"></div>
            </div>
        </div>
    </div>

    <div class="row col-md-10" style="margin:20px; height: 80%; overflow-y: auto">
        <table class="table table-bordered table-edit table-hover table-responsive" id="tblOfficers"
               style="font-size: 12px;">
            <thead>
                <tr>
                    <td>Emp No</td>
                    <td>Name</td>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4"></div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" onclick="addOfficerToLoanForm();"><span
                            class="glyphicon glyphicon-ok-circle"></span> Add
                    </button>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" id="btnSearchUseCancel" onclick="cancelOfficersTable();">
                        <span class="glyphicon glyphicon-remove-circle"></span> Cancel
                    </button>
                </div>
            </div>
        </div>
        <input type="hidden" id="hidOfficerType"/>
    </div>
</div>
<div id="message_newLonForm" class="searchCustomer box"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="docDivCheck" class="searchCustomer box" style="width: 85%;border: 0px solid #5e5e5e;margin-left:-15%"></div>
<div class="searchCustomer box" id="editRate_div"
     style="width: 40%; margin-left: 10%; margin-top: 10%; border:4px solid #1992d1"></div>
<input type="hidden" value="${message}" id="testMessage"/>
<div id="divLoanCapitalizeForm" class="searchCustomer"
     style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..."/>
</div>
<!--BlockUI-->


<script type="text/javascript">

    function sayHello(a) {
        alert("say hello " + $(this).val());
    }

    function getSameInstallment() {
//    var totalAmount = 
    }

    function getCustomInstallment() {

    }


    function setEqualAmount(cb) {

// validate the  loan amounts and the periods

        var loanAmount = $("#loanAmountId").val();

        var debtors = $(".debtorTable tr").length - 1;
        var singleValue = loanAmount / debtors;

        $(".userAmount").each(function () {
            $(this).val(singleValue);
        });

        if (cb.checked == true) {

            if (loanAmount == 0) {

                $("#checkEqualAmount").prop("checked", false);
                $("#loanAmountId").focus();

                $(".divError").html("");
                $(".divError").css({'display': 'block'});
                $(".divError").html("Enter loan amount");
                $("html, body").animate({scrollTop: 0}, "slow");
                $("#loanAmountId").addClass("txtError");
                return;
            } else {
                $(".divError").html("");
                $(".divError").css({'display': 'none'});
                $(".divError").html("");
                $("#loanAmountId").removeClass("txtError");
            }

            $(".userAmount").each(function () {
                $(this).prop("readonly", true);
            });

        } else {
            $(".userAmount").each(function () {
                $(this).prop("readonly", false);
                $(this).val("");

            });
        }

    }

    function setEqualPeriod(cb) {

// validate the  loan amounts and the periods

        var periods = $("#loanPeriods").val();

        $(".userPeriod").each(function () {
            $(this).val(periods);
        });


        if (cb.checked == true) {

            if (periods == 0) {
                $("#checkEqualPerod").prop("checked", false);
                $("#loanPeriods").focus();

                $(".divError").html("");
                $(".divError").css({'display': 'block'});
                $(".divError").html("Enter loan period");
                $("html, body").animate({scrollTop: 0}, "slow");
                $("#loanPeriods").addClass("txtError");
                return;
            } else {
                $(".divError").html("");
                $(".divError").css({'display': 'none'});
                $(".divError").html("");
                $("#loanPeriods").removeClass("txtError");
            }

            $(".userPeriod").each(function () {
                $(this).prop("readonly", true);
            });
        } else {
            $(".userPeriod").each(function () {
                $(this).prop("readonly", false);
                $(this).val("");
            });
        }
    }


    function setEqualRate(cb) {

// validate the  loan amounts and the periods

        var rate = $("#loanRateId").val();


        if (rate == 0) {
            $("#checkEqualRate").prop("checked", false);
            $("#loanRateId").focus();

            $(".divError").html("");
            $(".divError").css({'display': 'block'});
            $(".divError").html("Enter loan rate");
            $("html, body").animate({scrollTop: 0}, "slow");
            $("#loanRateId").addClass("txtError");
            return;
        } else {
            $(".divError").html("");
            $(".divError").css({'display': 'none'});
            $(".divError").html("");
            $("#loanRateId").removeClass("txtError");

            $(".userRate").each(function () {
                $(this).val(rate);
            });
            calGroupData();
        }


        if (cb.checked == true) {
            $(".userRate").each(function () {
                $(this).prop("readonly", true);
            });
        } else {
            $(".userRate").each(function () {
                $(this).prop("readonly", false);
                $(this).val("");
            });
        }
    }


    function handleChange(cb) {
        $("#groupLoanID").val("0");
        if (cb.checked == true) {
            $('#groupComponent').show("slow");
            $('#singleComponent').hide("slow");
            $("#branchID").prop("disabled", false);
            $("#centeID").prop("disabled", false);
            $("#groupID").prop("disabled", false);
            $("#isGroupLoan").val("1");
            $('#guarantorDetailsDiv').hide("2500");
            $('#singleLoanSummaryDiv').hide("2500");


        } else {
            $('#groupComponent').hide('slow');
            $('#singleComponent').show("slow");
            $("#branchID").prop("disabled", true);
            $("#centeID").prop("disabled", true);
            $("#groupID").prop("disabled", true);
            $("#isGroupLoan").val("0");
            $('#loanTypeGroup').hide("slow");
            $('#guarantorDetailsDiv').show("2500");
            $('#singleLoanSummaryDiv').sho("2500");
        }

        $("#branchID").prop("selectedIndex", 0);
        $("#centeID").hide();
        $("#groupID").hide();
        $("#groupUserTableDiv").html("");

    }


    function getGroupUsers() {
        $('#groupUserTableDiv').hide("slow");
        var groupID = $("#groupID").val();

        $("#groupLoanID").val(groupID);


        $.ajax({
            url: "/AxaBankFinance/loadGroupUsers/" + groupID,
            beforeSend: function (xhr) {
                $("#loadingImg3").show();
                $("#groupUserTableDiv").html("");

            },
            success: function (data) {

                $('#loanTypeGroup').show("slow");

                $("#groupUserTableDiv").html("");
                $("#groupUserTableDiv").html(data);
                $("#loadingImg3").hide();
                $('#groupUserTableDiv').show("slow");
            }
        });

    }


    var loanCapitalizeDetails = [];

    //Block Load SearchCustomer Form
    $(document).ready(function () {

        $("#loadingImg1").hide();
        $("#loadingImg2").hide();
        $('#groupComponent').hide();
        $('#groupUserTableDiv').hide();
        $("#loadingImg3").hide();
        $("#isGroupLoan").val("0");
        $("#groupLoanID").val("0");
        $('#loanTypeGroup').hide();

        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });

//        $('#txtRateType option:eq(FR)').prop('selected', true);
        $("#txtRateType").val("FR");


        $('#btnLoadGuarantor').click(function () {
            ui('#popup_searchCustomer');
            loadSearchCustomer(2);
        });
        $('#btnSearchCustomerExit').click(function () {
            unblockui();
        });

        //Block Laod GuarantorForm(Load SearchCustomer Form)    
        $('#btnSearchCustomer').click(function () {
            ui('#popup_searchCustomer');
            loadSearchCustomer(1);
        });
        $('#btnSearchCustomerExit').click(function () {
            unblockui();
        });

        //Block Laod Marketing Officer Form    
        $('#btnLoadMarketingOfficer').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign Marketing Officer");
            searchUserType(0);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });
        //Block Laod Marketing Manager Form    
        $('#btnLoadSaleParsonName').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 1st approval");
            searchUserType(1);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load Recovery Manager Form    
        $('#btnRecoveryManager').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 2nd approval");
            searchUserType(2);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load Supplier Form    
        $('#btnSupplier').click(function () {
            ui('#popup_searchOfficers');
            $("#app_title").text("Assign 3rd approval");
            searchUserType(3);
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });

        //Block Load property Form    
        $('#btnAddProperty').click(function () {
            ui('#popup_addProperty');
            loadPropertyForm();
        });
        $('#btnSearchUseCancel').click(function () {
            unblockui();
        });
    });


    $(function () {

        calculateLoan();

//        $("input[name=loanDownPayment]").val(0.00);
        //focus select loantype option
        $('select[name^="loanTypeList"]').eq(1).focus();

        //disabl enter key for submitting form
//        $(window).keydown(function(event) {
//            if (event.keyCode == 13) {
//                event.preventDefault();
//                return false;
//            }
//        });
        jQuery.extend(jQuery.expr[':'], {
            focusable: function (el, index, selector) {
                return $(el).is('a, button, :input, [tabindex]');
            }
        });

        //got to next input from enter key
        $(document).on('keypress', 'input,select,textarea', function (e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(':focusable');
                var index = $canfocus.index(this) + 1;
                if (index >= $canfocus.length)
                    index = 0;
                $canfocus.eq(index).focus();
            }
        });

        //press enter on button
        $('input[type="button"]').on("keypress", function (eve) {
            var key = eve.keyCode || e.which;
            if (key == 13) {
                $(this).click();
            }
            return false;
        });

        $('input[name=loanAmount]').keyup(function (e) {

            var loanAmount = $("#loanAmountId").val().trim();
            if (loanAmount !== "") {
                $("#loanAmountId").removeClass("txtError");
                $("#loanAmountId2").val(loanAmount);
                $("#viewInvestment").val(loanAmount);
            }

            $("#viewLoanAmount").autoNumeric();
            $("#viewLoanAmount").val(loanAmount);

            calculateCharge();
            calculateLoan();
        });

        //focus other input field other than loan period
        $("input[name=loanPeriod]").keyup(function () {
            var period = $(this).val();
            $("#viewLoanPeriod").val(period);
            calculateLoan();
        });

        //focus other input field other than loan downpayment
        $("input[name=loanDownPayment]").keyup(function () {
            calculateInvestment();
            calculateLoan();
        });


        $('input[name=loanId]').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (code === 13) {
                searchLoan();
            }
        });

        //format desimal numbers
        $('.decimalNumber').autoNumeric();

        //change loantype change loan rate
        $('#loanTypeList').change(function () {
            var selected = $(this).find('option:selected');
            var loanType = selected.val();
            if (loanType > 0) {
                $.getJSON('/AxaBankFinance/subLoanTypes/' + loanType, function (data) {
                    ui('#popup_subLoanTypes');
                    $("#tblsubtypes tbody").html("");
                    for (var i = 0; i < data.length; i++) {
                        $("#tblsubtypes tbody").append("<tr onclick='clickLoanSubtype(this)'><td>" + data[i].subLoanName +
                                "<input type='hidden' value='" + data[i].subLoanId + "'/></td><td style='display:none'><a onclick='editRate()'>" + data[i].subInterestRate + "</a></td></tr>");
                    }
                });
            }
        });
        //change oter property type 
        $('#slctProperty').change(function () {
            loadPropertyForm();
        });

        //change periodType
        $('#cmbPeriodType').change(function () {
            calculateLoan();
        });

    });

    //    click subloanType
    function clickLoanSubtype(tr) {
        var loanTypeId = $(tr).find("input").val();
        var rate = $(tr).find('a').text();
        $("#viewRate").val(rate);
        $("input[name=loanRate]").val(rate);
        $("input[name=loanType]").val(loanTypeId);
        calculateLoan();
        unblockui();
        $("#loanAmountId").focus();
        loadOtherCharges(loanTypeId);
        sameLoanValidationForCustomer(loanTypeId);
    }

    function sameLoanValidationForCustomer(loanTypeId) {
        var loanBookNo = $("#loanBookNoTxt").val();
        var debtorID = $("#debID").val();
        if (loanBookNo === "" || loanBookNo === null) {
//            alert("Member Number Cannot Be Empty.!");
            document.getElementById("btnSave").disabled = true;
        } else {
            $.ajax({
                url: '/AxaBankFinance/loanValidation/' + loanTypeId + "/" + debtorID,
                success: function (data) {
                    if (data > 0) {
                        $("#msgNicExits").html("This Customer has a On Going Loan.!");
                        $("#loanNic_id").addClass("txtError");
                        $("#loanNic_id").focus();
                        document.getElementById("btnSave").disabled = true;
                    } else {
                        $("#loanNic_id").removeClass("txtError");
                        $("#msgNicExits").html("");
                        $("#msgNicExits").removeClass("msgTextField");
                        $("#msgNicExits").html("");
                        document.getElementById("btnSave").disabled = false;
                    }
                }
            });
        }
    }

    //    Edit Rate
    function editRate() {
//        var loanRate = $(subLoan).val();
        alert("loanRate");
    }

    //load other charges types
    function loadOtherCharges(subId) {
        $.getJSON('/AxaBankFinance/otherChargesByLoanId/' + subId, function (data) {
            $('#otherChargesTable tbody').html("");

            for (var i = 0; i < data.length; i++) {
                var description = data[i].description;
                var isPresentage = data[i].isPrecentage;
                var charge, t, rateCharge;
                if (isPresentage) {
                    charge = data[i].taxRate;
                    rateCharge = charge + "%";
                    t = 1;
                } else {
                    charge = data[i].amount;
                    rateCharge = charge;
                    t = 0;
                }
                var id = data[i].id;
                var is_added = 0;
                var isAdded = data[i].isAdded;
                if (isAdded) {
                    is_added = 1;
                }
                $('#otherChargesTable tbody').append("<tr id='" + id + "'>" +
                        "<td>" + description + "<input type='hidden' value='" + t + "' id='t" + i + "'/><input type='hidden' value='" + id + "' id='cha" + i + "'/><input type='hidden' id='isadd" + i + "' value='" + is_added + "'/></td>" +
                        "<td><input type='hidden' value='" + charge + "' id='c" + i + "'/>" + "<a href='#' onclick='editCharge(" + i + ",1)' id='aaa" + i + "'>" + rateCharge + "</a></td>" +
                        "<td style='text-align:right' id='amount" + i + "'><a href='#' onclick='editCharge(" + i + ",2)' style='text-align: right;' id='hidAmnt" + i + "'></a></td></tr>");
            }
            $('#otherChargesTable tbody').append("<tr><td colspan='2'><label>Total</label></td><td class='decimalNumber'><label id='totalOther'><label></td>");
        });

    }


    //click loan other charges row
    var totalCharges = 0.00;
    var chargesAddedToLoan = 0.00;

    function calculateCharge() {
        totalCharges = 0.00;
        var i = 0;
        var loanAmount = $("input[name=loanAmount]").val();
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("last");
            } else {
                var labelId = "amount" + i;
                var amount;
                var chargeType = $("#t" + i).val();
                var charge = $("#c" + i).val();
                var isAdded = $("#isadd" + i).val();

                if (chargeType == 1) {
                    if (loanAmount != "") {
                        amount = ((charge * parseFloat(loanAmount)) / 100).toFixed(2);
                    } else {
                        amount = (parseFloat(0)).toFixed(2);
                    }
                } else {
                    amount = (parseFloat(charge)).toFixed(2);
                }
                if (isAdded == 1) {
                    chargesAddedToLoan = chargesAddedToLoan + amount;
                }

                if (isAdded == 0) {
                    totalCharges = totalCharges + parseFloat(amount);
                }

                $("#hidAmnt" + i).text(amount);
                $("#totalOther").text(totalCharges.toFixed(2));
            }
            i++;
        });
    }

    function loadSearchCustomer(type) {
        $("#gurant_debtr_type").val(type);
        $.ajax({
            url: '/AxaBankFinance/searchCustomer',
            success: function (data) {
                $('#popup_searchCustomer').html("");
                $('#popup_searchCustomer').html(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    function unloadSearchCustomer() {
        unblockui();
    }

    function saveLoanForm() {
        //check user is block
        var edit = '${edit}';

//        var dueDate = $("#dueDayTxt").val();
//
//        var splitDueDay = dueDate.split("-").pop(-1);
//        $("#dueDayy").val(splitDueDay);

        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function (data) {
                if (!data) {
                    var validate = validateForm();
                    SetOtherCharges();
                    calculateLoan();
                    setCapitalizeTypes();
                    if (validate) {
                        if (!edit) {
                            document.getElementById("btnSave").disabled = true;
                        }
                        ui("#proccessingPage");
                        $.ajax({
                            url: '/AxaBankFinance/saveNewLoanForm',
                            type: 'POST',
                            data: $("#newLoanForm").serialize(),
                            success: function (data) {
                                unblockui();
                                $('#formContent').html(data);
                                $(".divSuccess").css({'display': 'block'});
                                $("html, body").animate({scrollTop: 0}, "slow");
                            },
                            error: function () {
                                alert("Error Loading...");
                            }
                        });
                    } else {
                        $(".divError").html("");
                        $(".divError").css({'display': 'block'});
                        $(".divError").html(errorMessage);
                        errorMessage = "";
                        $("html, body").animate({scrollTop: 0}, "slow");
                    }
                } else {
                    warning("User Is Blocked", "message_newLonForm");
                }
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    function SetOtherCharges() {
        var i = 0;
        $("#hidCharges").html("");
        $('#otherChargesTable tbody tr').each(function (i) {
            if ($(this).is(":last-child")) {
                console.log("lastRO");
            } else {
                console.log("ROW" + i);
                var chargeId = $("#cha" + i).val();
                var charge = $("#c" + i).val();
                console.log(charge);
                var amount = $("#hidAmnt" + i).text();
                $("#hidCharges").append("<input type='hidden' name='chragesId' value='" + chargeId + "'/>" +
                        "<input type='hidden' name='chragesRate' value='" + charge + "'/>" +
                        "<input type='hidden' name='chragesAmount' value='" + amount + "'/>");
            }
            i++;
        });
    }

    $('#txtRateType').change(function () {
        calculateLoan();
    });


    function calculateLoan() {
//        var loanType = $(this).find('option:selected').val();
        var loanType = $("#loanTypeList").find('option:selected').val();
        var loanRateType = $("#txtRateType").find('option:selected').val();
        if (loanRateType === "FR") {
            if (loanType === 0) {
                return;
            }
            var downPayment = $("input[name=loanDownPayment]").val();
            $('#viewDownPayment').val(downPayment);
            var loanAmount = $("input[name=loanAmount]").val();
            if (loanAmount === "") {
                return;
            }


            var investment = 0.00;
            if (downPayment > 0) {
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
            } else {
                investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
            }
            $("input[name=loanInvestment]").val(investment);

            var amount = $("input[name=loanInvestment]").val();
            if (amount === null || amount === "")
                return;
            var period = $("#viewLoanPeriod").val();
            if (period === null || period === "")
                return;

            var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
            var periodType = $("#cmbPeriodType").find('option:selected').val();

            var newRate = $("#viewRate").val();
            var rate = newRate.replace('%', '');
            var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);
//            interest = interest / period;
            var totalInterest, totalAmount, installment, total;

            if (periodType === '3') {
                if (loanType === '10') {// For STLs
                    totalInterest = ((parseFloat(interest) * period)).toFixed(2);
                    totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                    installment = (parseFloat(totalAmount) / period).toFixed(2);
                    total = parseFloat(totalAmount).toFixed(2);

                } else {
                    totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
                    totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                    installment = (parseFloat(totalAmount) / period).toFixed(2);
                    total = parseFloat(totalAmount).toFixed(2);
                }
            } else {
                interest = interest / period;
                totalInterest = (parseFloat(interest) * period).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = (parseFloat(totalAmount) / period).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);

            }

            $("#viewInstallment").val(installment);
            $("#viewInterest").val(interest);
            $("#viewTotalInterest").val(totalInterest);
            $("#viewTotal").val(total);

            //reducing interest calculation
        } else {
            if (loanType == 0)
                return;
            var downPayment = $("input[name=loanDownPayment]").val();
            var loanAmount = $("input[name=loanAmount]").val();
            if (loanAmount == "")
                return;
            var investment = 0.00;
            if (downPayment > 0) {
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
            } else {
                investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
            }
            $("input[name=loanInvestment]").val(investment);

            var amount = $("input[name=loanInvestment]").val();
            if (amount == null || amount == "")
                return;
            var period = $("#viewLoanPeriod").val();
            if (period == null || period == "")
                return;

            var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
            var periodType = $("#cmbPeriodType").find('option:selected').val();

            var newRate = $("#viewRate").val();
            var rate = newRate.replace('%', '');
            rate = rate / 1200;
            var interest = (parseFloat(decimalAmount) * rate).toFixed(2);
            var totalInterest, totalAmount, installment, total;

            if (periodType === 3) {
                totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = decimalAmount * rate / (1 - (Math.pow(1 / (1 + rate), period / 30)));
                installment = parseFloat(installment).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);
            } else {
                totalInterest = (parseFloat(interest) * period).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = decimalAmount * rate / (1 - (Math.pow(1 / (1 + rate), period)));
                installment = parseFloat(installment).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);
            }
            $("#viewInstallment").val(installment);
            $("#viewInterest").val(interest);
            $("#viewTotalInterest").val(totalInterest);
            $("#viewTotal").val(total);

        }
    }


    function parseNumber(n) {
        var f = parseFloat(n); //Convert to float number.
        return isNaN(f) ? 0 : f; //treat invalid input as 0;
    }


    function calculateLoanForGroup(rowNo) {

//        var loanType = $(this).find('option:selected').val();
        var loanType = $("#loanTypeList").find('option:selected').val();
        var loanRateType = $("#txtRateType").find('option:selected').val();
        if (loanRateType === "FR") {
            if (loanType === 0) {
                return;
            }
            var downPayment = 0;
            var loanAmount = 0;
//             var period = $("#viewLoanPeriod").val();
            var period = 0;
            var newRate = 0;

//            var loanAmount = $("input[name=loanAmount]").val();

            var tbl = document.getElementById("tabela");
//            alert("before");
//            var tbl = $('#table').DataTable({});
//            alert(tbl);

            loanAmount = tbl.rows[rowNo].cells[4].children[0].value;
            amount = tbl.rows[rowNo].cells[4].children[0].value;
            period = tbl.rows[rowNo].cells[5].children[0].value;
            newRate = tbl.rows[rowNo].cells[6].children[0].value;
//            loanAmount = 5;
//            amount = 50000;
//            period = 12;
//            newRate = 500;

            if (loanAmount === "") {
                return;
            }

            var investment = 0.00;
            if (downPayment > 0) {
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
            } else {
                investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
            }
//            $("input[name=loanInvestment]").val(investment);

//            var amount = $("input[name=loanInvestment]").val();
            if (amount === null || amount === "")
                return;

            if (period === null || period === "")
                return;

            var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
            var periodType = $("#cmbPeriodType").find('option:selected').val();


            var rate = newRate.replace('%', '');
            var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);
//            interest = interest / period;
            var totalInterest, totalAmount, installment, total;

            if (periodType === '3') {
                if (loanType === '10') {// For STLs
                    totalInterest = ((parseFloat(interest) * period)).toFixed(2);
                    totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                    installment = (parseFloat(totalAmount) / period).toFixed(2);
                    total = parseFloat(totalAmount).toFixed(2);

                } else {
                    totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
                    totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                    installment = (parseFloat(totalAmount) / period).toFixed(2);
                    total = parseFloat(totalAmount).toFixed(2);
                }
            } else {
                interest = interest / period;
                totalInterest = (parseFloat(interest) * period).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = (parseFloat(totalAmount) / period).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);

            }

            // set interest
//                var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));

            tbl.rows[rowNo].cells[7].children[0].value = interest.toFixed(2);
            tbl.rows[rowNo].cells[8].children[0].value = totalInterest;
            tbl.rows[rowNo].cells[9].children[0].value = installment;
            tbl.rows[rowNo].cells[10].children[0].value = total;

0//            $("#viewInstallment").val(installment);
//            $("#viewInterest").val(interest);
//            $("#viewTotalInterest").val(totalInterest);
//            $("#viewTotal").val(total);

            //reducing interest calculation
        } else {
            if (loanType == 0)
                return;
            var downPayment = $("input[name=loanDownPayment]").val();
            var loanAmount = $("input[name=loanAmount]").val();
            if (loanAmount == "")
                return;
            var investment = 0.00;
            if (downPayment > 0) {
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
            } else {
                investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
            }
            $("input[name=loanInvestment]").val(investment);

            var amount = $("input[name=loanInvestment]").val();
            if (amount == null || amount == "")
                return;
            var period = $("#viewLoanPeriod").val();
            if (period == null || period == "")
                return;

            var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
            var periodType = $("#cmbPeriodType").find('option:selected').val();

            var newRate = $("#viewRate").val();
            var rate = newRate.replace('%', '');
            rate = rate / 1200;
            var interest = (parseFloat(decimalAmount) * rate);

//            var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);

            var totalInterest, totalAmount, installment, total;

            if (periodType === 3) {
                totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = decimalAmount * rate / (1 - (Math.pow(1 / (1 + rate), period / 30)));
                installment = parseFloat(installment).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);
            } else {
                totalInterest = (parseFloat(interest) * period).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = decimalAmount * rate / (1 - (Math.pow(1 / (1 + rate), period)));
                installment = parseFloat(installment).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);
            }

            var tbl = document.getElementById("tabela");

            tbl.rows[rowNo].cells[7].children[0].value = interest.toFixed(2);
            tbl.rows[rowNo].cells[8].children[0].value = totalInterest;
            tbl.rows[rowNo].cells[9].children[0].value = installment;
            tbl.rows[rowNo].cells[10].children[0].value = total;

            $("#viewInstallment").val(installment);
            $("#viewInterest").val(interest);
            $("#viewTotalInterest").val(totalInterest);
            $("#viewTotal").val(total);
        }
    }


    function calGroupData() {

        var loanType = $("#loanTypeList").find('option:selected').val();
        var table = document.getElementById("tabela");
        var rowCount = table.rows.length - 1;

        if (loanType == 0)
            return;
        var downPayment = 0;
        var loanAmount = $("input[name=loanAmount]").val();


        loanAmount = loanAmount / rowCount;


        if (loanAmount == "")
            return;
        var investment = 0.00;
        if (downPayment > 0) {
            investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);
        } else {
            investment = parseFloat(loanAmount) + parseFloat(chargesAddedToLoan);
        }
//        $("input[name=loanInvestment]").val(investment);

//        var amount = $("input[name=loanInvestment]").val();
        var amount = loanAmount;
        if (amount == null || amount == "")
            return;
        var period = $("#viewLoanPeriod").val();
        if (period == null || period == "")
            return;

//        var decimalAmount = Number(amount.replace(/[^0-9\.]+/g, ""));
        var decimalAmount = amount;
        var periodType = $("#cmbPeriodType").find('option:selected').val();


        var newRate = $("#viewRate").val();
        var rate = newRate.replace('%', '');
        var interest = ((parseFloat(decimalAmount) * rate) / 100).toFixed(2);

        var totalInterest, totalAmount, installment, total;


        if (periodType === '3') {
            if (loanType === '10') {// For STLs
                totalInterest = ((parseFloat(interest) * period)).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = (parseFloat(totalAmount) / period).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);

            } else {
                totalInterest = ((parseFloat(interest) * period) / 30).toFixed(2);
                totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
                installment = (parseFloat(totalAmount) / period).toFixed(2);
                total = parseFloat(totalAmount).toFixed(2);
            }
        } else {
            interest = interest / period;
            totalInterest = (parseFloat(interest) * period).toFixed(2);
            totalAmount = (parseFloat(decimalAmount) + parseFloat((totalInterest))).toFixed(2);
            installment = (parseFloat(totalAmount) / period).toFixed(2);
            total = parseFloat(totalAmount).toFixed(2);

        }

        var tbl = document.getElementById("tabela");

        var rowCount = $(".debtorTable tr").length;

        for (var i = 1; i < rowCount; i++) {
            tbl.rows[i].cells[7].children[0].value = interest;
            tbl.rows[i].cells[8].children[0].value = totalInterest;
            tbl.rows[i].cells[9].children[0].value = installment;
            tbl.rows[i].cells[10].children[0].value = total;
        }

        $("#viewInstallment").val(installment);
        $("#viewInterest").val(interest);
        $("#viewTotalInterest").val(totalInterest);
        $("#viewTotal").val(total);

    }


    function calculateInvestment() {
        var loanAmount = $("input[name=loanAmount]").val();
        var downPayment = $("input[name=loanDownPayment]").val();
        if (loanAmount !== "" && downPayment !== "") {
            var f_amount = parseFloat(loanAmount);
            var f_downpayment = parseFloat(downPayment);
            if (f_amount > f_downpayment) {
                var investment = 0.00;
                investment = (parseFloat(loanAmount) - parseFloat(downPayment)) + parseFloat(chargesAddedToLoan);

                $("input[name=loanInvestment]").val(investment);
                $("#viewDownPayment").val(downPayment);
                $("#viewInvestment").val(investment);
//                calculateCharge();
            } else {
                alert("Loan amount should be greater than down payment");
                $("input[name=loanDownPayment]").css("border", "1px solid red");
                $("input[name=loanDownPayment]").focus();
            }
        }
    }

    var errorMessage = "";

    function validateForm() {

        var groupLoan = 0;

        if ($('#checkBoxLoanType').is(":checked")) {
            groupLoan = 1;
        } else {
            groupLoan = 0;
        }

        var validate = true;
        var groupID = $("#groupName").find('option:selected').val();
        var branchID = $("#branchID").find('option:selected').val();
        var centerID = $("#centeID").find('option:selected').val();


        var loanBookNo = $("#loanBookNoTxt").val();

        if (loanBookNo === null || loanBookNo === '') {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Enter Member Number";
            $("#loanBookNoTxt").addClass("txtError");
        } else {
            $("#loanBookNoTxt").removeClass("txtError");
        }


//        var systemDate = $("#systemDateId").val();
//        var splitSystemDateId = systemDate.substring(8, 10);
//        $("#dueDayTxt").val(splitSystemDateId);


        var systemDate = $("#dueDayTxt").val();
        var splitSystemDateId = systemDate.substring(8, 10);
        $("#dueDayTxt").val(splitSystemDateId);
        $("#dueDateTxt").val(systemDate);

        var branchCodeId = $("#branchCodeId").val();
        var branchCode = $("#loanBookNoTxt").val();
        var splitBranchCode = branchCode.substring(0, 2);
        if (branchCodeId !== splitBranchCode) {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Invalid Member Number";
            $("#branchCodeId").addClass("txtError");
        } else {
//            $("#loanBookNoTxt").removeClass("txtError");
        }


        if (groupLoan == 1) {

            if (branchID == 0) {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Select Branch";
                $("#loanTypeList").addClass("txtError");
            } else {
                $("#loanTypeList").removeClass("txtError");
            }

            if (centerID == 0) {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Select Center";
                $("#loanTypeList").addClass("txtError");
            } else {
                $("#loanTypeList").removeClass("txtError");
            }

            if (groupID == 0) {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Select Group";
                $("#loanTypeList").addClass("txtError");
            } else {
                $("#loanTypeList").removeClass("txtError");
            }
        }

        var loanType = $("#loanTypeList").find('option:selected').val();
        if (loanType === 0) {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Select LoanType";
            $("#loanTypeList").addClass("txtError");
        } else {
            $("#loanTypeList").removeClass("txtError");
        }
        var loanAmount = $('input[name=loanAmount]').val();
        if (loanAmount == null || loanAmount == "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Enter Loan Amount";
            $("#loanAmountId").addClass("txtError");
        } else {
            $("#loanAmountId").removeClass("txtError");
        }

        var period = $('input[name=loanPeriod]').val();
        if (period == null || period == "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Enter Loan Time Period";
            $('input[name=loanPeriod]').addClass("txtError");
        } else {
            $('input[name=loanPeriod]').removeClass("txtError");
        }

        if (!$('#checkBoxLoanType').is(":checked")) {
            var debtorID = $("#debID").val();
            if (debtorID == null || debtorID == "") {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Select a Debtor";
                $("#debtorName").addClass("txtError");
            } else {
                $("#debtorName").removeClass("txtError");
            }
        }


//        var guarantord = $('input[name=guarantorID]').val();
//        if (guarantord == null) {
//            validate = false;
//            $(".divSuccess").css({'display': 'none'});
//            errorMessage = errorMessage + "<br>" + "Select atleast one Guarantor";
//            $("#guarantorName").addClass("txtError");
//        } else {
//            $("#guarantorName").removeClass("txtError");
//        }

        //validate approvelevels
        var app1 = $("#salesPersonName").val();
        var app2 = $("#recoveryPersonName").val();
        var app3 = $("#supplierName").val();
        if (app1 === "") {
            var comment1 = $("#app1_comment").val();
            if (comment1 === "") {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Add Comment If you are skipped first approval";
                $("#app1_comment").addClass("txtError");
            } else {
                $("#app1_comment").removeClass("txtError");
            }
        } else {
            $("#app1_comment").removeClass("txtError");
        }
        if (app2 === "") {
            var comment2 = $("#app2_comment").val();
            if (comment2 === "") {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Add Comment If you are skipped second approval";
                $("#app2_comment").addClass("txtError");
            } else {
                $("#app2_comment").removeClass("txtError");
            }
        } else {
            $("#app2_comment").removeClass("txtError");
        }
        if (app3 === "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "You Should add final approval";
            $("#supplierName").addClass("txtError");
        } else {
            $("#supplierName").removeClass("txtError");
        }

        var duedate = $("input[name=dueDay]").val();
        if (duedate == "") {
            validate = false;
            $(".divSuccess").css({'display': 'none'});
            errorMessage = errorMessage + "<br>" + "Please Enter Due Date";
            $("input[name=dueDay]").addClass("txtError");
        } else {
            if (duedate > 31 || duedate == 0) {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Please Enter Valid Date for Due Date";
                $("input[name=dueDay]").addClass("txtError");
            } else {
                $("input[name=dueDay]").removeClass("txtError");
            }
        }

        var loanId = $("input[name=loanId]").val();
        var hideLoan = $('#hLoanId').val();

        if (loanId !== "") {
            if (loanId != hideLoan) {
                validate = false;
                $(".divSuccess").css({'display': 'none'});
                errorMessage = errorMessage + "<br>" + "Can't Update this Loan" + "<br>" + "Invalide Loan Id";
            }
        }

//        if (duedate == "") {
//            validate = false;
//            errorMessage = errorMessage + "<br>" + "Please enter due date";
//            $("input[name=dueDay]").addClass("txtError");
//        } else {
//            if (duedate > 31 || duedate == 0) {
//                validate = false;
//                $(".divSuccess").css({'display': 'none'});
//                errorMessage = errorMessage + "<br>" + "Please enter valid date for due date";
//                $("input[name=dueDay]").addClass("txtError");
//            } else {
//                $("input[name=dueDay]").removeClass("txtError");
//            }
//        }

        return validate;
    }

    //add sales person,recovery manager,
    function searchUserType(type) {
        $("#hidOfficerType").val(type);
        var loanTypeId = $("input[name=loanType]").val();
        if (loanTypeId === "") {
            loanTypeId = 0;
        }
        $.ajax({
            url: '/AxaBankFinance/SearchOfficers/' + type + "/" + loanTypeId,
            success: function (data) {
                var tbody = $("#tblOfficers tbody");
                tbody.html("");
                for (var i = 0; i < data.length; i++) {
                    tbody.append("<tr onclick='clickOfficer(this)'><td>" + data[i].empId + "</td>" +
                            "<td>" + data[i].userName + "</td>" +
                            "<td style='display:none'>" + data[i].userId + "</td></tr>");
                }
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //click supplier row and add him to loan form
    var o_empNo = "", o_name = "", o_userId = "";

    function addOfficerToLoanForm() {
        var type = $("#hidOfficerType").val();
        if (o_userId == "") {
            alert("Please Select a name");
            return;
        } else {
            if (type == 0) {
                $("#marketingOfficerId").val(o_userId);
                $("#txtMarketingOfficer").val(o_name);
            } else if (type == 1) {
                $("#salesPersonId").val(o_userId);
                $("#salesPersonName").val(o_name);
            } else if (type == 2) {
                $("#recoveryPersonId").val(o_userId);
                $("#recoveryPersonName").val(o_name);
            } else if (type == 3) {
                $("#supplierId").val(o_userId);
                $("#supplierName").val(o_name);
            }
        }
        o_empNo = "", o_name = "", o_userId = "";
        unblockui();
    }

    //click supplier,recovery, sales offices table row
    function clickOfficer(tr) {
        var selected = $(tr).hasClass("highlight");
        $("#tblOfficers tbody tr").removeClass("highlight");
        if (!selected)
            $(tr).addClass("highlight");

        var tableRow = $(tr).children("td").map(function () {
            return $(this).text();
        }).get();
        o_empNo = $.trim(tableRow[0]);
        o_name = $.trim(tableRow[1]);
        o_userId = $.trim(tableRow[2]);
    }


    //close sales person, recovery officer, supplier pop up
    function cancelOfficersTable() {
//        o_id = "", o_name = "", o_address = "";
//        unblockui();
    }

    //other property
    function loadPropertyForm() {
        var select = $("#slctProperty").find('option:selected').val();
        $.ajax({
            url: '/AxaBankFinance/loadPropertForm/' + select,
            success: function (data) {
//                $("#slctProperty").css("display", "block");
                ui('#popup_addProperty');
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Save");
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //edit property
    function editPropertyForm(type, id) {
        $.ajax({
            url: '/AxaBankFinance/editPropertForm/' + type + '/' + id,
            success: function (data) {
                ui('#popup_addProperty');
                //$("#slctProperty").css("display", "none");
                $("#formPropertyContent").html("");
                $("#formPropertyContent").html(data);
                $(".saveUpadte").val("Update");
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    //search customer by nic
    $('#debtorNic').keyup(function (e) {

        var nicNo = $(this).val();
//        if (nicNo.length === 10) {
            $.ajax({
                dataType: 'json',
                url: '/AxaBankFinance/searchByNic_new/' + nicNo,
                success: function (data) {
                    var debId = data.debtorId;
                    if (debId > 0) {
                        var name = "", nic = "", address = "", telephone = "", memberNo = "";
                        if (data.nameWithInitial !== null)
                            name = data.nameWithInitial;
                        if (data.debtorNic !== null)
                            nic = data.debtorNic;
                        if (data.debtorPersonalAddress !== null)
                            address = data.debtorPersonalAddress;
                        if (data.debtorTelephoneMobile !== null)
                            telephone = data.debtorTelephoneMobile;
                        if (data.memberNumber !== null)
                            memberNo = data.memberNumber;

                        $("#debID").val(debId);
                        $("#debtorNic").val(nic);
                        $("#debtorName").val(name);
                        $("#debtorAddress").val(address);
                        $("#debtorTelephone").val(telephone);
                        $("#memberNumber").val(memberNo);

                        var url2 = "/AxaBankFinance/viewProfilePicture/" + debId;
                        $("#customerProfilePicture").html("<img src='" + url2 + "' ></img>");

                    } else {
                        alert("Search Result Not found");
                        $("#debtorNic").val("");
                        $("#debID").val("");
                        $("#debtorName").val("");
                        $("#debtorAddress").val("");
                        $("#debtorTelephone").val("");
                        $("#memberNumber").val("");
                    }

                },
                error: function () {
                    alert("Error Loading...");
                }
            });
//        }
    });

    //search gurantor by nic
    $('#gurantorNic').keyup(function (e) {
        var nicNo = $(this).val();
        console.log("enter+" + nicNo);
        if (nicNo.length === 10) {
            $.ajax({
                dataType: 'json',
                url: '/AxaBankFinance/searchByNic_new/' + nicNo,
                success: function (data) {
                    var debId = data.debtorId;
                    if (debId > 0) {
                        var name = "", nic = "", address = "", telephone = "", memberNo = "";
                        if (data.nameWithInitial !== null)
                            name = data.nameWithInitial;
                        if (data.debtorNic !== null)
                            nic = data.debtorNic;
                        if (data.debtorPersonalAddress !== null)
                            address = data.debtorPersonalAddress;
                        if (data.debtorTelephoneMobile !== null)
                            telephone = data.debtorTelephoneMobile;

                        if (data.memberNumber !== null)
                            memberNo = data.memberNumber;
                        $("#guarantorName").val(name);
                        $("#gurantorNic").val(nic);
                        var table = document.getElementById("guranterTable");
                        var rowCount = table.rows.length;
                        $('#guranterTable tbody').append('<tr id="' + debId + '"><td>' +
                                rowCount + '</td><td>' + name + '</td><td>' + nic + '</td><td>' +
                                address + '</td><td>' + telephone + '</td><td>' +
                                '<div onclick="" class="edit"></div><div class="delete" onclick="deleteRow(' + debId + ')" ></div>' + '</td></tr>');
                        $("#guarantorIDs").append("<input type='hidden' name='guarantorID' id='g_hid" + debId + "' value='" + debId + "'>");

                    } else {
                        alert("Search Result Not found");
                        $("#guarantorName").val("");
                        $("#gurantorNic").val("");
                    }

                },
                error: function () {
                    alert("Error Loading...");
                }
            });
        }
    });

    //delete gurantor
    function deleteRow(dId) {
        $('#' + dId).remove();
        $('#g_hid' + dId).remove();
        $("#guarantorName").val("");
    }

    //search loan by loan id
    function searchLoan() {

        var loanId = $("input[name=loanId]").val();
        var view = "message_newLonForm";
        if (loanId !== "") {
            $.ajax({
                url: '/AxaBankFinance/searchLoanDeatilsById/' + loanId,
                success: function (data) {
                    $('#formContent').html(data);
                    document.getElementById("txtLoanId").readOnly = true;
                    var msg = $("#testMessage").val();
                    if (msg != "") {
                        warning(msg, view);
                    }
                },
                error: function () {
                    alert("Error Loading...");
                }
            });

        } else {
            alert("Enter Loan Id");
        }

    }

    //proceed button
    function processLoanToApproval() {
        document.getElementById("btnProcess").disabled = true;
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loanDocumentSubmissionStatus/" + loan_id,
            success: function (data) {
                var process = data;
                var view = "message_newLonForm";
                if (process == 1) {
                    //cannot process
                    var msg = "You have to submit minimum documents to proceed";
                    warning(msg, view);
                } else if (process == 2) {
                    //process with pending dates
                    var msg = "Do you want to proceed loan without completing all documents";
                    dialog(msg, view, loan_id);
                } else {
                    //procees successfully
                    ui("#proccessingPage");
                    viewAllLoans(0);
                }
            },
            error: function () {

            }
        });
    }

    //document upload button
    function uploadDocuments() {
        var loan_id = $("input[name=loanId]").val();
        $.ajax({
            url: "/AxaBankFinance/loadDocumentChecking",
            data: {loan_id: loan_id},
            success: function (data) {
                $("#docDivCheck").html(data);
                ui('#docDivCheck');
            }
        });
    }

    //edit charge 
    function editCharge(row, type) {
        console.log(row);
        $.ajax({
            url: "/AxaBankFinance/loadEditRate/" + row + "/" + type,
            success: function (data) {
                $("#editRate_div").html(data);
                ui("#editRate_div");
            }
        });
    }

    function loadLoanCapitalizeForm() {
        var loanAmount = $("#loanAmountId").val().trim();
        var loanId = $("#hLoanId").val().trim();
        if (loanAmount !== "") {
            $("#loanAmountId").removeClass("txtError");
            if (loanId !== "") {
                editLoanCapitalizeForm(loanId);
            } else {
                addLoanCapitalizeForm();
            }
        } else {
            $("#loanAmountId").addClass("txtError");
            $("#loanAmountId").focus();
        }
    }

    // $("#loanAmountId").on("keyup", function () {
    //     var loanAmount = $("#loanAmountId").val().trim();
    //     if (loanAmount !== "") {
    //         $("#loanAmountId").removeClass("txtError");
    //         $("#loanAmountId2").val(loanAmount);
    //     }
    // });

    function setCapitalizeTypes() {
        if (loanCapitalizeDetails.length !== 0) {
            $('#divCapitalizeTypes').html("");
            for (var i = 0; i < loanCapitalizeDetails.length; i++) {
                $('#divCapitalizeTypes').append("<input type = 'hidden' name = 'capitalizeAmount' value = '" + loanCapitalizeDetails[i].amount + "'/>" +
                        "<input type = 'hidden' name = 'capitalizeId' value ='" + loanCapitalizeDetails[i].MCaptilizeType.id + "'/>");
            }
        }
    }

    function addLoanCapitalizeForm() {
        $.ajax({
            url: "/AxaBankFinance/loadLoanCapitalizeForm",
            success: function (data) {
                $("#divLoanCapitalizeForm").html("");
                $("#divLoanCapitalizeForm").html(data);
                if (loanCapitalizeDetails.length !== 0) {
                    loadLoanCapitalizeDetail();
                    ui("#divLoanCapitalizeForm");
                } else {
                    ui("#divLoanCapitalizeForm");
                }
            }
        });
    }

    function editLoanCapitalizeForm(loanId) {
        var loanAmount = "${loan.loanAmount}";
        loanAmount = parseFloat(loanAmount);
        var capitalizeAmount = "${capitalizeAmount}";
        capitalizeAmount = parseFloat(capitalizeAmount);
        loanAmount = loanAmount - capitalizeAmount;
        $("#loanAmountId2").val(loanAmount);
        $.ajax({
            url: "/AxaBankFinance/editLoanCapitalizeForm/" + loanId,
            success: function (data) {
                $("#divLoanCapitalizeForm").html("");
                $("#divLoanCapitalizeForm").html(data);
                if (loanCapitalizeDetails.length !== 0) {
                    loadLoanCapitalizeDetail();
                    ui("#divLoanCapitalizeForm");
                } else {
                    ui("#divLoanCapitalizeForm");
                }
            }
        });
    }


    $("#branchID").change(function () {

        var branchId = $("#branchID").val();
        $('#loanTypeGroup').hide("slow");
        $('#tabela').hide("slow");

        $.ajax({
            url: "/AxaBankFinance/loadCenter/" + branchId,
            beforeSend: function (xhr) {
                $("#loadingImg1").show();
                $("#centerDropDown").html("");
            },
            success: function (data) {
                $("#centerDropDown").html("");
                $("#centerDropDown").html(data);
                $("#loadingImg1").hide();
            }
        });

    });


    function getGroup() {
        var centeID = $("#centeID").val();
        $('#loanTypeGroup').hide("slow");
        $('#tabela').hide("slow");

        $.ajax({
            url: "/AxaBankFinance/loadGroup/" + centeID,
            beforeSend: function (xhr) {
                $("#loadingImg2").show();
                $("#groupDropDown").html("");
            },
            success: function (data) {
                $("#loadingImg2").hide();
                $("#groupDropDown").html("");
                $("#groupDropDown").html(data);
            }
        });
    }

    function memberNumberPattern() {
        $("#loanBookNoTxt").mask("a*/999/99/999");
    }

    function getMemberDetails() {
        var loanBookNo = $("#loanBookNoTxt").val();
        var changeLoanBookNo = loanBookNo.replace(/[^a-zA-Z0-9]/g, '-');

        if (loanBookNo === "" || loanBookNo === null) {
//            alert("Member Number Cannot Be Empty.!");
            document.getElementById("btnSave").disabled = true;
        } else {
            $.ajax({
                url: '/AxaBankFinance/debtorAdd/' + changeLoanBookNo,
                success: function (data) {
                    if (data !== null) {
                        var debtorId = "";
                        var debtorNic = "";
                        var debtorPersonalAddress = "";
                        var debtorTelephoneMobile = "";
                        var debtorName = "";
                        for (var p in data) {
                            var result = "";

                            if (data.hasOwnProperty(p)) {
                                result += p + " , " + data[p] + "\n";
                                if (p === 'debtorId') {
                                    debtorId = data[p];
                                }
                                if (p === 'debtorNic') {
                                    debtorNic = data[p];
                                }
                                if (p === 'debtorPersonalAddress') {
                                    debtorPersonalAddress = data[p];
                                }
                                if (p === 'debtorTelephoneMobile') {
                                    debtorTelephoneMobile = data[p];
                                }
                                if (p === 'debtorName') {
                                    debtorName = data[p];
                                }
                            }
                        }

                        $('#debID').val(debtorId);
                        $('#debtorNic').val(debtorNic);
                        $("#debtorName").val(debtorName);
                        $("#debtorAddress").val(debtorPersonalAddress);
                        $("#debtorTelephone").val(debtorTelephoneMobile);

                        document.getElementById("btnSave").disabled = false;
                    } else {
                        alert("Else");
                        $('#debID').val('');
                        $('#debtorNic').val('');
                        $("#debtorName").val('');
                        $("#debtorAddress").val('');
                        $("#debtorTelephone").val('');
                        document.getElementById("btnSave").disabled = true;
                    }
                }
            });
        }

    }
</script>
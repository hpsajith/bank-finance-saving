<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row" style="padding: 10px">
    <div class="row" style="margin: 10px; padding: 5px; border: 1px solid #08C; font-size: 12px;">
        <div class="row"><input type="hidden" id="hidTxtLoanId" value="${loanId}" /></div>
        <div class="col-md-12"><legend><strong>Document List</strong></legend></div>
        <div class="row" style="color: red; margin-bottom: 10px; text-align: left; font-weight: bold; margin-left: 30px;">${pendingMsg}</div>
        <div class="col-md-12" style="overflow-y: auto; height: 300px;"> 
            <table class="table table-bordered table-edit table-hover table-responsive dataTable">
                <thead>
                    <tr>
                        <th>Document</th>
                        <th>Name</th>
                        <th>Select File</th>
                        <th>Upload</th>
                        <th>View</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <c:forEach var="document" items="${checklist}">

                    <td style="text-align: left">
                        ${document.MCheckList.listDescription}
                        <c:set var="listId" value="${document.MCheckList.id}"></c:set>
                        <c:if test="${document.isCompulsory==1}">
                            <label style="color:red">*</label>
                        </c:if>
                    </td>
                    <c:forEach var="loanList" items="${loanChekList}">
                        <c:if test="${loanList.MCheckList.id==listId}">
                            <c:set var="isUpload" value="true"></c:set>
                            <c:set var="description" value="${loanList.decription}"></c:set>
                            <c:set var="id" value="${loanList.id}"></c:set>
                            <c:set var="docName" value="${document.MCheckList.listDescription}"></c:set>
                        </c:if>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${isUpload}">
                            <td><input type="text" name="FName${document.MCheckList.id}" value="${description}"/></td>
                            <td><input type="file" name="loader${document.MCheckList.id}" class="loader_1" /></td>
                            </c:when>
                            <c:otherwise>
                            <td><input type="text" name="FName${document.MCheckList.id}" /></td>
                            <td><input type="file" name="loader${document.MCheckList.id}" class="loader_1" /></td>
                            </c:otherwise>
                        </c:choose>
            </div>

            <td><input type="button" class="btn btn-default" style="margin-top: 4px;" id="upload${id}" value="Upload" onclick="processFileUpload(${document.MCheckList.id})"/></td>
                <c:choose>
                    <c:when test="${isUpload}">
                    <td><input type="button" class="btn btn-default" id="view_${id}" style="margin-top: 4px; width: 70px;" value="View" onclick="viewDocument(${id}, '${docName}')"/></td>
                    <td> <div id="fileComment${document.MCheckList.id}" class="fileComment success_file"></div></td>
                    </c:when>
                    <c:otherwise>
                    <td><label>Pending</label></td>
                    <td><div id="fileComment${document.MCheckList.id}" class="fileComment"></div></td>
                    </c:otherwise>
                </c:choose>

            </tr>
            <c:set var="isUpload" value="false"></c:set>
        </c:forEach>  
        </table>
    </div>

    <form method="GET" action="/AxaBankFinance/ReportController/generateDocumentReport" target="blank" id="formPDFDocument">
        <input type="hidden" name="docId"  id="txtDocId" value=""/>
        <input type="hidden" name="docName"  id="txtDocName" value=""/>
    </form>    
</div>
<div class="col-md-12 panel-footer">
    <div class="col-md-10">           
    </div>
    <div class="col-md-2">
        <div class="row">
            <button class="btn btn-default col-md-12" onclick="unloadDocumentList()"><span class="fa fa-times"></span> Exit</button>
        </div>
    </div>
</div>
</div>
<div class="searchCustomer" id="documentBox" style="width: 85%; margin-left: -15%">
    <div class="close_div" onclick="unloadDocumentView()"><label>X</label></div>
    <div style="clear:both"></div>
    <div class="row" id="document_title" style="margin-left: 15px; margin-right: 15px;"></div>
    <div class="row" id="document_content" style="margin-top: 15px; padding:5px; height: 400px; overflow-y: auto;">

    </div>
</div>
<div id="message_documentList" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script>

    $(function() {
        $(".loader_1").on('change', prepareLoad);
    });
    var files = null;
    function prepareLoad(event)
    {
        console.log(' event fired' + event.target.files[0].name);
        files = event.target.files;
    }
    function processFileUpload(id) {
        console.log("fileupload clicked");
        var oMyForm = new FormData();
        var loanId = $("#hidTxtLoanId").val();
        var fileName = $("input[name=FName" + id + "]").val();
        if (files == null) {
            alert("Select a File");
            return;
        }
        if (fileName == "") {
            alert("Enter File Name");
            return;
        }
        oMyForm.append("file", files[0]);
        oMyForm.append("name", fileName);
        oMyForm.append("loanId", loanId);
        oMyForm.append("listId", id);
        $.ajax({
            dataType: 'html',
            url: "${pageContext.request.contextPath}/uploadDocument?${_csrf.parameterName}=${_csrf.token}",
                        data: oMyForm,
                        type: "POST",
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            $("#message_documentList").html("");
                            $("#message_documentList").html(data);

                            files = null;
                            var result = $("#hid_pendingstatus").val();
//                            alert(result+"****");
                            if (result == 1) {
                                $("#fileComment" + id).addClass("success_file");
                            } else if (result == 0) {
                                $("#fileComment" + id).addClass("warning_file");
                            }
                            RefreshPage(loanId);
                        },
                        error: function() {
                            alert('Error:' + "File size exceeds the configured maximum file size");
                        }
                    });
                }

                //view documents
                function viewDocument(listId, docName) {
                    $("#txtDocId").val(listId);
                    $("#txtDocName").val(docName);
                    $("#formPDFDocument").submit();
                }

                function unloadDocumentList() {
                    var loanId = $("#hidTxtLoanId").val();
                    $.ajax({
                        url: "/AxaBankFinance/updateDocumentStatus/" + loanId,
                        success: function(data) {
                            console.log("submit success");
                        }
                    });
                    unblockui();
                }

                function unloadDocumentView() {
                    unblockui();
                    $("#document_title").html("");
                    $("#document_content").html("");
                    var loanId = $("#hidTxtLoanId").val();
                    RefreshPage(loanId);

                }

                function RefreshPage(loanId) {
                    $.ajax({
                        url: "/AxaBankFinance/loadDocumentChecking",
                        data: {loan_id: loanId},
                        success: function(data) {
                            $("#docDivCheck").html("");
                            $("#docDivCheck").html(data);
                            ui('#docDivCheck');
                        }
                    });
                }

</script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row">
    <div class="col-md-12">
        <form id="formOfficers">
        <div class="row" style="margin-top: 20px; padding: 2px 10px;">
            <input type="hidden" name="loan_id" value="${loanId}"/>
            <div class="col-md-4"><strong>Assign Officer</strong></div>
            <div class="col-md-8">
                <select id="slcOfficer" name="officer_id">
                    <c:forEach var="officer" items="${fieldOfficer}">
                        <option value="${officer.app_id1}">${officer.app_name1}</option>
                    </c:forEach>
                </select>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-6"></div>
            <div class="col-md-3"><input type="button" onclick="unblockui()"  class="btn btn-default col-md-12" value="Cancel"/></div>
            <div class="col-md-3"><input type="button" onclick="saveFieldOfficer()" class="btn btn-default col-md-12" value="Save"/></div>
        </div>
        </form>
    </div>
</div>
<div id="message_assignOfficer" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script>
    function saveFieldOfficer(){
        $.ajax({
            url: "/AxaBankFinance/saveFieldOfficer",
            data: $("#formOfficers").serialize(),
            type: "POST",
            success: function(data) {
                $("#message_assignOfficer").html(data);
                ui("#message_assignOfficer");
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }
        
</script>

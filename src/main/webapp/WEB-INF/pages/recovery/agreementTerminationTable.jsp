<%--
  Created by IntelliJ IDEA.
  User: Sahan Ekanayake
  Date: 10/3/2016
  Time: 3:10 PM
  To change this template use File | Settings | File Templates.
--%>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblAgreementTermination").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row" style="margin-top: 5px" id="agreeTermTable">
    <div class="row fixed-table">
        <div class="table-content">
            <c:choose>
                <c:when test="${message}">
                    <!--<table class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header" id="tblCustomer" style="margin-top: 10px;">-->
                    <table id="tblAgreementTermination" class="dc_table dc_fixed_tables table-bordered" width="100%"
                           border="0"
                           cellspacing="0" cellpadding="0">
                        <thead>
                        <tr>
                            <th style="text-align: center">Agreement No</th>
                            <th style="text-align: center">Vehicle No</th>
                            <th style="text-align: center">Customer</th>
                            <th style="text-align: center">Loan Amount</th>
                            <th style="text-align: center">Period</th>
                            <th style="text-align: center">Period Type</th>
                            <th style="text-align: center">Start Date</th>
                            <th style="text-align: center">Lap Date</th>
                            <th style="text-align: center">Terminate</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="terminate" items="${terminateList}">
                            <c:choose>
                                <c:when test="${terminate.loanSpec == 6}">
                                    <tr style="background-color: #F5A9A9">
                                        <td style="text-align: center">${terminate.loanAgreementNo}</td>
                                        <td style="text-align: center">${terminate.vehicleNo}</td>
                                        <td style="text-align: center">${terminate.debtorHeaderDetails.nameWithInitial}</td>
                                        <td style="text-align: right">${terminate.loanAmount}</td>
                                        <td style="text-align: center">${terminate.loanPeriod}</td>
                                        <c:choose>
                                            <c:when test="${terminate.loanPeriodType == 1}">
                                                <td style="text-align: center">Months</td>
                                            </c:when>
                                            <c:when test="${terminate.loanPeriodType == 2}">
                                                <td style="text-align: center">Days</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td style="text-align: center">Weeks</td>
                                            </c:otherwise>
                                        </c:choose>
                                        <td style="text-align: center">${terminate.loanStartDate}</td>
                                        <td style="text-align: center">${terminate.loanLapsDate}</td>
                                        <td style="text-align: center">
                                            <button type="button"
                                                    style="cursor: not-allowed"
                                                    class="btn btn-danger btn-sm"><span
                                                    class="glyphicon glyphicon-alert"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <td style="text-align: center">${terminate.loanAgreementNo}</td>
                                        <td style="text-align: center">${terminate.vehicleNo}</td>
                                        <td style="text-align: center">${terminate.debtorHeaderDetails.nameWithInitial}</td>
                                        <td style="text-align: right">${terminate.loanAmount}</td>
                                        <td style="text-align: center">${terminate.loanPeriod}</td>
                                        <c:choose>
                                            <c:when test="${terminate.loanPeriodType == 1}">
                                                <td style="text-align: center">Months</td>
                                            </c:when>
                                            <c:when test="${terminate.loanPeriodType == 2}">
                                                <td style="text-align: center">Days</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td style="text-align: center">Weeks</td>
                                            </c:otherwise>
                                        </c:choose>
                                        <td style="text-align: center">${terminate.loanStartDate}</td>
                                        <td style="text-align: center">${terminate.loanLapsDate}</td>
                                        <td style="text-align: center">
                                            <button type="button" data-toggle="modal"
                                                    onclick="setValueToConfirmBox(${terminate.loanId})"
                                                    data-target="#confirm-delete"
                                                    class="btn btn-danger btn-sm"><span
                                                    class="glyphicon glyphicon-remove"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="col-md-6" style="padding-top: 25px">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Terminate</label>

                                <div style="width:50px; height: 20px; background: #F5A9A9; border: solid"></div>
                                    <%--#00bb8c--%>
                            </div>
                            <div class="col-md-3">
                                <label>Not Terminate</label>

                                <div style="width:50px; height: 20px; background: #FFFFFF; border: solid"></div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <%--<c:otherwise><label class="delete">No Result Found!!!</label></c:otherwise>--%>
            </c:choose>
        </div>
    </div>
    <c:if test="${paymentList.size() > 0}">
        <div class="row" style="margin-top: 1%">
            <div class="col-md-3 ">
                <input type="button" id="correctButton" class="btn btn-success col-md-12 col-sm-12 col-lg-12"
                       value="Remove"/>
            </div>
        </div>
    </c:if>
</div>



<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <input type="hidden" id="loanId" value=""/>

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Terminate</h4>
            </div>

            <div class="modal-body">
                <p><h5><b>Are you Sure to Terminate this Agreement.?</b></h5></p>
                <p>Do you want to proceed?</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span> Cancel
                </button>
                <button type="button" onclick="terminateAgreement()" id="btnTerminate" class="btn btn-danger"
                        data-dismiss="modal"><span class="glyphicon glyphicon-trash"></span> Terminate
                </button>
            </div>
        </div>
    </div>
</div>
<%--<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>--%>

<script>
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
</script>
<script type="text/javascript">

    function terminateAgreement() {
        var loanNo = $("#loanId").val().trim();

        if (loanNo !== null) {
            $.ajax({
                url: "/AxaBankFinance/RecoveryController/agreementTerminateByLoanId",
                data: {"loanNo": loanNo},
                success: function (data) {
                    $("#agreeTermTableDiv").html("");
                    $("#agreeTermTableDiv").html(data);
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                }
            });
        }
    }

    function setValueToConfirmBox(value) {
        document.getElementById("loanId").value = value;
    }
</script>

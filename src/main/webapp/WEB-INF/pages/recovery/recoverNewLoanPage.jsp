<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#fieldOfficerTable").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "35%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });

        $("#fieldOfficerOldTable").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "40%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        $(".box").draggable();
    });
</script>
<div class="row"><label>New Loans</label></div>
<div class="row" style="overflow-y: auto">
    <table class="dc_fixed_tables table-bordered" id="fieldOfficerTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>Loan ID</th>
                <th>Loan Type</th>
                <th>Date</th>
                <th>Customer Name</th>
                <th>Guarantor 1</th>
                <th>Guarantor 2</th>
                <th>Guarantor 3</th>
                <th>Guarantor 4</th>
                <th>Status</th>

            </tr>
        </thead>
        <tbody>
            <c:forEach var="loans" items="${loanList}">
                <tr onclick="clickRow(${loans.loanId})">
                    <td>${loans.loanId}</td>
                    <td>${loans.loanType}</td>
                    <td>${loans.loanDate}</td>
                    <td>${loans.debtorName}<br/>${loans.debtorTelephone}</td>


                    <c:forEach var="gurantor" items="${loans.loanGuarantor}">
                        <td>${gurantor.nameWithInitial}<br/>${gurantor.debtorTelephoneMobile}</td>
                        </c:forEach>
                        <c:choose>
                            <c:when test="${loans.loanGuarantor.size()==1}">
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </c:when>
                        <c:when test="${loans.loanGuarantor.size()==2}">
                            <td>-</td>
                            <td>-</td>
                        </c:when>
                        <c:when test="${loans.loanGuarantor.size()==3}">
                            <td>-</td>
                        </c:when>
                    </c:choose>
                    <c:choose>
                        <c:when test="${loans.loanStatus==0}">
                            <td>Processing</td>
                        </c:when>
                        <c:when test="${loans.loanStatus==1}">
                            <td>Approved</td>
                        </c:when>
                        <c:otherwise>
                            <td>reject</td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<div class="row"><label>Recover Previous Loans</label></div> 
<div class="row">
    <table class="dc_fixed_tables table-bordered" id="fieldOfficerOldTable" width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>Agreement No</th>
                <th>Due Date</th>
                <th>Customer Name</th>
                <th>Customer TP</th>
                <th>Address</th>
                <th>Arrears</th>
                <th>Visit Type</th>
                <th>Remark</th>
                <th>Upload Document</th>

            </tr>
        </thead>
        <tbody>
            <c:forEach var="visit" items="${visitList}">
                <tr id="${visit.loanId}" onclick="loadRecoveryVisitForm(${visit.loanId},${visit.recoverId})">
                    <td>${visit.agreementNo}</td>
                    <td>${visit.dueDate}</td>
                    <td>${visit.debtorName}</td>
                    <td>${visit.telephone}</td>
                    <td>${visit.address}</td>
                    <td>${visit.arresAmount}</td>
                    <c:choose>
                        <c:when test="${visit.letteId==1}">
                            <td>1st Visit</td>
                        </c:when>
                        <c:when test="${visit.letteId==2}">
                            <td>2nd Visit</td>
                        </c:when>
                        <c:otherwise>
                            <td>3rd Visit</td>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${visit.comment1==null}">
                            <td></td>
                        </c:when>
                        <c:otherwise>
                            <td>${visit.comment1}</td>
                        </c:otherwise>
                    </c:choose>
                    <td>...</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<div class="row">
    <button id="btnCompletedList" class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="margin-top: 2%; width: 200px"><span class="glyphicon glyphicon-ok-circle"></span> View Completed List</button>
</div>

<div class="searchCustomer" style="margin-top: 5%; margin-left: 7%" id="popup_recoveryVisit">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;">
        <label>Recovery Visit Information</label>
        <div class="row" style="margin: 10px;">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        Comment
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <input type="text" id="txtComment" name="" class="col-md-12 col-lg-12 col-sm-12" style="padding: 1"/>
                        <label id="msgTxtComment" class="msgTextField"></label>
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        File Name
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <input type="text" id="txtFileName" name="" class="col-md-12 col-lg-12 col-sm-12" style="padding: 1"/>
                        <label id="msgTxtFileName" class="msgTextField"></label>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <input type="file" value="Browse." id="btnBrowse">
                        <label id="msgFile" class="msgTextField"></label>
                    </div>                    
                    <div class="col-md-1 col-lg-1 col-sm-1">

                    </div>
                </div> 
                <div class="row" >
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4"></div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <button class="btn btn-default col-md-12" id="btnSave" onclick="saveReport()"><span class="glyphicon glyphicon-ok-circle"></span> Save</button>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <button class="btn btn-default col-md-12" id="btnCancel" ><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="txtLoanId"/>
                    <input type="hidden" id="txtRecId"/>
                </div>                
            </div>
        </div>        
    </div>
</div>

<div id="message_Report" class="searchCustomer box" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<div class="searchCustomer" style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow: auto" id="popup_CompletedList">
    <div class="row" style="margin: 3px; background:#F2F7FC; text-align: left;">
        <label>Search by Submit Date</label>
        <div class="close_div" onclick="unloadPopUp()"><label>X</label></div>
        <div class="row" style="margin-top: 3%">
            <div class="col-md-4 col-sm-4 col-lg-4">
                <div class="col-md-2 col-sm-2 col-lg-2" style="width:100px">
                    Start Date
                </div>
                <div class="col-md-2 col-sm-2 col-lg-2" style="width:100px">
                    <span class="ui-helper-hidden-accessible"><input type="text"/></span>
                    <input type="text" name="sDate" id="sDate" class="txtCalendar" readonly/>
                    <label id="msgSDate" class="msgTextField"></label>
                </div>                
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <div class="col-md-2 col-sm-2 col-lg-2" style="width:100px">
                    End Date
                </div>
                <div class="col-md-2 col-sm-2 col-lg-2" style="width:100px">
                    <input type="text" name="eDate" id="eDate" class="txtCalendar" readonly/>
                    <label id="msgEDate" class="msgTextField"></label>
                </div>                 
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <button id="btnView" class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="width: 75px" onclick="getCompletedList()"><span> View</span></button>
            </div>
        </div>
        <div class="row" style="margin-top: 3%;height: 400px;overflow-y: auto; overflow-x: hidden">
            <table class="dc_fixed_tables table-bordered" id="completedDocumentTable" width="100%" border="0" cellspacing="0" cellpadding="0" >
                <thead>
                    <tr>
                        <th>Agreement No</th>
                        <th>Due Date</th>
                        <th>Submit Date</th>
                        <th>Visit Type</th>
                        <th>Remark</th>
                        <th>Upload Document</th>
                    </tr>
                </thead>   
                <tbody >

                </tbody>
            </table>
        </div>
    </div>    
</div>

<div class="searchCustomer" id="documentBox" style="width: 85%; margin-left: -15%">
    <div class="close_div" onclick="unloadDocumentView()"><label>X</label></div>
    <div style="clear:both"></div>
    <div class="row" id="document_title" style="margin-left: 15px; margin-right: 15px;"></div>
    <div class="row" id="document_content" style="margin-top: 15px; padding:5px; height: 400px; overflow-y: auto;"></div>
</div>
<div class="searchCustomer box" id="rec_report_div" style="width: 80%; margin-top: 2%; margin-left: -10%; height: 75%; overflow-y: auto;"></div>

<script type="text/javascript">

    function clickRow(loanId) {
        $.ajax({
            url: '/AxaBankFinance/viewRecoveryReport/' + loanId,
            success: function (data) {
                $('#rec_report_div').html(data);
                ui("#rec_report_div");
            },
            error: function () {
            }
        });
    }
    //Block Load RecoveryVisit Form    
    function loadRecoveryVisitForm(loanId, recId) {
        $("#txtLoanId").val(loanId);
        $("#txtRecId").val(recId);
        ui('#popup_recoveryVisit');
    }
    $('#btnCancel').click(function () {
        $("#txtLoanId").val("");
        $("#txtRecId").val("");
        $("#txtComment").val("");
        $("#txtComment").removeClass("txtError");
        $("#msgTxtComment").html("");
        $("#txtFileName").val("");
        $("#txtFileName").removeClass("txtError");
        $("#msgTxtFileName").html("");
        $("#msgFile").html("");
        unblockui();
    });

    var report = null;
    $(function () {
        $("#btnBrowse").on('change', prepareLoadReport);
    });
    function prepareLoadReport(event)
    {
        console.log(event.target.files[0].name);
        report = event.target.files;
    }
    //Save Report
    function saveReport() {
        if (fieldValidate()) {
            var formData = new FormData();
            formData.append("loanId", $("#txtLoanId").val());
            formData.append("recId", $("#txtRecId").val());
            formData.append("comment", $("#txtComment").val());
            formData.append("file", report[0]);
            $.ajax({
                dataType: 'html',
                url: "/AxaBankFinance/recoveryVisitReport?${_csrf.parameterName}=${_csrf.token}",
                data: formData,
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                success: function (data) {
                    $("#message_Report").html(data);
                    ui("#message_Report");
                    loadRecoverOfficerPage2();
                },
                error: function () {
                    alert("Error Loading...");
                }
            });
        }
    }
    function fieldValidate() {
        var comment = document.getElementById("txtComment").value;
        var fileName = document.getElementById("txtFileName").value;
        if (comment === null || comment === "") {
            $("#msgTxtComment").html("Enter Comment");
            $("#txtComment").addClass("txtError");
            $("#txtComment").focus();
            return false;
        } else {
            $("#txtComment").removeClass("txtError");
            $("#msgTxtComment").html("");
        }
        if (fileName === null || fileName === "") {
            $("#msgTxtFileName").html("Enter File Name");
            $("#txtFileName").addClass("txtError");
            $("#txtFileName").focus();
            return false;
        } else {
            $("#txtFileName").removeClass("txtError");
            $("#msgTxtFileName").html("");
        }
        if (report === null) {
            $("#msgFile").html("Upload File");
            return false;
        } else {
            $("#msgFile").html("");
        }
        return true;
    }

    $('#btnCompletedList').click(function () {
        $("#sDate").val("");
        $("#eDate").val("");
        $("#completedDocumentTable tbody").html("");
        ui('#popup_CompletedList');
    });
    $('#btnCompletedListCancel').click(function () {
        unblockui();
        $("#sDate").val('');
        $("#eDate").val('');
        $('#completedDocumentTable tbody').html("");
    });

    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });

    function getCompletedList() {
        if (dateValidation()) {
            var sDate = $("#sDate").val();
            var eDate = $("#eDate").val();
            $.ajax({
                url: '/AxaBankFinance/RecoveryController/getByDate',
                data: {"sDate": sDate, "eDate": eDate},
                success: function (data) {
                    $('#completedDocumentTable tbody').html("");
                    for (var i = 0; i < data.length; i++) {
                        var id = data[i].id;
                        var loanId = data[i].loanId;
                        var agreeNo = data[i].aggrNo;
                        var dueDate = data[i].ddate;
                        var submitDate = data[i].sdate;
                        var visitType = data[i].visitType;
                        var vType = "";
                        var remark = data[i].visitComment;
                        if (visitType === 1) {
                            vType = "First Visit";
                        } else if (visitType === 2) {
                            vType = "Second Visit";
                        } else {
                            vType = "Third Visit";
                        }
                        $('#completedDocumentTable tbody').append("<tr id='" + id + "'>" +
                                "<td>" + agreeNo + "</td>" +
                                "<td>" + dueDate + "</td>" +
                                "<td>" + submitDate + "</td>" +
                                "<td>" + vType + "</td>" +
                                "<td>" + remark + "</td>" +
                                "<td><button class='btn btn-default col-md-12 col-sm-12 col-lg-12' id='btnViewReport' style='margin-top: 2%; width:130px' onClick=viewReport(" + loanId + "," + id + ")><span class='glyphicon glyphicon-ok-circle'></span> View Report</button></td></tr>");
                    }
                },
                error: function () {
                    alert("Error Loading..");
                }
            });
        }
    }
    function dateValidation() {
        var sDate = $("#sDate").val();
        var eDate = $("#eDate").val();
        if (sDate === null || sDate === "") {
            $("#msgSDate").html("Start Date");
            $("#sDate").addClass("txtError");
            $("#sDate").focus();
            return false;
        } else {
            $("#sDate").removeClass("txtError");
            $("#msgSDate").html("");
        }
        if (eDate === null || eDate === "") {
            $("#msgEDate").html("End Date");
            $("#eDate").addClass("txtError");
            $("#eDate").focus();
            return false;
        } else {
            $("#eDate").removeClass("txtError");
            $("#msgEDate").html("");
        }
        return true;
    }

    function viewReport(loanId, recId) {
        var url = "/AxaBankFinance/viewRecoveryVisitReport?loanId=" + loanId + "&" + "recId=" + recId;
        $("#document_title").html("<legend style='text-align:left; font-weight:&" + "bold'>" + "Upload Report" + "</legend>");
        $("#document_content").html("<img src='" + url + "' ></img>");
        ui('#documentBox');
    }

    function unloadDocumentView() {
        unblockui();
        $("#document_title").html("");
        $("#document_content").html("");
        ui('#popup_CompletedList');
    }

    function unloadPopUp() {
        $("#sDate").val("");
        $("#eDate").val("");
        $("#sDate").removeClass("txtError");
        $("#msgSDate").html("");
        $("#eDate").removeClass("txtError");
        $("#msgEDate").html("");
        $("#completedDocumentTable tbody").html("");
        unblockui();
    }

</script>
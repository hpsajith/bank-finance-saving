<%--
  Created by IntelliJ IDEA.
  User: Sahan Ekanayake
  Date: 10/3/2016
  Time: 12:59 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        unblockui();
    });
</script>

<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption">
    <div class="row"><label style="margin-left: 20px;">Search Agreement Termination</label></div>

    <div class="row" style="margin-top: 5px">
        <div class="col-md-4">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8">
                <input type="text" value="${agreementNo}" style="width:100%" name="" id="txtAgreementNo"
                       onkeyup="searchByAgreementNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" name="" id="txtVehicleNo" onkeyup="searchByVehicleNo()"/>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" id="agreeTermTableDiv"></div>
<%--<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>--%>
<div id="divProcess" class="searchCustomer"
     style="width: 55%;height: 35%; margin: 5% 0%; border: 0px solid #5e5e5e;"></div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..."/>
</div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            url: '/AxaBankFinance/RecoveryController/loadAgreementTerminationTable',
            success: function (data) {
                $('#agreeTermTableDiv').html("");
                $('#agreeTermTableDiv').html(data);
            },
            error: function () {
            }
        });
    });

    function searchByAgreementNo() {
        var agreementNo = $("#txtAgreementNo").val().trim();
        if (agreementNo === null || agreementNo === "" || agreementNo === "0") {
        } else {
            if (agreementNo.length >= 8) {
                $.ajax({
                    url: "/AxaBankFinance/RecoveryController/loadAgreementTerminateByAgreementNo",
                    data: {"agreementNo": agreementNo},
                    success: function (data) {
                        $('#agreeTermTableDiv').html("");
                        $('#agreeTermTableDiv').html(data);
                        unblockui();
                    }
                });
            }
        }
    }

    function searchByVehicleNo() {
        var vehicleNo = $("#txtVehicleNo").val().trim();
        if (vehicleNo === null || vehicleNo === "") {
        } else {
            if (vehicleNo.length >= 6) {
                $.ajax({
                    url: "/AxaBankFinance/RecoveryController/loadAgreementTerminateByVehicleNo",
                    data: {"vehicleNo": vehicleNo},
                    success: function (data) {
                        $('#agreeTermTableDiv').html("");
                        $('#agreeTermTableDiv').html(data);
                        unblockui();
                    }
                });
            }
        }
    }

</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="row" style="text-align: left; margin: 0px 30px 5px 15px;padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
    <strong>Recovery Report for Loan No:${loanId}</strong></div>
<input type="hidden" value="${loanId}" id="hid_rec_loaniD"/>
<div class="row">
    <c:forEach var="cus" items="${customer}">
        <div class="col-md-11" style="margin-left: 30px; margin-bottom: 10px; gpadding: 5px; border: 1px solid #222; border-radius: 4px;">
            <div class="row" style="font-size: 14px; text-align: left; margin-left: 5px;">
                <label >${cus.title}</label>
            </div>
            <div class="row">
                <div class="col-md-3" style="text-align: left; margin-left: 10px;"><strong>Name :</strong>${cus.name}</div>             
                <div class="col-md-2" style="text-align: left"><strong>NIC :</strong>${cus.nic}</div>                      
                <div class="col-md-3" style="text-align: left"><strong>Mobile :</strong>${cus.mobile}</div>                    
                <div class="col-md-3" style="text-align: left"><strong>Address :</strong>${cus.address}</div>                    
            </div>
            <div class="row" style="margin: 10px; text-align: left;">            
                <c:choose>
                    <c:when test="${cus.loanList.size()>0}">
                        <p style="color: #777">Previous Loan Details</p>
                        <div class="col-md-11" style="margin: 20px; height: 90px; overflow-y: auto; ">

                            <table class="table table-bordered table-striped table-hover table-condensed table-edit ">
                                <thead>
                                    <tr>
                                        <th>Agreement No</th>
                                        <th>Loan Date</th>
                                        <th>Amount</th>
                                        <th>type</th>
                                        <th>period</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="loans" items="${cus.loanList}">
                                        <tr>
                                            <td>${loans.agreementNo}</td>
                                            <td>${loans.loanDate}</td>
                                            <td>${loans.loanAmount}</td>
                                            <td>${loans.loanType}</td>
                                            <td>${loans.loanPeriod}</td>
                                            <c:choose>
                                                <c:when test="${loans.loanSpec==0}">
                                                    <c:choose>
                                                        <c:when test="${loans.loanStatus==0}">
                                                            <td><label>Processing</label></td>
                                                        </c:when>
                                                        <c:when test="${loans.loanStatus==1}">
                                                            <td><label>On Going</label></td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td><label>Reject</label></td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${loans.loanSpec==1}">
                                                            <td><label>Laps</label></td>
                                                        </c:when>
                                                        <c:when test="${loans.loanSpec==2}">
                                                            <td><label>Legal</label></td>
                                                        </c:when>
                                                        <c:when test="${loans.loanSpec==3}">
                                                            <td><label>Seize</label></td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td><label>Full Paid</label></td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose> 
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <p style="color:#449d44">No Previous Loan Details</p>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row" style="margin: 10px; text-align: left">            
                <c:choose>
                    <c:when test="${cus.guarantorList.size()>0}">
                        <p style="color: #777">Previous Guarantor Details</p>
                        <div class="col-md-11" style="margin: 20px; height: 90px; overflow-y: auto; text-align: left">
                            <table class="table table-bordered table-striped table-hover table-condensed table-edit ">
                                <thead>
                                    <tr>
                                        <th>Agreement No</th>
                                        <th>Customer Name</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Period</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="guarantors" items="${cus.guarantorList}">
                                        <tr>
                                            <td>${guarantors.agreementNo}</td>
                                            <td>${guarantors.debtorName}</td>
                                            <td>${guarantors.loanType}</td>
                                            <td>${guarantors.loanAmount}</td>
                                            <td>${guarantors.loanPeriod}</td>
                                            <c:choose>
                                                <c:when test="${guarantors.loanSpec==0}">
                                                    <c:choose>
                                                        <c:when test="${guarantors.loanStatus==0}">
                                                            <td><label>Processing</label></td>
                                                        </c:when>
                                                        <c:when test="${guarantors.loanStatus==1}">
                                                            <td><label>On Going</label></td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td><label>Reject</label></td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${guarantors.loanSpec==1}">
                                                            <td><label>Laps</label></td>
                                                        </c:when>
                                                        <c:when test="${guarantors.loanSpec==2}">
                                                            <td><label>Legal</label></td>
                                                        </c:when>
                                                        <c:when test="${guarantors.loanSpec==3}">
                                                            <td><label>Seize</label></td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td><label>Full Paid</label></td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>                                            
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <p style="color:#449d44">No Previous Guarantor Details</p>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-2"><strong>Comment</strong></div>
                <div class="col-md-6" ><textarea name="comment" class="comment_text">${cus.comment}</textarea></div>
            </div>
        </div>
    </c:forEach>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-2"><input type="button" class="btn btn-default col-md-12" value="View" onclick="viewRecoveryReport1();"/></div>
            <div class="col-md-2"><input type="button" class="btn btn-default col-md-12" value="Cancel" onclick="unblockui()"/></div>
            <div class="col-md-2"><input type="button" class="btn btn-default col-md-12" value="Save" onclick="saveRecoveryReport()"/></div>
            <div class="col-md-1"></div>
        </div>
    </div>  
    <div class="hiddenRecForm">
        <form id="hidRecForm">
            <input type='hidden' name="loanId" value="${loanId}"/>
        </form>
    </div>
</div>
<form method="POST" action="/AxaBankFinance/ReportController/viewRecoverReport" target="blank" id="viewRecovery1">
    <input type="hidden" name="loan_id_r" value="${loanId}"/> 
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="message_recovery" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script>
    function saveRecoveryReport() {
        var comment = [];
        var i = 0;

//        var hiddenRecForm = "<form id='hidRecForm'>";
        $(".comment_text").each(function(i) {
            comment[i] = $(this).val();
            $("#hidRecForm").append("<input type='hidden' name='comment' value='" + comment[i] + "'/>");
            i++;
        });

        $.ajax({
            url: '/AxaBankFinance/saveRecoveryReport?${_csrf.parameterName}=${_csrf.token}',
            type: 'POST',
            data: $("#hidRecForm").serialize(),
            success: function(data) {
                $('#message_recovery').html(data);
                ui("#message_recovery");
            },
            error: function() {
                alert("Error Loading...");
            }
        });

    }
    function viewRecoveryReport1() {
        $("#viewRecovery1").submit();
    }
</script>  


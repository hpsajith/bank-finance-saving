<%-- 
    Document   : monthlySchedule
    Created on : Nov 26, 2015, 10:15:10 PM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style type="text/css">
    .disabled {
        pointer-events:none;
        cursor: default;
    }
</style>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="row" style="margin:5 5 5 5">
        <div class="col-md-1">
            <label class="lb"> Type </label>
        </div>
        <div class="col-md-2">
            <select id="loanTypeInput" style="width: 100%">
                <c:forEach var="loanType" items="${loanTypes}">
                    <option value="${loanType.loanTypeId}">${loanType.loanTypeName}</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-2">
            <input type="button" value="Target" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="getTargetAmount()"/>
        </div>
        <div class="col-md-6" style="margin-left: 4%">
            <label id="lblTarget" style="font-size: 18px; font-weight: bold"></label>
            <label id="lblReceivable" style="margin-left: 10%; font-size: 18px; font-weight: bold"></label>
        </div>
    </div>
    <div class="row" style="margin:5 5 5 5">
        <div class="col-md-1">
            <label class="lb"> Start </label>
        </div>
        <div class="col-md-3">
            <input type="text" id="sDate" class="dtdPck" style="width: 100%" readonly/>
            <label id="msgSDate" class="msgTextField"></label>
        </div>
        <div class="col-md-1">
            <label class="lb"> End </label>
        </div>
        <div class="col-md-3">
            <input type="text" id="eDate" class="dtdPck" style="width: 100%" readonly/>
            <label id="msgEDate" class="msgTextField"></label>
        </div>
        <div class="col-md-4">
            <input type="button" id="btnCreate" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Create Schedule" style="width: 60%" onclick="createSchedule()"/>
        </div>
    </div>

    <div id="scheduleTable"></div> 
</div>

<div id="message_save" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script type="text/javascript">
    $(".dtdPck").datepicker({
        changeMonth: false,
        changeYear: false,
        dateFormat: 'yy-mm-dd',
        duration: 'fast',
        stepMonths: 0
    });

    $(function() {
        $("#sDate").addClass('disabled');
        $("#eDate").addClass('disabled');
        $("#btnCreate").addClass('disabled');
        $("#tblSchedule").hide();
        $("#btnSave").hide();
    });

    var tgtAmount = 0;
    function getTargetAmount() {
        var loanType = $("#loanTypeInput").val();
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/getTargetAmountByRecOff',
            data: {"loanType": loanType},
            success: function(data) {
                if (data.length > 0) {
                    tgtAmount = data[0].toFixed(2);
                    $("#lblTarget").text("Expected : " + data[0].toFixed(2));
                    $("#lblTarget").css("color", "#1652BA");
                    $("#lblReceivable").text("Receivable : " + data[1].toFixed(2));    
                    $("#lblReceivable").css("color", "#965B42");
                    $("#sDate").removeClass('disabled');
                    $("#eDate").removeClass('disabled');
                    $("#btnCreate").removeClass('disabled');
                } else {
                    $("#lblTarget").text("There is no Assigned Target");
                    $("#lblTarget").css("color", "#D62222");
                }
            },
            error: function() {
                console.log("Error Loading..");
            }
        });
    }

    function createSchedule() {
        if (fieldValidate()) {
            var loanType = $("#loanTypeInput").val();
            var sDate = $("#sDate").val();
            var eDate = $("#eDate").val();
            $('#tblSchedule tbody').html("");
            $("#tblSchedule").show();
            $.ajax({
                url: '/AxaBankFinance/DayPlanController/getMonthSchedule',
                data: {"loanType": loanType, "sDate": sDate, "eDate": eDate},
                success: function(data) {
                    $("#scheduleTable").html("");
                    $("#scheduleTable").html(data);
                },
                error: function() {
                    console.log("Error Loading..");
                }
            });

        }
    }

    function saveSchedule() {
        var loanType = $("#loanTypeInput").val();
        var tgtDates = [];
        var tgtAmounts = [];
        var achAmounts = [];
        var diffAmounts = [];
        var trs = $('#tblSchedule tbody').find('tr');
        for (var i = 0; i < trs.length; i++) {
            var tr = trs[i];
            var tds = tr.children;
            tgtDates[i] = tds[0].textContent;
            tgtAmounts[i] = tds[1].children[0].value;
            achAmounts[i] = tds[2].children[0].value;
            diffAmounts[i] = tds[3].children[0].value;
        }
        document.getElementById("btnSave").disabled = true;
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/saveSchedule?${_csrf.parameterName}=${_csrf.token}',
            type: 'POST',
            data: {"loanType": loanType, "tgtDates": tgtDates, "tgtAmounts": tgtAmounts, "achAmounts": achAmounts, "diffAmounts": diffAmounts},
            success: function(data) {
                $('#message_save').html(data);
                ui("#message_save");
                document.getElementById("btnSave").disabled = false;
                createSchedule();
            },
            error: function() {
                console.log("Error Loading..");
            }
        });
    }

    function calculateDiff(id, event) {
        if (isNaN($("#txtTarget" + id).val().trim())) {
            $("#txtTarget" + id).val('');
            $("#txtTarget" + id).addClass("txtError");
            $("#txtTarget" + id).focus();
        } else {
            $("#txtTarget" + id).removeClass("txtError");
            if (event.keyCode === 13) {
                var tgt = $("#txtTarget" + id).val().trim();
                var clc = $("#txtCollection" + id).val().trim();
                var diff = clc - tgt;
                $("#txtDifference" + id).val(diff);
            }
        }
    }

    function active(dayPlanId, event) {
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/activeDayPlan',
            data: {"dayPlanId": dayPlanId},
            success: function(data) {
                $('#message_save').html(data);
                ui("#message_save");
                getTargetAmount();
                createSchedule();
            },
            error: function() {
                console.log("Error Loading..");
            }
        });
    }

    function inActive(id, dayPlanId, event) {
        var reason = $("#" + "txtReason" + id).val();
        if (reason !== null && reason !== "") {
            $("#" + "txtReason" + id).removeClass("txtError");
            $.ajax({
                url: '/AxaBankFinance/DayPlanController/inActiveDayPlan',
                data: {"dayPlanId": dayPlanId, "reason": reason},
                success: function(data) {
                    $('#message_save').html(data);
                    ui("#message_save");
                    getTargetAmount();
                    createSchedule();
                },
                error: function() {
                    console.log("Error Loading..");
                }
            });
        } else {
            $("#" + "txtReason" + id).addClass("txtError");
            $("#" + "txtReason" + id).focus();
        }
    }

    function fieldValidate() {
        var sDate = $("#sDate").val();
        var eDate = $("#eDate").val();
        if (sDate === null || sDate === "") {
            $("#msgSDate").html("Select Start Date");
            $("#sDate").addClass("txtError");
            $("#sDate").focus();
            return false;
        } else {
            $("#msgSDate").html("");
            $("#sDate").removeClass("txtError");
        }
        if (eDate === null || eDate === "") {
            $("#msgEDate").html("Select End Date");
            $("#eDate").addClass("txtError");
            $("#eDate").focus();
            return false;
        } else {
            $("#msgEDate").html("");
            $("#eDate").removeClass("txtError");
        }
        return true;
    }

//    function changeAmount(txtInput) {
//        var typedAmount = $("#" + txtInput + "txtTarget").val();
//        console.log(typedAmount);
//        if (typedAmount <= tgtAmount) {
//            var bal = tgtAmount - typedAmount;
//            tgtAmount = bal;
//            $("#lblTarget").text("TGT : " + bal.toFixed(2));
//        } else {
//            alert("Please enter less amount");
//        }
 //   }
</script>

<%-- 
    Document   : targetMonthCEDForm
    Created on : Nov 16, 2015, 4:16:49 PM
    Author     : ites
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#viewLoanType").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "50%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });
</script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="row" style="margin:5 5 5 5">
        <div class="col-md-1">
            <label class="lb"> Month </label>
        </div>
        <div class="col-md-3">
            <input type="text" name="month" id="cmdMonth" value="" class="date-picker-year" style="width: 80%" readonly/>
            <label id="msgCmdMonth" class="msgTextField"></label>
        </div>
        <div class="col-md-1">
            <label class="lb"> Branch </label>
        </div>
        <div class="col-md-3">
            <select id="branchInput" style="width: 100%">
                <c:if test="${branches.size()>0}">
                    <c:forEach var="branch" items="${branches}">
                        <c:choose>
                            <c:when test="${branch.branchId==loggedBranch}">
                                <option value="${branch.branchId}" selected>${branch.branchName}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${branch.branchId}">${branch.branchName}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:if>
            </select>
        </div>        
    </div>
    <div class="row" style="overflow-x: auto">
        <table id="viewLoanType"  class="dc_fixed_tables table-bordered" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: center">Loan</th>
                    <th style="text-align: center">Process</th>
                    <th style="text-align: center">Due Amount</th>
                    <th style="text-align: center">Arrears Amount</th>
                    <th style="text-align: center">D/Rate(%)</th>
                    <th style="text-align: center">A/Rate(%)</th>
                    <th style="text-align: center">Create</th>
                    <th style="text-align: center">Value</th>
                    <th style="text-align: center">Manager</th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${targetAssingCeds.size() > 0}">
                        <c:set var="check"  value="false"></c:set>
                        <c:set var="targetCED" value=""></c:set>
                        <c:forEach var="mLoanType" items="${mLoanTypes}">
                            <c:forEach var="target" items="${targetAssingCeds}">
                                <c:if test="${mLoanType.loanTypeId==target.loanType}">
                                    <c:set var="check"  value="true"></c:set>
                                    <c:set var="targetCED" value="${target}"></c:set>
                                </c:if>             
                            </c:forEach>
                            <c:choose>
                                <c:when test="${check}">
                                    <tr id='${mLoanType.loanTypeId}'>
                                        <td>${mLoanType.loanTypeName}</td>
                                        <td><input type="button" id="btnProcess${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Process" onclick="getDayPlanInfo(${mLoanType.loanTypeId})"/></td>
                                        <td><input type="text" id="txtDueAmount${mLoanType.loanTypeId}" value="${targetCED.totalDueAmount}" readonly style="text-align: right"/></td>
                                        <td><input type="text" id="txtArrsAmount${mLoanType.loanTypeId}" value="${targetCED.totalArriesAmount}" readonly style="text-align: right"/></td>
                                        <td><input type="text" id="txtDueRate${mLoanType.loanTypeId}" value="${targetCED.targetDueRate}" style="text-align: right" size="5" onkeyup="checkNumbers(this)"/></td>
                                        <td><input type="text" id="txtArrsRate${mLoanType.loanTypeId}" value="${targetCED.targetArriesRate}" style="text-align: right" size="5" onkeyup="checkNumbers(this)"/></td>
                                        <td><input type="button" id="btnCteateTarget${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Target" onClick="calculateTarget(${mLoanType.loanTypeId})"/></td>
                                        <td><input type='text' id="txtTarget${mLoanType.loanTypeId}" readonly value="${targetCED.monthTargetAmount}" style="text-align: right"/></td>
                                        <td><input type='button' id="btnAssign${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Assign" onClick="getOfficers(${mLoanType.loanTypeId})"/></td>                                            
                                    </tr>
                                    <c:set var="check"  value="false"></c:set>
                                </c:when>
                                <c:otherwise>
                                    <tr id="${mLoanType.loanTypeId}">
                                        <td>${mLoanType.loanTypeName}</td>
                                        <td><input type="button" id="btnProcess${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Process" onclick="getDayPlanInfo(${mLoanType.loanTypeId})"/></td>
                                        <td><input type="text" id="txtDueAmount${mLoanType.loanTypeId}" value="0.00" readonly  disabled style="text-align: right"/></td>
                                        <td><input type="text" id="txtArrsAmount${mLoanType.loanTypeId}" value="0.00" readonly  disabled style="text-align: right"/></td>
                                        <td><input type="text" id="txtDueRate${mLoanType.loanTypeId}" value="0" disabled style="text-align: right" size="5" onkeyup="checkNumbers(this)"/></td>
                                        <td><input type="text" id="txtArrsRate${mLoanType.loanTypeId}" value="0" disabled style="text-align: right" size="5" onkeyup="checkNumbers(this)"/></td>
                                        <td><input type="button" id="btnCteateTarget${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Target"  disabled onClick="calculateTarget(${mLoanType.loanTypeId})"/></td>
                                        <td><input type='text' id="txtTarget${mLoanType.loanTypeId}" value="0.00" readonly  disabled style="text-align: right"/></td>
                                        <td><input type='button' id="btnAssign${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Assign" disabled onClick="getOfficers(${mLoanType.loanTypeId})"/></td>                                            
                                    </tr>                                       
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="mLoanType" items="${mLoanTypes}">
                            <tr id='${mLoanType.loanTypeId}'>
                                <td>${mLoanType.loanTypeName}</td>
                                <td><input type="button" id="btnProcess${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Process" onclick="getDayPlanInfo(${mLoanType.loanTypeId})"/></td>
                                <td><input type="text" id="txtDueAmount${mLoanType.loanTypeId}" readonly  disabled style="text-align: right"/></td>
                                <td><input type="text" id="txtArrsAmount${mLoanType.loanTypeId}" readonly  disabled style="text-align: right"/></td>
                                <td><input type="text" id="txtDueRate${mLoanType.loanTypeId}" disabled style="text-align: right" size="5" onkeyup="checkNumbers(this)"/></td>
                                <td><input type="text" id="txtArrsRate${mLoanType.loanTypeId}" disabled style="text-align: right" size="5" onkeyup="checkNumbers(this)"/></td>
                                <td><input type="button" id="btnCteateTarget${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Target"  disabled onClick="calculateTarget(${mLoanType.loanTypeId})"/></td>
                                <td><input type='text' id="txtTarget${mLoanType.loanTypeId}" readonly  disabled style="text-align: right"/></td>
                                <td><input type='button' id="btnAssign${mLoanType.loanTypeId}" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Assign" disabled onClick="getOfficers(${mLoanType.loanTypeId})"/></td>
                            </tr>                            
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
        <div class="dc_clear"></div>
    </div>
</div>
<div id="popup_searchOfficers" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;">
</div>
<script type="text/javascript">

    $('#cmdMonth').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            $("#msgCmdMonth").html("");
            $("#cmdMonth").removeClass("txtError");
        }
    });

    $(function() {
        var currDate = new Date();
        var month = Number(currDate.getMonth()) + 1;
        var tgtDate = currDate.getFullYear() + '-0' + month + '-' + '01';
        $('#cmdMonth').val(tgtDate);
    });

    function getDayPlanInfo(loanType) {
        var month = $("#cmdMonth").val();
        if (month !== null && month !== "") {
            document.getElementById('btnProcess' + loanType).disabled = true;
            document.getElementById('btnProcess' + loanType).value = 'Processing..';
            document.getElementById('btnCteateTarget' + loanType).disabled = true;
            document.getElementById('btnAssign' + loanType).disabled = true;
            $('#' + 'txtDueAmount' + loanType).val('');
            $('#' + 'txtArrsAmount' + loanType).val('');
            var branchId = $("#branchInput").val();
            $.ajax({
                url: '/AxaBankFinance/DayPlanController/getDayPlanTargetInfo',
                data: {"month": month, "loanType": loanType, "branchId": branchId},
                success: function(data) {
                    document.getElementById('btnProcess' + loanType).disabled = false;
                    document.getElementById('btnProcess' + loanType).value = 'Process';
                    $('#' + 'txtDueAmount' + loanType).val(data.dueAmount.toFixed(2));
                    $('#' + 'txtArrsAmount' + loanType).val(data.arriesAmount.toFixed(2));
                    document.getElementById('txtDueAmount' + loanType).disabled = false;
                    document.getElementById('txtArrsAmount' + loanType).disabled = false;
                    document.getElementById('txtDueRate' + loanType).disabled = false;
                    document.getElementById('txtArrsRate' + loanType).disabled = false;
                    document.getElementById('btnCteateTarget' + loanType).disabled = false;
                    document.getElementById('txtTarget' + loanType).disabled = false;
                    document.getElementById('btnAssign' + loanType).disabled = false;
                }
            });
        } else {
            $("#msgCmdMonth").html("Select Month");
            $("#cmdMonth").addClass("txtError");
            $("#cmdMonth").focus();
        }
    }

    function calculateTarget(loanType) {
        var dueRate = $('#' + 'txtDueRate' + loanType).val();
        var arrsRate = $('#' + 'txtArrsRate' + loanType).val();
        var dueAmount = $('#' + 'txtDueAmount' + loanType).val();
        var arrsAmount = $('#' + 'txtArrsAmount' + loanType).val();
        if ((dueRate !== null && dueRate !== "") && (arrsRate !== null && arrsRate !== "")) {
            $('#' + 'txtDueRate' + loanType).removeClass("txtError");
            $('#' + 'txtArrsRate' + loanType).removeClass("txtError");
            var dueTarget = (parseFloat(dueAmount) * parseInt(dueRate) / 100);
            var arrsTarget = (parseFloat(arrsAmount) * parseInt(arrsRate) / 100);
            var totTarget = dueTarget + arrsTarget;
            $('#' + 'txtTarget' + loanType).val(totTarget.toFixed(2));
        } else {
            $('#' + 'txtDueRate' + loanType).addClass("txtError");
            $('#' + 'txtDueRate' + loanType).focus();
            $('#' + 'txtArrsRate' + loanType).addClass("txtError");
            $('#' + 'txtArrsRate' + loanType).focus();
            $('#' + 'txtTarget' + loanType).val('');
        }
    }

    function getOfficers(loanType) {
        var target = $('#' + 'txtTarget' + loanType).val();
        if (target !== null && target !== "") {
            $('#' + 'txtTarget' + loanType).removeClass("txtError");
            var branchId = $("#branchInput").val();
            var month = $("#cmdMonth").val();
            $.ajax({
                url: '/AxaBankFinance/DayPlanController/getRecoveryManagers',
                data: {"userType": 6, "branchId": branchId, "loanType": loanType, "tgtDate": month},
                success: function(data) {
                    $('#popup_searchOfficers').html('');
                    $('#popup_searchOfficers').html(data);
                    $('#hidLoanType').val(loanType);
                    $('#hidTotDue').val($('#' + 'txtDueAmount' + loanType).val());
                    $('#hidTotArrs').val($('#' + 'txtArrsAmount' + loanType).val());
                    $('#hidDueRate').val($('#' + 'txtDueRate' + loanType).val());
                    $('#hidArrsRate').val($('#' + 'txtArrsRate' + loanType).val());
                    $('#hidTarget').val(target);
                    $('#hidMonth_Year').val($("#cmdMonth").val());
                    $('#hidBranch').val(branchId);
                    ui('#popup_searchOfficers');
                },
                error: function() {
                    console.log("iiiiiiiiiiiii");
                }
            });
        } else {
            $('#' + 'txtTarget' + loanType).addClass("txtError");
            $('#' + 'txtTarget' + loanType).focus();
        }
    }
</script>

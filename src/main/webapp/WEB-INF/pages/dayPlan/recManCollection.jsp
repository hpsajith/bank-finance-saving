<%-- 
    Document   : recManCollection
    Created on : Dec 14, 2015, 10:44:41 PM
    Author     : IT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script src="${cp}/resources/js/Chart.js"></script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="row">
        <div class="row" style="margin:5 5 5 5">
            <div class="col-md-2">
                <label class="lb"> Manager : </label>
            </div>
            <div class="col-md-2" style="margin-left: -45px">
                <select id="inputRecMan" style="width: 100%">
                    <c:if test="${managers.size()>0}">
                        <c:forEach var="manager" items="${managers}">
                            <option value="${manager.empNo}">${manager.empLname}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
            <div class="col-md-2">
                <label class="lb"> Loan Type : </label>
            </div>
            <div class="col-md-2" style="margin-left: -45px">
                <select id="inputLoanType" disabled style="width: 100%"></select>
            </div>  
            <div class="col-md-2">
                <label class="lb"> Fetch Type : </label>
            </div>
            <div class="col-md-2" style="margin-left: -45px">
                <select id="inputFetchType" style="width: 100%">
                    <option value="1">Monthly</option>
                    <option value="2">Daily</option>
                </select>
            </div>             
            <div class="col-md-1" style="margin-left: 10px; margin-top: -4px">
                <input type="button" id="btnFetchChart" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="fetchChart()" value="Fetch" disabled style="width:100%">
            </div>
        </div>    
    </div>    
</div>
<div id="chartDiv" style="width: 85%; margin-left: -15%" class="searchCustomer">
    <div class="close_div" onclick="unblockui()"><label>X</label></div>
    <div class="row" id="chartTitle" style="margin-left: 15px; margin-right: 15px;"></div>
    <div class="row" >
        <div id='canvasDiv' class="row" style="margin-top: 10px"> 
        </div>          
    </div>  
    <div class="row" style="margin-top: 20px">
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2" id="collection">
        </div>
        <div class="col-md-2" id="target">
        </div>
    </div>    
</div>
<script type="text/javascript">
    $(function() {
        $("#inputRecMan").change(function() {
            var managerId = $("#inputRecMan").find("option:selected").val();
            $.ajax({
                url: "/AxaBankFinance/DayPlanController/getAssignedLoanTypesToRecMan",
                data: {"managerId": managerId},
                success: function(data) {
                    if (data.length !== 0) {
                        $("#inputLoanType").html("");
                        for (var i = 0; i < data.length; i++) {
                            var loanTypeId = data[i].loanTypeId;
                            var loanTypeName = data[i].loanTypeName;
                            $("#inputLoanType").append("<option value = '" + loanTypeId + "'>" + loanTypeName + "</option>");
                        }
                        document.getElementById("inputLoanType").disabled = false;
                        document.getElementById("btnFetchChart").disabled = false;
                    } else {
                        $("#inputLoanType").html("");
                        document.getElementById("inputLoanType").disabled = true;
                        document.getElementById("btnFetchChart").disabled = true;
                    }
                }});
        });
    });

    function fetchChart() {
        var managerId = $("#inputRecMan").find("option:selected").val();
        var loanType = $("#inputLoanType").find("option:selected").val();
        var fetchType = $("#inputFetchType").find("option:selected").val();
        if (managerId !== null && managerId !== "") {
            $.ajax({
                url: "/AxaBankFinance/DayPlanController/getRecManChartDataSet",
                data: {"managerId": managerId, "loanType": loanType, "type": fetchType},
                success: function(data) {
                    if (data !== null) {
                        ui("#chartDiv");
                        if (fetchType==="1") {
                            $("#chartTitle").html("<legend style='text-align:left; font-weight:bold'>"+"Monthly Progress Chart - "+new Date().getFullYear()+" (All Months)"+"</legend>");
                        }else if (fetchType==="2") {
                            $("#chartTitle").html("<legend style='text-align:left; font-weight:bold'>"+"Daily Progress Chart - "+new Date().getFullYear()+" (Current Month)"+"</legend>");
                        }
                        $("#canvasDiv").html("");
                        $("#canvasDiv").append("<canvas id='myChart' width='900' height='600'></canvas>");
                        var ctx = document.getElementById("myChart").getContext("2d");
                        var chart = new Chart(ctx).Bar(data);
                        $("#collection").html("");
                        $("#collection").html("<label style='margin-left:0px'>Achivement</label><div style='width:10px; height: 10px; background: #4485F6; margin-left:70px'></div>");
                        $("#target").html("");
                        $("#target").html("<label style='margin-left:0px'>Target</label><div style='width:10px; height: 10px; background: #1AD190; margin-left:70px'></div>");
                    } else {
                        $("#collection").html("");
                        $("#target").html("");
                    }
                }
            });
        }        
    }
</script> 
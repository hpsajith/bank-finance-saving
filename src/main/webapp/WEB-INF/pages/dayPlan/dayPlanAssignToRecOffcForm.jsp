<%-- 
    Document   : dayPlanAssignToRecOffcForm
    Created on : Nov 18, 2015, 4:48:10 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#viewLoanType").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "50%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });
</script> 
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="row" style="margin:5 5 5 5">
        <div class="col-md-1">
            <label class="lb"> Type </label>
        </div>
        <div class="col-md-2">
            <select id="loanTypeInput" style="width: 100%">
                <c:forEach var="loanType" items="${loanTypes}">
                    <option value="${loanType.loanTypeId}" selected>${loanType.loanTypeName}</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-1">
            <label class="lb"> Month </label>
        </div>
        <div class="col-md-2">
            <input type="text" name="month" id="cmdMonth" value="" class="date-picker-year" style="width: 100%"/>
            <label id="msgCmdMonth" class="msgTextField"></label>
        </div>  
        <div class="col-md-2">
            <input type="button" value="Get Target" id="btnGetTarget" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="getTargetAmount()"/>
        </div>
        <div class="col-md-4">
            <label id="lblTargetAmount" style="margin-top: 1%; font-size: 20px; font-weight: bold"></label>
        </div>
    </div>
    <div class="row" style="overflow-x: auto">
        <table id="tblRecOffc"  class="dc_fixed_tables table-bordered" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: center">Officer No</th>
                    <th style="text-align: center">Officer Name</th>
                    <th style="text-align: center">Contact No</th>
                    <th style="text-align: center">Receivable Amount</th>
                    <th style="text-align: center">Expected Amount</th>
                    <th style="text-align: center">Assign</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <div class="dc_clear"></div>
    </div>
</div>
<div class="hiddenTarget">
    <form id="targetForm">
        <input type='hidden' name="recOffId" id="hidRecOffId"/>
        <input type='hidden' name="tgtAmount" id="hidTgtAmount"/>
        <input type='hidden' name="tgtDate" id="hidTgtDate"/>
        <input type='hidden' name="loanType" id="hidLoanType"/>
    </form>
</div>
<div id="message_save" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">

    $(function() {
        $('#tblRecOffc').hide();
    });

    $('#cmdMonth').datepicker({
        showButtonPanel: true,
        stepMonths: 0,
        dateFormat: 'yy-mm-dd',
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(new Date().getFullYear(), new Date().getMonth(), 1));
            $("#msgCmdMonth").html("");
            $("#cmdMonth").removeClass("txtError");
        }
    });

    var tgm;
    function getTargetAmount() {
        var loanType = $("#loanTypeInput").val();
        var targetDate = $("#cmdMonth").val();
        if (targetDate !== null && targetDate !== "") {
            $.ajax({
                url: '/AxaBankFinance/DayPlanController/getTarget',
                data: {"loanType": loanType, "targetDate": targetDate},
                success: function(data) {
                    if (data > 0) {
                        tgm = data;
                        $('#lblTargetAmount').text("Target Amount: " + data.toFixed(2));
                        $('#lblTargetAmount').css("color", "#1652BA");
                        $('#lblTargetAmount').show();
                        getRecoveryOfficersWithCollection(loanType,targetDate);
                    } else {
                        $('#lblTargetAmount').text("There is no Assigned Target");
                        $('#lblTargetAmount').css("color", "#D62222");
                        $('#lblTargetAmount').show();
                        $('#tblRecOffc').hide();
                    }
                }
            });
        } else {
            $("#msgCmdMonth").html("Select Month");
            $("#cmdMonth").addClass("txtError");
            $("#cmdMonth").focus();
        }
    }

    function getRecoveryOfficersWithCollection(loanType,targetDate) {
        $.ajax({
            url: '/AxaBankFinance/DayPlanController/getRecoveryOfficersWithCollection',
            data: {"loanType": loanType, "targetDate": targetDate},
            success: function(data) {
                $('#tblRecOffc tbody').html("");
                for (var i = 0; i < data.length; i++) {
                    $('#tblRecOffc tbody').append("<tr id='" + data[i].recOffId + "'>" +
                            "<td style='text-align: center'>" + data[i].recOffId + "</td>" +
                            "<td style='text-align: center'>" + data[i].recOffName + "</td>" +
                            "<td style='text-align: center'>" + data[i].contatcNo + "</td>" +
                            "<td style='text-align: right'>" + data[i].receivable.toFixed(2) + "</td>" +
                            "<td><input type='text' size='20' id='" + data[i].recOffId + "txtTargetAmount' onkeyup='validateAmount(" + data[i].recOffId + ")' value='"+data[i].assignedAmount.toFixed(2)+"' style='width:100%; text-align:right'/></td>" +
                            "<td><input type='button' class='btn btn-default col-md-12 col-sm-12 col-lg-12' value='Assign' id='" + data[i].recOffId + "btnAmount' onclick='saveAssignedTarget(" + data[i].recOffId + ")'/></td>" +
                            "</tr>");
                }
                $('#tblRecOffc').show();
            }
        });
    }

    function saveAssignedTarget(empNo) {
        var tgtAmount = $('#' + empNo + 'txtTargetAmount').val();
        var loanType = $("#loanTypeInput").val();
        var targetDate = $("#cmdMonth").val();
        if (tgtAmount !== null && tgtAmount !== "" && tgtAmount > 0) {
            $('#' + empNo + 'txtTargetAmount').removeClass("txtError");
            $('#hidRecOffId').val(empNo);
            $('#hidTgtAmount').val(tgtAmount);
            $('#hidTgtDate').val(targetDate);
            $('#hidLoanType').val(loanType);
            $.ajax({
                url: '/AxaBankFinance/DayPlanController/saveAssignedTargetInfo?${_csrf.parameterName}=${_csrf.token}',
                type: 'POST',
                data: $("#targetForm").serialize(),
                success: function(data) {
                    $('#message_save').html(data);
                    ui("#message_save");
                    getTargetAmount();
                },
                error: function() {
                    alert("Please Re-Login");
                }
            });
        } else {
            $('#' + empNo + 'txtTargetAmount').addClass("txtError");
            $('#' + empNo + 'txtTargetAmount').focus();
        }
    }

//    function editAssignedAmount(empNo) {
//        var loanType = $("#loanTypeInput").val();
//        var targetDate = $("#cmdMonth").val();
//        if ((loanType !== null && loanType !== "") && (targetDate !== null && targetDate !== "")) {
//            $('#' + empNo + 'txtTargetAmount').removeClass("txtError");
//            $.ajax({
//                url: '/AxaBankFinance/DayPlanController/getAssignedAmountToRecOff',
//                data: {"loanType": loanType, "targetDate": targetDate, "recOffId": empNo},
//                success: function(data) {
//                    $('#' + empNo + 'txtTargetAmount').val(data.toFixed(2));
//                },
//                error: function() {
//                    alert("Please Re-Login");
//                }
//            });
//        }
//    }

    function validateAmount(empNo) {
        var tgtAmount = $('#' + empNo + 'txtTargetAmount').val();
        if (isNaN(tgtAmount)) {
            $('#' + empNo + 'txtTargetAmount').val('');
        } else {
            $('#' + empNo + 'txtTargetAmount').removeClass("txtError");
            if (tgtAmount > tgm) {
                alert("Not a Valid Amount.");
                document.getElementById(empNo + 'btnAmount').disabled = true;
            } else {
                document.getElementById(empNo + 'btnAmount').disabled = false;
            }
        }
    }



</script>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="row" style="overflow-x: auto ; margin:5 5 5 30; width: 95%">
    <table id="tblSchedule"  class="dc_fixed_tables table-bordered" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th style="text-align: center">Date</th>
                <th style="text-align: center">Target</th>
                <th style="text-align: center">Collection</th>
                <th style="text-align: center">Difference</th>
                <th style="text-align: center">Reason</th>
                <th style="text-align: center">In-Active</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="d" items="${dateList}">
                <c:set var="sid" value="${fn:split(d, '-')}"></c:set>
                <c:set var="id" value="${fn:join(sid, '')}"></c:set>
                <c:set var="isTargetDate" value="false"></c:set>
                <c:set var="sch" value=""></c:set>
                <c:forEach var="sc" items="${schedule}">
                    <c:if test="${sc.targetDate==d}">
                        <c:set var="isTargetDate" value="true"></c:set>
                        <c:set var="sch" value="${sc}"></c:set>
                    </c:if>
                </c:forEach>
                <c:choose>
                    <c:when test="${isTargetDate}">
                        <c:choose>
                            <c:when test="${sch.isactive}">
                                <tr>
                                    <td>${d}</td>
                                    <td><input type="text" id="txtTarget${id}" value="${sch.target}" style="text-align: right" onkeyup="calculateDiff(${id}, event)"/></td>
                                    <td><input type="text" id="txtCollection${id}" value="${sch.collection}" style="text-align: right" readonly/></td>
                                    <td><input type="text" id="txtDifference${id}" value="${sch.difference}" style="text-align: right" readonly/></td>
                                    <td><input type="text" id="txtReason${id}" value="${sch.reason}" style="text-align: right"/></td>
                                        <c:choose>
                                            <c:when test="${sch.pk>0}">
                                            <td><div class="delete" href='#' id="btnInactive${id}" onclick="inActive(${id},${sch.pk}, event)"></div></td>
                                            </c:when>
                                            <c:otherwise>
                                            <td><div class="x" href='#' id="btnInactive${id}" ></div></td>
                                            </c:otherwise>
                                        </c:choose>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr style='background-color: #B8B8B8 ;color: #000000'>
                                    <td>${d}</td>
                                    <td><input type="text" id="txtTarget${id}" value="${sch.target}" style="text-align: right" onkeyup="calculateDiff(${id}, event)"/></td>
                                    <td><input type="text" id="txtCollection${id}" value="${sch.collection}" style="text-align: right" readonly /></td>
                                    <td><input type="text" id="txtDifference${id}" value="${sch.difference}" style="text-align: right" readonly/></td>
                                    <td><input type="text" id="txtReason${id}" value="${sch.reason}" style="text-align: right"/></td>
                                        <c:choose>
                                            <c:when test="${sch.pk>0}">
                                            <td><div class="btnActive" href='#' id="btnInactive${id}" onclick="active(${sch.pk}, event)"></div></td>
                                            </c:when>
                                            <c:otherwise>
                                            <td><div class="x" href='#' ></div></td>
                                            </c:otherwise>
                                        </c:choose>
                                </tr>                         
                            </c:otherwise>
                        </c:choose>                             
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td>${d}</td>
                            <td><input type="text" id="txtTarget${id}" value="0.00" style="text-align: right" onkeyup="calculateDiff(${id}, event)"/></td>
                            <td><input type="text" id="txtCollection${id}" value="0.00" style="text-align: right" readonly /></td>
                            <td><input type="text" id="txtDifference${id}" value="0.00" style="text-align: right" readonly/></td>
                            <td><input type="text" id="txtReason${id}" value="" style="text-align: right"/></td>
                            <td><div class="x" href='#'></div></td>
                        </tr>                            
                    </c:otherwise>
                </c:choose>
            </c:forEach>                    
        </tbody>
    </table>
    <div class="dc_clear"></div>
</div>
<div class="row" style="margin: 1 5 5 14">
    <div class="col-md-2">
        <input type="button" id="btnSave" class="btn btn-default col-md-12 col-sm-12 col-lg-12" value="Save" style="width: 100%" onclick="saveSchedule()"/>
    </div>
    <label id="lblTgtTot" style="margin-left: 120px; font-size: 20px">${totTarget}</label>
    <label id="lblClcTot" style="margin-left: 80px; font-size: 20px">${totCollection}</label>
    <label id="lblDiffTot" style="margin-left: 90px; font-size: 20px">${totDifference}</label>
</div>

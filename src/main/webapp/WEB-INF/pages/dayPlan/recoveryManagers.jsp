<%-- 
    Document   : recoveryManagers
    Created on : Jan 26, 2016, 12:30:00 AM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="row" style="margin:20px; height: 65%; overflow-y: auto">
        <label>Recovery Managers</label>
        <table class="table table-bordered table-edit table-responsive dataTable" id="tblOfficers" style="font-size: 12px;">
            <thead> 
                <tr>
                    <td>Assigned</td>
                    <td>No</td>
                    <td>Name</td>
                    <td>Phone</td>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${assignedRecManagers.size() > 0}">
                        <c:forEach var="recManager" items="${recManagers}">
                            <c:forEach var="assignedRecManager" items="${assignedRecManagers}">
                                <c:choose>
                                    <c:when test="${recManager.empNo==assignedRecManager.empNo}">
                                        <tr id="${recManager.empNo}" class="highlight">
                                            <td><input type="checkbox" id="chx${recManager.empNo}" class="recManCheckBox" checked="true" onclick="selectRecMan(${recManager.empNo})"/></td>
                                            <td>${recManager.empNo}</td>
                                            <td>${recManager.empLname}</td>
                                            <td>${recManager.phone2}</td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <tr id="${recManager.empNo}">
                                            <td><input type="checkbox" class="recManCheckBox" id="chx${recManager.empNo}" onclick="selectRecMan(${recManager.empNo})"/></td>
                                            <td>${recManager.empNo}</td>
                                            <td>${recManager.empLname}</td>
                                            <td>${recManager.phone2}</td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:forEach>                        
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="recManager" items="${recManagers}">
                            <tr id="${recManager.empNo}">
                                <td><input type="checkbox" class="recManCheckBox" id="chx${recManager.empNo}" onclick="selectRecMan(${recManager.empNo})"/></td>
                                <td>${recManager.empNo}</td>
                                <td>${recManager.empLname}</td>
                                <td>${recManager.phone2}</td>
                            </tr>
                        </c:forEach>      
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
    <div class="row" >
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4"></div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" id="btnSave" onclick="saveTargetInfo()"><span class="glyphicon glyphicon-ok-circle"></span> Add</button>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <button class="btn btn-default col-md-12" id="btnCancel" onclick="cancelPopUp()"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hiddenDayPlan">
    <form id="dayPanForm">
        <input type='hidden' name="loanType" id="hidLoanType"/>
        <input type='hidden' name="totDue" id="hidTotDue"/>
        <input type='hidden' name="totArrs" id="hidTotArrs"/>
        <input type='hidden' name="dueRate" id="hidDueRate"/>
        <input type='hidden' name="arrsRate" id="hidArrsRate"/>
        <input type='hidden' name="target" id="hidTarget"/>
        <input type='hidden' name="month_year" id="hidMonth_Year"/>
        <input type='hidden' name="branch" id="hidBranch"/>
        <input type='hidden' name="recMan" id="hidRecMan"/>
    </form>
</div>
<div id="message_save" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">
    function saveTargetInfo() {
        var recMan = $('#hidRecMan').val();
        if (recMan !== 0 && recMan !== null && recMan !== "") {
            $("#msgError").text("");
            $.ajax({
                url: '/AxaBankFinance/DayPlanController/saveTargetInfo?${_csrf.parameterName}=${_csrf.token}',
                type: 'POST',
                data: $("#dayPanForm").serialize(),
                success: function(data) {
                    $('#message_save').html(data);
                    ui("#message_save");
                    loadTargetMain();
                },
                error: function() {
                    alert("Please Re-Login");
                }
            });
        }
    }

    var recManId;
    var checked = false;
    function selectRecMan(rManId) {
        $('#chx' + rManId).is(':checked');
        $('.recManCheckBox').prop('checked', false);
        $('#chx' + rManId).prop('checked', true);
        $('#hidRecMan').val(rManId);
        recManId = rManId;
        checked = true;
    }

    function cancelPopUp() {
        $('#hidRecMan').val("");
        unblockui();
    }
</script>
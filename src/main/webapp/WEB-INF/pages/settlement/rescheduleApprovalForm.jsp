<%-- 
    Document   : rescheduleApprovalForm
    Created on : May 5, 2016, 10:43:04 AM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Re-Schedule Approval</strong>
    </div>
    <form name="loanRescheduleForm" id="approveLoanReschedule">
        <div class="col-md-12">
            <input type="hidden" name="id" value="${loanRescheduleDetail.id}"/>
            <input type="hidden" name="loanBalance" value="${loanRescheduleDetail.loanBalance}"/>
            <input type="hidden" name="remainBalance" value="${loanRescheduleDetail.remainBalance}"/>
            <input type="hidden" name="remainInstallments" value="${loanRescheduleDetail.remainInstallments}"/>
            <input type="hidden" name="paidInstallments" value="${loanRescheduleDetail.paidInstallments}"/>
            <input type="hidden" name="remainInterest" value="${loanRescheduleDetail.remainInterest}"/>
            <input type="hidden" name="rescheduleBalance" value="${loanRescheduleDetail.rescheduleBalance}"/>
            <input type="hidden" name="loanId" value="${loanRescheduleDetail.loanHeaderDetailsByParentLoan.loanId}"/>
            <div class="row" style="padding: 5px;">
                <c:choose>
                    <c:when test="${loanRescheduleDetail.approvalComment != null}">
                        <c:choose>
                            <c:when test="${loanRescheduleDetail.approval > 0}">
                                <div class="col-md-2"></div>
                                <div class="col-md-1"><input type="radio" name="approval" value="1" checked="true"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="approval" value="0" /></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-2"></div>
                                <div class="col-md-1"><input type="radio" name="approval" value="1"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="approval" value="0" checked="true"/></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:otherwise>                    
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-2"></div>
                        <div class="col-md-1"><input type="radio" name="approval" value="1"/></div>
                        <div class="col-md-3"><strong>Approve</strong></div>
                        <div class="col-md-1"><input type="radio" name="approval" value="0" /></div>
                        <div class="col-md-3"><strong>Reject</strong></div>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Comment</div>
                <div class="col-md-10">
                    <textarea rows="3" id="txtApprovalComment" name="approvalComment" style="width: 100%">${loanRescheduleDetail.approvalComment}</textarea>
                    <label id="msgApprovalComment" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                    <c:choose>
                        <c:when test="${loanRescheduleDetail.approvalComment != null}">
                        <div class="col-md-4"><input type="button"  value="Update" class="btn btn-default col-md-12" onclick="approveRescheduleLoan()"></div>
                        </c:when>
                        <c:otherwise>
                        <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="approveRescheduleLoan()"></div>
                        </c:otherwise>
                    </c:choose>
            </div>
        </div>  
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div> 
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>

    function approveRescheduleLoan() {
        var approvalComment = $("#txtApprovalComment").val().trim();
        if (approvalComment !== null || approvalComment !== "") {
            $("#msgApprovalComment").html("");
            $("#txtApprovalComment").removeClass("txtError");
            $.ajax({
                url: '/AxaBankFinance/SettlementController/saveOrUpdateLoanRescheduleDetails',
                type: 'POST',
                data: $('#approveLoanReschedule').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadRescheduleApprovalMain();
                    }, 1500);
                },
                error: function() {
                    alert("Error Loding..");
                }
            });
        } else {
            $("#msgApprovalComment").html("Please Enter Comment");
            $("#txtApprovalComment").addClass("txtError");
            $("#txtApprovalComment").focus();
        }
    }

</script>
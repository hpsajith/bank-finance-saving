<%-- 
    Document   : OverPaymentRefundForm
    Created on : Sep 23, 2016, 8:45:08 AM
    Author     : Chanaka
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
    });</script>
<div class="col-lg-12 col-md-12 col-sm-12">
    <h2 class="text-center">Payment Refund !</h2>
    <form name="PaymentRefundForm" id="PaymentRefundForm" style="padding: 15px " >
        <div class="row">
            <div class="col-md-1 col-sm-1 col-lg-1"><input type="hidden" id="loanId" name="loanId" value="${settlementmain.loanId}"></div>
            <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Loan Agreement No</div>
            <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" value="${settlementmain.loanHeaderDetails.loanAgreementNo}" readonly="true" style="width:100%" /></div>   
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
        </div>
        <div class="row">
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
            <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Amount</div>
            <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" value="${settlementmain.overPaymentAmt}" name="refundAmount" id="refundAmount" style="width:100%" /> </div>   
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
        </div>
        <div class="row">
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
            <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Receipt No</div>
            <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" value="${settlementmain.receiptNo}" name="receiptNo" readonly="true" style="width:100%" /> </div>   
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
        </div>
        <div class="row">
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
            <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Add Reason *</div>
            <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" id="reason" name="reason" style="width:100%"></div>
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
        </div>
        <div class="row" style="margin-top: 5px">
            <div class="col-md-2 col-sm-2 col-lg-2"></div>
            <div class="col-md-3 col-sm-3 col-lg-3"></div>
            <div class="col-md-3 col-sm-3 col-lg-3"><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()" style="width:100%"></div>
            <div class="col-md-3 col-sm-3 col-lg-3"><input type="button" value="Refund" class="btn btn-success col-md-12 col-sm-12 col-lg-12" onclick="saveRefund()" style="width:100%"></div>
            <div class="col-md-1 col-sm-1 col-lg-1"></div>
        </div> 

    </form>
    <form id="defaultForm">
        <input type="hidden" readonly="true" id="fullAmount" value="${settlementmain.overPaymentAmt}">
    </form>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>
    function saveRefund() {
        var full = document.getElementById("fullAmount").value;
        var pay = document.getElementById("refundAmount").value;
        if (parseFloat(full) >= parseFloat(pay) >= 0) {
            $.ajax({
                url: "/AxaBankFinance/SettlementController/saveRefund",
                type: 'GET',
                data: $('#PaymentRefundForm').serialize(),
                success: function (data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function () {
                        loadSearchforRefund();
                    }, 1000);

                },
                error: function (data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                }
            });
        } else {
            alert("Please enter valid amount");
        }
    }
</script>


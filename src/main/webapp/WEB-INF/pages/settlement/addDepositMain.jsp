<%-- 
    Document   : addDepositMain
    Created on : Apr 7, 2016, 12:55:37 PM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#tblDeposit").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "50%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });

</script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-5" id="divDepositForm">
        <!--Deposit add form-->
    </div>
    <div class="col-md-7" style="margin-top: 50px; margin-bottom: 20px;">
        <table id="tblDeposit" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: center">Debtor</th>
                    <th style="text-align: center">Account No</th>
                    <th style="text-align: center">Deposit</th>
                    <th style="text-align: center">Date</th>
                    <th style="text-align: center">Edit</th>
                    <th style="text-align: center">Delete</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="deposit" items="${deposits}">
                    <tr id="${deposit.depositId}">
                        <td style="text-align: center">${deposit.debtorName}</td>
                        <td style="text-align: center">${deposit.accountNo}</td>
                        <td style="text-align: right">${deposit.depositAmount}</td>
                        <td style="text-align: center">${deposit.depositDate}</td>
                        <td style="text-align: center"><div class='edit' href='#' onclick="findDeposit(${deposit.depositId})"></div></td>
                        <td style="text-align: center"><div class="delete" href='#' onclick=""></div></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>
<script type="text/javascript">

    $(function() {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadAddDepositForm',
            success: function(data) {
                $('#divDepositForm').html(data);
            },
            error: function() {
            }
        });
    });

    function findDeposit(depositId) {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/findDeposit/' + depositId,
            success: function(data) {
                $('#divDepositForm').html("");
                $('#divDepositForm').html(data);
                document.getElementById("btnSaveDeposit").value = "Update";
            }
        });
    }

</script>
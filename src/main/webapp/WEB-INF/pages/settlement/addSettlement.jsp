<%-- 
    Document   : addSettlement
    Created on : Mar 27, 2015, 3:08:09 PM
    Author     : Administrator
--%>
<!DOCTYPE HTML SYSTEM>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#viewInstallmentList").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "25%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
    });
</script>

<div id="formContentttttt">

    <form name="printForm" id="printForm"  target="blank" action="/AxaBankFinance/SettlementController/printReceipt" method="GET">
     <!--<input type="hidden" value="${loanId}" name="loanId" id="loanId">-->
    </form>

    <label>Installments</label>
                <table id="viewInstallmentList" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <th>Description</th>
                    <th>Due Date</th>
                    <th>Charge</th>
                    <th>Paid</th>
                    <th>Balance</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Down Payment / Other Charges</td>
                            <td></td>
                            <td class="col-md-2" style="text-align: right">${intialDownPayment}</td>
                            <td class="col-md-2" style="text-align: right">${paidDownPayment}</td>
                            <td><input type="text" class="col-md-12" style="text-align: right;padding: 1" value="${downPaymentBalance}" readonly="true"  name="downPayment" id="downPayment"></td>
                        </tr>
                        <tr>
                            <td>Total Arrears</td>
                            <td></td>
                            <td class="col-md-2" style="text-align: right">${totalOpenning}</td>
                            <td class="col-md-2" style="text-align: right">${paidArrears}</td>
                            <td><input type="text" class="col-md-12" style="text-align: right;padding: 1" value="${totalOpeningBalance}" readonly="true"  name="totalArrears" id="totalArrears"></td>
                        </tr>
                        <tr>
                            <td>Total Insurance</td>
                            <td></td>
                            <td class="col-md-2" style="text-align: right">${totalInsurance}</td>
                            <td class="col-md-2" style="text-align: right">${paidInsurance}</td>
                            <td><input type="text" class="col-md-12" style="text-align: right;padding: 1" value="${insuranceBalance}" readonly="true"  name="insuranceBalance" id="insuranceBalance"></td>
                        </tr>
                        <c:forEach var="charges" items="${installmentsList}" varStatus="status">
                            <tr>
                                <td>${charges.description}</td>
                                <td>${charges.due}</td>
                                <td class="col-md-2" style="text-align: right">${charges.charge}</td>
                                <td class="col-md-2" style="text-align: right">${charges.paid}</td>
                                <td><input type="text" class="col-md-12" style="text-align: right;padding: 1" value="${charges.balance}" readonly="true" name="installmentAmount" id="installmentAmount" ></td>    
                            </tr>                     
                        </c:forEach>
                        <c:forEach var="adjudtmt" items="${adjustmentList}" varStatus="status">
                            <tr>
                                <td>${adjudtmt.description}</td>
                                <td>${adjudtmt.due}</td>
                                <td class="col-md-2" style="text-align: right">${adjudtmt.charge}</td>
                                <td class="col-md-2" style="text-align: right">${adjudtmt.paid}</td>
                                <td><input type="text" class="col-md-12" style="text-align: right;padding: 1" value="${adjudtmt.balance}" readonly="true" ></td>    
                            </tr>                     
                        </c:forEach>
                    </tbody> 
                </table> 

    <form name="newPaymentForm" id="newPaymentForm" > 

        <div class="row">    
            <div class="col-md-12 col-lg-12 col-sm-12" style=" border-radius: 2px;">   

                <input type="button" class="btn btn-default" id="showInDetail" value="Charges in Detail" />
                <input type="button" class="btn btn-default" id="btnLoanCheque" onclick="loadAddPDcheque()" value="Add Cheque" />

                <!--<div class="row divError" style="padding:10px; "></div>-->   

                <div class="container-fluid" id="instalmeDetail" style="display:none;">

                    <c:choose>
                        <c:when test="${msgDownPayment}">
                            <label>Down Payment</label>
                            <table class="dc_table dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>      
                                    <tr>
                                        <td>Down Payment</td>
                                        <td>${totalDownPayment}</td>
                                    </tr>
                                </tbody>
                            </table>    
                        </c:when>
                    </c:choose>


                    <c:choose>
                        <c:when test="${msgOther}">
                            <label>Other Charges</label>
                            <table class="dc_table dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <c:forEach var="charges" items="${otherChargesList}">         
                                        <tr>
                                            <td>${charges.description}</td>
                                            <td>${charges.amount}</td>
                                        </tr>
                                    </c:forEach>  
                                </tbody>
                            </table>    
                        </c:when>
                    </c:choose>

                    <c:choose>
                        <c:when test="${msgOpenBal}">
                            <label>Arrears</label>
                            <table class="dc_table dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <c:forEach var="balancs" items="${openningBalanceList}">         
                                        <tr>
                                            <td>${balancs.arreasDescription}</td>
                                            <td>${balancs.amount}</td>
                                        </tr>
                                    </c:forEach>  
                                    <tr>
                                        <td>Opening Without ODI</td>
                                        <td>${openningBalanceWithoputODI}</td>
                                    </tr>
                                </tbody>
                            </table>    
                        </c:when>
                    </c:choose>
                </div>    
                <div class="row"> </div>
                   
            </div>    
        </div>        



        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12"> 

                <!--<div class="row" >-->
                <div class="row divError" style="padding:10px; "></div>    
                <!--    </div>-->
                <div class="row" style="border-top: 1px solid #222; padding-top: 5px; margin-top: 5px; "></div>

                <div class="row">
                    <div class="col-md-2 col-lg-2 col-sm-2"style="font-size: 15px">Paid Installments</div>
                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" value="${instalmentCount}" readonly="true" style="border:2px solid #58A0E8; border-radius: 2px;font-weight: bold;font-size: 12px;"></div>
                    <div class="col-md-1 col-lg-1 col-sm-1"></div>
                    <div class="col-md-3 col-lg-3 col-sm-3" style="font-size: 18px; text-align: right"><strong>Total Due</strong></div>
                    <div class="col-md-4 col-lg-4 col-sm-4"><input type="text" value="${balance}" readonly="true"style="border:2px solid #58A0E8; border-radius: 2px;font-weight: bold;font-size: 20px;text-align: right;width: 100%"></div>              
                </div>

                <c:if test="${msgRebate}">  
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5"></div>
                        <div class="col-md-3 col-lg-3 col-sm-3" style="font-size: 18px; text-align: right"><strong>Rebate Amount</strong></div>
                        <div class="col-md-4 col-lg-4 col-sm-4"><input type="text" value="${rebateBalance}" name="rebateBalance" id="rebateBalance" readonly="true"style="border:2px solid #58A0E8; border-radius: 2px;font-weight: bold;font-size: 20px;text-align: right;width: 100%"></div>  
                    </div>
                </c:if>
                <div class="row">
                    <c:choose>
                        <c:when test="${msgOverPay}">                   
                            <div class="col-md-3 col-lg-3 col-sm-3">Add Over Pay Amount To Installment</div>
                            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" value="${overPayAmount}" readonly="true" name="overPayAmount" id="overPayAmount" style="text-align: right"><input type="checkbox" name="isOverPay" id="isOverPay" >
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-2">
                                <a href="#" onclick="loadSettlementTransferForm(${loanId})" title="Facility Transfer">Add Transfer</a>
                            </div>
                        </c:when>
                    </c:choose> 
                </div> 
                <div id="divProcess" class="jobsPopUp" style="width: 40%;height: 30%"></div>
                <label id="msgOdiAdjust" class="msgTextField"></label> 
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3">ODI Balance</div>
                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="number" value="${odiBalance}" name="odi" id="odi" style="text-align: right" ></div>
                    <div class="col-md-3 col-lg-3 col-sm-3">
                        <input type="button" class="btn btn-default col-md-7 col-sm-7 col-lg-7" id="addAdjustment"  onclick="saveOdi(${loanId})" value="Update ODI" />
                    </div>
                </div>
                <label id="msgAdjRemark" class="msgTextField"></label> 
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3">Remark</div>
                    <div class="col-md-3 col-lg-3 col-sm-3"><input type="text" style="width: 68%" name="remark" id="remark" ></div>
                </div>   
                <label id="msgAdjAmount" class="msgTextField"></label> 

                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3">Add Adjustment</div>
                    <div class="col-md-2 col-lg-2 col-sm-2"><input type="number"  value="" name="adjustment" id="adjustment" style="text-align: right" ></div>
                    <div class="col-md-2">
                        <select name="adjAccount" id="adjAccId" style="width: 95%">
                            <option value="0">SELECT</option>
                            <option value="1-D">Down Payment</option>
                            <option value="2-I">Installment</option>
                            <option value="3-I">Interest</option>
                            <option value="4-I">Over Payment</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-3">
                        <input type="button" class="btn btn-default col-md-9 col-sm-9 col-lg-9"  id="addAdjustment" onclick="saveAdjustment(${loanId})" value="Update Adjustment" />
                    </div>
                </div>  

                <div class="row"style="border: 2px solid #F08080;">

                    <c:if test="${msgDownPayment}">
                        <div class="col-md-3 col-lg-3 col-sm-3">Down Payment/Other charge <input type="checkbox" name="isDownPayment" id="isDownPayment" ></div>
                        </c:if>
                        <c:if test="${msgOpenBal}">
                        <div class="col-md-2 col-lg-2 col-sm-2">Arrears Payment <input type="checkbox" name="isArreas" id="isArreas" ></div>
                        </c:if>
                        <c:if test="${msgInsurance}">
                        <div class="col-md-2 col-lg-2 col-sm-2">Insurance Payment <input type="checkbox" name="isInsurance" id="isInsurance" ></div>
                        </c:if>
                        <c:if test="${msgRebate}">
                        <div class="col-md-2 col-lg-2 col-sm-2">Rebate Payment <input type="checkbox"  name="isRabatePayment" id="isRabatePayment" ></div>
                        </c:if>
                    <div class="col-md-3 col-lg-3 col-sm-3">Installment Payment <input type="checkbox" name="isInstalment" id="isInstalment" ></div>
                </div> 


                <div class="row">
                    <table class="dc_fixed_tables" id="tablePaymntDetil" >
                        <thead>
                        <th>Pay Type</th>
                        <th>Amount</th>
                        <th>Amount in Text</th>
                        <th>Bank</th>
                        <th>Cheque No/<br>Deposite Account No</th>
                        <th>Realize Date/<br>Deposite Date</th>
                        </thead>    
                        <tbody id="tablePaymntDetilBody">
                            <tr id="tableRow">                        
                                <td>
                                    <select name="paymentTypeId" id="paymentTypeId" onchange="checkLoanChequeCount()">
                                        <c:forEach var="v_payType" items="${payTypes}">
                                            <option value="${v_payType.paymentTypeId}" > ${v_payType.paymentType}</option>
                                        </c:forEach>
                                    </select>     
                                </td>       
                                <td><input type="text" name="Amount" id="Amount" value="" onkeypress="return checkNumbers(this)" autocomplete="off"/></td>
                                <td><input type="text" name="amountText" id="amountText" readonly="true"/></td>
                                <td>
                                    <select name="BankId" id="BankId" autocomplete="off">
                                        <option value="0">--Select Bank --</option>
                                        <c:forEach var="v_bank" items="${mBankDetailList}">
                                            <option value="${v_bank.id}"  > ${v_bank.bankName} - ${v_bank.bankBranch} </option>
                                        </c:forEach>
                                    </select>                
                                </td>
                                <td><input type="text" name="AccNo" id="AccNo" value="" autocomplete="off"/></td>
                                <td><input type="text" class="txtCalendar" id="TransDate" name="TransDate" value="" autocomplete="off"/></td>
                        <input type="hidden" value="${loanId}" name="loanId" id="loanId">
                        </tr>   

                        </tbody>
                    </table>
                </div>

                <div class="row" id="preview" style="width:50%;" >
                </div>         
                <div class="row">

                    <!--<input type="button" class="btn col-md-2 col-lg-2 col-sm-2" id="addNewRowBtn" value="New Pay Type" />--> 
                    <!--<input type="button" id="removeNewRowBtn" class="btn col-md-2 col-lg-2 col-sm-2" value="Remove" />-->
                    <input type="button"  onclick="previewPayment()" class="btn btn-info col-md-2 col-lg-2 col-sm-2" value="Preview"/>       
                    <input type="button"  onclick="savePayment()" id="btnPrintReceipt" class="btn btn-success col-md-2 col-lg-2 col-sm-2" value="Print"/>         
                    <input type="button"  onclick="viewDebtorDepositList()" id="btnViewDeposit" class="btn btn-default col-md-2 col-sm-2 col-lg-2" value="View Deposit" style="margin-left: 298px"/>
                    <input type="button"  onclick="viewLoanChequeList()" id="btnViewCheques" class="btn btn-default col-md-2 col-sm-2 col-lg-2" value="View Cheques" style="margin-left: 10px"/>
                </div>
            </div>    
        </div>               
        <input type="hidden" name="pdChequeId" id="pdChequeId" value="0"/>
    </form>  
    <input type="hidden" id="chqueLoanID" value="${loanId}"/>


</div>
<div id="message_sett_form" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<div id="divLoanChequeForm" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="divLoanChequeList" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<div id="divAddTransferForm" class="searchCustomer" style="width: 40%;margin: 5% 10%; border: 0px solid #5e5e5e;"></div>
<script src="${cp}/resources/js/toWord.js" type="text/javascript"></script>                    
<script type="text/javascript">

                        $(function () {
                            $(".txtCalendar").datepicker({
                                dateFormat: 'yy-mm-dd'
                            });
                            var loan_id = $("#loanId").val();
                            loadOtherCharge(loan_id);
                        });

                        $('#Amount').keyup(function (e) {
                            var payingAmount = $("#Amount").val();

                            var words = toWords(payingAmount);
                            $("#amountText").val(words);
                        });

                        var clickCount = 0;
                        $('#showInDetail').click(function () {
                            if (clickCount === 0) {
                                $("#instalmeDetail").css({'display': 'block'});
                                clickCount++;
                            } else {
                                $("#instalmeDetail").css({'display': 'none'});
                                clickCount = 0;
                            }

                        });

                        var count = 1;
                        $('#addNewRowBtn').click(function () {
                            if (count < 5) {
//           var $clone = $input.val('').clone( true )
//            $input.replaceWith($clone);
//            $input = $clone;    
                                var itm = document.getElementById('tableRow');
                                var clone = itm.cloneNode(true);

                                document.getElementById('tablePaymntDetilBody').appendChild(clone);
                                count++;
                            }
                        });

                        $('#removeNewRowBtn').click(function () {
                            if (count > 1) {
                                document.getElementById('tablePaymntDetilBody').deleteRow({'index': count});
                                count--;
                            }
                        });

                        function loadPaymentDetailForm(trId) {
                            var payTypeId = $("#paymentTypeId").val();
                            if (payTypeId === 1) {
                                alert("Pay type " + payTypeId + "  row id" + trId);
//        $.getJSON('/AxaBankFinance/SettlementController/getBankDetailList', function(data) {
//            for (var i = 0; i < data.length; i++) {
//                            var name = data[i].bankName;
//                            var branch = data[i].bankBranch;
//                            var id = data[i].id;
//    $("#tablePaymntDetil tbody tr[id=tableRow1] td select[id=BankId]").append("<option value='"+id+"'>'"+name+"' - '"+branch+"'</option>");   
//          }  
//        });  
//          $("#tablePaymntDetil tbody tr[id=tableRow1] td select[id=BankId]").val('');
                                $("#tablePaymntDetil tbody tr[id=tableRow] td select[id=BankId]").css({'pointer-events': 'none', 'disable': 'true'});
                                $("#tablePaymntDetil tbody tr[id=tableRow] td select[id=BankId]").val('');
                                $("#tablePaymntDetil tbody tr[id=tableRow] td input[id=ChequeNo]").css({'pointer-events': 'none', 'disable': 'true'});
                                $("#tablePaymntDetil tbody tr[id=tableRow] td input[id=ChequeNo]").val('');
                                $("#tablePaymntDetil tbody tr[id=tableRow] td input[id=RealizeDate]").css({'pointer-events': 'none', 'disable': 'true'});
                                $("#tablePaymntDetil tbody tr[id=tableRow] td input[id=RealizeDate]").val('');
                            } else {
                                $("#tablePaymntDetil tbody tr[id=tableRow] td select[id=BankId]").css({'pointer-events': 'auto', 'disable': 'false'});
                                $("#tablePaymntDetil tbody tr[id=tableRow] td input[id=ChequeNo]").css({'pointer-events': 'none', 'disable': 'false'});
                                $("#tablePaymntDetil tbody tr[id=tableRow] td input[id=RealizeDate]").css({'pointer-events': 'none', 'disable': 'false'});
                            }

                        }
                        function previewPayment() {


                            $.ajax({
                                url: '/AxaBankFinance/checkIsBlock',
                                success: function (data) {
                                    if (!data) {
                                        var paymentCategory = selectPaymentCategory();
                                        var validatePayment = validatePaymentForm();
                                        var validateArrears = true;

                                        if (validatePayment && paymentCategory && validateArrears) {
                                            $.ajax({
                                                url: '/AxaBankFinance/SettlementController/viewPreview',
                                                type: 'GET',
                                                data: $("#newPaymentForm").serialize(),
                                                success: function (data) {
                                                    $('#preview').html(data);
                                                    window.scrollBy(0, 500);

                                                }
                                            });
                                        } else {
                                            $(".divError").html("");
                                            $(".divError").css({'display': 'block'});
                                            $(".divError").html(errorMessageArears + errorMessageInstlmt + errorMessage);

                                            errorMessage = "";
                                            errorMessageInstlmt = "";
                                            errorMessageArears = "";
                                        }
                                    } else {
                                        warning("User Is Blocked", "message_sett_form");
                                    }
                                },
                                error: function () {
                                    alert("Connection is refuced by the server...");
                                }
                            });

                        }

                        function savePayment() {

                            $.ajax({
                                url: '/AxaBankFinance/checkIsBlock',
                                success: function (data) {
                                    if (!data) {
                                        var paymentCategory = selectPaymentCategory();
                                        var validatePayment = validatePaymentForm();
//                    var validateArrears = validateArears();        
                                        var validateArrears = true;

                                        if (validatePayment && paymentCategory && validateArrears) {
                                            document.getElementById("btnPrintReceipt").disabled = true;
                                            $.ajax({
                                                url: '/AxaBankFinance/SettlementController/saveAddPayment',
                                                type: 'GET',
                                                data: $("#newPaymentForm").serialize(),
                                                success: function (data) {
//                        printReceipt();                     
                                                    $('#formContentttttt').html(data);
                                                    $("#printForm").submit();
//                        $('#form_').html('');

                                                },
                                                error: function (data) {
                                                    alert(data);
                                                    window.location.reload();
                                                }
                                            });
                                        } else {
                                            $(".divError").html("");
                                            $(".divError").css({'display': 'block'});
                                            $(".divError").html(errorMessageArears + errorMessageInstlmt + errorMessage);

                                            errorMessage = "";
                                            errorMessageInstlmt = "";
                                            errorMessageArears = "";
//                $("html, body").animate({scrollTop: 0}, "slow");
                                        }
                                    } else {
                                        warning("User Is Blocked", "message_sett_form");
                                    }
                                },
                                error: function () {
                                    alert("Connection is refuced by the server...");
                                }
                            });
//    var validateInstallments = validateInstallmentTable();  


                        }

                        //Enter key event
                        $('input[name=Amount]').keydown(function (e) {
                            var code = e.keyCode || e.which;
                            if (code === 13) {
                                previewPayment();
                            }
                        });


                        var errorMessageArears = "";
                        function validateArears() {
                            var validate = true;
                            var isArr = $('#isArreas').is(':checked');

                            var arrears = $('#totalArrears').val();
                            var Amount = $('#Amount').val();
                            if (isArr) {
                                if (arrears - 1 < Amount) {
                                    validate = true;
                                    $("#totalArrears").removeClass("txtError");
                                } else {
                                    validate = false;
                                    errorMessageArears = errorMessageArears + "Pay Total Arears";
                                    $("#totalArrears").addClass("txtError");
                                }
                            }
                            return validate;
                        }


                        var errorMessageInstlmt = "";
                        function validateInstallmentTable() {
                            var validate = true;

                            var otherChargesAmount = $('input[name=otherChargesAmount]').val();
//        var sumOtherCharges =$('input[name=sumOtherCharges]').val();

                            if (otherChargesAmount === null || otherChargesAmount === "") {
                                validate = false;
                                errorMessageInstlmt = errorMessageInstlmt + "Pay Other Charges";
                                $("#otherChargesAmount").addClass("txtError");
                            } else {
                                validate = true;
                                $("#otherChargesAmount").removeClass("txtError");
                            }
                            return validate;
                        }


                        var errorMessage = "";
                        function validatePaymentForm() {
                            var validate = true;

                            var payType = $('#paymentTypeId').val();

                            var payAmount = $('#Amount').val();
                            if (payAmount === null || payAmount === "") {
                                validate = false;
                                errorMessage = errorMessage + "Enter Pay Amount     ";
                                $("#Amount").addClass("txtError");
                            } else {
                                $("#Amount").removeClass("txtError");
                            }


                            var amountText = $('#amountText').val();
                            if (amountText === null || amountText === "") {
                                validate = false;
                                errorMessage = errorMessage + "Enter text amount     ";
                                $("#amountText").addClass("txtError");
                            } else {
                                $("#amountText").removeClass("txtError");
                            }

                            var BankId = $('#BankId').val();
                            if (payType == 2 && BankId == 0) {
                                validate = false;
                                errorMessage = errorMessage + "Select Bank     ";
                                $("#BankId").addClass("txtError");
                            } else {
                                $("#BankId").removeClass("txtError");
                            }


                            var ChequeNo = $('#AccNo').val();
                            if (payType == 2 && ChequeNo == "") {
                                validate = false;
                                errorMessage = errorMessage + "Enter Cheque No     ";
                                $("#AccNo").addClass("txtError");
                            } else {
                                $("#AccNo").removeClass("txtError");
                            }

                            var realizeDate = $('#TransDate').val();
                            if (payType == 2 && realizeDate == "") {
                                validate = false;
                                errorMessage = errorMessage + "Enter Realize date     ";
                                $("#TransDate").addClass("txtError");
                            } else {
                                $("#TransDate").removeClass("txtError");
                            }



                            var BankId = $('#BankId').val();

                            if (payType == 3 && BankId == 0) {
                                validate = false;
                                errorMessage = errorMessage + "Select Bank     ";
                                $("#BankId").addClass("txtError");
                            } else {
                                $("#BankId").removeClass("txtError");
                            }

                            var depositeNo = $('#AccNo').val();
                            if (payType == 3 && depositeNo == "") {
                                validate = false;
                                errorMessage = errorMessage + "Enter Deposite number     ";
                                $("#AccNo").addClass("txtError");
                            } else {
                                $("#AccNo").removeClass("txtError");
                            }


                            var depositeDate = $('#TransDate').val();
                            if (payType == 3 && depositeDate == "") {
                                validate = false;
                                errorMessage = errorMessage + "Enter Deposited date    ";
                                $("#TransDate").addClass("txtError");
                            } else {
                                $("#TransDate").removeClass("txtError");
                            }
                            return validate;
                        }

                        function selectPayCategory() {
                            if ($('#isArreas').is(':checked')) {
                                $('#isDownPayment').prop('checked', false);
                                $('#isRabatePayment').prop('checked', false);
                                $('#isInsurance').prop('checked', false);
                                $('#isArreas').prop('checked', true);
                            }
                            if ($('#isDownPayment').is(':checked')) {
                                $('#isArreas').prop('checked', false);
                                $('#isRabatePayment').prop('checked', false);
                                $('#isInsurance').prop('checked', false);
                                $('#isDownPayment').prop('checked', true);
                            }
                            if ($('#isRabatePayment').is(':checked')) {
                                $('#isArreas').prop('checked', false);
                                $('#isDownPayment').prop('checked', false);
                                $('#isInsurance').prop('checked', false);
                                $('#isRabatePayment').prop('checked', true);
                            }
                            if ($('#isInsurance').is(':checked')) {
                                $('#isArreas').prop('checked', false);
                                $('#isRabatePayment').prop('checked', false);
                                $('#isDownPayment').prop('checked', false);
                                $('#isInsurance').prop('checked', true);
                            }
                        }
                        function selectPaymentCategory() {
                            var isArr = $('#isArreas').is(':checked');
                            var isDwn = $('#isDownPayment').is(':checked');
                            var isRbt = $('#isRabatePayment').is(':checked');
                            var isIns = $('#isInsurance').is(':checked');
                            var inInstlm = $('#isInstalment').is(':checked');
                            var status = isArr || isDwn || isRbt || isIns || inInstlm;

                            if (!status) {
                                alert("Select The Payment Category");
                                return false;
                            } else {
                                return true;
                            }
                        }

                        function saveAdjustment(loanID) {

                            var accId = $("#adjAccId").val();


                            var validateAdjust = validateAdjustForm();
                            if (validateAdjust) {
                                var loanId = loanID;
                                var amount = $('#adjustment').val();
                                var remark = $('#remark').val();

                                $.ajax({
                                    url: '/AxaBankFinance/SettlementController/saveAdjustment',
                                    type: 'GET',
                                    data: {"loanId": loanId, "amount": amount, "remark": remark, "accId": accId},
                                    success: function (data) {
                                        if (data) {
                                            loadMainPage(loanId);
                                        } else {
                                            loadCheckAdjPassWord(loanId, amount, remark);
                                        }
//                                        $('#msgDiv').html(data);
//                                        ui('#msgDiv');
//                                        $('#adjustment').val("");
//                                        $('#remark').val("");
//                                        $("#msgAdjAmount").html("");
//                                        $("#msgAdjRemark").html("");
                                    },
                                    error: function () {
                                        alert("Your Session is Expired. Please Login");
                                        window.location.reload();
                                    }
                                });
                            } else {
                                $("#msgAdjAmount").html(errorMessage10);
                                $("#msgAdjRemark").html(errorMessage11);

                                errorMessage10 = "";
                                errorMessage11 = "";
                            }
                        }

                        function validateAdjustForm() {
                            var validate = true;

                            var adjstAmount = $('#adjustment').val();
                            if (adjstAmount === null || adjstAmount === "") {
                                validate = false;
                                errorMessage10 = "Enter Adjust Amount     ";
                                $("#adjustment").addClass("txtError");
                            } else {
                                $("#adjustment").removeClass("txtError");
                            }

                            var remark = $('#remark').val();
                            if (remark === null || remark === "") {
                                validate = false;
                                errorMessage11 = "Enter Remark     ";
                                $("#remark").addClass("txtError");
                            } else {
                                $("#remark").removeClass("txtError");
                            }

                            var accId = $("#adjAccId").val();
                            if (accId == 0) {
                                validate = false;
                                errorMessage11 = errorMessage11 + ",  Select Account Type     ";
                                $("#adjAccId").addClass("txtError");
                            } else {
                                $("#adjAccId").removeClass("txtError");
                            }
                            return validate;
                        }


                        function saveOdi(loanId) {
                            var odi = $('#odi').val();
                            var loanId = loanId;

                            var validOdi = validateOdi();
                            if (validOdi) {
                                $.ajax({
                                    url: '/AxaBankFinance/SettlementController/saveOdi',
                                    data: {"odi": odi, "loanId": loanId},
                                    success: function (data) {
                                        if (data) {
                                            loadMainPage(loanId);
                                        } else {
                                            loadCheckPassWord(loanId, odi);
                                        }
                                    }
                                });
                            } else {
                                $("#msgOdiAdjust").html(errorMessage14);

                                errorMessage14 = "";
                            }

                        }

                        var errorMessage14 = "";
                        function validateOdi() {
                            var validate = true;

                            var odi = $('#odi').val();
                            if (odi === null || odi === "") {
                                validate = false;
                                errorMessage14 = "Enter Adjust Amount     ";
                                $("#odi").addClass("txtError");
                            } else {
                                $("#odi").removeClass("txtError");
                            }
                            return validate;
                        }

                        function loadMainPage(loanId) {
                            $.ajax({
                                    url: '/AxaBankFinance/SettlementController/loadMainPage',
                                data: {"loanId": loanId},
                                success: function (data) {
                                    $('#formContentttttt').html(data);
                                }
                            });
                        }
                        function loadCheckPassWord(loanId, odi) {
                            $.ajax({
                                url: '/AxaBankFinance/SettlementController/loadCheckPassWord',
                                data: {"odi": odi, "loanId": loanId},
                                success: function (data) {
                                    $('#divProcess').html("");
                                    $('#divProcess').html(data);
                                    ui('#divProcess');
                                }
                            });

                        }
                        function loadCheckAdjPassWord(loanId, amount, remark) {
                            $.ajax({
                                url: '/AxaBankFinance/SettlementController/loadAdjCheckPassWord',
                                data: {"amount": amount, "loanId": loanId, "remark": remark},
                                success: function (data) {
                                    $('#divProcess').html("");
                                    $('#divProcess').html(data);
                                    ui('#divProcess');
                                }
                            });

                        }

                        function loadOtherCharge(loan_id) {
                            $.getJSON('/AxaBankFinance/SettlementController/loadOtherChargeAccounts/' + loan_id, function (data) {

                                for (var i = 0; i < data.length; i++) {
                                    var type = data[i].id + "-O";
                                    $("#adjAccId").append("<option value='" + type + "'>" + data[i].description + "</option>");
                                }
                            });
                        }

                        function setTwoNumberDecimal() {
                            alert("XXXX");
//                            this.value = parseFloat(this.value).toFixed(2);
                        }

                        // Loan Cheque -- (Thushan)
                        function loadAddPDcheque() {
                            $.ajax({
                                url: '/AxaBankFinance/SettlementController/loadAddLoanChequeForm',
                                type: 'GET',
                                success: function (data) {
                                    $("#divLoanChequeForm").html("");
                                    $("#divLoanChequeForm").html(data);
                                    $("#txtLoanId").val($("#chqueLoanID").val());
                                    ui("#divLoanChequeForm");
                                }
                            });
                        }

                        $(function () {
                            document.getElementById("btnViewCheques").disabled = true;
                        });

                        function checkLoanChequeCount() {
                            var paymentType = $("#paymentTypeId").find("option:selected").val();
                            if (paymentType === "2") {
                                var lId = $("#chqueLoanID").val();
                                $.ajax({
                                    url: "/AxaBankFinance/SettlementController/findLoanChequeCount/" + lId,
                                    type: 'GET',
                                    success: function (data) {
                                        if (data !== 0) {
                                            document.getElementById("btnViewCheques").disabled = false;
                                        } else {
                                            document.getElementById("btnViewCheques").disabled = true;
                                        }
                                    }
                                });
                            } else {
                                document.getElementById("btnViewCheques").disabled = true;
                            }
                        }

                        $(function () {
                            document.getElementById("btnViewDeposit").disabled = true;
                        })

                        function viewLoanChequeList() {
                            $("#BankId").empty();
                            var lId = $("#chqueLoanID").val();
                            $.ajax({
                                url: "/AxaBankFinance/SettlementController/getLoanCheques/" + lId,
                                type: 'GET',
                                success: function (data) {
                                    $("#divLoanChequeList").html("");
                                    $("#divLoanChequeList").html(data);
                                    ui("#divLoanChequeList");
                                }
                            });
                        }

                        function checkDebtorDepositCount(debtorId, accNo) {
                            if (debtorId !== "" && accNo !== "") {
                                $.ajax({
                                    url: "/AxaBankFinance/SettlementController/findDebtorDepositCount/" + debtorId + "/" + accNo,
                                    type: 'GET',
                                    success: function (data) {
                                        if (data !== 0) {
                                            document.getElementById("btnViewDeposit").disabled = false;
                                        } else {
                                            document.getElementById("btnViewDeposit").disabled = true;
                                        }
                                    }
                                });
                            } else {
                                document.getElementById("btnViewCheques").disabled = true;
                            }
                        }

                        function viewDebtorDepositList() {
                            var debtorId = $("#debID").val();
                            var accNo = $("#customerCode").val();
                            $.ajax({
                                url: "/AxaBankFinance/SettlementController/findDebtorDeposits/" + debtorId + "/" + accNo,
                                type: 'GET',
                                success: function (data) {
                                    $("#divLoanChequeList").html("");
                                    $("#divLoanChequeList").html(data);
                                    ui("#divLoanChequeList");
                                }
                            });
                        }

                        function loadSettlementTransferForm(transferLoanId) {
                            $.ajax({
                                url: "/AxaBankFinance/SettlementController/loadSettlementTransferForm/" + transferLoanId,
                                type: 'GET',
                                success: function (data) {
                                    $('#divAddTransferForm').html(data);
                                    ui('#divAddTransferForm');
                                },
                                error: function () {
                                    alert("Error Loding..");
                                }
                            });
                        }

</script>    

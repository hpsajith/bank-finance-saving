<%-- 
    Document   : OverPaymentRefund
    Created on : Sep 21, 2016, 3:26:26 PM
    Author     : Chanaka
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        unblockui();
    });
</script>
<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption"> 
    <div class="row"><label style="margin-left: 20px;">Search for Payments to Refund</label></div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Start Date</div>
            <div class="col-md-8">
                <input type="text" name="sDate"  class="txtCalendar"   style="width:100%"  id="txtsDate"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">End Date</div>
            <div class="col-md-8">
                <input type="text" name="eDate" class="txtCalendar"  value="" style="width:100%"  id="txteDate"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">
                <input type="button" class="btn btn-default searchButton" id="btnSearchPayments" style="height: 25px" onclick="getPaymentsForDate()">
            </div>
            <div class="col-md-8">
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-4">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8">
                <input type="text"  style="width:100%" name="" id="txtAgreementNo" onkeyup="getPaymentsForAgreementNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text"  style="width:100%" name="" id="txtVehicleNo" onkeyup="getPaymentsForVehicleNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Customer</div>
            <div class="col-md-8">
                <input type="text"  style="width:100%" name="" id="txtCustomerName" onkeyup="getPaymentsForCustomer()"/>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" id="tablecontent">
</div>
<div id="divProcess" class="searchCustomer " style="width: 65%;height: 30%; margin: 5% 0%; border: 0px solid #5e5e5e;">
</div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..." />
</div>
<script>
    $(".txtCalendar").datepicker({
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd'
    });

    function getPaymentsForDate() {
        var sDate = $("#txtsDate").val();
        var eDate = $("#txteDate").val();
        if (sDate === "" && eDate === "" || sDate === null && eDate === null) {
        } else {
            $.ajax({
                url: "/AxaBankFinance/SettlementController/loadRefundTable",
                data: {"sDate": sDate, "eDate": eDate},
                success: function (data) {
                    $('#tablecontent').html(data);
                }
            });
        }
    }

    function getPaymentsForAgreementNo() {
        var agreementNo = $("#txtAgreementNo").val().trim();
        $.ajax({
            url: "/AxaBankFinance/SettlementController/loadRefundTablebyagno",
            data: {"agreementNo": agreementNo},
            success: function (data) {
                $('#tablecontent').html(data);
            }
        });
    }

    function getPaymentsForVehicleNo() {
        var vehicleNo = $("#txtVehicleNo").val().trim();
        $.ajax({
            url: "/AxaBankFinance/SettlementController/loadRefundTableByVehNo",
            data: {"vehicleNo": vehicleNo},
            success: function (data) {
                $('#tablecontent').html(data);
            }
        });
    }
    
    function getPaymentsForCustomer() {
        var customerName = $("#txtCustomerName").val().trim();
        $.ajax({
            url: "/AxaBankFinance/SettlementController/loadRefundTableByCustomer",
            data: {"customerName": customerName},
            success: function (data) {
                $('#tablecontent').html(data);
            }
        });
    }
</script>
<%-- 
    Document   : rescheduleApprovalMain
    Created on : May 5, 2016, 10:41:35 AM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        unblockui();
    });

</script>
<div class="row fixed-table" id="divRescheduleLoanList" style="overflow-y: auto">
    <div class="table-content">
        <table id="tblRescheduleLoanList" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: center">Agreement No</th>
                    <th style="text-align: center">Debtor</th>
                    <th style="text-align: center">Loan Balance</th>
                    <th style="text-align: center">Interest</th>
                    <th style="text-align: center">Remain Terms</th>
                    <th style="text-align: center">Paid Terms</th>
                    <th style="text-align: center">Schedule Balance</th>
                    <th style="text-align: center">Approval</th>
                    <th style="text-align: center">Process</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="loanRescheduleDetail" items="${loanRescheduleDetails}">
                    <tr>
                        <td style="text-align: center">${loanRescheduleDetail.loanHeaderDetailsByParentLoan.loanAgreementNo}</td>
                        <td style="text-align: center">${loanRescheduleDetail.loanHeaderDetailsByParentLoan.debtorHeaderDetails.nameWithInitial}</td>
                        <td style="text-align: right">${loanRescheduleDetail.loanBalance}</td>
                        <td style="text-align: right">${loanRescheduleDetail.remainInterest}</td>
                        <td style="text-align: center">${loanRescheduleDetail.remainInstallments}</td>
                        <td style="text-align: center">${loanRescheduleDetail.paidInstallments}</td>
                        <td style="text-align: right">${loanRescheduleDetail.rescheduleBalance}</td>
                        <c:choose>
                            <c:when test="${loanRescheduleDetail.approval > 0}">
                                <td style="text-align: center"><a  href='#' onclick="checkRescheduleApproval(${loanRescheduleDetail.id})" class="underline-blue" style="font-style: italic">Approved</a></td>
                            </c:when>
                            <c:otherwise>
                                <td style="text-align: center"><a  href='#' onclick="checkRescheduleApproval(${loanRescheduleDetail.id})" class="underline">Not Approved</a></td>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${loanRescheduleDetail.approval > 0}">
                                <td style="text-align: center"><input type="button" id="btnNewLoan" onclick="newLoan(${loanRescheduleDetail.id})" class="btn btn-default col-md-12" value="New Loan" /></td>
                                </c:when>
                                <c:otherwise>
                                <td style="text-align: center"><input type="button" id="btnNewLoan" onclick="" class="btn btn-default col-md-12" value="New Loan" disabled /></td>
                                </c:otherwise>
                            </c:choose>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="dc_clear"></div>
    </div>
</div>
<div id="divRescheduleForm" class="searchCustomer" style="width: 40%;margin: 5% 10%; border: 0px solid #5e5e5e;">
</div>
<script type="text/javascript">

    function checkRescheduleApproval(rescheduleId) {
        $.ajax({
            url: "/AxaBankFinance/SettlementController/checkLoanRescheduleApproval/" + rescheduleId,
            type: 'GET',
            success: function(data) {
                $('#divRescheduleForm').html(data);
                ui('#divRescheduleForm');
            },
            error: function() {
                alert("Error Loding..");
            }
        });
    }

    function newLoan(rescheduleId) {
        findRescheduleLoanDeatilsById(rescheduleId);
    }

</script>
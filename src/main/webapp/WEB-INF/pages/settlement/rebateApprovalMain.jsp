<%-- 
    Document   : rebateApprovals
    Created on : Jan 25, 2016, 11:16:28 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
//        $("#tblRebateLoans").chromatable({
//            width: "100%", // specify 100%, auto, or a fixed pixel amount
//            height: "80%",
//            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
//        });
        unblockui();
    });

</script>
<div class="row">
    <div class="row" >
        <table class="dc_fixed_tables table-bordered " width="100%" border="0" cellspacing="0" cellpadding="0" id="tblRebateLoans"  >
            <thead>
                <tr>
                    <th style="text-align: center">Agreement No</th>
                    <th style="text-align: center">Vehicle No</th>
                    <th style="text-align: center">Loan Balance</th>
                    <th style="text-align: center">Interest</th>
                    <th style="text-align: center">D.Rate</th>
                    <th style="text-align: center">Discount</th>
                    <th style="text-align: center">Charge</th>
                    <th style="text-align: center">Receivable</th>
                    <th style="text-align: center">Print</th>
                    <th style="text-align: center">Approval 1</th>
                    <th style="text-align: center">Approval 2</th>
                </tr>
            </thead>
            <tbody> 
                <c:forEach var="rebateLoans" items="${rebateDetails}">
                    <tr>
                        <td style="text-align: center">${rebateLoans.loanAggNo}</td>
                        <td style="text-align: center">${rebateLoans.vehicleNo}</td>
                        <td style="text-align: right">${rebateLoans.totalBalance}</td>
                        <td style="text-align: right">${rebateLoans.remainingInterest}</td>
                        <td style="text-align: center">${rebateLoans.discountRate}</td>
                        <td style="text-align: right">${rebateLoans.discountAmount}</td>
                        <td style="text-align: right">${rebateLoans.charge}</td>
                        <td style="text-align: right">${rebateLoans.totalReceivable}</td>
                        <td style="text-align: center"><a href="#" onclick="printRebateQuotation(${rebateLoans.loanId})">Quotation</a></td>
                        <c:choose>
                            <c:when test="${rebateLoans.isApproved > 0}">
                                <td style="text-align: center"><a  href='#' onclick="rebateApproval(${rebateLoans.loanId}, 0)" class="underline-blue"style="font-style: italic">approved</a></td>
                            </c:when>
                            <c:otherwise>
                                <td style="text-align: center"><a  href='#' onclick="rebateApproval(${rebateLoans.loanId}, 0)" class="underline" style="font">not approved</a></td>
                            </c:otherwise>
                        </c:choose>
                        <c:if test="${rebateLoans.approvalStatus == 1}">
                            <td style="text-align: center"><a  href='#' class="underline" onclick="rebateApproval(${rebateLoans.loanId}, 1)">not approved</a></td>
                        </c:if>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<!--    Approve Level 1-->
<div class="searchCustomer" style="width:40%; margin: 5% 10%" id="popup_rebate_approval" ></div>
<form method="GET" action="/AxaBankFinance/ReportController/printRebateQuotation" target="blank" id="formRebateQuotation">
    <input type="hidden" name="loanId" id="txtQuoLoanId"/>
</form>
<script>
    function rebateApproval(loanId, type) {
        var form = new FormData();
        form.append("loanId", loanId);
        form.append("type", type);
        $.ajax({
            url: '/AxaBankFinance/SettlementController/addRebateApproval?${_csrf.parameterName}=${_csrf.token}',
            type: 'POST',
            data: form,
            processData: false,
            contentType: false,
            success: function (data) {
                $("#popup_rebate_approval").html("");
                $("#popup_rebate_approval").html(data);
                ui('#popup_rebate_approval');
            }
        });
    }

    function printRebateQuotation(loanId) {
        $("#txtQuoLoanId").val(loanId);
        $("#formRebateQuotation").submit();
    }

</script>



<%-- 
    Document   : addDepositForm
    Created on : Apr 7, 2016, 9:46:20 AM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="row">
    <div class="col-md-12" style="padding: 5 5 5 15"><strong style="font-size: 15px;color: #10383c">Add Deposit Payment</strong></div>    
    <div class="col-md-12" style="margin-top: 10px">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: right;width: 35%"><label>Search Customer</label></div>
            <div class="col-md-1 col-sm-1 col-lg-1" ><input type="button" class="btn btn-default searchButton" id="btnSearchCustomer" style="height: 25px"></div>
            <div class="col-md-4 col-sm-7 col-lg-7"></div>
        </div>
    </div>
    <div class="col-md-12">
        <form name="depositForm" id="addDeposit">
            <div class="row">
                <input type="hidden" name="depositId" id="txtDepositId" value="${debtorDeposit.depositId}"/>
                <input type="hidden" name="debtorId" id="debID" value="${debtorDeposit.debtorId}"/>
                <input type="hidden" name="depositDate" id="txtDepositDate" value="${debtorDeposit.depositDate}"/>
                <input type="hidden" name="isNew" id="txtIsNew" value="true"/>
                <div class="col-md-4 col-sm-4 col-lg-4" style="margin-top: 10px">Customer Name</div> 
                <div class="col-md-8 col-sm-8 col-lg-8" style="margin-top: 10px">
                    <input type="text" name="" id="debtorName" style="width: 100%" value="${debtorDeposit.debtorName}" readonly />
                    <label id="msgDebtorName" class="msgTextField"></label>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-lg-4 ">Account No</div>    
                <div class="col-md-8 col-sm-8 col-lg-8">
                    <input type="text" name="accountNo" id="txtAccountNo" style="width: 100%" value="${debtorDeposit.accountNo}" readonly/>
                    <label id="msgAccountNo" class="msgTextField"></label>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-lg-4 ">Deposit Amount</div>    
                <div class="col-md-8 col-sm-8 col-lg-8">
                    <input type="text" name="depositAmount" id="txtDepositAmount" style="width: 100%" value="${debtorDeposit.depositAmount}" onkeyup="checkNumbers(this)"/>
                    <label id="msgDepositAmount" class="msgTextField"></label>
                </div> 
            </div>  
            <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
                <div  class="col-md-6 col-sm-6 col-lg-6"></div>
                <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
                <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveDeposit" value="Save" onclick="saveOrUpdateDeposit()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
            </div>
            <input type="hidden" id="gurant_debtr_type" />
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>        
    </div>
</div>
<!--Search Customer-->
<div class="searchCustomer box" style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto" id="popup_searchCustomer"></div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    $(document).ready(function() {
        //Block Laod GuarantorForm(Load SearchCustomer Form)    
        $('#btnSearchCustomer').click(function() {
            ui('#popup_searchCustomer');
            loadSearchCustomer(1);
        });
        $('#btnSearchCustomerExit').click(function() {
            unblockui();
        });
    });

    function saveOrUpdateDeposit() {
        if (fieldValidate()) {
            document.getElementById("btnSaveDeposit").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/SettlementController/saveOrUpdateDebtorDeposit',
                type: 'POST',
                data: $("#addDeposit").serialize(),
                success: function(data) {
                    document.getElementById("btnSaveDeposit").disabled = false;
                    $("#msgDiv").html("");
                    $("#msgDiv").html(data);
                    ui("#msgDiv");
                    setTimeout(function() {
                        loadAddDeposit();
                    }, 1500);
                },
                error: function() {
                    console.log("Error in Deposit");
                }
            });
        }
    }

    function fieldValidate() {
        var debtorName = $("#debtorName").val();
        var accountNo = $("#txtAccountNo").val();
        var depositAmount = $("#txtDepositAmount").val();
        if (debtorName === "" || debtorName === null) {
            $("#msgDebtorName").html("Enter Debtor Name");
            $("#debtorName").addClass("txtError");
            $("#debtorName").focus();
            return false;
        } else {
            $("#msgDebtorName").html("");
            $("#debtorName").removeClass("txtError");
        }
        if (accountNo === "" || accountNo === null) {
            $("#msgAccountNo").html("Enter Account No");
            $("#txtAccountNo").addClass("txtError");
            $("#txtAccountNo").focus();
            return false;
        } else {
            $("#msgAccountNo").html("");
            $("#txtAccountNo").removeClass("txtError");
        }
        if (depositAmount === "" || depositAmount === null) {
            $("#msgDepositAmount").html("Enter Deposit Amount");
            $("#txtDepositAmount").addClass("txtError");
            $("#txtDepositAmount").focus();
            return false;
        } else {
            $("#msgDepositAmount").html("");
            $("#txtDepositAmount").removeClass("txtError");
        }
        return true;
    }

    function loadSearchCustomer(type) {
        $("#gurant_debtr_type").val(type);
        $.ajax({
            url: '/AxaBankFinance/searchCustomer',
            success: function(data) {
                $('#popup_searchCustomer').html("");
                $('#popup_searchCustomer').html(data);
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }

    function unloadSearchCustomer() {
        unblockui();
    }

</script>
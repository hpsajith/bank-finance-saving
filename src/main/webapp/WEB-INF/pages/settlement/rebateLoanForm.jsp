<%-- 
    Document   : rebateLoanForm
    Created on : Dec 21, 2015, 5:24:04 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
    var view = "";
</script>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <form name="rebateForm" id="rebateLoan">
        <div class="row "><label class = "successMsg" style="font-size: 18px">${rebateMsg}</label></div>
        <div class="row" style="margin-bottom: 5px">
            <div class="col-md-5"><strong style="font-size: 15px;color: #10383c">Loan Rebate / Fully Paid / Re-Schedule</strong></div>
            <div class="col-md-7">
                <div class="row" style="border: 1px solid #0075b0">
                    <div class="col-md-2">Rebate</div>
                    <div class="col-md-1"><input type="radio"  name="isFullyPaid" checked="" onclick="clickToRebate(1)" value="0"/></div>
                    <div class="col-md-2">Fully Paid</div>
                    <div class="col-md-1"><input type="radio"  name="isFullyPaid" onclick="clickToFullyPaid(1)"  value="1"/></div>
                    <div class="col-md-2">Re-Schedule</div>
                    <div class="col-md-1"><input type="radio"  name="isFullyPaid" onclick="clickToReschedule(0)"  value=""/></div>
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-3">Total Loan Balance</div>
            <div class="col-md-3"><input type="text" name="totalBalance" id="txtTotalBalance" readonly style="width: 100%" value="${loan_Balance}"/></div>
            <div class="col-md-3">Capital Upto Date</div>
            <div class="col-md-3"><input type="text" name="remainingBalance" id="txtReBalance" readonly style="width: 100%" value="${remInsBalance}"/></div>
        </div>
        <div class="row">
            <div class="col-md-3">Paid Installment</div>
            <div class="col-md-3"><input type="text" name="" id="txtPaidInstalment" readonly style="width: 100%" value="${paidIns}"/></div>
            <div class="col-md-3">Remaining Installment</div>
            <div class="col-md-3"><input type="text" name="reInstalment" id="txtReInstalment" readonly style="width: 100%" value="${reInsCount}"/></div>
        </div>
        <div class=" row">
            <div class="col-md-3">Interest Upto Date</div>
            <div class="col-md-3"><input type="text" name="remainingInterest" id="txtReInterest" readonly style="width: 100%" value="${interest}"/></div>
            <div class="col-md-3">Discount Rate(%)<label class="redmsg">*</label></div>
            <div class="col-md-3"><input type="text" name="discountRate" onkeypress="return checkNumbers(this)" id="txtRebateRate" value="0.00" style="width: 100%"/>
                <label id="msgRebateRate" class="msgTextField"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">Discount Value </div>
            <div class="col-md-3"><input type="text" name="discountAmount" readonly value="0.00"  id="txtDisAmount" style="width: 100%"/>
                <label id="msgDisAmount" class="msgTextField"></label>
            </div>
            <div class="col-md-3" id="txtChangeTxt1">Rebate Amount </div>
            <div class="col-md-3"><input type="text" name="rebateAmount" value="0.00" readonly  id="txtRebtAmount" style="width: 100%"/>
                <label id="msgTotalReceible" class="msgTextField"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3" id="txtChangeTxt2">Rebate Charge </div>
            <div class="col-md-3"><input type="text" name="charge" value="0.00"  id="txtRebtCharge" style="width: 100%" onblur="calculateTotRec()"/>
                <label id="msgDisAmount" class="msgTextField"></label>
            </div>
            <div class="col-md-3">Total Receivable Amount </div>
            <div class="col-md-3"><input type="text" name="totalReceivable" value="0.00" readonly  id="txtTotalReceible" style="width: 100%"/>
                <label id="msgTotalReceible" class="msgTextField"></label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3" id="txtChangeTxt2">Total Paid</div>
            <div class="col-md-3"><input type="text" name="totalPaid" readonly value="${totalPaid}"  id="txtTotalPaid" style="width: 100%"/>
                <label id="msgDisAmount" class="msgTextField"></label>
            </div>
        </div>


        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-3"><input type="button" id="btnPrintRebate" value="Print" class="btn btn-default col-md-12" onclick="printRabate()"/></div>
            <div class="col-md-3"><input type="button" id="btnCalRebate" value="Calculate" class="btn btn-default col-md-12" onclick="calculateRabate()"/></div>
            <div class="col-md-3"><input type="button" id="btnSaveRabate" value="Rebate" class="btn btn-default col-md-12" onclick="save()"/></div>
        </div>
        <input type="hidden" name="loanId" value="${loanId}"/>
        <input type="hidden" name="rebateId" value=""/>
        <input type="hidden" name="rebateCode" value=""/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
    <form name="loanRescheduleForm" id="loanReschedule">
        <input type="hidden" name="loanBalance" id="txtSchLoanBalance"/>
        <input type="hidden" name="remainBalance" id="txtSchRemainBalance"/>
        <input type="hidden" name="remainInstallments" id="txtSchRemainInstallments"/>
        <input type="hidden" name="paidInstallments" id="txtSchPaidInstallments"/>
        <input type="hidden" name="remainInterest" id="txtSchRemainInterest"/>
        <input type="hidden" name="rescheduleBalance" id="txtSchescheduleBalance"/>
        <input type="hidden" name="loanId" id="txtSchLoanId" value="${loanId}"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>

    <form id="submitRebateLoan" method="GET" action="/AxaBankFinance/ReportController/rebateEarlyTermination" target="blank" >
        <input type="hidden" name="loanId" value="${loanId}"/>
        <input type="hidden" name="remainingInterest" value="${interest}"/>
        <input type="hidden" name="discountRate" id="txtRebateRate2" value="0.00"/>
        <input type="hidden" name="discountAmount" value="0.00"  id="txtDisAmount2"/>
        <input type="hidden" name="rebateAmount" value="0.00" id="txtRebtAmount2"/>
        <input type="hidden" name="charge" id="txtRebtCharge2" value="0.00" />
        <input type="hidden" name="totalReceivable" value="0.00" id="txtTotalReceible2"/>
        <input type="hidden" name="totalPaid" value="${totalPaid}"  id="txtTotalPaid"/>
        <input type="hidden" name="totalBalance" id="txtTotalBalance2" value="0.00"/>
        <input type="hidden" name="remainingBalance" id="txtReBalance" value="${remInsBalance}"/>
    </form>

</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>

    var processType = 1; // whether Rebate/Fully Paid/Re-schedule

    $(function () {
        var rebateBal = $("#txtTotalBalance").val();
        $('#btnSaveRabate').prop('disabled', true);
        $('#txtDisAmount').prop('disabled', true);
        $('#txtTotalReceible').prop('disabled', true);
        $('#txtRebtAmount').prop('disabled', true);
        $('#txtRebtCharge').prop('disabled', true);
        $('#btnPrintRebate').prop('disabled', true);
        if (rebateBal <= 0) {
            $('#btnCalRebate').prop('disabled', true);
            $('#txtRebateRate').prop('disabled', true);
            $('#btnSaveRabate').prop('disabled', true);
        }
    });
    $('input[name=discountRate]').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
            calculateRabate();
            $('#btnCalRebate').prop('disabled', true);
        }
    });
    function calculateRabate() {
        var reInterest = $("#txtReInterest").val();
        var totalBalance = $("#txtTotalBalance").val();
        var rebateRate = document.getElementById('txtRebateRate').value;
        var rebateCharge = document.getElementById('txtRebtCharge').value;
        var disAmount = (reInterest * rebateRate) / 100;
        var rebateAmount = totalBalance - disAmount;
        $('#txtDisAmount').val(disAmount.toFixed(2));
        $('#txtDisAmount2').val(disAmount.toFixed(2));
        $('#txtRebtAmount').val(rebateAmount.toFixed(2));
        $('#txtRebtAmount2').val(rebateAmount.toFixed(2));
        $('#btnSaveRabate').prop('disabled', false);
        $('#txtDisAmount').prop('disabled', false);
        $('#txtTotalReceible').prop('disabled', false);
        $('#txtRebtAmount').prop('disabled', false);
        $('#txtRebtCharge').prop('disabled', false);
        $('#btnPrintRebate').prop('disabled', false);
        $('#txtRebateRate2').val(rebateRate);
        $('#txtRebtCharge2').val(rebateCharge);
        $('#txtTotalBalance2').val(totalBalance);


    }

    function calculateTotRec() {
        var rebtAmount = Number($("#txtRebtAmount").val());
        var rebtCharge = Number($("#txtRebtCharge").val());
        var totRec = rebtAmount + rebtCharge;
        $('#txtTotalReceible').val(parseFloat(totRec).toFixed(2));
        $('#txtTotalReceible2').val(parseFloat(totRec).toFixed(2));
    }

    function save() {
        if (fieldValidate()) {
            if (processType === 1) {
                saveRebateLoan();
            } else {
                fillLoanRescheduleForm();
                saveRescheduleLoan();
            }
        }
        processType = null;
    }

    function saveRebateLoan() {
        $("#btnSaveRabate").disabled = true;
        $.ajax({
            url: '/AxaBankFinance/SettlementController/saveRebateLoan',
            type: 'POST',
            data: $('#rebateLoan').serialize(),
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                if (view == "rebateLoanViewMain") {
                    setTimeout(function () {
                        loadRebateLoanList();
                    }, 1500);
                } else {
                    setTimeout(function () {
                        loanRebateLoan();
                    }, 1500);
                }
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function saveRescheduleLoan() {
        $("#btnSaveRabate").disabled = true;
        $.ajax({
            url: '/AxaBankFinance/SettlementController/saveOrUpdateLoanRescheduleDetails',
            type: 'POST',
            data: $('#loanReschedule').serialize(),
            success: function (data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                if (view === "rebateLoanViewMain") {
                    setTimeout(function () {
                        loadRebateLoanList();
                    }, 1500);
                } else {
                    setTimeout(function () {
                        loanRebateLoan();
                    }, 1500);
                }
            },
            error: function () {
                alert("Error Loding..");
            }
        });
    }

    function clickToFullyPaid(type) {
        processType = type;
        $('#txtRebateRate').val(0);
        $("#txtRebateRate").removeClass("txtError");
        $('#txtRebateRate').prop('disabled', true);
        $('#txtRebtAmount').val(0.00);
        $('#txtRebtAmount').prop('disabled', true);
        $('#txtRebtCharge').val(0.00);
        $("#txtRebtCharge").prop('disabled', true);
        $('#txtDisAmount').val(0.00);
        $("#txtDisAmount").prop('disabled', true);
        $("#txtTotalReceible").val($("#txtTotalBalance").val());
        $("#btnSaveRabate").val("Fully Paid");
        document.getElementById('btnPrintRebate').style.visibility = 'hidden';
        document.getElementById('btnPrintRebate').style.visibility = 'hidden';
    }
    function clickToRebate(type) {
        processType = type;
        $('#txtRebateRate').val("");
        $("#txtRebateRate").removeClass("txtError");
        $('#txtRebateRate').prop('disabled', false);
        $('#txtChangeTxt1').text("Rebate Amount");
        $('#txtChangeTxt2').text("Rebate Charge");
        $('#txtRebateRate').val("");
        $('#txtRebateRate').val(0.00);
        $("#btnSaveRabate").val("Rebate");
        document.getElementById('btnPrintRebate').style.visibility = 'visible';
        $('#btnPrintRebate').prop('disabled', true);
    }
    function clickToReschedule(type) {
        processType = type;
        $('#txtRebateRate').val("100");
        $("#txtRebateRate").removeClass("txtError");
        $('#txtRebateRate').prop('disabled', true);
        $('#txtChangeTxt1').text("Reschedule Amount");
        $('#txtChangeTxt2').text("Reschedule Charge");
        $('#txtDisAmount').val(0.00);
        $("#txtDisAmount").prop('disabled', true);
        $("#txtRebtAmount").val($("#txtReBalance").val());
        $("#txtTotalReceible").val("0.00");
        $('#txtRebtCharge').val("0.00");
        $("#txtRebtCharge").prop('disabled', false);
        $("#btnSaveRabate").val("Re-Schedule");
    }
    function fieldValidate() {
        var rate = document.getElementById('txtRebateRate');
        var discountAmount = document.getElementById('txtDisAmount');
        var totalReceivable = document.getElementById('txtTotalReceible');
        if (rate.value === null || rate.value === "") {
            $("#msgRebateRate").html("Rebate Rate must be filled out");
            $("#txtRebateRate").addClass("txtError");
            $("#txtRebateRate").focus();
            return false;
        } else {
            $("#txtRebateRate").removeClass("txtError");
            $("#msgRebateRate").html("");
            $("#msgRebateRate").removeClass("msgTextField");
        }
        if (discountAmount.value === null || discountAmount.value === "") {
            $("#msgRebateValue").html("Calculate Rebate");
            $("#txtRebateValue").addClass("txtError");
            $("#txtRebateValue").focus();
            return false;
        } else {
            $("#txtRebateValue").removeClass("txtError");
            $("#msgRebateValue").html("");
            $("#msgRebateValue").removeClass("msgTextField");
        }

        if (totalReceivable.value === null || totalReceivable.value === "") {
            $("#msgTotalReceible").html("Rebate Calculation Fail");
            $("#txtTotalReceible").addClass("txtError");
            $("#txtTotalReceible").focus();
            return false;
        } else {
            $("#txtTotalReceible").removeClass("txtError");
            $("#msgTotalReceible").html("");
            $("#msgTotalReceible").removeClass("msgTextField");
        }
        return true;
    }

    function fillLoanRescheduleForm() {
        $("#txtSchLoanBalance").val($("#txtTotalBalance").val());
        $("#txtSchRemainBalance").val($("#txtReBalance").val());
        $("#txtSchRemainInstallments").val($("#txtReInstalment").val());
        $("#txtSchPaidInstallments").val($("#txtPaidInstalment").val());
        $("#txtSchRemainInterest").val($("#txtReInterest").val());
        $("#txtSchescheduleBalance").val($("#txtTotalReceible").val());
    }

    function printRabate() {
//        $.ajax({
//            url: '/AxaBankFinance/ReportController/rebateEarlyTermination',
//            type: 'GET',
//            data: $('#submitRebateLoan').submit()
//        });
        $("#submitRebateLoan").submit();
    }

</script>





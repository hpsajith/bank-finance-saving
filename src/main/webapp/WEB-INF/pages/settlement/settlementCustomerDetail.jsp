<%-- 
    Document   : newSettlementForm
    Created on : Mar 17, 2015, 11:00:22 AM
    Author     :Sajith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
         $(".box").draggable();
    });
</script>
    <div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;" id="form_">

            <div class="col-md-12 col-lg-12 col-sm-12">
<!--            <div class="row divError"></div>-->
            <div class="row" ><label>Add Payment</label></div>    
               <div class="col-md-12 col-lg-12 col-sm-12">
                   <div class="row">
                       <div class="col-md-2 col-lg-2 col-sm-2">Search</div>
                       <div class="col-md-1 col-lg-1 col-sm-1"><input type="button" class="btn btn-default searchButton" id="btnSearchCustomer" style="height: 25px"></div>
                   </div>
                    <div class="row">   
                        <div class="col-md-2 col-lg-2 col-sm-2">Customer Code</div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input readonly type="text" style="width: 100%" id="customerCode"/></div>                        

                        <div class="col-md-2 col-lg-2 col-sm-2" style="">Name</div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" id="debtorName"/></div>
                        
                        <div class="col-md-2 col-lg-2 col-sm-2" style="">NIC</div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" id="debtorNic" /></div>
                    </div>
                   <div class="row">                        
                        <div class="col-md-2 col-lg-2 col-sm-2">Contact No</div>
                        <div class="col-md-2 col-lg-2 col-sm-2" style=""><input readonly type="text" style="width: 100%" id="debtorTelephone"/></div>
                        
                        <div class="col-md-2 col-lg-2 col-sm-2">Address</div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><textarea readonly name="" id="debtorAddress" style="width: 100% "></textarea></div>   

                        <input type="hidden" style="width: 100%" id="LoanId"/>
                        <input type="hidden" id="debID" name="debtorId"/>
                        <input type="hidden" id="gurant_debtr_type" />
                    </div>
               </div>
   
            </div>    

        <div id="loanDetailformContent" >  
        </div>
        <div id="loanSettlementContent" >   
        </div>
    </div>
            
            
    <!--Search Customer-->
    <div class="searchCustomer" style="display:none; height: 90%; width: 70%; margin-top: 1%;margin-left: -1%;overflow-y: auto" id="popup_searchCustomer">     
    </div>       
            
    <script type="text/javascript">
    
    $(document).ready(function() {
        //Block Laod GuarantorForm(Load SearchCustomer Form)    
        $('#btnSearchCustomer').click(function() {
            ui('#popup_searchCustomer');
            loadSearchCustomer(1);
        });
       
    });
 
    
        function loadSearchCustomer(type) {
           $("#gurant_debtr_type").val(type);
            $.ajax({
                url: '/AxaBankFinance/SettlementController/searchCustomer',
                success: function(data) {
                    $('#popup_searchCustomer').html(data);

                },
                error: function() {
                    alert("Error Loading...2");
                }
            });
        }
 
    function unloadSearchLoanCustomer() {
        unblockui();
    }
    </script>

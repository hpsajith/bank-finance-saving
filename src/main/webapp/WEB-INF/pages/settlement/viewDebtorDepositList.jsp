<%-- 
    Document   : viewDebtorDepositList
    Created on : Apr 22, 2016, 12:19:38 PM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="col-md-12 col-sm-12 col-lg-12" style="width: 100%">
    <div class="row" style="margin: 0 8 2 0"><legend>Debtor Deposit List</legend></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12" id="divViewDeposit">
            <div class="row" style="overflow-y: auto;height: 85%">
                <table id="tblDebtorDeposit" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="text-align: center">Account No</th>
                            <th style="text-align: center">Deposit Amount</th>
                            <th style="text-align: center">Deposit Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="debtorDeposit" items="${debtorDeposits}">
                            <tr id="${debtorDeposit.depositId}" onclick="clickDebtorDeposit(this)">
                                <td style="text-align: center">${debtorDeposit.accountNo}</td>
                                <td style="text-align: center">${debtorDeposit.depositAmount}</td>
                                <td style="text-align: center">${debtorDeposit.depositDate}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="dc_clear"></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-7"></div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-6">
                    <input type="button" value="Cancel" class="btn btn-default" style="width: 100%" id="btnCancel" onclick="unblockui()"/>
                </div>
                <div class="col-md-6">
                    <input type="button" value="Select" class="btn btn-default" style="width: 100%" id="btnSave" onclick="selectLoanCheque()"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var debtorDeposit = null;
    function selectLoanCheque() {
        if (debtorDeposit !== null) {
            $("#Amount").val(debtorDeposit[1]);
            $("#amountText").val(toWords(debtorDeposit[1]));
            unblockui();
        }
    }

    function clickDebtorDeposit(tr) {
        var selected = $(tr).hasClass("highlight");
        $("#tblDebtorDeposit tbody tr").removeClass("highlight");
        if (!selected) {
            $(tr).addClass("highlight");
        }
        var tableRow = $(tr).children("td").map(function() {
            return $(this).text();
        }).get();
        debtorDeposit = tableRow;
    }
</script>
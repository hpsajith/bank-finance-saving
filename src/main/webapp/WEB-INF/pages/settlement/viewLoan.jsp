<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<h4></h4>
<h6></h6>
<div class="container-fluid">
    <div class="col-md-12 col-lg-12 col-sm-12">
<div class="row" style="overflow-y: auto;">
    <!--<div class="col-md-4">-->
    <input type="button" class="btn btn-default" id="showloanDetail" value="Loan in Detail" />
    
    <div class="col-md-12 col-lg-12 col-sm-12" id="contentLoanDet" style="display:none;">
        <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2">Loan Amount</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanAmount}"/></div>            

            <div class="col-md-2 col-lg-2 col-sm-2">Down Payment</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanDownPayment}"/></div>

            
            <div class="col-md-2 col-lg-2 col-sm-2">Due Date</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanDueDate}"/></div>
        </div>
        <div class="row">    
            <div class="col-md-2 col-lg-2 col-sm-2">Loan Rate</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanInterestRate}"/></div>        
            
            <div class="col-md-2 col-lg-2 col-sm-2">Invesment</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanInvestment}"/></div>
        
            <div class="col-md-2 col-lg-2 col-sm-2">Loan Process Date</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanDate}"/></div>
         </div>
        <div class="row"> 
            
            <div class="col-md-2 col-lg-2 col-sm-2">Interest per Ins</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanInterest}"/></div>
            
            <div class="col-md-2 col-lg-2 col-sm-2">Period</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanPeriod}"/></div>
                    
            <div class="col-md-2 col-lg-2 col-sm-2">Loan End Date</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly  style="width: 100%" name="" value="${loanEndDate}"/></div>

        </div>
        <div class="row"> 
            <div class="col-md-2 col-lg-2 col-sm-2">Agreement No</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanAgreementNo}"/></div>
            
            <div class="col-md-2 col-lg-2 col-sm-2">Installment</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanInstallment}"/></div>
       
            <div class="col-md-2 col-lg-2 col-sm-2">Period Type</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${LoanPeriodType}"/></div>
        </div>
        <div class="row"> 
            <div class="col-md-2 col-lg-2 col-sm-2">Member No</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${loan.loanBookNo}"/></div>
           
        </div>
        <div class="row" style="border-top: 1px solid #222; padding-top: 5px; margin-top: 5px; ">
            <div class="col-md-2 col-lg-2 col-sm-2">Total</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" readonly style="width: 100%" name="" value="${Total}"/></div>
        </div>
        <div class="row" style="border-top: 1px solid #222; padding-top: 5px; margin-top: 5px; "></div>
    </div>

        <!--<div id="editCustomer" hidden="true" style="margin: 5 10 2 2;overflow-y: auto;height: 50%; "></div>-->
    </div>
    </div>
</div>


<script type="text/javascript">
    
    var clickCount = 0;
    $('#showloanDetail').click(function(){        
        if(clickCount === 0){
            $("#contentLoanDet").css({'display': 'block'});
            clickCount++;
        }else{
            $("#contentLoanDet").css({'display':'none'});
            clickCount = 0;
        }
        
    });
</script>

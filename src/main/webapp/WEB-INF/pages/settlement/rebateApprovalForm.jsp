<%-- 
    Document   : rebateApprovalForm
    Created on : Jan 25, 2016, 4:08:57 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>${title}</strong>
       
    </div>
    <form name="rebateApprovalForm" id ="rebateApproval">
        <div class="col-md-12">
            <label id="lblapproveTitle"></label>
            <input type="hidden" name="rebateAppId" id="" value="${approval.rebateAppId}"/>
            <input type="hidden" name="rebateLoanId" id="txtRebateLoanId" value="${loanID}"/>
            <input type="hidden" name="approvalStatus" id="txtApprovalStatus" value="${type}"/>
            <div class="row" style="padding: 5px;">
                <c:choose>
                    <c:when test="${approval.rebateAppId>0}">
                        <c:choose>
                            <c:when test="${approval.isReject == 1}">
                                <div class="col-md-2"></div>
                                <div class="col-md-1"><input type="radio" name="isReject" value="1" checked="true"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="isReject" value="0" /></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-2"></div>
                                <div class="col-md-1"><input type="radio" name="isReject" value="1"/></div>
                                <div class="col-md-3"><strong>Approve</strong></div>
                                <div class="col-md-1"><input type="radio" name="isReject" value="0" checked="true"/></div>
                                <div class="col-md-3"><strong>Reject</strong></div>
                            </c:otherwise>                    
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-2"></div>
                        <div class="col-md-1"><input type="radio" name="isReject" value="1"/></div>
                        <div class="col-md-3"><strong>Approve</strong></div>
                        <div class="col-md-1"><input type="radio" name="isReject" value="0" /></div>
                        <div class="col-md-3"><strong>Reject</strong></div>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-2">Comment</div>
                <div class="col-md-10"><textarea rows="3" name="approvalComments" style="width: 100%">${approval.approvalComments}</textarea></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="row" style="margin-top: 15px">  
                <c:choose>
                    <c:when test="${userType==1}">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                            <c:choose>
                                <c:when test="${approval.rebateAppId>0}">
                                <div class="col-md-4"><input type="button"  value="Update" class="btn btn-default col-md-12" onclick="submitRebateApproveLevels()"></div>
                                </c:when>
                                <c:otherwise>
                                <div class="col-md-4"><input type="button"  value="Save" class="btn btn-default col-md-12" onclick="submitRebateApproveLevels()"></div>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                        <div class="col-md-8"><label class="redmsg">You don't have privileges to Approve this Rebate..! please Contact Administrator.</label></div>
                        <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                        </c:otherwise>
                    </c:choose>   
            </div>
        </div>  
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div> 
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>
    function submitRebateApproveLevels() {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/saveRebateApproveLevels',
            type: 'POST',
            data: $("#rebateApproval").serialize(),
            success: function(data) {
                $('#msgDiv').html(data);
                ui("#msgDiv");
                setTimeout(function() {
                    loadRebateApprovalList();
                }, 2000);
            },
            error: function() {
                alert("Error Loading...");
            }
        });
    }
</script>
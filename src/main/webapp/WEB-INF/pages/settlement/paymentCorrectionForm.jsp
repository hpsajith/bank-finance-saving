<%-- 
    Document   : paymentCorrectionForm
    Created on : Jul 20, 2015, 11:13:03 AM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<h1>Payment Remove !</h1>
<form name="printCorrectionForm" id="printCorrectionForm"  target="blank" action="/AxaBankFinance/SettlementController/printCorrectionReceipt" method="GET">
    <input type="hidden" value="${paymentDetail.receiptNo}" name="receiptNo" id="receiptNo" >
</form>
<c:choose>
    <c:when test="${validation}">
        <form name="newPaymentCorrectionForm" id="newPaymentCorrectionForm">
            <input type="hidden" name="transactionId" id="transactionId" value="${paymentDetail.transactionId}"/> 
            <div class="row">
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
                <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Receipt No</div>
                <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" readonly="true" style="width:100%" value="${paymentDetail.receiptNo}"/></div>   
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
            </div> 
            <div class="row" >
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
                <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Paid Amount</div>
                <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" readonly="true" style="width:100%" value="${paymentDetail.paymentAmount}"/> </div>   
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
            </div> 
            <div class="row" >
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
                <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Paid Amount in Text</div>
                <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" readonly="true" style="width:100%" value="${paymentDetail.amountText}"/> </div>   
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
            </div> 
            <div class="row" >
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
                <div class="col-md-3 col-sm-3 col-lg-3" style="text-align: left">Add Reason *</div>
                <div class="col-md-7 col-sm-7 col-lg-7"><input type="text" id="reason" name="reason" style="width:100%"></div>
                <div class="col-md-1 col-sm-1 col-lg-1"></div>
            </div>    
            <label id="msgReason" class="msgTextField"></label> 
        </form>
        <div class="row" >
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="col-md-12">
                    <div class="col-md-4"><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
                    <div class="col-md-4">
                        <c:choose>
                            <c:when test="${beforeDate}">
                                <input type="button"  value="Done" class="btn btn-success col-md-12 col-sm-12 col-lg-12" onclick="savePaymentRemove()" disabled style="cursor: not-allowed"/>
                            </c:when>
                            <c:otherwise>
                                <input type="button"  value="Done" class="btn btn-success col-md-12 col-sm-12 col-lg-12" onclick="savePaymentRemove()" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-md-4"><input type="button" value="Print Receipt" class="btn btn-info col-md-12 col-sm-12 col-lg-12" onclick="printReceipt()"/></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="row" >
            <h4>You don't have privileges...!</h4>
            <div  class="col-md-3 "><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
            <div  class="col-md-3 "></div>
        </div>
    </c:otherwise>
</c:choose>   
<script type="text/javascript">

    function savePaymentRemove() {

        $.ajax({
            url: '/AxaBankFinance/checkIsBlock',
            success: function(data) {
                if (!data) {
                    var validation = false;
                    validation = validateForm();
                    if (validation) {
                        $.ajax({
                            url: '/AxaBankFinance/SettlementController/savePaymentRemove',
                            type: 'GET',
                            data: $("#newPaymentCorrectionForm").serialize(),
                            success: function(data) {
                                $('#divProcess').html("");
                                $('#msgDiv').html(data);
                                ui('#msgDiv');
                                loadPayments();
                            },
                            error: function() {
                                alert("Error Loading...");
                            }
                        });
                    } else {
                        $(".divError").html("");
                        $(".divError").css({'display': 'block'});
                        $("#msgReason").html(errorMessage);
                        errorMessage = "";
                    }

                } else {
                    warning("User Is Blocked", "message_sett_form");
                }
            },
            error: function() {
                alert("Connection is refuced by the server...");
            }
        });
    }

    function printReceipt() {
        $("#printCorrectionForm").submit();
    }

    var errorMessage = "";
    function validateForm() {
        var status = true;
        var reason = $('input[name=reason]').val();
        if (reason === null || reason === "") {
            status = false;
            errorMessage = "Enter the reason";
            $("#reason").addClass("txtError");
        } else {
            $("#reason").removeClass("txtError");
            $("#msgReason").html("");
        }

        return status;
    }

    function loadPayments() {

        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadCorrectPayment',
            success: function(data) {
                $('#formContent').html("");
                $('#formContent').html(data);
            },
            error: function() {
            }
        });
    }
</script>
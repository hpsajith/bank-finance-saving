<%-- 
    Document   : addSettlementTransfer
    Created on : Aug 29, 2016, 1:40:30 PM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<div class="col-md-12">
    <div class="row" style="text-align: left; padding: 5px; background: #C8C8C8; border: 1px solid #9d9d9d; border-radius: 4px;">
        <strong>Add Transfer</strong>
    </div>
    <form name="settlementTransferForm" id="addSettlementTransferForm">
        <input type="hidden" id="txtTransferId" name="transferId" value="${transferId}"/>
        <input type="hidden" id="txtTransfereeId" name="transfereeId" />
        <div class="col-md-12">
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3" style="text-align: left">Over Payment</div>
                <div class="col-md-9">
                    <input type="text" id="txtOverPayment" value="${overPayment}" readonly style="width: 100%"/>
                    <label id="msgTxtSubject" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 5px;">
                <div class="col-md-3" style="text-align: left">Amount</div>
                <div class="col-md-9">
                    <input type="text" id="txtTransferAmount" name="transferAmount" value="0.00" style="width: 100%"/>
                    <label id="msgTxtSubject" class="msgTextField"></label>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-md-3" style="text-align: left">Transferee</div>
                <div class="col-md-9">
                    <input type="text" id="txtTransferee" placeholder="Enter Vehicle No" style="width: 100%" onkeyup="searchTransferee()"/>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <select id="transferee_suggestions" style="width: 100%" disabled>
                    </select>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">  
                <div class="col-md-4"></div>
                <div class="col-md-4"><input type="button"  value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"></div>
                <div class="col-md-4"><input type="button"  value="Transfer" class="btn btn-default col-md-12" onclick="addSettlementTransfer()"></div>
            </div>
        </div>  
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div> 
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">

    var loanId = '${transferId}';

    function formValidate() {
        return true;
    }

    function addSettlementTransfer() {
        if (formValidate()) {
            $.ajax({
                url: "/AxaBankFinance/SettlementController/addSettlementTransfer",
                type: 'POST',
                data: $("#addSettlementTransferForm").serialize(),
                success: function (data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function () {
                        loadSettlement(loanId);
                        unblockui();
                    }, 400);
                },
                error: function () {
                    alert("Error Loding..");
                }
            });
        }
    }

    function searchTransferee() {
        var transferee = $("#txtTransferee").val().trim();
        if (transferee.length > 0) {
            $.ajax({
                url: "/AxaBankFinance/findVehicleByRegNo",
                type: 'GET',
                data: {"vehicleRegNo": transferee},
                success: function (data) {
                    if (data.length > 0) {
                        var suggesstion = "<option value='0' selected>--SELECT--</option>";
                        for (var i = 0; i < data.length; i++) {
                            suggesstion = suggesstion + "<option value='" + data[i].loanId + "' onclick='selectTransferee(" + data[i].loanId + ")'>" + data[i].vehicleRegNo + "</option>";
                        }
                        $("#transferee_suggestions").html("");
                        $("#transferee_suggestions").html(suggesstion);
                        $("#transferee_suggestions").removeAttr("disabled");
                    } else {
                        $("#transferee_suggestions").html("");
                        $("#transferee_suggestions").attr("disabled", true);
                    }
                }
            });
        } else {
            $("#transferee_suggestions").html("");
            $("#transferee_suggestions").attr("disabled", true);
        }
    }

    function selectTransferee(loanId) {
        if (loanId !== "0") {
            $("#txtTransfereeId").val(loanId);
            $("#txtTransferee").val("");
        } else {
            alert("Select Vehicle No");
        }
    }

    function validateTransferAmount() {}

    function loadSettlement(loanid) {
        var LoanId = loanid;
        $.ajax({
            url: '/AxaBankFinance/SettlementController/addSettlement/' + LoanId,
            success: function (data) {
                $('#loanSettlementContent').html(data);
            },
            error: function () {
                alert("Error Loading...loanSettlementContent");
            }
        });
    }

</script>
<%-- 
    Document   : checkPassWord
    Created on : Aug 14, 2015, 3:46:59 PM
    Author     : ITESS
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<h3>Need Valid Privileges...!!!</h3>

<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>

    <div class="col-md-12 col-lg-12 col-sm-12" style="display:block;">
        
        <input type="hidden" name="" id="remark" value="${remark}"/>
        <input type="hidden" name="" id="amount" value="${amount}"/>
        <input type="hidden" name="" id="loanId" value="${loanId}"/>
        <label id="msgUname" class="msgTextField"></label>
        <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2">Username</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="text" value="" name="uname" id="uname" ></div>
        </div>  
        <label id="msgPword" class="msgTextField"></label>
        <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2">Password</div>
            <div class="col-md-2 col-lg-2 col-sm-2"><input type="password" value="" name="pword" id="pword" ></div>
        </div> 

        <label id="msgPriv" class="msgTextField"></label>
        <div  class="col-md-3 "><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>    
        <div  class="col-md-3 "><input type="button"  onclick="checkPriviledges()" class="btn btn-success col-md-12 col-sm-12 col-lg-12" value="Check"/></div>    

    </div>
</div>
<script>
//
//    function checkPriviledges() {
//
//        var validForm = validateForm();
//        var msg = "This User Don't Have Priviledges";
//        var uname = 
//        var pword =
//        if (validForm) {
//            $.ajax({
//                url: '/AxaBankFinance/SettlementController/checkPriviledges',
//                data: {"uname": uname, "pword": pword},
//                success: function(data) {
//                    if (data) {
//                        $("#msgPriv").html("");
//                        unblockui()
//                    } else {
//                        $("#msgPriv").html(msg);
//                    }
//                },
//                error: function() {
//                }
//            });
//        }else{
//             $("#msgUname").html(errorMessage);
//             $("#msgPword").html(errorMessage1);
//            errorMessage= "";
//            errorMessage1= "";
//        }
//    }
//
//
//    var errorMessage = "";
//    var errorMessage1 = "";
//    function validateForm() {
//        var validate = true;
//
//        var uname = $('#uname').val();
//        if (uname === null || uname === "") {
//            validate = false;
//            errorMessage = "Enter User Name ";
//            $("#uname").addClass("txtError");
//        } else {
//            $("#uname").removeClass("txtError");
//        }
//
//        var pword = $('#pword').val();
//        if (pword === null || pword === "") {
//            validate = false;
//            errorMessage1 = "Enter Password     ";
//            $("#pword").addClass("txtError");
//        } else {
//            $("#pword").removeClass("txtError");
//        }
//        return validate;
//    }

function checkPriviledges() {

        var validForm = validateForm();
        var msg = "This User Don't Have Priviledges";
        var uname = $('#uname').val();
        var pword = $('#pword').val();
        var amount = $('#amount').val();
        var loanId = $('#loanId').val();
        var remark = $('#remark').val();
        if (validForm) {
            $.ajax({
                url: '/AxaBankFinance/SettlementController/checkPriviledges',
                data: {"uname":uname,"pword":pword},
                success: function(data) {
                    if (data) {
                        $('#msgPriv').html("");
                        saveAdjCheange(amount, loanId,uname,remark);
                        unblockui();
                    } else {
                         $('#msgUname').html("");
                        $('#msgPword').html("");
                        $('#msgPriv').html(msg);
                        $('#pword').val("");
                    }
                },
                error: function() {
                }
            });
        } else {
            $('#msgUname').html(errorMessage);
            $('#msgPword').html(errorMessage1);
            errorMessage = "";
            errorMessage1 = "";
        }
    }


    var errorMessage = "";
    var errorMessage1 = "";
    function validateForm() {
        var validate = true;

        var uname = $('#uname').val();
        if (uname === null || uname === "") {
            validate = false;
            errorMessage = "Enter User Name ";
            $("#uname").addClass("txtError");
        } else {
            $("#uname").removeClass("txtError");
        }

        var pword = $('#pword').val();
        if (pword === null || pword === "") {
            validate = false;
            errorMessage1 = "Enter Password     ";
            $("#pword").addClass("txtError");
        } else {
            $("#pword").removeClass("txtError");
        }
        return validate;
    }

    function saveAdjCheange(amount, loanId,uname,remark) {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/saveAdj2',
            data: {"amount": amount, "loanId": loanId,"uname":uname,"remark":remark},
            success: function(data) {
              if(data){
                  loadMainPage(loanId);
              }
            }
        });

    }
    
    function loadMainPage(loanId){
          $.ajax({
            url: '/AxaBankFinance/SettlementController/loadMainPage',
            data: {"loanId": loanId},
            success: function(data) {
                 $('#formContentttttt').html(data);
            }
        });
    }
</script>
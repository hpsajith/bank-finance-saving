<%-- 
    Document   : paymentCorrectionTable
    Created on : Aug 2, 2016, 9:33:17 AM
    Author     : User
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row" style="margin-top: 5px" id="payCorrectTable">
    <div class="row fixed-table">
        <div class="table-content">
            <c:choose>
                <c:when test="${message}">        
                    <!--<table class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header" id="tblCustomer" style="margin-top: 10px;">-->
                    <table id="tblCustomer" class="dc_table dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0" >  
                        <thead>
                            <tr>
                                <th style="text-align: center; padding-left: 20px">#</th>
                                <th style="text-align: center">Agreement No</th>
                                <th style="text-align: center">Vehicle No</th>
                                <th style="text-align: center">Account No</th>
                                <th style="text-align: center">Receipt No</th>
                                <th style="text-align: center">Payment Type</th>
                                <th style="text-align: center">Payment Amount</th>
                                <th style="text-align: center">System Date</th>
                            </tr>
                        </thead>
                        <tbody>                    
                            <c:forEach var="payments" items="${paymentList}">
                                <tr>     
                                    <c:choose>
                                        <c:when test="${payments.transactionId!=null}">
                                            <td style="text-align: center; padding-left: 20px">
                                                <input type="checkbox" class="transactionCheckBox" id="transactionCheck${payments.transactionId}" onclick="selectPaymentID(${payments.transactionId})"/>
                                            </td>   
                                        </c:when>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${payments.agreementNo!=null}">
                                            <td style="text-align: center">${payments.agreementNo}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>     
                                    <c:choose>
                                        <c:when test="${payments.vehicleNo!=null}">
                                            <td style="text-align: center">${payments.vehicleNo}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>     
                                    <c:choose>
                                        <c:when test="${payments.debtAccountNo!=null}">
                                            <td style="text-align: center">${payments.debtAccountNo}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>         
                                    <c:choose>
                                        <c:when test="${payments.receiptNo!=null}">
                                            <td style="text-align: center">${payments.receiptNo}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>                                       
                                    <c:choose>
                                        <c:when test="${payments.payType!=null}">
                                            <td style="text-align: center">${payments.payType}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${payments.paymentAmount!=null}">
                                            <td style="text-align: right">${payments.paymentAmount}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${payments.systemDate!=null}">
                                            <td style="text-align: center">${payments.systemDate}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise><label class="delete">No Result Found!!!</label></c:otherwise>
            </c:choose>
        </div>
    </div>
    <c:if test="${paymentList.size() > 0}">
        <div class="row" style="margin-top: 1%">
            <div  class="col-md-3 ">
                <input type="button" id="correctButton" class="btn btn-success col-md-12 col-sm-12 col-lg-12" value="Remove" />
            </div>
        </div>
    </c:if>
</div>
<script type="text/javascript">
    var transactionId;
    var checked = false;
    function selectPaymentID(transId) {
        $('#transactionCheck' + transId).is(':checked');
        $('.transactionCheckBox').prop('checked', false);
        $('#transactionCheck' + transId).prop('checked', true);
        transactionId = transId;
        checked = true;
    }

    $('#correctButton').click(function () {
        if (checked) {
            $.ajax({
                url: '/AxaBankFinance/SettlementController/loadPaymentCorrectionForm/' + transactionId,
                success: function (data) {
                    $('#divProcess').html(data);
                    ui('#divProcess');
                },
                error: function () {
                }
            });
        }
    });
</script>
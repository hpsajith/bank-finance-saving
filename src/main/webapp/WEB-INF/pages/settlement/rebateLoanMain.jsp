<%-- 
    Document   : newSettlementForm
    Created on : Mar 17, 2015, 11:00:22 AM
    Author     :Sajith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;" id="form_">
    <div class="row" ><strong style="font-size: 15px;color: #10383c">Search Loan For Rebate / Fully Paid / Re-Schedule</strong></div>    
    <div class="row">
        <div class="col-md-2 col-lg-2 col-sm-2"><label>Search</label></div>
        <div class="col-md-1 col-lg-1 col-sm-1"><input type="button" class="btn btn-default searchButton" id="btnSearchCustomer" style="height: 25px"></div>
    </div>
    <div class="row">   
        <div class="col-md-2"><label style="display: inline-block; width: 70px; text-align: left">Name</label></div>
        <div class="col-md-3"><input type="text" readonly style="width: 100%" id="debtorName"/></div>
        <div class="col-md-2" align="center"><label style="display: inline-block; width: 70px; text-align: left">NIC</label></div>
        <div class="col-md-3"><input type="text" readonly style="width: 100%" id="debtorNic" /></div>
    </div>
    <div class="row">                        
        <div class="col-md-2"><label style="display: inline-block; width: 70px; text-align: left">Contact No</label></div>
        <div class="col-md-3" style=""><input readonly type="text" style="width: 100%" id="debtorTelephone"/></div>

        <div class="col-md-2" align="center"><label style="display: inline-block; width: 70px; text-align: left">Address</label></div>
        <div class="col-md-5"><textarea readonly name="" id="debtorAddress" style="width: 100% "></textarea></div>   

        <input type="hidden" id="LoanId"/>
        <input type="hidden" id="debID" name="debtorId"/>
        <input type="hidden" id="gurant_debtr_type" />
    </div>


    <div id="loanDetailformContent" ></div>
</div>
<!--// Load Rebate Loan-->
<div id="loadRebateformContent" style="margin-top: 10px"></div>

<!--Search Customer-->
<div class="searchCustomer" style="display:none; height: 85%; width: 70%; margin-top: 1%;overflow-y: auto" id="popup_searchCustomer">     
</div>       

<script type="text/javascript">

    $(document).ready(function() {
        //Block Laod GuarantorForm(Load SearchCustomer Form)    
        $('#btnSearchCustomer').click(function() {
            ui('#popup_searchCustomer');
            loadSearchCustomer(1);
        });

    });


    function loadSearchCustomer(type) {
        $("#gurant_debtr_type").val(type);
        $.ajax({
            url: '/AxaBankFinance/SettlementController/searchCustomer',
            success: function(data) {
                $('#popup_searchCustomer').html(data);


            },
            error: function() {
                alert("Error Loading...2");
            }
        });
    }

    function unloadSearchLoanCustomer() {
        unblockui();
    }
</script>

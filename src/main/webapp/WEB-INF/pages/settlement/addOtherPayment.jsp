<%-- 
    Document   : addOtherPayment
    Created on : Jul 29, 2015, 9:16:28 AM
    Author     : admin
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script src="${cp}/resources/js/toWord.js" type="text/javascript"></script>
<%@ page session="true" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
    });
</script>
<style type="text/css">
    #acounts{float:left;list-style:none;margin:0;padding:0;width:190px;width: 100%}
    #acounts li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;width: 100%}
    #acounts li:hover{background:#D8D8D8; cursor: pointer; width: 100%}
    .divWithScroll{ height:130px;width: 100%;overflow:scroll;overflow-x:hidden;border: #5e5e5e solid;border-width: 2px;}
</style>
<!DOCTYPE html>
<div class="row" style="padding: 20px"> 
    <div class="col-md-10" style="border: 1px solid #737373; margin-top: 20px;">
        <div class="row" style="padding: 10px;"><label style="font-size: 15px;">Other Payments</label></div>
        <form name="printForm" id="printForm"  target="blank" action="/AxaBankFinance/OtherPaymentController/printOtherReceipt" method="GET">
            <input type="hidden" value="" id="otherRecieptNo" name="rNo"/>
        </form>
        <form name="otherPaymentForm" id="otherPayment" >
            <div class="col-md-12 col-lg-12 col-sm-12" style=" border-radius: 2px;">
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="col-md-4 col-lg-4 col-sm-4"></div>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <input type="checkbox" id="txtIsInsurance" value="1"/>
                            <label> Insurance Payment</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <label>Search Account : </label>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <input type="text" placeholder="Search Account Name" id="txtAccountName" onkeyup="findAccount()" style="width: 100%"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <label>Account Name : </label>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <select id="inpAccounts" name="account"  onchange ="onClickAccount()" style="width: 100%">
                                <option value="0">--SELECT--</option>
                                <c:forEach var="acc" items="${chaAccountList}">
                                    <option value="${acc.accountNo}">${acc.accountName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-4">
                            <label>Customer Name</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="cusName" id="txtCusName" value="" style="width: 100%"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-4">
                            <label>Customer Address</label>
                        </div>
                        <div class="col-md-8">
                            <textarea name="cusAddress" id="txtCusAddress" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <label>Payment Type : </label>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <select name="payType" id="payType_" autocomplete="off" onchange="changePayType()" style="width: 100%">
                                <option value="0">--Select Payment Type --</option>
                                <option value="1">Cash</option>
                                <option value="2">Cheque</option>
                                <option value="3">Bank</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="checkdiv">
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <label>Cheque No : </label>
                            </div>
                            <div class="col-md-8 col-lg-8 col-sm-8">
                                <input type="text" id="chequeno_" name="chequeno" style="width: 100%"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <label>Bank : </label>
                            </div>
                            <div class="col-md-8 col-lg-8 col-sm-8">
                                <select name="ChequeBank" id="ChequeBank" autocomplete="off" style="width: 100%">
                                    <option value="0">--Select Bank --</option>
                                    <c:forEach var="v_bank" items="${mBankDetailList}">
                                        <option value="${v_bank.id}"  > ${v_bank.bankName} - ${v_bank.bankBranch} </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <label>Realize Date : </label>
                            </div>
                            <div class="col-md-8 col-lg-8 col-sm-8">
                                <input class="txtCalendar" type="text" id="RealizeDate_" name="RealizeDate" style="width: 100%"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bankdiv">
                    <div class="row" >
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <label>Bank : </label>
                            </div>
                            <div class="col-md-8 col-lg-8 col-sm-8">
                                <select name="Bank" id="Bank" autocomplete="off" style="width: 100%">
                                    <option value="0">--Select Bank --</option>
                                    <c:forEach var="v_bank" items="${mBankDetailList}">
                                        <option value="${v_bank.id}"  > ${v_bank.bankName} - ${v_bank.bankBranch} </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <label>Account No : </label>
                            </div>
                            <div class="col-md-8 col-lg-8 col-sm-8">
                                <input type="text" id="bankaccountno_" name="bankaccountno" style="width: 100%"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <label>Payment Amount : </label>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <input type="text" name="amount" id="pamount" onkeypress="return checkNumbers(this)" style="width: 100%"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <label>Description : </label>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            <textarea name="description" id="descriptionid" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 20px;margin-bottom: 10px;">

                    <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" value="Refresh" class="btn btn-default col-md-12" onclick="loadOtherPayment()"/></div>
                    <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" value="Cancel" class="btn btn-default col-md-12" onclick="pageExit()"/></div>
                    <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" value="Payment" onclick="saveOtherPayment()" class="btn btn-default col-md-12"/></div>
                    <div class="col-md-1 col-sm-1 col-lg-3"></div>
                </div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
            <input type="hidden" name="amountText" id="amountText"/>
        </form>
    </div>
</div>

<div class="row" style="padding: 20px;">
    <div class="col-md-6" style="border: 1px solid #737373; margin-top: 20px;">
        <div class="row" style="padding: 10px;"><label style="font-size: 15px;">Paying Cash</label></div>
        <form id="newVouchForm">
            <div class="row">
                <div class=" col-md-3"><label>Payment Type:</label></div>
                <div class=" col-md-6">
                    <select name="paymentType" class="col-md-12" id="p_catogery" style="width: 100%">
                        <option value="0">SELECT</option>
                        <c:forEach var="vouchCat" items="${voucherCategorys}">
                            <option value="${vouchCat.id}">${vouchCat.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"><label>Description:</label></div>
                <div class="col-md-6"><textarea id="p_description" name="description" style="width: 100%"></textarea></div>
            </div>
            <div class="row">
                <div class="col-md-3"><label>Amount:</label></div>
                <div class="col-md-6"><input type="text" name="amount" id="p_amount" style="width: 100%"/></div>
                <input type="hidden" name="word" id="p_amountword"/>
            </div>
            <div class="row" style="margin-top: 12px;">
                <div class="col-md-4"><input type="button" value="Cancel" class="btn btn-default btn-edit col-md-12" onclick="pageExit()"/></div>
                <div class="col-md-4"><input type="button" value="Save" class="btn btn-default btn-edit col-md-12" onclick="SaveNewVoucher()"/></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
        </form>
    </div>
</div>        



<div id="message_alert" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">
    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
        $("#checkdiv").hide();
        $("#bankdiv").hide();

        $('#pamount').keyup(function (e) {
            var payingAmount = $("#pamount").val();
            var words = toWords(payingAmount);
            $("#amountText").val(words);
        });




    });
    function changePayType() {
        if ($("#payType_").val() == 1) {
            $("#checkdiv").hide();
            $("#bankdiv").hide();
        } else if ($("#payType_").val() == 2) {
            $("#checkdiv").show();
            $("#bankdiv").hide();
        } else if ($("#payType_").val() == 3) {
            $("#checkdiv").hide();
            $("#bankdiv").show();
        }
    }
    function saveOtherPayment() {
        var check = false;
        var checkamount = false;
        var checkl = false;
        if ($("#account_").val() == 0) {
            alert("Please Select an Account Name !");
        } else {
            if ($("#pamount").val().trim() == "") {
                $("#pamount").css("border", "1px solid red");
                document.getElementById('pamount').addEventListener('keyup', function () {
                    $("#pamount").css("border", "1px solid #c0c0c0");
                }, false);
                checkamount = false;
            } else {
                checkamount = true;
            }
            if ($("#payType_").val() == 0) {
                alert("Please Select a Payment Type !");
                check = false;
            } else if ($("#payType_").val() == 1) {
                if (checkamount) {
                    check = true;
                }
            } else if ($("#payType_").val() == 2) {
                if ($("#chequeno_").val().trim() == "") {
                    $("#chequeno_").css("border", "1px solid red");
                    document.getElementById('chequeno_').addEventListener('keyup', function () {
                        $("#chequeno_").css("border", "1px solid #c0c0c0");
                    }, false);
                    check = false;
                } else {
                    check = true;
                }
                if ($("#ChequeBank").val() == 0) {
                    alert("Please Select a Bank !");
                    check = false;
                } else {
                    check = true;
                }
                if ($("#RealizeDate_").val().trim() == "") {
                    $("#RealizeDate_").css("border", "1px solid red");
                    document.getElementById('RealizeDate_').addEventListener('keyup', function () {
                        $("#RealizeDate_").css("border", "1px solid #c0c0c0");
                    }, false);
                    check = false;
                } else {
                    check = true;
                }
            } else if ($("#payType_").val() == 3) {
                if ($("#bankaccountno_").val().trim() == "") {
                    $("#bankaccountno_").css("border", "1px solid red");
                    document.getElementById('bankaccountno_').addEventListener('keyup', function () {
                        $("#bankaccountno_").css("border", "1px solid #c0c0c0");
                    }, false);
                    check = false;
                } else {
                    check = true;
                }
                if ($("#Bank").val() == 0) {
                    alert("Please Select a Bank !");
                    check = false;
                } else {
                    check = true;
                }
            }
            if (check && checkamount) {
                checkl = true;
            }
        }

        if (checkl) {
            $.ajax({
                url: '/AxaBankFinance/OtherPaymentController/save_OtherPayment',
                type: 'POST',
                data: $("#otherPayment").serialize(),
                success: function (data) {
                    $("#message_alert").html(data);
                    ui("#message_alert");
                    var rctNo = $("#hid_pendingstatus").val();
                    $("#otherRecieptNo").val(rctNo);
                    generateRecpt();
                    $("#otherRecieptNo").val("");
                },
                error: function () {
                    alert("Error Loading...");
                }
            });
        }

    }

    function generateRecpt() {
        $("#printForm").submit();
    }

    function SaveNewVoucher() {
        var paymentType = $("#p_catogery").val();
        var amount = $("#p_amount").val();
        var description = $("#p_description").text();
        if (paymentType == 0) {
            alert("Select Type");
            return;
        }
        if (amount == "") {
            alert("Enter Amount");
            return;
        }
        var words = toWords(amount);
        $("#p_amountWord").val(words);
        $.ajax({
            url: '/AxaBankFinance/OtherPaymentController/save_newVoucher',
            type: 'POST',
            data: $("#newVouchForm").serialize(),
            success: function (data) {
                $("#message_alert").html(data);
                ui("#message_alert");
                loadOtherPayment();
            },
            error: function () {
                alert("Error Loading...");
            }
        });

    }

    // new
    function findAccount() {
        var accName = $("#txtAccountName").val().trim();
        var isInsurance = $('#txtIsInsurance:checked').val();
        if (isInsurance == 1) {
            $.ajax({
                url: "/AxaBankFinance/OtherPaymentController/findChtAccountsForOtherInsurance",
                data: {"accName": accName},
                success: function (data) {
                    var suggesstion = "<option value='0'>--SELECT--</option>";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            suggesstion = suggesstion + "<option value='" + data[i].insuCustAccountNo + "' onclick='selectAccount()'>" + data[i].insuFullName + "</option>";
                        }
                        $("#inpAccounts").html("");
                        $("#inpAccounts").html(suggesstion);
                    } else {
                        alert("Can't Find Insurance Account");
                    }
                }
            });
        } else {
            if (accName !== null && accName !== "") {
                $.ajax({
                    url: "/AxaBankFinance/OtherPaymentController/findChtAccountsByName",
                    type: 'GET',
                    data: {"accName": accName},
                    success: function (data) {
                        var suggesstion = "<option value='0'>--SELECT--</option>";
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                suggesstion = suggesstion + "<option value='" + data[i].accountNo + "' onclick='selectAccount()'>" + data[i].accountName + "</option>";
                            }
                            $("#inpAccounts").html("");
                            $("#inpAccounts").html(suggesstion);
                        } else {
                            loadChartOffAccounts();
                        }
                    }
                });
            } else {
                loadChartOffAccounts();
            }
        }
    }

    function loadChartOffAccounts() {
        $.ajax({
            url: "/AxaBankFinance/OtherPaymentController/loadChartOfAccounts",
            type: 'GET',
            success: function (data) {
                var accounts = "<option value='0'>--SELECT--</option>";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        accounts = accounts + "<option value='" + data[i].accountNo + "'>" + data[i].accountName + "</option>";
                    }
                    $("#inpAccounts").html("");
                    $("#inpAccounts").html(accounts);
                }
            }
        });
    }

    function selectAccount() {
        $("#txtAccountName").val("");
    }
    function onClickAccount() {
        var isInsurance = $('#txtIsInsurance:checked').val();
        var accountNo = $("#inpAccounts").find('option:selected').val();
        if (accountNo != null && isInsurance == 1) {
            $.ajax({
                url: "/AxaBankFinance/OtherPaymentController/loadAccountDetails",
                data: {"accName": accountNo},
                success: function (data) {
                    if (data != null) {
                        document.getElementById('txtCusName').value = data.insuFullName;
                        document.getElementById('txtCusAddress').value = data.insuAddress;
                    }
                }
            });
        }
    }





</script>


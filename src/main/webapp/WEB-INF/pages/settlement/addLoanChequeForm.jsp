<%-- 
    Document   : addLoanChequeForm
    Created on : Jan 11, 2016, 11:48:01 PM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div><label class = "successMsg">${successMsg}</label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>
<form name="addLoanChequeForm" id="addLoanCheque">
    <div class="row" style="margin: 0 8 2 0"><legend>Add Loan Cheque</legend></div>
    <div class="row">
        <input type="hidden" name="id" id="txtId"/>
        <input type="hidden" name="loanId" id="txtLoanId"/>
        <div class="col-md-4 col-sm-4 col-lg-4 ">Cheque No</div> 
        <div class="col-md-8 col-sm-8 col-lg-8">
            <input type="text" name="chequeNo" id="txtPdchequeNo" style="width: 100%" onkeyup="checkNumbers(this)"/>
            <label id="msgPdchequeNo" class="msgTextField"></label>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-4 col-lg-4 ">Cheque Amount</div>    
        <div class="col-md-8 col-sm-8 col-lg-8">
            <input type="text" name="chequeAmount" id="txtPdchequeAmount" style="width: 100%" onkeyup="checkNumbers(this)"/>
            <label id="msgPdchequeAmount" class="msgTextField"></label>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-4 col-lg-4 ">Cheque Date</div>    
        <div class="col-md-8 col-sm-8 col-lg-8">
            <input type="text" name="chequeDate" id="txtchequeDate" class="txtCalendar" style="width: 100%" readonly/>
            <label id="msgPdchequeDate" class="msgTextField"></label>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-4 col-lg-4 ">Bank</div>    
        <div class="col-md-8 col-sm-8 col-lg-8">
            <select name="bankId" id="inputBankId" style="width: 100%">
                <option value="0" selected>-- SELECT --</option>
                <c:forEach var="bank" items="${bankList}">
                    <option value="${bank.id}">${bank.bankName}</option>
                </c:forEach>
            </select>
            <label id="msgInputBankId" class="msgTextField"></label>
        </div> 
    </div>    
    <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
        <div  class="col-md-6 col-sm-6 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSave" value="Save" onclick="saveLoanCheque()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">
    $(function() {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd'
        });
    });

    function saveLoanCheque() {
        if (fieldValidate()) {
            document.getElementById("btnSave").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/SettlementController/saveOrUpdateLoanCheque',
                type: 'POST',
                data: $("#addLoanCheque").serialize(),
                success: function(data) {
                    $("#divLoanChequeForm").html(data);
                    document.getElementById("btnSave").disabled = false;
                    setTimeout(function() {
                        loadAddPDcheque();
                    }, 500);
                },
                error: function() {
                    console.log("Error in LoanCheque");
                }
            });
        }
    }

    function fieldValidate() {
        var pdChequeNo = $("#txtPdchequeNo").val();
        var pdChequeAmount = $("#txtPdchequeAmount").val();
        var pdChequeDate = $("#txtchequeDate").val();
        var bankId = $("#inputBankId").find("option:selected").val();
        if (pdChequeNo === "" || pdChequeNo === null) {
            $("#msgPdchequeNo").html("Enter Cheque No");
            $("#txtPdchequeNo").addClass("txtError");
            $("#txtPdchequeNo").focus();
            return false;
        } else {
            $("#msgPdchequeNo").html("");
            $("#txtPdchequeNo").removeClass("txtError");
        }
        if (pdChequeAmount === "" || pdChequeAmount === null) {
            $("#msgPdchequeAmount").html("Enter Cheque Amount");
            $("#txtPdchequeAmount").addClass("txtError");
            $("#txtPdchequeAmount").focus();
            return false;
        } else {
            $("#msgPdchequeAmount").html("");
            $("#txtPdchequeAmount").removeClass("txtError");
        }
        if (pdChequeDate === "" || pdChequeDate === null) {
            $("#msgPdchequeDate").html("Enter Cheque Date");
            $("#txtPdchequeDate").addClass("txtError");
            $("#txtPdchequeDate").focus();
            return false;
        } else {
            $("#msgPdchequeDate").html("");
            $("#txtPdchequeDate").removeClass("txtError");
        }
        if (bankId == 0) {
            $("#msgInputBankId").html("Select Bank");
            $("#inputBankId").addClass("txtError");
            $("#inputBankId").focus();
            return false;
        } else {
            $("#msgInputBankId").html("");
            $("#inputBankId").removeClass("txtError");
        }
        return true;
    }

    function clearForm() {
        $("#txtPdchequeNo").val("");
        $("#txtPdchequeAmount").val("");
        $("#txtchequeDate").val("");
        $("#inputBankId").val("0");
    }
</script>
<%-- 
    Document   : rebateLoanViewMain
    Created on : Jan 5, 2016, 1:38:46 AM
    Author     : IT
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        unblockui();
    });
    
</script>
<div class="row fixed-table" id="divRebateLoanList" style="overflow-y: auto">
    <div class="table-content">
        <table id="tblRebateLoanList" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: center">Loan Id</th>
                    <th style="text-align: center">Agreement No</th>
                    <th style="text-align: center">Balance</th>
                    <th style="text-align: center">Re. Installment</th>
                    <th style="text-align: center">Interest</th>
                    <th style="text-align: center">Rate</th>
                    <th style="text-align: center">Discount</th>
                    <th style="text-align: center">Receivable</th>
                    <th style="text-align: center">Edit</th>
                    <th style="text-align: center">Delete</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="rebateLoan" items="${rebateDetails}">
                    <tr>
                        <td style="text-align: center">${rebateLoan.loanId}</td>
                        <td style="text-align: center">${rebateLoan.loanAggNo}</td>
                        <td style="text-align: right">${rebateLoan.totalBalance}</td>
                        <td style="text-align: center">${rebateLoan.reInstalment}</td>
                        <td style="text-align: right">${rebateLoan.remainingInterest}</td>
                        <td style="text-align: right">${rebateLoan.discountRate}</td>
                        <td style="text-align: right">${rebateLoan.discountAmount}</td>
                        <td style="text-align: right">${rebateLoan.totalReceivable}</td>
                        <td><div class='edit' href='#' onclick="editRebate(${rebateLoan.loanId},${rebateLoan.rebateId},'${rebateLoan.rebateCode}')"></div></td>
                        <td><div class="delete" href='#' onclick="delRebate(${rebateLoan.loanId})"></div></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="dc_clear"></div>
    </div>
</div>
<div id="divRebateForm" class="searchCustomer" style="width: 70%;margin-top: 10%; border: 0px solid #5e5e5e;">
</div>
<div id="message" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">
    function editRebate(loanId,rebateId,rebateCode) {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/addRebateLoanForm/' + loanId,
            success: function(data) {
                $('#divRebateForm').html('');
                $('#divRebateForm').append('<div class="close_div" onclick="unblockui()">X</div>');
                $('#divRebateForm').append(data);
                $('input[name=rebateId]').val(rebateId);
                $('input[name=rebateCode]').val(rebateCode);
                selectedView = "rebateLoanViewMain";
                ui('#divRebateForm');
            },
            error: function() {
               alert("Error Rebate Form Loading..");
            }
        });
    }
    
    function delRebate(loanId){
        $.ajax({
            url: '/AxaBankFinance/SettlementController/deleteConform/' + loanId,
            success: function(data) {
                $('#message').html('');
                $('#message').html(data);
                ui('#message');
            },
            error: function() {
                alert("Error Rebate Delete Conform Form Loading..");
            }
        });        
    }
</script>
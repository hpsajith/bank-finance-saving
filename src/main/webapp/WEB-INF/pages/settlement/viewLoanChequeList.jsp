<%-- 
    Document   : viewLoanChequeList
    Created on : Jan 12, 2016, 4:59:47 AM
    Author     : IT
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="col-md-12 col-sm-12 col-lg-12" style="width: 100%">
    <div class="row" style="margin: 0 8 2 0"><legend>Loan Cheque List</legend></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12" id="divViewLoanType">
            <div class="row" style="overflow-y: auto;height: 85%">
                <table id="tblLoanCheques" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>Cheque No</th>
                            <th>Cheque Amount</th>
                            <th>Cheque Date</th>
                            <th>Bank Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="loanCheque" items="${loanChequesList}">
                            <tr id="${loanCheque.pdChequeId}" onclick="clickLoanCheque(this)">
                                <td>${loanCheque.chequeNo}</td>
                                <td>${loanCheque.remainChequeAmount}</td>
                                <td>${loanCheque.chequeDate}</td>
                                <c:forEach var="bank" items="${bankList}">
                                    <c:if test="${bank.id == loanCheque.bankId}">
                                        <td id="${bank.id}">${bank.bankName}</td>
                                    </c:if>
                                </c:forEach>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="dc_clear"></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-7"></div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-6">
                    <input type="button" value="Cancel" class="btn btn-default" style="width: 100%" id="btnCancel" onclick="unblockui()"/>
                </div>
                <div class="col-md-6">
                    <input type="button" value="Select" class="btn btn-default" style="width: 100%" id="btnSave" onclick="selectLoanCheque()"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var loanCheque = null;
    var pdChequeId = 0;
    var mBankId = 0;
    function selectLoanCheque() {
        if (loanCheque !== null) {
            $("#pdChequeId").val(pdChequeId);
            $("#AccNo").val(loanCheque[0]);
            $("#Amount").val(loanCheque[1]);
            $("#amountText").val(toWords(loanCheque[1]));
            $("#TransDate").val(loanCheque[2]);
            $('#BankId').html("<option value='" + mBankId + "'>" + loanCheque[3] + "</option>");
            //disableLoanChequeFields(false);
            unblockui();
        }
    }

    function clickLoanCheque(tr) {
        var selected = $(tr).hasClass("highlight");
        $("#tblLoanCheques tbody tr").removeClass("highlight");
        if (!selected) {
            $(tr).addClass("highlight");
        }
        var tableRow = $(tr).children("td").map(function() {
            return $(this).text();
        }).get();
        var childrens = $(tr).children("td");
        mBankId = childrens[3].id;
        loanCheque = tableRow;
        pdChequeId = tr.id;
    }
</script>
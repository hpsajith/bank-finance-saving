<%-- 
    Document   : paymentCorrection
    Created on : Jul 20, 2015, 8:37:10 AM
    Author     : ITESS
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        unblockui();
    });
</script>
<c:choose>
    <c:when test="${bySysDate}">
        <h3>${systemDate} (Today) Payments</h3>
    </c:when>
    <c:otherwise>
        <h3>(${sDate}) To (${eDate}) Payments</h3>
    </c:otherwise>
</c:choose>
<div class="row" style="padding: 5px; background: #F2F7FC" id="searchOption"> 
    <div class="row"><label style="margin-left: 20px;">Search Payments</label></div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-4">Start Date</div>
            <div class="col-md-8">
                <input type="text"  class="txtCalendar"  value="${searchByDue}" style="width:100%" name="" id="txtsDate"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">End Date</div>
            <div class="col-md-8">
                <input type="text"  class="txtCalendar"  value="" style="width:100%" name="" id="txteDate"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">
                <input type="button" class="btn btn-default searchButton" id="btnSearchPayments" style="height: 25px" onclick="getPaymentsForDate()">
            </div>
            <div class="col-md-8">
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-4">
            <div class="col-md-4">Agreement No</div>
            <div class="col-md-8">
                <input type="text" value="${agreementNo}" style="width:100%" name="" id="txtAgreementNo" onkeyup="getPaymentsForAgreementNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Vehicle No</div>
            <div class="col-md-8">
                <input type="text" style="width:100%" name="" id="txtVehicleNo" onkeyup="getPaymentsForVehicleNo()"/>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-4">Pay Mode</div>
            <div class="col-md-8">
                <select name="" id="payMode" onchange="getPaymentsForPayMode()" style="width: 100%">
                    <option value="1">Cash</option>
                    <option value="2">Cheque Deposit</option>
                    <option value="3">Direct Bank Deposit</option>                    
                </select>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" id="payCorrectTableDiv"></div>
<div id="divProcess" class="searchCustomer" style="width: 55%;height: 35%; margin: 5% 0%; border: 0px solid #5e5e5e;"></div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<div id="proccessingPage" class="processingDivImage">
    <img id="loading-image" src="${cp}/resources/img/processing.gif" alt="Loading..." />
</div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            url: '/AxaBankFinance/SettlementController/loadCorrectPaymentTable',
            success: function (data) {
                $('#payCorrectTableDiv').html("");
                $('#payCorrectTableDiv').html(data);
            },
            error: function () {
            }
        });
    });

    $(".txtCalendar").datepicker({
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd'
    });

    function getPaymentsForDate() {
        var sDate = $("#txtsDate").val();
        var eDate = $("#txteDate").val();

        if (sDate === "" && eDate === "" || sDate === null && eDate === null) {
        } else {
            $.ajax({
                url: "/AxaBankFinance/SettlementController/loadCorrectPaymentForDate",
                data: {"sDate": sDate, "eDate": eDate},
                success: function (data) {
                    $('#payCorrectTableDiv').html("");
                    $('#payCorrectTableDiv').html(data);
                    unblockui();
                }
            });
        }
    }

    function getPaymentsForAgreementNo() {
        var agreementNo = $("#txtAgreementNo").val().trim();
        if (agreementNo === null || agreementNo === "" || agreementNo === "0") {
        } else {
            if (agreementNo.length >= 8) {
                $.ajax({
                    url: "/AxaBankFinance/SettlementController/loadCorrectPaymentByAgreementNo",
                    data: {"agreementNo": agreementNo},
                    success: function (data) {
                        $('#payCorrectTableDiv').html("");
                        $('#payCorrectTableDiv').html(data);
                        unblockui();
                    }
                });
            }
        }
    }

    function getPaymentsForVehicleNo() {
        var vehicleNo = $("#txtVehicleNo").val().trim();
        if (vehicleNo === null || vehicleNo === "") {
        } else {
            if (vehicleNo.length >= 4) {
                $.ajax({
                    url: "/AxaBankFinance/SettlementController/loadCorrectPaymentByVehicleNo",
                    data: {"vehicleNo": vehicleNo},
                    success: function (data) {
                        $('#payCorrectTableDiv').html("");
                        $('#payCorrectTableDiv').html(data);
                        unblockui();
                    }
                });
            }
        }
    }

    function getPaymentsForPayMode() {
        var payMode = $("#payMode").val();

        if (payMode === null || payMode === "" || payMode === "0") {
        } else {
            $.ajax({
                url: "/AxaBankFinance/SettlementController/loadCorrectPaymentByPayMode",
                data: {"payMode": payMode},
                success: function (data) {
                    $('#payCorrectTableDiv').html("");
                    $('#payCorrectTableDiv').html(data);
                    unblockui();
                }
            });
        }
    }

</script>
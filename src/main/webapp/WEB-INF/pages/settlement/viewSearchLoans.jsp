<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    $(document).ready(function () {
        $("#tblSearchLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "300px",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<div class="col-md-12">
    <div class="row" style=" padding: 10;  background: #F2F7FC;">
        <div class="col-md-3">
            <strong><label>${name}</label></strong>
        </div> 
        <!--        <div class="col-md-4">
                    <strong><span class="fa fa-credit-card"></span>  9015205274V </strong> 
                </div>-->

        <!--        <div class="col-md-5">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <strong> <span class="fa fa-money"></span> </strong>
                    </div>
                </div>-->
    </div>
    <div class="row fixed-table">
        <div class="table-content">
            <c:choose>
                <c:when test="${message}">
                    <!--<table class="table table-bordered table-striped table-hover table-condensed table-edit table-fixed-header" style="margin-top: 10px;">-->
                    <table id="tblSearchLoan" class="dc_table dc_fixed_tables table-bordered">                   
                        <thead>
                            <tr>
                                <th style="text-align: center">Loan Id</th>
                                <th style="text-align: center">Agreement No</th>
                                <th style="text-align: center">Vehicle No</th>
                                <th style="text-align: center">Loan</th>
                                <th style="text-align: center">Period</th>
                                <th style="text-align: center">Rate</th>
                                <th style="text-align: center">Branch</th>
                                <th style="text-align: center">Status</th>
                                <th style="text-align: center">Select</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="loan" items="${loanList}">
                                <tr>
                                    <td style="text-align: center">${loan.loanId}</td>
                                    <td style="text-align: center">${loan.loanAgreementNo}</td>
                                    <td style="text-align: center">${loan.vehicleNo}</td>
                                    <td style="text-align: right">${loan.loanAmount}</td>
                                    <td style="text-align: center">${loan.loanPeriod}</td>
                                    <td style="text-align: center">${loan.loanInterestRate}</td>
                                    <c:forEach var="branch" items="${branchList}">
                                        <c:if test="${branch.branchId == loan.branchId}">
                                            <td style="text-align: center">${branch.branchName}</td>
                                        </c:if>
                                    </c:forEach>
                                    <c:choose>
                                        <c:when test="${loan.loanSpec==0}">
                                            <c:choose>
                                                <c:when test="${loan.loanStatus==0}">
                                                    <td style="color: orange;text-align: center">Processing</td>
                                                </c:when>
                                                <c:when test="${loan.loanStatus==1}">
                                                    <td style="color: green;text-align: center">On Going</td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td style="color: red;text-align: center">Rejected</td>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==1}">
                                            <td style="color: red;text-align: center">Laps</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==2}">
                                            <td style="color: red;text-align: center">Legal</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==3}">
                                            <td style="color: red;text-align: center">Seize</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==4}">
                                            <td style="color: green;text-align: center">Full Paid</td>
                                        </c:when>
                                        <c:when test="${loan.loanSpec==5}">
                                            <td style="color: green;text-align: center">Rebate</td>
                                        </c:when>
                                    </c:choose>          
                                    <td style="text-align: center"><input type="checkbox" class="loanCheckBox" id="loanCheck${loan.loanId}" onclick="selectLoanID(${loan.loanId})"/></td>        
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <label style="font-size: 13px; margin-top: 15px; color: #0075b0">No any Loans</label>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>  

<script>

    var looanId = "";
    var loanID = "";

    function selectLoanID(loanId) {
        looanId = loanId;
        if ($('#loanCheck' + loanId).is(':checked')) {
            $('.loanCheckBox').prop('checked', false);
            $('#loanCheck' + loanId).prop('checked', true);
            $(".btnLoans").prop("disabled", false);
            $("#LoanId").val(looanId);
            loanID = looanId;
        } else {
            alert("Error select : " + looanId);
            $("#LoanId").val(looanId);
        }
    }

</script>        
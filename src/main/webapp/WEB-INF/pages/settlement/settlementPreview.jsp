<%-- 
    Document   : preview
    Created on : Sep 29, 2015, 1:01:22 PM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <table class="dc_table dc_fixed_tables table-bordered" width="50%" border="0" cellspacing="0" cellpadding="0">
        <tbody> 
                <tr>
                    <td>Receipt No</td>
                    <td>${receiptNo}</td>
                </tr>   
                <tr>
                    <td>Remaining Balance</td>
                    <td>${balance}</td>
                </tr> 
                <tr>
                    <td>${settlementPreview.refNoInstallmentCodeDscrpt}</td>
                    <td>${settlementPreview.installmentCodeDscrptAmount}</td>
                </tr>
                <tr>
                    <td>${settlementPreview.refNoOverPayCodeDescrpt}</td>
                    <td>${settlementPreview.overPayAmount}</td>
                </tr>
                <tr>
                    <td>${settlementPreview.refNoDownPymntSuspCodeDscrpt}</td>
                    <td>${settlementPreview.downPymntSuspCodeDscrptAmount}</td>
                </tr>
                <tr>
                    <td>${settlementPreview.refArrearsCodeDscrpt}</td>
                    <td>${settlementPreview.arrearsAmount}</td>
                </tr>
                <tr>
                    <td>${settlementPreview.refNoOtherChargeCodeDscrpt}</td>
                    <td>${settlementPreview.otherChargeCodeDscrptAmount}</td>
                </tr>
                <tr>
                    <td>${settlementPreview.refNoInsuaranceCodeDscrpt}</td>
                    <td>${settlementPreview.insuaranceCodeDscrptAmount}</td>
                </tr>
                <tr>
                    <td>${settlementPreview.refNoODICodeDscrpt}</td>
                    <td>${settlementPreview.paidCurrentOdi}</td>
                </tr>
                 <tr>
                    <td>${settlementPreview.refNoRebateDscrpt}</td>
                    <td>${settlementPreview.rebateAmount}</td>
                </tr>
        </tbody>
    </table>   
</html>

<script>
    $(document).ready(function () {
        pageAuthentication();
        $(".box").draggable();
    });

</script>
<div class="container-fluid" id="searchCustomerContent" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <input type="hidden" id="gurant_debtr_type">
    <div class="row" style="background: #F2F7FC">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3"><label>Search Customer</label></div>
                <div class="col-md-9"></div>
            </div>
            <div class="row" style="margin-top: 10px; padding: 5">
                <div class="col-md-6">
                    <div class="col-md-3" style="text-align: left">Member/Agre.No</div>
                    <div class="col-md-9">
                        <input type="text" style="width:100%" name="" id="searchByLoanNo"/>
                    </div>
                </div>
                <div class="col-md-6" style="text-align: left">
                    <div class="col-md-3">Vehicle No</div>
                    <div class="col-md-9">
                        <input type="text" style="width:100%" name="" id="searchByVehicleNo"/>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 5px; padding: 5">
                <div class="col-md-6" style="text-align: left">
                    <div class="col-md-3">NIC </div>
                    <div class="col-md-9">
                        <input type="text" style="width:100%" name="" id="searchByNic"/>
                    </div>
                </div>
                <div class="col-md-6" style="text-align: left">
                    <div class="col-md-3">Customer</div>
                    <div class="col-md-9">
                        <input type="text" style="width:100%" name="" id="searchByName"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row " id="searchedCustomer" style="height:35%; overflow: auto;">                
    </div>
    <div class="row" id="searchedLoans" style="height: 35%; overflow: auto;">                
    </div>


    <div class="" id="searchButton">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <!--<div class="row">-->
            <div class="row" style="margin-bottom: 10px;margin-top: 10px">
                <div class="col-md-3">
                    <input type="button" class="btn btn-default col-md-12" id="btnSearchCustomerExit" value="Exit" onclick="unblockui()"/>
                </div>
                <div class="col-md-3">
                    <input type="button" class="btn btn-default col-md-12" onclick="clearForm()" value="Clear"/> 
                </div>
                <div class="col-md-4">
                    <input type="button" class="btn btn-default col-md-12" onclick="selectLoan()" value="Payment"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var name, nic, address, telephone, dId = "", customerCode = "", loanID = "", memberNumber = "";
    $(function () {        
        $('#searchByNic').keyup(function (e) {
            var nicNo = $(this).val().trim();
            if (nicNo.length > 0) {
                $.ajax({
                    url: '/AxaBankFinance/SettlementController/searchByNic/' + nicNo,
                    success: function (data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function () {
                        alert("Error Loading...4");
                    }
                });
            }
        });

        $('#searchByName').keyup(function (e) {
            var name = $(this).val().trim();
            if (name.length > 4) {
                $.ajax({
                    url: '/AxaBankFinance/SettlementController/searchByName/' + name,
                    success: function (data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function () {
                        alert("Error Loading...3");
                    }
                });
            }
        });

        $('#searchByLoanNo').keyup(function (e) {
            var loanNo = $(this).val().trim();
            if (loanNo.length > 0) {
                var newLoanNo = loanNo.replace(/\//g, "-");
                $.ajax({
                    url: '/AxaBankFinance/SettlementController/searchByLoanNo',
                    data: {loanNo: newLoanNo},
                    success: function (data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function () {
                        alert("Error Loading");
                    }
                });
            }
        });

        $('#searchByVehicleNo').keyup(function (e) {
            var vehicleNo = $(this).val().trim();
            if (vehicleNo.length >= 4) {
                $.ajax({
                    url: '/AxaBankFinance/SettlementController/searchByVehicleNo',
                    data: {vehicleNo: vehicleNo},
                    success: function (data) {
                        $('#searchedCustomer').html(data);
                    },
                    error: function () {
                        alert("Error Loading");
                    }
                });
            }
        });

    });

    function loadNewCustomer() {
        $.ajax({
            url: '/AxaBankFinance/newCustomer',
            success: function (data) {
                $('#searchCustomerContent').html('');
                $('#searchCustomerContent').append(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }
    function clearForm() {
        $('#newFormContent').html('');
        $('#searchedCustomer').html('');
        $('#searchedLoans').html('');
    }

    function selectLoan() {
        var guarantor_debtor_type = $("#gurant_debtr_type").val();
        var type = "";
        if (guarantor_debtor_type == 1)
            type = "Debtor";
        else
            type = "Guarantor";

        if (dId != null && dId != "") {


            unloadSearchLoanCustomer();
            if (guarantor_debtor_type == 1) {

                $("#debID").val(dId);
                $("#customerCode").val(customerCode);
                $("#debtorNic").val(nic);
                $("#debtorName").val(name);
                $("#debtorAddress").val(address);
                $("#debtorTelephone").val(telephone);            

                var loanid = $("#LoanId").val();
                $.ajax({
                    url: '/AxaBankFinance/SettlementController/viewLoanDetails/' + loanid,
                    success: function (data) {
                        $('#loanDetailformContent').html(data);
                        loadSettlementContent(loanid, dId, customerCode);
                        loadRebateformContent(loanid);

                    },
                    error: function () {
                    }
                });

            } else {
//                $("#guarantorName").val(name);
//                var table = document.getElementById("guranterTable");
//                var rowCount = table.rows.length;
//                $('#guranterTable tbody').append('<tr id="' + dId + '"><td>' +
//                        rowCount + '</td><td>' + name + '</td><td>'  + nic + '</td><td>'+
//                        address + '</td><td>' + telephone + '</td><td>' +
//                        '<div onclick="" class="edit"></div><div class="delete" onclick="deleteRow('+dId+')" ></div>' + '</td></tr>');
//                $("#guarantorIDs").append("<input type='hidden' name='guarantorID' id='g_hid"+dId+"' value='" + dId + "'>");

            }
        } else
            alert("Select " + type);

    }

    function editCustomer(dId) {
        var debtorId = dId;
        $.ajax({
            url: '/AxaBankFinance/editDebtorFrom/' + debtorId,
            success: function (data) {
                ui('#popup_searchCustomer');
                $('#searchCustomerContent').html('');
                $('#searchCustomerContent').append(data);
            },
            error: function () {
                alert("Error Loading...");
            }
        });
    }

    function loadSettlementContent(loanid, debtorId, accNo) {
        var LoanId = loanid;
        $.ajax({
            url: '/AxaBankFinance/SettlementController/addSettlement/' + LoanId,
            success: function (data) {
                $('#loanSettlementContent').html(data);
                checkDebtorDepositCount(debtorId, accNo);
            },
            error: function () {
                alert("Error Loading...loanSettlementContent");
            }
        });
    }

    //load RebateLoanForm jsp
    function loadRebateformContent(loanid) {
        var LoanId = loanid;
        $.ajax({
            url: '/AxaBankFinance/SettlementController/addRebateLoanForm/' + LoanId,
            success: function (data) {
                $('#loadRebateformContent').html(data);
            },
            error: function () {
                alert("Error Loading...Can't View Rebate Form");
            }
        });
    }
    function unLoadForm(dId) {
//        unblockui();        
        editCustomer(dId);
    }

    function deleteRow(dId) {
        $('#' + dId).remove();
        $('#g_hid' + dId).remove();
        $("#guarantorName").val("");
    }
</script>
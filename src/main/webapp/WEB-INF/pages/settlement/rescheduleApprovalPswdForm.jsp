<%-- 
    Document   : rescheduleApprovalPswdForm
    Created on : May 9, 2016, 9:39:05 AM
    Author     : ITESS
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
    });
</script>
<h3>Need Valid Privileges...!!!</h3>
<div class="container-fluid" style="border-radius: 2px;">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="col-md-12 col-lg-12 col-sm-12" style="display:block;">
        <label id="msgPswd" class="msgTextField"></label>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4">Password</div>
            <div class="col-md-8 col-lg-8 col-sm-8"><input type="password" id="txtPswd" style="width: 100%"></div>
        </div> 
        <div class="row">
            <input type="hidden" id="txtRescheduleId" value="${rescheduleId}"/>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><input type="button" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>    
            <div class="col-md-4"><input type="button"  onclick="loadRescheduleApprovalForm()" class="btn btn-success col-md-12 col-sm-12 col-lg-12" value="Check"/></div>    
        </div>
    </div>
</div>
<script type="text/javascript">
    function loadRescheduleApprovalForm() {
        var rescheduleId = $("#txtRescheduleId").val();
        var pswd = $("#txtPswd").val().trim();
        if (pswd !== null && pswd !== "") {
            if (rescheduleId !== "") {
                $.ajax({
                    url: '/AxaBankFinance/SettlementController/loadLoanRescheduleApprovalForm',
                    type: 'GET',
                    data: {"rescheduleId": rescheduleId, "pswd": pswd},
                    success: function(data) {
                        $('#divRescheduleForm').html("");
                        $('#divRescheduleForm').html(data);
                        ui('#divRescheduleForm');
                    }
                });
            }
        } else {
            $("#msgPswd").text("Enter Password..");
            $("#txtPswd").addClass("txtError");
            $("#txtPswd").focus();
        }
    }
</script>

<%-- 
    Document   : Overpaidloantable
    Created on : Sep 21, 2016, 4:02:19 PM
    Author     : Chanaka
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row" style="margin-top: 5px" id="payCorrectTable">
    <div class="row fixed-table">
        <div class="table-content">
                <table  id="tblCustomer" class="dc_table dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0" >  
                    <thead>
                        <tr>
                            <th style="text-align: center; padding-left: 20px">#</th>
                            <th style="text-align: center">Agreement No</th>
                            <th style="text-align: center">Debtor Name</th>
                            <th style="text-align: center">Transaction No</th>
                            <th style="text-align: center">Amount</th>
                            <th style="text-align: center">Receipt No</th>
                            <th style="text-align: center">Date</th>
                        </tr>
                    </thead>
                    <tbody >                    
                        <c:forEach var="payments" items="${paymentList}">
                            <tr>     
                                <td style="text-align: center; padding-left: 20px">
                                    <input type="checkbox" style="cursor: pointer" class="transactionCheckBox" id="transactionCheck${payments.loanId}" onclick="selectPaymentID(${payments.loanId})"/>
                                </td>   
                                <td style="text-align: center">${payments.loanHeaderDetails.loanAgreementNo}</td>
                                <td style="text-align: justify">${payments.loanHeaderDetails.debtorHeaderDetails.debtorName}</td>
                                <td style="text-align: center">${payments.transactionNoCrdt}</td>
                                <td style="text-align: right">${payments.overPaymentAmt}</td>
                                <td style="text-align: center">${payments.receiptNo}</td>
                                <td style="text-align: center">${payments.systemDate}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
        </div>
    </div>
    <c:if test="${paymentList.size() > 0}">
        <div class="row" style="margin-top: 1%">
            <div  class="col-md-3 ">
                <input type="button" id="correctButton" class="btn btn-success col-md-12 col-sm-12 col-lg-12" value="Refund" />
            </div>
        </div>
    </c:if>
</div>
<script>
    var loanId;
    var checked = false;
    function selectPaymentID(loanid) {
        $('#transactionCheck' + loanid).is(':checked');
        $('.transactionCheckBox').prop('checked', false);
        $('#transactionCheck' + loanid).prop('checked', true);
        loanId = loanid;
        checked = true;
    }
    $('#correctButton').click(function () {
        if (checked) {
            $.ajax({
                url: '/AxaBankFinance/SettlementController/loadoprefundform/' + loanId,
                success: function (data) {
                    $('#divProcess').html(data);
                    ui('#divProcess');
                },
                error: function () {
                }
            });
        }
        else{
            alert("Please Select a Payment to Refund.");
        }
    });
</script>
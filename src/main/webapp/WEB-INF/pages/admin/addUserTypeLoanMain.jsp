<%-- 
    Document   : addApprovalForm
    Created on : Mar 19, 2015, 4:09:32 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#tblViewUserTypeLoan").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "30%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });

</script>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-7" id="divUserTypeLoan">

    </div>
    <div class="col-md-5" style="margin-top: 50px; margin-bottom: 20px;">
        <table id="tblViewUserTypeLoan" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="userTypeLoan" items="${userTypeLoans}">
                    <c:forEach var="user" items="${userList}">
                        <c:if test="${userTypeLoan == user.userId}">
                            <tr>
                                <td>${user.userName}</td>
                                <td><div class="edit" href='#' onclick="clickToEdit(${user.userId})"></div></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

</div>

<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/viewUserTypeForm',
            success: function(data) {
                $('#divUserTypeLoan').html(data);
            },
            error: function() {
            }
        });
    });

    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/findUserTypeLoanList/' + id,
            success: function(data) {
                $('#divUserTypeLoan').html("");
                $('#divUserTypeLoan').html(data);
                document.getElementById("btnSaveUserTypeSubLoan").value = "Update";
            }
        });
    }


</script>
<%-- 
    Document   : addFieldOfficeMain
    Created on : May 18, 2015, 4:57:44 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#tblViewFieldOffice").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "90%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
          unblockui();
    });

</script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-6" id="divAddFieldOfficeForm"></div>

    <div class="col-md-1"></div>

    <div class="col-md-5" style="margin:35 0 10 0">
        <table id="tblViewFieldOffice" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Recovery Manager</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="v_manager" items="${managers}">
                    <tr id="${v_manager.managerId}">
                        <c:forEach var="v_user" items="${UmUser}">
                            <c:if test="${v_manager.managerId == v_user.userId}">
                                <td>${v_user.userName}</td>
                            </c:if>
                        </c:forEach>
                        <td><div class="edit" href='#' onclick="clickToEdit(${v_manager.managerId})"></div></td>
                    </tr>
                </c:forEach>

            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>

<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/loadFieldOfficeForm',
            success: function(data) {
                $('#divAddFieldOfficeForm').html(data);
            }
        });
    });
</script>

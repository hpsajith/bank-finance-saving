<%-- 
    Document   : collectionForm
    Created on : Dec 1, 2015, 12:04:35 AM
    Author     : IT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container-fluid">
    <div class="row" style="margin: 5 8 2 0"><strong style="font-size: 15px"><legend>Collection Dates</legend></strong></div>
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <table id="tblDate" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Date</th>
                            <c:forEach var="recOff" items="${recOffs}">
                                <th id="${recOff.empNo}">${recOff.empLname}</th>
                            </c:forEach>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="i" begin="1" end="31">
                        <tr id="row${i}">
                            <td>${i}</td>
                        <c:forEach var="recOff" items="${recOffs}">
                            <td id="clmn${recOff.empNo}">
                                <input type="checkbox"/>
                            </td>
                        </c:forEach>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-7"></div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-6">
                    <input type="button" value="Cancel" class="btn btn-default" style="width: 100%" id="btnCancel" onclick="unblockui()"/>
                </div>
                <div class="col-md-6">
                    <input type="button" value="Save" class="btn btn-default" style="width: 100%" id="btnSave" onclick="saveCollection()"/>

                </div>
            </div>

        </div>
    </div>
</div>
<div id="message" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">
    
    $(function (){
       getCollection();
    });
    
    function getCollection() {
        $.ajax({
            url: "/AxaBankFinance/getCollectionDates",
            success: function(data) {
                if (data.length !== 0) {
                    for (var i = 0; i < data.length; i++) {
                        var recOffId = "clmn"+data[i].recOffId;
                        var date = data[i].date;
                        var tds = $("#row" + date).find("td");
                        for (var j = 1; j < tds.length; j++) {
                            var td = tds[j];
                            if (td.id === recOffId) {
                                td.children[0].checked = true;
                            }
                        }
                    }
                }
            }
        });
    }
    function saveCollection() {
        var rocdfs = [];
        var trs = $("#tblDate tbody").find("tr");
        for (var i = 0; i < trs.length; i++) {
            var tr = trs[i];
            var tds = tr.children;
            for (var j = 1; j < tds.length; j++) {
                var td = tds[j];
                if (td.children[0].checked) {
                    var recOffId = td.id.replace("clmn","");
                    var date = tr.id.replace("row","");
                    var o = new recOfColDate(recOffId, date);
                    rocdfs.push(o);
                }
            }
        }
        $.ajax({
            url: "/AxaBankFinance/saveOrUpadteCollectionDates?${_csrf.parameterName}=${_csrf.token}",
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify({"rocdfs": rocdfs}),
            success: function(data) {
                $("#message").html("");
                $("#message").html(data);
                ui("#message");
            }
        });

    }

    function recOfColDate(recOffId, date) {
        this.recOffId = recOffId;
        this.date = date;
    }
</script>
<%-- 
    Document   : addSupplierForm
    Created on : Nov 14, 2007, 3:18:51 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form name="newSupplierForm" id="newSupplier">
    <div class="row" style="margin: 0 8 2 0"><legend>Add Supplier</legend></div>
    <div class="row">
        <input type="hidden" name="supplierId" value="${supplier.supplierId}"/>
        <div class="col-md-5 col-sm-5 col-lg-5 ">Supplier Name</div> 
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text" name="supplierName" id="txtSupplierName" style="width: 100%" value="${supplier.supplierName}" maxlength="50"/>
            <label id="msgSupplierName" class="msgTextField"></label>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5 ">Supplier Address</div>    
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text"name="supplierAddress" id="txtSupplierAddress" style="width: 100%" value="${supplier.supplierAddress}" maxlength="100"/>
            <label id="msgSupplierAddress" class="msgTextField"></label>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5 ">Supplier Telephone</div>    
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text"name="supplierTelephone" id="txtSupplierTelephone" style="width: 100%" value="${supplier.supplierTelephone}" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10"/>
            <label id="msgSupplierTelephone" class="msgTextField"></label>
        </div> 
    </div>    
    <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
        <div  class="col-md-6 col-sm-6 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveSupplier" value="Save" onclick="saveSupplier()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script type="text/javascript">
    function saveSupplier() {
        if (fieldValidate()) {
        document.getElementById("btnSaveSupplier").disabled = true;
        $.ajax({
                url: '/AxaBankFinance/saveOrUpdateSupplier',
                type: 'POST',
                data: $('#newSupplier').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                       loadAddSupplierMain();
                    }, 2000);
                    
                },
                error: function() {
                    alert("error Loading");
                }
            });
        }
    }    
    
    function fieldValidate() {
        var supplierName = document.getElementById('txtSupplierName');
        var supplierAddress = document.getElementById('txtSupplierAddress');
        var supplierTelephone = document.getElementById('txtSupplierTelephone');
        
        if (supplierName.value === null || supplierName.value === "") {
            $("#msgSupplierName").html("Name must be filled out");
            $("#txtSupplierName").addClass("txtError");
            $("#txtSupplierName").focus();
            return false;
        } else {
            $("#txtSupplierName").removeClass("txtError");
            $("#msgSupplierName").html("");
        }
        if (supplierAddress.value === null || supplierAddress.value === "") {
            $("#msgSupplierAddress").html("Address must be filled out");
            $("#txtSupplierAddress").addClass("txtError");
            $("#txtSupplierAddress").focus();
            return false;
        } else {
            $("#txtSupplierAddress").removeClass("txtError");
            $("#msgSupplierAddress").html("");
        }
        if (supplierTelephone.value === null || supplierTelephone.value === "") {
            $("#msgSupplierTelephone").html("Telephone must be filled out");
            $("#txtSupplierTelephone").addClass("txtError");
            $("#txtSupplierTelephone").focus();
            return false;
        } else {
            $("#txtSupplierTelephone").removeClass("txtError");
            $("#msgSupplierTelephone").html("");
        }        
 
        return true;
    }     
</script>
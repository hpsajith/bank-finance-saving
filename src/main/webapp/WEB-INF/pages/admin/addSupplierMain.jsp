<%-- 
    Document   : addSupplierMain
    Created on : Nov 14, 2007, 2:53:01 AM
    Author     : Admin
--%>
<script type="text/javascript">
    $(document).ready(function(){
        pageAuthentication();
        $("#viewSupplier").chromatable({
                width: "100%", // specify 100%, auto, or a fixed pixel amount
                height: "40%",
                scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
                 
            });
              unblockui();
    });
     
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-12 col-sm-12 col-lg-12" style="width: 100%">
        <div class="row">
            <div class="col-md-5 col-sm-5 col-lg-5" id="divSupplier" style="margin: 24px;">
                <!--add Supplier Inner HTML-->
            </div>
            <div class="col-md-2 col-sm-2 col-lg-2" style="margin-right: -40px"></div>

            <div class="col-md-5 col-sm-5 col-lg-5" id="divViewSupplier" style="margin: 45px">
                <div class="row" style="overflow-y: auto;height: 85%">
                    <table id="viewSupplier" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Address</th>
                                <th>Telephone</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="dc_clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">    
    $(function() {
        //Load Supplier Form
        $.ajax({
               url: '/AxaBankFinance/addSupplierForm',
               success: function(data) {
                   $('#divSupplier').html(data);
               },
               error: function() {
               }
           });
           
        //Load Supplier Table
        $.getJSON('/AxaBankFinance/loadSupplier', function(data) {
            for (var i = 0; i < data.length; i++) {
                var supplierId = data[i].supplierId;
                var supplierName = data[i].supplierName;
                var supplierAddress = data[i].supplierAddress;
                var supplierTelephone = data[i].supplierTelephone;
                $('#viewSupplier tbody').append("<tr id='" + supplierId + "'>" +
                        "<td>" + supplierName + "</td>" +
                        "<td>" + supplierAddress + "</td>" +
                        "<td>" + supplierTelephone + "</td>" +
                        "<td> <div class='edit' href='#' onclick='clickToEdit(" + supplierId + ")'></div></td></tr>");

            }
        });
    });
    
        function clickToEdit(ID) {
            $.ajax({
                url: '/AxaBankFinance/findSupplier/' + ID,
                success: function(data) {
                    $('#divSupplier').html("");
                    $('#divSupplier').html(data);
                    document.getElementById("btnSaveSupplier").value="Update";
                },
                error: function() {
                    alert("Error View..!");
                }
            });
        }      
    
</script>


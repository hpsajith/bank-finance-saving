<%-- 
    Document   : addApprovalMain1
    Created on : Apr 7, 2015, 11:07:40 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function () {
        $("#tblAppLevel").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "50%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });

    });
</script>

<form name="adminApproveLevelForm" id="adminApproveLevel">
    <div class="row" style="margin: 5 8 2 0"><legend>Add Approve Levels</legend></div>

    <div class="row" style="margin-top: 15px">
        <div class="col-md-3 col-sm-3 col-lg-3 ">User</div> 
        <div class="col-md-8 col-sm-8 col-lg-8">
            <select name="userId" id="user" class="col-md-6" style="padding: 1">
                <option value="0">--SELECT--</option>
                <c:forEach var="users" items="${user}">
                    <c:choose>
                        <c:when test="${users[0] == userId}">
                            <option value="${users[0]}" selected="selected">${users[1]}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${users[0]}">${users[1]}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>

            <label id="msgUser" class="msgTextField"></label>          

        </div>
        <div class="col-md-1 col-sm-1 col-lg-1 "></div> 
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-3 col-sm-3 col-lg-3">Approval Level</div>
        <div class="col-md-8 col-sm-8 col-lg-8">
            <table id="tblAppLevel" class=" dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th></th>
                        <th>Approval Level</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="appList" items="${appLevels}">
                        <tr id="${appList.appId}">
                            <c:set var="hasApproval" value="false"></c:set>
                            <c:forEach var="approvedList" items="${e_approvals}">
                                <c:if test="${appList.appId==approvedList.MApproveLevels.appId}">
                                    <c:set var="hasApproval" value="true"></c:set>
                                </c:if>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${hasApproval}">
                                    <td><input type="checkbox" id="app${appList.appId}" checked="true"/></td>
                                    <td>${appList.appDescription}</td>
                                </c:when>
                                <c:otherwise>
                                    <td><input type="checkbox" id="app${appList.appId}"/></td>
                                    <td>${appList.appDescription}</td>
                                </c:otherwise>
                            </c:choose>                           
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div class="dc_clear"></div>
        </div>
        <div class="col-md-1 col-sm-1 col-lg-1 "></div> 
    </div>

    <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
        <div  class="col-md-5 col-sm-5 col-lg-5"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveAppLevels" value="Save" onclick="saveUpdateApproLevels()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
        <div class="col-md-1 col-sm-1 col-lg-1"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <div id="hideAppType"></div>
</form>
<script>
    function saveUpdateApproLevels() {
        if (fieldValidate()) {
            setToAppType();
            document.getElementById("btnSaveAppLevels").disabled = true;
            //$("#btnSaveAppLevels").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateAppLevels',
                type: 'POST',
                data: $('#adminApproveLevel').serialize(),
                success: function (data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function () {
                        loadApproveLevelForm();
                    }, 2000);
                },
                error: function () {
                    alert("Error Loding..");
                }

            });
        }

    }
    function setToAppType() {
        var hasAppType = false;
        $('#tblAppLevel tbody tr').each(function () {
            var appId = $(this).attr('id');
            if ($('#app' + appId).is(':checked')) {
                hasAppType = true;
                $('#hideAppType').append("<input type = 'hidden' name = 'approveId' value = '" + appId + "'/>");
            }
        });
        if (!hasAppType) {
            $("#hideAppType").append("<input type = 'hidden' name = 'approveId' value = '0'/>");
        }

    }

    function fieldValidate() {
        var user = $("#user").find('option:selected').val();
        if (user == 0) {
            $("#msgUser").html("Select The User");
            $("#user").addClass("txtError");
            $("#user").focus();
            return false;
        } else {
            $("#user").removeClass("txtError");
            $("#msgUser").html("");
            $("#msgUser").removeClass("msgTextField");
        }
        return true;
    }

</script>
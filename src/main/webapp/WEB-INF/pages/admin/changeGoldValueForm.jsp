<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
    Document   : addLoanTypeFormAdd
    Created on : Mar 13, 2015, 12:49:39 PM
    Author     : SOFT
--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#viewAccountCheckList").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "500px",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<form name="addNewGoldForm" id="addNewGoldForm">
    <input type="hidden" name="weightId" value="${unitList.weightId}">
    <div class="row" style="margin: 0px 8px 2px 0px">
        <legend>Update Gold Price</legend>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-1"></div>
        <div class="col-md-3 col-sm-3 col-lg-2">Measurement Type</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text" value="${unitList.weightUnitName}" name="weightUnitName" id="weightUnitName" style="width: 55%"/>
        <label id="msgAccountTypeName" class="msgTextField"></label>
    </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-1"></div>
        <div class="col-md-3 col-sm-3 col-lg-2">Current Price (LKR)</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <%--<input type="hidden" name="weightId" value="${unitList.weightId}">--%>
            <label id="currntPrice" class="msgTextField">${unitList.pricePerUnit}</label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-1"></div>
        <div class="col-md-3 col-sm-3 col-lg-2">Price Per Unit</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text" name="pricePerUnit" id="pricePerUnit" style="width: 55%" value="${unitList.pricePerUnit}"/>
        </div>
    </div>

    <div class="row" id="hideCheckList"></div>

    <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
        <div class="col-md-3 col-sm-6 col-lg-3"></div>
        <div class="col-md-1 col-sm-3 col-lg-2"><input type="button" name="" value="Cancel"
                                                       class="btn btn-default col-md-12 col-sm-12 col-lg-12"
                                                       onclick="pageExit()"/></div>
        <div class="col-md-1 col-sm-3 col-lg-2"><input type="button" name="btnSave" id=name="btnSave" value="Save" onclick="saveNewGoldPrice()"
                                                       class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
</form>

<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script type="text/javascript">
    function saveNewGoldPrice() {

            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateGoldValue',
                type: 'GET',
                data: $('#addNewGoldForm').serialize(),
                success: function (data) {
                    $('#formContent').html(data);
                    $(".divSuccess").css({'display': 'block'});
                    $("html, body").animate({scrollTop: 0}, "fast");
                },
                error: function () {
                    alert("Error in Saving Seizer Details..!");
                }
            });
        // }
    }

    // function fieldValidate() {
    //     var loanTypeName = document.getElementById('txtLoanTypeName');
    //     var loanTypeCode = document.getElementById('txtLoanTypeCode');
    //     if (loanTypeName.value == null || loanTypeName.value == "") {
    //         alert("Loan Type Name must be filled out");
    //         $("#txtLoanTypeName").addClass("txtError");
    //         return false;
    //     } else {
    //         $("#txtLoanTypeName").removeClass("txtError");
    //     }
    //     if (loanTypeCode.value == null || loanTypeCode.value == "") {
    //         alert("Loan Type Code must be filled out");
    //         $("#txtLoanTypeCode").addClass("txtError");
    //         return false;
    //     } else {
    //         $("#txtLoanTypeCode").removeClass("txtError");
    //     }
    //     return true;
    // }
</script>

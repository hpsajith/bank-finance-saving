<%-- 
    Document   : addGroupForm
    Created on : June 27, 2017, 8:24:43 AM
    Author     : Harshana
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function () {
        pageAuthentication();
        $("#tblSeizer").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "90%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="container-fluid"style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-7 col-sm-7 col-lg-7" id="viewAddSeizerForm" style="margin-top: 5px"></div>
    <div class="col-md-5 col-sm-5 col-lg-5" style="margin-top: 45px">
        <div class="row" style="margin-bottom: 30px;">
            <table id="tblSeizer"class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="text-align: center">Id</th>
                        <th style="text-align: center">Branch</th>
                        <th style="text-align: center">Center</th>
                        <th style="text-align: center">Group</th>
                        <th style="text-align: center">Detail</th>
                        <th style="text-align: center">Edit</th>
                        <th style="text-align: center">InActive</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="groups" items="${groupList}">
                        <c:choose>
                            <c:when test="${groups.isActive == 0}">
                                <tr id="${groups.groupID}" style="background-color: #BDBDBD;color: #888">
                                    <td style="text-align: center">${groups.groupID}</td>
                                    <c:forEach var="branch" items="${branchList}">
                                        <c:if test="${branch.branchId == groups.branchID}">
                                            <td style="text-align: center">${branch.branchName} </td>
                                        </c:if>
                                    </c:forEach>
                                    <c:forEach var="center" items="${centerList}">
                                        <c:if test="${center.centerID == groups.centeID}">
                                            <td style="text-align: center">${center.centerName} </td>
                                        </c:if>
                                    </c:forEach>
                                    <td style="text-align: center">${groups.groupName}</td>
                                    <td style="text-align: center">${groups.detail}</td>
                                    <td style="text-align: center"><div class="" href='#'></div></td>
                                    <td style="text-align: right"><div class="btnActive" href='#' id="btnInactive" onclick="clickToActive(${groups.groupID})"></div></td>
                                </tr> 
                            </c:when> 
                            <c:otherwise>
                                <tr id="${groups.groupID}">
                                    <td style="text-align: center">${groups.groupID}</td>
                                    <c:forEach var="branch" items="${branchList}">
                                        <c:if test="${branch.branchId == groups.branchID}">
                                            <td style="text-align: center">${branch.branchName} </td>
                                        </c:if>
                                    </c:forEach>
                                    <c:forEach var="center" items="${centerList}">
                                        <c:if test="${center.centerID == groups.centeID}">
                                            <td style="text-align: center">${center.centerName} </td>
                                        </c:if>
                                    </c:forEach>
                                    <td style="text-align: center">${groups.groupName}</td>
                                    <td style="text-align: center">${groups.detail}</td>
                                    <td style="text-align: center"><div class="edit" href='#' onclick="clickToEdit(${groups.groupID})"></div></td>
                                    <td style="text-align: right"><div class="delete" href='#' id="btnInactive" onclick="clickToInactive(${groups.groupID})"></div></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>                                                                                                                                                     
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            url: '/AxaBankFinance/loadAddGroupForm',
            success: function (data) {
                $('#viewAddSeizerForm').html("");
                $('#viewAddSeizerForm').html(data);
            },
            error: function () {
            }
        });
    });
    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/editGroup/' + id,
            success: function (data) {
                $('#viewAddSeizerForm').html("");
                $('#viewAddSeizerForm').html(data);
                document.getElementById("btnsaveSeizer").value = "Update";
            }
        });
    }
    function clickToInactive(id) {
        $.ajax({
            url: '/AxaBankFinance/inActiveGroup/' + id,
            success: function (data) {
                if (data == true) {
                    loadAddGroupMain();
                }
            }
        });
    }
    function clickToActive(id) {
        $.ajax({
            url: '/AxaBankFinance/activeGroup/' + id,
            success: function (data) {
                if (data == true) {
                    loadAddGroupMain();
                }
            }
        });
    }
</script>


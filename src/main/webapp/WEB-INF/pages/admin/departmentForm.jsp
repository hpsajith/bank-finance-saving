<%-- 
    Document   : branchForm
    Created on : May 8, 2015, 5:45:40 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        $("#").chromatable({
            width: "100%", //specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>
<div class = "labelMessage" id="lblMessage"></div>
<div class="row" style="margin:-25 305 5 0"><strong>Add Department</strong></div>
<div class="container-fluid">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row" style="margin: 1px; color: red; text-align: left;"><label id="errmsgForTab"></label></div>
    <form name="userDepartment_Form" id="user_department">
        <input type="hidden" name="empId" id="txtEmpId" value="${emp_ID}"/>
        <div class="row">
            <div class="col-md-2">Branch</div> 
            <div class="col-md-10">
                <select name="branchId" id="branch" class="col-md-6 col-sm-6 col-lg-6" style="padding: 1">
                    <option value="0">--SELECT--</option>
                    <c:forEach var="user_branch" items="${userBranch}">
                        <c:forEach var="branch" items="${umBranch}">
                            <c:if test="${user_branch.branchId == branch.branchId}">
                                <option value="${branch.branchId}">${branch.branchName}</option>
                            </c:if>
                        </c:forEach>
                    </c:forEach>
                </select>
                <label id="msgBranch" class="msgTextField"></label>
            </div> 

        </div>
        <div class="row" style="margin:5 5 5 5">
            <table id="tblDep"  class="dc_fixed_tables table-bordered" width="100%" border="3" cellspacing="1" cellpadding="1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Department Name</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="v_Dep" items="${departmentList}">
                        <tr id="${v_Dep.depId}">
                            <td><input type="checkbox" id="dep${v_Dep.depId}"</td>
                            <td>${v_Dep.depName}</td>                        
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
        </div>
        <div class="row" style="margin-top: 5%">
            <div class="col-md-3"><input type="button" name="" value="Refesh" class="btn btn-default col-md-12" onclick="loadNewEmployeeForm()"/></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"/></div>
            <div class="col-md-3"><input type="button" id="btnSaveUserDep" name="" value="Save" onclick="saveUserDepartmennt()" class="btn btn-default col-md-12"/></div>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />   
        <div id="hideDepartment" style="display: none"></div>
    </form>


</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script>
    function saveUserDepartmennt() {
        if (!setToPages()) {
            $("#errmsgForTab").text("Please select atleast one Department");
        } else {
            if (checkSelectBranch()) {
                document.getElementById("btnSaveUserDep").disabled = true;
                $.ajax({
                    url: '/AxaBankFinance/saveUserDepartment',
                    type: 'POST',
                    data: $('#user_department').serialize(),
                    success: function(data) {
                        $('#lblMessage').text(data);
                        $('#hideDepartment').html("");
                        loadAddDepartment();
                        window.scrollTo();

                    },
                    error: function() {
                        alert("Error Loading...!");
                    }
                });
            }
        }
    }
    function  setToPages() {
        var hasDep = false;
        $('#tblDep tbody tr').each(function() {
            var depId = $(this).attr('id');
            if ($('#dep' + depId).is(':checked')) {
                hasDep = true;
                $("#hideDepartment").append("<input type='text' name='DepID' value='" + depId + "'/>");
            }

        });
        return hasDep;
    }
    function checkSelectBranch() {
        var branch = $("#branch").find("option:selected").val();

        if (branch == 0) {
            $("#msgBranch").html("Select The Branch");
            $("#branch").addClass("txtError");
            $("#branch").focus();
            return false;
        } else {
            $("#branch").removeClass("txtError");
            $("#msgBranch").html("");
            $("#msgBranch").removeClass("msgTextField");
        }
        return true;
    }

</script>




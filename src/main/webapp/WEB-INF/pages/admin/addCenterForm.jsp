<%-- 
    Document   : addCenterForm
    Created on : June 22, 2017, 10:27:55 AM
    Author     : Harshana
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div><label class = "successMsg">${successMsg} </label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>


<form name="newCenterForm" id="newCenter">
    <div class="row" style="margin: 5 8 2 0">
        <div class="col-md-8">
            <legend><h3>Add New Center</h3></legend>
        </div>
    </div>

    <input type="hidden" name="centerID" id="centerID" value="${center.centerID}"/>

    <div class="row">
        <div class="col-md-4">Branch</div>
        <div class="col-md-8">



            <select style="width: 80%" id="branchID" name="branchID" >
                <option value="0">-select the branch-</option>
                <c:forEach var="v_branch" items="${branchList}">
                    <c:choose>
                        <c:when test="${v_branch.isActive == true}">
                            <option value="${v_branch.branchId}" >${v_branch.branchName}</option>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </select>

            <label id="msgBranchId" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Center Code</div>
        <div class="col-md-8"><input type="text" id="txtCenterCode" name="centerCode"  value="${center.centerCode}" style="width: 80%"/>
            <label id="msgCenterCode" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Center Name</div>
        <div class="col-md-8"><input type="text" id="txtCenterName" name="centerName" value="${center.centerName}" style="width: 80%"/>
            <label id="msgCenterName" class="msgTextField"></label>
        </div>
    </div>

    <div class="row" style="margin-top: 5px">

        <!--<div  class="col-md-4"><input type="button" id="btnAddDepartment" value="Add Department" class="btn btn-default col-md-12" disabled="disabled" onclick="loadDepartment()"/></div>-->
        <div  class="col-md-4 "></div>
        <div  class="col-md-3 "><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>

        <div  class="col-md-3"><input type="button" name="" value="Save"id="btnsaveCenter" onclick="saveNewCenter()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>


        <div  class="col-md-2"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="department" class="jobsPopUp" style="width: 35%;height: 50%"></div>
<div id="message_branch" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script>
    function saveNewCenter() {
        
//         var branchID = $("#branchID").find('option:selected').val();
//        alert("Branch ID : " + branchID);
        
//
//        $.ajax({
//            url: "/AxaBankFinance/loadWarningBox/" + "Please Contact ITES!!",
//            success: function (data) {
//                $("#message_branch").html(data);
//                ui("#message_branch");
//            }
//        });

        if (fieldValidate()) {
            document.getElementById("btnsaveCenter").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateCenter',
                type: 'POST',
                data: $('#newCenter').serialize(),
                success: function (data) {

//                    alert("data : " + data);
//                    $('#divAddCenterForm').html(data);
//                    document.getElementById("btnsaveCenter").disabled = false;
//                    $("#txtCenterCode").val("");
//                    $("#txtCenterName").val("");
//                    $("#txtDescription").val("");
//                    $("#txtHotline").val("");


                    //--------------------------

                    $.ajax({
                        url: '/AxaBankFinance/loadAddCenterMain',
                        success: function (data) {
                            $('#formContent').html(data);
                        },
                        error: function () {
                        }
                    });

                    //--------------------------


                },
                error: function () {
                    alert("Error Loding..");
                }

            });
        }

    }
    function loadDepartment() {
        var branchId = document.getElementById("txtBranchId").value;
        $.ajax({
            url: '/AxaBankFinance/loadDepartmentform/' + branchId,
            success: function (data) {
                $("#department").html("");
                $("#department").html(data);
                ui('#department');
            }
        });
    }

    function fieldValidate() {
        var centerName = $("#txtCenterName").val();
        var centerCode = $("#txtCenterCode").val();

        var branchID = $("#branchID").find('option:selected').val();


        if (branchID == 0) {
            $("#msgBranchId").html("Select the branch");
            $("#branchID").addClass("txtError");
            $("#branchID").focus();
            return false;
        } else {
            $("#branchID").removeClass("txtError");
            $("#msgBranchId").html("");
            $("#msgBranchId").removeClass("msgTextField");
        }

        if (centerCode == "" || centerCode == null) {
            $("#msgCenterCode").html("Enter center code");
            $("#txtCenterCode").addClass("txtError");
            $("#txtCenterCode").focus();
            return false;
        } else {
            $("#txtCenterCode").removeClass("txtError");
            $("#msgCenterCode").html("");
            $("#msgCenterCode").removeClass("msgTextField");
        }

        if (centerName == "" || centerName == null) {
            $("#msgCenterName").html("Enter center name");
            $("#txtCenterName").addClass("txtError");
            $("#txtCenterName").focus();
            return false;
        } else {
            $("#txtCenterName").removeClass("txtError");
            $("#msgCenterName").html("");
            $("#msgCenterName").removeClass("msgTextField");
        }
        return true;

    }
</script>
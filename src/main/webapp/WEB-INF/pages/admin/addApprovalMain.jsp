<%-- 
    Document   : addApprovalForm
    Created on : Mar 19, 2015, 4:09:32 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
     $(document).ready(function() {
         pageAuthentication();
        $("#tblViewApproveLevel").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
         unblockui();
    });

</script>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-7" id="divApprovalForm">

    </div>
    <div class="col-md-5" style="margin-top: 50px; margin-bottom: 20px;">
        <table id="tblViewApproveLevel" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Edit</th>
                    
                </tr>
            </thead>
            <tbody>
                <c:forEach var="v_appList" items="${appLevelList}">
                    <tr id="${v_appList[0]}">
                        <c:forEach var="umUser" items="${userList}">
                            <c:if test="${v_appList[1] == umUser.userId}">
                                <td>${umUser.userName}</td>
                            </c:if>
                        </c:forEach>
                        <td><div class="edit" href='#' onclick="clickToEdit(${v_appList[1]})"></div></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

</div>

<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/viewApprovalForm',
            success: function(data) {
                $('#divApprovalForm').html(data);
            },
            error: function() {
            }
        });
    });
    
    function clickToEdit(userId) {
        $.ajax({
            url: '/AxaBankFinance/viewEditAppLevels/' + userId,
            success: function(data) {
                $('#divApprovalForm').html("");
                $('#divApprovalForm').html(data);
                document.getElementById("btnSaveAppLevels").value = "Update";
            }
        });
    }


</script>
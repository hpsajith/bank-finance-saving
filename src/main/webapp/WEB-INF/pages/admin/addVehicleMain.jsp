<%-- 
    Document   : addVehicleMain
    Created on : Jul 27, 2016, 10:19:36 AM
    Author     : User
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblSeizer").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "90%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="container-fluid"style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-6 col-sm-6 col-lg-6" id="viewAddVehicleForm" style="margin-top: 5px"></div>

    <div class="col-md-6 col-sm-6 col-lg-6" style="margin-top: 45px">
        <div class="row" style="margin-bottom: 30px;">
            <table id="tblYard"class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="text-align: center">Vehicle Type</th>
                        <th style="text-align: center">Edit</th>
                        <th style="text-align: center">InActive</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="vehicle" items="${vehicleTypeList}">
                        <c:choose>
                            <c:when test="${vehicle.isActive == false}">
                                <tr id="${vehicle.vehicleType}" style="background-color: #BDBDBD;color: #888">
                                    <td style="text-align: center">${vehicle.vehicleTypeName}</td>
                                    <td style="text-align: center"><div class="" href='#'></div></td>
                                    <td style="text-align: right"><div class="btnActive" href='#' id="btnInactive" onclick="clickToActive(${vehicle.vehicleType})"></div></td>
                                </tr> 
                            </c:when>
                            <c:otherwise>
                                <tr id="${vehicle.vehicleType}">
                                    <td style="text-align: center">${vehicle.vehicleTypeName}</td>
                                    <td style="text-align: center"><div class="edit" href='#' onclick="clickToEdit(${vehicle.vehicleType})"></div></td>
                                    <td style="text-align: right"><div class="delete" href='#' id="btnInactive" onclick="clickToInactive(${vehicle.vehicleType})"></div></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/loadAddVehicleForm',
            success: function(data) {
                $('#viewAddVehicleForm').html("");
                $('#viewAddVehicleForm').html(data);
            },
            error: function() {
            }
        });
    });

    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/editeVehicleType/' + id,
            success: function(data) {
                $('#viewAddVehicleForm').html("");
                $('#viewAddVehicleForm').html(data);
                document.getElementById("btnsaveVehicleType").value = "Update";
            }
        });
    }

    function clickToInactive(id) {
        $.ajax({
            url: '/AxaBankFinance/inActiveVehicle/' + id,
            success: function(data) {
                if (data == true) {
                    loadAddVehicleMainPage();
                }
            }
        });
    }

    function clickToActive(id) {
        $.ajax({
            url: '/AxaBankFinance/activeVehicle/' + id,
            success: function(data) {
                if (data == true) {
                    loadAddVehicleMainPage();
                }
            }
        });
    }

</script>

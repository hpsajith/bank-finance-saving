<%-- 
    Document   : addUserTypeMain
    Created on : Apr 29, 2015, 11:57:49 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#tblViewUserType").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
          unblockui();
    });

</script>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-5" id="divUserType"></div>
    <div class="col-md-2" ></div>
    <div class="col-md-5" style="margin:45 0 10 70">
        <table id="tblViewUserType" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>User Type Name</th>
                    <th>Description</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="v_userType" items="${userTypeList}">
                    <tr id="${v_userType.userTypeId}">
                        <td>${v_userType.userTypeName}</td>
                        <td>${v_userType.userTypeDes}</td>
                        <td><div class="edit" href='#' onclick="clickToEdit(${v_userType.userTypeId})"></div></td>
                    </tr>
                </c:forEach>

            </tbody>
        </table>
    </div>
</div>
<script>
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/loadUserTypeForm',
            success: function(data) {
                $('#divUserType').html("");
                $('#divUserType').html(data);
            },
            error: function() {
            }
        });
    });
    function clickToEdit(Id) {
        $.ajax({
            url: '/AxaBankFinance/editUserType/' + Id,
            success: function(data) {
                $('#divUserType').html("");
                $('#divUserType').html(data);
                document.getElementById("btnSaveUserType").value = "Update";
            },
            error: function() {
            }
        });

    }
</script>
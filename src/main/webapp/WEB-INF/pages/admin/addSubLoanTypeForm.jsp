
<%-- 
    Document   : addSubLoanTypeEdit
    Created on : Mar 28, 2015, 10:46:55 AM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function () {
        $("#tblOtherChargers").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "50%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use


        });
        $("#tblDocumentCheckList").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "50%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        $("#tblChartOfAccounts").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "25%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });</script>

<form name="SubLoanTypeForm" id="addSubLoanType">
    <div class="row"><legend>Add Sub Loan Type</legend></div>
    <div class="row">
        <input type="hidden" name="subLoanId" value="${subLoanType.subLoanId}"/>
        <input type="hidden" name="isActive" value="${subLoanType.isActive}"/>
        <input type="hidden" name="maxCount" value="${subLoanType.maxCount}"/>
        <div class="col-md-5 col-sm-5 col-lg-5">Loan Type</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <select name="loanTypeId" style="width: 172px" id="loanTypes">
                <option selected="selected">SELECT</option>
                <c:forEach var="mloanType" items="${mloanTypes}">
                    <c:choose>
                        <c:when test="${mloanType.loanTypeId == subLoanType.loanTypeId}">
                            <option selected="selected" value="${subLoanType.loanTypeId}">${mloanType.loanTypeName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${mloanType.loanTypeId}">${mloanType.loanTypeName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
            <label id="msgLoanType" class="msgTextField"></label> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Sub Loan Type Name</div>
        <div class="col-md-7 col-sm-7 col-lg-7">            
            <c:choose>
                <c:when test="${subLoanType.subLoanName != null}">
                    <input type="text" name="subLoanName" id="txtSubLoanName" value="${subLoanType.subLoanName}" onblur="checkSubLoanType()" style="width: 90%" readonly/>                    
                </c:when>
                <c:otherwise>
                    <input type="text" name="subLoanName" id="txtSubLoanName" value="${subLoanType.subLoanName}" onblur="checkSubLoanType()" style="width: 90%"/>                    
                </c:otherwise>    
            </c:choose>
            <label id="msgLoan_TypeName" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Interest Rate</div>
        <div class="col-md-6 col-sm-6 col-lg-6" style="padding-right: 1"><input type="text" onkeypress="return checkNumbers(this)" name="subInterestRate" id="txtInterestRate" value="${subLoanType.subInterestRate}" style="width: 100%"/>            
            <label id="msgSubInterestRate" class="msgTextField"></label>
        </div>
        <div class="col-md-1 col-sm-1 col-lg-1" style="padding-left: 1"><strong>%</strong></div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Sub Loan Code</div>
        <div class="col-md-5 col-sm-5 col-lg-5">
            <c:choose>
                <c:when test="${subLoanType.subLoanCode != null}">
                    <input type="text" name="subLoanCode" maxlength="5" id="txtSubLoanCode" value="${subLoanType.subLoanCode}" style="width: 100%" readonly/>                    
                </c:when>
                <c:otherwise>
                    <input type="text" name="subLoanCode" maxlength="5" id="txtSubLoanCode" value="${subLoanType.subLoanCode}" style="width: 100%"/>                    
                </c:otherwise>    
            </c:choose>            
            <label id="msgLoanTypeCode" class="msgTextField"></label>
        </div>

    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Insurance Apply</div>
        <c:choose>
            <c:when test="${subLoanType.isInsurance == true}">
                <div class="col-md-5 col-sm-5 col-lg-1">
                    <input type="checkbox" checked="" name="isInsurance" onclick="showHide()"  id="checkInsurance" value="1" style="width: 100%"/></div>
                <label class="redmsg">(only HP and LS)</label>
            </c:when>
            <c:otherwise>
                <div class="col-md-5 col-sm-5 col-lg-1">
                    <input type="checkbox" name="isInsurance"  id="checkInsurance" value="1" style="width: 100%" onclick="showHide()"/></div>
                <label class="redmsg" style="font-size: 10px">(only HP and LS)</label>
            </c:otherwise>
        </c:choose>

        <div class="col-md-5 col-sm-5 col-lg-6"></div>

    </div>

    <!--CHART OF ACCOUNTS-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <table id="tblChartOfAccounts" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>                       
                        <th>Account Type</th>
                        <th>Code</th>
                    </tr>
                </thead>
                <tbody>
                    <c:choose>
                        <c:when test="${chartOfAccount.size() > 0}">
                            <c:choose>
                                <c:when test="${chartOfAccount.size() == 6}">
                                    <c:forEach var="account" items="${chartOfAccount}">
                                        <c:if test="${account.accountType == 'INTR'}">
                                            <tr>
                                                <td>Earned Income</td>
                                                <td><input type="text" id="intrAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgIntrAcc" class="msgTextField"></label> 
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'RECE'}">
                                            <tr>
                                                <td>Installment Collection</td>
                                                <td><input type="text" id="receAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgReceAcc" class="msgTextField"></label> 
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'INTRS'}">
                                            <tr>
                                                <td>UnEarned Income</td>
                                                <td><input type="text" id="intrsAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgIntrsAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'PAYBLE'}">
                                            <tr>
                                                <td>Payable</td>
                                                <td><input type="text" id="paybAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgPaybAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'DWN'}">
                                            <tr>
                                                <td>Down Payment</td>
                                                <td><input type="text" id="dwnAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgDwnAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'ODI'}">
                                            <tr>
                                                <td>Over Due Account</td>
                                                <td><input type="text" id="odiAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgOdiAcc" class="msgTextField"></label>
                                                </td>
                                            </tr> 
                                        </c:if>   
                                        <c:if test="${account.accountType == 'OVP'}">
                                            <tr>
                                                <td>Over Payment Account</td>
                                                <td><input type="text" id="odiAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgOdiAcc" class="msgTextField"></label>
                                                </td>
                                            </tr> 
                                        </c:if>
                                        <c:if test="${account.accountType == 'INTSUSP'}">
                                            <tr>
                                                <td>Interest Suspense Account</td>
                                                <td><input type="text" id="intSuspense" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgIntSuspense" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                    <tr id="InsuranceCredit" style="display: none;">
                                        <td>Insurance Commission</td>
                                        <td><input type="text" id="insCreditAcc" style="width: 90%" />
                                            <label id="msgInsCreditAcc" class="msgTextField"></label>
                                        </td>
                                    </tr>  
                                    <tr id="InsurancePayable" style="display: none;">
                                        <td>Insurance Payable</td>
                                        <td><input type="text" id="insPayAcc" style="width: 90%" />
                                            <label id="msgInsPayAcc" class="msgTextField"></label>
                                        </td>
                                    </tr>                                              
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="account" items="${chartOfAccount}">
                                        <c:if test="${account.accountType == 'INTR'}">
                                            <tr>
                                                <td>Earned Income</td>
                                                <td><input type="text" id="intrAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgIntrAcc" class="msgTextField"></label> 
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'RECE'}">
                                            <tr>
                                                <td>Installment Collection</td>
                                                <td><input type="text" id="receAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgReceAcc" class="msgTextField"></label> 
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'INTRS'}">
                                            <tr>
                                                <td>Unearned Income</td>
                                                <td><input type="text" id="intrsAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgIntrsAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'PAYBLE'}">
                                            <tr>
                                                <td>Payable</td>
                                                <td><input type="text" id="paybAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgPaybAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'DEBT'}">
                                            <tr>
                                                <td>Debtor Control</td>
                                                <td><input type="text" id="debtorAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgDebtorAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>

                                        <c:if test="${account.accountType == 'DWN'}">
                                            <tr>
                                                <td>Down Payment</td>
                                                <td><input type="text" id="dwnAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgDwnAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'ODI'}">
                                            <tr>
                                                <td>Over Due Account</td>
                                                <td><input type="text" id="odiAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgOdiAcc" class="msgTextField"></label>
                                                </td>
                                            </tr> 
                                        </c:if>
                                             <c:if test="${account.accountType == 'OVP'}">
                                            <tr>
                                                <td>Over Payment Account</td>
                                                <td><input type="text" id="ovpAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgOvpAcc" class="msgTextField"></label>
                                                </td>
                                            </tr> 
                                        </c:if>
                                             <c:if test="${account.accountType == 'INTSUSP'}">
                                            <tr>
                                                <td>Interest Suspense Account</td>
                                                <td><input type="text" id="intSuspense" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgIntSuspense" class="msgTextField"></label>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${account.accountType == 'INS_P'}">
                                            <tr id="InsuranceCredit">
                                                <td>Insurance Commission</td>
                                                <td><input type="text" id="insCreditAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgInsCreditAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>  
                                        </c:if>
                                        <c:if test="${account.accountType == 'INS_C'}">
                                            <tr id="InsurancePayable">
                                                <td>Insurance Payable</td>
                                                <td><input type="text" id="insPayAcc" value="${account.accountNo}" style="width: 90%" readonly/>
                                                    <label id="msgInsPayAcc" class="msgTextField"></label>
                                                </td>
                                            </tr>  
                                        </c:if>                                 
                                    </c:forEach>                                    
                                </c:otherwise>            
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td>Earned Income</td>
                                <td><input type="text" id="intrAcc" style="width: 90%" />
                                    <label id="msgIntrAcc" class="msgTextField"></label> 
                                </td>
                            </tr>
                            <tr>
                                <td>Installment Collection</td>
                                <td><input type="text" id="receAcc" style="width: 90%" />
                                    <label id="msgReceAcc" class="msgTextField"></label> 
                                </td>
                            </tr>
                            <tr>
                                <td>UnEarned Income</td>
                                <td><input type="text" id="intrsAcc" style="width: 90%" />
                                    <label id="msgIntrsAcc" class="msgTextField"></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Payable</td>
                                <td><input type="text" id="paybAcc" style="width: 90%" />
                                    <label id="msgPaybAcc" class="msgTextField"></label>
                                </td>
                            </tr>
                            <tr>                               
                                <td>Debtor Control</td>
                                <td><input type="text" id="debtorAcc" style="width: 90%" />
                                    <label id="msgDebtorAcc" class="msgTextField"></label> 
                                </td>
                            </tr>
                            <tr>
                                <td>Down Payment</td>
                                <td><input type="text" id="dwnAcc" style="width: 90%" />
                                    <label id="msgDwnAcc" class="msgTextField"></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Over Due Account</td>
                                <td><input type="text" id="odiAcc" style="width: 90%" />
                                    <label id="msgOdiAcc" class="msgTextField"></label>
                                </td>
                            </tr> 
                            <tr>
                                <td>Over Payment Account</td>
                                <td><input type="text" id="ovpAcc" style="width: 90%" />
                                    <label id="msgOvpAcc" class="msgTextField"></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Interest Suspense Account</td>
                                <td><input type="text" id="intSuspense" style="width: 90%" />
                                    <label id="msgIntSuspense" class="msgTextField"></label>
                                </td>
                            </tr>
                            <tr id="InsuranceCredit" style="display: none;">
                                <td>Insurance Commission</td>
                                <td><input type="text" id="insCreditAcc" style="width: 90%" />
                                    <label id="msgInsCreditAcc" class="msgTextField"></label>
                                </td>
                            </tr>  
                            <tr id="InsurancePayable" style="display: none;">
                                <td>Insurance Payable</td>
                                <td><input type="text" id="insPayAcc" style="width: 90%" />
                                    <label id="msgInsPayAcc" class="msgTextField"></label>
                                </td>
                            </tr>  
                        </c:otherwise>
                    </c:choose>
                </tbody>
            </table> 
        </div>
    </div>         


    <div class="row" style="margin-top: 10px">
        <!--OTHER CHARGES-->
        <div class="col-md-6 col-sm-6 col-lg-6">
            <table id="tblOtherChargers" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th></th>   
                        <th>Description</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                <tbody>

                    <c:forEach var="vOtherCharge" items="${otherChargesList}">
                        <tr id="${vOtherCharge.subTaxId}">
                            <c:set var="isCheck" value="false"></c:set>
                            <c:forEach var="eOtherCharges" items="${otherCharge}">
                                <c:if test="${vOtherCharge.subTaxId == eOtherCharges.MSubTaxCharges.subTaxId}">
                                    <c:set var="isCheck" value="true"></c:set>
                                </c:if>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${isCheck}">
                                    <td><input type="checkbox" id="otherCharge${vOtherCharge.subTaxId}" checked="true"</td>
                                    </c:when>
                                    <c:otherwise>
                                    <td><input type="checkbox" id="otherCharge${vOtherCharge.subTaxId}"</td>
                                    </c:otherwise>
                                </c:choose>
                            <td>${vOtherCharge.subTaxDescription}</td>
                            <c:choose>
                                <c:when test="${vOtherCharge.isPrecentage == true}">
                                    <td>${vOtherCharge.subTaxRate}%</td>
                                </c:when>
                                <c:otherwise>
                                    <td>Rs:${vOtherCharge.subTaxValue}</td>
                                </c:otherwise>
                            </c:choose>
                            <c:set var="isCheck" value="false"></c:set> 
                            </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div class="dc_clear"></div>
        </div>

        <!--OTHER DOCUMENTS LIST-->
        <div class="col-md-6 col-sm-6 col-lg-6">
            <table id="tblDocumentCheckList" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th></th>   
                        <th>Description</th>
                        <th>Compulsory</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="dCheckList" items="${documentCheckList}">
                        <tr id="${dCheckList.id}">
                            <c:set var="hasList" value="false"></c:set>
                            <c:set var="isCompulsory" value="0"></c:set>
                            <c:forEach var="eCheckList" items="${checkList}">
                                <c:if test="${dCheckList.id == eCheckList.MCheckList.id}">
                                    <c:set var="hasList" value="true"></c:set>
                                    <c:set var="isCompulsory" value="${eCheckList.isCompulsory}"></c:set>
                                </c:if>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${hasList}">
                                    <td><input type="checkbox" id="document${dCheckList.id}" checked="true"/></td>
                                    </c:when>
                                    <c:otherwise>
                                    <td><input type="checkbox" id="document${dCheckList.id}"</td>
                                    </c:otherwise>
                                </c:choose>
                            <td>${dCheckList.listDescription}</td>
                            <td><select id="document_com${dCheckList.id}">
                                    <c:choose>
                                        <c:when test="${isCompulsory == 1}">
                                            <option value='0'>No</option>
                                            <option value ='1' selected="selected">Yes</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value='0'>No</option>
                                            <option value ='1'>Yes</option>
                                        </c:otherwise>
                                    </c:choose>
                                </select></td>
                                <c:set var="hasList" value="false"></c:set>
                            </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div class="dc_clear"></div>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;margin-bottom: 5px;">
        <div  class="col-md-6 col-sm-6 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3" style=""><input type="button" id="btnSaveSubLoanType" name="" value="Save" onclick="saveSubLoanType()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <div id="hideChartOfAccounts"></div>
    <div id="hideOtherCharge"></div>
    <div id="hideDocumentCheckList"></div>
</form>   
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script>

    $(function () {
        $("#checkInsurance").val('0');
    });

    function saveSubLoanType() {
        if (fieldValidate()) {
            document.getElementById("btnSaveSubLoanType").disabled = true;
            setToChartOfAccounts();
            setToOtherCharge();
            setToDocumentCheckList();
            $.ajax({
                url: '/AxaBankFinance/saveSubLoanType',
                type: 'post',
                data: $('#addSubLoanType').serialize(),
                success: function (data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function () {
                        loadSubLoanTypeForm();
                    }, 2000);
                },
                error: function () {
                    alert("Error Loading..!");
                }
            });
        }
    }

    function setToChartOfAccounts() {
        var hideChartOfAccounts = document.getElementById("hideChartOfAccounts");
        var intrAcc = document.getElementById("intrAcc").value;
        var receAcc = document.getElementById("receAcc").value;
        var intrsAcc = document.getElementById("intrsAcc").value;
        var paybAcc = document.getElementById("paybAcc").value;
        var dwnAcc = document.getElementById("dwnAcc").value;
        var odiAcc = document.getElementById("odiAcc").value;
        
        var ovpAcc = document.getElementById("ovpAcc").value;
        var intSuspense = document.getElementById("intSuspense").value;
        var debtorAcc = document.getElementById("debtorAcc").value;
        var check = $("#checkInsurance").is(":checked");
        if (intrAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + intrAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INTR'/>");
        }
        if (receAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + receAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='RECE'/>");
        }
        if (intrsAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + intrsAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INTRS'/>");
        }
        if (paybAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + paybAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='PAYBLE'/>");
        }
        if (debtorAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + debtorAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='DEBT'/>");
        }
        if (dwnAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + dwnAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='DWN'/>");
        }
        if (odiAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + odiAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='ODI'/>");
        }
        if (ovpAcc != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + ovpAcc + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='OVP'/>");
        }
        if (intSuspense != null) {
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + intSuspense + "'/>");
            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INTSUSP'/>");
        }
        if (check) {
            var insCreditAcc = document.getElementById("insCreditAcc").value;
            var insPayAcc = document.getElementById("insPayAcc").value;
            if (insCreditAcc != null) {
                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + insCreditAcc + "'/>");
                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INS_P'/>");
            }
            if (insPayAcc != null) {
                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + insPayAcc + "'/>");
                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INS_C'/>");
            }
        }

    }
    function  setToOtherCharge() {
        var hasOtherCharge = false;
        $('#tblOtherChargers tbody tr').each(function () {
            var otherChargeId = $(this).attr('id');
            if ($('#otherCharge' + otherChargeId).is(':checked')) {
                hasOtherCharge = true;
                $("#hideOtherCharge").append("<input type = 'hidden' name = 'otherChargeID' value = '" + otherChargeId + "'/>");
            }
        });
        if (!hasOtherCharge) {
            $("#hideOtherCharge").append("<input type = 'hidden' name = 'otherChargeID' value = '0'/>");
        }
    }
    function setToDocumentCheckList() {
        var hasDocumentList = false;
        $('#tblDocumentCheckList tbody tr').each(function () {
            var documentId = $(this).attr('id');
            if ($('#document' + documentId).is(':checked')) {
                hasDocumentList = true;
                var isCompulsory = $('#document_com' + documentId).find("option:selected").val();
                $('#hideDocumentCheckList').append("<input type = 'hidden' name = 'documentCheckListId' value = '" + documentId + "'/>" +
                        "<input type = 'hidden' name = 'documentIsCompulsory' value ='" + isCompulsory + "'/>");
            }
        });
        if (!hasDocumentList) {
            $('#hideDocumentCheckList').append("<input type = 'hidden' name = 'documentCheckListId' value = '0'/>" +
                    "<input type = 'hidden' name = 'documentIsCompulsory' value ='0'/>");
        }
    }
    function fieldValidate() {
        var subLoanName = document.getElementById('txtSubLoanName');
        var rate = document.getElementById('txtInterestRate');
        var subLoanCode = document.getElementById('txtSubLoanCode');
        var loantype = $("#loanTypes").find("option:selected").val();
        var intrAcc = document.getElementById("intrAcc").value;
        var receAcc = document.getElementById("receAcc").value;
        var intrsAcc = document.getElementById("intrsAcc").value;
        var paybAcc = document.getElementById("paybAcc").value;
        var dwnAcc = document.getElementById("dwnAcc").value;
        var odiAcc = document.getElementById("odiAcc").value;
        var ovpAcc = document.getElementById("ovpAcc").value;
        var debtorAcc = document.getElementById("debtorAcc").value;
        var check = $("#checkInsurance").is(":checked");
        if (check) {
            var insCreditAcc = document.getElementById("insCreditAcc").value;
            var insPayAcc = document.getElementById("insPayAcc").value;
        }

        if (loantype === 0) {
            $("#msgLoanType").html("Loan Type must be Selected");
            $("#loanTypes").addClass("txtError");
            $("#loanTypes").focus();
            return false;
        } else {
            $("#loanTypes").removeClass("txtError");
            $("#msgLoanType").html("");
        }
        if (subLoanName.value === null || subLoanName.value === "") {
            $("#msgLoan_TypeName").html("Sub Loan Name must be filled out");
            $("#txtSubLoanName").addClass("txtError");
            $("#loanTypes").focus();
            return false;
        } else {
            $("#txtSubLoanName").removeClass("txtError");
            $("#msgLoan_TypeName").html("");
        }
        if (rate.value === null || rate.value === "") {
            $("#msgSubInterestRate").html("Rate must be filled out");
            $("#txtInterestRate").addClass("txtError");
            $("#txtInterestRate").focus();
            return false;
        } else {
            $("#txtInterestRate").removeClass("txtError");
            $("#msgSubInterestRate").html("");
        }
        if (subLoanCode.value === null || subLoanCode.value === "") {
            $("#msgLoanTypeCode").html("Sub Loan Code must be filled out");
            $("#txtSubLoanCode").addClass("txtError");
            $("#txtSubLoanCode").focus();
            return false;
        } else {
            $("#txtSubLoanCode").removeClass("txtError");
            $("#msgLoanTypeCode").html("");
        }
        if (intrAcc === null || intrAcc === "") {
            $("#msgIntrAcc").html("Enter Interest Account Code");
            $("#intrAcc").addClass("txtError");
            $("#intrAcc").focus();
            return false;
        } else {
            $("#intrAcc").removeClass("txtError");
            $("#msgIntrAcc").html("");
        }
        if (receAcc === null || receAcc === "") {
            $("#msgReceAcc").html("Enter Reciveble Account Code");
            $("#receAcc").addClass("txtError");
            $("#receAcc").focus();
            return false;
        } else {
            $("#receAcc").removeClass("txtError");
            $("#msgReceAcc").html("");
        }
        if (intrsAcc === null || intrsAcc === "") {
            $("#msgIntrsAcc").html("Enter Intresest Suspend Account Code");
            $("#intrsAcc").addClass("txtError");
            $("#intrsAcc").focus();
            return false;
        } else {
            $("#intrsAcc").removeClass("txtError");
            $("#msgIntrsAcc").html("");
        }
        if (paybAcc === null || paybAcc === "") {
            $("#msgPaybAcc").html("Enter Payable Account Code");
            $("#paybAcc").addClass("txtError");
            $("#paybAcc").focus();
            return false;
        } else {
            $("#paybAcc").removeClass("txtError");
            $("#msgPaybAcc").html("");
        }
        if (debtorAcc === null || debtorAcc === "") {
            $("#msgDebtorAcc").html("Enter Debtor Account Code");
            $("#debtorAcc").addClass("txtError");
            $("#debtorAcc").focus();
            return false;
        } else {
            $("#debtorAcc").removeClass("txtError");
            $("#msgDebtorAcc").html("");
        }

        if (dwnAcc === null || dwnAcc === "") {
            $("#msgDwnAcc").html("Enter Down Payment Account Code");
            $("#dwnAcc").addClass("txtError");
            $("#dwnAcc").focus();
            return false;
        } else {
            $("#dwnAcc").removeClass("txtError");
            $("#msgDwnAcc").html("");
        }
        if (odiAcc === null || odiAcc === "") {
            $("#msgOdiAcc").html("Enter ODI Account Code");
            $("#odiAcc").addClass("txtError");
            $("#odiAcc").focus();
            return false;
        } else {
            $("#odiAcc").removeClass("txtError");
            $("#msgOdiAcc").html("");
        }
        if (ovpAcc === null || ovpAcc === "") {
            $("#msgOvpAcc").html("Enter OVP Account Code");
            $("#ovpAcc").addClass("txtError");
            $("#ovpAcc").focus();
            return false;
        } else {
            $("#ovpAcc").removeClass("txtError");
            $("#msgOvpAcc").html("");
        }
        if (check) {
            if (insCreditAcc === null || insCreditAcc === "") {
                $("#msgInsCreditAcc").html("Enter Ins Commission Account Code");
                $("#insCreditAcc").addClass("txtError");
                $("#insCreditAcc").focus();
                return false;
            } else {
                $("#insCreditAcc").removeClass("txtError");
                $("#msgInsCreditAcc").html("");
            }
            if (insPayAcc === null || insPayAcc === "") {
                $("#msgInsPayAcc").html("Enter Ins Pay Account Code");
                $("#insPayAcc").addClass("txtError");
                $("#insPayAcc").focus();
                return false;
            } else {
                $("#insPayAcc").removeClass("txtError");
                $("#msgInsPayAcc").html("");
            }
        }

        return true;
    }
    //    function concatRate() {
    //        var interestRate = document.getElementById("txtInterestRate").value;
    //        var rate = interestRate + "%";
    //        document.getElementById("txtHiddenRate").value = rate;
    //    }

    function showHide() {        
        if ($("#checkInsurance").is(":checked")) {
            $("#InsuranceCredit").show();
            $("#insCreditAcc").val('');
            $("#InsurancePayable").show();
            $("#insPayAcc").val('');
            $("#checkInsurance").val('1');
        } else {
            $("#InsuranceCredit").hide();
            $("#insCreditAcc").val('');
            $("#InsurancePayable").hide();
            $("#insPayAcc").val('');
            $("#checkInsurance").val('0');
        }
    }


    function checkSubLoanType() {
        var subLoanName = document.getElementById('txtSubLoanName').value;
        var checkBtn = document.getElementById("btnSaveSubLoanType").value;
        if (subLoanName !== null && subLoanName !== "" && checkBtn !== null && checkBtn !== "Update") {
            $.ajax({
                url: '/AxaBankFinance/subLoanTypeCheck/' + subLoanName,
                success: function (data) {
                    if (data === 1) {
                        $("#msgLoan_TypeName").html("Sub Loan Name Is Alredy Exits");
                        $("#txtSubLoanName").addClass("txtError");
                        $("#txtSubLoanName").focus();
                        document.getElementById("btnSaveSubLoanType").disabled = true;
                    } else {
                        $("#txtSubLoanName").removeClass("txtError");
                        $("#msgLoan_TypeName").html("");
                        $("#msgLoan_TypeName").removeClass("msgTextField");
                        document.getElementById("btnSaveSubLoanType").disabled = false;
                    }
                }
            });
        }
    }

</script>
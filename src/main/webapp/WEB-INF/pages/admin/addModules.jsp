<%-- 
    Document   : addModules
    Created on : Apr 4, 2016, 3:40:15 PM
    Author     : ITESS
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        $("#tblAdminPages").chromatable({
            width: "100%", //specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<div class="row" style="margin:-25 305 5 0"><strong>Define Modules</strong></div>
<div class="container-fluid">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row" style="margin: 1px; color: red; text-align: left;"><label id="errmsgForTab"></label></div>
    <form name="jobsform" id="jobs">
        <div id="hidePages" style="display: none"></div>
        <input type="hidden" name="status" id="txtStatus"/> 
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />   
    </form>
    <table id="tblAdminPages"  class="dc_fixed_tables table-bordered" width="100%" border="3" cellspacing="1" cellpadding="1">
        <c:set var="checkModules" value="false"></c:set>
        <c:forEach var="job" items="${jobDefines}">
            <tr id="${job.mainTabId}"><th colspan="2">${job.mainTabName}</th></tr>
                    <c:forEach var="subJob" items="${job.subList}">
                        <c:choose>
                            <c:when test="${subJob.isActive}">
                        <tr id="${subJob.subTabNo}" style="background-color:#99CCFF">
                            <c:set var="checkModules" value="true"></c:set>
                        </c:when>
                        <c:otherwise>   
                        <tr id="${subJob.subTabNo}">
                        </c:otherwise>
                    </c:choose>
                    <td>
                        <input type="checkbox" id="pages${subJob.subTabNo}"/>
                    </td>
                <input type="hidden" id="tab${subJob.subTabNo}" value="${subJob.subTabNo}"/>
                <td>${subJob.subTabName}</td>        
                </tr>
            </c:forEach>
        </c:forEach>
    </table>
    <div class="row" style="margin-top: 5%">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-3">
                    <label>Defined</label>
                    <div style="width:50px; height: 10px; background: #99CCFF"></label> </div>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-3">
            <c:choose>
                <c:when test="${checkModules}">
                    <input type="button" value="Remove" class="btn btn-default col-md-12" onclick="addOrRemove(0)"/>
                </c:when>
                <c:otherwise>
                    <input type="button" value="Remove" class="btn btn-default col-md-12" disabled/>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-md-3"><input type="button" id="btnAssign" value="Add" onclick="addOrRemove(1)" class="btn btn-default col-md-12"/></div>
    </div>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script>
    function addOrRemove(status) {
        if (!setModules()) {
            $("#errmsgForTab").text("Please select atleast one Page");
        } else {
            document.getElementById("btnAssign").disabled = true;
            $("#txtStatus").val(status);
            $.ajax({
                url: '/AxaBankFinance/addOrRemoveModules',
                type: 'POST',
                data: $('#jobs').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadAddUserTypeForm();
                    }, 2000);

                },
                error: function() {
                    alert("Error Loading...!");
                }

            });
        }
    }

    function  setModules() {
        var hasPages = false;
        $('#tblAdminPages tbody tr').each(function() {
            var pageIndex = $(this).attr('id');
            var tabId = $("#tab" + pageIndex).val();
            if ($('#pages' + pageIndex).is(':checked')) {
                hasPages = true;
                $("#hidePages").append("<input type='text' name='modules' value='" + tabId + "'/>");
            }
        });
        return hasPages;
    }
</script>


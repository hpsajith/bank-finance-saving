<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/17/2018
  Time: 9:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function () {
//        $("#tblOtherChargers").chromatable({
//            width: "100%", // specify 100%, auto, or a fixed pixel amount
//            height: "50%",
//            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
//        });
//        $("#tblDocumentCheckList").chromatable({
//            width: "100%", // specify 100%, auto, or a fixed pixel amount
//            height: "50%",
//            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
//        });
//        $("#tblChartOfAccounts").chromatable({
//            width: "100%", // specify 100%, auto, or a fixed pixel amount
//            height: "25%",
//            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
//        });
    });
</script>

<form name="SubAccountTypeForm" id="subAccountType">
    <div class="row">
        <legend>Add Sub Account Type</legend>
    </div>
    <div class="row">
        <input type="hidden" name="subSavingsId" value="${subAccountType.subSavingsId}"/>
        <input type="hidden" name="isActive" value="${subAccountType.isActive}"/>
        <input type="hidden" name="maxCount" value="${subAccountType.maxCount}"/>
        <div class="col-md-5 col-sm-5 col-lg-5">Account Type</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <select name="savingsTypeId" style="width: 172px" id="accountTypes">
                <option selected="selected">SELECT</option>
                <c:forEach var="maccountType" items="${maccountTypes}">
                    <c:choose>
                        <c:when test="${maccountType.savingsTypeId == subAccountType.savingsTypeId}">
                            <option selected="selected"
                                    value="${subAccountType.savingsTypeId}">${maccountType.savingsTypeName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${maccountType.savingsTypeId}">${maccountType.savingsTypeName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
            <label id="msgAccountType" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Sub Account Type Name</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <c:choose>
                <c:when test="${subAccountType.subSavingsName != null}">
                    <input type="text" name="subSavingsName" id="txtSubAccountName"
                           value="${subAccountType.subSavingsName}" onblur="checkSubAccountType()" style="width: 90%"/>
                </c:when>
                <c:otherwise>
                    <input type="text" name="subSavingsName" id="txtSubAccountName"
                           value="${subAccountType.subSavingsName}" onblur="checkSubAccountType()" style="width: 90%"/>
                </c:otherwise>
            </c:choose>
            <label id="msgAccount_TypeName" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Sub Account Code</div>
        <div class="col-md-6 col-sm-6 col-lg-5" style="padding-right: 1px">
            <c:choose>
                <c:when test="${subAccountType.subSavingsCode != null}">
                    <input type="text" name="subSavingsCode" maxlength="5" id="txtSubAccountCode"
                           value="${subAccountType.subSavingsCode}" style="width: 100%"/>
                </c:when>
                <c:otherwise>
                    <input type="text" name="subSavingsCode" maxlength="5" id="txtSubAccountCode"
                           value="${subAccountType.subSavingsCode}" style="width: 100%"/>
                </c:otherwise>
            </c:choose>
            <label id="msgAccountTypeCode" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Interest Rate</div>
        <div class="col-md-5 col-sm-5 col-lg-5">
            <input type="text" onkeypress="return checkNumbers(this)" name="subInterestRate" id="txtInterestRate"
                   value="${subAccountType.subInterestRate}" style="width: 100%"/>
            <label id="msgSubInterestRate" class="msgTextField"></label>
        </div>
        <div class="col-md-1 col-sm-1 col-lg-1" style="padding-left: 1px"><strong>%</strong></div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Interest Apply Type</div>
        <div class="col-md-5 col-sm-5 col-lg-5">

            <select name="interestTypeId" style="width: 172px" id="interestTypes">
                    <c:choose>
                        <c:when test="${subAccountType.isIntrYearly == '1'}">
                            <option value="-1">SELECT</option>
                            <option value="1" selected="selected">Annually</option>
                            <option value="2">Daily</option>
                        </c:when>
                        <c:when test="${subAccountType.isIntrYearly == '0'}">
                            <option value="-1" >SELECT</option>
                            <option value="1">Annually</option>
                            <option value="2" selected="selected">Daily</option>
                        </c:when>
                        <c:otherwise>
                            <option value="-1" selected="selected">SELECT</option>
                            <option value="1">Annually</option>
                            <option value="2">Daily</option>
                        </c:otherwise>
                    </c:choose>
            </select>
            <label id="msgInterestType" class="msgTextField"></label>
        </div>
        <div class="col-md-5 col-sm-5 col-lg-6"></div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Minimum Deposit</div>
        <div class="col-md-6 col-sm-6 col-lg-5" style="padding-right: 1">
            <c:choose>
                <c:when test="${subAccountType.minimumDeposit != null}">
                    <input type="text" name="minimumDeposit" id="txtSubMinimumDeposite"
                           onkeypress="return checkNumbers(this)"
                           value="${subAccountType.minimumDeposit}" style="width: 90%"/>
                </c:when>
                <c:otherwise>
                    <input type="text" name="minimumDeposit" id="txtSubMinimumDeposite"
                           onkeypress="return checkNumbers(this)"
                           value="${subAccountType.minimumDeposit}" style="width: 90%"/>
                </c:otherwise>
            </c:choose>
            <label id="msgMinimumDeposite" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Minimum Account Balance</div>
        <div class="col-md-6 col-sm-6 col-lg-5" style="padding-right: 1">
            <c:choose>
                <c:when test="${subAccountType.minimumAccountBal != null}">
                    <input type="text" name="minimumAccountBal" id="txtSubMinimumBalance"
                           onkeypress="return checkNumbers(this)"
                           value="${subAccountType.minimumAccountBal}" style="width: 90%"/>
                </c:when>
                <c:otherwise>
                    <input type="text" name="minimumAccountBal" id="txtMinimumBalance"
                           onkeypress="return checkNumbers(this)"
                           value="${subAccountType.minimumAccountBal}" style="width: 90%"/>
                </c:otherwise>
            </c:choose>
            <label id="msgMinimumBalance" class="msgTextField"></label>
        </div>
    </div>

    <%--<div class="row">--%>
    <%--<div class="col-md-5 col-sm-5 col-lg-5">Other Benefits</div>--%>
    <%--<div class="col-md-6 col-sm-6 col-lg-7" style="padding-right: 1">--%>
    <%--<c:choose>--%>
    <%--<c:when test="${subAccountType.subOtherBenefits != null}">--%>
    <%--<textarea rows="3" cols="50" name="subOtherBenefits" id="txtOtherBenefits" value="${subAccountType.subOtherBenefits}"--%>
    <%--style="width: 90%" readonly></textarea>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
    <%--<textarea rows="3" cols="50" name="subOtherBenefits" id="txtOtherBenefits" value="${subAccountType.subOtherBenefits}"--%>
    <%--style="width: 90%" readonly></textarea>--%>
    <%--</c:otherwise>--%>
    <%--</c:choose>--%>
    <%--<label id="msgAccount_OtherBenefits" class="msgTextField"></label>--%>
    <%--</div>--%>
    <%--</div>--%>

    <%--<div class="row">--%>
    <%--<div class="col-md-5 col-sm-5 col-lg-5">Overview</div>--%>
    <%--<div class="col-md-6 col-sm-6 col-lg-7" style="padding-right: 1">--%>
    <%--<c:choose>--%>
    <%--<c:when test="${subAccountType.subOverview != null}">--%>
    <%--<textarea rows="3" cols="50" name="subOverview" id="txtOverview" value="${subAccountType.subOverview}"--%>
    <%--style="width: 90%" readonly></textarea>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
    <%--<textarea rows="3" cols="50" name="subOverview" id="txtOverview" value="${subAccountType.subOverview}"--%>
    <%--style="width: 90%" readonly></textarea>--%>
    <%--</c:otherwise>--%>
    <%--</c:choose>--%>
    <%--<label id="msgAccount_Overview" class="msgTextField"></label>--%>
    <%--</div>--%>
    <%--</div>--%>

    <%--<div class="row" style="margin-top: 10px">--%>
    <%--<!--OTHER CHARGES-->--%>
    <%--<div class="col-md-6 col-sm-6 col-lg-6">--%>
    <%--<table id="tblOtherChargers" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0"--%>
    <%--cellpadding="0">--%>
    <%--<thead>--%>
    <%--<tr>--%>
    <%--<th></th>--%>
    <%--<th>Description</th>--%>
    <%--<th>Rate</th>--%>
    <%--</tr>--%>
    <%--</thead>--%>
    <%--<tbody>--%>

    <%--<c:forEach var="vOtherCharge" items="${otherChargesList}">--%>
    <%--<tr id="${vOtherCharge.subTaxId}">--%>
    <%--<c:set var="isCheck" value="false"></c:set>--%>
    <%--<c:forEach var="eOtherCharges" items="${otherCharge}">--%>
    <%--<c:if test="${vOtherCharge.subTaxId == eOtherCharges.MSubTaxCharges.subTaxId}">--%>
    <%--<c:set var="isCheck" value="true"></c:set>--%>
    <%--</c:if>--%>
    <%--</c:forEach>--%>
    <%--<c:choose>--%>
    <%--<c:when test="${isCheck}">--%>
    <%--<td><input type="checkbox" id="otherCharge${vOtherCharge.subTaxId}" checked="true"/></td>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
    <%--<td><input type="checkbox" id="otherCharge${vOtherCharge.subTaxId}"/></td>--%>
    <%--</c:otherwise>--%>
    <%--</c:choose>--%>
    <%--<td>${vOtherCharge.subTaxDescription}</td>--%>
    <%--<c:choose>--%>
    <%--<c:when test="${vOtherCharge.isPrecentage == true}">--%>
    <%--<td>${vOtherCharge.subTaxRate}%</td>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
    <%--<td>Rs:${vOtherCharge.subTaxValue}</td>--%>
    <%--</c:otherwise>--%>
    <%--</c:choose>--%>
    <%--<c:set var="isCheck" value="false"></c:set>--%>
    <%--</tr>--%>
    <%--</c:forEach>--%>
    <%--</tbody>--%>
    <%--</table>--%>
    <%--<div class="dc_clear"></div>--%>
    <%--</div>--%>

    <%--<!--OTHER DOCUMENTS LIST-->--%>
    <%--<div class="col-md-6 col-sm-6 col-lg-6">--%>
    <%--<table id="tblDocumentCheckList" class="dc_fixed_tables table-bordered" width="100%" border="0"--%>
    <%--cellspacing="0" cellpadding="0">--%>
    <%--<thead>--%>
    <%--<tr>--%>
    <%--<th></th>--%>
    <%--<th>Description</th>--%>
    <%--<th>Compulsory</th>--%>
    <%--</tr>--%>
    <%--</thead>--%>
    <%--<tbody>--%>
    <%--<c:forEach var="dCheckList" items="${documentCheckList}">--%>
    <%--<tr id="${dCheckList.id}">--%>
    <%--<c:set var="hasList" value="false"></c:set>--%>
    <%--<c:set var="isCompulsory" value="0"></c:set>--%>
    <%--<c:forEach var="eCheckList" items="${checkList}">--%>
    <%--<c:if test="${dCheckList.id == eCheckList.MCheckList.id}">--%>
    <%--<c:set var="hasList" value="true"></c:set>--%>
    <%--<c:set var="isCompulsory" value="${eCheckList.isCompulsory}"></c:set>--%>
    <%--</c:if>--%>
    <%--</c:forEach>--%>
    <%--<c:choose>--%>
    <%--<c:when test="${hasList}">--%>
    <%--<td><input type="checkbox" id="document${dCheckList.id}" checked="true"/></td>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
    <%--<td><input type="checkbox" id="document${dCheckList.id}"</td>--%>
    <%--</c:otherwise>--%>
    <%--</c:choose>--%>
    <%--<td>${dCheckList.listDescription}</td>--%>
    <%--<td><select id="document_com${dCheckList.id}">--%>
    <%--<c:choose>--%>
    <%--<c:when test="${isCompulsory == 1}">--%>
    <%--<option value='0'>No</option>--%>
    <%--<option value='1' selected="selected">Yes</option>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
    <%--<option value='0'>No</option>--%>
    <%--<option value='1'>Yes</option>--%>
    <%--</c:otherwise>--%>
    <%--</c:choose>--%>
    <%--</select></td>--%>
    <%--<c:set var="hasList" value="false"></c:set>--%>
    <%--</tr>--%>
    <%--</c:forEach>--%>
    <%--</tbody>--%>
    <%--</table>--%>
    <%--<div class="dc_clear"></div>--%>
    <%--</div>--%>
    <%--</div>--%>

    <div class="row" style="margin-top: 20px;margin-bottom: 5px;">
        <div class="col-md-6 col-sm-6 col-lg-6"></div>
        <div class="col-md-3 col-sm-3 col-lg-3">
            <input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12"
                   onclick="pageExit()"/>
        </div>
        <div class="col-md-3 col-sm-3 col-lg-3" style="">
            <input type="button" id="btnSaveSubAccountType" name="" value="Save" onclick="saveSubAccountType()"
                   class="btn btn-default col-md-12 col-sm-12 col-lg-12"/>
        </div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <div id="hideChartOfAccounts"></div>
    <div id="hideOtherCharge"></div>
    <div id="hideDocumentCheckList"></div>
</form>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>

    //    $(function () {
    //        $("#checkInsurance").val('0');
    //    });

    function saveSubAccountType() {
//        if (fieldValidate()) {
        document.getElementById("btnSaveSubAccountType").disabled = true;
//            setToChartOfAccounts();
//            setToOtherCharge();
//            setToDocumentCheckList();
        $.ajax({
            url: '/AxaBankFinance/SavingsController/saveOrUpdateSubSavingsType',
            type: 'post',
            data: $('#subAccountType').serialize(),
            success: function (data) {
                $('#msgDiv').html("");
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function () {
                    loadSubAccountTypeForm();
                }, 2000);
                document.getElementById("btnSaveSubAccountType").disabled = false;
            },
            error: function () {
                alert("Error Loading..!");
            }
        });
//        }
    }

    //    function setToChartOfAccounts() {
    //        var hideChartOfAccounts = document.getElementById("hideChartOfAccounts");
    //        var intrAcc = document.getElementById("intrAcc").value;
    //        var receAcc = document.getElementById("receAcc").value;
    //        var intrsAcc = document.getElementById("intrsAcc").value;
    //        var paybAcc = document.getElementById("paybAcc").value;
    //        var dwnAcc = document.getElementById("dwnAcc").value;
    //        var odiAcc = document.getElementById("odiAcc").value;
    //
    //        var ovpAcc = document.getElementById("ovpAcc").value;
    //        var intSuspense = document.getElementById("intSuspense").value;
    //        var debtorAcc = document.getElementById("debtorAcc").value;
    //        var check = $("#checkInsurance").is(":checked");
    //        if (intrAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + intrAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INTR'/>");
    //        }
    //        if (receAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + receAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='RECE'/>");
    //        }
    //        if (intrsAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + intrsAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INTRS'/>");
    //        }
    //        if (paybAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + paybAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='PAYBLE'/>");
    //        }
    //        if (debtorAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + debtorAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='DEBT'/>");
    //        }
    //        if (dwnAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + dwnAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='DWN'/>");
    //        }
    //        if (odiAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + odiAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='ODI'/>");
    //        }
    //        if (ovpAcc != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + ovpAcc + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='OVP'/>");
    //        }
    //        if (intSuspense != null) {
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + intSuspense + "'/>");
    //            $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INTSUSP'/>");
    //        }
    //        if (check) {
    //            var insCreditAcc = document.getElementById("insCreditAcc").value;
    //            var insPayAcc = document.getElementById("insPayAcc").value;
    //            if (insCreditAcc != null) {
    //                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + insCreditAcc + "'/>");
    //                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INS_P'/>");
    //            }
    //            if (insPayAcc != null) {
    //                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccount' value='" + insPayAcc + "'/>");
    //                $(hideChartOfAccounts).append("<input type ='hidden' name = 'chartOfAccountName' value='INS_C'/>");
    //            }
    //        }
    //
    //    }

    //    function setToOtherCharge() {
    //        var hasOtherCharge = false;
    //        $('#tblOtherChargers tbody tr').each(function () {
    //            var otherChargeId = $(this).attr('id');
    //            if ($('#otherCharge' + otherChargeId).is(':checked')) {
    //                hasOtherCharge = true;
    //                $("#hideOtherCharge").append("<input type = 'hidden' name = 'otherChargeID' value = '" + otherChargeId + "'/>");
    //            }
    //        });
    //        if (!hasOtherCharge) {
    //            $("#hideOtherCharge").append("<input type = 'hidden' name = 'otherChargeID' value = '0'/>");
    //        }
    //    }
    //
    //    function setToDocumentCheckList() {
    //        var hasDocumentList = false;
    //        $('#tblDocumentCheckList tbody tr').each(function () {
    //            var documentId = $(this).attr('id');
    //            if ($('#document' + documentId).is(':checked')) {
    //                hasDocumentList = true;
    //                var isCompulsory = $('#document_com' + documentId).find("option:selected").val();
    //                $('#hideDocumentCheckList').append("<input type = 'hidden' name = 'documentCheckListId' value = '" + documentId + "'/>" +
    //                    "<input type = 'hidden' name = 'documentIsCompulsory' value ='" + isCompulsory + "'/>");
    //            }
    //        });
    //        if (!hasDocumentList) {
    //            $('#hideDocumentCheckList').append("<input type = 'hidden' name = 'documentCheckListId' value = '0'/>" +
    //                "<input type = 'hidden' name = 'documentIsCompulsory' value ='0'/>");
    //        }
    //    }

    //    function fieldValidate() {
    //        var subAccountName = document.getElementById('txtSubAccountName');
    //        var rate = document.getElementById('txtInterestRate');
    //        var subAccountCode = document.getElementById('txtSubAccountCode');
    //        var accounttype = $("#accountTypes").find("option:selected").val();
    //        var intrAcc = document.getElementById("intrAcc").value;
    //        var receAcc = document.getElementById("receAcc").value;
    //        var intrsAcc = document.getElementById("intrsAcc").value;
    //        var paybAcc = document.getElementById("paybAcc").value;
    //        var dwnAcc = document.getElementById("dwnAcc").value;
    //        var odiAcc = document.getElementById("odiAcc").value;
    //        var ovpAcc = document.getElementById("ovpAcc").value;
    //        var debtorAcc = document.getElementById("debtorAcc").value;
    //        var check = $("#checkInsurance").is(":checked");
    //        if (check) {
    //            var insCreditAcc = document.getElementById("insCreditAcc").value;
    //            var insPayAcc = document.getElementById("insPayAcc").value;
    //        }
    //
    //        if (accounttype === 0) {
    //            $("#msgAccountType").html("Account Type must be Selected");
    //            $("#accountTypes").addClass("txtError");
    //            $("#accountTypes").focus();
    //            return false;
    //        } else {
    //            $("#accountTypes").removeClass("txtError");
    //            $("#msgAccountType").html("");
    //        }
    //        if (subAccountName.value === null || subAccountName.value === "") {
    //            $("#msgAccount_TypeName").html("Sub Account Name must be filled out");
    //            $("#txtSubAccountName").addClass("txtError");
    //            $("#accountTypes").focus();
    //            return false;
    //        } else {
    //            $("#txtSubAccountName").removeClass("txtError");
    //            $("#msgAccount_TypeName").html("");
    //        }
    //        if (rate.value === null || rate.value === "") {
    //            $("#msgSubInterestRate").html("Rate must be filled out");
    //            $("#txtInterestRate").addClass("txtError");
    //            $("#txtInterestRate").focus();
    //            return false;
    //        } else {
    //            $("#txtInterestRate").removeClass("txtError");
    //            $("#msgSubInterestRate").html("");
    //        }
    //        if (subAccountCode.value === null || subAccountCode.value === "") {
    //            $("#msgAccountTypeCode").html("Sub Account Code must be filled out");
    //            $("#txtSubAccountCode").addClass("txtError");
    //            $("#txtSubAccountCode").focus();
    //            return false;
    //        } else {
    //            $("#txtSubAccountCode").removeClass("txtError");
    //            $("#msgAccountTypeCode").html("");
    //        }
    //        if (intrAcc === null || intrAcc === "") {
    //            $("#msgIntrAcc").html("Enter Interest Account Code");
    //            $("#intrAcc").addClass("txtError");
    //            $("#intrAcc").focus();
    //            return false;
    //        } else {
    //            $("#intrAcc").removeClass("txtError");
    //            $("#msgIntrAcc").html("");
    //        }
    //        if (receAcc === null || receAcc === "") {
    //            $("#msgReceAcc").html("Enter Reciveble Account Code");
    //            $("#receAcc").addClass("txtError");
    //            $("#receAcc").focus();
    //            return false;
    //        } else {
    //            $("#receAcc").removeClass("txtError");
    //            $("#msgReceAcc").html("");
    //        }
    //        if (intrsAcc === null || intrsAcc === "") {
    //            $("#msgIntrsAcc").html("Enter Intresest Suspend Account Code");
    //            $("#intrsAcc").addClass("txtError");
    //            $("#intrsAcc").focus();
    //            return false;
    //        } else {
    //            $("#intrsAcc").removeClass("txtError");
    //            $("#msgIntrsAcc").html("");
    //        }
    //        if (paybAcc === null || paybAcc === "") {
    //            $("#msgPaybAcc").html("Enter Payable Account Code");
    //            $("#paybAcc").addClass("txtError");
    //            $("#paybAcc").focus();
    //            return false;
    //        } else {
    //            $("#paybAcc").removeClass("txtError");
    //            $("#msgPaybAcc").html("");
    //        }
    //        if (debtorAcc === null || debtorAcc === "") {
    //            $("#msgDebtorAcc").html("Enter Debtor Account Code");
    //            $("#debtorAcc").addClass("txtError");
    //            $("#debtorAcc").focus();
    //            return false;
    //        } else {
    //            $("#debtorAcc").removeClass("txtError");
    //            $("#msgDebtorAcc").html("");
    //        }
    //
    //        if (dwnAcc === null || dwnAcc === "") {
    //            $("#msgDwnAcc").html("Enter Down Payment Account Code");
    //            $("#dwnAcc").addClass("txtError");
    //            $("#dwnAcc").focus();
    //            return false;
    //        } else {
    //            $("#dwnAcc").removeClass("txtError");
    //            $("#msgDwnAcc").html("");
    //        }
    //        if (odiAcc === null || odiAcc === "") {
    //            $("#msgOdiAcc").html("Enter ODI Account Code");
    //            $("#odiAcc").addClass("txtError");
    //            $("#odiAcc").focus();
    //            return false;
    //        } else {
    //            $("#odiAcc").removeClass("txtError");
    //            $("#msgOdiAcc").html("");
    //        }
    //        if (ovpAcc === null || ovpAcc === "") {
    //            $("#msgOvpAcc").html("Enter OVP Account Code");
    //            $("#ovpAcc").addClass("txtError");
    //            $("#ovpAcc").focus();
    //            return false;
    //        } else {
    //            $("#ovpAcc").removeClass("txtError");
    //            $("#msgOvpAcc").html("");
    //        }
    //        if (check) {
    //            if (insCreditAcc === null || insCreditAcc === "") {
    //                $("#msgInsCreditAcc").html("Enter Ins Commission Account Code");
    //                $("#insCreditAcc").addClass("txtError");
    //                $("#insCreditAcc").focus();
    //                return false;
    //            } else {
    //                $("#insCreditAcc").removeClass("txtError");
    //                $("#msgInsCreditAcc").html("");
    //            }
    //            if (insPayAcc === null || insPayAcc === "") {
    //                $("#msgInsPayAcc").html("Enter Ins Pay Account Code");
    //                $("#insPayAcc").addClass("txtError");
    //                $("#insPayAcc").focus();
    //                return false;
    //            } else {
    //                $("#insPayAcc").removeClass("txtError");
    //                $("#msgInsPayAcc").html("");
    //            }
    //        }
    //
    //        return true;
    //    }

    //    function concatRate() {//already commented(check before uncommenting)
    //        var interestRate = document.getElementById("txtInterestRate").value;
    //        var rate = interestRate + "%";
    //        document.getElementById("txtHiddenRate").value = rate;
    //    }

    //    function showHide() {
    //        if ($("#checkInsurance").is(":checked")) {
    //            $("#InsuranceCredit").show();
    //            $("#insCreditAcc").val('');
    //            $("#InsurancePayable").show();
    //            $("#insPayAcc").val('');
    //            $("#checkInsurance").val('1');
    //        } else {
    //            $("#InsuranceCredit").hide();
    //            $("#insCreditAcc").val('');
    //            $("#InsurancePayable").hide();
    //            $("#insPayAcc").val('');
    //            $("#checkInsurance").val('0');
    //        }
    //    }

    function checkSubAccountType() {
        var subAccountName = document.getElementById('txtSubAccountName').value;
        var checkBtn = document.getElementById("btnSaveSubAccountType").value;
        if (subAccountName !== null && subAccountName !== "" && checkBtn !== null && checkBtn !== "Update") {
            $.ajax({
                url: '/AxaBankFinance/subAccountTypeCheck/' + subAccountName,
                success: function (data) {
                    if (data === 1) {
                        $("#msgAccount_TypeName").html("Sub Account Name Is Alredy Exits");
                        $("#txtSubAccountName").addClass("txtError");
                        $("#txtSubAccountName").focus();
                        document.getElementById("btnSaveSubAccountType").disabled = true;
                    } else {
                        $("#txtSubAccountName").removeClass("txtError");
                        $("#msgAccount_TypeName").html("");
                        $("#msgAccount_TypeName").removeClass("msgTextField");
                        document.getElementById("btnSaveSubAccountType").disabled = false;
                    }
                }
            });
        }
    }

</script>
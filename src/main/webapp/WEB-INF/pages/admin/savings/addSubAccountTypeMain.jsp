<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/17/2018
  Time: 9:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">

    $(document).ready(function() {
        pageAuthentication();
        $("#viewSubAccountType").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "85%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });

</script>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6" style="" id="divAddSubAccountType">
            <!--add Sub Account Type form-->
        </div>

        <div class="col-md-1 col-sm-1 col-lg-1" style=""></div>
        <div class="col-md-5 col-sm-5 col-lg-5 " style="margin-top: 20px">
            <div class="row">
                <table id="viewSubAccountType" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th style="text-align: center">Sub Account Type</th>
                        <th style="text-align: center">Rate</th>
                        <th style="text-align: center">Edit</th>
                        <c:if test="${isSuperAdmin}">
                            <th style="text-align: center">In-Active</th>
                        </c:if>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="subAccountType" items="${mSubAccountTypes}">
                        <c:choose>
                            <c:when test="${subAccountType.isActive}">
                                <tr id="${subAccountType.subSavingsId}">
                            </c:when>
                            <c:otherwise>
                                <tr id="${subAccountType.subSavingsId}" style='background-color: #B8B8B8 ;color: #000000'>
                            </c:otherwise>
                        </c:choose>
                        <td style="text-align: center">${subAccountType.subSavingsName}</td>
                        <td style="text-align: center">${subAccountType.subInterestRate}</td>
                        <td style="text-align: center">
                            <c:choose>
                                <c:when test="${subAccountType.isActive}">
                                    <div class='edit' href='#' onclick='viewToSubAccountType(${subAccountType.subSavingsId})'></div>
                                </c:when>
                                <c:otherwise>
                                    <div class='edit' href='#'></div>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <c:if test="${isSuperAdmin}">
                            <td style="text-align: center">
                                <c:choose>
                                    <c:when test="${subAccountType.isActive}">
                                        <div class="delete" href='#' title="In-Active" onclick="changeSubAccountType(${subAccountType.subSavingsId}, 0)"></div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="btnActive" href='#' title="Active" onclick="changeSubAccountType(${subAccountType.subSavingsId}, 1)"></div>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="dc_clear"></div>
            </div>
        </div>
    </div>
</div>


</div>
<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '../AxaBankFinance/addSubAccountTypeForm',
            success: function(data) {
                $('#divAddSubAccountType').html(data);
            },
            error: function() {
                alert("Error View..!");
            }
        });
    });

    function viewToSubAccountType(Id) {
        $.ajax({
            url: '/AxaBankFinance/viewSubAccountTypeById/' + Id,
//            url: '/AxaBankFinance/viewSubAccountTypeById/' + Id,
            success: function(data) {
                $('#divAddSubAccountType').html("");
                $('#divAddSubAccountType').html(data);
                document.getElementById("btnSaveSubAccountType").value = "Update";
            },
            error: function() {
                alert("Error View..!");
            }
        });
    }

    function changeSubAccountType(subAccountTypeId, status) {
        $.ajax({
            url: "/AxaBankFinance/changeSubAccountType",
            data: {"subAccountTypeId": subAccountTypeId, "status": status},
            success: function(data) {
                $('#msgDiv').html("");
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function() {
                    loadSubAccountTypeForm();
                }, 2000);
            },
            error: function() {
                console.log("Error in change Sub Account type");
            }
        });
    }

</script>
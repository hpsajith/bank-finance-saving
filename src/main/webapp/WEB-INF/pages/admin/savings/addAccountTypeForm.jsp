<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/16/2018
  Time: 3:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
        $("#viewAccountCheckList").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "500px",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<form name="addAccountTypeForm" id="addAccountType">
    <div class="row" style="margin: 0px 8px 2px 0px"><legend>Add Account Type</legend></div>
    <div class="row">
        <input type="hidden" name="savingsTypeId" value="${addAccountType.savingsTypeId}"/>
        <input type="hidden" name="isActive" value="${addAccountType.isActive}"/>
        <div class="col-md-5 col-sm-5 col-lg-5">Account Type Name</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text"name="savingsTypeName" id="txtAccountTypeName" style="width: 100%" value="${addAccountType.savingsTypeName}" />
            <label id="msgAccountTypeName" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Account Type Code</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text"name="savingsTypeCode" maxlength="5" min="2" id="txtAccountTypeCode" style="width: 100%" value="${addAccountType.savingsTypeCode}" />
            <label id="msgAccountTypeCode" class="msgTextField"></label>
        </div>
    </div>
    <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
        <div  class="col-md-6 col-sm-6 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveAccountType" value="Save" onclick="saveUpdateAccountType()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script type="text/javascript">
    function saveUpdateAccountType() {
        if (fieldValidate()) {
            document.getElementById("btnSaveAccountType").disabled = true;
            // setCheckList();
            $.ajax({
                url: '/AxaBankFinance/SavingsController/saveOrUpdateSavingsType',
                type: 'POST',
                data: $('#addAccountType').serialize(),
                success: function(data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadAddAccountTypeForm();
                    }, 2000);
                },
                error: function() {
                    alert("error Loading");
                }
            });
        }
    }
    function fieldValidate() {
        var accountTypeName = document.getElementById('txtAccountTypeName');
        var accountTypeCode = document.getElementById('txtAccountTypeCode');
        if (accountTypeName.value == null || accountTypeName.value == "") {
            $("#msgAccountTypeName").html("Name must be filled out");
            $("#txtAccountTypeName").addClass("txtError");
            $("#txtAccountTypeName").focus();
            return false;
        } else {
            $("#txtAccountTypeName").removeClass("txtError");
            $("#msgAccountTypeName").html("");
        }
        if (accountTypeCode.value == null || accountTypeCode.value == "") {
            $("#msgAccountTypeCode").html("Code must be filled out");
            $("#txAccountTypeCode").addClass("txtError");
            $("#txtAccountTypeCode").focus();
            return false;
        } else {
            $("#txtAccountTypeCode").removeClass("txtError");
            $("#msgAccountTypeCode").html("");
        }

        return true;
    }

</script>


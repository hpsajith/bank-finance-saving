<%--
  Created by IntelliJ IDEA.
  User: ITES
  Date: 5/16/2018
  Time: 3:56 PM
  To change this template use File | Settings | File Templates.
--%>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#viewAccountType").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "75%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });

</script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-12 col-sm-12 col-lg-12" style="width: 100%">
        <div class="row">
            <div class="col-md-5 col-sm-5 col-lg-5" id="divAccountType" style="margin: 24px;">
                <!--add Account Type Div-->
            </div>
            <div class="col-md-2 col-sm-2 col-lg-2" style="margin-right: -40px"></div>

            <div class="col-md-5 col-sm-5 col-lg-5" id="divViewAccountType" style="margin: 45px">
                <div class="row" style="overflow-y: auto;height: 85%">
                    <table id="viewAccountType" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr>
                            <th style="text-align: center">Account Type</th>
                            <th style="text-align: center">Account Code</th>
                            <th style="text-align: center">Edit</th>
                            <c:if test="${isSuperAdmin}">
                                <th style="text-align: center">In-Active</th>
                            </c:if>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="accountType" items="${mAccountTypes}">
                            <c:choose>
                                <c:when test="${accountType.isActive}">
                                    <tr id="${accountType.savingsTypeId}">
                                </c:when>
                                <c:otherwise>
                                    <tr id="${accountType.savingsTypeId}" style='background-color: #B8B8B8 ;color: #000000'>
                                </c:otherwise>
                            </c:choose>
                            <td style="text-align: center">${accountType.savingsTypeName}</td>
                            <td style="text-align: center">${accountType.savingsTypeCode}</td>
                            <td style="text-align: center">
                                <c:choose>
                                    <c:when test="${accountType.isActive}">
                                        <div class='edit' href='#' onclick='clickToEdit(${accountType.savingsTypeId})'></div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class='edit' href='#'></div>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <c:if test="${isSuperAdmin}">
                                <td style="text-align: center">
                                    <c:choose>
                                        <c:when test="${accountType.isActive}">
                                            <div class="delete" href='#' title="In-Active" onclick="changeAccountType(${accountType.savingsTypeId}, 0)"></div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="btnActive" href='#' title="Active" onclick="changeAccountType(${accountType.savingsTypeId}, 1)"></div>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </c:if>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="dc_clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/addAccountTypeForm',
            success: function(data) {
                $('#divAccountType').html(data);
            },
            error: function() {
            }
        });
    });

    function clickToEdit(ID) {
        // alert("msd");
        $.ajax({
            url: '/AxaBankFinance/viewAccountTypeById/' + ID,
            success: function(data) {
                $('#divAccountType').html("");
                $('#divAccountType').html(data);
                document.getElementById("btnSaveAccountType").value = "Update";
            },
            error: function() {
                alert("Error View..!");
            }
        });
    }

    function changeAccountType(accountTypeId, status) {
        $.ajax({
            url: "/AxaBankFinance/changeAccountType",
            data: {"accountTypeId": accountTypeId, "status": status},
            success: function(data) {
                $('#msgDiv').html("");
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function() {
                    loadAddAccountTypeForm();
                }, 2000);
            },
            error: function() {
                console.log("Error in change Account type");
            }
        });
    }

</script>
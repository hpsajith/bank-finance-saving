<%-- 
    Document   : addChartOfAccount
    Created on : Dec 21, 2015, 1:52:08 PM
    Author     : ITES
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        unblockui();
    });
</script>
<div class="row" style="margin-left: 20px"><legend>Chart Of Account</legend></div>
<div class="row">
    <form id="formChartOfAcc">
        <div class="col-md-4 col-sm-4 col-lg-4">
            <div style="margin-right: 30px"><label>Finance Accounts</label></div>
            <select size="3" id="listFromLeft" name="fromLeft"  multiple= "multiple" style=" width: 300px; height: 500px; margin-left: 10px" id="list1">
                <c:forEach var="chart" items="${charOfList}">
                    <option value='{"acSubCode":"${chart.acSubCode}","accountNo":"${chart.accountNo}","accountType":"${chart.accountType}","accountName":"${chart.accountName}","representDept":"${chart.representDept}","description":"${chart.description}"}'>${chart.accountName}</option>
                </c:forEach>
            </select>
        </div>
        <div class="col-md-3 col-sm-3 col-lg-3 ">
            <div class="row"><div  class="col-md-6 col-sm-6 col-lg-6" style="margin-bottom: 200px"></div>
                <div  class="col-md-12 col-sm-12 col-lg-12" style="margin-left: 50px"><input type="button" value=">>" onclick="move(this.form.fromLeft, this.form.toRight)" class="btn btn-default col-md-6 col-sm-6 col-lg-6"/></div>
            </div>
            <div class="row"><div  class="col-md-6 col-sm-6 col-lg-6" style="margin-bottom: 5px"></div>
                <div  class="col-md-12 col-sm-12 col-lg-12" style="margin-left: 50px"><input type="button" value="<<" onclick="move(this.form.toRight, this.form.fromLeft)" class="btn btn-default col-md-6 col-sm-6 col-lg-6"/></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-lg-4">
            <div><label>Bank Finance Accounts</label></div>
            <select size="3" id="listToRight" name="toRight"  multiple= "multiple" style=" width: 300px; height: 500px" id="list2">
            </select>
        </div>        
    </form>
</div>
<div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-4">
        <div style="margin-top:10px;margin-left:2px" class="col-md-2">
            <input type="button" class="btn btn-default" value="Cancel" onclick="pageExit()" style="width:90px">
        </div>  	
        <div style="margin-top:10px;margin-left:55" class="col-md-2">
            <input type="button" class="btn btn-default" value="Save" id="btnSaveChartofAccount" onclick="save()" style="width:100px">
        </div>
    </div>
</div>
<div id="message" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>
<script type="text/javascript">

    function move(tbFrom, tbTo) {
        var arrFrom = new Array();
        var arrTo = new Array();
        var arrLU = new Array();
        var i;
        for (i = 0; i < tbTo.options.length; i++) {
            arrLU[tbTo.options[i].text] = tbTo.options[i].value;
            arrTo[i] = tbTo.options[i].text;
        }
        var fLength = 0;
        var tLength = arrTo.length;
        for (i = 0; i < tbFrom.options.length; i++) {
            arrLU[tbFrom.options[i].text] = tbFrom.options[i].value;
            if (tbFrom.options[i].selected && tbFrom.options[i].value !== "") {
                arrTo[tLength] = tbFrom.options[i].text;
                tLength++;
            }
            else {
                arrFrom[fLength] = tbFrom.options[i].text;
                fLength++;
            }
        }

        tbFrom.length = 0;
        tbTo.length = 0;
        var ii;

        for (ii = 0; ii < arrFrom.length; ii++) {
            var no = new Option();
            no.value = arrLU[arrFrom[ii]];
            no.text = arrFrom[ii];
            tbFrom[ii] = no;
        }

        for (ii = 0; ii < arrTo.length; ii++) {
            var no = new Option();
            no.value = arrLU[arrTo[ii]];
            no.text = arrTo[ii];
            tbTo[ii] = no;
        }
    }

    function save() {
        var accounts = [];
        $.each($("#listToRight option"), function() {
            accounts.push(jQuery.parseJSON($(this).val()));
        });
        //console.log(accounts);
        //console.log(JSON.stringify({"accounts": accounts}));
        if (accounts.length > 0) {
            document.getElementById("btnSaveChartofAccount").disabled = true;
            $.ajax({
                url: "/AxaBankFinance/saveChartOfAccounts?${_csrf.parameterName}=${_csrf.token}",
                type: "POST",
                contentType: 'application/json',
                data: JSON.stringify({"accounts": accounts}),
                success: function(data) {
                    $("#message").html("");
                    $("#message").html(data);
                    ui("#message");
                    setTimeout(function() {
                        loadChartOfAccount();
                    }, 2000);
                    document.getElementById("btnSaveChartofAccount").disabled = false;

                }
            });
        }
    }

</script>


<%-- 
    Document   : addSeizerForm
    Created on : Feb 9, 2016, 8:24:43 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form name="newSeizerForm" id="newSeizer">
    <div class="row"><legend><strong>Add New Seizer Details</strong></legend></div>
    <input type="hidden" name="seizerId" id="txtSeizerId" value="${seizer.seizerId}"/>
    <input type="hidden" name="seizerAccountNo" id="txtSeizerAccountNo" value="${seizer.seizerAccountNo}"/>
    <div class="row">
        <div class="col-md-4">Title <label class="redmsg">*</label></div>
        <div class="col-md-8">
            <select name ="seizerTitle" id="txtTitle" style="width: 80%">
                <option value="0">SELECT</option>
                <c:choose>
                    <c:when test="${seizer.seizerTitle == 'Mr'}">
                        <option value="Mr" selected="true">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                    </c:when>
                    <c:when test="${seizer.seizerTitle == 'Mrs'}">
                        <option value="Mr">Mr</option>
                        <option value="Mrs" selected="true">Mrs</option>
                        <option value="Miss">Miss</option>
                    </c:when>
                    <c:when test="${seizer.seizerTitle == 'Miss'}">
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss" selected="true">Miss</option>
                    </c:when>
                    <c:otherwise>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                    </c:otherwise>
                </c:choose>
            </select>
            <label id="msgTitle" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">Name <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtSeizerName" name="seizerName" value="${seizer.seizerName}" style="width: 80%"/>
            <label id="msgSeizerName" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">Address <label class="redmsg"></label></div>
        <div class="col-md-8"><textarea type="text" id="txtseizerAddress" name="seizerAddress" style="width: 80%">${seizer.seizerAddress}</textarea>
            <label id="msgSeizerAddress" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">Contact Mobile <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtMobileNo" name="seizerContactMobile" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" value="${seizer.seizerContactMobile}"style="width: 80%"/>
            <label id="msgMobileNo" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">Contact Office No</div>
        <div class="col-md-8"><input type="text" id="txtOfficeNo"  name="seizerContactOffice" value="${seizer.seizerContactOffice}" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
    </div>
    <div class="row">
        <div class="col-md-4">Emergency Contact No</div>
        <div class="col-md-8"><input type="text" id="txtEmergencyNo"   name="emergencyContactMobile" value="${seizer.emergencyContactMobile}" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
    </div>
    <div class="row">
        <div class="col-md-4">Email</div>
        <div class="col-md-8"><input type="text" name="seizerEmail" id="txtseizerEmail" onblur="validateEmail(this, '#msgEmail1')" value="${seizer.seizerEmail}"style="width: 80%"/>
            <label id="msgEmail1"></label>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div  class="col-md-4"></div>
        <div  class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3"><input type="button" name="" value="Save"id="btnsaveSeizer" onclick="saveNewSeizer()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
        <div  class="col-md-2"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<script>
    function saveNewSeizer() {
        if (fieldValidate()) {
            document.getElementById("btnsaveSeizer").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateNewSeizer',
                type: 'POST',
                data: $('#newSeizer').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadNewSeizerMain();
                    }, 2000);
                },
                error: function() {
                    alert("Error in Saving Seizer Details..!");
                }
            });
        }
    }
    function fieldValidate() {
        var name = document.getElementById('txtSeizerName');
        var address = document.getElementById('txtseizerAddress');
        var mobileNo = document.getElementById('txtMobileNo');
        var title = $("#txtTitle").find('option:selected').val();
        if (title == 0) {
            $("#msgTitle").html("Select The Title");
            $("#txtTitle").addClass("txtError");
            $("#txtTitle").focus();
            return false;
        } else {
            $("#txtTitle").removeClass("txtError");
            $("#msgTitle").html("");
            $("#msgTitle").removeClass("msgTextField");
        }
        if (name.value == null || name.value == "") {
            $("#msgSeizerName").html("Name must be filled out");
            $("#txtFname").addClass("txtError");
            $("#txtFname").focus();
            return false;
        } else {
            $("#txtFname").removeClass("txtError");
            $("#msgSeizerName").html("");
            $("#msgSeizerName").removeClass("msgTextField");
        }
        if (address.value == null || address.value == "") {
            $("#msgSeizerAddress").html("Address must be filled out");
            $("#txtseizerAddress").addClass("txtError");
            $("#txtseizerAddress").focus();
            return false;
        } else {
            $("#txtseizerAddress").removeClass("txtError");
            $("#msgSeizerAddress").html("");
            $("#msgSeizerAddress").removeClass("msgTextField");
        }
        if (mobileNo.value == null || mobileNo.value == "") {
            $("#msgMobileNo").html("Mobile No must be filled out");
            $("#txtMobileNo").addClass("txtError");
            $("#txtMobileNo").focus();
            return false;
        } else {
            $("#txtMobileNo").removeClass("txtError");
            $("#msgMobileNo").html("");
            $("#msgMobileNo").removeClass("msgTextField");
        }
        return true;
    }
</script>

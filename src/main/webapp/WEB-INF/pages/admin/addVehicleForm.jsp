<%-- 
    Document   : addVehicleForm
    Created on : Jul 27, 2016, 10:19:18 AM
    Author     : User
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form name="vehicleTypeForm" id="vehicleType">
    <input type="hidden" name="vehicleType" id="txtvehicleTypeId" value="${vehicle.vehicleType}"/>

    <div class="row"><legend><strong>Add New Vehicle Details</strong></legend></div>

    <div class="row">
        <div class="row" style="padding-left: 30px; padding-bottom: 10px"><strong>Add New Vehicle Type</strong></div>
        <div class="col-md-4">Vehicle Type <label class="redmsg">*</label></div>
        <div class="col-md-7"><input type="text" id="txtVehicleType" name="vehicleTypeName" value="${vehicle.vehicleTypeName}" style="width: 100%"/>
            <label id="msgVehicleType" class="msgTextField"></label>
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row" style="margin-top: 5px">        
        <div  class="col-md-4"></div>
        <div  class="col-md-7" style="margin-top: 10px">
            <div class="row">
                <div  class="col-md-6">
                    <input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()" style="width: 100%"/>
                </div>
                <div  class="col-md-6">
                    <input type="button" name="" value="Save"id="btnsaveVehicleType" onclick="saveNewVehicleType()" class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="width: 100%"/>
                </div>
            </div>
        </div>

        <div  class="col-md-4"></div>
        <div  class="col-md-7">
            <c:choose>
                <c:when test="${vehicle.vehicleType != null}">
                    <div class="row">
                        <div  class="col-md-6">
                            <input type="button" name="" value="Make"id="btnMake" onclick="loadMakeForm()" class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="width: 100%"/>
                        </div>
                        <div  class="col-md-6">
                            <input type="button" name="" value="Model"id="btnModel" onclick="loadModelForm()"  class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="width: 100%"/>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div  class="col-md-6">
                            <input type="button" name="" value="Make"id="btnMake" onclick="loadMakeForm()" class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="width: 100%" disabled/>
                        </div>
                        <div  class="col-md-6">
                            <input type="button" name="" value="Model"id="btnModel" onclick="loadModelForm()"  class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="width: 100%" disabled/>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>  
        <div  class="col-md-1"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<div id="divMake" class="jobsPopUp" style="width: 35%;height: 50%;overflow-y: auto"></div>
<div id="divModel" class="jobsPopUp" style="width: 35%;height: 50%;overflow-y: auto"></div>
<script>
    function saveNewVehicleType() {
        if (fieldValidate()) {
            document.getElementById("btnsaveVehicleType").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateVehicleType',
                type: 'POST',
                data: $('#vehicleType').serialize(),
                success: function (data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function () {
                        loadAddVehicleMainPage();
                    }, 2000);
                },
                error: function () {
                }
            });
        }
    }

    function loadMakeForm() {
        var vehicleType = $("#txtvehicleTypeId").val();
        $.ajax({
            url: '/AxaBankFinance/loadAddMakeForm/' + vehicleType,
            success: function (data) {
                $('#divMake').html("");
                $('#divMake').html(data);
                ui('#divMake');
            }
        });
    }

    function loadModelForm() {
        var vehicleType = $("#txtvehicleTypeId").val();
        $.ajax({
            url: '/AxaBankFinance/loadAddModelForm',
            data: {"vehicleType": vehicleType},
            success: function (data) {
                $('#divModel').html("");
                $('#divModel').html(data);
                ui('#divModel');
            }
        });
    }

    function fieldValidate() {
        var vehicleType = document.getElementById('txtVehicleType');
        if (vehicleType.value == null || vehicleType.value == "") {
            $("#msgVehicleType").html("Vehicle Type must be filled out");
            $("#txtVehicleType").addClass("txtError");
            $("#txtVehicleType").focus();
            return false;
        } else {
            $("#txtVehicleType").removeClass("txtError");
            $("#msgVehicleType").html("");
            $("#msgVehicleType").removeClass("msgTextField");
        }
        return true;
    }
</script>
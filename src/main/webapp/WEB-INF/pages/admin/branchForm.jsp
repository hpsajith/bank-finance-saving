<%-- 
    Document   : branchForm
    Created on : May 8, 2015, 5:45:40 PM
    Author     : MsD
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        $("#").chromatable({
            width: "100%", //specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<div class="row" style="margin:-25 305 5 0"><strong>Add Branch</strong></div>
<div class="container-fluid">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row" style="margin: 1px; color: red; text-align: left;"><label id="errmsgForTab"></label></div>
    <form name="userTypeBranchForm" id="userTypeBranch">
        <input type="hidden" name="empID" id="" value="${empId}"/>

        <table id="tblBranch"  class="dc_fixed_tables table-bordered" width="100%" border="3" cellspacing="1" cellpadding="1">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Branch Name</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="v_branch" items="${branchList}">
                    <tr id="${v_branch.branchId}">
                        <c:set var="isBranch" value="false"></c:set>
                        <c:forEach var="e_userBranch" items="${userBranche}">
                            <c:if test="${e_userBranch.branchId == v_branch.branchId}">
                                <c:set var="isBranch" value="true"></c:set>
                            </c:if>
                        </c:forEach>
                        <c:choose>
                            <c:when test="${isBranch}">
                                <td><input type="checkbox" checked="true" id="branch_ID${v_branch.branchId}"</td>
                                </c:when>
                                <c:otherwise>
                                <td><input type="checkbox"id="branch_ID${v_branch.branchId}" </td>
                                </c:otherwise>
                            </c:choose>
                        <td>${v_branch.branchName}</td>      
                    </tr>
                </c:forEach>

            </tbody>
        </table>

        <div class="row" style="margin-top: 5%">
            <div class="col-md-4"></div>
            <div class="col-md-2"></div>
            <div class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"/></div>
            <div class="col-md-3"><input type="button" id="btnSaveUserType" name="" value="Save" onclick="saveAddPages()" class="btn btn-default col-md-12"/></div>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />   
        <div id="hideBranch" style="display: none"></div>
    </form>
    <input type="hidden" id="txtNumOfBranch" value="${numOfBranch}"/>
    <input type="hidden" id="txtCount"/>

</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script>
    function saveAddPages() {
        if (!checkTicBox()) {
            $("#errmsgForTab").text("Please select atleast one Branch");
        } else {
            var branch = document.getElementById("txtNumOfBranch").value;
            var branchCount = document.getElementById("txtCount").value;
            if (branch == 0) {
                document.getElementById("btnSaveUserType").disabled = true;
                $.ajax({
                    url: '/AxaBankFinance/saveBranchUserType',
                    type: 'POST',
                    data: $('#userTypeBranch').serialize(),
                    success: function(data) {
                        $('#msgDiv').html(data);
                        ui('#msgDiv');
                        document.getElementById("btnDependent").disabled = false;
                    },
                    error: function() {
                        alert("Error Loading...!");
                    }
                });
            } else {
                if (branchCount <= branch) {
                    $.ajax({
                        url: '/AxaBankFinance/saveBranchUserType',
                        type: 'POST',
                        data: $('#userTypeBranch').serialize(),
                        success: function(data) {
                            $('#msgDiv').html(data);
                            ui('#msgDiv');
                            document.getElementById("btnDependent").disabled = false;
                        },
                        error: function() {
                            alert("Error Loading...!");
                        }
                    });
                }

                else {
                    $("#errmsgForTab").text("Please the select branch less than " + branch);
                }
            }
        }
    }
    function checkTicBox() {
        var hasBranch = false;
        var count = 0;
        $('#tblBranch tbody tr').each(function() {
            var branchId = $(this).attr('id');
            if ($('#branch_ID' + branchId).is(':checked')) {
                hasBranch = true;
                count++;
                $("#hideBranch").append("<input type='text' id = 'temp" + branchId + "'name='branchID' value='" + branchId + "'/>");
            }
            else {
                $("#temp" + branchId).remove();
            }
            document.getElementById("txtCount").value = count;
        });
        return hasBranch;
    }

</script>




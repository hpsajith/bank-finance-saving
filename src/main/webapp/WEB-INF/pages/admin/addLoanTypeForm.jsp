<%-- 
    Document   : addLoanTypeFormAdd
    Created on : Mar 13, 2015, 12:49:39 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function() {
        $("#viewLoanCheckList").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "500px",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<form name="addLoanTypeForm" id="addLoanType">
    <div class="row" style="margin: 0 8 2 0"><legend>Add Loan Type</legend></div>
    <div class="row">
        <input type="hidden" name="loanTypeId" value="${loanType.loanTypeId}"/>
        <input type="hidden" name="isActive" value="${loanType.isActive}"/>
        <div class="col-md-5 col-sm-5 col-lg-5 ">Loan Type Name</div> 
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text"name="loanTypeName" id="txtLoanTypeName" style="width: 100%" value="${loanType.loanTypeName}" />
            <label id="msgLoanTypeName" class="msgTextField"></label>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5 ">Loan Type Code</div>    
        <div class="col-md-7 col-sm-7 col-lg-7">
            <input type="text"name="loanTypeCode" maxlength="5" min="2" id="txtLoanTypeCode" style="width: 100%" value="${loanType.loanTypeCode}" />
            <label id="msgLoanTypeCode" class="msgTextField"></label>
        </div> 
    </div>
    <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
        <div  class="col-md-6 col-sm-6 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveLoanType" value="Save" onclick="saveUpdateLoanType()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script type="text/javascript">
    function saveUpdateLoanType() {
        if (fieldValidate()) {
            document.getElementById("btnSaveLoanType").disabled = true;
            // setCheckList();
            $.ajax({
                url: '/AxaBankFinance/saveLoanType',
                type: 'POST',
                data: $('#addLoanType').serialize(),
                success: function(data) {
                    $('#msgDiv').html("");
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadAddLoanTypeForm();
                    }, 2000);
                },
                error: function() {
                    alert("error Loading");
                }
            });
        }
    }
    function fieldValidate() {
        var loanTypeName = document.getElementById('txtLoanTypeName');
        var loanTypeCode = document.getElementById('txtLoanTypeCode');
        if (loanTypeName.value == null || loanTypeName.value == "") {
            $("#msgLoanTypeName").html("Name must be filled out");
            $("#txtLoanTypeName").addClass("txtError");
            $("#txtLoanTypeName").focus();
            return false;
        } else {
            $("#txtLoanTypeName").removeClass("txtError");
            $("#msgLoanTypeName").html("");
        }
        if (loanTypeCode.value == null || loanTypeCode.value == "") {
            $("#msgLoanTypeCode").html("Code must be filled out");
            $("#txtLoanTypeCode").addClass("txtError");
            $("#txtLoanTypeCode").focus();
            return false;
        } else {
            $("#txtLoanTypeCode").removeClass("txtError");
            $("#msgLoanTypeCode").html("");
        }

        return true;
    }

</script>

<%-- 
    Document   : addBranchMain
    Created on : May 8, 2015, 2:17:39 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        $("#tblViewBranch").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });

</script>

<div><label class = "successMsg">${successMsg}</label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-6" id="divAddBranchForm">

    </div>
    <div class="col-md-6" style="margin:50 1 10 -1">
        <table id="tblViewBranch" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Branch Name</th>
                    <th>Registration No</th>
                    <th>Branch Code</th>
                    <th>Edit</th>
                    <th>InActive</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="v_branch" items="${branchList}">
                    <c:choose>
                        <c:when test="${v_branch.isActive == false}">
                            <tr id="${v_branch.branchId}" style="background-color: #BDBDBD;color: #888">
                                <td>${v_branch.branchName}</td>
                                <td>${v_branch.branchTaxRegNo}</td>
                                <td>${v_branch.branchCode}</td>
                                <td><div class="" href='#'></div></td>
                                <td><div class="btnActive" href='#' id="btnInactive" onclick="clickToAtctive(${v_branch.branchId})"></div></td>
                            </tr>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr id="${v_branch.branchId}">
                                <td>${v_branch.branchName}</td>
                                <td>${v_branch.branchTaxRegNo}</td>
                                <td>${v_branch.branchCode}</td>
                                <td><div class="edit" href='#' onclick="clickToEdit(${v_branch.branchId})"></div></td>
                                <td><div class="delete" href='#' id="btnInactive" onclick="clickToDelete(${v_branch.branchId})"></div></td>
                                </c:otherwise>
                            </c:choose>

                    </c:forEach>

            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            url: '/AxaBankFinance/loadAddBranchForm',
            success: function (data) {
                $("#divAddBranchForm").html(data);
            }
        });
    });

    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/editBranch/' + id,
            success: function (data) {
                $('#divAddBranchForm').html("");
                $('#divAddBranchForm').html(data);
                document.getElementById("btnsaveBranch").value = "Update";
                document.getElementById("btnAddDepartment").disabled = false;
            }
        });

    }

    function clickToDelete(id) {
        $.ajax({
            url: '/AxaBankFinance/inActiveBranch/' + id,
            success: function (data) {
                if (data == true) {
                    loadAddBranchMain();
                }
            }
        });

    }

    function clickToAtctive(id) {
        $.ajax({
            url: '/AxaBankFinance/activeBranch/' + id,
            success: function (data) {
                if (data == true) {
                    loadAddBranchMain();
                }
            }
        });
    }

</script>

<%-- 
    Document   : addFieldOfficerForm
    Created on : May 18, 2015, 4:58:41 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function() {
        $("#tblOfficer").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<form name="addFieldOfficerForm" id="addFieldoffice">
    <div class="row"><legend><strong>Add Field Officer</strong></legend></div>

    <div class="row">
        <input type="hidden" name="" value=""/>
        <div class="col-md-4">Recovery Manager</div> 
        <div class="col-md-8 ">
            <select name="managerId" id="recoverManager" class="col-md-12" style="padding: 1">
                <option value="0">--SELECT--</option>
                <c:forEach var="v_Manager" items="${Rmanager}">
                    <option value="${v_Manager.userId}">${v_Manager.userName}</option>
                </c:forEach>
            </select>
            <label id="msgManager" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">Branch</div> 
        <div class="col-md-8 ">
            <select name="branchId" id="managerBranch" class="col-md-12" style="padding: 1">
                <option value="0">--SELECT--</option>
            </select>
            <label id="msgBranch" class="msgTextField"></label>
        </div>
    </div>
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-4 ">Recovery Officer</div>
        <div class="col-md-8 ">
            <table id="tblOfficer" class=" dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Recovery Officer</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="v_officer" items="${ROfficer}">
                        <tr id="${v_officer.userId}">
                            <td><input type="checkbox" id="off${v_officer.userId}"</td>
                            <td>${v_officer.userName}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div class="dc_clear"></div>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
        <div  class="col-md-5 col-sm-5 col-lg-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveOfficer" value="Save" onclick="saveUpdateOfficer()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>

    </div>

    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <div id="hideOfficer" style="display: none"></div>
</form>
<script type="text/javascript">
    function saveUpdateOfficer() {
        if (fieldValidate() && setToOfficer()) {
            document.getElementById("btnSaveOfficer").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateOfficer',
                type: 'POST',
                data: $('#addFieldoffice').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadFieldOfficerMain();
                    }, 2000);

                },
                error: function() {
                    alert("Error Loding..");
                }

            });

        }
    }

    function setToOfficer() {
        var hasOfficer = false;
        $('#tblOfficer tbody tr').each(function() {
            var officer = $(this).attr('id');
            if ($('#off' + officer).is(':checked')) {
                hasOfficer = true;
                $('#hideOfficer').append("<input type = 'hidden' name = 'officerID' value = '" + officer + "'/>");
            }
        });
        if (!hasOfficer) {
            $("#hideOfficer").append("<input type = 'hidden' name = 'officerID' value = '0'/>");
        }
        return hasOfficer;
    }

    $(function() {
        $('#recoverManager').change(function() {
            var managerID = $("#recoverManager").find("option:selected").val();
            $.ajax({
                url: '/AxaBankFinance/loadBranchForManagerID/' + managerID,
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var branchId = data[i][0];
                        var branchName = data[i][1];
                        $("#managerBranch").append("<option value = '" + branchId + "'>" + branchName + "</option>");
                    }
                },
                error: function() {
                    alert("error Loading...!");
                }
            });

        });
    });

    function fieldValidate() {
        var manager = $("#recoverManager").find('option:selected').val();
        var branch = $("#managerBranch").find('option:selected').val();


        if (manager == 0) {
            $("#msgManager").html("Select The Recovery Manager");
            $("#recoverManager").addClass("txtError");
            $("#recoverManager").focus();
            return false;
        } else {
            $("#recoverManager").removeClass("txtError");
            $("#msgManager").html("");
            $("#msgManager").removeClass("msgTextField");
        }
        if (branch == 0) {
            $("#msgBranch").html("Select The Manager Branch");
            $("#managerBranch").addClass("txtError");
            $("#managerBranch").focus();
            return false;
        } else {
            $("#managerBranch").removeClass("txtError");
            $("#msgBranch").html("");
            $("#msgBranch").removeClass("msgTextField");
        }
        return true;
    }
</script>

<%-- 
    Document   : addUserTypeForm
    Created on : Apr 29, 2015, 1:59:18 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div><label class = "successMsg">${successMsg}</label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>
<form name="userTypeForm" id="userType">
    <div class="row" style="margin: 5 8 2 0"><legend>Add User Type</legend></div>
    <input type="hidden" id="txtUserTypeId" name="userTypeId" value="${userType.userTypeId}"/>
    <div class="row">
        <div class="col-md-4">User Type Name</div>
        <div class="col-md-8"><input type="text" id="txtUserTypeName" name="userTypeName" style="width: 100%" value="${userType.userTypeName}"/><label id="msgUserType" class="msgTextField"></label></div>
    </div>
    <div class="row">
        <div class="col-md-4">Description</div>
        <div class="col-md-8"><input type="text" name="userTypeDes" style="width: 100%" value="${userType.userTypeDes}"/></div>
    </div>
    <div class="row" id="branchRow" style="margin : 5 0 5 0;border: 1px solid #0075b0;padding: 2">
        <div class="col-md-3"></div>
        <div class="col-md-9">
            <c:choose>
                <c:when test="${userType.isBranch == 0}">
                    <div class="col-md-1"><input type="radio" name="isBranch" onclick="hideBranches()" checked="true" value="0"/></div>
                    <div class="col-md-5">Head Office</div>
                    <div class="col-md-1"><input type="radio" name="isBranch" onclick="showBranches()" value="1"/></div>
                    <div class="col-md-3">Branch</div>
                </c:when>
                <c:when test="${userType.isBranch == 1}">
                    <div class="col-md-1"><input type="radio" name="isBranch" onclick="hideBranches()" value="0"/></div>
                    <div class="col-md-5">Head Office</div>
                    <div class="col-md-1"><input type="radio" name="isBranch" onclick="showBranches()" checked="true" value="1"/></div>
                    <div class="col-md-3">Branch</div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-1"><input type="radio" name="isBranch" onclick="hideBranches()"  value="0"/></div>
                    <div class="col-md-5">Head Office</div>
                    <div class="col-md-1"><input type="radio" name="isBranch" onclick="showBranches()" checked="true" value="1"/></div>
                    <div class="col-md-3">Branch</div>
                </c:otherwise>
            </c:choose>
        </div>

    </div>
    <c:choose>
        <c:when test="${userType.isBranch == 0}">
            <div class="row"  id="hideBranches" style="display: none">
                <div class="col-md-4">Number Of Branches</div>
                <div class="col-md-8"><input type="text" name="numOfBranch" id="txtNumOfBranch" style="width: 100%" value="${userType.numOfBranch}"/><label id="msgNumOfBranch" class="msgTextField"></label></div>
            </div>
        </c:when>
        <c:when test="${userType.isBranch == 1}">
            <div class="row" id="hideBranches" style="display: block">
                <div class="col-md-4">Number Of Branches</div>
                <div class="col-md-8"><input type="text" name="numOfBranch" id="txtNumOfBranch" style="width: 100%" value="${userType.numOfBranch}"/><label id="msgNumOfBranch" class="msgTextField"></label></div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="row" id="hideBranches" >
                <div class="col-md-4">Number Of Branches</div>
                <div class="col-md-8"><input type="text" id="txtNumOfBranch" name="numOfBranch" style="width: 100%" value="${userType.numOfBranch}"/><label id="msgNumOfBranch" class="msgTextField"></label></div>
            </div>
        </c:otherwise>
    </c:choose>


    <div class="row" style="margin-top: 10px">
        <div class="col-md-3">
            <c:choose>
                <c:when test="${isAdmin}">
                    <input type="button" name="" id="btnAddPages" value="Job" onclick="addPages()" class="btn btn-default col-md-12"/>
                </c:when>
                <c:otherwise>
                    <input type="button" name="" id="btnAddPages" value="Job" style="display: none" class="btn btn-default col-md-12"/>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-md-3">
            <c:choose>
                <c:when test="${isSuperAdmin}">
                    <input type="button" name="" id="btnAdminAccess" value="Modules" onclick="addAdminPages()" class="btn btn-default col-md-12"/>
                </c:when>
                <c:otherwise>
                    <input type="button" name="" id="btnAdminAccess" value="Modules" style="display: none" class="btn btn-default col-md-12"/>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12" onclick="pageExit()"/></div>
        <div class="col-md-3"><input type="button" id="btnSaveUserType" name="" value="Save" onclick="saveOrUpdateUserType()" class="btn btn-default col-md-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="divAddPages" class="jobsPopUp" style="width: 35%;height: 60%"></div>

<script>

    //document.getElementById(two).style.display = 'none';
    function hideBranches() {
        document.getElementById("hideBranches").hidden = true;
        $("#hideBranches").css('display', ' none');
    }

    function showBranches() {
        document.getElementById("hideBranches").hidden = false;
        $("#hideBranches").css('display', ' block');

    }
    function saveOrUpdateUserType() {
        if (fieldValidate()) {
            document.getElementById("btnSaveUserType").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateUserType',
                type: 'POST',
                data: $('#userType').serialize(),
                success: function(data) {

                    $('#divUserType').html(data);
                    $("#btnAddPages").css('display', 'block');
                },
                error: function() {
                    alert("Error Loding..");
                }

            });
        }

    }
    
    function addPages() {
        var userTypeId = document.getElementById("txtUserTypeId").value;
        $.ajax({
            url: '/AxaBankFinance/loadJobs/' + userTypeId,
            success: function(data) {
                $('#divAddPages').html("");
                $('#divAddPages').html(data);
                ui('#divAddPages');
            }
        });
    }
    
    function addAdminPages() {
        $.ajax({
            url: '/AxaBankFinance/loadModules/',
            success: function(data) {
                $('#divAddPages').html("");
                $('#divAddPages').html(data);
                ui('#divAddPages');
            }
        });
    }

    function fieldValidate() {
        var userType = document.getElementById('txtUserTypeName');
        var numOfBranch = document.getElementById('txtNumOfBranch');
        var isBranch = $("input[name='isBranch']:checked").val();

        if (userType.value == null || userType.value == "") {
            $("#msgUserType").html("User Type Name must be filled out");
            $("#txtUserTypeName").addClass("txtError");
            $("#txtUserTypeName").focus();
            return false;
        } else {
            $("#txtUserTypeName").removeClass("txtError");
            $("#msgUserType").html("");
            $("#msgUserType").removeClass("msgTextField");
        }
        if (isBranch == 1) {
            if (numOfBranch.value == null || numOfBranch.value == "") {
                $("#msgNumOfBranch").html("Number Of Branch must be filled out");
                $("#txtNumOfBranch").addClass("txtError");
                $("#txtNumOfBranch").focus();
                return false;
            } else {
                $("#txtNumOfBranch").removeClass("txtError");
                $("#msgNumOfBranch").html("");
                $("#msgNumOfBranch").removeClass("msgTextField");
            }
        }

        return true;

    }
</script>
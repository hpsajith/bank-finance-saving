<%-- 
    Document   : addApprovalMain1
    Created on : Apr 7, 2015, 11:07:40 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        $("#tblAppLevel").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });
</script>

<form name="adminUserTypeSubLoanForm" id="adminUserTypeSubLoan">
    <div class="row" style="margin: 5 8 2 0"><legend>Add User Type Sub Loan</legend></div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3 ">User</div> 
        <div class="col-md-8 col-sm-8 col-lg-8">
            <select name="userId" id="userInput" class="col-md-6 col-sm-6 col-lg-6" style="padding: 1">
                <c:choose>
                    <c:when test="${userList.size()>0}">
                        <option value="0" selected>--SELECT--</option>
                        <c:forEach var="user" items="${userList}">
                            <option value="${user.userId}">${user.userName}</option>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <option value="${user.userId}" selected>${user.userName}</option>
                    </c:otherwise>
                </c:choose>
            </select>
            <label id="msgUserInput" class="msgTextField"></label>
        </div>
        <div class="col-md-1 col-sm-1 col-lg-1 "></div> 
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-3 col-sm-3 col-lg-3">Sub Loan Type</div>
        <div class="col-md-8 col-sm-8 col-lg-8">
            <table id="tblUserTypeSubLoan" class=" dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th></th>
                        <th>Sub Loan Name</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="subLoan" items="${subLoanType}">
                        <c:set var="isExist" value="false"></c:set>
                        <c:forEach var="userTypeLoan" items="${userTypeLoans}">
                            <c:if test="${subLoan.subLoanId==userTypeLoan.MSubLoanType.subLoanId}">
                                <c:set var="isExist" value="true"></c:set>
                            </c:if>
                        </c:forEach>
                        <c:choose>
                            <c:when test="${isExist}">
                                <tr id="${subLoan.subLoanId}">
                                    <td><input type="checkbox" id="subLoan${subLoan.subLoanId}" checked/></td>
                                    <td>${subLoan.subLoanName}</td>
                                </tr>                                
                            </c:when>
                            <c:otherwise>
                                <tr id="${subLoan.subLoanId}">
                                    <td><input type="checkbox" id="subLoan${subLoan.subLoanId}"/></td>
                                    <td>${subLoan.subLoanName}</td>
                                </tr>                                   
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tbody>
            </table>
            <div class="dc_clear"></div>
        </div>
        <div class="col-md-1 col-sm-1 col-lg-1 "></div> 
    </div>

    <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
        <div  class="col-md-5 col-sm-5 col-lg-5"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" id="btnSaveUserTypeSubLoan" value="Save" onclick="saveUpdateUserTypeSubLoan()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
        <div class="col-md-1 col-sm-1 col-lg-1"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <div id="hideUserSubLoan"></div>
</form>
<script>
//    $(function() {
//        $('#branch').change(function() {
//            var branchId = $("#branch").find("option:selected").val();
//            $.ajax({
//                url: '/AxaBankFinance/loadDepartment/' + branchId,
//                success: function(data) {
//                    for (var i = 0; i < data.length; i++) {
//                        var depId = data[i].depId;
//
//                        var depName = data[i].depName;
//                        $('#department').append("<option value = '" + depId + "'>" + depName + "</option>")
//                    }
//
//                },
//                error: function() {
//                    alert("error Loading...!");
//                }
//            });
//
//        });
//
//        $('#department').change(function() {
//            var depId = $("#department").find("option:selected").val();
//            $.ajax({
//                url: '/AxaBankFinance/loadUser/' + depId,
//                success: function(data) {
//                    for (var i = 0; i < data.length; i++) {
//                        var userId = data[i].userId;
//
//                        var userName = data[i].userName;
//                        $('#user').append("<option value = '" + userId + "'>" + userName + "</option>");
//                    }
//
//                },
//                error: function() {
//                    alert("error Loading...!");
//                }
//            });
//
//        });
//    });

    function saveUpdateUserTypeSubLoan() {
        setToUserLoan();
        if (validate()) {
            document.getElementById("btnSaveUserTypeSubLoan").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateUserTypeSubLoan',
                type: 'POST',
                data: $('#adminUserTypeSubLoan').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadUserTypeLoanMain();
                    }, 2000);
                },
                error: function() {
                    alert("Error Loding..");
                }
            });
        }
    }

    function validate() {
        var user = $("#userInput").find('option:selected').val();
        if (user === '0') {
            $("#userInput").addClass(".txtError");
            $("#msgUserInput").text("Select User");
            $("#userInput").focus();
            return false;
        } else {
            $("#userInput").removeClass(".txtError");
            $("#msgUserInput").text("");
            return true;
        }
    }

    function setToUserLoan() {
        var hasSubLoan = false;
        $('#tblUserTypeSubLoan tbody tr').each(function() {
            var subLoanId = $(this).attr('id');
            if ($('#subLoan' + subLoanId).is(':checked')) {
                hasSubLoan = true;
                $('#hideUserSubLoan').append("<input type = 'hidden' name = 'subLoanId' value = '" + subLoanId + "'/>");
            }
        });
        if (!hasSubLoan) {
            $('#hideUserSubLoan').append("<input type = 'hidden' name = 'subLoanId' value = '0'/>");
        }
    }

</script>
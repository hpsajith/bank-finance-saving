<%-- 
    Document   : addSeizerMain
    Created on : Feb 9, 2016, 8:24:24 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblSeizer").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "90%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="container-fluid"style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-7 col-sm-7 col-lg-7" id="viewAddSeizerForm" style="margin-top: 5px"></div>
    <div class="col-md-5 col-sm-5 col-lg-5" style="margin-top: 45px">
        <div class="row" style="margin-bottom: 30px;">
            <table id="tblSeizer"class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="text-align: center">Id</th>
                        <th style="text-align: center">Name</th>
                        <th style="text-align: center">Mobile</th>
                        <th style="text-align: center">Edit</th>
                        <th style="text-align: center">InActive</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="seizer" items="${seizerList}">
                        <c:choose>
                            <c:when test="${seizer.isActive == false}">
                                <tr id="${seizer.seizerId}" style="background-color: #BDBDBD;color: #888">
                                    <td style="text-align: center">${seizer.seizerId}</td>
                                    <td style="text-align: center">${seizer.seizerName}</td>
                                    <td style="text-align: center">${seizer.seizerContactMobile}</td>
                                    <td style="text-align: center"><div class="" href='#'></div></td>
                                    <td style="text-align: right"><div class="btnActive" href='#' id="btnInactive" onclick="clickToActive(${seizer.seizerId})"></div></td>
                                </tr> 
                            </c:when>
                            <c:otherwise>
                                <tr id="${seizer.seizerId}">
                                    <td style="text-align: center">${seizer.seizerId}</td>
                                    <td style="text-align: center">${seizer.seizerName}</td>
                                    <td style="text-align: center">${seizer.seizerContactMobile}</td>
                                    <td style="text-align: center"><div class="edit" href='#' onclick="clickToEdit(${seizer.seizerId})"></div></td>
                                    <td style="text-align: right"><div class="delete" href='#' id="btnInactive" onclick="clickToInactive(${seizer.seizerId})"></div></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/loadAddSeizerForm',
            success: function(data) {
                $('#viewAddSeizerForm').html("");
                $('#viewAddSeizerForm').html(data);
            },
            error: function() {
            }
        });
    });
    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/editSeizerDetails/' + id,
            success: function(data) {
                $('#viewAddSeizerForm').html("");
                $('#viewAddSeizerForm').html(data);
                document.getElementById("btnsaveSeizer").value = "Update";
            }
        });
    }
    function clickToInactive(id) {
        $.ajax({
            url: '/AxaBankFinance/inActiveSeizer/' + id,
            success: function(data) {
                if (data == true) {
                    loadNewSeizerMain();
                }
            }
        });
    }
    function clickToActive(id) {
        $.ajax({
            url: '/AxaBankFinance/activeSeizer/' + id,
            success: function(data) {
                if (data == true) {
                    loadNewSeizerMain();
                }
            }
        });
    }
</script>


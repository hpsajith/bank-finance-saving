<%-- 
    Document   : addEmployeeFormView
    Created on : Apr 2, 2015, 4:10:58 PM
    Author     : SOFT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div><label class = "successMsg">${successMsg}</label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>
<form name="newEmployeeForm" id="newEmployee">
    <div class="row">
        <legend><strong>Add New Employee</strong></legend>
    </div>
    <input type="hidden" name="empNo" id="empId" value="${employee.empNo}"/>
    <div class="row">
        <div class="col-md-4">Title <label class="redmsg">*</label></div>
        <div class="col-md-8">
            <select name ="titel" id="txtTitle" style="width: 80%">
                <option value="0">SELECT</option>
                <c:choose>
                    <c:when test="${employee.titel == 'Mr'}">
                        <option value="Mr" selected="true">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                    </c:when>
                    <c:when test="${employee.titel == 'Mrs'}">
                        <option value="Mr">Mr</option>
                        <option value="Mrs" selected="true">Mrs</option>
                        <option value="Miss">Miss</option>
                    </c:when>
                    <c:when test="${employee.titel == 'Miss'}">
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss" selected="true">Miss</option>
                    </c:when>
                    <c:otherwise>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                    </c:otherwise>
                </c:choose>
            </select>
            <label id="msgTitle" class="msgTextField"></label>

        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Full Name <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtFname" name="empFname" value="${employee.empFname}" style="width: 80%"/>
            <label id="msgFname" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Name With Initial<label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtLname" name="empLname" value="${employee.empLname}"style="width: 80%"/>
            <label id="msgLname" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">User Name <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtUserName" name="userName" onblur="userNameCheck()" value="${employee.userName}" style="width: 80%"/> 
            <label id="msgUserName" class="msgTextField"></label>
            <label id="msgUserExits" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Address 1 <label class="redmsg"></label></div>
        <div class="col-md-8"><textarea type="text" id="txtAddress1" name="addres1" style="width: 80%">${employee.addres1}</textarea>
            <label id="msgAddress" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Address 2</div>
        <div class="col-md-8"><textarea type="text" name="addres2" style="width: 80%">${employee.addres2}</textarea></div>
    </div>

    <div class="row">
        <div class="col-md-4">Contact Mobile <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtMobileNo" name="phone2" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" value="${employee.phone2}"style="width: 80%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Contact Home</div>
        <div class="col-md-8"><input type="text"  name="phone1" value="${employee.phone1}" onblur="checkTelNumbers(this)" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
    </div>

    <div class="row">

        <div class="col-md-4">Email 1</div>
        <div class="col-md-8"><input type="text" name="email1" onblur="validateEmail(this, '#msgEmail1')" value="${employee.email1}"style="width: 80%"/>
            <label id="msgEmail1"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Email 2</div>
        <div class="col-md-8"><input type="text" name="email2" onblur="validateEmail(this, '#msgEmail2')" value="${employee.email2}" style="width: 80%"/>
            <label id="msgEmail2"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">NIC <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="txtNicNo" name="nicNo" onblur="nictoLowerCase(this)" maxlength="12" value="${employee.nicNo}"style="width: 80%"/>
            <label id="msgNic" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Passport No</div>
        <div class="col-md-8"><input type="text" name="passportNo" value="${employee.passportNo}"style="width: 80%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Designation</div>
        <div class="col-md-8"><input type="text" name="designation" value="${employee.designation}" style="width: 80%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Sex</div>
        <div class="col-md-8">
            <div class="row"> 
                <c:choose>
                    <c:when test="${employee.sex == 0}">
                        <div class="col-md-1"><input type="radio" checked="checked" name="sex" value="0"/></div>
                        <div class="col-md-3">Male</div>
                        <div class="col-md-1"><input type="radio" name="sex" value="1"/></div>
                        <div class="col-md-3">Female</div>
                    </c:when>                    
                    <c:when test="${employee.sex == 1}">
                        <div class="col-md-1"><input type="radio" name="sex" value="0"/></div>
                        <div class="col-md-3">Male</div>
                        <div class="col-md-1"><input type="radio" checked="checked" name="sex" value="1"/></div>
                        <div class="col-md-3">Female</div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-1"><input type="radio" name="sex" value="0"/></div>
                        <div class="col-md-3">Male</div>
                        <div class="col-md-1"><input type="radio" name="sex" value="1"/></div>
                        <div class="col-md-3">Female</div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Emergency Contact Name</div>
        <div class="col-md-8"><input type="text" name="emergencyConName" value="${employee.emergencyConName}" style="width: 80%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Emergency Contact No</div>
        <div class="col-md-8"><input type="text" name="emergencyconPhone" onblur="checkTelNumbers(this)" value="${employee.emergencyconPhone}" onkeypress="return checkNumbers(this)" maxlength="10" style="width: 80%"/></div>
    </div>
    <div class="row">
        <div class="col-md-4">Date Of Birth</div>
        <div class="col-md-8"><input type="text" name="dob" value="${employee.dob}" class="txtCalendar" style="width: 80%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Start Date</div>
        <div class="col-md-8"><input type="text" name="startDate" value="${employee.startDate}"class="txtCalendar" style="width: 80%"/></div>
    </div>

    <div class="row" style="margin-top: 10px">
        <div class="col-md-12" style="border: 1px solid #0075b0;width: 85%">
            <div class="row"style="padding: 5">
                <div class="col-md-2"></div> 
                <c:choose>
                    <c:when test="${empUser.userTypeId == 0}">
                        <div class="col-md-1"><input type="radio"  name="userDefind" onclick="hideUserType()"  checked="true" value="0"/></div>
                        <div class="col-md-3" style="padding: 1"><strong>Custom User </strong></div>
                        <div class="col-md-1"><input type="radio" name="userDefind" id="preDefind"  onclick="showUserType()" value="1"/></div>
                        <div class="col-md-3" style="padding: 1"><strong>Pre Defind User</strong></div>
                        <div class="col-md-2"></div> 
                    </c:when>
                    <c:when test="${empUser.userTypeId > 0}">
                        <div class="col-md-1"><input type="radio"  name="userDefind" onclick="hideUserType()" value="0"/></div>
                        <div class="col-md-3" style="padding: 1"><strong>Custom User </strong></div>
                        <div class="col-md-1"><input type="radio" name="userDefind" id="preDefind"  onclick="showUserType()" checked="true" value="1"/></div>
                        <div class="col-md-3" style="padding: 1"><strong>Pre Defind User</strong></div>
                        <div class="col-md-2"></div> 
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-1"><input type="radio"  name="userDefind" onclick="hideUserType()" value="0"/></div>
                        <div class="col-md-3" style="padding: 1"><strong>Custom User</strong></div>
                        <div class="col-md-1"><input type="radio" name="userDefind" id="preDefind"  onclick="showUserType()" checked="true" value="1"/></div>
                        <div class="col-md-3" style="padding: 1"><strong>Pre Defind User</strong></div>
                        <div class="col-md-2"></div> 
                    </c:otherwise>
                </c:choose>

            </div>

        </div>
    </div>

    <div class="row" id="hideUserType" style="margin-top: 5px">
        <div class="col-md-4">User Type <label class="redmsg">*</label></div>
        <div class="col-md-8">
            <select name="userType" id="userType" style="width: 70%">
                <option value="0">SELECT</option>
                <c:forEach var="userType" items="${userTypeList}">
                    <c:choose>
                        <c:when test="${empUser.userTypeId == userType.userTypeId}">
                            <option value="${userType.userTypeId}" selected="selected">${userType.userTypeName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${userType.userTypeId}">${userType.userTypeName}</option>
                        </c:otherwise>
                    </c:choose>

                </c:forEach>
            </select>
            <label id="msgUserType" class="msgTextField"></label>
        </div>
    </div>

    <div class="row" style="margin-top: 10px; margin-right: 50px">
        <div class="col-md-4"><input type="button"  name="" value="Branch" disabled="disabled" id="btnBranch" onclick="loadBranch()" class="btn btn-default col-md-12" ></div>
            <c:choose>
                <c:when test="${numOfBranch > 0}">
                <div class="col-md-4"><input type="button" name="" value="Department" id="btnDependent" onclick="loadAddDepartment()" class="btn btn-default col-md-12"/></div>
                </c:when>
                <c:otherwise>
                <div class="col-md-4"><input type="button" name="" value="Department" disabled="disabled" id="btnDependent" onclick="loadAddDepartment()" class="btn btn-default col-md-12"/></div>
                </c:otherwise>
            </c:choose> 
        <div class="col-md-4">
            <input type="button" disabled name="" value="Assing Due" id="btnClcDate" onclick="loadCollection()" class="btn btn-default col-md-12">
        </div>
    </div>
    <div class="row" style="margin-top: 10px; margin-right: 50px">
        <div class="col-md-6">
            <input type="button" name="" value="Cancel" style="width: 100%" class="btn btn-default col-md-12" onclick="pageExit()"/>
        </div>
        <div class="col-md-6">
            <input type="button" id="btnSaveNewEmployee" name="" style="width: 100%" value="Save" onclick="saveNewEmployee()" class="btn btn-default col-md-12"/>
        </div>
    </div>
    <div class="row" style="margin-top: 10px; margin-right: 50px" id="divAddPages">
        <div class="col-md-4">
            <input type="button" id="btnAddPages" value="Add Pages" style="width: 100%" class="btn btn-default col-md-12" onclick="addPages()"/>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="divBranch" class="jobsPopUp" style="width: 35%;height: 50%;overflow-y: auto"></div>
<div id="divDepartment" class="jobsPopUp" style="width: 35%;height: 70%;overflow-y: auto"></div>
<div id="divCollection" class="collectionForm" style="width: 50%;height: 70%;overflow-y: auto; overflow-x:auto"></div>
<div id="divAddPages_PopUp" class="jobsPopUp" style="width: 35%;height: 60%"></div>
<script>
    $(function () {
        $(".txtCalendar").datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "-100:+0"
        });
    });
    $(function () {
        $("#divAddPages").hide();
        document.getElementById("btnAddPages").disabled = true;
    });
    $(function () {
        var remainUserCount = $("#txtRemainUserCount").val();
        if (remainUserCount === "0") {
            $("#btnSaveNewEmployee").attr("disabled", true);
        }
    });
    function hideUserType() {
        document.getElementById("hideUserType").hidden = true;
    }

    function showUserType() {
        document.getElementById("hideUserType").hidden = false;
    }

    function saveNewEmployee() {
        if (fieldValidate() && checkNIC('txtNicNo')) {
            document.getElementById("btnSaveNewEmployee").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateNewEmployee',
                type: 'POST',
                data: $('#newEmployee').serialize(),
                success: function (data) {
                    $('#viewAddEmploye').html(data);
                    document.getElementById("btnSaveNewEmployee").value = "Update";
                    document.getElementById("btnBranch").disabled = false;
                    var usertType = $("#userType").val();
                    if (usertType === "4") {
                        document.getElementById("btnClcDate").disabled = false;
                    } else {
                        document.getElementById("btnClcDate").disabled = true;
                    }
                    if ($("#userType").val() === "0") {
                        $("#divAddPages").show();
                        document.getElementById("btnAddPages").disabled = false;
                    }
                    window.scrollTo(100, 50);

                },
                error: function () {
                    alert("Error Loading..!");
                }
            });
        }
    }
    function loadBranch() {
        var empId = document.getElementById("empId").value;
        $.ajax({
            url: '/AxaBankFinance/loadBranch/' + empId,
            success: function (data) {
                $('#divBranch').html("");
                $('#divBranch').html(data);
                ui('#divBranch');
            }
        });

    }
    function loadAddDepartment() {
        var empId = document.getElementById("empId").value;
        $.ajax({
            url: '/AxaBankFinance/loadUserDepartment/' + empId,
            success: function (data) {
                $('#divDepartment').html("");
                $('#divDepartment').html(data);
                ui('#divDepartment');
            }
        });

    }


    function  userNameCheck() {
        var userName = document.getElementById('txtUserName').value;
        var empId = document.getElementById('empId').value;
        if (empId == null || empId == 0) {
            $.ajax({
                url: '/AxaBankFinance/userNameCheck/' + userName,
                success: function (data) {
                    if (data === 1) {
                        $("#msgUserExits").html("User Name is Alredy Exits..!");
                        $("#txtUserName").addClass("txtError");
                        $("#txtUserName").focus();
                    } else {
                        $("#txtUserName").removeClass("txtError");
                        $("#msgUserExits").html("");
                        $("#msgUserExits").removeClass("msgTextField");

                    }
                }
            });
        }
    }

    function fieldValidate() {
        var fname = document.getElementById('txtFname');
        var lname = document.getElementById('txtLname');
        var address = document.getElementById('txtAddress1');
        var mobileNo = document.getElementById('txtMobileNo');
        var nic = document.getElementById('txtNicNo');
        var userName = document.getElementById('txtUserName');
        var title = $("#txtTitle").find('option:selected').val();

        if (title == 0) {
            $("#msgTitle").html("Select The Title");
            $("#txtTitle").addClass("txtError");
            $("#txtTitle").focus();
            return false;
        } else {
            $("#txtTitle").removeClass("txtError");
            $("#msgTitle").html("");
            $("#msgTitle").removeClass("msgTextField");
        }
        if (fname.value == null || fname.value == "") {
            $("#msgFname").html("First Name must be filled out");
            $("#txtFname").addClass("txtError");
            $("#txtFname").focus();
            return false;
        } else {
            $("#txtFname").removeClass("txtError");
            $("#msgFname").html("");
            $("#msgFname").removeClass("msgTextField");
        }
        if (lname.value == null || lname.value == "") {
            $("#msgLname").html("Last Name must be filled out");
            $("#txtLname").addClass("txtError");
            $("#txtLname").focus();
            return false;
        } else {
            $("#txtLname").removeClass("txtError");
            $("#msgLname").html("");
            $("#msgLname").removeClass("msgTextField");
        }
        if (userName.value == null || userName.value == "") {
            $("#msgUserName").html("User Name must be filled out");
            $("#txtUserName").addClass("txtError");
            $("#txtUserName").focus();
            return false;
        } else {
            $("#txtUserName").removeClass("txtError");
            $("#msgUserName").html("");
            $("#msgUserName").removeClass("msgTextField");
        }
        if (address.value == null || address.value == "") {
            $("#msgAddress").html("Address 1 must be filled out");
            $("#txtAddress1").addClass("txtError");
            $("#txtAddress1").focus();
            return false;
        } else {
            $("#txtAddress1").removeClass("txtError");
            $("#msgAddress").html("");
            $("#msgAddress").removeClass("msgTextField");
        }
        if (mobileNo.value == null || mobileNo.value == "") {
            $("#msgMobile").html("Mobile No must be filled out");
            $("#txtMobileNo").addClass("txtError");
            $("#txtMobileNo").focus();
            return false;
        } else {
            $("#txtMobileNo").removeClass("txtError");
            $("#msgMobile").html("");
            $("#msgMobile").removeClass("msgTextField");
        }
        if (nic.value == null || nic.value == "") {
            $("#msgNic").html("Nic No must be filled out");
            $("#txtNicNo").addClass("txtError");
            $("#txtNicNo").focus();
            return false;
        } else {
            $("#txtNicNo").removeClass("txtError");
            $("#msgNic").html("");
            $("#msgNic").removeClass("msgTextField");
        }
        if (document.getElementById('preDefind').checked) {
            var userType = $("#userType").find("option:selected").val();
            if (userType == 0) {
                $("#msgUserType").html("Select User Type");
                $("#userType").addClass("txtError");
                $("#userType").focus();
                return false;
            } else {
                $("#userType").removeClass("txtError");
                $("#msgUserType").html("");
                $("#msgUserType").removeClass("msgTextField");
            }
        }
        return true;

    }

    function loadCollection() {
        $.ajax({
            url: '/AxaBankFinance/loadCollection',
            success: function (data) {
                $('#divCollection').html("");
                $('#divCollection').html(data);
                ui('#divCollection');
            }
        });
    }

    function addPages() {
        var empId = $("#empId").val();
        if (empId !== null && empId !== "") {
            $.ajax({
                url: '/AxaBankFinance/loadJobsForCustomUser/' + empId,
                success: function (data) {
                    $('#divAddPages_PopUp').html("");
                    $('#divAddPages_PopUp').html(data);
                    $("#txtUserType").val("1"); // set as custom user
                    $("#txtEmpId").val(empId); // set employee id
                    ui('#divAddPages_PopUp');
                }
            });
        }
    }

</script>

<%-- 
    Document   : addSubLoanTypeForm
    Created on : Mar 2, 2015, 9:41:46 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#viewSubLoanType").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "85%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();

    });

</script>
<div class="container-fluid">

    `
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6" style="" id="divAddSubLoanType">
            <!--add Sub loan Type form-->
        </div>

        <div class="col-md-1 col-sm-1 col-lg-1" style=""></div>
        <div class="col-md-5 col-sm-5 col-lg-5 " style="margin-top: 20px">
            <div class="row">
                <table id="viewSubLoanType" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="text-align: center">Sub Loan Type</th>
                            <th style="text-align: center">Rate</th>
                            <th style="text-align: center">Edit</th>
                                <c:if test="${isSuperAdmin}">
                                <th style="text-align: center">In-Active</th>
                                </c:if>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="subLoanType" items="${mSubLoanTypes}">
                            <c:choose>
                                <c:when test="${subLoanType.isActive}">
                                    <tr id="${subLoanType.subLoanId}">
                                    </c:when>
                                    <c:otherwise>
                                    <tr id="${subLoanType.subLoanId}" style='background-color: #B8B8B8 ;color: #000000'>
                                    </c:otherwise>                                 
                                </c:choose>
                                <td style="text-align: center">${subLoanType.subLoanName}</td>
                                <td style="text-align: center">${subLoanType.subInterestRate}</td>
                                <td style="text-align: center">
                                    <c:choose>
                                        <c:when test="${subLoanType.isActive}">
                                            <div class='edit' href='#' onclick='viewToSubLoanType(${subLoanType.subLoanId})'></div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class='edit' href='#'></div>
                                        </c:otherwise>   
                                    </c:choose>
                                </td>
                                <c:if test="${isSuperAdmin}">
                                    <td style="text-align: center">
                                        <c:choose>
                                            <c:when test="${subLoanType.isActive}">
                                                <div class="delete" href='#' title="In-Active" onclick="changeSubLoanType(${subLoanType.subLoanId}, 0)"></div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="btnActive" href='#' title="Active" onclick="changeSubLoanType(${subLoanType.subLoanId}, 1)"></div>
                                            </c:otherwise>   
                                        </c:choose>
                                    </td>                                    
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="dc_clear"></div>
            </div>
        </div>
    </div>
</div>


</div>
<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/addSubloanTypeForm',
            success: function(data) {
                $('#divAddSubLoanType').html(data);
            },
            error: function() {
            }
        });
    });

    function viewToSubLoanType(Id) {
        $.ajax({
            url: '/AxaBankFinance/viewUpdateSubLoanType/' + Id,
            success: function(data) {
                $('#divAddSubLoanType').html("");
                $('#divAddSubLoanType').html(data);
                document.getElementById("btnSaveSubLoanType").value = "Update";
            },
            error: function() {
                alert("Error View..!");
            }
        });
    }

    function changeSubLoanType(subLoanTypeId, status) {
        $.ajax({
            url: "/AxaBankFinance/changeSubLoanType",
            data: {"subLoanTypeId": subLoanTypeId, "status": status},
            success: function(data) {
                $('#msgDiv').html("");
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function() {
                    loadSubLoanTypeForm();
                }, 2000);
            },
            error: function() {
                console.log("Error in change subloan type");
            }
        });
    }

</script>

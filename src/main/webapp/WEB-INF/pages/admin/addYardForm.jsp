<%-- 
    Document   : addYardForm
    Created on : Jul 26, 2016, 9:08:17 AM
    Author     : User
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form name="newYardForm" id="newYard">
    <input type="hidden" name="yardId" id="txtYardId" value="${yard.yardId}"/>
    <input type="hidden" name="currentVehicleCount" id="txtcurrentVehicleCount" value="${yard.currentVehicleCount}"/>
    <div class="row"><legend><strong>Add New Yard Details</strong></legend></div>

    <div class="row">
        <div class="col-md-4">Name <label class="redmsg">*</label></div>
        <div class="col-md-7"><input type="text" id="txtYardName" name="yardName" value="${yard.yardName}" style="width: 100%"/>
            <label id="msgYardName" class="msgTextField"></label>
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-4">Max Vehicle Count <label class="redmsg">*</label></div>
        <div class="col-md-7"><input type="number" id="txtMaxVehCount" min="0" name="maxVehicleCount" value="${yard.maxVehicleCount}" style="width: 100%"/>
            <label id="msgMaxVehCount" class="msgTextField"></label>
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-4">Description <label class="redmsg"></label></div>
        <div class="col-md-7"><textarea type="text" id="txtYardDescription" name="yardDescription" style="width: 100%">${yard.yardDescription}</textarea>
            <label id="msgYardDescription" class="msgTextField"></label>
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row" style="margin-top: 5px">
        <div  class="col-md-4"></div>
        <div  class="col-md-7">
            <div class="row">
                <div  class="col-md-6">
                    <input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()" style="width: 100%"/>
                </div>
                <div  class="col-md-6">
                    <input type="button" name="" value="Save"id="btnsaveYard" onclick="saveNewYard()" class="btn btn-default col-md-12 col-sm-12 col-lg-12" style="width: 100%"/>
                </div>
            </div>
        </div>
        <div  class="col-md-1"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script>
    function saveNewYard() {
        if (fieldValidate()) {
            document.getElementById("btnsaveYard").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateNewYard',
                type: 'POST',
                data: $('#newYard').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadAddYardMainPage();
                    }, 2000);
                },
                error: function() {
                    alert("Error in Saving Yard Details..!");
                }
            });
        }
    }

    function fieldValidate() {
        var name = document.getElementById('txtYardName');
        var maxVehCount = document.getElementById('txtMaxVehCount');
        if (name.value == null || name.value == "") {
            $("#msgYardName").html("Yard Name must be filled out");
            $("#txtYardName").addClass("txtError");
            $("#txtYardName").focus();
            return false;
        } else {
            $("#txtYardName").removeClass("txtError");
            $("#msgYardName").html("");
            $("#msgYardName").removeClass("msgTextField");
        }
        if (maxVehCount.value == null || maxVehCount.value == "") {
            $("#msgMaxVehCount").html("Maximum Vehicle Count must be filled out");
            $("#txtMaxVehCount").addClass("txtError");
            $("#txtMaxVehCount").focus();
            return false;
        } else {
            $("#txtMaxVehCount").removeClass("txtError");
            $("#msgMaxVehCount").html("");
            $("#msgMaxVehCount").removeClass("msgTextField");
        }
        return true;
    }
</script>
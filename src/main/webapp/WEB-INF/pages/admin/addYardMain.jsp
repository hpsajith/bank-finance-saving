<%-- 
    Document   : addYardMain
    Created on : Jul 26, 2016, 9:08:34 AM
    Author     : User
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblSeizer").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "90%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });
</script>
<div class="container-fluid"style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-6 col-sm-6 col-lg-6" id="viewAddYardForm" style="margin-top: 5px"></div>
    <div class="col-md-6 col-sm-6 col-lg-6" style="margin-top: 45px">
        <div class="row" style="margin-bottom: 30px;">
            <table id="tblYard"class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="text-align: center">Name</th>
                        <th style="text-align: center">Description</th>
                        <th style="text-align: center">Max Veh. Count</th>
                        <th style="text-align: center">Current Veh. Count</th>
                        <th style="text-align: center">Edit</th>
                        <th style="text-align: center">InActive</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="yard" items="${yardList}">
                        <c:choose>
                            <c:when test="${yard.isActive == false}">
                                <tr id="${yard.yardId}" style="background-color: #BDBDBD;color: #888">
                                    <td style="text-align: center">${yard.yardName}</td>
                                    <td style="text-align: center">${yard.yardDescription}</td>
                                    <td style="text-align: center">${yard.maxVehicleCount}</td>
                                    <td style="text-align: center">${yard.currentVehicleCount}</td>
                                    <td style="text-align: center"><div class="" href='#'></div></td>
                                    <td style="text-align: right"><div class="btnActive" href='#' id="btnInactive" onclick="clickToActive(${yard.yardId})"></div></td>
                                </tr> 
                            </c:when>
                            <c:otherwise>
                                <tr id="${yard.yardId}">
                                    <td style="text-align: center">${yard.yardName}</td>
                                    <td style="text-align: center">${yard.yardDescription}</td>
                                    <td style="text-align: center">${yard.maxVehicleCount}</td>
                                    <td style="text-align: center">${yard.currentVehicleCount}</td>
                                    <td style="text-align: center"><div class="edit" href='#' onclick="clickToEdit(${yard.yardId})"></div></td>
                                    <td style="text-align: right"><div class="delete" href='#' id="btnInactive" onclick="clickToInactive(${yard.yardId})"></div></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/loadAddYardForm',
            success: function(data) {
                $('#viewAddYardForm').html("");
                $('#viewAddYardForm').html(data);
            },
            error: function() {
            }
        });
    });

    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/editYardDetails/' + id,
            success: function(data) {
                $('#viewAddYardForm').html("");
                $('#viewAddYardForm').html(data);
                document.getElementById("btnsaveYard").value = "Update";
            }
        });
    }

    function clickToInactive(id) {
        $.ajax({
            url: '/AxaBankFinance/inActiveYard/' + id,
            success: function(data) {
                if (data == true) {
                    loadAddYardMainPage();
                }
            }
        });
    }

    function clickToActive(id) {
        $.ajax({
            url: '/AxaBankFinance/activeYard/' + id,
            success: function(data) {
                if (data == true) {
                    loadAddYardMainPage();
                }
            }
        });
    }
</script>

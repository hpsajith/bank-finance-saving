<%-- 
    Document   : addBranchForm
    Created on : May 8, 2015, 2:17:55 PM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div><label class = "successMsg">${successMsg}</label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>
<form name="newBranchForm" id="newBranch">
    <div class="row" style="margin: 5 8 2 0">
        <div class="col-md-4">
            <legend>Add New Branch</legend>
        </div>
        <div class="col-md-8">
            <input type="hidden" id="txtRemainBranchCount" value="${remainBranchCount}"/>
            <c:choose>
                <c:when test="${remainBranchCount == 0}">
                    <label style="font-size: 14px; color: crimson">(Max Branches limit exceeded)</label>
                </c:when>
                <c:otherwise>
                    <label style="font-size: 14px; color: #3071a9">Max Branches limit is : </label><label style="font-size: 14px; color: crimson"> ${remainBranchCount}</label>
                </c:otherwise>  
            </c:choose>
        </div>
    </div>



    <input type="hidden" name="branchId" id="txtBranchId" value="${branch.branchId}"/>
    <div class="row">
        <div class="col-md-4">Branch Name</div>
        <div class="col-md-8"><input type="text" id="txtBranchName" name="branchName" value="${branch.branchName}" style="width: 80%"/>
            <label id="msgBranch" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Branch Last Name</div>
        <div class="col-md-8"><input type="text" id="" name="branchLname"  value="${branch.branchLname}" style="width: 80%"/></div>
    </div>

    <div class="row">
        <div class="col-md-4">Registration No</div>
        <div class="col-md-8"><input type="text" id="txtRegNo" name="branchRegNo" value="${branch.branchRegNo}" style="width: 80%"/>
            <label id="msgRegNo" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Tax Registration No</div>
        <div class="col-md-8"><input type="text" name="branchTaxRegNo"  value="${branch.branchTaxRegNo}" style="width: 80%"/></div>
    </div>
    <div class="row">
        <div class="col-md-4">Branch Code</div>
        <div class="col-md-8"><input type="text" id="txtBranchCode" name="branchCode"  value="${branch.branchCode}" style="width: 80%"/>
            <label id="msgBranchCode" class="msgTextField"></label>
        </div>
    </div>

    <div class="row" style="margin-top: 5px">

        <div  class="col-md-4"><input type="button" id="btnAddDepartment" value="Add Department" class="btn btn-default col-md-12" disabled="disabled" onclick="loadDepartment()"/></div>
        <div  class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
            <c:choose>
                <c:when test="${remainBranchCount == 0}">
                <div  class="col-md-3"><input type="button" disabled="" value="Save"id="btnsaveBranch"  class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                </c:when>
                <c:otherwise>
                <div  class="col-md-3"><input type="button" name="" value="Save"id="btnsaveBranch" onclick="saveNewBranch()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
                </c:otherwise>
            </c:choose>

        <div  class="col-md-2"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<div id="department" class="jobsPopUp" style="width: 35%;height: 50%"></div>
<div id="message_branch" class="searchCustomer" style="width: 40%;margin-left: 12%; margin-top: 10%; border: 0px solid #5e5e5e;"></div>

<script>
    function saveNewBranch() {
//
//        $.ajax({
//            url: "/AxaBankFinance/loadWarningBox/" + "Please Contact ITES!!",
//            success: function (data) {
//                $("#message_branch").html(data);
//                ui("#message_branch");
//            }
//        });

        if (fieldValidate()) {
            document.getElementById("btnsaveBranch").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateBranch',
                type: 'POST',
                data: $('#newBranch').serialize(),
                success: function (data) {
                    $('#divAddBranchForm').html(data);
                    document.getElementById("btnAddDepartment").disabled = false;
                },
                error: function () {
                    alert("Error Loding..");
                }

            });
        }

    }
    function loadDepartment() {
        var branchId = document.getElementById("txtBranchId").value;
        $.ajax({
            url: '/AxaBankFinance/loadDepartmentform/' + branchId,
            success: function (data) {
                $("#department").html("");
                $("#department").html(data);
                ui('#department');
            }
        });
    }

    function fieldValidate() {
        var branchName = document.getElementById('txtBranchName');
        var branchCode = document.getElementById('txtBranchCode');

        if (branchName.value == "" || branchName == null) {
            $("#msgBranch").html("Branch Name must be filled out");
            $("#txtBranchName").addClass("txtError");
            $("#txtBranchName").focus();
            return false;
        } else {
            $("#txtBranchName").removeClass("txtError");
            $("#msgBranch").html("");
            $("#msgBranch").removeClass("msgTextField");
        }
        if (branchCode.value == "" || branchCode == null) {
            $("#msgBranchCode").html("Branch Code No must be filled out");
            $("#txtBranchCode").addClass("txtError");
            $("#txtBranchCode").focus();
            return false;
        } else {
            $("#txtBranchCode").removeClass("txtError");
            $("#msgBranchCode").html("");
            $("#msgBranchCode").removeClass("msgTextField");
        }
        return true;
    }
</script>
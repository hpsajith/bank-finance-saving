<%-- 
    Document   : addUser
    Created on : Mar 3, 2015, 2:45:55 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form name="addUserForm" id="addUser" >
    <div class="row"><legend>Add User</legend></div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">User Name</div>
        <div class="col-md-7 col-sm-7 col-lg-7">
            <select name="userId" id="txtUserName" class="col-md-6 col-sm-6 col-lg-6" style="padding: 1;width: 80%">
                <option value="0">--SELECT--</option>
                <c:forEach var="v_user" items="${user}">
                    <c:choose>
                        <c:when test="${userName.userId == v_user.userId}">
                            <option selected="selected" value="${userName.userId}">${v_user.userName}</option>
                        </c:when> 
                        <c:otherwise>
                            <option value="${v_user.userId}">${v_user.userName}</option>
                        </c:otherwise>

                    </c:choose>
                </c:forEach>
            </select>
            <label id="msgUserName" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Password</div>
        <div class="col-md-7 col-sm-7 col-lg-7"><input type="password" name="password1" autocomplete="off" value="" id="txtPwd1" style="width: 100%"/>
            <label id="msgPassword1" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5">Confirm Password</div>
        <div class="col-md-7 col-sm-7 col-lg-7"><input type="password" name="password" autocomplete="off" id="txtPwd2" onkeyup="checkPwd()" value="" style="width: 100%"/>
            <label id="msgPassword2" class="msgTextField"></label>
        </div>

    </div>
    <div class="row">
        <div class="col-md-5 col-sm-5 col-lg-5"></div>
        <div class="col-md-7 col-sm-7 col-lg-7"><span id="confirmMessage" class="confirmMessage"></span></div>
    </div>

    <div class="row" style="margin-top: 5px;margin-bottom: 5px;">
        <div  class="col-md-6"></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 col-sm-3 col-lg-3"><input type="button" id="btnSaveUser" value="Save" onclick="saveUser()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<script type="text/javascript">
    function saveUser() {
        if (fieldValidate() && checkPwd()) {
            document.getElementById("btnSaveUser").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateUser',
                type: 'POST',
                data: $('#addUser').serialize(),
                success: function (data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function () {
                        loadAddUserMain();
                    }, 2000);
                },
                error: function () {
                    alert("error Loading");
                }
            });
        }
    }
    //check User Password
    function checkPwd() {
        var password1 = document.getElementById('txtPwd1');
        var password2 = document.getElementById('txtPwd2');
        var message = document.getElementById('confirmMessage');
        var goodColor = "#00FF00";
        var badColor = "#FF0000";
        if (password1.value == password2.value) {
            $("#txtPwd2").removeClass("txtError");
            message.style.color = goodColor;
            message.innerHTML = "Passwords Match!"
            return  true;
        } else {
            $("#txtPwd2").addClass("txtError");
            message.style.color = badColor;
            message.innerHTML = "Passwords Do Not Match!"
            return false;
        }
    }
    function fieldValidate() {
        var password1 = document.getElementById('txtPwd1');
        var password2 = document.getElementById('txtPwd2');
        var userName = $("#txtUserName").find('option:selected').val();

        if (userName == 0) {
            $("#msgUserName").html("Select The User Name");
            $("#txtUserName").addClass("txtError");
            $("#txtUserName").focus();
            return false;
        } else {
            $("#txtUserName").removeClass("txtError");
            $("#msgUserName").html("");
            $("#msgUserName").removeClass("msgTextField");
        }
        if (password1.value == null || password1.value == "") {
            $("#msgPassword1").html("Password must be filled out");
            $("#txtPwd1").addClass("txtError");
            $("#txtPwd1").focus();
            return false;
        } else {
            $("#txtPwd1").removeClass("txtError");
            $("#msgPassword1").html("");
            $("#msgPassword1").removeClass("msgTextField");
        }
        if (password2.value == null || password2.value == "") {
            $("#msgPassword2").html("Confirm Password must be filled out");
            $("#txtPwd2").addClass("txtError");
            $("#txtPwd2").focus();
            return false;
        } else {
            $("#txtPwd2").removeClass("txtError");
            $("#msgPassword2").html("");
            $("#msgPassword2").removeClass("msgTextField");
        }
        return true;
    }
</script>
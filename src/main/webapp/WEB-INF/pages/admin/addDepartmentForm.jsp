<%-- 
    Document   : addBranchForm
    Created on : May 8, 2015, 2:17:55 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div><label class = "labelMessage" id="lblMessage"></label></div>
<form name="newDepartmentForm" id="newDepartment">
    <div class="row" style="margin: 5 8 2 0"><legend>Add New Department</legend></div>
    <input type="hidden" name="depId" value="${department.depId}"/>
    <div class="row">
        <div class="col-md-5" style="margin-left: -12px">Branch Name  </div>
        <div class="col-md-7">
            <input type="text" id="txtBranch" style="width: 75%;margin-left: -55px" readonly="" value="${branch.branchName}"/>
            <input type="hidden" id="txtBranch1" name="branchId"  value="${branch.branchId}"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">Department Name</div>
        <div class="col-md-7" style="margin-left: -40px"><input type="text" id="txtDepName" name="depName" style="width: 75%"/>
            <label id="msgDepName" class="msgTextField"></label>
        </div>
    </div>

    <div class="row" style="margin-top: 5px;margin-left: 28px;">
        <div  class="col-md-4"></div>
        <div  class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="unblockui()"/></div>
        <div  class="col-md-3"><input type="button" name="" value="Save"id="btnsaveBranch" onclick="saveDepartment()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
        <div  class="col-md-2"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<script>
    function saveDepartment() {
        if (fieldValidate()) {
            $.ajax({
                url: '/AxaBankFinance/saveNewDepartment',
                type: 'POST',
                data: $('#newDepartment').serialize(),
                success: function(data) {
                    $('#lblMessage').text(data);
                    $('#txtDepName').val("");d
//                    loadDepartment();
                },
                error: function() {
                    alert("Error Loding..");
                }
            });
        }
    }
    function fieldValidate() {
        var depName = document.getElementById('txtDepName');

        if (depName.value == "" || depName == null) {
            $("#msgDepName").html("Enter The Department Name");
            $("#txtDepName").addClass("txtError");
            $("#txtDepName").focus();
            return false;
        } else {
            $("#txtDepName").removeClass("txtError");
            $("#msgDepName").html("");
            $("#msgDepName").removeClass("msgTextField");
        }
        return true;
    }
</script>
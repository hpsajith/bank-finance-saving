<%-- 
    Document   : AddDocumentCheckListMain
    Created on : Apr 9, 2015, 11:46:38 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#tblDocumentList").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
          unblockui();
    });

</script>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-5" id="divDocumentList">
        
    </div>
    <div class="col-md-7" style="margin: 25 0 40 0">
        <table id="tblDocumentList" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Description</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="v_docList" items="${documentList}">
                    <tr id="${v_docList.id}">
                        <td>${v_docList.listDescription}</td>
                        
                        <td><div class="edit" href='#' onclick="clickToEdit(${v_docList.id})"></div></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
<script>
    $(function (){
        $.ajax({
            url: '/AxaBankFinance/loadDocumentListForm',
            success: function (data) {
                $('#divDocumentList').html(data);
            },
            error:function (){}
        });
    });
    
    function clickToEdit(docId){
            $.ajax({
                url: '/AxaBankFinance/findDocument/' + docId,
                success: function(data) {
                    $('#divDocumentList').html("");
                    $('#divDocumentList').html(data);
                    document.getElementById("btnSaveDocumentList").value="Update";
                },
                error: function() {
                    alert("Error View..!");
                }
            });        
    }
</script>



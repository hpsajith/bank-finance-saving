<%-- 
    Document   : addEmployee
    Created on : Apr 2, 2015, 9:32:33 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        pageAuthentication();
        $("#tblNewEmployee").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "90%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });
</script>


<div class="container-fluid"style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-7 col-sm-7 col-lg-7" id="viewAddEmploye"></div>
    <div class="col-md-5 col-sm-5 col-lg-5" style="margin-top: 45px">
        <div class="row" style="margin-bottom: 30px;">
            <table id="tblNewEmployee"class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>User Name</th>
                        <th>Edit</th>
                        <th>InActive</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="employees" items="${employeeList}">
                        <c:choose>
                            <c:when test="${employees.isActive == false}">
                                <tr id="${employees.empNo}" style="background-color: #BDBDBD;color: #888">
                                    <td>${employees.empLname}</td>
                                    <td>${employees.userName}</td>
                                    <td><div class="" href='#'></div></td>
                                    <td><div class="btnActive" href='#' id="btnInactive" onclick="clickToActive(${employees.empNo})"></div></td>
                                </tr> 
                            </c:when>
                            <c:otherwise>
                                <tr id="${employees.empNo}">
                                    <td>${employees.empLname}</td>
                                    <td>${employees.userName}</td>
                                    <td><div class="edit" href='#' onclick="clickToEdit(${employees.empNo})"></div></td>
                                    <td><div class="delete" href='#' id="btnInactive" onclick="clickToInactive(${employees.empNo})"></div></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>


                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/viewEmployeeForm',
            success: function(data) {
                $('#viewAddEmploye').html(data);
            },
            error: function() {
            }
        });
    });

    function clickToEdit(id) {
        $.ajax({
            url: '/AxaBankFinance/viewEditEmployee/' + id,
            success: function(data) {
                $('#viewAddEmploye').html("");
                $('#viewAddEmploye').html(data);
                document.getElementById("btnSaveNewEmployee").value = "Update";
                document.getElementById("btnBranch").disabled = false;
                document.getElementById("txtUserName").readOnly = true;
                var usertType = $("#userType").val();
                if (usertType === "0") {
                    $("#divAddPages").show();
                    document.getElementById("btnAddPages").disabled = false;
                } else {
                    if (usertType === "4") {
                        document.getElementById("btnClcDate").disabled = false;
                    } else {
                        document.getElementById("btnClcDate").disabled = true;
                    }
                }
            }
        });
    }
    function clickToInactive(id) {
        $.ajax({
            url: '/AxaBankFinance/inActiveEmployee/' + id,
            success: function(data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function() {
                    loadNewEmployeeForm();
                }, 2000);
            }
        });

    }

    function clickToActive(id) {
        $.ajax({
            url: '/AxaBankFinance/activeEmployee/' + id,
            success: function(data) {
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function() {
                    loadNewEmployeeForm();
                }, 2000);
            }
        });
    }


</script>


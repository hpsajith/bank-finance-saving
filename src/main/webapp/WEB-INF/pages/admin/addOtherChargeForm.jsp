<%-- 
    Document   : addOtherChargeForm
    Created on : Apr 10, 2015, 7:50:55 AM
    Author     : Msd
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form name="otherChargeForm" id="otherChargeList">
    <div class="row" style="margin: 5 8 2 0"><legend>Add Other Charges</legend></div>
    <div class="row">
        <input type="hidden" name="subTaxId" value="${otherCharge.subTaxId}"/>
        <div class="col-md-4">Description <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" name="subTaxDescription" id="txtDescription"value="${otherCharge.subTaxDescription}" style="width:100%"/></div>
        <label id="msgDescription" class="msgTextField"></label>
    </div>

    <div class="row">
        <div class="col-md-4">Chart of Account No <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" name="subTaxAccountNo" id="txtAccountNo" value="${otherCharge.subTaxAccountNo}" style="width:100%"/></div>
        <label id="msgAccountNo" class="msgTextField"></label>
    </div>

    <div class="row">
        <div class="col-md-4">Sub Tax Rate</div>
        <div class="col-md-8"><input type="text" name="subTaxRate" id="txtTaxRate"id="txtTaxRate" onkeypress="return checkNumbers(this)" value ="${otherCharge.subTaxRate}" onblur="hideTaxValue()" style="width:90%"/>%</div>
        <label id="msgTaxRate" class="msgTextField"></label>
    </div>

    <div class="row">
        <div class="col-md-4">Sub Tax Value</div>
        <div class="col-md-8"><input type="text" name="subTaxValue" id="txtTaxValue" id="taxValue" onkeypress="return checkNumbers(this)" onblur ="hideTaxRate()" value="${otherCharge.subTaxValue}" style="width:100%"/></div>
        <label id="msgTaxValue" class="msgTextField"></label>
    </div>

    <div class="row" style="display: none">
        <div class="col-md-4">Is Precentage</div>
        <div class="col-md-8">
            <div class="row"> 
                <c:choose>
                    <c:when test="${otherCharge.isPrecentage}">
                        <div class="col-md-1"><input type="radio" checked="checked" id="radioTax" name="isPrecentage" value="1"/></div>
                        <div class="col-md-4">Precentage</div>
                        <div class="col-md-1"><input type="radio" name="isPrecentage" id="radioValue" value="0"/></div>
                        <div class="col-md-4">Value</div>
                    </c:when>                    
                    <c:when test="${otherCharge.isPrecentage}">
                        <div class="col-md-1"><input type="radio" name="isPrecentage" id="radioTax" value="1"/></div>
                        <div class="col-md-4">Precentage</div>
                        <div class="col-md-1"><input type="radio" checked="checked" name="isPrecentage" id="radioValue" value="0"/></div>
                        <div class="col-md-4">Value</div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-1"><input type="radio" name="isPrecentage" onclick="clickRate()" id="radioTax" value="1"/></div>
                        <div class="col-md-4">Precentage</div>
                        <div class="col-md-1"><input type="radio" name="isPrecentage" id="radioValue" onclick="clickValue()" value="0"/></div>
                        <div class="col-md-4">Value</div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 15px">
        <div class="col-md-4"></div>
        <div  class="col-md-4 "><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-4 "><input type="button" name="" id="btnSaveOtherCharges" value="Save" onclick="saveUpdateOtherCharges()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<script>
    function saveUpdateOtherCharges() {

        if (fieldValidate()) {
            document.getElementById("btnSaveOtherCharges").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateOtherCharges',
                type: 'POST',
                data: $('#otherChargeList').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadOtherChargesMain();
                    }, 2000);
                },
                error: function() {
                    alert("Error Loading...!");
                }
            });
        }
    }
    function hideTaxValue() {
        var rate = document.getElementById('txtTaxRate');
        if (!(rate.value == null || rate.value == "")) {
            document.getElementById('txtTaxValue').disabled = true;
            document.getElementById("radioTax").checked = true;
        } else {
            document.getElementById("txtTaxValue").disabled = false;
            document.getElementById("radioTax").checked = false;
        }
    }

    function hideTaxRate() {
        var taxValue = document.getElementById('txtTaxValue');
        if (!(taxValue.value == 0 || taxValue.value == "")) {
            document.getElementById('txtTaxRate').disabled = true;
            document.getElementById("radioValue").checked = true;
        } else {
            document.getElementById('txtTaxRate').disabled = false;
            document.getElementById("radioValue").checked = false;
        }

    }
    function clickRate() {
        document.getElementById('txtTaxValue').value = "";
        document.getElementById("txtTaxRate").disabled = false;
        document.getElementById('txtTaxRate').focus();
    }
    function clickValue() {
        document.getElementById('txtTaxRate').value = "";
        document.getElementById('txtTaxValue').disabled = false;
        document.getElementById('txtTaxValue').focus();
    }

    function fieldValidate() {
        var des = $("#txtDescription").val();
        var accNo = $("#txtAccountNo").val();
        var taxRate = $("#txtTaxRate").val();
        var taxValue = $("#txtTaxValue").val();

        if (des == null || des == "") {
            $("#msgDescription").html("Description must be filled out");
            $("#txtDescription").addClass("txtError");
            $("#txtDescription").focus();
              return false;
        }
        else {
            $("#txtDescription").removeClass("txtError");
            $("#msgDescription").html("");
            $("#msgDescription").removeClass("msgTextField");
        }

        if (accNo == null || accNo == "") {
            $("#msgAccountNo").html("Tax Account No must be filled out");
            $("#txtAccountNo").addClass("txtError");
            $("#txtAccountNo").focus();
            return false;
        } else {
            $("#txtAccountNo").removeClass("txtError");
            $("#msgAccountNo").html("");
            $("#msgAccountNo").removeClass("msgTextField");
        }
        if (taxRate == "" || taxRate == null) {
            if (taxValue == "" || taxValue == "") {
                $("#msgTaxRate").html("Tax Rate must be filled out");
                $("#txtTaxRate").addClass("txtError");
                $("#txtTaxRate").focus();
                return false;
            }
            return true;
        } else {
            $("#txtTaxRate").removeClass("txtError");
            $("#msgUserName").html("");
            $("#msgUserName").removeClass("msgTextField");
        }
        if (taxValue == "" || taxRate == "") {
            if (taxRate == "" || taxRate == null) {
                $("#msgTaxValue").html("Tax Value must be filled out");
                $("#txtTaxValue").addClass("txtError");
                $("#txtTaxValue").focus();
                return false;
            }
            return true;
        } else {
            $("#txtTaxValue").removeClass("txtError");
            $("#msgTaxValue").html("");
            $("#msgTaxValue").removeClass("msgTextField");
        }

        return true;
    }



</script>


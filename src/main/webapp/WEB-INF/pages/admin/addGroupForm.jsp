<%-- 
    Document   : addGroupForm
    Created on : June 27, 2017, 8:24:43 AM
    Author     : Harshana
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div><label class = "successMsg">${successMsg} </label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>

<form name="newSeizerForm" id="newSeizer">
    <div class="row"><legend><strong>Add New Group Details</strong></legend></div>
    <input type="hidden" name="groupID" id="groupID" value="${group.groupID}"/>
    
    <div class="row">
        <div class="col-md-4">Branch <label class="redmsg">*</label></div>
        <div class="col-md-8">

            <select name="branchID" style="width: 172px" id="branchID">
                <option value="0"  >-select branch-</option>
                <c:forEach var="branch" items="${branchList}">
                    <c:choose>
                        <c:when test="${branch.branchId == group.branchID}">
                            <option selected="selected" value="${branch.branchId}">${branch.branchName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${branch.branchId}">${branch.branchName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>

            <label id="msgBranchId" class="msgTextField"></label>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">Center <label class="redmsg">*</label></div>
        <div class="col-md-8">

            <select name="centeID" style="width: 172px" id="centeID">
                <option value="0"  >-select center-</option>
                <c:forEach var="center" items="${centerList}">
                    <c:choose>
                        <c:when test="${center.centerID == group.centeID}">
                            <option selected="selected" value="${center.centerID}">${center.centerName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${center.centerID}">${center.centerName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
            <label id="msgCenterId" class="msgTextField"></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">Group <label class="redmsg">*</label></div>
        <div class="col-md-8"><input type="text" id="groupName" name="groupName" value="${group.groupName}" style="width: 80%"/>
            <label id="msgGroupName" class="msgTextField"></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">Detail <label class="redmsg"></label></div>
        <div class="col-md-8"><textarea type="text" id="detail" name="detail" style="width: 80%">${group.detail}</textarea>
            <label id="msgGroupDetail" class="msgTextField"></label>
        </div>
    </div>

    <div class="row" style="margin-top: 5px">
        <div  class="col-md-4"></div>
        <div  class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3"><input type="button" name="" value="Save"id="btnsaveSeizer" onclick="saveNewSeizer()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
        <div  class="col-md-2"></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<script>
    function saveNewSeizer() {
        if (fieldValidate()) {
            document.getElementById("btnsaveSeizer").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateGroup',
                type: 'POST',
                data: $('#newSeizer').serialize(),
                success: function (data) {
//                    alert(data);
//                    alert(data.successMsg);
//                    alert(data[0]);
                    
//                    $('#msgDiv').html(data);
//                    ui('#msgDiv');
//                    setTimeout(function () {
//                        loadAddGroupMain();
//                    }, 2000);
                    loadAddGroupMain();
                },
                error: function () {
                    alert("Error in Saving Seizer Details..!");
                }
            });
        }
    }

    function fieldValidate() {
        var groupName = $("#groupName").val();
        var branchID = $("#branchID").find('option:selected').val();
        var centerID = $("#centeID").find('option:selected').val();


        if (branchID == 0) {
            $("#msgBranchId").html("Select the branch");
            $("#branchID").addClass("txtError");
            $("#branchID").focus();
            return false;
        } else {
            $("#branchID").removeClass("txtError");
            $("#msgBranchId").html("");
            $("#msgBranchId").removeClass("msgTextField");
        }

        if (centerID == 0) {
            $("#msgCenterId").html("Select the center");
            $("#branchID").addClass("txtError");
            $("#branchID").focus();
            return false;
        } else {
            $("#branchID").removeClass("txtError");
            $("#msgCenterId").html("");
            $("#msgCenterId").removeClass("msgTextField");
        }

        if (groupName == "" || groupName == null) {
            $("#msgGroupName").html("Enter group name");
            $("#groupName").addClass("txtError");
            $("#groupName").focus();
            return false;
        } else {
            $("#groupName").removeClass("txtError");
            $("#msgGroupName").html("");
            $("#msgGroupName").removeClass("msgTextField");
        }


        return true;

    }

</script>

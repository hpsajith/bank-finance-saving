<%-- 
    Document   : addCenterMain
    Created on : June 22, 2017, 10:27:55 AM
    Author     : Harshana
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function () {
        pageAuthentication();
        $("#tblViewBranch").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use
        });
        unblockui();
    });

</script>

<div><label class = "successMsg">${successMsg}</label></div>
<div><label class = "errorMsg">${errorMsg}</label></div>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-6" id="divAddBranchForm">

    </div>
    <div class="col-md-6" style="margin:50 1 10 -1">
        <table id="tblViewBranch" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Branch</th>
                    <th>Code</th>
                    <th>Center Name</th>
                    <th>Edit</th>
                    <th>Active</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="v_center" items="${centerList}">
                    <c:choose>
                        <c:when test="${v_center.isActive ==0}">
                            <tr id="${v_center.branchID}" style="background-color: #BDBDBD;color: #888">
                                <c:forEach var="branch" items="${branchList}">
                                        <c:if test="${branch.branchId == v_center.branchID}">
                                            <td style="text-align: center">${branch.branchName} </td>
                                        </c:if>
                                    </c:forEach>
                                <td>${v_center.centerCode}</td>
                                <td>${v_center.centerName}</td>

                                <td><div class="" href='#'></div></td>
                                <td><div class="btnActive" href='#' id="btnInactive" onclick="clickToAtctive(${v_center.centerID})"></div></td>
                            </tr>
                        </c:when>
                        <c:when test="${v_center.isActive ==1}">
                            <tr id="${v_center.branchID}" >
                                <c:forEach var="branch" items="${branchList}">
                                        <c:if test="${branch.branchId == v_center.branchID}">
                                            <td style="text-align: center">${branch.branchName} </td>
                                        </c:if>
                                    </c:forEach>
                                <td>${v_center.centerCode}</td>
                                <td>${v_center.centerName}</td>
                                <td><div class="edit" href='#' onclick="clickToEdit(${v_center.centerID},${v_center.branchID})"></div></td>
                                <td><div class="delete" href='#' id="btnInactive" onclick="clickToDelete(${v_center.centerID})"></div></td>
                            </tr>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>
<script type="text/javascript">
    $(function () {
        $.ajax({
            url: '/AxaBankFinance/loadAddCenterForm',
            success: function (data) {
                $("#divAddBranchForm").html(data);
            }
        });
    });

    function clickToEdit(id, bid) {
        $.ajax({
            url: '/AxaBankFinance/editCenter/' + id,
            success: function (data) {

                $('#divAddBranchForm').html("");
                $('#divAddBranchForm').html(data);
                document.getElementById("btnsaveCenter").value = "Update";

                $('#branchID').val(bid);
                $("#branchID>option:eq(" + bid + ")").attr('selected', true);

//                $('#branchID option').eq(bid).prop('selected', true);
//
//                document.getElementById("branchID").disabled = true;
//                $("#branchID").addClass("txtError");
            }
        });

    }

    function clickToDelete(id) {
        $.ajax({
            url: '/AxaBankFinance/inActiveCenter/' + id,
            success: function (data) {
                if (data == true) {
                    loadAddCenterMain();
                }
            }
        });

    }

    function clickToAtctive(id) {
        $.ajax({
            url: '/AxaBankFinance/activeCenter/' + id,
            success: function (data) {
                if (data == true) {
                    loadAddCenterMain();
                }
            }
        });
    }

</script>

<%-- 
    Document   : addDocumentListForm
    Created on : Apr 9, 2015, 12:25:29 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form name="documentListForm" id="documentList">
    <div class="row" style="margin: 5 8 2 0"><legend>Add Documents</legend></div>
    <div class="row">
        <input type="hidden" name="id" value="${mcheckList.id}"/>
        <div class="col-md-4">List Description</div>
        <div class="col-md-8"><input type="text" name="listDescription" id="docDiscription" value="${mcheckList.listDescription}" style="width: 100%"/><label id="msgdis" class="msgTextField"></label></div>
    </div>


    <div class="row" style="margin-top: 15px">
        <div class="col-md-6"></div>
        <div  class="col-md-3 "><input type="button" name="" value="Cancel" class="btn btn-default col-md-12 col-sm-12 col-lg-12" onclick="pageExit()"/></div>
        <div  class="col-md-3 "><input type="button" name="" id="btnSaveDocumentList" value="Save" onclick="saveUpdateDocumentList()" class="btn btn-default col-md-12 col-sm-12 col-lg-12"/></div>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<script>
    function saveUpdateDocumentList() {
        if (fieldValidate()) {
            document.getElementById("btnSaveDocumentList").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateDocumentlist',
                type: 'POST',
                data: $('#documentList').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadDocumentListMain();
                    }, 2000);
                   
                },
                error: function() {
                    alert("Error Saving..!");
                }
            });
        }
    }
    function fieldValidate() {
        var docDis = document.getElementById('docDiscription');
        if (docDis.value == 0 || docDis.value == "") {
            $("#msgdis").html("Discription must be filled out");
            $("#docDiscription").addClass("txtError");
            $("#docDiscription").focus();
            return false;
        }
        return true;
    }


</script>
<%-- 
    Document   : addVehicleModelForm
    Created on : Jul 28, 2016, 5:21:42 PM
    Author     : User
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        $("tblMake").chromatable({
            width: "100%", //specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>
<div class="row">
    <div class="col-md-5"><strong>Add Vehicle Model</strong></div>
    <div class="col-md-6"><label id="msg"></label></div>
</div>
<div class="container-fluid">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row" style="margin: 1px; color: red; text-align: left;"><label id="errmsgForTab"></label></div>
    <form name="vehicleModelForm" id="vehicleModel">
        <input type="hidden" name="vehicleTypeId" id="txtVehicle_Type" value="${vehicleTypeId}"/>
        <div class="row">
            <div class="col-md-4">Vehicle Model<label class="redmsg">*</label></div>
            <div class="col-md-7"><input type="text" id="txtVehicleModel" name="vehicleModelName" value="${vehicle.vehicleModelName}" style="width: 100%"/>
                <label id="msgVehicleModel" class="msgTextField"></label>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row">
            <div class="col-md-4">Vehicle Make<label class="redmsg">*</label></div>
            <div class="col-md-7">
                <select name="vehicleMakeId" id="vehicle_Make" class="col-md-12">
                    <option value="0">SELECT</option>
                    <c:forEach var="make" items="${makeList}">
                        <option id="makeValue" value="${make.vehicleMake}">${make.vehicleMakeName}</option>
                    </c:forEach>
                </select>
                <label id="msgVehicleMake" class="msgTextField"></label>
            </div>
            <div class="col-md-1"></div>
        </div>

        <table id="tblModel" style="margin-top: 5px" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: left">Model Name</th>                    
                </tr>
            </thead>
            <tbody>
                <c:forEach var="vehicle_model" items="${modelList}">
                    <c:choose>
                        <c:when test="${vehicle_model.isActive == false}">
                            <tr id="${vehicle_model.vehicleModel}" style="background-color: #BDBDBD;color: #888">
                                <td style="text-align: left">${vehicle_model.vehicleModelName}</td>
                            </tr> 
                        </c:when>
                        <c:otherwise>
                            <tr id="${vehicle_model.vehicleModel}">
                                <td style="text-align: left">${vehicle_model.vehicleModelName}</td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </tbody>
        </table>

        <div class="row" style="margin-top: 5%">
            <div class="col-md-4"></div>
            <div class="col-md-2"></div>
            <div class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"/></div>
            <div class="col-md-3"><input type="button" id="btnSaveModel" name="" value="Save" onclick="saveNewModel()" class="btn btn-default col-md-12"/></div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />   
    </form>
</div>

<script>
    function saveNewModel() {
        if (fieldValidate()) {
            document.getElementById("btnSaveModel").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateModel',
                type: 'POST',
                data: $('#vehicleModel').serialize(),
                success: function(data) {
                    $('#msg').html(data);
                    setTimeout(function() {
                        loadModelForm();
                    }, 2000);
                },
                error: function() {
                }
            });
        }
        function fieldValidate() {
            var vehicleModel = document.getElementById('txtVehicleModel');
            var vehicleMake = document.getElementById('vehicle_Make');

            if (vehicleModel.value == null || vehicleModel.value == "") {
                $("#msgVehicleModel").html("Vehicle Model must be filled out");
                $("#txtVehicleModel").addClass("txtError");
                $("#txtVehicleModel").focus();
                return false;
            } else {
                $("#txtVehicleModel").removeClass("txtError");
                $("#msgVehicleModel").html("");
                $("#msgVehicleModel").removeClass("msgTextField");
            }

            if (vehicleMake.value == null || vehicleMake.value == "" || vehicleMake.value == 0) {
                $("#msgVehicleMake").html("Vehicle Make must be filled out");
                $("#vehicle_Make").addClass("txtError");
                $("#vehicle_Make").focus();
                return false;
            } else {
                $("#vehicle_Make").removeClass("txtError");
                $("#msgVehicleMake").html("");
                $("#msgVehicleMake").removeClass("msgTextField");
            }
            return true;
        }
    }
</script>

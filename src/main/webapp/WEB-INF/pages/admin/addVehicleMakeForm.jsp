<%-- 
    Document   : vehicleMakeForm
    Created on : Jul 28, 2016, 10:20:34 AM
    Author     : User
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        $("tblMake").chromatable({
            width: "100%", //specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>
<div class="row">
    <div class="col-md-5"><strong>Add Vehicle Make</strong></div>
    <div class="col-md-6"><label id="msg"></label></div>
</div>
<div class="container-fluid">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row" style="margin: 1px; color: red; text-align: left;"><label id="errmsgForTab"></label></div>
    <form name="vehicleMakeForm" id="vehicleMake">
        <input type="hidden" name="vehicleTypeId" id="txtVehicle_Type" value="${vehicle_Type}"/>
        <div class="row">
            <div class="col-md-4">Vehicle Make<label class="redmsg">*</label></div>
            <div class="col-md-7"><input type="text" id="txtVehicleMake" name="vehicleMakeName" value="${vehicle.vehicleMakeName}" style="width: 100%"/>
                <label id="msgVehicleMake" class="msgTextField"></label>
            </div>
            <div class="col-md-1"></div>
        </div>

        <table id="tblMake"class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="text-align: left">Make Name</th>                    
                </tr>
            </thead>
            <tbody>
                <c:forEach var="vehicle_make" items="${vehicleMakesList}">
                    <c:choose>
                        <c:when test="${vehicle_make.isActive == false}">
                            <tr id="${vehicle_make.vehicleMake}" style="background-color: #BDBDBD;color: #888">
                                <td style="text-align: left">${vehicle_make.vehicleMakeName}</td>
                            </tr> 
                        </c:when>
                        <c:otherwise>
                            <tr id="${vehicle_make.vehicleMake}">
                                <td style="text-align: left">${vehicle_make.vehicleMakeName}</td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </tbody>
        </table>

        <div class="row" style="margin-top: 5%">
            <div class="col-md-4"></div>
            <div class="col-md-2"></div>
            <div class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"/></div>
            <div class="col-md-3"><input type="button" id="btnSaveMakeType" name="" value="Save" onclick="saveNewMakeType()" class="btn btn-default col-md-12"/></div>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />   
    </form>


</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script>
    function saveNewMakeType() {
        if (fieldValidate()) {
            document.getElementById("btnSaveMakeType").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveOrUpdateMakeType',
                type: 'POST',
                data: $('#vehicleMake').serialize(),
                success: function(data) {
                    $('#msg').html(data);
                    setTimeout(function() {
                        loadMakeForm();
                    }, 2000);
                },
                error: function() {
                }
            });
        }

        function fieldValidate() {
            var vehicleMake = document.getElementById('txtVehicleMake');
            if (vehicleMake.value == null || vehicleMake.value == "") {
                $("#msgVehicleMake").html("Vehicle Make must be filled out");
                $("#txtVehicleMake").addClass("txtError");
                $("#txtVehicleMake").focus();
                return false;
            } else {
                $("#txtVehicleMake").removeClass("txtError");
                $("#msgVehicleMake").html("");
                $("#msgVehicleMake").removeClass("msgTextField");
            }
            return true;
        }
    }
</script>
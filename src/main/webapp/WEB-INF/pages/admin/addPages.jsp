<%-- 
    Document   : addPages
    Created on : May 5, 2015, 3:23:48 PM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script>
    $(document).ready(function() {
        $("#tblPages").chromatable({
            width: "100%", //specify 100%, auto, or a fixed pixel amount
            height: "70%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
    });

</script>

<div class="row" style="margin:-25 305 5 0"><strong>Define Job</strong></div>
<div class="container-fluid">
    <div class="close_div" onclick="unblockui()">X</div>
    <div class="row" style="margin: 1px; color: red; text-align: left;"><label id="errmsgForTab"></label></div>
    <form name="jobsform" id="jobs">
        <input type="hidden" name="userTypeId" id="txtUserTypeId" value="${userTpe}"/>
        <input type="hidden" name="isUserType" id="txtUserType" value="0"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />   
        <div id="hidePages" style="display: none"></div>
    </form>
    <table id="tblPages"  class="dc_fixed_tables table-bordered" width="100%" border="3" cellspacing="1" cellpadding="1">
        <thead>
            <tr>
                <th>#</th>
                <th>Tab</th>
            </tr>
        </thead>
        <tbody>

            <c:forEach var="v_pages" items="${jobs}">
                <tr id="${v_pages.index}">
                    <c:forEach var="d_defineJobs" items="${defineJobs}">
                        <c:if test="${d_defineJobs.tabId == v_pages.tabId && d_defineJobs.isMainTab == v_pages.isMainTab}">
                            <c:set var="isChecked" value="true"></c:set>
                        </c:if>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${isChecked}">
                            <td><input type="checkbox" checked="checked" id="pages${v_pages.index}"</td>
                            </c:when>
                            <c:otherwise>                                
                            <td><input type="checkbox" id="pages${v_pages.index}"</td>
                            </c:otherwise>
                        </c:choose>
            <input type="hidden" id="tab${v_pages.index}" value="${v_pages.tabId}"/>
            <input type="hidden" id="isMain${v_pages.index}" value="${v_pages.isMainTab}"/>
            <td>${v_pages.tabName}</td>        
            </tr>
            <c:set var="isChecked" value="false"></c:set>
        </c:forEach>
        </tbody>
    </table>

    <div class="row" style="margin-top: 5%">
        <div class="col-md-4"></div>
        <div class="col-md-2"></div>
        <div class="col-md-3"><input type="button" name="" value="Cancel" class="btn btn-default col-md-12" onclick="unblockui()"/></div>
        <div class="col-md-3"><input type="button" id="btnSaveUserType" name="" value="Save" onclick="saveAddPages()" class="btn btn-default col-md-12"/></div>
    </div>
</div>
<div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>

<script>
    function saveAddPages() {
        if (!setToPages()) {
            $("#errmsgForTab").text("Please select atleast one job");
        } else {
            document.getElementById("btnSaveUserType").disabled = true;
            $.ajax({
                url: '/AxaBankFinance/saveAddPages',
                type: 'POST',
                data: $('#jobs').serialize(),
                success: function(data) {
                    $('#msgDiv').html(data);
                    ui('#msgDiv');
                    setTimeout(function() {
                        loadAddUserTypeForm();
                    }, 2000);

                },
                error: function() {
                    alert("Error Loading...!");
                }

            });
        }
    }

    function  setToPages() {
        var hasPages = false;
        $('#tblPages tbody tr').each(function() {
            var pageIndex = $(this).attr('id');
            var tabId = $("#tab" + pageIndex).val();
            var isMain = $("#isMain" + pageIndex).val();
            if ($('#pages' + pageIndex).is(':checked')) {
                hasPages = true;
                $("#hidePages").append("<input type='text' name='tabId' value='" + tabId + "'/>" +
                        "<input type='text' name='isMainId' value='" + isMain + "'/>");
            }
        });
        return hasPages;
    }
</script>




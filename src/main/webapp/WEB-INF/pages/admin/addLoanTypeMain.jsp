<%-- 
    Document   : addLoanTypeForm
    Created on : Mar 2, 2015, 9:42:25 AM
    Author     : MsD
--%>
<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#viewLoanType").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "75%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
        unblockui();
    });

</script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">

    <div class="col-md-12 col-sm-12 col-lg-12" style="width: 100%">
        <div class="row">
            <div class="col-md-5 col-sm-5 col-lg-5" id="divLoanType" style="margin: 24px;">
                <!--add Loan Type Div-->
            </div>
            <div class="col-md-2 col-sm-2 col-lg-2" style="margin-right: -40px"></div>

            <div class="col-md-5 col-sm-5 col-lg-5" id="divViewLoanType" style="margin: 45px">
                <div class="row" style="overflow-y: auto;height: 85%">
                    <table id="viewLoanType" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th style="text-align: center">Loan Type</th>
                                <th style="text-align: center">Code</th>
                                <th style="text-align: center">Edit</th>
                                    <c:if test="${isSuperAdmin}">
                                    <th style="text-align: center">In-Active</th>
                                    </c:if>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="loanType" items="${mLoanTypes}">
                                <c:choose>
                                    <c:when test="${loanType.isActive}">
                                        <tr id="${loanType.loanTypeId}">
                                        </c:when>
                                        <c:otherwise>
                                        <tr id="${loanType.loanTypeId}" style='background-color: #B8B8B8 ;color: #000000'>
                                        </c:otherwise>                                     
                                    </c:choose>
                                    <td style="text-align: center">${loanType.loanTypeName}</td>
                                    <td style="text-align: center">${loanType.loanTypeCode}</td>
                                    <td style="text-align: center">
                                        <c:choose>
                                            <c:when test="${loanType.isActive}">
                                                <div class='edit' href='#' onclick='clickToEdit(${loanType.loanTypeId})'></div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class='edit' href='#'></div>
                                            </c:otherwise>   
                                        </c:choose>
                                    </td>
                                    <c:if test="${isSuperAdmin}">
                                        <td style="text-align: center">
                                            <c:choose>
                                                <c:when test="${loanType.isActive}">
                                                    <div class="delete" href='#' title="In-Active" onclick="changeLoanType(${loanType.loanTypeId}, 0)"></div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="btnActive" href='#' title="Active" onclick="changeLoanType(${loanType.loanTypeId}, 1)"></div>
                                                </c:otherwise>   
                                            </c:choose>
                                        </td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div class="dc_clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/addLoanTypeForm',
            success: function(data) {
                $('#divLoanType').html(data);
            },
            error: function() {
            }
        });
    });

    function clickToEdit(ID) {
        // alert("msd");
        $.ajax({
            url: '/AxaBankFinance/viewLoanType/' + ID,
            success: function(data) {
                $('#divLoanType').html("");
                $('#divLoanType').html(data);
                document.getElementById("btnSaveLoanType").value = "Update";
            },
            error: function() {
                alert("Error View..!");
            }
        });
    }

    function changeLoanType(loanTypeId, status) {
        $.ajax({
            url: "/AxaBankFinance/changeLoanType",
            data: {"loanTypeId": loanTypeId, "status": status},
            success: function(data) {
                $('#msgDiv').html("");
                $('#msgDiv').html(data);
                ui('#msgDiv');
                setTimeout(function() {
                    loadAddLoanTypeForm();
                }, 2000);
            },
            error: function() {
                console.log("Error in change loan type");
            }
        });
    }

</script>

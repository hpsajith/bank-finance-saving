<%-- 
    Document   : addOtherChargeMain
    Created on : Apr 10, 2015, 7:50:35 AM
    Author     : MsD
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function() {
        pageAuthentication();
        $("#tblOtherCharge").chromatable({
            width: "100%", // specify 100%, auto, or a fixed pixel amount
            height: "80%",
            scrolling: "yes" // must have the jquery-1.3.2.min.js script installed to use

        });
          unblockui();
    });

</script>

<div class="container-fluid" style="border:1px solid #BDBDBD; border-radius: 2px;">
    <div class="col-md-5" id="divOtherChargeForm"></div>
    <div class="col-md-7" style="margin-top: 50px; margin-bottom: 20px;">
        <table id="tblOtherCharge" class="dc_fixed_tables table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Rate</th>
                    <th>Value</th>
                    <th></th>
                </tr>
            </thead>
            <c:forEach var="charges" items="${otherCharges}">
                <tr id="${charges.subTaxId}">
                    <td>${charges.subTaxDescription}</td>
                    <td>${charges.subTaxRate}</td>
                    <td>${charges.subTaxValue}</td>
                    <td><div class="edit" href='#' onclick="clickToEdit(${charges.subTaxId})"></div></td>
                </tr>
            </c:forEach>
            <tbody>

            </tbody>
        </table>
    </div>
    <div id="msgDiv" class="searchCustomer" style="width: 35%;margin-left: 10%;"></div>
</div>

<script>
    $(function() {
        $.ajax({
            url: '/AxaBankFinance/viewOtherChargesForm',
            success: function(data) {
                $('#divOtherChargeForm').html(data);
            },
            error: function() {
            }
        });
    });
    function clickToEdit(Id) {
        $.ajax({
            url: '/AxaBankFinance/viewEditOtherCharges/' + Id,
            success: function(data) {
                $('#divOtherChargeForm').html("");
                $('#divOtherChargeForm').html(data);
                document.getElementById("btnSaveOtherCharges").value = "Update";
            }
        });
    }

</script>


<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="classic"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="535"
		 pageHeight="802"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="0"
		 rightMargin="0"
		 topMargin="0"
		 bottomMargin="0"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="SUBREPORT_DIR" isForPrompting="true" class="java.lang.String">
		<defaultValueExpression ><![CDATA[".\\"]]></defaultValueExpression>
	</parameter>
	<parameter name="date1" isForPrompting="true" class="java.lang.String"/>
	<parameter name="date2" isForPrompting="true" class="java.lang.String"/>
	<parameter name="branchId" isForPrompting="true" class="java.lang.Integer"/>
	<queryString><![CDATA[SELECT
  ch.Account_Name,
  SUM(sp.PaymentAmount)
  
FROM settlement_payment_other AS sp,
  settlement_payment_type AS pt,
  chartofaccount AS ch,
  um_user AS u
WHERE sp.SystemDate BETWEEN  $P{date1} AND $P{date2}
    AND sp.BranchId = $P{branchId}
    AND sp.PaymentType = pt.Paymant_Type_Id
    AND sp.AccountNo = ch.Account_No
    AND sp.UserId = u.User_ID
    AND sp.AccountNo NOT LIKE 'I%'
    GROUP BY ch.Account_No]]></queryString>

	<field name="Account_Name" class="java.lang.String"/>
	<field name="SUM(sp.PaymentAmount)" class="java.lang.Double"/>

	<variable name="totalPayments" class="java.lang.Double" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$F{SUM(sp.PaymentAmount)}]]></variableExpression>
	</variable>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="15"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="130"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-1"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="Thin" leftBorderColor="#000000" leftPadding="5" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Justified" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Account_Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="130"
						y="0"
						width="130"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="8" rightBorder="Thin" rightBorderColor="#000000" rightPadding="5" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.Double"><![CDATA[$F{SUM(sp.PaymentAmount)}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="45"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="130"
						y="0"
						width="130"
						height="15"
						key="textField-3"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="8" rightBorder="Thin" rightBorderColor="#000000" rightPadding="5" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Double"><![CDATA[$V{totalPayments}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="130"
						height="15"
						key="staticText-1"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="Thin" leftBorderColor="#000000" rightBorder="Thin" rightBorderColor="#000000" rightPadding="3" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Other Payments receivable]]></text>
				</staticText>
				<subreport  isUsingCache="true">
					<reportElement
						x="0"
						y="15"
						width="260"
						height="28"
						key="subreport-1"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="SUBREPORT_DIR">
						<subreportParameterExpression><![CDATA[$P{SUBREPORT_DIR}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter  name="sumOfOther">
						<subreportParameterExpression><![CDATA[$V{totalPayments}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{SUBREPORT_DIR} + "cashier_report_cash_all_sub_other_sub_oth_insu.jasper"]]></subreportExpression>
				</subreport>
			</band>
		</summary>
</jasperReport>

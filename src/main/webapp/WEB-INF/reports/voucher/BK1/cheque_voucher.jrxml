<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="cheque_voucher"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="voucher_no" isForPrompting="true" class="java.lang.Integer"/>
	<parameter name="SUBREPORT_DIR" isForPrompting="true" class="java.lang.String">
		<defaultValueExpression ><![CDATA[".//"]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT settlement_voucher.Voucher_No AS 'Voucher_NO',  
debtor_header_details.Debtor_AccountNo AS 'Account_No', 
loan_header_details.Loan_AgreementNo AS 'Agreement_No',
loan_header_details.Loan_Book_No AS 'Book_No',
loan_header_details.Loan_Amount AS 'Paid_Sum',
settlement_voucher.Amount AS 'Amount',
debtor_header_details.Debtor_Name AS 'To',
um_user.User_Name AS 'User',
settlement_voucher.Loan_Id AS 'loanId'
FROM settlement_voucher,loan_header_details,debtor_header_details,um_user
WHERE settlement_voucher.Id=$P{voucher_no} AND settlement_voucher.Loan_Id=loan_header_details.Loan_Id 
AND loan_header_details.Loan_Debtor_Id=debtor_header_details.Debtor_Id 
AND um_user.User_ID=settlement_voucher.User_Id]]></queryString>

	<field name="Voucher_NO" class="java.lang.String"/>
	<field name="Account_No" class="java.lang.String"/>
	<field name="Agreement_No" class="java.lang.String"/>
	<field name="Book_No" class="java.lang.String"/>
	<field name="Paid_Sum" class="java.math.BigDecimal"/>
	<field name="Amount" class="java.lang.Double"/>
	<field name="To" class="java.lang.String"/>
	<field name="User" class="java.lang.String"/>
	<field name="loanId" class="java.lang.Integer"/>

	<variable name="paidSum" class="java.lang.Double" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[new Double($V{paidCash}.doubleValue()+$V{paidCheq}.doubleValue())]]></variableExpression>
	</variable>
	<variable name="paidCash" class="java.lang.Double" resetType="Report" calculation="Nothing">
	</variable>
	<variable name="paidCheq" class="java.lang.Double" resetType="Report" calculation="Nothing">
	</variable>
	<variable name="loanAmount" class="java.lang.Double" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{Amount}]]></variableExpression>
	</variable>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="32"  isSplitAllowed="true" >
				<subreport  isUsingCache="true">
					<reportElement
						x="14"
						y="5"
						width="512"
						height="25"
						key="subreport-2"/>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{SUBREPORT_DIR} + "Company_Details.jasper"]]></subreportExpression>
				</subreport>
			</band>
		</title>
		<pageHeader>
			<band height="85"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="14"
						y="10"
						width="356"
						height="1"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="370"
						y="0"
						width="144"
						height="19"
						key="staticText-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="4" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Serif" pdfFontName="Helvetica-Bold" size="13" isBold="true"/>
					</textElement>
				<text><![CDATA[PAYMENT VOUCHER]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="517"
						y="10"
						width="12"
						height="1"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="113"
						y="13"
						width="139"
						height="15"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Voucher_NO}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="15"
						y="14"
						width="85"
						height="16"
						key="staticText-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Voucher No ]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="15"
						y="32"
						width="85"
						height="17"
						key="staticText-5"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Agreement No]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="15"
						y="70"
						width="85"
						height="14"
						key="staticText-7"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[TO]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="113"
						y="31"
						width="139"
						height="17"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Agreement_No}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="379"
						y="21"
						width="33"
						height="19"
						key="staticText-8"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Date  :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="yyyy-MMM-dd  hh:mm aaa" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="412"
						y="21"
						width="117"
						height="17"
						key="textField-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Top">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="412"
						y="40"
						width="117"
						height="18"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{User}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="100"
						y="14"
						width="13"
						height="18"
						key="staticText-20"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="100"
						y="32"
						width="13"
						height="17"
						key="staticText-22"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="379"
						y="40"
						width="32"
						height="18"
						key="staticText-24"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[User :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="15"
						y="51"
						width="85"
						height="17"
						key="staticText-25"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Member No]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="100"
						y="51"
						width="13"
						height="17"
						key="staticText-26"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="113"
						y="51"
						width="139"
						height="17"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Book_No}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="24"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="15"
						y="1"
						width="98"
						height="18"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Account_No}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="113"
						y="1"
						width="280"
						height="18"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{To}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="10"
						y="21"
						width="516"
						height="1"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</columnHeader>
		<detail>
			<band height="250"  isSplitAllowed="true" >
				<subreport  isUsingCache="true">
					<reportElement
						x="17"
						y="21"
						width="500"
						height="99"
						key="subreport-3"/>
					<subreportParameter  name="voucher_no">
						<subreportParameterExpression><![CDATA[$P{voucher_no}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter  name="SUBREPORT_DIR">
						<subreportParameterExpression><![CDATA[$P{SUBREPORT_DIR}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter  name="loanAmount">
						<subreportParameterExpression><![CDATA[$V{loanAmount}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter  name="loan_id">
						<subreportParameterExpression><![CDATA[$F{loanId}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{SUBREPORT_DIR} + "main_sub.jasper"]]></subreportExpression>
				</subreport>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="120"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="10"
						y="2"
						width="519"
						height="1"
						key="line-4"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="14"
						y="8"
						width="59"
						height="16"
						key="staticText-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[Towards ]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="32"
						y="85"
						width="94"
						height="15"
						key="staticText-11"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Top">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[-------------------------------]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="32"
						y="97"
						width="94"
						height="16"
						key="staticText-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[Prepared By]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="157"
						y="97"
						width="83"
						height="16"
						key="staticText-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[Checked By]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="157"
						y="85"
						width="84"
						height="15"
						key="staticText-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Top">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[------------------------------]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="266"
						y="85"
						width="106"
						height="15"
						key="staticText-15"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Top">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[-----------------------------------]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="266"
						y="97"
						width="106"
						height="16"
						key="staticText-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[Authorized Signature]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="398"
						y="97"
						width="108"
						height="16"
						key="staticText-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[Recipient Signature]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="397"
						y="85"
						width="109"
						height="15"
						key="staticText-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Top">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<text><![CDATA[----------------------------------]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="81"
						y="7"
						width="106"
						height="17"
						key="textField-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Agreement_No}]]></textFieldExpression>
				</textField>
			</band>
		</summary>
</jasperReport>

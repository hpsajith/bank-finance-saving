
function success(msg, view) {
    $.ajax({
        url: "/AxaBankFinance/loadSuccessBox/" + msg,
        success: function(data) {
            $("#" + view).html(data);
            ui("#" + view);
        }
    });
}
function warning(msg, view) {
    $.ajax({
        url: "/AxaBankFinance/loadWarningBox/" + msg,
        success: function(data) {
            $("#" + view).html(data);
            ui("#" + view);
        }
    });
}

function dialog(msg, view, loanId) {
    $.ajax({
        url: "/AxaBankFinance/loadDialogBox/" + msg + "/" + loanId,
        success: function(data) {
            $("#" + view).html(data);
            ui("#" + view);
        }
    });
}

function error(msg, view) {
    $.ajax({
        url: "/AxaBankFinance/loadErrorBox/" + msg,
        success: function(data) {
            $("#" + view).html(data);
            ui("#" + view);
        }
    });
}

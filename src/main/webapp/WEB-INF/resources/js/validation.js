/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//function numbersOnly(evt) {
//    var theEvent = evt || window.event;
//  var key = theEvent.keyCode || theEvent.which;
//  key = String.fromCharCode( key );
//  var regex = /[0-9]|\./;
//  if( !regex.test(key) ) {
//    theEvent.returnValue = false;
//    if(theEvent.preventDefault) theEvent.preventDefault();
//  }
//}

//checking numbers
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function validateForm() {
    //< !-- Real - time Validation -- >
    //        < !--Name can't be blank-->
    $('#txtloanAmount').on('input', function () {
        var input = $(this);
        var is_name = input.val();
        if (is_name) {
            input.removeClass("invalid").addClass("valid");
        }
        else {
            input.removeClass("valid").addClass("invalid");
        }
    });
    //        < !--Name can't be blank-->
    $('#txtloanPeriod').on('input', function () {
        var input = $(this);
        var is_name = input.val();
        if (is_name) {
            input.removeClass("invalid").addClass("valid");
        }
        else {
            input.removeClass("valid").addClass("invalid");
        }
    });

//    //        < !--Email must be an email -- >
//    $('#contact_email').on('input', function() {
//        var input = $(this);
//        var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
//        var is_email = re.test(input.val());
//        if (is_email) {
//            input.removeClass("invalid").addClass("valid");
//        }
//        else {
//            input.removeClass("valid").addClass("invalid");
//        }
//    });
//    //        < !--Website must be a website -- >
//    $('#contact_website').on('input', function() {
//        var input = $(this);
//        if (input.val().substring(0, 4) == 'www.') {
//            input.val('http://www.' + input.val().substring(4));
//        }
//        var re = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
//        var is_url = re.test(input.val());
//        if (is_url) {
//            input.removeClass("invalid").addClass("valid");
//        }
//        else {
//            input.removeClass("valid").addClass("invalid");
//        }
//    });
//    //        < !--Message can't be blank -->
//    $('#contact_message').keyup(function(event) {
//        var input = $(this);
//        var message = $(this).val();
//        console.log(message);
//        if (message) {
//            input.removeClass("invalid").addClass("valid");
//        }
//        else {
//            input.removeClass("valid").addClass("invalid");
//        }
//    });
    //        < !-- After Form Submitted Validation-- >
    $("#btnSubmit").click(function (event) {
        var form_data = $("#basic-loan-form").serializeArray();
        var error_free = true;
        for (var input in form_data) {
            var element = $("#basic-loan-form_" + form_data[input]['txtloanAmount']);
            var valid = element.hasClass("valid");
            var error_element = $("span", element.parent());
            if (!valid) {
                error_element.removeClass("error").addClass("error_show");
                error_free = false;
            }
            else {
                error_element.removeClass("error_show").addClass("error");
            }
        }
        if (!error_free) {
            event.preventDefault();
        }
        else {
            alert('No errors: Form will be submitted');
        }
    });
}

////////////////////////////////////////
function nictoLowerCase(nic) {
    var v = nic.value;
    nic.value = v.toUpperCase();
}
function checkNIC(nic) {
    var v = document.getElementById(nic).value;
    document.getElementById(nic).value = v.toUpperCase();
    if (v.length === 12) {
        if (isNaN(v)) {
            alert("NIC Length is not Valid!");
            document.getElementById(nic).value = "";
            document.getElementById(nic).style.border = "1px solid red";
            return false;
        } 
    } else {
        if (v.length === 10) {
        } else {
            alert("NIC Length is not Valid!");
            document.getElementById(nic).value = "";
            document.getElementById(nic).style.border = "1px solid red";
            return false;
        }
        var s = v.substr(0, 9);
        var cond = isFinite(s);
        if (cond === true) {
        } else {
            alert("Input 9 Numbers and a letter for NIC!");
            document.getElementById(nic).value = "";
            document.getElementById(nic).style.border = "1px solid red";
            return false;
        }
        if (v.charAt(9) === "X" || v.charAt(9) === "V" || v.charAt(9) === "x" || v.charAt(9) === "v") {
        } else {
            alert("NIC Last index must be V or X!");
            document.getElementById(nic).value = "";
            document.getElementById(nic).style.border = "1px solid red";
            return false;
        }
        document.getElementById(nic).style.border = "1px solid #c0c0c0";
    }
    return true;
}
function setBColour(text) {
    if (document.getElementById(text).value.length <= 0) {
        document.getElementById(text).style.border = "1px solid red";
    } else {
        document.getElementById(text).style.border = "1px solid #c0c0c0";
    }
}
function checkNumbers(feild) {
    if (isNaN(feild.value)) {
        feild.value = "";
    } else {

    }
}

function checkTelNumbers(feild) {
    var phoneno = /^\d{10}$/;
    if (feild.value.match(phoneno)) {

    } else {
        feild.style.border = "1px solid red";
        feild.value = "";
    }
}
function validateEmail(email, lable)
{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value))
    {
        if (email.style.border === "1px solid red") {
            email.style.border = "1px solid #c0c0c0";
            $(lable).html("");
        }
        return true;
    }
    if (!email.value <= 0) {
        email.style.border = "1px solid red";
        $(lable).html("You have entered an invalid email address!");
        $(lable).removeClass('msgTextField');
        $(lable).addClass('msgTextField');
    } else {

        if (email.style.border === "1px solid red") {
            email.style.border = "1px solid #c0c0c0";
            $(lable).removeClass('msgTextField');
            $(lable).html("");
        }
    }

    return false;
}
//function resetFields() {
//    var frm = document.getElementById('loan_form_temp');
//    for (i = 0; i < frm.length; i++) {
//        if (frm.elements[i].style.border === "1px solid red") {
//            frm.elements[i].style.border = "1px solid #c0c0c0";
//        }
//        //alert(frm.elements[i]);
//    }
//}

function validateFields(feilds) {
    for (i = 0; i < feilds.length; i++) {
        var feild = feilds[i];
        if (feild.value.length <= 0) {
            feild.style.border = "1px solid red";
            feild.addEventListener('keyup', function () {
                feild.style.border = "1px solid #c0c0c0";
            }, false);
            return false;
        } else {

        }
    }
    return true;
}

function validateOneField(fields, clsname) {
    for (i = 0; i < fields.length; i++) {
        var field = fields[i];
        if (field.value.length <= 0) {
            field.addEventListener('keyup', function () {
                $(clsname).css("border", "1px solid #c0c0c0");
            }, false);
        } else {
            $(clsname).css("border", "1px solid #c0c0c0");
            return true;
        }
        $(clsname).css("border", "1px solid red");
    }
}

